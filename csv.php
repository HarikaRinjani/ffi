<?php
/**
 * (c)2013 Transformatika
 * User: ag <agung@transformatika.com>
 * Date: 6/13/13
 * Time: 3:51 PM
 *
 */
//error_reporting(0);
ini_set('display_errors','Off');
 
if(php_sapi_name() !== 'cli'){
    die('Please run this file only on CLI mode');
}

setlocale(LC_ALL, 'id_ID.UTF8', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');
// I18N support information here
$language = "id_ID";
putenv("LANG=" . $language);
setlocale(LC_ALL, $language);

$domain = "messages";
bindtextdomain($domain, "Locale");
bind_textdomain_codeset($domain, 'UTF-8');

textdomain($domain);

define('DS', DIRECTORY_SEPARATOR);

if(!defined('CLI')){
    require dirname(__FILE__).DS.'paths.php';

    require path('sys').DS.'autoloader.php';
    spl_autoload_register(array('load', 'init'));
// Load Configuration File
    Config::load();

    Microtime::microtime_start();
    date_default_timezone_set(Config::$application['timezone']);
    foreach(Config::$application['autoload'] as $k=>$v){
        if($k == 'helper'){
            load::helper($v);
        }
        if($k == 'library'){
            load::library($v);
        }
    }

    //security::hostInit();

    if(Config::$application['is_client'] === true){
        session_id(SHA1('alakarorahe'));
    }

    session::start(MD5('basicsafety2014'));
    $router = new Router();
    $router->pathRoute('/csvscan');
    $router->render();
    if(!is_null($view = View::instance())){
        $view->render();
    }

}