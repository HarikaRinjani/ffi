<?php
/**
 * (c)2013 Transformatika
 * User: ag <agung@transformatika.com>
 * Date: 6/13/13
 * Time: 3:51 PM
 *
 */
error_reporting(E_ALL);
//ini_set('display_errors','On');

define('DS', DIRECTORY_SEPARATOR);
require dirname(__FILE__).DS.'paths.php';

require path('sys').DS.'autoloader.php';
spl_autoload_register(array('load', 'init'));
// Load Configuration File
Config::load();
date_default_timezone_set(Config::$application['timezone']);
load::library('session');
$router = new Router();
$router->pathRoute('/samplingcron/index');
$router->render();
if(!is_null($view = View::instance())){
    $view->render();
}
