��    /      �  C           
        $  4   ,  &   a     �     �     �  	   �     �     �     �     �       "         1     R     W  "   v     �     �     �     �     �     
  &   %     L     ]     s     y     �     �     �     �     �     �     �               /     ;     H  0   T     �     �     �  %   �  �  �     �	     �	  5   �	  *   
     -
  "   D
     g
     m
     t
     x
     �
     �
     �
     �
     �
     �
  #   �
          -     9     R  "   h     �     �      �     �     �     	          &     ,     F     K     `     o     ~     �     �  	   �     �  
   �  (   �          &     ;  '   T     -   (                                 .      $                             
              #                )   !                                           /   '       ,   *   +          %              &      "                       	    Add Farmer Address Along with this, we delivered fresh milk as follows: Are you sure want to delete this data? Bookmark deleted Cannot sent your mail' Check in Check out Collection Point Configuration saved Contents Officer Cooperative Customer Data has been successfully deleted Data has been successfully saved Date Delivery Order already printed Delivery Order has been completed! Edit Farmer Failed deleting bookmark Failed deleting mail Failed installing package Failed saving your bookmarks Failed while deleting data Failed while saving data into database Farmer not found Goods Carrier Officer Group Mail deleted Mobile Phone Module not configured properly Name No record found Not valid ID Password do not match Please upload a package Successfully installed Successfully repaired Transaction Truck Number Weight (Kg) You dont have permission to delete this bookmark Your mail has been sent laboratory officer username not found your username is not active or banned Project-Id-Version: MCP 1.0
POT-Creation-Date: 2015-02-23 15:37+0700
PO-Revision-Date: 2015-02-23 22:36+0700
Last-Translator: Prastowo Agung Widodo <agung@transformatika.com>
Language-Team: Transformatika <agung@transformatika.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
X-Poedit-KeywordsList: _;gettext;gettext_noop
X-Poedit-Basepath: .
Language: id_ID
X-Poedit-SourceCharset: UTF-8
 Tambah Farmer Alamat Bersama ini kami kirimkan susu segar sebagai berikut: Apakah Anda yakin akan menghapus data ini? Bookmark telah dihapus Tidak dapat mengirimkan pesan anda Masuk Keluar TPK Konfigurasi telah disimpan Petugas Pengisian Koperasi Konsumen Data berhasil dihapus Data berhasil disimpan Tanggal Delivery Order sudah pernah dicetak Delivery Order sudah selesai Edit Farmer Gagal menghapus bookmark Gagal menghapus pesan Gagal melakukan installasi package Gagal menyimpan bookmarks Gagal menghapus data Gagal menyimpan data ke database Farmer tidak ditemukan Petugas Pembawa Barang Kelompok Pesan telah dihapus No HP Modul belum dikonfigurasi Nama Data tidak ditemukan ID tidak valid Password salah Mohon upload package file Berhasil diinstall Berhasil diperbaiki Transaksi No Polisi Truck Berat (Kg) Anda tidak berhak menghapus bookmark ini Pesan Anda telah dikirim Petugas Laboratorium username tidak ditemukan username Anda tidak aktif atau diblokir 