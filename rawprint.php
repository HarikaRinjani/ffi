<?php 
    //echo $_COOKIE['transaksi_id'];exit();
    //error_reporting(0);
    session_start();
    define('DS',DIRECTORY_SEPARATOR);
    include 'paths.php';
    include 'webclientprint/Source/WebClientPrint.php';
    use Neodynamic\SDK\Web\WebClientPrint;
    use Neodynamic\SDK\Web\Utils;
    use Neodynamic\SDK\Web\DefaultPrinter;
    use Neodynamic\SDK\Web\InstalledPrinter;
    use Neodynamic\SDK\Web\ClientPrintJob;

    // Process request
    // Generate ClientPrintJob? only if clientPrint param is in the query string
    $urlParts = parse_url($_SERVER['REQUEST_URI']);
    
    if (isset($urlParts['query'])){
        $rawQuery = $urlParts['query'];
        //var_dump($rawQuery);exit();
        if($rawQuery[WebClientPrint::CLIENT_PRINT_JOB]){
            parse_str($rawQuery, $qs);

            //$useDefaultPrinter = ($qs['useDefaultPrinter'] === 'checked');
            
            

            //Create ESC/POS commands for sample receipt
            $esc = '0x1B'; //ESC byte in hex notation
            $newLine = '0x0A'; //LF byte in hex notation

           	$stringnya = $qs['str'];
            $cpj = new ClientPrintJob();
           	if(!isset($qs['barcode'])){
           		$cmds = $esc."@";
           		$cmds .= str_replace("[newline]",$newLine,rawurldecode($stringnya));
                
           	}else{
           		//$template = file_get_contents('FFI-barcode.txt');
           		//$cmds = trim(str_replace('[BARCODE]',$qs['str'],$template));
                $cmds = "^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR2,2~SD20^JUS^LRN^CI0^XZ";
                $cmds .= "^XA";
                $cmds .= "^MMT";
                $cmds .= "^PW400";
                $cmds .= "^LL0160";
                $cmds .= "^LS0";
                $cmds .= "^BY1,3,83^FT66,94^B3N,N,,N,N";
                $cmds .= "^FD>".str_replace("'","",rawurldecode($qs['str']))."C^FS";
                $cmds .= "^FT148,146^A0N,28,28^FH\^FD".str_replace("'","",rawurldecode($qs['farmer_id']))."^FS";
                $cmds .= "^FT67,120^A0N,28,38^FH\^FD".str_replace("'","",rawurldecode($qs['str']))."^FS";
                $cmds .= "^PQ1,0,1,Y^XZ";
                //set ESC/POS commands to print...
                
           	}
            $cpj->printerCommands = $cmds;
            $cpj->formatHexValues = true;
            //Create a ClientPrintJob obj that will be processed at the client side by the WCPP
            
            
            //set client printer
            if(isset($qs['printerName']) && $qs['printerName'] != '' && $qs['printerName'] != 'null'){
                $cpj->clientPrinter = new InstalledPrinter(str_replace("'","",$qs['printerName']));
            }else{
            	$cpj->clientPrinter = new DefaultPrinter();
            }

            //Send ClientPrintJob back to the client
            ob_clean();
            echo $cpj->sendToClient();
            ob_end_flush(); 
            exit();
        }
    }
?>
