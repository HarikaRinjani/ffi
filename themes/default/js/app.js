if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function(searchElement, fromIndex) {
        var k;

        // 1. Let O be the result of calling ToObject passing
        //    the this value as the argument.
        if (this === null) {
            throw new TypeError('"this" is null or not defined');
        }

        var O = Object(this);
        // 2. Let lenValue be the result of calling the Get
        //    internal method of O with the argument "length".
        // 3. Let len be ToUint32(lenValue).
        var len = O.length >>> 0;
        // 4. If len is 0, return -1.
        if (len === 0) {
            return -1;
        }

        // 5. If argument fromIndex was passed let n be
        //    ToInteger(fromIndex); else let n be 0.
        var n = +fromIndex || 0;

        if (Math.abs(n) === Infinity) {
            n = 0;
        }

        // 6. If n >= len, return -1.
        if (n >= len) {
            return -1;
        }

        // 7. If n >= 0, then Let k be n.
        // 8. Else, n<0, Let k be len - abs(n).
        //    If k is less than 0, then let k be 0.
        k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

        // 9. Repeat, while k < len
        while (k < len) {
            var kValue;
            // a. Let Pk be ToString(k).
            //   This is implicit for LHS operands of the in operator
            // b. Let kPresent be the result of calling the
            //    HasProperty internal method of O with argument Pk.
            //   This step can be combined with c
            // c. If kPresent is true, then
            //    i.  Let elementK be the result of calling the Get
            //        internal method of O with the argument ToString(k).
            //   ii.  Let same be the result of applying the
            //        Strict Equality Comparison Algorithm to
            //        searchElement and elementK.
            //  iii.  If same is true, return k.
            if (k in O && O[k] === searchElement) {
                return k;
            }
            k++;
        }
        return -1;
    };
}
var RESULT = {
    "y":"Pass",
    "n":"Fail",
    "a":"N/A"
};
var YESNO = {
    "y":"Yes",
    "n":"No"
};
var ACTIVENOTACTIVE = {
    "y":"Active",
    "n":"Not Active"
};
var SAMPLINGSTATUS = {
    "p":"Pending",
    "y":"Taken"
};
var SAMPLINGSESI = {
    "p":"Morning",
    "s":"Afternoon"
};
var delay = (function(){
    var timer = 0;
    return function(callback, ms){
        clearTimeout (timer);
        timer = setTimeout(callback, ms);
    };
})();

$(document).ready(function(){

    var BASE_URL = $.cookie('BASE_URL');
    $('.toggleSidebar').on('click',function(){
        $(this).next('.navigation').toggleClass('show');
    });
    var url = document.location.href;
    $('.navigation').find('a').each(function(){
        var ini = $(this);
        var href = ini.attr('href');
        var hrefs = href.replace(BASE_URL,'');
        if (url.indexOf(hrefs) > 0) {
            ini.parent('li').addClass('active');
            var ulParent = ini.closest('ul');
            if(ulParent.hasClass('active')){

            }else{
                ulParent.addClass('active');
            }
            ulParent.parent('li').find('.navtoggle i').attr('class','ion-chevron-down');
        }
    });
    $('.window-header > a.close').on('click',function(){
        //alert('tes');
        $(this).closest('.window').fadeOut();
    });
    if($('.format-date').length > 0){
        $('.format-date').each(function(){
            var ini = $(this);
            var tgl = ini.text();
            //moment.locale('id');
            if(moment(tgl,'YYYY-MM-DD',true).isValid() || moment(tgl,'YYYY-MM-DD HH:mm:ss',true).isValid()){
                var formatTgl = moment(tgl).format('LL');
                ini.html(formatTgl);
            }

        });
    }
    if($('.format-datetime').length > 0){
        $('.format-datetime').each(function(){
            var ini = $(this);
            var tgl = ini.text();
            //moment.locale('id');
            if(moment(tgl,'YYYY-MM-DD HH:mm:ss',true).isValid()){
                var formatTgl = moment(tgl).format('LLL');
                ini.html(formatTgl);
            }
        });
    }
    if($('.format-time').length > 0){
        $('.format-time').each(function(){
            var ini = $(this);
            var tgl = ini.text();
            //moment.locale('id');
            if(moment(tgl,'YYYY-MM-DD HH:mm:ss',true).isValid() || moment(tgl,'HH:mm:ss',true).isValid() || moment(tgl,'HH:mm',true).isValid()){
                var formatTgl =moment(tgl).format('LT');
                ini.html(formatTgl);
            }
        });
    }
    if($('.format-times').length > 0){
        $('.format-times').each(function(){
            var ini = $(this);
            var tgl = ini.text();
            //moment.locale('id');
            if(moment(tgl,'YYYY-MM-DD HH:mm:ss',true).isValid() || moment(tgl,'HH:mm:ss',true).isValid()){
                var formatTgl =moment(tgl).format('LTS');
                ini.html(formatTgl);
            }
        });
    }
    if($('.modal').length > 0){
        var backdrop = '<div class="backdrop"></div>';
        $('.modal.drop').prepend(backdrop);
        $('.modal').find('a.close').on('click',function(){
            $(this).closest('.modal').removeClass('fadein');
        });
        $('*[data-toggle=modal]').on('click',function(){
            var target = $(this).attr('data-target');
            $(target).addClass('fadein');
        });
        $('*[data-dismiss=modal]').on('click',function(){
            $(this).closest('.modal').removeClass('fadein');
        });
        $('.modal').each(function(){
            if($(this).hasClass('bg-blur')){
                var curEl = $(this);
                var modalEl = curEl.html();
                var kelas = curEl.attr('class');
                var id = curEl.attr('id');
                var newmodal = '<div class="'+kelas+'" id="'+id+'">'+modalEl+'</div>';
                $('body').append(newmodal);
                curEl.remove();
            }
        });
        $('.modal.drop').on('show',function(){
            $('#transformatika-container').addClass('blur');
        });
        $('.modal.drop').on('hide',function(){
            $('#transformatika-container').removeClass('blur');
        });
        $('.modal.draggable').find('.modal-dialog').draggable();
        $('.modal.draggable-header').find('.modal-dialog').draggable({
            handle:'.modal-header'
        });
    }

    $('a[disabled=disabled]').on('click',function(e){
        e.preventDefault();
        return false;
    });
    $('button[disabled=disabled]').on('click',function(e){
        e.preventDefault();
        return false;
    });

    var elh = $('.sidebar').find('.title');
    elh.on('click',function(e){
        var el = $(this).parent('li');
        var ikon = el.find('.navtoggle i').attr('class');
        if(ikon == 'ion-chevron-right'){
            el.children('ul').addClass('active');
            el.find('.navtoggle i').attr('class','ion-chevron-down');
        }else{
            el.children('ul').removeClass('active');
            el.find('.navtoggle i').attr('class','ion-chevron-right');
        }
    });

    $('.sidebar').on('mouseover',function(){
        if($('.navtoggle').hasClass('active')){
            return false;
        }else{
            $('.navtoggle').addClass('active');
        }
    });
    $('.sidebar').on('mouseout',function(){
        if($('.navtoggle').hasClass('active')){
            $('.navtoggle').removeClass('active');
        }else{
            return false;
        }
    });

    regenerateSidebar();
    $('.sidebar').resizable({
        handles:{
            'e':'#resize-handler'
        },
        resize: function(event, ui){
            var w = ui.size.width;
            $.cookie('sidebar_width',w);
            regenerateSidebar('false');
        }
    });
    if($('.datepicker').length > 0){
        $(".datepicker").datepicker({
            showAnim:'drop',
            dateFormat:'dd/mm/yy'
        });
    }
    if($('.datetimepicker').length > 0){
        $(".datetimepicker").datetimepicker({
            showAnim:'drop',
            dateFormat:'dd/mm/yy',
            controlType: 'select'
        });
    }
    if($('.timepicker').length > 0){
        $(".timepicker").timepicker({
            controlType:'select'
        });
    }
    if($('.input-time').length > 0){
        $(".input-time").mask('99:99');
    }
    //if($('select.department').length > 0){
    //    var data = [];
    //    $.ajax({
    //        url:BASE_URL+'index.php?/department/loadAll',
    //        dataType:'json',
    //        success:function(res){
    //            data = res.records;
    //            $('select.department').each(function(){
    //                var opt = '';
    //                var value =  $(this).attr('data-value');
    //                for(var k in data){
    //                    opt += '<option value="'+data[k].department_id+'"';
    //                    if(typeof value !== 'undefined' && value !== ''){
    //                        if(value === data[k].department_id){
    //                            opt += ' selected="selected"';
    //                        }
    //                    }
    //                    opt += '">'+data[k].department_name+'</option>';
    //                }
    //                $(this).html(opt);
    //            });
    //        }
    //    });
    //}
    var DISTRIBUTIONSTATUS = {
        "n":"On Delivery",
        "y":"Completed",
        "r":"Rejected"
    };
    if($('.distribution-status').length > 0){
        $('.distribution-status').each(function(){
            var ini = $(this);
            var tex = ini.text();
            var stat = DISTRIBUTIONSTATUS[tex];
            var bg;
            if(tex === 'n'){
                bg = 'bg-gray fg-white';
            }else if(tex === 'y'){
                bg = 'bg-blue fg-white';
            }else{
                bg = 'bg-red fg-white'
            }
            ini.html("<span class=\""+bg+" padding-top-2 padding-bottom-3 padding-left-5 padding-right-5 rounded\">"+stat+"</span>");
        });
    }
    if($('select.tpk').length > 0){
        var data = [];
        $.ajax({
            url:BASE_URL+'index.php?/tpk/loadAll',
            dataType:'json',
            success:function(res){
                data = res.records;
                $('select.tpk').each(function(){
                    var opt = '';
                    var value =  $(this).attr('data-value');

                    for(var k in data){
                        opt += '<option value="'+data[k].tpk_id+'"';
                        if(typeof value !== 'undefined' && value !== ''){
                            if(value === data[k].tpk_id){
                                opt += ' selected="selected"';
                            }
                        }
                        opt += ' data-tpk_kode="'+data[k].tpk_kode+'"';
                        opt += ' data-tpk_koperasi="'+data[k].tpk_koperasi+'"';
                        opt += '">'+data[k].tpk_nama+'</option>';
                    }
                    $(this).html(opt);
                });
            }
        });
    }
    if($('select.koperasi').length > 0){
        var data = [];
        $.ajax({
            url:BASE_URL+'index.php?/koperasi/loadAll',
            dataType:'json',
            success:function(res){
                data = res.records;
                $('select.koperasi').each(function(){
                    var opt = '';
                    var value =  $(this).attr('data-value');
                    for(var k in data){
                        opt += '<option value="'+data[k].koperasi_id+'"';
                        if(typeof value !== 'undefined' && value !== ''){
                            if(value === data[k].koperasi_id){
                                opt += ' selected="selected"';
                            }
                        }
                        opt += '">'+data[k].koperasi_nama+'</option>';
                    }
                    $(this).html(opt);
                });
            }
        });
    }
    if($('select.kelompok').length > 0){
        var data = [];
        $.ajax({
            url:BASE_URL+'index.php?/kelompok/loadAll',
            dataType:'json',
            success:function(res){
                data = res.records;
                $('select.kelompok').each(function(){
                    var opt = '';
                    var value =  $(this).attr('data-value');
                    for(var k in data){
                        opt += '<option value="'+data[k].kelompok_id+'"';
                        if(typeof value !== 'undefined' && value !== ''){
                            if(value === data[k].kelompok_id){
                                opt += ' selected="selected"';
                            }
                        }
                        opt += ' data-kelompok_kode="'+data[k].kelompok_kode+'"';
                        opt += ' data-tpk_id="'+data[k].tpk_id+'"';
                        opt += '">'+data[k].kelompok_nama+'</option>';
                    }
                    $(this).html(opt);
                });
            }
        });
    }
    if($('select.kelompokharga').length > 0){
        var data = [];
        $.ajax({
            url:BASE_URL+'index.php?/kelompokharga/loadAll',
            dataType:'json',
            success:function(res){
                data = res.records;
                $('select.kelompokharga').each(function(){
                    var opt = '';
                    var value =  $(this).attr('data-value');
                    for(var k in data){
                        opt += '<option value="'+data[k].kelompokharga_id+'"';
                        if(typeof value !== 'undefined' && value !== ''){
                            if(value === data[k].kelompokharga_id){
                                opt += ' selected="selected"';
                            }
                        }
                        opt += ' data-kelompokharga_kode="'+data[k].kelompokharga_kode+'"';
                        opt += ' data-tpk_id="'+data[k].tpk_id+'"';
                        opt += ' data-kelompok_id="'+data[k].kelompok_id+'"';
                        opt += ' data-kelompokharga_hargadasar="'+data[k].kelompokharga_hargadasar+'"';
                        opt += '">'+data[k].kelompokharga_nama+'</option>';
                    }
                    $(this).html(opt);
                });
            }
        });
    }
    if($('select.combobox').length > 0){
        $('select.combobox').each(function(){
            var el = $(this);
            var model = el.attr('data-model');
            if(typeof model !== 'undefined' && model !== ''){
                var data = [];
                var keyword = '';

                var currentId = el.attr('id');
                if(typeof currentId === 'undefined'){
                    currentId = 'selectc'+uuid;
                    el.attr('id',currentId);
                }
                var text_field = el.attr('data-key');
                if(typeof text_field === 'undefined'){
                    text_field = model+'_nama';
                }
                var uuid = generate_id();
                el.parent().css({
                    position:'relative'
                });

                comboboxSearch(model,text_field,keyword,uuid,currentId);
            }
        });
    }

    function comboboxInitSelect(id,res,uuid,model,text_field,keyword){
        var el = $('#'+id);

        var padding = el.parent().css('padding-left');
        var name = el.attr('name');
        var value = el.attr('data-value');
        //var text_field = el.attr('data-key');


        var select = '';
        var preselect = '';
        if(res.length > 0){
            for(var k in res){

                if(res[k].id === value){
                    preselect = res[k].text;
                }
                select += '<a href="javascript:void(0);"  class="padding-bottom-5 comboselect'+uuid+'" data-id="'+res[k].id+'" style="display:block;">'+res[k].text+'</a>';
            }
        }else{
            select = '<div>No record found</div>';
        }
        if(typeof value !== 'undefined'){
            keyword = preselect;
        }
        var kelas = el.attr('class');
        var inputbaru = '<input type="text" class="'+kelas+'" id="comboinput'+uuid+'" value="'+keyword+'">';
        var hiddeninput = '<input type="hidden" name="'+name+'" id="combovalue'+uuid+'" value="'+value+'">';

        el.before(inputbaru);
        el.after(hiddeninput);
        var widthnya = $('#comboinput'+uuid).outerWidth();
        var selectarea = '<div class="border bg-white" id="comboresult'+uuid+'" style="width:'+widthnya+'px;padding:4px 8px;min-width:200px;height:auto;display:none;position:absolute;top:33px;left:'+padding+'px;z-index:99999;">';

        el.after(selectarea);
        el.remove();




        $('#comboresult'+uuid).html(select);
        $('#comboinput'+uuid).bind('focus',function(){
            $('#comboresult'+uuid).slideDown('fast');
        });
        $('#comboinput'+uuid).bind('blur',function(){
            $('#comboresult'+uuid).slideUp('fast');
        });
        $('.comboselect'+uuid).bind('click',function(){
            var id = $(this).attr('data-id');
            var text = $(this).text();
            //alert(text);
            $('#combovalue'+uuid).val(id);
            $('#comboinput'+uuid).val(text);
        });
        $('#comboinput'+uuid).bind('keyup',function(){
            var elemen = $(this);
            delay(function(){
                var kwd = elemen.val();
                comboboxSearch(model,text_field,kwd,uuid,id);
            },500);
        });
    }

    function comboboxSearch(model,text_field,keyword,uuid,id){
        $.ajax({
            url:BASE_URL+'index.php?/load/find',
            type:'post',
            data:{
                keyword:keyword,
                field:text_field,
                model:model
            },
            dataType:'json',
            success:function(res){
                comboboxInitSelect(id,res,uuid,model,text_field,keyword)
            }
        });
    }
    function comboboxClickSelect(uuid,id,text){
        $('#combovalue'+uuid).val(id);
        $('#comboinput'+uuid).val(text);
    }
    if($('.rating').length > 0){
        $('.rating').each(function(){
            var nilai = $(this).text();
            var nilaix = parseInt(nilai);
            var star = createRating(nilaix);
            $(this).html(star);
        });
    }
    if($('.image-upload').length > 0){
        $('.image-upload').each(function(){
            var frm = $(this).closest('form');
            var input_name = $(this).attr('name');
            var uuid = generate_id();
            var newElement = '<input type="hidden" name="'+input_name+'" id="input'+uuid+'">';
            var btn = 'click image to upload a photo';
            var resize = $(this).attr('data-resize');
            var curImage = $(this).attr('data-img');
            if(curImage === '' || typeof curImage === 'undefined'){
                curImage = 'images/photo.png';
            }
            var currentImage = urlencode(curImage);
            var imgPreview = '<img id="imgpreview'+uuid+'" class="img-responsive" align="center" style="margin:0 auto;cursor:pointer;" src="'+BASE_URL+'index.php?/load/file/'+currentImage+'" >';
            var enableresize = false;
            if(resize !== '' && typeof resize !== 'undefined'){
                enableresize = true;
            }
            var hiddeninputfile = '<input type="file" id="uploader'+uuid+'" style="width:1px;height:1px;visibility:hidden;position:fixed;">';
            var divnya = '<div class="text-center">'+imgPreview+'<p><small>'+btn+'</small></p></div>';
            $(this).after(divnya);
            frm.append(newElement);
            frm.after(hiddeninputfile);
            $(this).removeAttr('name');
            $(this).hide();
            $('#imgpreview'+uuid).on('click',function(){
                $('#uploader'+uuid).trigger('click');
            });
            $('#uploader'+uuid).uploader({
                resize:enableresize,
                onSuccess:function(res){
                    $('#input'+uuid).val(res);
                    $('#imgpreview'+uuid).attr('src',res);
                }
            });
        });
    }
    if($('.barcode').length > 0){
        $('.barcode').each(function(){
            var textnya = $(this).text();
            $(this).barcode(textnya,"code128")
        });
    }
    if($('.status').length > 0){
        $('.status').each(function(){
            var statuse = $(this).text();
            var setatus = STATUS[statuse];
            var bg;
            if(statuse == 'p'){
                bg = 'bg-yellow';
            }
            if(statuse == 'a'){
                bg = 'bg-green fg-white';
            }
            if(statuse == 'd'){
                bg = 'bg-red fg-white';
            }
            if(statuse == 'y'){
                bg = 'bg-blue fg-white';
            }
            if(statuse == 'n'){
                bg = 'bg-gray fg-white';
            }

            $(this).html('<span class="'+bg+' padding-top-2 padding-bottom-3 padding-left-5 padding-right-5 rounded">'+setatus+'</span>');
        });
    }
    var SYNCSTATUS = {
        'y':'Synced',
        'n':'Not Sync',
        'p':'Processing'
    };
    if($('.syncstatus').length > 0){
        $('.syncstatus').each(function(){
            var statuse = $(this).text();
            var setatus = SYNCSTATUS[statuse];
            var bg;
            if(statuse == 'p'){
                bg = 'bg-yellow';
            }
            if(statuse == 'a'){
                bg = 'bg-green fg-white';
            }
            if(statuse == 'd'){
                bg = 'bg-red fg-white';
            }
            if(statuse == 'y'){
                bg = 'bg-blue fg-white';
            }
            if(statuse == 'n'){
                bg = 'bg-gray fg-white';
            }

            $(this).html('<span class="'+bg+' padding-top-2 padding-bottom-3 padding-left-5 padding-right-5 rounded">'+setatus+'</span>');
        });
    }
    if($('.samplingstatus').length > 0){
        $('.samplingstatus').each(function(){
            var statuse = $(this).text();
            var setatus = SAMPLINGSTATUS[statuse];
            var bg;
            if(statuse == 'p'){
                bg = 'bg-yellow fg-white';
            }
            if(statuse == 'y'){
                bg = 'bg-blue fg-white';
            }

            $(this).html('<span class="'+bg+' padding-top-2 padding-bottom-3 padding-left-5 padding-right-5 rounded">'+setatus+'</span>');
        });
    }
     if($('.samplingsesi').length > 0){
        $('.samplingsesi').each(function(){
            var statuse = $(this).text();
            var setatus = SAMPLINGSESI[statuse];
            var bg;
            if(statuse == 'p'){
                bg = 'bg-green fg-white';
            }
            if(statuse == 's'){
                bg = 'bg-red fg-white';
            }

            $(this).html('<span class="'+bg+' padding-top-2 padding-bottom-3 padding-left-5 padding-right-5 rounded">'+setatus+'</span>');
        });
    }
    if($('span.result').length > 0){
        $('span.result').each(function(){
            var statuse = $(this).text();
            var setatus = RESULT[statuse];
            var bg;
            if(statuse == 'y'){
                bg = 'bg-green fg-white';
            }
            if(statuse == 'n'){
                bg = 'bg-yellow fg-white';
            }
            if(statuse == 'a'){
                bg = 'bg-gray fg-white';
            }

            $(this).html('<span class="'+bg+' padding-top-2 padding-bottom-3 padding-left-5 padding-right-5 rounded">'+setatus+'</span>');
        });
    }
    if($('div.result').length > 0){
        $('div.result').each(function(){
            var statuse = $(this).text();
            var setatus = RESULT[statuse];
            var bg;
            if(statuse == 'y'){
                bg = 'bg-green fg-white';
            }
            if(statuse == 'n'){
                bg = 'bg-yellow fg-white';
            }
            if(statuse == 'a'){
                bg = 'bg-gray fg-white';
            }

            $(this).html('<span class="'+bg+' padding-top-2 padding-bottom-3 padding-left-5 padding-right-5 rounded">'+setatus+'</span>');
        });
    }
    if($('select.result').length > 0){
        $('select.result').each(function(){
            var opt = '';
            var value = $(this).attr('data-value');
            for(var k in RESULT){
                opt += '<option value="'+k+'"';
                if(typeof value !== 'undefined' && value !== ''){
                    if(value === k){
                        opt += ' selected="selected"';
                    }
                }
                opt += '>'+RESULT[k]+'</option>';
            }
            $(this).html(opt);
        });
    }
    if($('select.status').length > 0){
        $('select.status').each(function(){
            var opt = '';
            var value = $(this).attr('data-value');
            for(var k in ACTIVENOTACTIVE){
                opt += '<option value="'+k+'"';
                if(typeof value !== 'undefined' && value !== ''){
                    if(value === k){
                        opt += ' selected="selected"';
                    }
                }
                opt += '>'+ACTIVENOTACTIVE[k]+'</option>';
            }
            $(this).html(opt);
        });
    }
    if($('input.status').length > 0){
        $('input.status').each(function(){
            var opt = '';
            var name = $(this).attr('name');
            var value = $(this).attr('data-value');
            var blok = false;
            if($(this).hasClass('input-block')){
                blok = true;
            }
            for(var k in ACTIVENOTACTIVE){
                if(blok === true){
                    opt += '<div class="">';
                }
                opt += '<input type="radio" name="'+name+'" value="'+k+'"';
                if(typeof value !== 'undefined' && value !== ''){
                    if(value === k){
                        opt += ' checked="checked"';
                    }
                }
                opt += '><label>'+ACTIVENOTACTIVE[k]+'</label> ';
                if(blok === true){
                    opt += '</div>';
                }
            }
            $(this).after(opt);
            $(this).remove();
        });
    }
    if($('select.yesno').length > 0){
        $('select.yesno').each(function(){
            var opt = '';
            var value = $(this).attr('data-value');
            for(var k in YESNO){
                opt += '<option value="'+k+'"';
                if(typeof value !== 'undefined' && value !== ''){
                    if(value === k){
                        opt += ' selected="selected"';
                    }
                }
                opt += '>'+YESNO[k]+'</option>';
            }
            $(this).html(opt);
        });
    }
    if($('select.month').length > 0){
        $('select.month').each(function(){
            var opt = '';
            var value = $(this).attr('data-value');
            var ARRAYBULAN = {1:"Januari",2:"Februari",3:"Maret",4:"April",5:"Mei",6:"Juni",7:"Juli",8:"Agustus",9:"September",10:"Oktober",11:"November",12:"Desember"};
            for(i=1;i<=12;i++){
                opt += '<option value="'+i+'"';
                if(typeof value !== 'undefined' && value !== ''){
                    if(parseInt(value) === i){
                        opt += ' selected="selected"';
                    }
                }
                opt += '>'+ARRAYBULAN[i]+'</option>';
            }
            $(this).html(opt);
        });
    }
    if($('select.year').length > 0){
        $('select.year').each(function(){
            var opt = '';
            var value = $(this).attr('data-value');
            var start_year = 2014;
            var current_year = new Date().getFullYear();
            for(i=start_year;i<=current_year;i++){
                opt += '<option value="'+i+'"';
                if(typeof value !== 'undefined' && value !== ''){
                    if(parseInt(value) === i){
                        opt += ' selected="selected"';
                    }
                }
                opt += '>'+i+'</option>';
            }
            $(this).html(opt);
        });
    }
    if($('input.number').length > 0){
        $('input.number').bind('keydown',function(evt){
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            //alert(charCode);
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                evt.preventDefault();
                return false;
            }
            return true;
        });
    }
    if($('input.numeric').length > 0){
        $('input.numeric').bind('keydown',function(evt){
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;

            if(charCode === 190 || charCode === 189){
                return true;
            }else{
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    evt.preventDefault();
                    return false;
                }else{
                    return true;
                }
            }
        });
    }
    if($('input.email').length > 0){
        $('input.email').bind('blur',function(){
            var emailnya = $(this).val();
            if(!validateEmail(emailnya)){
                $(this).focus();
                notification('Please input valid email address!');
            }else{
                return true;
            }
        });
    }
    if($('table.fixedheader').length > 0){
        $('table.fixedheader').each(function(){
            var el = $(this);
            var wd = [];
            var tr = el.find('tbody tr:first-child');
            var index = 0;
            tr.find('td').each(function(){
                 wd[index] = $(this).width();
                index++;
            });
            el.find('thead').css({
                display:'block'
            });
            el.find('tbody').css({
                display:'block',
                'overflow-x':'hidden',
                'overflow-y':'auto'
            });
            el.find('tbody').width(el.width());
            el.find('thead').width(el.width());
            var i = 0;
            tr.find('td').each(function(){
                $(this).width(wd[i]);
                i++;
            });
            var ii = 0;
            el.find('th').each(function(){
                $(this).width(wd[ii]);
                ii++;
            });
        });
    }
    if($('input.append').length > 0){
        $('input.append').parent().css('position','relative');
        var h = $('input.append').height();
        var top = (h/2);
        $('.append-text').css({
            position:'absolute',
            right:'25px',
            top:(top - 5),
            'font-weight':'bold',
            color:'#888'
        });
    }
    var hiddenbtn = '<button type="submit" style="width:1px;height:1px;position:fixed;visibility:hidden;"></button>';
    $('#keyword').before(hiddenbtn);

    if($('.ribbon').length > 0){
        var top = $('.ribbon').offset().top - parseFloat($('.ribbon').css('marginTop').replace(/auto/, 100));
        $('.content').scroll(function (event) {
            // what the y position of the scroll is
            //console.log(top);
            var y = $(this).scrollTop();
            //console.log(y);
            var sidebarWIdth = $('.sidebar').width();
            // whether that's below the form
            if (y >= top) {
                // if so, ad the fixed class
                $('.ribbon').addClass('fixed');
                $('.ribbon').css({
                    left:sidebarWIdth
                });
            } else {
                // otherwise remove it
                $('.ribbon').css('left','').removeClass('fixed');
            }
        });
    }

    if(notify.isSupported){
        notify.requestPermission();
    }

});
function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
function createRating(val,max){
    if(typeof val === 'undefined' || val === '' || isNaN(val)){
        val = 0;
    }
    if(typeof max === 'undefined'){
        max = 10;
    }
    var emptyStar = '<i class="ion-ios7-star-outline"></i>';
    var halfStar = '<i class="ion-ios7-star-half"></i>';
    var fullStar = '<i class="ion-ios7-star"></i>';

    var nilaiStar = (((val/max) * 100) / 100);
    var nilaiStars = nilaiStar * 5;
    var bulatin = roundHalf(nilaiStars);
    //console.log(val);
    var i;
    var html = '';
    for(i=1;i<=5;i++){
        if((i - .5)>bulatin){
            html += '<a href="javascript:void(0);">'+emptyStar+'</a>';
        }else{
            if((bulatin +.5) > i){
                html += '<a href="javascript:void(0);">'+fullStar+'</a>';
            }else{
                html += '<a href="javascript:void(0);">'+halfStar+'</a>';
            }

        }
    }
    return html;
}
function createTpkSelect(target){
    var data = [];
    var BASE_URL = $.cookie('BASE_URL');
    $.ajax({
        url:BASE_URL+'index.php?/tpk/loadAll',
        dataType:'json',
        success:function(res){
            data = res.records;
            $(target).each(function(){
                var opt = '';
                var value =  $(this).attr('data-value');
                for(var k in data){
                    opt += '<option value="'+data[k].tpk_id+'"';
                    if(typeof value !== 'undefined' && value !== ''){
                        if(value === data[k].tpk_id){
                            opt += ' selected="selected"';
                        }
                    }
                    opt += ' data-tpk_kode="'+data[k].tpk_kode+'"';
                    opt += '">'+data[k].tpk_nama+'</option>';
                }
                $(this).html(opt);
            });
        }
    });
}
function createKelompokSelect(target){
    var data = [];
    var BASE_URL = $.cookie('BASE_URL');
    $.ajax({
        url:BASE_URL+'index.php?/kelompok/loadAll',
        dataType:'json',
        success:function(res){
            data = res.records;
            $(target).each(function(){
                var opt = '';
                var value =  $(this).attr('data-value');
                for(var k in data){
                    opt += '<option value="'+data[k].kelompok_id+'"';
                    if(typeof value !== 'undefined' && value !== ''){
                        if(value === data[k].kelompok_id){
                            opt += ' selected="selected"';
                        }
                    }
                    opt += ' data-kelompok_kode="'+data[k].kelompok_kode+'"';
                    opt += ' data-tpk_id="'+data[k].tpk_id+'"';
                    opt += '">'+data[k].kelompok_nama+'</option>';
                }
                $(this).html(opt);
            });
        }
    });
}
function createResultSelect(target){
    $(target).each(function(){
        var opt = '';
        var value = $(this).attr('data-value');
        for(var k in RESULT){
            opt += '<option value="'+k+'"';
            if(typeof value !== 'undefined' && value !== ''){
                if(value === k){
                    opt += ' selected="selected"';
                }
            }
            opt += '>'+RESULT[k]+'</option>';
        }
        $(this).html(opt);
    });
}
function roundHalf(num) {
    if(num === 0){
        return 0;
    }
    num = Math.round(num*2)/2;
    return num;
}
function generate_id(){
    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
    s[8] = s[13] = s[18] = s[23] = "";

    var uuid = s.join("");
    return uuid;
}
function regenerateSidebar(animate){
    var w = $.cookie('sidebar_width');
    if(w === '' || typeof w === 'undefined'){
        w = 200;
    }
    //console.log(w);
    if(typeof animate === 'undefined' || animate === ''){
        $('.sidebar').animate({width:w},'fast');
        $('.content.padded').animate({left:w+"px"},'fast');
        $('.breadcrumb').animate({left:w+"px"},'fast');
        $('#resize-handler').animate({left:(w-5)+"px"},'fast');
    }else{
        $('.sidebar').width(w);
        $('.content.padded').css({left:w+"px"});
        $('.breadcrumb').css({left:w+"px"});
        $('#resize-handler').css({left:(w-5)+"px"});
    }
    
}
function openUrl(url){
    var BASE_URL = $.cookie('BASE_URL');
    window.location.href = BASE_URL+'index.php?/'+url;
}
function urlencode(str) {
    str = (str + '')
    .toString();
    return encodeURIComponent(str)
    .replace(/!/g, '%21')
    .replace(/'/g, '%27')
    .replace(/\(/g, '%28')
    .replace(/\)/g, '%29')
    .replace(/\*/g, '%2A')
    .replace(/%20/g, '+');
}
function validateNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
function generate_id(){
    var s = [];
    var hexDigits = "0123456789abcdef";
    for (var i = 0; i < 36; i++) {
        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
    }
    s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
    s[8] = s[13] = s[18] = s[23] = "";

    var uuid = s.join("");
    return uuid;
}

function notification(str){
    if(notify.isSupported && notify.permissionLevel() === 'granted'){
        notify.config({pageVisibility: false, autoClose: 5000});
        notify.createNotification("Notification", {body:str, icon: "themes/default/images/alert.png"});
        $('#chatAudio')[0].play();
    }else{
        var uuid = generate_id();
        var html = '<div id="notification'+uuid+'" class="notification window no-border" >';
        html += '<div class="window-header no-border" >';
        html += '<span class="pull-left padding-left-10"><strong>Notification</strong></span>';
        html += '<a href="javascript:void(0);" class="close fg-lightgray" onclick="$(this).closest(\'.window\').fadeOut();">&times;</a>';
        html += '</div>';
        html += '<div class="window-body">';
        html += str;
        html += '</div>';
        html += '</div>';
        $('body').append(html);
        $('#chatAudio')[0].play();
        setTimeout(function(){
            var el = $('#notification'+uuid);
            el.fadeOut('fast',function(){
                el.remove();
            });
        },5000);
    }


}
function notifikasi(str){
    notification(str);
}
+function ($) {
  'use strict';

  // TAB CLASS DEFINITION
  // ====================

  var Tab = function (element) {
    this.element = $(element)
  }

  Tab.VERSION = '3.3.0'

  Tab.TRANSITION_DURATION = 150

  Tab.prototype.show = function () {
    var $this    = this.element
    var $ul      = $this.closest('ul:not(.dropdown-menu)')
    var selector = $this.data('target')

    if (!selector) {
      selector = $this.attr('href')
      selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') // strip for ie7
    }

    if ($this.parent('li').hasClass('active')) return

    var $previous = $ul.find('.active:last a')
    var hideEvent = $.Event('hide.bs.tab', {
      relatedTarget: $this[0]
    })
    var showEvent = $.Event('show.bs.tab', {
      relatedTarget: $previous[0]
    })

    $previous.trigger(hideEvent)
    $this.trigger(showEvent)

    if (showEvent.isDefaultPrevented() || hideEvent.isDefaultPrevented()) return

    var $target = $(selector)

    this.activate($this.closest('li'), $ul)
    this.activate($target, $target.parent(), function () {
      $previous.trigger({
        type: 'hidden.bs.tab',
        relatedTarget: $this[0]
      })
      $this.trigger({
        type: 'shown.bs.tab',
        relatedTarget: $previous[0]
      })
    })
  }

  Tab.prototype.activate = function (element, container, callback) {
    var $active    = container.find('> .active')
    var transition = callback
      && $.support.transition
      && (($active.length && $active.hasClass('fade')) || !!container.find('> .fade').length)

    function next() {
      $active
        .removeClass('active')
        .find('> .dropdown-menu > .active')
          .removeClass('active')
        .end()
        .find('[data-toggle="tab"]')
          .attr('aria-expanded', false)

      element
        .addClass('active')
        .find('[data-toggle="tab"]')
          .attr('aria-expanded', true)

      if (transition) {
        element[0].offsetWidth // reflow for transition
        element.addClass('in')
      } else {
        element.removeClass('fade')
      }

      if (element.parent('.dropdown-menu')) {
        element
          .closest('li.dropdown')
            .addClass('active')
          .end()
          .find('[data-toggle="tab"]')
            .attr('aria-expanded', true)
      }

      callback && callback()
    }

    $active.length && transition ?
      $active
        .one('bsTransitionEnd', next)
        .emulateTransitionEnd(Tab.TRANSITION_DURATION) :
      next()

    $active.removeClass('in')
  }


  // TAB PLUGIN DEFINITION
  // =====================

  function Plugin(option) {
    return this.each(function () {
      var $this = $(this)
      var data  = $this.data('bs.tab')

      if (!data) $this.data('bs.tab', (data = new Tab(this)))
      if (typeof option == 'string') data[option]()
    })
  }

  var old = $.fn.tab

  $.fn.tab             = Plugin
  $.fn.tab.Constructor = Tab


  // TAB NO CONFLICT
  // ===============

  $.fn.tab.noConflict = function () {
    $.fn.tab = old
    return this
  }


  // TAB DATA-API
  // ============

  var clickHandler = function (e) {
    e.preventDefault()
    Plugin.call($(this), 'show')
  }

  $(document)
    .on('click.bs.tab.data-api', '[data-toggle="tab"]', clickHandler)
    .on('click.bs.tab.data-api', '[data-toggle="pill"]', clickHandler)

}(jQuery);
$(window).load(function(){
    $('#loading').hide();
});
