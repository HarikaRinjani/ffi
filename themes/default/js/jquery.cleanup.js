
/*

Copyright (C) 2012 Jody Salt, http://jodysalt.com

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

(function($){
  
  function hook_method(name, fn){
    var old_method = $.fn[name];
    $.fn[name] = function(){
      fn.apply(this, arguments);
      return old_method.apply(this, arguments);
    };
  }
  
  
  function recursive_clean_up(){
    if(!this._cleaned_up){
      this._cleaned_up = true;
      $(this).trigger('cleanup');
      $(this).children().each(recursive_clean_up);
    }
  }
  
  hook_method('html', function(){
    if(arguments.length == 1){
      $(this).each(function(){
        $(this).children().each(recursive_clean_up);
      });
    }
  });
  
  hook_method('text', function(){
    if(arguments.length == 1){
      $(this).each(function(){
        $(this).children().each(recursive_clean_up);
      });
    }
  });
  
  hook_method('empty', function(){
    $(this).each(function(){
      $(this).children().each(recursive_clean_up);
    });
  });
  
  hook_method('remove', function(selector){
    if(selector){
      $(this).filter(selector).each(recursive_clean_up);
    } else {
      $(this).each(recursive_clean_up);
    }
  });
  
  hook_method('unwrap', function(){
    $(this).each(function(){
      var parent = $(this).parent()[0];
      if(!parent._cleaned_up){
        parent._cleaned_up = true;
        $(parent).trigger('cleanup');
      }
    });
  });
  
  hook_method('replaceAll', function(selector){
    $(selector).each(recursive_clean_up);
  });
  
  
  hook_method('replaceWith', function(){
    $(this).each(recursive_clean_up);
  });
  
})(jQuery);

