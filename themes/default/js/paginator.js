/**
 * Created by transformatika on 12/12/2014.
 */

$.fn.transpaginator = function (options) {
    //setting default value
    var element = this;
    var defaults = {
            onSuccess: function (callback) { },
            currentPage : 1,
            totalPages : 1,
            totalData:-1,
            pageUrl :'',
            firstBtn: 'First',
            lastBtn:'Last',
            prevBtn:'Prev',
            nextBtn: 'Next',
            maxShow:5
        },
        settings = $.extend({}, defaults, options);
    var _nextBtn = false;
    var _prevBtn = false;
    var _firstBtn = true;
    var _lastBtn = true;
    if(settings.currentPage === 1 && settings.totalPages > 1){
        _nextBtn = true;
    }
    if(settings.currentPage === settings.totalPages && settings.totalPages > 1){
        _nextBtn = false;
        _prevBtn = true;
    }
    if(settings.currentPage > 1 && settings.currentPage < settings.totalPages && settings.totalPages > 1){
        _nextBtn = true;
        _prevBtn = true;
    }

    var paging = '';
    if(_firstBtn == true){
        paging += '<a class="first" href="'+settings.pageUrl+'1"></a>';
    }else{
        paging += '<a class="first" href="javascript:void(0);"></a>';
    }
    if(_prevBtn == true){
        paging += '<a class="prev" href="'+settings.pageUrl+(settings.currentPage - 1)+'"></a>';
    }else{
        paging += '<a class="prev" href="javascript:void(0);"></a>';
    }


    //for(i=1;i<=settings.totalPages;i++){
    //    paging += '<a';
    //    if(i === settings.currentPage){
    //        paging += ' class="active"';
    //    }
    //    paging += ' href="'+settings.pageUrl+i+'">'+i+'</a>';
    //}
    paging += '<span class="separator"></span>';
    paging += '<span class="info"> page <input type="text" class="_pagingPage" value="'+settings.currentPage+'" style="min-width:2px;width:32px;text-align:center;"> of '+settings.totalPages+'</span>';
    paging += '<span class="separator"></span>';
    if(_nextBtn == true){
        paging += '<a class="next" href="'+settings.pageUrl+(settings.currentPage + 1)+'"></a>';
    }else{
        paging += '<a class="next" href="javascript:void(0);"></a>';
    }
    if(_lastBtn == true){
        paging += '<a class="last" href="'+settings.pageUrl+settings.totalPages+'"></a>';
    }else{
        paging += '<a class="last" href="javascript:void(0);"></a>';
    }
    paging += '<span class="separator"></span>';
    paging += '<a href="javascript:void(0);" class="refresh _pageReload"></a>';
    if(settings.totalData >= 0){
        paging += '<span class="pull-right padding-top-3 padding-right-15">Total Data: '+settings.totalData+'</span>';
    }
    element.html(paging);
    element.find('._pagingPage').on('keydown',function(evt){
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if(charCode === 13){
            window.location.href = settings.pageUrl+$(this).val();
        }else{
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }else{
                return true;
            }
        }
    });
    element.find('._pageReload').on('click',function(){
        location.reload();
    });
}