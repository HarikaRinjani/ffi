<?php
//
$__chaced = true;
$_GET['v'] = !isset($_GET['v']) ? '.min' : $_GET['v'];
$files = array(
    'normalize.css',
    'jquery-ui.css',
    'jquery-ui-timepicker-addon.min.css',
    'font.css',
    'main.css',
    'input.css',
    'grid.css',
    'ionicons.css',
    'sheet.css',
    'select.css',
    'tile.css',
    'spinner.css',
    'responsive.css'
);
$filename = '';
$filePath = 'mcp'.$_GET['v'].'.css';
if(!file_exists($filePath) || $__chaced === false){
    $handle = fopen($filePath,'w');
    $compressedCss = '';
    foreach ($files as $cssFile) {
        $compressedCss .= file_get_contents($cssFile);
    }
    $compressedCss = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '', $compressedCss);// Remove comments
    $compressedCss = str_replace(': ', ':', $compressedCss);// Remove space after colons
    $compressedCss = str_replace(array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), ' ', $compressedCss);// Remove whitespace
    fwrite($handle,$compressedCss);
}else{
    $compressedCss = file_get_contents($filePath);
}

header('Content-type: text/css');
echo $compressedCss;
