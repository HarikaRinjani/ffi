<?php
if ( strpos(strtolower($_SERVER['SCRIPT_NAME']),strtolower(basename(__FILE__))) ){ die(header('HTTP/1.0 403 Forbidden')); }
define('BASE_DIR',dirname(__FILE__));

//$dirSystem = realpath(BASE_DIR . DS.'..');

$GLOBALS ['sys'] = realpath(BASE_DIR.DS.'..'.DS.'..').DS.'tmvc'.DS.'system';

//$GLOBALS ['sys'] = BASE_DIR.DS.'tmvc'; // MVC Directory
$GLOBALS ['app'] = BASE_DIR . DS . 'application';
$GLOBALS ['views'] = BASE_DIR . DS . 'application' . DS . 'views';
$GLOBALS ['base'] = BASE_DIR;
$base_url = ((isset ( $_SERVER ['HTTPS'] ) && $_SERVER ['HTTPS'] == "on") ? "https" : "http");
$base_url .= "://" . $_SERVER ['HTTP_HOST'];
$base_url .= str_replace ( basename ( $_SERVER ['SCRIPT_NAME'] ), "", $_SERVER ['SCRIPT_NAME'] );

define ( 'BASE_URL', $base_url );

function path($path) {
    return $GLOBALS [$path];
}
