<?php
/**
 * (c)2013 Transformatika
 * User: ag <agung@transformatika.com>
 * Date: 6/13/13
 * Time: 3:51 PM
 *
 */
#error_reporting(E_ALL);
#ini_set('display_errors','On');

include dirname(__FILE__).DIRECTORY_SEPARATOR.'locale.php';
$___language = $locale;
putenv("LANG=" . $___language);
setlocale(LC_ALL, $___language, $___language.'.UTF8',$___language.'.UTF-8');

$domain = "messages";
bindtextdomain($domain, "Locale");
//bind_textdomain_codeset($domain, 'UTF-8');

textdomain($domain);

define('DS', DIRECTORY_SEPARATOR);

if(!defined('CLI')){
    require dirname(__FILE__).DS.'paths.php';

    require path('sys').DS.'autoloader.php';
    spl_autoload_register(array('load', 'init'));
// Load Configuration File
    Config::load();

    Microtime::microtime_start();
    date_default_timezone_set(Config::$application['timezone']);
    foreach(Config::$application['autoload'] as $k=>$v){
        if($k == 'helper'){
            load::helper($v);
        }
        if($k == 'library'){
            load::library($v);
        }
    }

    security::hostInit();

    if(Config::$application['is_client'] === true){
        session_id(SHA1('alakarorahe'));
        $_COOKIE['PHPSESSID'] = SHA1('alakarorahe');
    }

    session::start(MD5('basicsafety2014'));

    load::library('db');
    if(!db::connect(false)){
        header('location:setup/');
        exit();
    }

    // load::transCheckLisence();
    if(Config::$application['is_client'] === true){
        load::library('client');
        client::check();
    }

    $router = new Router();
    if(isset($_SERVER['QUERY_STRING'])){
        $router->pathRoute( $_SERVER['QUERY_STRING'] );
    }else{
        $router->defaultRoute();
    }
    $router->render();
    if(!is_null($view = View::instance())){
        $view->render();
    }

}
