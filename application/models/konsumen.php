<?php
/**
 * Created by PhpStorm.
 * User: PrastowoaGung
 * Date: 12/30/2014
 * Time: 9:41 PM
 */
class konsumen extends Model{
    public $table = 't_konsumen';
    public $primary_key = 'konsumen_id';
    public $status_field = 'konsumen_status';
    function relations(){
        return array(
            'koperasi'=>array(
                'ONE_TO_ONE',
                'konsumen_koperasi',
                'koperasi_id'
            )
        );
    }
}
