<?php
class milkpriceformula extends Model{
	public $table = 't_milkpriceformula';
	public $primary_key = 'milkpriceformula_id';
	public $status_field = 'milkpriceformula_status';
	function relations(){
		return array(
			'koperasi'=>array(
				'ONE_TO_ONE',
				'milkpriceformula_koperasi',
				'koperasi_id'
				),
			'milkpricecondition'=>array(
				'ONE_TO_MANY',
				'milkpriceformula_id',
				'milkpricecondition_milkpriceformula'
				)
			);
	}
}