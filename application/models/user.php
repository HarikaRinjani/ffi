<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 12/8/2014
 * Time: 9:54 AM
 */
class user extends Model{
    public $table = 't_user';
    public $primary_key = 'user_id';
    public $status_field = 'user_status';
    function relations(){
    	return array(
    		'usertype'=>array(
    			'ONE_TO_ONE',
    			'user_group',
    			'usertype_id'
    			),
            'koperasi'=>array(
                'ONE_TO_ONE',
                'user_koperasi',
                'koperasi_id'
                )
    		);
    }
}
