<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 12/12/2014
 * Time: 8:09 PM
 */
class inbox extends Model{
    public $table = 't_inbox';
    public $primary_key = 'inbox_id';
    public $status_field = 'inbox_status';
    function relations(){
        return array(
            'user'=>array(
                'ONE_TO_ONE',
                'inbox_sender',
                'user_id'
            )
        );
    }
}