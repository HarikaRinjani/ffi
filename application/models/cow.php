<?php

class cow extends Model{
	public $table = 't_cow';
	public $primary_key = 'cow_id';
	public $status_field = 'cow_status';

	function relations(){
		return array(
			'anggota'=>array(
				'ONE_TO_ONE',
				'cow_anggota',
				'anggota_id'
				),
			'koperasi'=>array(
				'ONE_TO_ONE',
				'cow_koperasi',
				'koperasi_id'
				),
			'tpk'=>array(
				'ONE_TO_ONE',
				'cow_tpk',
				'tpk_id'
				),
			'kelompok'=>array(
				'ONE_TO_ONE',
				'cow_kelompok',
				'kelompok_id'
				)
			);
	}
}