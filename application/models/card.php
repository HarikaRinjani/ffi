<?php
class card extends Model{
	public $table = 't_card';
	public $primary_key = 'card_id';
	public $status_field = 'card_status';

	function relations(){
		return array(
			'anggota'=>array(
				'ONE_TO_ONE',
				'card_anggota',
				'anggota_id'
				),
			'koperasi'=>array(
				'ONE_TO_ONE',
				'card_koperasi',
				'koperasi_id'
				),
			'tpk'=>array(
				'ONE_TO_ONE',
				'card_tpk',
				'tpk_id'
				),
			'kelompok'=>array(
				'ONE_TO_ONE',
				'card_kelompok',
				'kelompok_id'
				),
			'user'=>array(
				'ONE_TO_ONE',
				'card_entryby',
				'user_id'
				)
			);
	}

	function generateNumber(){
		while(1){
			$nomor = $this->generateNum();
			if($this->checkNumber($nomor) === true){
				return $nomor;
			}
		}
	}
	function checkNumber($nomor=''){
		$card = load::model('card');
		$cek = $card->setCondition('card_number = \''.$nomor.'\'')
					->read();
		if($cek['total_data'] === 0){
			return true;
		}else{
			return false;
		}
	}
	protected function generateNum(){
		$time = microtime();
		$timearray = explode(" ",$time);
		$time1 = str_replace(array('.',','),'',$timearray[0]);
		$time2 = $timearray[1];
		$random1 = mt_rand(1111,9999);
		$random2 = mt_rand(1111,9999);
		$jumlahin = ((int) $time1 + (int) $time2 -  $random1 + $random2);
		return (int) $jumlahin;
	}
}