<?php

class distribution extends Model{
	public $table = 't_distribution';
	public $primary_key = 'distribution_id';
	public $status_field = 'distribution_status';
	function relations(){
		return array(
			'user'=>array(
				'ONE_TO_ONE',
				'distribution_entryby',
				'user_id'
			),
			'truck'=>array(
				'ONE_TO_ONE',
				'distribution_truck',
				'truck_id'
			),
			'konsumen'=>array(
				'ONE_TO_ONE',
				'distribution_konsumen',
				'konsumen_id'
				),
			'koperasi'=>array(
				'ONE_TO_ONE',
				'distribution_koperasi',
				'koperasi_id'
				)
		);
	}
}