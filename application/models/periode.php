<?php
class periode extends Model{
	public $table = 't_periode';
	public $primary_key = 'periode_id';
	public $status_field = 'periode_status';
	function relations(){
		return array(
			'koperasi'=>array(
				'ONE_TO_ONE',
				'periode_koperasi',
				'koperasi_id'
				),
			'periodedetail'=>array(
				'ONE_TO_MANY',
				'periode_id',
				'periodedetail_periode'
				)
			);
	}
}