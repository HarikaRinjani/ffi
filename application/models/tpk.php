<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 12/24/2014
 * Time: 6:52 AM
 */
class tpk extends Model{
    public $table = 't_tpk';
    public $primary_key = 'tpk_id';
    public $status_field = 'tpk_status';
    function relations(){
        return array(
            'koperasi'=>array(
                'ONE_TO_ONE',
                'tpk_koperasi',
                'koperasi_id'
            )
        );
    }
}