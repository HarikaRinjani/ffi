<?php 

class OauthClients extends Model
{
	public $table = 'oauth_clients';
    public $primary_key = 'client_id';

    public function relations()
    {
    	return array(
            'koperasi'=>array(
                'ONE_TO_ONE',
                'client_koperasi',
                'koperasi_id'
            ),
            'tpk'=>array(
                'ONE_TO_ONE',
                'client_tpk',
                'tpk_id'
            )
        );
    }
}