<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 12/16/2014
 * Time: 10:45 AM
 */
class bookmark extends Model{
    public $table = 't_bookmark';
    public $primary_key = 'bookmark_id';
    public $status_field = 'bookmark_status';
}