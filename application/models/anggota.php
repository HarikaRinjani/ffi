<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 12/28/2014
 * Time: 12:43 PM
 */
class anggota extends Model{
    public $table = 't_anggota';
    public $primary_key = 'anggota_id';
    public $status_field = 'anggota_status';
    function relations(){
        return array(
            'kelompok'=>array(
                'ONE_TO_ONE',
                'anggota_kelompok',
                'kelompok_id'
            ),
            'tpk'=>array(
                'ONE_TO_ONE',
                'anggota_tpk',
                'tpk_id'
            ),
            'koperasi'=>array(
                'ONE_TO_ONE',
                'anggota_koperasi',
                'koperasi_id'
            ),
            'card'=>array(
                'ONE_TO_ONE',
                'anggota_id',
                'card_anggota'
                ),
            'cow'=>array(
                'ONE_TO_ONE',
                'anggota_id',
                'cow_anggota'
                )
        );
    }
}