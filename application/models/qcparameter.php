<?php
class qcparameter extends Model{
	public $table = 't_qcparameter';
	public $primary_key = 'qcparameter_id';
	public $status_field = 'qcparameter_status';

	function relations(){
		return array(
			'transaksi'=>array(
				'ONE_TO_ONE',
				'qcparameter_transaksi',
				'transaksi_id'
				)
			);

	}
}