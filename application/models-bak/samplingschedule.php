<?php

/**
 * Sampling Schedule Models
 */
class samplingschedule extends Model{
	public $table = 't_samplingschedule';
	public $primary_key = 'samplingschedule_id';
	public $status_field = 'samplingschedule_status';
	function relations(){
		return array(
			'anggota'=>array(
				'ONE_TO_ONE',
				'samplingschedule_anggota',
				'anggota_id'
				),
			'koperasi'=>array(
				'ONE_TO_ONE',
				'samplingschedule_koperasi',
				'koperasi_id'
				),
			'tpk' => array(
				'ONE_TO_ONE',
				'samplingschedule_tpk',
				'tpk_id'
			)
			);
	}

	/**
	 * Get Current Periode
	 * @param string $koperasi_id [description]
	 * @param string $date        [description]
	 */
	function getCurrentPeriode($koperasi_id='',$date=''){
		if($koperasi_id ==''){
			$koperasi_id = session::get('user_koperasi');
		}
		if($date == ''){
			$date = date('Y-m-d');
		}
		$periode = load::model('periode');
		$periode->setCondition('periode_effectivedate <= \''.date('Y-m-d').'\' AND periode_status = \'y\' AND periode_koperasi = \''.$koperasi_id.'\'')
				->setOrderBy('periode_effectivedate')
				->setOrderType('DESC')
				->setLimit(1);

		$dataperiode = $periode->read();
		foreach($dataperiode['records'][0]['periodedetail'] as $k=>$v){
			$sekarang = date('j',strtotime($date));
			if($sekarang >= $v['periodedetail_startdate'] && $sekarang <= $v['periodedetail_enddate']){
				$currentPeriode = $v['periodedetail_name'];
			}
		}
		return $currentPeriode;
	}
	function getTotalPeriode($koperasi_id='',$date=''){
		if($koperasi_id ==''){
			$koperasi_id = session::get('user_koperasi');
		}
		if($date == ''){
			$date = date('Y-m-d');
		}
		$periode = load::model('periode');
		$periode->setCondition('periode_effectivedate <= \''.date('Y-m-d').'\' AND periode_status = \'y\' AND periode_koperasi = \''.$koperasi_id.'\'')
				->setOrderBy('periode_effectivedate')
				->setOrderType('DESC')
				->setLimit(1);

		$dataperiode = $periode->read();
		if($dataperiode['total_data'] === 0){
			$interval = 10;
		}else{
			$interval = $dataperiode['records'][0]['periode_interval'];
		}
		$total_hari =  cal_days_in_month(CAL_GREGORIAN, date('n',strtotime($date)), date('Y',strtotime($date)));
		$total_periode = ceil($total_hari / $interval);
		return $total_periode;
	}

	function getDataPeriode($koperasi_id='',$date=''){
		if($koperasi_id ==''){
			$koperasi_id = session::get('user_koperasi');
		}
		if($date == ''){
			$date = date('Y-m-d');
		}
		$periode = load::model('periode');
		$periode->setCondition('periode_effectivedate <= \''.date('Y-m-d').'\' AND periode_status = \'y\' AND periode_koperasi = \''.$koperasi_id.'\'')
				->setOrderBy('periode_effectivedate')
				->setOrderType('DESC')
				->setLimit(1);

		$dataperiode = $periode->read();
		$total_periode = 0;
		foreach($dataperiode['records'][0]['periodedetail'] as $k=>$v){
			$sekarang = date('j',strtotime($date));
			$total_periode++;
			if($sekarang >= $v['periodedetail_startdate'] && $sekarang <= $v['periodedetail_enddate']){
				$currentPeriode = $v['periodedetail_name'];
				$res['periode_startdate'] = $v['periodedetail_startdate'];
				$res['periode_enddate'] = $v['periodedetail_enddate'];
			}
		}

		$res['total_periode'] = $total_periode;
		$res['current_periode'] = $currentPeriode;

		return $res;
	}
}
