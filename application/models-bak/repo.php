<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 12/28/2014
 * Time: 12:43 PM
 */
class repo extends Model{
    public $table = 't_repo';
    public $primary_key = 'repo_id';

    public function updateRepo($type, $id='', $koperasiId='', $tpkId='')
    {
        $anggota = load::model('anggota');
        $user = load::model('user');
        $koperasi = load::model('koperasi');
        load::library('security');
        switch($type){
            case 'anggota':
                // UPDATE REPO
                $anggota->resetValue();
                if (empty($id)) {
                    if (!empty($koperasiId) && !empty($tpkId)) {
                         $anggota->setCondition('anggota_koperasi = \''.$koperasiId.'\' AND anggota_tpk = \''.$tpkId.'\'');
                    }
                   
                    $anggota->setLimit(0);
                } else {
                    $anggota->setCondition('anggota_id = \''.$id.'\'');
                    $anggota->setLimit(1);
                }
                $dataAnggotanya = $anggota->read();

                $repoDataContent = array();
                foreach ($dataAnggotanya['records'] as $k => $v) {
                    $repoDataContent['Id'] = $v['anggota_id'];
                    $repoDataContent['KoperasiId'] = $v['anggota_koperasi'];
                    $repoDataContent['Koperasi'] = array(
                        'Id' => $v['koperasi_id'],
                        'Nama' => $v['koperasi_nama'],
                        'Kode' => $v['koperasi_kode']
                    );
                    $repoDataContent['TpkId'] = $v['anggota_tpk'];
                    $repoDataContent['Tpk'] = array(
                        'Id' => $v['tpk_id'],
                        'Nama' => $v['tpk_nama'],
                        'Kode' => $v['tpk_kode']
                    );
                    $repoDataContent['Nama'] = $v['anggota_nama'];
                    $repoDataContent['KelompokId'] = $v['anggota_kelompok'];
                    $repoDataContent['Kelompok'] = array(
                        'Id' => $v['kelompok_id'],
                        'Nama' => $v['kelompok_nama'],
                        'Kode' => $v['kelompok_kode'],
                    );
                    $repoDataContent['Nomor'] = $v['anggota_nomor'];
                    $repoDataContent['NoHp'] = $v['anggota_nohp'];
                    $repoDataContent['Alamat'] = $v['anggota_alamat'];
                    $repoDataContent['Status'] = $v['anggota_status'];
                    $repoDataContent['Photo'] = $v['anggota_photo'];
                    $repoDataContent['CardNumber'] = $v['card_number'];
                    $repoDataContent['Card'] = array(
                        'Id' => $v['card_id'],
                        'Number' => $v['card_number']
                    );
                    $repoDataContent['Cow'] = array(
                        'Id' => $v['cow_id'],
                        'Population' => $v['cow_population'],
                        'Antibiotic' => $v['cow_antibiotic'],
                        'Lactating' => $v['cow_lactating'],
                        'AntibioticStart' => $v['cow_antibioticstartdate'],
                        'AntibioticEnd' => $v['cow_antibioticenddate'],
                        'Remark' => $v['cow_antibioticremark']
                    );
                    $this->resetValue();
                    $this->setCondition('repo_dataid = \''.$v['anggota_id'].'\' AND repo_type = \'anggota\' AND repo_koperasi = \''.$koperasiId.'\' AND repo_tpk = \''.$tpkId.'\'');
                    $this->setLimit(1);
                    $cekRepo = $this->read();
                    $repoData['repo_type'] = 'anggota';
                    $repoData['repo_name'] = 'main';
                    $repoData['repo_data'] = json_encode($repoDataContent);
                    $repoData['repo_dataid'] = $v['anggota_id'];
                    $repoData['repo_koperasi'] = $koperasiId;
                    $repoData['repo_tpk'] = $tpkId;
                    $repoData['updated_at'] = date('Y-m-d H:i:s');
                    if ($cekRepo['total_data'] > 0) {
                        $this->setId($cekRepo['records'][0]['repo_id']);
                        $this->setDataArray($repoData);
                        $this->update();
                    } else {
                        $repoData['repo_id'] = fn::generate_id(true);
                        $repoData['created_at'] = $repoData['updated_at'];
                        $this->setDataArray($repoData);
                        $this->insert();
                    }
                }

                break;
            case 'user':
                if (empty($id)) {
                    $dtUsernya = $user->setLimit(0)->read();
                } else {
                    $user->setCondition('user_id = \''.$id.'\'')->setLimit(1)->read();
                }

                $repoDataKoperasi = array();
                foreach ($dtUsernya['records'] as $k => $v) {
                    $this->resetValue();
                    $this->setCondition('repo_type = \'user\' AND repo_koperasi = \''.$v['user_koperasi'].'\' AND repo_dataid = \''.$v['user_id'].'\'');
                    $this->setLimit(1);
                    $cekRepo = $this->read();

                    $repoData['repo_type'] = 'user';
                    $repoData['repo_name'] = 'main';
                    $repoData['repo_data'] = json_encode(array(
                        'Id' => $v['user_id'],
                        'Name' => $v['user_name'],
                        'FullName' => $v['user_fullname'],
                        'Email' => $v['user_email'],
                        'SuperUser' => $v['user_su'],
                        'Group' => $v['user_group'],
                        'Avatar' => $v['user_avatar'],
                        'Status' => $v['user_status'],
                        'KoperasiId' => $v['user_koperasi'],
                        'Koperasi' => array(
                            'Id' => $v['koperasi_id'],
                            'Nama' => $v['koperasi_nama'],
                            'Kode' => $v['koperasi_kode']
                        ),
                        'Password' => MD5(security::decrypt($v['user_password']))
                    ));
                    $repoData['repo_koperasi'] = $v['user_koperasi'];
                    $repoData['repo_dataid'] = $v['user_id'];
                    $repoData['repo_tpk'] = '';
                    $repoData['updated_at'] = date('Y-m-d H:i:s');
                    if ($cekRepo['total_data'] > 0) {
                        $this->setId($cekRepo['records'][0]['repo_id']);
                        $this->setDataArray($repoData);
                        $this->update();
                    } else {
                        $repoData['repo_id'] = fn::generate_id(true);
                        $repoData['created_at'] = $repoData['updated_at'];
                        $this->setDataArray($repoData);
                        $this->insert();
                    }
                }

                break;
            case 'koperasi':
                $tpk = load::model('tpk');
                if (empty($id)) {
                    $dtKoperasi = $koperasi->setLimit(0)->read();
                    $dtTpk = $tpk->setLimit(0)->read();
                } else {
                    $dtKoperasi = $koperasi->setCondition('koperasi_id = \''.$id.'\'')->setLimit(1)->read();
                    $dtTpk = $tpk->setCondition('tpk_koperasi = \''.$id.'\'')->setLimit(0)->read();
                }

                foreach ($dtKoperasi['records'] as $k => $v) {
                    $koperasiData = array();
                    $koperasiData['Id'] = $v['koperasi_id'];
                    $koperasiData['Nama'] = $v['koperasi_nama'];
                    $koperasiData['Alamat'] = $v['koperasi_alamat'];
                    $koperasiData['Kode'] = $v['koperasi_kode'];
                    $koperasiData['Status'] = $v['koperasi_status'];
                    $koperasiData['Logo'] = $v['koperasi_logo'];
                    $koperasiData['Tpk'] = array();
                    foreach ($dtTpk['records'] as $key => $val) {
                        if ($val['tpk_koperasi'] === $v['koperasi_id']) {
                            $koperasiData['Tpk'][] = array(
                                'Id' => $val['tpk_id'],
                                'Nama' => $val['tpk_nama'],
                                'Kode' => $val['tpk_kode'],
                                'Status' => $val['tpk_status']
                            );
                        }
                    }
                    $this->resetValue();
                    $this->setCondition('repo_type = \'koperasi\' AND repo_dataid = \''.$v['koperasi_id'].'\'');
                    $this->setLimit(1);
                    $cekRepo = $this->read();

                    $repoData['repo_type'] = 'koperasi';
                    $repoData['repo_name'] = 'main';
                    $repoData['repo_data'] = json_encode($koperasiData);
                    $repoData['repo_koperasi'] = '';
                    $repoData['repo_dataid'] = $v['koperasi_id'];
                    $repoData['repo_tpk'] = '';
                    $repoData['updated_at'] = date('Y-m-d H:i:s');
                    if ($cekRepo['total_data'] > 0) {
                        $this->setId($cekRepo['records'][0]['repo_id']);
                        $this->setDataArray($repoData);
                        $this->update();
                    } else {
                        $repoData['repo_id'] = fn::generate_id(true);
                        $repoData['created_at'] = $repoData['updated_at'];
                        $this->setDataArray($repoData);
                        $this->insert();
                    }
                }

                break;
            case 'periode':
                $periode = load::model('periode');
                 if (empty($id)) {
                    $dtPeriode = $periode->setLimit(0)->read();
                   
                } else {
                    $dtPeriode = $periode->setCondition('periode_id = \''.$id.'\'')->setLimit(1)->read();
                }
                foreach ($dtPeriode['records'] as $k => $v) {
                    $periodeData = array();
                    $periodeData['Id'] = $v['periode_id'];
                    $periodeData['KoperasiId'] = $v['periode_koperasi'];
                    $periodeData['EffectiveDate'] = $v['periode_effectivedate'];
                    $periodeData['Name'] = $v['periode_name'];
                    $periodeData['Status'] = $v['periode_status'];
                    $periodeDetail = array();
                    foreach ($v['periodedetail'] as $key => $val) {
                        $periodeDetail[$key]['Id'] = $val['periodedetail_id'];
                        $periodeDetail[$key]['Name'] = $val['periodedetail_name'];
                        $periodeDetail[$key]['StartDate'] = $val['periodedetail_startdate'];
                        $periodeDetail[$key]['EndDate'] = $val['periodedetail_enddate'];
                        $periodeDetail[$key]['PeriodeId'] = $val['periodedetail_periode'];
                    }
                    $periodeData['PeriodeDetail'] = $periodeDetail;

                    $this->resetValue();
                    $this->setCondition('repo_type = \'periode\' AND repo_dataid = \''.$v['periode_id'].'\'');
                    $this->setLimit(1);
                    $cekRepo = $this->read();

                    $repoData['repo_type'] = 'periode';
                    $repoData['repo_name'] = 'main';
                    $repoData['repo_data'] = json_encode($periodeData);
                    $repoData['repo_koperasi'] = '';
                    $repoData['repo_dataid'] = $v['periode_id'];
                    $repoData['repo_tpk'] = '';
                    $repoData['updated_at'] = date('Y-m-d H:i:s');
                    if ($cekRepo['total_data'] > 0) {
                        $this->setId($cekRepo['records'][0]['repo_id']);
                        $this->setDataArray($repoData);
                        $this->update();
                    } else {
                        $repoData['repo_id'] = fn::generate_id(true);
                        $repoData['created_at'] = $repoData['updated_at'];
                        $this->setDataArray($repoData);
                        $this->insert();
                    }
                }
                break;
        }
    }
}
