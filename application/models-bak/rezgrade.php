<?php 
class rezgrade extends Model{
	public $table = 't_rezgrade';
	public $primary_key = 'rezgrade_id';
	public $status_field = 'rezgrade_status';
	function relations(){
		return array(
			'koperasi'=>array(
				'ONE_TO_ONE',
				'rezgrade_koperasi',
				'koperasi_id'
				),
			'rezgradedetail'=>array(
				'ONE_TO_MANY',
				'rezgrade_id',
				'rezgradedetail_rezgrade'
				)
			);
	}
}