<?php

class update extends Model{
    public $table = 't_update';
    public $primary_key = 'update_id';
    public $status_field = 'update_status';
}
