<?php
class distributionprintlog extends Model{
	public $table = 't_distributionprintlog';
	public $primary_key = 'printlog_id';
	public $status_field = 'printlog_status';
	function relations(){
		return array(
			'distribution'=>array(
				'ONE_TO_ONE',
				'printlog_distribution',
				'distribution_id'
				),
			'user'=>array(
				'ONE_TO_ONE',
				'printlog_entryby',
				'user_id'
				)
			);
	}
}