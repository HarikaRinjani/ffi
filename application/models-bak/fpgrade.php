<?php
class fpgrade extends Model{
	public $table = 't_fpgrade';
	public $primary_key = 'fpgrade_id';
	public $status_field ='fpgrade_status';
	function relations(){
		return array(
			'koperasi'=>array('ONE_TO_ONE','fpgrade_koperasi','koperasi_id'),
			'fpgradedetail'=>array('ONE_TO_MANY','fpgrade_id','fpgradedetail_fpgrade')
		);
	}
}