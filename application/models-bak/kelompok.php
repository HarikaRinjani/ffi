<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 12/24/2014
 * Time: 7:40 AM
 */
class kelompok extends Model{
    public $table = 't_kelompok';
    public $primary_key = 'kelompok_id';
    public $status_field = 'kelompok_status';
    function relations(){
        return array(
            'koperasi'=>array(
                'ONE_TO_ONE',
                'kelompok_koperasi',
                'koperasi_id'
            ),
            'tpk'=>array(
                'ONE_TO_ONE',
                'kelompok_tpk',
                'tpk_id'
            )
        );
    }
}