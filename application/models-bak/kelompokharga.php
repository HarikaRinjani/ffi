<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 12/27/2014
 * Time: 4:56 PM
 */
class kelompokharga extends Model{
    public $table = 't_kelompokharga';
    public $primary_key = 'kelompokharga_id';
    public $status_field = 'kelompokharga_status';
    function relations(){
        return array(
            't_kelompok'=>array(
                'LEFT JOIN',
                'kelompokharga_kelompok',
                'kelompok_id'
            ),
            't_tpk'=>array(
                'LEFT JOIN',
                'kelompokharga_tpk',
                'tpk_id'
            ),
            't_koperasi'=>array(
                'LEFT JOIN',
                'kelompokharga_koperasi',
                'koperasi_id'
            )
        );
    }
}