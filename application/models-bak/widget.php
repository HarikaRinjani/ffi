<?php
/**
 * Created by PhpStorm.
 * User: ag
 * Date: 5/12/14
 * Time: 2:57 PM
 */
class widget extends Model{
    public $table = 't_widget';
    public $primary_key = 'widget_id';
    public $permanentDelete = true;
    function getList(){
        $this->setCondition('widget_status = \'y\' AND widget_user_id = \''.session::get('user_id').'\'');
        $this->order_by = 'widget_urutan';
        $data = $this->read();
        $res = array();
        foreach($data['records'] as $k=>$v){
            $res[$k] = $v;
            $detail = $this->detail($v['widget_type'],$v['widget_display_type']);
            $res[$k]['detail'] = $detail['records'];
        }
        return $res;
    }
    function detail($widget_type='',$display_type=''){
        if(!empty($widget_type)){
            switch($widget_type){
                case 'kebijakan':
                    $dt = load::model('kebijakan');
                    if($display_type == 'table'){
                        $dt->setCondition('kebijakan_status = \'y\'');
                        $dt->setLimit(5);
                        return $dt->read();
                    }else{
                        $dt->setCondition('kebijakan_status = \'y\'');
                        $dt->setChartType('column');
                        $dt->setChartTitle('Total Kebijakan');
                        $dt->setChartSubtitle('Tahun '.date('Y'));
                        $dt->setChartHeight('200px');
                        $dt->setChartPeriode('monthRange');
                        $dt->setChartPeriodeField('entry_date');
                        $dt->setChartFields(array(
                            array(
                                'kebijakan_name',
                                'Total'
                            )
                        ));
                        $dt->setChartContainer('widget_kebijakan_chart');
                        $data['records'] = $dt->generateChart();
                        return $data;
                    }
                    break;
                case 'inbox':
                    $dt = load::model('inbox');
                    if($display_type == 'table'){
                        $dt->setCondition('inbox_user = \''.session::get('user_id').'\' AND inbox_status != \'d\'');
                        $dt->setLimit(5);
                        return $dt->read();
                    }else{
                        $dt->setCondition('inbox_user = \''.session::get('user_id').'\' AND inbox_status != \'d\'');
                        $dt->setChartType('column');
                        $dt->setChartTitle('Total Mail');
                        $dt->setChartSubtitle('Tahun '.date('Y'));
                        $dt->setChartHeight('200px');
                        $dt->setChartPeriode('monthRange');
                        $dt->setChartPeriodeField('inbox_date');
                        $dt->setChartFields(array(
                            array(
                                'inbox_title',
                                'Total'
                            )
                        ));
                        $dt->setChartContainer('widget_inbox_chart');
                        $data['records'] = $dt->generateChart();
                        return $data;
                    }
                    break;
                case 'identifikasi':
                    $dt = load::model('identifikasi');
                    $dt->setCondition('identifikasi_status != \'d\' AND date_part(\'year\',identifikasi_tgl) = \''.date('Y').'\'');
                    if($display_type == 'table'){
                        $dt->setLimit(5);
                        $dt->setOrderBy('identifikasi_no');
                        return $dt->read();
                    }else{
                        $dt->setChartType('column');
                        $dt->setChartTitle('Total Identifikasi');
                        $dt->setChartSubtitle('Tahun '.date('Y'));
                        $dt->setChartHeight('200px');
                        $dt->setChartPeriode('monthRange');
                        $dt->setChartPeriodeField('entry_date');
                        $dt->setChartFields(array(
                            array(
                                'aspek',
                                'Total'
                            )
                        ));
                        $dt->setChartContainer('widget_identifikasi_chart');
                        $data['records'] = $dt->generateChart();
                        return $data;
                    }
                    break;
                case 'program':

                    break;
                case 'contact':
                    $dt = load::model('contact');
                    $dt->setCondition('contact_status != \'d\' AND date_part(\'year\',contact_date) = \''.date('Y').'\'');
                    if($display_type == 'table'){
                        $dt->setLimit(5);
                        $dt->setOrderBy('contact_date');
                        return $dt->read();
                    }else{
                        $dt->setChartType('column');
                        $dt->setChartTitle(_('Total Laporan Potensi Bahaya'));
                        $dt->setChartSubtitle(_('Tahun').' '.date('Y'));
                        $dt->setChartHeight('200px');
                        $dt->setChartPeriode('monthRange');
                        $dt->setChartPeriodeField('contact_date');
                        $dt->setChartFields(array(
                            array(
                                'contact_name',
                                'Total'
                            )
                        ));
                        $dt->setChartContainer('widget_contact_chart');
                        $data['records'] = $dt->generateChart();
                        return $data;
                    }
                    break;
            }
        }
    }
}