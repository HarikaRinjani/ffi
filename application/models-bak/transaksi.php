<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 12/29/2014
 * Time: 1:59 PM
 */
class transaksi extends Model{
    public $table = 't_transaksi';
    public $primary_key = 'transaksi_id';
    public $status_field = 'transaksi_status';
    function relations(){
        return array(
            'koperasi'=>array(
                'ONE_TO_ONE',
                'transaksi_koperasi',
                'koperasi_id'
            ),
            'tpk'=>array(
                'ONE_TO_ONE',
                'transaksi_tpk',
                'tpk_id'
            ),
            'kelompok'=>array(
                'ONE_TO_ONE',
                'transaksi_kelompok',
                'kelompok_id'
            ),
            // 't_kelompokharga'=>array(
            //     'LEFT JOIN',
            //     'transaksi_kelompokharga',
            //     'kelompokharga_id'
            // ),
            'anggota'=>array(
                'ONE_TO_ONE',
                'transaksi_anggota',
                'anggota_id'
            )
        );
    }
    function getId($tpk_id=''){
        //echo $tpk_id;exit();
        if(!empty($tpk_id)){
            $sql = 'SELECT transaksi_uniqid FROM '.$this->table.' WHERE transaksi_tpk = \''.$tpk_id.'\' AND date_part(\'year\', transaksi_waktu) = '.date('Y').' ORDER BY transaksi_uniqid DESC LIMIT 1';
            $query = db::query($sql);
            if(db::numRows($query) === 0){
                return 1;
            }else{
                $dt = db::fetchAssoc($query);
                //print_r($dt);exit();
                return ((int)$dt['transaksi_uniqid'] + 1);
            }
        }

    }

}