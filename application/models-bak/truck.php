<?php
class truck extends Model{
	public $table = 't_truck';
	public $primary_key = 'truck_id';
	public $status_field = 'truck_status';
	function relations(){
		return array(
			'koperasi'=>array(
				'ONE_TO_ONE',
				'truck_koperasi',
				'koperasi_id'
			)
		);
	}
}