<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('settings/settings.menu.php');?>

<div class="content padded">
    <div class="breadcrumb">
        <ul class="tab">
            <!-- <li >
                <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-add-department">add new</a>
            </li> -->
            <li class="active">
                <a href="<?php echo BASE_URL;?>index.php?/koperasi/setting" disabled="disabled">Resazurine Grade</a>
            </li>
        </ul>
        <div class="pull-right">
            <ul class="breadcrumb-nav">
                <li>
                    <a href="">Apps Settings</a>
                </li>
                <li>
                    <a href="">Resazurine Grade</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row padding-bottom-40">
            <div class="ribbon padding-left-15 padding-right-15 padding-bottom-10 padding-top-10 border-bottom text-right">
                <div class="menu">
                    <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-add-department"><i class="ion-ios7-plus-empty"></i>add new</a>
                   <!--  <a href="javascript:void(0);" id="__edit" class="fg-lightgray editbtn"><i class="ion-ios7-compose-outline"></i>edit</a> -->
                   <!--  <a href="javascript:void(0);" id="__delete" class="fg-lightgray editbtn"><i class="ion-ios7-trash-outline"></i>delete</a> -->
                </div>
                <form action="javascript:void(0);" id="searchForm" onsubmit="return filter();">
                    <?php if(!in_array(session::get('user_group'),Config::$application['super_group'])){ ?>
                    <input type="hidden" name="koperasi" id="koperasi" value="<?php echo session::get('user_koperasi');?>">
                    <?php }else{ ?>
                    <select name="koperasi" id="koperasi" onchange="return filter();">
                        <option value="">All</option>
                        <?php foreach($koperasi['records'] as $k=>$v){ ?>
                            <option value="<?php echo $v['koperasi_id'];?>" <?php if($v['koperasi_id'] === $_GET['koperasi']){ echo 'selected="selected"'; } ?>><?php echo $v['koperasi_nama'];?></option>
                        <?php } ?>
                    </select>
                    <?php } ?>
                </form>
            </div>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th style="width:24px;">#</th>
                    <th><?php echo _('Cooperative');?></th>
                    <th>Grade</th>
                    <th><?php echo _('Effective Date');?></th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                <?php if($grade['total_data'] == 0){ ?>
                    <tr>
                        <td colspan="6"><?php echo _('No record found');?></td>
                    </tr>
                <?php }else{ ?>
                    <?php foreach($grade['records'] as $k=>$v){ ?>
                        <tr class="tr_selector cursor-pointer" data-id="<?php echo $v['rezgrade_id'];?>" id="tr<?php echo $v['rezgrade_id'];?>">
                            <td style="vertical-align:top;">
                                <?php
                                if($grade['page'] == 1){
                                    echo ($k+1);
                                }else{
                                    echo ($k + 1 + ($grade['limit'] * ($grade['page'] - 1)));
                                }

                                if($v['koperasi_logo'] == '' || $v['koperasi_logo'] == null){
                                    $logo = 'index.php?/load/file/'.urlencode('images/photo.png');
                                }else{
                                    $logo = $v['koperasi_logo'];
                                }
                                ?>
                            </td>
                            <td style="vertical-align:top;min-width:200px;">
                                <img src="<?php echo $logo;?>" alt="" class="pull-left" style="margin-right:5px;width:32px;height:32px;vertical-align:middle">
                                <?php echo $v['koperasi_nama'];?>
                            </td>
                            <td style="vertical-align:top;">
                                <?php 
                                foreach($v['rezgradedetail'] as $key=>$val){
                                    echo '<div class="padding-bottom-10 border-bottom dotted">'.$val['rezgradedetail_grade'].' - ';
                                    echo 'TPC: '.$val['rezgradedetail_tpcstart'].' - '.$val['rezgradedetail_tpcend'];
                                    echo ' &middot; add price: '.number_format($val['rezgradedetail_price']);
                                    echo '</div>';
                                }
                                ?>
                            </td>
                            <td class="format-date" style="vertical-align:top;"><?php echo $v['rezgrade_effectivedate'];?></td>
                            <td style="vertical-align:top;"><span class="status" style="vertical-align:top;"><?php echo $v['rezgrade_status'];?></span></td>
                        </tr>
                    <?php } ?>
                <?php } ?>
                </tbody>
            </table>
            <div class="grid-pagging border-top margin-bottom-30"></div>
        </div>
    </div>
</div>
<div class="modal draggable-header" id="modal-add-department">
    <div class="modal-dialog">
        <div class="modal-header">
            <h3 class="modal-title">Add Grade</h3>
            <a href="javascript:void(0);" class="close">&times;</a>
        </div>
        <div class="modal-body no-padding">
            <form action="<?php echo BASE_URL;?>index.php?/rezgrade/save" method="post" enctype="multipart/form-data">
                <div class="tabs-line">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-addgrade1" data-toggle="tab">Information</a></li>
                        <li><a href="#tab-addgrade2" data-toggle="tab">Detail</a></li>
                    </ul>
                </div>            
                <div class="tab-content border-top padding-15">
                     <div id="tab-addgrade1" class="tab-pane tab-modal fade in active">
                        <div class="form-group">
                            <label for="add-department_name" class="col-md-4"><?php echo _('Cooperative');?></label>
                            <div class="col-md-8">
                                <select id="add-department_name" class="input-block koperasi" name="rezgrade_koperasi"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-rezgrade_effectivedate" class="col-md-4">Effective Date</label>
                            <div class="col-md-8">
                                <input type="text" class="input-block datepicker" name="rezgrade_effectivedate" required />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4" for="add-rezgrade_status">Status</label>
                            <div class="col-md-8">
                                <select name="rezgrade_status" data-value="y" id="add-rezgrade_status" class="input-block status"></select>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-addgrade2">
                        <table class="table border">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th style="width:60px;">Grade</th>
                                <th>TPC Start</th>
                                <th>TPC End</th>
                                <th>Add Price</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php for($i=1;$i<=10;$i++){ ?>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="rezgradedetail_check[<?php echo $i;?>]" value="<?php echo $i;?>"> <label for=""></label>
                                    </td>
                                    <td>
                                        <input type="text" class="input-block text-center number" value="<?php echo $i;?>" name="rezgradedetail_grade[<?php echo $i;?>]">
                                    </td>
                                    <td>
                                        <input type="text" class="input-block text-right numeric" name="rezgradedetail_tpcstart[<?php echo $i;?>]">
                                    </td>
                                    <td>
                                        <input type="text" class="input-block text-right numeric" name="rezgradedetail_tpcend[<?php echo $i;?>]">
                                    </td>
                                    <td>
                                        <input type="text" class="input-block text-right numeric" name="rezgradedetail_price[<?php echo $i;?>]">
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="padding-top-25 padding-bottom-15 padding-right-15 text-right">
                    <button type="button" data-dismiss="modal">close</button>
                    <button type="submit" class="default">save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal draggable-header" id="modal-edit-department">
    <div class="modal-dialog">
        <div class="modal-header">
            <h3 class="modal-title">Add Grade</h3>
            <a href="javascript:void(0);" class="close">&times;</a>
        </div>
        <div class="modal-body no-padding">
            <form action="<?php echo BASE_URL;?>index.php?/rezgrade/save" method="post" enctype="multipart/form-data">
                <input type="hidden" name="rezgrade_id" id="edit-rezgrade_id">
                <div class="tabs-line">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-editgrade1" data-toggle="tab">Information</a></li>
                        <li><a href="#tab-editgrade2" data-toggle="tab">Detail</a></li>
                    </ul>
                </div>            
                <div class="tab-content border-top padding-15">
                     <div id="tab-editgrade1" class="tab-pane tab-modal fade in active">
                        <div class="form-group">
                            <label for="edit-rezgrade_koperasi" class="col-md-4"><?php echo _('Cooperative');?></label>
                            <div class="col-md-8">
                                <select id="edit-rezgrade_koperasi" class="input-block koperasi" name="rezgrade_koperasi"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="edit-rezgrade_effectivedate" class="col-md-4">Effective Date</label>
                            <div class="col-md-8">
                                <input type="text" class="datepicker input-block" name="rezgrade_effectivedate" id="edit-rezgrade_effectivedate" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4" for="edit-rezgrade_status">Status</label>
                            <div class="col-md-8">
                                <select name="rezgrade_status" data-value="y" id="edit-rezgrade_status" class="input-block status"></select>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-editgrade2">
                        <table class="table border">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th style="width:60px;">Grade</th>
                                <th>TPC Start</th>
                                <th>TPC End</th>
                                <th>Add Price</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php for($i=1;$i<=10;$i++){ ?>
                                <tr>
                                    <td>
                                        <input type="checkbox" class="rezgradecheckbox" id="edit-rezgradedetail_check<?php echo $i;?>" name="rezgradedetail_check[<?php echo $i;?>]" value="<?php echo $i;?>"> <label for=""></label>
                                    </td>
                                    <td>
                                        <input type="text" id="edit-rezgradedetail_grade<?php echo $i;?>" class="input-block text-center number" value="<?php echo $i;?>" name="rezgradedetail_grade[<?php echo $i;?>]">
                                    </td>
                                    <td>
                                        <input type="text" id="edit-rezgradedetail_tpcstart<?php echo $i;?>" class="input-block text-right numeric" name="rezgradedetail_tpcstart[<?php echo $i;?>]">
                                    </td>
                                    <td>
                                        <input type="text" id="edit-rezgradedetail_tpcend<?php echo $i;?>" class="input-block text-right numeric" name="rezgradedetail_tpcend[<?php echo $i;?>]">
                                    </td>
                                    <td>
                                        <input type="text" id="edit-rezgradedetail_price<?php echo $i;?>" class="input-block text-right numeric" name="rezgradedetail_price[<?php echo $i;?>]">
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="padding-top-25 padding-right-15 padding-bottom-15 text-right">
                    <button type="button" data-dismiss="modal">close</button>
                    <button type="submit" class="default">save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){

        $('.grid-pagging').transpaginator({
            totalPages: <?php echo $grade['total_page'];?>,
            totalData: <?php echo $grade['total_data'];?>,
            currentPage: <?php echo $grade['page'];?>,
            pageUrl: '<?php echo BASE_URL;?>index.php?/rezgrade/index/&keyword=<?php echo $grade['keyword'];?>&page='
        });
        $('.tr_selector').on('click',function(){
            var id = $(this).attr('data-id');
            $('.editbtn').removeClass('fg-lightgray').removeAttr('disabled').attr('data-id',id);
            $('.tr_selector').removeClass('active');
            $(this).addClass('active');
        });
        $('#__edit').on('click',function(){
            var id = $(this).attr('data-id');
            if(typeof id !== 'undefined' && id !== ''){
                $.ajax({
                    url:'<?php echo BASE_URL;?>index.php?/rezgrade/edit/'+id,
                    dataType:'json',
                    success:function(res){
                        $('#edit-rezgrade_effectivedate').val(moment(res.rezgrade_effectivedate).format('DD/MM/YYYY'));
                        $('#edit-rezgrade_status').val(res.rezgrade_status);
                        $('#edit-rezgrade_koperasi').val(res.rezgrade_koperasi);
                        $('#edit-rezgrade_id').val(res.rezgrade_id);
                        var detail = res.rezgradedetail;
                        var i = 1;
                        $('.rezgradecheckbox').prop('checked',false);
                        for(var k in detail){
                            $('#edit-rezgradedetail_price'+i).val(detail[k].rezgradedetail_price);
                            $('#edit-rezgradedetail_tpcstart'+i).val(detail[k].rezgradedetail_tpcstart);
                            $('#edit-rezgradedetail_tpcend'+i).val(detail[k].rezgradedetail_tpcend);
                            $('#edit-rezgradedetail_grade'+i).val(detail[k].rezgradedetail_grade);
                            $('#edit-rezgradedetail_check'+i).prop('checked',true);
                            i++;
                        }
                        $('#modal-edit-department').addClass('fadein');
                    }
                });
            }
        });

        $('#__delete').on('click',function(){
            var id = $(this).attr('data-id');
            if(typeof id !== 'undefined' && id !== ''){
                if(confirm('<?php echo _('Are you sure want to delete this data?');?>')){
                    $.ajax({
                        url:'<?php echo BASE_URL;?>index.php?/rezgrade/delete',
                        type:'post',
                        data:{
                            rezgrade_id:id
                        },
                        success:function(){
                            location.reload();
                        }
                    });
                }
            }
        });

    });

    function filter(){
        var koperasi = $('#koperasi').val();
        window.location.href = '<?php echo BASE_URL;?>index.php?/rezgrade/index/&koperasi='+koperasi+'&page=1';
    }
</script>
<?php View::inc('footer.php');?>
