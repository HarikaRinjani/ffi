<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo session::get('title');?></title>
    <link rel="stylesheet" href="<?php echo BASE_URL;?>/themes/default/css/mcp0.3.css"/>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <script src="<?php echo BASE_URL;?>/themes/default/js/jquery-1.11.1.min.js"></script>
    <script src="<?php echo BASE_URL;?>/themes/default/js/jquery.cookie.js"></script>
    <script src="<?php echo BASE_URL;?>/themes/default/js/html5upload.js"></script>
    <script src="<?php echo BASE_URL;?>/themes/default/highcharts/js/highcharts.js"></script>
    <script src="<?php echo BASE_URL;?>/themes/default/highcharts/js/themes/metro.js"></script>
    <script src="<?php echo BASE_URL;?>/themes/default/highcharts/js/modules/exporting.src.js"></script>
    <script type="text/javascript">
        $.cookie('BASE_URL','<?php echo BASE_URL;?>');
        var STATUS = <?php echo json_encode(Config::$application['status']);?>;
    </script>
    <script type="text/javascript" src="<?php echo BASE_URL;?>themes/default/js/app.min.js"></script>
    <style>
        /* HIDE IKLAN TELKOM SPEEDY */
        #pushstat{
            display:none !important;
        }
		select{
            -webkit-appearance:none !important;
            -webkit-border-radius:0;
        }
    </style>
</head>
<body class="theme-<?php echo session::get('user_theme');?>">
<div id="loading">
    <span class="loading-1"></span>
    <span class="loading-2"></span>
    <span class="loading-3"></span>
    <span class="loading-4"></span>
    <span class="loading-5"></span>
    <span class="loading-6"></span>
</div>
<div id="transformatika-container">
