<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('settings/settings.menu.php');?>

<div class="content padded">
    <div class="breadcrumb">
        <ul class="tab">
            <!-- <li >
                <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-add-department">add new</a>
            </li> -->
            <li class="active">
                <a href="<?php echo BASE_URL;?>index.php?/koperasi/setting" disabled="disabled">Freezing Point Grade</a>
            </li>
        </ul>
        <div class="pull-right">
            <ul class="breadcrumb-nav">
                <li>
                    <a href="">Apps Settings</a>
                </li>
                <li>
                    <a href="">Freezing Point Grade</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row padding-bottom-40">
            <div class="ribbon text-right">
                <div class="menu">
                    <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-add-department"><i class="ion-ios7-plus-empty"></i>add new</a>
                    <!-- <a href="javascript:void(0);" id="__edit" class="fg-lightgray editbtn"><i class="ion-ios7-compose-outline"></i>edit</a> -->
                    <!-- <a href="javascript:void(0);" id="__delete" class="fg-lightgray editbtn"><i class="ion-ios7-trash-outline"></i>delete</a> -->
                </div>
                <form action="javascript:void(0);" id="searchForm" onsubmit="return filter();">
                    <?php if(!in_array(session::get('user_group'),Config::$application['super_group'])){ ?>
                    <input type="hidden" name="koperasi" id="koperasi" value="<?php echo session::get('user_koperasi');?>">
                    <?php }else{ ?>
                    <select name="koperasi" id="koperasi" onchange="return filter();">
                        <option value="">All</option>
                        <?php foreach($koperasi['records'] as $k=>$v){ ?>
                            <option value="<?php echo $v['koperasi_id'];?>" <?php if($v['koperasi_id'] === $_GET['koperasi']){ echo 'selected="selected"'; } ?>><?php echo $v['koperasi_nama'];?></option>
                        <?php } ?>
                    </select>
                    <?php } ?>
                </form>
            </div>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th style="width:24px;">#</th>
                    <th><?php echo _('Cooperative');?></th>
                    <th><?php echo _('Grade');?></th>
                    <th><?php echo _('Effective Date');?></th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                <?php if($grade['total_data'] == 0){ ?>
                    <tr>
                        <td colspan="6"><?php echo _('No record found');?></td>
                    </tr>
                <?php }else{ ?>
                    <?php foreach($grade['records'] as $k=>$v){ ?>
                        <tr class="tr_selector cursor-pointer" data-id="<?php echo $v['fpgrade_id'];?>" id="tr<?php echo $v['fpgrade_id'];?>">
                            <td style="vertical-align:top;">
                                <?php
                                if($grade['page'] == 1){
                                    echo ($k+1);
                                }else{
                                    echo ($k + 1 + ($grade['limit'] * ($grade['page'] - 1)));
                                }

                                if($v['koperasi_logo'] == '' || $v['koperasi_logo'] == null){
                                    $logo = 'index.php?/load/file/'.urlencode('images/photo.png');
                                }else{
                                    $logo = $v['koperasi_logo'];
                                }
                                ?>
                            </td>
                            <td style="vertical-align:top;min-width:200px;">
                                <img src="<?php echo $logo;?>" alt="" class="pull-left" style="margin-right:5px;width:32px;height:32px;vertical-align:middle">
                                <?php echo $v['koperasi_nama'];?>
                            </td>
                            <td style="vertical-align:top;">
                                <?php 
                                foreach($v['fpgradedetail'] as $key=>$val){
                                    echo '<div class="padding-bottom-10 border-bottom dotted">'.$val['fpgradedetail_grade'].'. ';
                                    echo 'FP: '.$val['fpgradedetail_fpstart'].' - '.$val['fpgradedetail_fpend'];
                                    echo ' &middot; Add Price: '.number_format($val['fpgradedetail_price']);
                                    echo '</div>';
                                }
                                ?>
                            </td style="vertical-align:top;">
                            <td class="format-date" style="vertical-align:top;"><?php echo $v['fpgrade_effectivedate'];?></td>
                            <td><span class="status" style="vertical-align:top;"><?php echo $v['fpgrade_status'];?></span></td>
                        </tr>
                    <?php } ?>
                <?php } ?>
                </tbody>
            </table>
            <div class="grid-pagging border-top margin-bottom-30"></div>
        </div>
    </div>
</div>
<div class="modal draggable-header" id="modal-add-department">
    <div class="modal-dialog">
        <div class="modal-header">
            <h3 class="modal-title">Add Grade</h3>
            <a href="javascript:void(0);" class="close">&times;</a>
        </div>
        <div class="modal-body no-padding">
            <form action="<?php echo BASE_URL;?>index.php?/fpgrade/save" method="post" enctype="multipart/form-data">
                <div class="tabs-line">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-addgrade1" data-toggle="tab">Information</a></li>
                        <li><a href="#tab-addgrade2" data-toggle="tab">Detail</a></li>
                    </ul>
                </div>            
                <div class="tab-content border-top padding-15">
                     <div id="tab-addgrade1" class="tab-pane tab-modal fade in active">
                        <div class="form-group">
                            <label for="add-department_name" class="col-md-4"><?php echo _('Cooperative');?></label>
                            <div class="col-md-8">
                                <select id="add-department_name" class="input-block koperasi" name="fpgrade_koperasi"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-fpgrade_effectivedate" class="col-md-4">Effective Date</label>
                            <div class="col-md-8">
                                <input type="text" class="datepicker input-block" name="fpgrade_effectivedate" required />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4" for="add-fpgrade_status">Status</label>
                            <div class="col-md-8">
                                <select name="fpgrade_status" data-value="y" id="add-fpgrade_status" class="input-block status"></select>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-addgrade2">
                        <table class="table border">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th style="width:60px;">Grade</th>
                                <th>FP Start</th>
                                <th>FP End</th>
                                <th>Add Price</th>
                            </tr>
                            </thead>
                            <tbody>
                                <?php for($i=1;$i<=10;$i++){ ?>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="fpgradedetail_check[<?php echo $i;?>]" value="<?php echo $i;?>"> <label for=""></label>
                                    </td>
                                    <td>
                                        <input type="text" class="input-block text-center number" value="<?php echo $i;?>" name="fpgradedetail_grade[<?php echo $i;?>]">
                                    </td>
                                    <td>
                                        <input type="text" class="input-block text-right numeric" name="fpgradedetail_fpstart[<?php echo $i;?>]">
                                    </td>
                                    <td>
                                        <input type="text" class="input-block text-right numeric" name="fpgradedetail_fpend[<?php echo $i;?>]">
                                    </td>
                                    <td>
                                        <input type="text" class="input-block text-right numeric" name="fpgradedetail_price[<?php echo $i;?>]">
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="padding-top-25 padding-right-15 padding-bottom-15 text-right">
                    <button type="button" data-dismiss="modal">close</button>
                    <button type="submit" class="default">save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal draggable" id="modal-edit-department">
    <div class="modal-dialog">
        <div class="modal-header">
            <h3 class="modal-title">Edit Grade</h3>
            <a href="javascript:void(0);" class="close">&times;</a>
        </div>
        <div class="modal-body">
            <form action="<?php echo BASE_URL;?>index.php?/fpgrade/save" method="post">
                <input type="hidden" name="fpgrade_id" id="edit-fpgrade_id">
                <div class="form-group">
                    <label for="edit-fpgrade_koperasi" class="col-md-4"><?php echo _('Cooperative');?></label>
                    <div class="col-md-8">
                        <select id="edit-fpgrade_koperasi" class="input-block koperasi" name="fpgrade_koperasi"></select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="edit-fpgrade_fpstart" class="col-md-4">FP Start Value</label>
                    <div class="col-md-8">
                        <input type="text" id="edit-fpgrade_fpstart" class="input-block numeric" name="fpgrade_fpstart" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit-fpgrade_fpend" class="col-md-4">FP End Value</label>
                    <div class="col-md-8">
                        <input type="text" id="edit-fpgrade_fpend" class="input-block numeric" name="fpgrade_fpend" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit-fpgrade_grade" class="col-md-4">Grade</label>
                    <div class="col-md-8">
                        <input type="text" id="edit-fpgrade_grade" class="input-block number" name="fpgrade_grade" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit-fpgrade_price" class="col-md-4">Price</label>
                    <div class="col-md-8">
                        <input type="text" id="edit-fpgrade_price" class="input-block numeric" name="fpgrade_price" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4" for="edit-fpgrade_status">Status</label>
                    <div class="col-md-8">
                        <select name="fpgrade_status" data-value="y" id="edit-fpgrade_status" class="input-block status"></select>
                    </div>
                </div>
                <div class="padding-top-25 text-right">
                    <button type="button" data-dismiss="modal">close</button>
                    <button type="submit" class="default">save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){

        $('.grid-pagging').transpaginator({
            totalPages: <?php echo $grade['total_page'];?>,
            totalData: <?php echo $grade['total_data'];?>,
            currentPage: <?php echo $grade['page'];?>,
            pageUrl: '<?php echo BASE_URL;?>index.php?/fpgrade/index/&keyword=<?php echo $grade['keyword'];?>&page='
        });
        $('.tr_selector').on('click',function(){
            var id = $(this).attr('data-id');
            $('.editbtn').removeClass('fg-lightgray').removeAttr('disabled').attr('data-id',id);
            $('.tr_selector').removeClass('active');
            $(this).addClass('active');
        });
        $('#__edit').on('click',function(){
            var id = $(this).attr('data-id');
            if(typeof id !== 'undefined' && id !== ''){
                $.ajax({
                    url:'<?php echo BASE_URL;?>index.php?/fpgrade/edit/'+id,
                    dataType:'json',
                    success:function(res){
                        for(var k in res){
                            $('#edit-'+k).val(res[k]);
                        }
                        $('#modal-edit-department').addClass('fadein');
                    }
                });
            }
        });

        $('#__delete').on('click',function(){
            var id = $(this).attr('data-id');
            if(typeof id !== 'undefined' && id !== ''){
                if(confirm('<?php echo _('Are you sure want to delete this data?');?>')){
                    $.ajax({
                        url:'<?php echo BASE_URL;?>index.php?/fpgrade/delete',
                        type:'post',
                        data:{
                            fpgrade_id:id
                        },
                        success:function(){
                            location.reload();
                        }
                    });
                }
            }
        });

    });

    function filter(){
        var koperasi = $('#koperasi').val();
        window.location.href = '<?php echo BASE_URL;?>index.php?/fpgrade/index/&koperasi='+koperasi+'&page=1';
    }
</script>
<?php View::inc('footer.php');?>
