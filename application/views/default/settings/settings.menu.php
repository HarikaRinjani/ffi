<div class="sidebar">
    <span id="resize-handler" class="hidden-xs hidden-sm ui-resizable-handle ui-resizable-e">&nbsp;</span>
    <a href="javascript:void(0);" class="visible-sm toggleSidebar"><i class="ion-ios7-more"></i></a>
    <?php View::inc('koperasilogo.php');?>
    <ul class="navigation icon">
        <?php View::inc('bookmark/bookmark.php');?>
        <li>
            <a href="javascript:void(0);" class="navtoggle"><i class="ion-chevron-right"></i></a>
            <h3 class="title">Settings</h3>
            <ul>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/settings/index"><i class="ion-ios7-information-outline"></i>Apps Info</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL; ?>index.php?/koperasi/setting"><i class="ion-ios7-gear-outline"></i><?php echo _('Cooperative');?></a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL; ?>index.php?/tpk/setting"><i class="ion-ios7-home-outline"></i><?php echo _('Collection Point');?></a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL; ?>index.php?/kelompok/setting"><i class="ion-ios7-people-outline"></i><?php echo _('Farmers Group');?></a>
                </li>
                <!-- <li>
                    <a href="<?php echo BASE_URL; ?>index.php?/kelompokharga/setting"><i class="ion-ios7-barcode-outline"></i>Kelompok Harga</a>
                </li> -->
                <li>
                    <a href="<?php echo BASE_URL; ?>index.php?/anggota/setting"><i class="ion-ios7-contact-outline"></i><?php echo _('Farmers');?></a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL; ?>index.php?/truck/setting"><i class="ion-ios7-box-outline"></i>Truck</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL; ?>index.php?/konsumen/setting"><i class="ion-ios7-flag-outline"></i><?php echo _('Customer');?></a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL; ?>index.php?/milkprice"><i class="ion-ios7-calculator-outline"></i>Milk Price Formulation</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL; ?>index.php?/rezgrade/index"><i class="ion-ios7-copy-outline"></i>Resazurine Grade</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL; ?>index.php?/fpgrade/index"><i class="ion-ios7-cloud-download-outline"></i>Freezing Point Grade</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/periode/setting"><i class="ion-ios7-timer-outline"></i>Sampling Periode</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/card/"><i class="ion-ios7-barcode"></i>Card Management</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/cow/management"><i class="ion-ios7-paw-outline"></i>Cow Population</a>
                </li> 
            </ul>
        </li>
    </ul>
</div>
