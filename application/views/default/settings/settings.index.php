<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('settings/settings.menu.php');?>
<div class="content padded">
    <div class="breadcrumb">
        <ul class="tab">
            <li class="active">
                <a href="">Apps Settings</a>
            </li>

        </ul>
        <div class="pull-right">
            <ul class="breadcrumb-nav">
                <li>
                    <a href="">System Settings</a>
                </li>
                <li>
                    <a href="">Apps Settings</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <h1>Apps Info</h1>
            </div>
            <div class="col-md-2">
                <img class="img-responsive" src="<?php echo BASE_URL;?>themes/default/images/K3.png" alt="">
            </div>
            <div class="col-md-6">
                <h3>Milk Collection</h3>
                <div class="padding-left-20">
                    <p>Apps Version: <?php echo Config::$application['app_version'];?></p>
                    <p>Registered to: <?php echo Config::$application['company_name'];?></p>
                    <p>Expired date: <?php echo session::get('license_expired');?></p>
                    <p class="text-info">&copy; 2014 PT Daya Transformatika</p>
                    <p class="text-info"><a href="http://www.transformatika.co.id" target="_blank">www.transformatika.co.id</a></p>
                </div>
            </div>
        </div>
        <div class="row padding-top-20">
            <div class="col-md-6">
                <div class="panel">
                    <div class="panel-header">
                        <h3 class="panel-title">Notices</h3>
                    </div>
                    <div class="panel-body">
                        <?php foreach($notice as $k=>$v){ ?>
                        <p><i class="ion-alert-circled fg-red"></i> <?php echo $v;?></p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php View::inc('footer.php');?>
