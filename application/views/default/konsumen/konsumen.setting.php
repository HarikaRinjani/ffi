<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('settings/settings.menu.php');?>

<div class="content padded" xmlns="http://www.w3.org/1999/html">
    <div class="breadcrumb">
        <ul class="tab">
            <li >
                <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-add-konsumen">add new</a>
            </li>
            <li class="active">
                <a href="<?php echo BASE_URL;?>index.php?/konsumen/setting" disabled="disabled"><?php echo _('Customer');?></a>
            </li>
        </ul>
        <div class="pull-right">
            <ul class="breadcrumb-nav">
                <li>
                    <a href="">Apps Settings</a>
                </li>
                <li>
                    <a href=""><?php echo _('Customer');?></a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row padding-bottom-40">
            <div class="ribbon padding-left-15 padding-right-15 padding-bottom-10 padding-top-10 border-bottom text-right">
                <div class="menu">
                    <a href="javascript:void(0);" id="__edit" class="editbtn"><i class="ion-ios7-compose-outline"></i>edit</a>
                    <a href="javascript:void(0);" id="__delete" class="editbtn"><i class="ion-ios7-trash-outline"></i>delete</a>
                </div>
                <form action="javascript:void(0);" id="searchForm" onsubmit="return filter();">
                    <?php if(!in_array(session::get('user_group'),Config::$application['super_group'])){ ?>
                    <input type="hidden" name="koperasi" id="koperasi" value="<?php echo session::get('user_koperasi');?>">
                    <?php }else{ ?>
                    <select name="koperasi" id="koperasi" onchange="return filter();">
                        <option value="">All</option>
                        <?php foreach($koperasi['records'] as $k=>$v){ ?>
                            <option value="<?php echo $v['koperasi_id'];?>" <?php if($v['koperasi_id'] === $_GET['koperasi']){ echo 'selected="selected"'; } ?>><?php echo $v['koperasi_nama'];?></option>
                        <?php } ?>
                    </select>
                    <?php } ?>
                    <input type="text" placeholder="Search..." id="keyword" style="width:200px" value="<?php echo $konsumen['keyword'];?>">
                </form>
            </div>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th style="width:24px;">#</th>
                    <?php if(in_array(session::get('user_group'),Config::$application['super_group'])){ ?>
                    <th><?php echo _('Cooperative');?></th>
                    <?php } ?>
                    <th><?php echo _('Customer Name');?></th>
                    <th><?php echo _('Phone');?></th>
                    <th><?php echo _('Address');?></th>
                    <th><?php echo _('Status');?></th>
                </tr>
                </thead>
                <tbody>
                <?php if($konsumen['total_data'] == 0){ ?>
                    <tr>
                        <td colspan="5"><?php echo _('No record found');?></td>
                    </tr>
                <?php }else{ ?>
                    <?php foreach($konsumen['records'] as $k=>$v){ ?>
                        <tr class="tr_selector cursor-pointer" data-id="<?php echo $v['konsumen_id'];?>" id="tr<?php echo $v['konsumen_id'];?>">
                            <td>
                                <?php
                                if($konsumen['page'] == 1){
                                    echo ($k+1);
                                }else{
                                    echo ($k + 1 + ($konsumen['limit'] * ($konsumen['page'] - 1)));
                                }
                                ?>
                            </td>
                            <?php if(in_array(session::get('user_group'),Config::$application['super_group'])){ ?>
                            <td><?php echo $v['koperasi_nama'];?></td>
                            <?php } ?>
                            <td><?php echo $v['konsumen_nama'];?></td>
                            <td><?php echo $v['konsumen_telp'];?></td>
                            <td><?php echo $v['konsumen_alamat'];?></td>
                            <td><span class="status"><?php echo $v['konsumen_status'];?></span></td>
                        </tr>
                    <?php } ?>
                <?php } ?>
                </tbody>
            </table>
            <div class="grid-pagging border-top margin-bottom-30"></div>
        </div>
    </div>
</div>
<div class="modal draggable" id="modal-add-konsumen">
    <div class="modal-dialog">
        <div class="modal-header">
            <h3 class="modal-title">Add Customer</h3>
            <a href="javascript:void(0);" class="close">&times;</a>
        </div>
        <div class="modal-body">
            <form action="<?php echo BASE_URL;?>index.php?/konsumen/setting/save" method="post">
                <?php if(in_array(session::get('user_group'),Config::$application['super_group'])){ ?>
                <div class="form-group">
                    <label for="add-konsumen_koperasi" class="col-md-4"><?php echo _('Cooperative');?></label>
                    <div class="col-md-8">
                        <select name="konsumen_koperasi" id="add-konsumen_koperasi" class="input-block koperasi"></select>
                    </div>
                </div>
                <?php } else { ?>
                <input type="hidden" name="konsumen_koperasi" value="<?php echo session::get('user_koperasi');?>">
                <?php } ?>
                <div class="form-group">
                    <label for="add-konsumen_nama" class="col-md-4"><?php echo _('Customer Name');?> </label>
                    <div class="col-md-8">
                        <input type="text" id="add-konsumen_nama" class="input-block" name="konsumen_nama" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="add-konsumen_telp" class="col-md-4"><?php echo _('Phone');?> </label>
                    <div class="col-md-8">
                        <input type="text" id="add-konsumen_telp" class="input-block" name="konsumen_telp" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="add-konsumen_alamat" class="col-md-4"><?php echo _('Address');?> </label>
                    <div class="col-md-8">
                        <textarea id="add-konsumen_alamat" class="input-block" name="konsumen_alamat" required></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4" for="add-konsumen-status">Status</label>
                    <div class="col-md-8">
                        <select name="konsumen_status" id="konsumen_status" class="input-block status"></select>
                    </div>
                </div>

                <div class="padding-top-25 text-right">
                    <button type="button" data-dismiss="modal">close</button>
                    <button type="submit" class="default">save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal draggable" id="modal-edit-konsumen">
    <div class="modal-dialog">
        <div class="modal-header">
            <h3 class="modal-title">Edit Customer</h3>
            <a href="javascript:void(0);" class="close">&times;</a>
        </div>
        <div class="modal-body">
            <form action="<?php echo BASE_URL;?>index.php?/konsumen/setting/save" method="post">
                <input name="konsumen_id" type="hidden" id="edit-konsumen_id"/>
                <?php if(in_array(session::get('user_group'),Config::$application['super_group'])){ ?>
                <div class="form-group">
                    <label for="edit-konsumen_koperasi" class="col-md-4"><?php echo _('Cooperative');?></label>
                    <div class="col-md-8">
                        <select name="konsumen_koperasi" id="edit-konsumen_koperasi" class="input-block koperasi"></select>
                    </div>
                </div>
                <?php } else { ?>
                <input type="hidden" name="konsumen_koperasi" value="<?php echo session::get('user_koperasi');?>">
                <?php } ?>
                <div class="form-group">
                    <label for="edit-konsumen_nama" class="col-md-4"><?php echo _('Customer Name');?> </label>
                    <div class="col-md-8">
                        <input type="text" id="edit-konsumen_nama" class="input-block" name="konsumen_nama" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit-konsumen_telp" class="col-md-4"><?php echo _('Phone');?> </label>
                    <div class="col-md-8">
                        <input type="text" id="edit-konsumen_telp" class="input-block" name="konsumen_telp" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit-konsumen_alamat" class="col-md-4"><?php echo _('Address');?> </label>
                    <div class="col-md-8">
                        <textarea id="edit-konsumen_alamat" class="input-block" name="konsumen_alamat" required></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-4" for="edit-konsumen-status">Status</label>
                    <div class="col-md-8">
                        <select name="konsumen_status" id="edit-konsumen_status" class="input-block status"></select>
                    </div>
                </div>

                <div class="padding-top-25 text-right">
                    <button type="button" data-dismiss="modal">close</button>
                    <button type="submit" class="default">save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.grid-pagging').transpaginator({
            totalPages: <?php echo $konsumen['total_page'];?>,
            totalData: <?php echo $konsumen['total_data'];?>,
            currentPage: <?php echo $konsumen['page'];?>,
            pageUrl: '<?php echo BASE_URL;?>index.php?/konsumen/setting/&koperasi=<?php echo $_GET['koperasi'];?>&keyword=<?php echo $konsumen['keyword'];?>&page='
        });
        $('.tr_selector').on('click',function(){
            var id = $(this).attr('data-id');
            $('.editbtn').removeClass('fg-lightgray').removeAttr('disabled').attr('data-id',id);
            $('.tr_selector').removeClass('active');
            $(this).addClass('active');
        });
        $('#__edit').on('click',function(){
            var id = $(this).attr('data-id');
            if(typeof id !== 'undefined' && id !== ''){
                $.ajax({
                    url:'<?php echo BASE_URL;?>index.php?/konsumen/setting/edit/'+id,
                    dataType:'json',
                    success:function(res){
                        $('#edit-konsumen_id').val(res.konsumen_id);
                        $('#edit-konsumen_nama').val(res.konsumen_nama);
                        $('#edit-konsumen_status').val(res.konsumen_status);
                        $('#edit-konsumen_alamat').text(res.konsumen_alamat);
                        $('#edit-konsumen_telp').val(res.konsumen_telp);
                        $('#edit-konsumen_koperasi').val(res.konsumen_koperasi);
                        $('#modal-edit-konsumen').addClass('fadein');
                    }
                });
            }
        });

        $('#__delete').on('click',function(){
            var id = $(this).attr('data-id');
            if(typeof id !== 'undefined' && id !== ''){
                if(confirm('<?php echo _('Are you sure want to delete this data?');?>')){
                    $.ajax({
                        url:'<?php echo BASE_URL;?>index.php?/konsumen/setting/delete',
                        type:'post',
                        data:{
                            konsumen_id:id
                        },
                        success:function(){
                            location.reload();
                        }
                    });
                }
            }
        });

    });

    function filter(){
        var keyword = $('#keyword').val();
        var koperasi = $('#koperasi').val();
        window.location.href = '<?php echo BASE_URL;?>index.php?/konsumen/setting/&koperasi='+koperasi+'&keyword='+keyword+'&page=1';
    }
</script>
<?php View::inc('footer.php');?>
