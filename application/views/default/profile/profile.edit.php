<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('dashboard/dashboard.menu.php');?>
    <div class="content padded">
        <div class="breadcrumb">
            <ul class="tab">
                <li class="active">
                    <a href="">My Profile</a>
                </li>
                <li>
                	<a href="<?php echo BASE_URL;?>index.php?/profile/bookmark">bookmark</a>
                </li>
            </ul>
            <div class="pull-right">
                <ul class="breadcrumb-nav">
                    <li>
                        <a href="">Dashboard</a>
                    </li>
                    <li>
                        <a href="">Profile</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class=" ribbon">
                    <div class="menu">
                    	<a href="<?php echo BASE_URL; ?>index.php?/profile" class=" "><i class="ion-ios7-information-outline"></i>information</a>
                    	<a href="" class="separator"></a>
                    	<a href="javascript:void(0);" class="" onclick="$('#profile-save-btn').trigger('click');"><i class="ion-ios7-save"></i>save change</a>
                    	
                    </div>
                </div>
                <div class="col-md-12 margin-top-15">
                    <div class="row">
                        <div class="col-md-2">
                            <img src="<?php echo session::get('user_avatar'); ?>" alt="" class="img img-thumbnail"/>
                        </div>
                        <div class="col-md-6">
                            <form action="<?php echo BASE_URL; ?>index.php?/profile/save" method="post" enctype="multipart/form-data">
                                <table class="table no-hover">
                                    <tr>
                                        <td style="width:100px;">Username</td>
                                        <td>
                                            <?php echo $user['user_name'];?>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:100px;">Fullname</td>
                                        <td>
                                            <input type="text" class="input-block" name="user_fullname" value="<?php echo $user['user_fullname'];?>" required/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:100px;">Email</td>
                                        <td>
                                            <input type="text" class="input-block" name="user_email" value="<?php echo $user['user_email'];?>" required/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:100px;">Password</td>
                                        <td>
                                            <input type="password" class="input-block" name="user_password" value="" placeholder=""/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:100px;">Theme</td>
                                        <td>
                                            <select name="user_theme" id="user_theme">
                                                <?php
                                                foreach(Config::$application['color_scheme'] as $k=>$v){
                                                    echo '<option value="'.$k.'"';
                                                    if($user['user_theme'] == $k){
                                                        echo ' selected="selected"';
                                                    }
                                                    echo '>'.$v.'</option>';
                                                }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Photo</td>
                                        <td>
                                            <input type="file" name="user_avatar" id="user_avatar"/>
                                        </td>
                                    </tr>
                                    <button style="visibility:hidden;width:1px;height:1px" id="profile-save-btn" type="submit">Save change</button>
                                </table>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php View::inc('footer.php');?>