<?php View::inc('header.php');?>
<?php View::inc('navbarFarmer.php');?>
<?php View::inc('dashboard/dashboard.menuFarmer.php');?>
<div class="content padded">
    <div class="breadcrumb">
        <ul class="tab">
            <li class="active">
                <a href="">My Profile</a>
            </li>
            <li>
            	<a href="<?php echo BASE_URL;?>index.php?/profile/bookmark">bookmark</a>
            </li>
        </ul>
        <div class="pull-right">
            <ul class="breadcrumb-nav">
                <li>
                    <a href="">Dashboard</a>
                </li>
                <li>
                    <a href="">Profile</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="ribbon">
                <div class="menu">
                	<a href="<?php echo BASE_URL; ?>index.php?/profile" class=" "><i class="ion-ios7-information-outline"></i>information</a>
                	<a href="" class="separator"></a>
                	<a href="<?php echo BASE_URL; ?>index.php?/profile/edit" class=""><i class="ion-ios7-compose-outline"></i>edit</a>
                	
                </div>
                    
            </div>
            <div class="col-md-12 padding-top-15">
                <div class="row">
                    <div class="col-md-2">
                        <img src="<?php echo session::get('user_avatar'); ?>" alt="" class="img img-thumbnail"/>
                    </div>
                    <div class="col-md-8">
                        <table class="table no-hover" style="width:auto;">
                            <tr>
                                <td>Username</td>
                                <td><?php echo session::get('user_name');?></td>
                            </tr>
                            <tr>
                                <td>Fullname</td>
                                <td><?php echo session::get('user_fullname');?></td>
                            </tr>
                            <tr>
                                <td>Email</td>
                                <td><?php echo session::get('user_email');?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php View::inc('footer.php');?>