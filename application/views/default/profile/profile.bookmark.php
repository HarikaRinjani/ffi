<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('dashboard/dashboard.menu.php');?>
    <div class="content padded">
        <div class="breadcrumb">
            <ul class="tab">
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/profile/index/">My Profile</a>
                </li>
                <li class="active">
                    <a href="">Bookmark</a>
                </li>
            </ul>
            <div class="pull-right">
                <ul class="breadcrumb-nav">
                    <li>
                        <a href="">Dashboard</a>
                    </li>
                    <li>
                        <a href="">Profile</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="ribbon">
                    <div class="menu">
                        <a href="<?php echo BASE_URL; ?>index.php?/profile/bookmark" class="padding-right-15"><i class="ion-ios7-folder-outline"></i>bookmark</a>
                        <a href="" class="separator"></a>
                        <a href="javascript:void(0);"><i class="ion-ios7-help-outline"></i>help</a>
                    </div>
                </div>
                <table class="table">
                    <thead>
                    <tr>
                        <th>Bookmark Name</th>
                        <th>Bookmark URL</th>
                        <th class="text-center">DELETE</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($bookmark['records'] as $k=>$v){ ?>
                    <tr>
                        <td><?php echo $v['bookmark_title'];?></td>
                        <td><?php echo BASE_URL.$v['bookmark_url'];?></td>
                        <td class="text-center">
                            <h4 class="no-padding no-margin">
                                <a href="javascript:void(0);" onclick="deleteBookmark('<?php echo $v['bookmark_id'];?>');">
                                    <i class="ion-ios7-trash"></i>
                                </a>
                            </h4>

                        </td>
                    </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script>
        function deleteBookmark(id){
            $.ajax({
                type:'post',
                url:'<?php echo BASE_URL;?>index.php?/bookmark/delete',
                data:{
                    bookmark_id:id
                },
                success:function(){
                    location.reload();
                }

            });
        }
    </script>
<?php View::inc('footer.php');?>