<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('report/report.menu.php');?>

<div class="content padded" xmlns="http://www.w3.org/1999/html">
    <div class="breadcrumb">
        <ul class="tab">
            <li class="active">
                <a href="<?php echo BASE_URL;?>index.php?/anggota/setting" disabled="disabled">Sampling Schedule</a>
            </li>
        </ul>
        <div class="pull-right">
            <ul class="breadcrumb-nav">
                <li>
                    <a href="">Report</a>
                </li>
                <li>
                    <a href="">Sampling Schedule</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="ribbon text-right">
                <div class="menu">
                    <a href="javascript:void(0);" class="no-hover"><i class="ion-ios7-gear-outline"></i>Coop:</a>
                    <select id="inputfilter-koperasi" onchange="filter();">
                        <option value="">All</option>
                        <?php foreach ($koperasi['records'] as $k => $v) {
                            echo '<option value="'.$v['koperasi_id'].'" ';
                            if ($v['koperasi_id'] === $_GET['koperasi']) {
                                echo 'selected="selected"';
                            }
                            echo '>'.$v['koperasi_nama'].'</option>';
                        }
                        ?>
                    </select>
                    <select id="inputfilter-tpk" onchange="filter();">
                        <option value="">All</option>
                        <?php foreach ($tpk['records'] as $k => $v) {
                            echo '<option value="'.$v['tpk_id'].'" ';
                            if ($v['tpk_id'] === $_GET['tpk']) {
                                echo 'selected="selected"';
                            }
                            echo '>'.$v['tpk_nama'].'</option>';
                        }
                        ?>
                    </select>
                </div>
                <form action="javascript:void(0);" id="searchForm" onsubmit="return filter();">
                    <input type="text" class="datepicker" id="dateselector" value="<?php echo $_GET['date'];?>" onchange="filter();">
                    <input type="text" placeholder="Search..." id="keyword" style="width:200px" value="<?php echo $schedule['keyword'];?>">
                </form>
            </div>

            <div class="padding-15">
                <div><?php echo $chart;?></div>
            </div>
            <div class="col-md-12 margin-top-15">
                <div class="text-right">
                    <h3>
                        <a href="<?php echo BASE_URL;?>index.php?/samplingschedule/index/&date=<?php echo $_GET['date'];?>&export=true" target="_blank"><i class="ion-ios7-download-outline"></i> export</a>
                    </h3>
                </div>
            </div>
            <table class="table table-hover border-top">
                <thead>
                <tr>
                    <th style="width:24px;">#</th>
                    <th><?php echo _('Cooperative');?></th>
					<th><?php echo _('Collection Point');?></th>
                    <th><?php echo _('Date');?></th>
                    <th><?php echo _('Farmer');?></th>
                    <th><?php echo _('Session');?></th>
                    <th><?php echo _('Periode');?></th>
                    <th><?php echo _('Status');?></th>
                </tr>
                </thead>
                <tbody>
                <?php if($schedule['total_data'] == 0){ ?>
                    <tr>
                        <td colspan="100%"><?php echo _('No record found');?></td>
                    </tr>
                <?php }else{ ?>
                    <?php foreach($schedule['records'] as $k=>$v){ ?>
                        <tr class="tr_selector cursor-pointer" data-id="<?php echo $v['samplingschedule_id'];?>" id="tr<?php echo $v['samplingschedule_id'];?>">
                            <td>
                                <?php
                                if($schedule['page'] == 1){
                                    echo ($k+1);
                                }else{
                                    echo ($k + 1 + ($schedule['limit'] * ($schedule['page'] - 1)));
                                }
                                ?>
                            </td>
                            <td><?php echo $v['koperasi_nama'];?></td>
							<td><?php echo $v['tpk_nama'];?></td>
                            <td class="format-date"><?php echo $v['samplingschedule_date'];?></td>
                            <td><?php echo $v['anggota_nomor'].' - '.$v['anggota_nama'];?></td>
                            <td class="samplingsesi"><?php echo $v['samplingschedule_sesi'];?></td>
                            <td><?php echo $v['samplingschedule_periode'];?></td>
                            <td><span class="samplingstatus"><?php echo $v['samplingschedule_status'];?></span></td>
                        </tr>
                    <?php } ?>
                <?php } ?>
                </tbody>
            </table>
            <div class="grid-pagging border-top margin-bottom-30"></div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('.grid-pagging').transpaginator({
            totalPages: <?php echo $schedule['total_page'];?>,
            currentPage: <?php echo $schedule['page'];?>,
            totalData: <?php echo $schedule['total_data'];?>,
            pageUrl: '<?php echo BASE_URL;?>index.php?/samplingschedule/index/&date=<?php echo $_GET['date'];?>&keyword=<?php echo $schedule['keyword'];?>&page='
        });
        $('.tr_selector').on('click',function(){
            var id = $(this).attr('data-id');
            $('.editbtn').removeClass('fg-lightgray').removeAttr('disabled').attr('data-id',id);
            $('.tr_selector').removeClass('active');
            $(this).addClass('active');
        });
        /* $('#__edit').on('click',function(){
            var id = $(this).attr('data-id');
            if(typeof id !== 'undefined' && id !== ''){
                $.ajax({
                    url:'<?php echo BASE_URL;?>index.php?/periode/setting/edit/'+id,
                    dataType:'json',
                    success:function(res){
                        $('#edit-periode_id').val(res.periode_id);
                        $('#edit-periode_status').val(res.periode_status);
                        $('#edit-periode_effectivedate').val(moment(res.periode_effectivedate).format('DD/MM/YYYY'));
                        $('#edit-periode_interval').val(res.periode_interval);
                        $('#edit-periode_koperasi').val(res.periode_koperasi);
                        $('#modal-edit-periode').addClass('fadein');
                    }
                });
            }
        });

        $('#__delete').on('click',function(){
            var id = $(this).attr('data-id');
            if(typeof id !== 'undefined' && id !== ''){
                if(confirm('<?php echo _('Are you sure want to delete this data?');?>')){
                    $.ajax({
                        url:'<?php echo BASE_URL;?>index.php?/anggota/setting/delete',
                        type:'post',
                        data:{
                            anggota_id:id
                        },
                        success:function(){
                            location.reload();
                        }
                    });
                }
            }
        }); */

    });

    function filter(){
        var keyword = $('#keyword').val();
        var date = $('#dateselector').val();
        var koperasi = $('#inputfilter-koperasi').val();
        var tpk = $('#inputfilter-tpk').val();
        window.location.href = '<?php echo BASE_URL;?>index.php?/samplingschedule/index/&koperasi='+koperasi+'&tpk='+tpk+'&keyword='+keyword+'&date='+date+'&page=1';
    }
</script>
<?php View::inc('footer.php');?>
