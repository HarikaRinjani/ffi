<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('qcparameter/qcparameter.menu.php');?>

<div class="content padded">
    <div class="breadcrumb">
        <ul class="tab">
            <li class="active">
                <a href="<?php echo BASE_URL;?>index.php?/anggota/setting" disabled="disabled">Sampling Schedule</a>
            </li>
        </ul>
        <div class="pull-right">
            <ul class="breadcrumb-nav">
                <li>
                    <a href="">Quality</a>
                </li>
                <li>
                    <a href="">Sampling Schedule</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="ribbon text-right">
                <div class="menu">
                    <a href="javascript:void(0)" id="generate-sample" data-toggle="modal" data-target="#generate-modal"><i class="ion-ios7-reload"></i> Generate</a>
                </div>
                <form action="javascript:void(0);" id="searchForm" onsubmit="return filter();">
                    <select name="koperasi" id="inputfilter-koperasi" class="" onChange="filter();">
                        <option value="">All</option>
                        <?php
                        foreach ($koperasi as $k => $v) {
                            echo '<option value="'.$v['koperasi_id'].'"';
                            if ($v['koperasi_id'] == $_GET['koperasi']) {
                                echo ' selected="selected"';
                            }
                            echo '>'.$v['koperasi_nama'].'</option>';
                        }
                        ?>
                    </select>
                    <select name="tpk" id="inputfilter-tpk" class="" onChange="filter();">
                        <option value="">All</option>
                        <?php
                        foreach ($tpk as $k => $v) {
                            echo '<option value="'.$v['tpk_id'].'"';
                            if ($v['tpk_id'] == $_GET['tpk']) {
                                echo ' selected="selected"';
                            }
                            echo '>'.$v['tpk_nama'].'</option>';
                        }
                        ?>
                    </select>
                    <input type="text" class="datepicker" id="dateselector" value="<?php echo $_GET['date'];?>" onchange="filter();">
                    <select name="sesi" id="sesi" onchange="filter()">
                        <?php
                        $sesi = array(
                            '' => 'All',
                            'p' => 'Morning',
                            's' => 'Afternoon',
                        );
                        foreach ($sesi as $k => $v) {
                            echo '<option value="'.$k.'"';
                            if (@$_GET['sesi'] === $k) {
                                echo ' selected="selected"';
                            }
                            echo '>'.$v.'</option>';
                        }
                        ?>
                    </select>
                    <input type="text" placeholder="Search..." id="keyword" style="width:200px" value="<?php echo $schedule['keyword'];?>">
                </form>
            </div>

            <table class="table table-hover">
                <thead>
                <tr>
                    <th style="width:24px;">#</th>
                    <th><?php echo _('Cooperative');?></th>
                    <th><?php echo _('Collection Point');?></th>
                    <th><?php echo _('Date');?></th>
                    <th><?php echo _('Farmer');?></th>
                    <th><?php echo _('Session');?></th>
                    <th><?php echo _('Periode');?></th>
                    <th><?php echo _('Status');?></th>
                </tr>
                </thead>
                <tbody>
                <?php if ($schedule['total_data'] == 0) {
    ?>
                    <tr>
                        <td colspan="100%"><?php echo _('No record found');
    ?></td>
                    </tr>
                <?php
} else {
    ?>
                    <?php foreach ($schedule['records'] as $k => $v) {
    ?>
                        <tr class="tr_selector cursor-pointer" data-id="<?php echo $v['samplingschedule_id'];
    ?>" id="tr<?php echo $v['samplingschedule_id'];
    ?>">
                            <td>
                                <?php
                                if ($schedule['page'] == 1) {
                                    echo $k + 1;
                                } else {
                                    echo $k + 1 + ($schedule['limit'] * ($schedule['page'] - 1));
                                }
    ?>
                            </td>
                            <td><?php echo $v['koperasi_nama'];
    ?></td>
                            <td><?php echo $v['tpk_nama'];
    ?></td>
                            <td class="format-date"><?php echo $v['samplingschedule_date'];
    ?></td>
                            <td><?php echo ''.$v['anggota_nomor'].' - '.$v['anggota_nama'];
    ?></td>
                            <td class="samplingsesi"><?php echo $v['samplingschedule_sesi'];
    ?></td>
                            <td><?php echo $v['samplingschedule_periode'];
    ?></td>
                            <td><span class="samplingstatus"><?php echo $v['samplingschedule_status'];
    ?></span></td>
                        </tr>
                    <?php
}
    ?>
                <?php
} ?>
                </tbody>
            </table>
            <div class="grid-pagging border-top margin-bottom-30"></div>
            <div class="padding-10">
                <em>
                *) Morning Schedule will be processed on next day.
                </em>
            </div>
        </div>
    </div>
</div>
<div class="modal draggable" id="generate-modal">
    <div class="modal-dialog modal-sm">
        <div class="modal-header">
            <h3 class="modal-title">Generate Sampling</h3>
        </div>
        <div class="modal-content">
            <form action="<?php echo BASE_URL;?>index.php?/samplingschedule/generate" method="post">
                <input type="hidden" name="date" value="<?php echo $_GET['date'];?>" />
                <div class="form-group">
                    <label class="col-md-12" for="samplingschedule_koperasi">Cooperative</label>
                    <div class="col-md-12">
                        <select class="input-block" id="samplingschedule_koperasi" name="samplingschedule_koperasi" required="required">
                            <option value="">-- Please select Cooperative --</option>
                            <?php foreach ($koperasi['records'] as $k => $v) {
    ?>
                                <option value="<?php echo $v['koperasi_id'];
    ?>"><?php echo $v['koperasi_nama'];
    ?></option>
                            <?php
} ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" for="samplingschedule_tpk">Collection Point</label>
                    <div class="col-md-12">
                        <select name="samplingschedule_tpk" id="samplingschedule_tpk" class="input-block" required="required">
                            <option value="">-- please select cooperative --</option>
                        </select>
                    </div>
                </div>
                <div class="text-right margin-top-25">
                    <button type="button" data-dismiss="modal">Close</button>
                    <button type="submit" class="default">Generate</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.grid-pagging').transpaginator({
            totalPages: <?php echo $schedule['total_page'];?>,
            currentPage: <?php echo $schedule['page'];?>,
            totalData: <?php echo $schedule['total_data'];?>,
            pageUrl: '<?php echo BASE_URL;?>index.php?/samplingschedule/data/&sesi=<?php echo $_GET['sesi'];?>&date=<?php echo $_GET['date'];?>&keyword=<?php echo $schedule['keyword'];?>&page='
        });
        $('#samplingschedule_koperasi').on('change',function(){
            var el = $(this);
            var valu = el.val();
            var opt = '';
            if(valu !== ''){
                $.ajax({
                    url:'<?php echo BASE_URL;?>index.php?/tpk/findByKoperasi/'+valu,
                    dataType:'json',
                    success:function(res){
                        var data = res.records;
                        for(var k in data){
                            opt += '<option value="'+data[k].tpk_id+'">';
                            opt += data[k].tpk_nama;
                            opt += '</option>';
                        }
                        $('#samplingschedule_tpk').html(opt);
                        opt = null;
                    }
                });
            }else{
                $('#samplingschedule_tpk').html('').val('');
            }
        });
        $('.tr_selector').on('click',function(){
            var id = $(this).attr('data-id');
            $('.editbtn').removeClass('fg-lightgray').removeAttr('disabled').attr('data-id',id);
            $('.tr_selector').removeClass('active');
            $(this).addClass('active');
        });

    });

    function filter(){
        var keyword = $('#keyword').val();
        var date = $('#dateselector').val();
        var sesi = $('#sesi').val();
        var koperasi = $('#inputfilter-koperasi').val();
        var tpk = $('#inputfilter-tpk').val();
        window.location.href = '<?php echo BASE_URL;?>index.php?/samplingschedule/data/&koperasi='+koperasi+'&tpk='+tpk+'&sesi='+sesi+'&keyword='+keyword+'&date='+date+'&page=1';
    }
</script>
<?php View::inc('footer.php');?>
