<div class="navbar">
    <a href="javascript:void(0);" onclick="$('.topmenu').toggleClass('show');" class="hidden-md toggleNavbar"><i class="ion-ios7-keypad"></i></a>
    <?php if(session::get('user_koperasi') != 'none'){ ?>
    <a href="" class="brand">
        <?php if(Config::$application['display_cooperativelogo'] === true && Config::$application['cooperativelogo_size'] === 1 && session::get('user_koperasi_logo')){ ?>
        <img src="<?php echo session::get('user_koperasi_logo');?>" alt="" style="width:40px;height:40px;margin-left:-15px;margin-right:5px;float:left;">
        <?php }else{ ?>
        <span style="margin-left: -20px;margin-right:5px;height: 40px;width: 40px;padding-top: 2px;text-align: center;background: rgba(0,0,0,.1);" class="visible-md visible-lg pull-left fg-white" id="__toggleSidebar"><i class="ion-navicon"></i></span>
        <?php }?>
        <?php echo session::get('user_koperasi_nama');?>
    </a>
    <?php }else{ ?>
    <a href="" class="brand">
        <span style="margin-left: -20px;margin-right:5px;height: 40px;width: 40px;padding-top: 2px;text-align: center;background: rgba(0,0,0,.1);" class="visible-md visible-lg pull-left fg-white" id="__toggleSidebar"><i class="ion-navicon"></i></span>
        Milk Collection
    </a>
    <?php } ?>
    <ul class="nav topmenu">
        <li id="topmenu-dashboard">
            <a href="<?php echo BASE_URL;?>index.php?/dashboard">
                <span class="ion-ios7-home-outline"></span> Dashboard
            </a>
        </li>
        <?php if(Config::$application['is_client'] === true){ ?>
        <li id="topmenu-transaksi">
            <a href="<?php echo BASE_URL;?>index.php?/transaksi">
                <span class="ion-ios7-compose-outline"></span> MCP
            </a>
        </li>

        <?php }else{ ?>
        <li id="topmenu-quality">
            <a href="<?php echo BASE_URL;?>index.php?/qcparameter/index">
                <span class="ion-ios7-lightbulb-outline"></span> Quality
            </a>
        </li>
        <li id="topmenu-distribution">
            <a href="<?php echo BASE_URL;?>index.php?/distribution/index">
                <span class="ion-ios7-navigate-outline"></span> Distribution
            </a>
        </li>
        <li id="topmenu-report">
            <a href="<?php echo BASE_URL;?>index.php?/report/index">
                <span class="ion-ios7-pie-outline"></span> &nbsp;Report
            </a>
        </li>
        <?php } ?>
        <!-- <li id="topmenu-sync">
            <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-sinkronisasi">
                <span class="ion-ios7-loop margin-right-5"></span> &nbsp;Sync
            </a>
        </li> -->
        <li id="topmenu-help">
            <a href="<?php echo BASE_URL;?>index.php?/help/index">
                <span class="ion-ios7-help-outline"></span> Help
            </a>
        </li>
    </ul>
    <div class="pull-right">
        <ul class="nav">
<!--            <li>-->
<!--                <a href="#!/mail">-->
<!--                    <span class="ion-ios7-email-outline"></span>-->
<!--                </a>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a href="">-->
<!--                    <span class="ion-ios7-help-outline"></span>-->
<!--                </a>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a href="javascript:void(0)" id="messenger-toggle">-->
<!--                    <span class="ion-ios7-chatbubble-outline"></span>-->
<!--                </a>-->
<!--                <span class="chat-bubble bg-white fg-red hidden" id="chat-bubble">0</span>-->
<!--            </li>-->
<!--            <li>-->
<!--                <a href="">-->
<!--                    <span class="ion-ios7-information-outline"></span>-->
<!--                </a>-->
<!--            </li>-->
            <li>
                <a href="javascript:void(0);" onclick="$(this).next().toggleClass('fadein');" class="profile">
                    <small class="hidden-xs hidden-sm"><?php echo session::get('user_fullname');?></small>
                    <img src="<?php echo session::get('user_avatar');?>" alt=""/>
                </a>
                <div class="dropdown right" style="width:320px">
                    <div class="dropdown-content">
                        <div class="padding-bottom-20 margin-bottom-10 border-bottom">
                            <img src="<?php echo session::get('user_avatar');?>" class="pull-left margin-right-10" alt="" style="width:50px;height:50px;">
                            <p class="margin-bottom-5 margin-top-5 fg-gray"><?php echo session::get('user_fullname');?></p>
                            <small class="fg-blue hidden-xs hidden-sm">last activity: <?php echo date('d F Y H:i',strtotime(session::get('user_lastactivity')));?></small>
                        </div>
                        <a class="media list" href="<?php echo BASE_URL;?>index.php?/settings/index">
                            <div class="pull-left bg-blue icon">
                                <i class="ion-ios7-cog"></i>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">Apps Settings</h4>
                                Module Settings
                            </div>
                        </a>
                        <a class="media list" href="<?php echo BASE_URL;?>index.php?/system/index">
                            <div class="pull-left bg-purple icon">
                                <i class="ion-ios7-monitor"></i>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">System Settings</h4>
                                Administration Panel
                            </div>
                        </a>
                        <a class="media list" href="<?php echo BASE_URL;?>index.php?/dashboard/logout">
                            <div class="pull-left bg-gray icon" >
                                <i class="ion-ios7-redo"></i>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">Sign out</h4>
                                Sign out from Milk Collection System
                            </div>
                        </a>
                        <div class="margin-top-30 padding-top-10 border-top text-center">
                            <small class="fg-gray">&copy;2015 Frisian Flag Indonesia</small>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>

<script>
    $(document).ready(function(){
        var curURL = document.location.href;
        var stripeURL = curURL.replace('<?php echo BASE_URL;?>','');
        // index.php?/
        var controller = 'dashboard';
        if(stripeURL.length < 11){
            controller = 'dashboard';
        }else{
            var stripeINDEX = stripeURL.replace('index.php?/','');
            var explodeURL = stripeINDEX.split('/');
            controller = explodeURL[0];
        }

        if(controller === 'dashboard' || controller === 'profile' || controller === 'mail'){
            $('#topmenu-dashboard').addClass('active');
        }else if(controller === 'transaksi'){
            $('#topmenu-transaksi').addClass('active');
        }else if(controller === 'report'){
            $('#topmenu-report').addClass('active');
        }else if(controller === 'distribution'){
            $('#topmenu-distribution').addClass('active');
        }else if(controller === 'qcparameter'){
            $('#topmenu-quality').addClass('active');
        }else if(controller === 'help'){
            $('#topmenu-help').addClass('active');
        }
        $('#__toggleSidebar').on('click',function(e){
            e.preventDefault();
             var w = $.cookie('sidebar_width');
            if(w === '' || typeof w === 'undefined'){
                $.cookie('sidebar_width',200);
            }else{
                if(w > 0){
                    $.cookie('sidebar_width',0);
                }else{
                    $.cookie('sidebar_width',200);
                }
            }

            //console.log(w);
            //console.log(neww);
            regenerateSidebar();
        });
    });
</script>
