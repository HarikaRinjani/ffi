<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('dashboard/dashboard.menu.php');?>
    <div class="content padded">
        <div class="breadcrumb">
            <ul class="tab">
                <li class="active">
                    <a href="<?php echo BASE_URL;?>index.php?/mail/index">my inbox</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/mail/sent">sent item</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/mail/trash">trash</a>
                </li>
            </ul>
            <div class="pull-right">
                <ul class="breadcrumb-nav">
                    <li>
                        <a href="">Dashboard</a>
                    </li>
                    <li>
                        <a href="">Internal Messages</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row ">
                <div class="ribbon ">
                    <div class="menu">
                        <a href="<?php echo BASE_URL; ?>index.php?/mail" class="padding-right-10"><i class="ion-ios7-email-outline"></i>inbox</a>
                        <a href="<?php echo BASE_URL; ?>index.php?/mail/reply/<?php echo $mail['inbox_id'];?>" class="padding-right-10"><i class="ion-ios7-redo-outline"></i>reply</a>
                        <a href="javascript:void(0);" onclick="deleteMail('<?php echo $mail['inbox_id'];?>');" class="padding-right-10"><i class="ion-ios7-trash-outline"></i>delete</a>
                    </div>
                </div>
                <div class="col-md-12">
                    <?php
                    $avatar = 'files/images/avatar.jpg';
                    if(!empty($mail['user_avatar'])){
                        $avatar = $mail['user_avatar'];
                    }

                    ?>
                    <table class="margin-top-10" style="width:100%;">
                        <tr>
                            <td class="padding-top-5" style="width:50px;vertical-align: top;">
                                <img src="<?php echo $avatar; ?>" alt="" class="img-responsive"/>
                            </td>
                            <td class="padding-top-5 padding-left-20" style="vertical-align: top;">
                                <small>
                                    <?php echo date('D d/m/Y h:i A',strtotime($mail['inbox_date']));?>
                                </small>
                                <h3 class="margin-top-0" style="font-size:16px;"><?php echo $mail['user_fullname'].' &lt;'.$mail['user_email'].'&gt;';?></h3>
                                <h4 style="font-size:14px;"><?php echo $mail['inbox_title'];?></h4>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="fg-gray padding-bottom-5">
                                <small>To &nbsp; <?php echo session::get('user_fullname');?> &lt;<?php echo session::get('user_email');?>&gt;</small>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="border-top">
                                <div class="padding-10">
                                    <?php echo $mail['inbox_content'];?>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
<script type="text/javascript">
    function deleteMail(inbox_id){
        if(confirm('Are you sure want to delete this item?')){
            if(typeof inbox_id !== 'undefined'){
                $.ajax({
                    url:'<?php echo BASE_URL;?>index.php?/mail/delete',
                    data:{
                        inbox_id:inbox_id
                    },
                    type:'post',
                    success:function(res){
                        if(res === 'success'){
                            location.href = '<?php echo BASE_URL;?>index.php?/mail/index';
                        }else{
                            location.reload();
                        }
                    }
                });
            }
        }
    }
</script>
<?php View::inc('footer.php');?>