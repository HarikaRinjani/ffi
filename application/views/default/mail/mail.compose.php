<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('dashboard/dashboard.menu.php');?>
    <link rel="stylesheet" href="<?php echo BASE_URL;?>themes/default/jquery-tokeninput/styles/token-input-facebook.css"/>
    <div class="content padded">
        <div class="breadcrumb">
            <ul class="tab">
                <li class="active">
                    <a href="<?php echo BASE_URL;?>index.php?/mail/compose">write mail</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/mail/sent">sent item</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/mail/trash">trash</a>
                </li>
            </ul>
            <div class="pull-right">
                <ul class="breadcrumb-nav">
                    <li>
                        <a href="">Dashboard</a>
                    </li>
                    <li>
                        <a href="">Internal Messages</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row ">
                <div class="ribbon border-bottom text-right">
                    <div class="menu">
                        <a href="<?php echo BASE_URL;?>index.php?/mail/index"><i class="ion-ios7-email-outline"></i>inbox</a>
                        <a href="" class="separator"></a>
                        <a href="javascript:void(0);" onclick="$('#sendmail-btn').trigger('click');"><i class="ion-ios7-paperplane-outline"></i>send</a>
                    </div>
                </div>
                <div class="col-md-12">
                    <form method="post" action="<?php echo BASE_URL; ?>index.php?/mail/send" enctype="multipart/form-data">
                        <table class="margin-top-15 margin-bottom-15" style="width:100%;">
                            <tr>
                                <td style="width:80px;">to</td>
                                <td>
                                    <input type="text" name="inbox_user" id="inbox_user" class="input-block" required/>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div style="height:10px;display:block;"></div>
                                </td>
                            </tr>
                            <tr>
                                <td>subject</td>
                                <td>
                                    <input type="text" name="inbox_title" id="inbox_title" class="input-block" required/>
                                </td>
                            </tr>
                        </table>
                        <textarea name="inbox_content" id="inbox_content" class="ckeditor" rows="10"></textarea>
                        <div class="padding-top-10">
                            <span class="margin-right-10"><i class="ion-ios7-folder-outline"></i> attachment:</span>
                            <input type="file" name="inbox_attachment" id="inbox_attachment"/>
                        </div>
                        <button id="sendmail-btn" type="submit" style="visibility:hidden;width:1px;height:1px;"></button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="<?php echo BASE_URL;?>themes/default/jquery-tokeninput/src/jquery.tokeninput.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('#inbox_user').tokenInput('<?php echo BASE_URL;?>index.php?/mail/searchUser',{
                'placeholder': 'to: ',
                method:'POST',
                theme: "facebook"
            });
        });
    </script>
<?php View::inc('footer.php');?>