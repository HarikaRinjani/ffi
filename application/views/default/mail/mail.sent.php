<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('dashboard/dashboard.menu.php');?>

<div class="content padded">
    <div class="breadcrumb">
        <ul class="tab">
            <li>
                <a href="<?php echo BASE_URL;?>index.php?/mail/index">my inbox</a>
            </li>
            <li  class="active">
                <a href="">sent item</a>
            </li>
            <li>
                <a href="<?php echo BASE_URL;?>index.php?/mail/trash">trash</a>
            </li>
        </ul>
        <div class="pull-right">
            <ul class="breadcrumb-nav">
                <li>
                    <a href="">Dashboard</a>
                </li>
                <li>
                    <a href="">Internal Messages</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row ">
            <div class="ribbon border-bottom text-right">
                <div class="menu">
                    <a href="<?php echo BASE_URL;?>index.php?/mail/compose"><i class="ion-ios7-email-outline"></i>new mail</a>
                </div>
                <form action="javascript:void(0);" id="searchForm" onsubmit="return filter();">
                    <input type="text" placeholder="Search mail..." id="keyword" style="width:300px" value="<?php echo $mail['keyword'];?>">
                </form>
            </div>
            <table class="table">
                <tr>
                    <th>Date</th>
                    <th>To</th>
                    <th>Mail Title</th>
                </tr>
                <?php if($mail['total_data'] === 0){ ?>
                <tr>
                    <td colspan="3"><?php echo _('No record found');?></td>
                </tr>
                <?php } ?>
                <?php foreach($mail['records'] as $k=>$v){ 
                    $style = '';
                    if($v['inbox_read'] == 'n'){ 
                        $style = 'font-weight:bold;';
                    }
                    ?>
                <tr onclick="goto('<?php echo $v['inbox_id'];?>');">
                    <td style="<?php echo $style;?>" class="format-datetime"><?php echo $v['inbox_date'];?></td>
                    <td style="<?php echo $style;?>"><?php echo $v['inbox_userdata']['user_fullname'];?></td>
                    <td style="<?php echo $style;?>"><?php echo $v['inbox_title'];?></td>
                    
                </tr>
                <?php } ?>
            </table>
            <div class="grid-pagging border-top margin-bottom-30"></div>
        </div>
    </div>
</div>
<div class="modal" id="modalreadmail">
    <div class="modal-dialog">
        <div class="modal-header">
            <h3 class="modal-title" id="read-inbox_title">mail title</h3>
            <a href="javascript:void(0);" class="close">&times;</a>
        </div>
        <div class="modal-body no-padding">
            <div class="padding-left-15 padding-right-15 padding-top-10 padding-bottom-10 border-bottom dotted">
                <span id="read-inbox_user">to: user@userdomain.com</span>
                <span class="pull-right fg-gray small" id="read-inbox_date"></span>
            </div>
            <div id="read-inbox_content" class="padding-15" style="height:300px;overflow:auto;"></div>
            <div id="read-inbox_attachment" class=""></div>
            <div class="margin-top-10 border-top padding-top-10 padding-right-15 padding-bottom-15 text-right">
                <button data-dismiss="modal">close</button>
            </div>
        </div>
    </div> 
</div>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.grid-pagging').transpaginator({
                totalPages: <?php echo $mail['total_page'];?>,
                totalData: <?php echo $mail['total_data'];?>,
                currentPage: <?php echo $mail['page'];?>,
                pageUrl: '<?php echo BASE_URL;?>index.php?/mail/inbox/&keyword=<?php echo $mail['keyword'];?>&page='
            });
        });
        function goto(mail_id){
            $.ajax({
                url:'<?php echo BASE_URL;?>index.php?/mail/sentread/'+mail_id,
                dataType:'json',
                success:function(res){
                    $('#read-inbox_title').html(res.inbox_title);
                    $('#read-inbox_user').html('to: '+res.userdata.user_fullname);
                    $('#read-inbox_content').html(res.inbox_content);
                    $('#read-inbox_date').html(moment(res.inbox_date).format('lll'));
                    $('#modalreadmail').addClass('fadein');
                }
            });
        }
        function filter(){
            var keyword = $('#keyword').val();
            window.location.href = '<?php echo BASE_URL;?>index.php?/mail/inbox/&keyword='+keyword+'&page=1';
        }
    </script>
<?php View::inc('footer.php');?>