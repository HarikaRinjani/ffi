<?php View::inc('header.php');?>
<?php View::inc('navbarFarmer.php');?>
<?php View::inc('dashboard/dashboard.menuFarmer.php');?>

<div class="content padded">
    <div class="breadcrumb">
        <ul class="tab">
            <li class="active">
                <a href="">my inbox</a>
            </li>
            <li>
                <a href="<?php echo BASE_URL;?>index.php?/mail/sent">sent item</a>
            </li>
            <li>
                <a href="<?php echo BASE_URL;?>index.php?/mail/trash">trash</a>
            </li>
        </ul>
        <div class="pull-right">
            <ul class="breadcrumb-nav">
                <li>
                    <a href="">Dashboard</a>
                </li>
                <li>
                    <a href="">Internal Messages</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row ">
            <div class="ribbon border-bottom text-right">
                <div class="menu">
                    <a href="<?php echo BASE_URL;?>index.php?/mail/compose"><i class="ion-ios7-email-outline"></i>new mail</a>
                </div>
                <form action="javascript:void(0);" id="searchForm" onsubmit="return filter();">
                    <input type="text" placeholder="Search mail..." id="keyword" style="width:300px" value="<?php echo $mail['keyword'];?>">
                </form>
            </div>
            <table class="table">
                <tr>
                    <th>Date</th>
                    <th>Sender</th>
                    <th>Mail Title</th>
                </tr>
                <?php if($mail['total_data'] === 0){ ?>
                <tr>
                    <td colspan="3"><?php echo _('No record found');?></td>
                </tr>
                <?php } ?>
                <?php foreach($mail['records'] as $k=>$v){ 
                    $style = '';
                    if($v['inbox_read'] == 'n'){ 
                        $style = 'font-weight:bold;';
                    }
                    ?>
                <tr onclick="goto('<?php echo $v['inbox_id'];?>');">
                    <td style="<?php echo $style;?>" class="format-datetime"><?php echo $v['inbox_date'];?></td>
                    <td style="<?php echo $style;?>"><?php echo $v['user_fullname'];?></td>
                    <td style="<?php echo $style;?>"><?php echo $v['inbox_title'];?></td>
                </tr>
                <?php } ?>
            </table>
            <div class="grid-pagging border-top margin-bottom-30"></div>
        </div>
    </div>
</div>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.grid-pagging').transpaginator({
                totalPages: <?php echo $mail['total_page'];?>,
                totalData: <?php echo $mail['total_data'];?>,
                currentPage: <?php echo $mail['page'];?>,
                pageUrl: '<?php echo BASE_URL;?>index.php?/mail/inbox/&keyword=<?php echo $mail['keyword'];?>&page='
            });
        });
        function goto(mail_id){
            window.location.href = '<?php echo BASE_URL;?>index.php?/mail/read/'+mail_id;
        }
        function filter(){
            var keyword = $('#keyword').val();
            window.location.href = '<?php echo BASE_URL;?>index.php?/mail/inbox/&keyword='+keyword+'&page=1';
        }
    </script>
<?php View::inc('footer.php');?>