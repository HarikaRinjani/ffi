<?php View::inc('header.php');?>
<?php View::inc('navbarFarmer.php');?>
<?php View::inc('dashboard/dashboard.menuFarmer.php');?>
<div class="content padded">
    <div class="breadcrumb">
        <ul class="tab">
            <li class="active">
                <a href="">My Dashboard</a>
            </li>
        </ul>
        <div class="pull-right">
            <ul class="breadcrumb-nav">
                <li>
                    <a href="">Dashboard</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <h1>Selamat Datang <?php echo session::get('user_fullname');?></h1>
            </div>
            <?php if(Config::$application['is_client'] === true){ ?>
            <div class="col-md-4 padding-top-20 text-right">
                <button class="bg-purple fg-white" onclick="printTotalTransaction();"><i class="ion-ios7-printer"></i> &nbsp; PRINT TOTAL TRANSACTION</button>
            </div>
            <?php } ?>
            <div class="col-md-6 padding-top-15">
                <div class="window">
                    <div class="window-body" style="height:195px;">
                        <div class="pull-left margin-right-10">
                            <img src="<?php echo session::get('user_avatar'); ?>" class="border padding-5" alt="" style="width:80px;height:auto;"/>
                        </div>
                        <div class="padding-left-10">
                            <h3 class="no-padding no-margin"><?php echo session::get('user_fullname');?></h3>
                            <small>Aktivitas terakhir: <?php echo session::get('user_lastactivity');?></small>
                            <table class="margin-top-10">
                                <tr>
                                    <td class="padding-bottom-10">Nama Pengguna</td>
                                    <td class="padding-bottom-10 padding-left-5">:</td>
                                    <td class="padding-bottom-10 padding-left-10"><?php echo session::get('user_name');?></td>
                                </tr>
                                <tr>
                                    <td class="padding-bottom-10">Email</td>
                                    <td class="padding-bottom-10 padding-left-5">:</td>
                                    <td class="padding-bottom-10 padding-left-5"><?php echo session::get('user_email');?></td>
                                </tr>
                                <tr>
                                    <td class="padding-bottom-10">Anggota Grup</td>
                                    <td class="padding-bottom-10 padding-left-5">:</td>
                                    <td class="padding-bottom-10 padding-left-5"><?php echo Config::$application['user_group'][session::get('user_group')];?></td>
                                </tr>
                                <tr>
                                    <td class="padding-bottom-10"><?php echo _('Koperasi');?></td>
                                    <td class="padding-bottom-10 padding-left-5">:</td>
                                    <td class="padding-bottom-10 padding-left-5"><?php echo session::get('user_koperasi_nama');?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 padding-top-15">
                <table class="table table-hover border">
                    <thead>
                        <tr>
                            <th class="border-right text-center" colspan="2"><?php echo _('Cow Population');?></th>
                            <th class="border-right text-center" colspan="2">Antibiotic</th>
                            <th rowspan="2" class="text-center"><i class="ion-ios7-email-outline" style="font-size:24px;"></i></th>
                        </tr>
                        <tr>
                            <th class="border-right text-center">Lactating</th>
                            <th class="border-right text-center">Total</th>
                            <th class="border-right text-center">Status</th>
                            <th class="text-center">Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="border-right dotted"><?php echo $cow_lactating;?></td>
                            <td class="border-right dotted"><?php echo $cow_population;?></td>
                            <td class="text-center border-right dotted">
                                <?php if($cow_antibiotic === 0){ ?>
                                    <span class="bg-green fg-white padding-left-5 padding-right-5 padding-bottom-3 rounded">Negative</span>
                                <?php }else{ ?>
                                    <span class="bg-red fg-white padding-left-5 padding-right-5 padding-bottom-3 rounded">Positive</span>
                                <?php } ?>
                            </td>
                            <td class="text-center border-right dotted">
                                <?php if($cow_antibiotic === 0){ ?>
                                    -
                                <?php }else{ ?>
                                    <div>
                                        <?php echo date('d/m/Y',strtotime($cow_antibioticstartdate)).' - '.date('d/m/Y',strtotime($cow_antibioticenddate));?>
                                    </div>
                                <?php } ?>
                            </td>
                            <td class="text-center border-right dotted" style="position:relative;">
                                <a href="javascript:void(0);" onclick="$(this).next().toggleClass('fadein');">
                                    <i class="ion-ios7-email-outline" style="font-size:24px;"></i>
                                </a>
                                <div class="popover bottom" style="right:3px;top:35px;left:auto;width:200px;">
                                    <div class="arrow" style="right:10px;left:auto !important;"></div>
                                    <div class="popover-content">
                                        <?php echo $cow_antibioticremark;?>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-12 padding-top-15">
                <div class="window">
                    <div class="window-header">
                        <h3 class="window-title">Grafik Penerimaan Susu</h3>
                    </div>
                    <div class="window-body" style="">
                        <?php echo $farmer_chart; ?>
                    </div>
                </div>
            </div>
            <div class="col-md-12 padding-top-15">
                <div class="padding-bottom-40 ">
                    <h2>Kualitas Data (<?php echo date('F').'  '.$_GET['year'];?>)</h2>
                    <table class="table table-hover border">
                        <thead>
                        <tr>
                            <th class="border-right dotted" rowspan="2">Periode</th>
                            <th class="border-right dotted" rowspan="2">TPC</th>
                            <!-- <th class="border-right dotted">IBC</th> -->
                            <th class="border-right dotted" rowspan="2">FAT</th>
                            <th class="border-right dotted" rowspan="2">Protein</th>
                            <th class="border-right dotted" rowspan="2">Lactose</th>
                            <th class="border-right dotted" rowspan="2">TS</th>
                            <th class="border-right dotted" rowspan="2">FP</th>
                            <th class="border-right dotted" rowspan="2">Acidity</th>
                            <th class="border-right dotted" rowspan="2">Urea</th>
                            <th class="border-right dotted" rowspan="2">SNF</th>
                            <th class="border-right dotted" rowspan="2">STD Price</th>
                            <th class="border-right dotted" rowspan="2">Rez Penalty</th>
                            <th class="border-right dotted" rowspan="2">FP Penalty</th>
                            <th rowspan="2">Milk Price</th>
                        </tr>
                        </thead>
                        <tbody>
                            <?php foreach($periode['records'][0]['periodedetail'] as $key=>$val){ ?>
                                
                                <?php if($data['total_data'] == 0){ ?>
                                    <tr>
                                        <td colspan="100%"><?php echo _('No record found');?></td>
                                    </tr>
                                <?php }else{ ?>
                                    <?php foreach($data['records'] as $k=>$v){ ?>
                                        <tr class="tr_selector cursor-pointer" data-id="<?php echo $v[$val['periodedetail_name']]['anggota_id'];?>" id="tr<?php echo $v['anggota_id'];?>">
                                            <td class="border-right dotted">
                                                <strong>Periode <?php echo $val['periodedetail_name'];?></strong>
                                            </td>
                                            <td class="border-right dotted"><?php echo (float) $v[$val['periodedetail_name']]['qcparameter_cfu'];?></td>
                                            
                                            <!-- <td class="border-right dotted"><?php echo (float) $v[$val['periodedetail_name']]['qcparameter_ibc'];?></td> -->
                                            <td class="border-right dotted"><?php echo (float) $v[$val['periodedetail_name']]['qcparameter_fat'];?></td>
                                            <td class="border-right dotted"><?php echo (float) $v[$val['periodedetail_name']]['qcparameter_protein'];?></td>
                                            <td class="border-right dotted"><?php echo (float) $v[$val['periodedetail_name']]['qcparameter_lactose'];?></td>
                                            <td class="border-right dotted"><?php echo (float) $v[$val['periodedetail_name']]['qcparameter_ts'];?></td>
                                            <td class="border-right dotted"><?php echo (float) $v[$val['periodedetail_name']]['qcparameter_fp'];?></td>
                                            <td class="border-right dotted"><?php echo (float) $v[$val['periodedetail_name']]['qcparameter_acidity'];?></td>
                                            <td class="border-right dotted"><?php echo (float) $v[$val['periodedetail_name']]['qcparameter_urea'];?></td>
                                            <td class="border-right dotted"><?php echo (float) $v[$val['periodedetail_name']]['qcparameter_snf'];?></td>
                                            <td class="border-right dotted"><?php echo number_format($v[$val['periodedetail_name']]['hargadasar']);?></td>
                                            <td class="border-right dotted"><?php echo number_format($v[$val['periodedetail_name']]['hargarez']);?></td>
                                            <td class="border-right dotted"><?php echo number_format($v[$val['periodedetail_name']]['hargafp']);?></td>
                                            <?php $milkprice = $v[$val['periodedetail_name']]['hargadasar']+ $v[$val['periodedetail_name']]['hargarez'] + $v[$val['periodedetail_name']]['hargafp'];?>
                                            <td class=""><?php echo number_format($milkprice);?></td>
                                        </tr>
                                    <?php } ?>
                                <?php } ?>
                               
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="clearfix"></div>
            
        </div>
    </div>
</div>
<script type="text/javascript">
    var cookieEnabled=(navigator.cookieEnabled)? true : false
    function generateSampling(koperasi_id){
        $.ajax({
            url:'<?php echo BASE_URL;?>index.php?/samplingschedule/generate/&koperasi_id='+koperasi_id,
            success:function(res){
                location.reload();
            }
        });
    }
    if (typeof navigator.cookieEnabled=="undefined" && !cookieEnabled){
        document.cookie="testcookie"
        cookieEnabled=(document.cookie.indexOf("testcookie")!=-1)? true : false
    }
    if(!cookieEnabled){
        alert('PLEASE ACTIVATE BROWSER COOKIE!!!');
    }

    <?php if(session::get('user_firstuse') === 'y'){ ?>
        $.get('<?php echo BASE_URL;?>index.php?/dashboard/firstuser_confirm',function(res){
            var tip = '<div id="firstuse-wizard" onclick="$(\'#firstuse-wizard\').remove();" style="position:fixed;right:10px;top:0;z-index:9999;"><img src="<?php echo BASE_URL;?>themes/default/images/moremenu.png"></div>';
            $('body').append(tip);
        });
    <?php } ?>
</script>
<?php View::inc('footer.php');?>
