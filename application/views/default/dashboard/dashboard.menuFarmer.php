<div class="sidebar">
    <span id="resize-handler" class="hidden-xs hidden-sm ui-resizable-handle ui-resizable-e">&nbsp;</span>
    <a href="javascript:void(0);" class="visible-sm toggleSidebar"><i class="ion-ios7-more"></i></a>
    <?php View::inc('koperasilogo.php');?>
    <ul class="navigation icon">
        <?php View::inc('bookmark/bookmark.php');?>
        <li>
            <h3 class="title">Dashboard</h3>
            <a href="javascript:void(0);" class="navtoggle"><i class="ion-chevron-down"></i></a>
            <ul>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/dashboard"><i class="ion-ios7-home-outline"></i>Beranda</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/profile"><i class="ion-ios7-contact-outline"></i>Profile</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/mail"><i class="ion-ios7-email-outline"></i>Pesan Masuk</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/dashboard/logout"><i class="ion-ios7-redo"></i>Keluar</a>
                </li>
            </ul>
        </li>
    </ul>
</div>
