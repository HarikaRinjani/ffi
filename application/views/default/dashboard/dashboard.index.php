<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('dashboard/dashboard.menu.php');?>

<div class="content padded">
    <div class="breadcrumb">
        <ul class="tab">
            <li class="active">
                <a href="">My Dashboard</a>
            </li>
        </ul>
        <div class="pull-right">
            <ul class="breadcrumb-nav">
                <li>
                    <a href="">Dashboard</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8">
                <h1>Welcome <?php echo session::get('user_fullname');?></h1>
            </div>
            <?php if(Config::$application['is_client'] === true){ ?>
            <div class="col-md-4 padding-top-20 text-right">
                <button class="bg-purple fg-white" onclick="printTotalTransaction();"><i class="ion-ios7-printer"></i> &nbsp; PRINT TOTAL TRANSACTION</button>
            </div>
            <?php } ?>
            <div class="col-md-6 padding-top-15">
                <div class="window">
                    <div class="window-body" style="height:195px;">
                        <div class="pull-left margin-right-10">
                            <img src="<?php echo session::get('user_avatar'); ?>" class="border padding-5" alt="" style="width:80px;height:auto;"/>
                        </div>
                        <div class="padding-left-10">
                            <h3 class="no-padding no-margin"><?php echo session::get('user_fullname');?></h3>
                            <small>last activity: <?php echo session::get('user_lastactivity');?></small>
                            <table class="margin-top-10">
                                <tr>
                                    <td class="padding-bottom-10">Username</td>
                                    <td class="padding-bottom-10 padding-left-5">:</td>
                                    <td class="padding-bottom-10 padding-left-10"><?php echo session::get('user_name');?></td>
                                </tr>
                                <tr>
                                    <td class="padding-bottom-10">Email</td>
                                    <td class="padding-bottom-10 padding-left-5">:</td>
                                    <td class="padding-bottom-10 padding-left-5"><?php echo session::get('user_email');?></td>
                                </tr>
                                <tr>
                                    <td class="padding-bottom-10">User Group</td>
                                    <td class="padding-bottom-10 padding-left-5">:</td>
                                    <td class="padding-bottom-10 padding-left-5"><?php echo Config::$application['user_group'][session::get('user_group')];?></td>
                                </tr>
                                <tr>
                                    <td class="padding-bottom-10"><?php echo _('Cooperative');?></td>
                                    <td class="padding-bottom-10 padding-left-5">:</td>
                                    <td class="padding-bottom-10 padding-left-5"><?php echo session::get('user_koperasi_nama');?></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 padding-top-15">
                <div class="window">
                    <div class="window-header">
                        <h3 class="window-title">Notices</h3>
                    </div>
                    <div class="window-body" style="height:160px;overflow: auto;">
                        <h4>below is some issues detected by system: </h4>
                        <ol>
                            <?php foreach($notice as $k=>$v){;?>
                            <li>
                                <?php echo $v;?>
                            </li>
                            <?php } ?>
                        </ol>


                    </div>
                </div>
            </div>
            <?php if(Config::$application['is_client'] === true){ ?>
                <div class="clearfix"></div>
                <div class="col-md-12">
                    <div class="margin-top-15 margin-bottom-15 padding-top-15 border">
                        <?php echo $chart;?>
                    </div>
                </div>
            <?php } ?>
            <div class="clearfix"></div>
            <?php foreach($widget as $k=>$v){ ?>
                <div class="<?php echo $v['widget_width'];?>" id="widget<?php echo $v['widget_id'];?>">
                    <div class="window">
                        <div class="window-header">
                            <h3 class="window-title"><?php echo $v['widget_name'];?></h3>
                        </div>
                        <?php if($v['widget_display_type'] == 'table'){ ?>
                            <div class="window-body no-padding">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <?php foreach(Config::$application['widget_fields'][$v['widget_type']] as $key=>$value){
                                                echo '<th>'.$value.'</th>';
                                            } ?>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach($v['detail'] as $kk=>$row){ ?>
                                            <tr>
                                                <?php foreach(Config::$application['widget_fields'][$v['widget_type']] as $key=>$value){
                                                    echo '<td>'.fn::limitChar(strip_tags($row[$key]),20).'</td>';
                                                } ?>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <?php }else{ ?>
                            <div class="window-body">
                                <?php echo $v['detail'];?>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    var cookieEnabled=(navigator.cookieEnabled)? true : false
    function generateSampling(koperasi_id){
        $.ajax({
            url:'<?php echo BASE_URL;?>index.php?/samplingschedule/generate/&koperasi_id='+koperasi_id,
            success:function(res){
                location.reload();
            }
        });
    }
    if (typeof navigator.cookieEnabled=="undefined" && !cookieEnabled){
        document.cookie="testcookie"
        cookieEnabled=(document.cookie.indexOf("testcookie")!=-1)? true : false
    }
    if(!cookieEnabled){
        alert('PLEASE ACTIVATE BROWSER COOKIE!!!');
    }

    <?php if(session::get('user_firstuse') === 'y'){ ?>
        $.get('<?php echo BASE_URL;?>index.php?/dashboard/firstuser_confirm',function(res){
            var tip = '<div id="firstuse-wizard" onclick="$(\'#firstuse-wizard\').remove();" style="position:fixed;right:10px;top:0;z-index:9999;"><img src="<?php echo BASE_URL;?>themes/default/images/moremenu.png"></div>';
            $('body').append(tip);
        });
    <?php } ?>
</script>
<?php View::inc('footer.php');?>
