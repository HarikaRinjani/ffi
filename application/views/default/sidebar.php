<div class="sidebar">
    <span id="resize-handler" class="hidden-xs hidden-sm ui-resizable-handle ui-resizable-e">&nbsp;</span>
    <a href="javascript:void(0);" class="visible-sm toggleSidebar"><i class="ion-ios7-more"></i></a>
    
    <ul class="navigation icon">
        <?php View::inc('bookmark/bookmark.php');?>
        <li>
            <a href="javascript:void(0);" class="navtoggle"><i class="ion-chevron-right"></i></a>
            <h3 class="title">Safety Plan</h3>
            <ul class="">
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/risks" title="Potential Hazard"><i class="ion-ios7-bolt-outline"></i>Potential Hazard</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/identifikasi" title="Hazard Identification"><i class="ion-ios7-copy-outline"></i>Hazard Identification</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/penilaian" title="Risk Analysis"><i class="ion-ios7-compose-outline"></i>Risk Analysis</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/pengendalian" title="Development and Controlling"><i class="ion-ios7-browsers-outline"></i>Development & Controlling</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:void(0);" class="navtoggle"><i class="ion-chevron-right"></i></a>
            <h3 class="title">Safety Application</h3>
            <ul class="">
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/safetyawareness" title="Safety Awareness"><i class="ion-ios7-mic-outline"></i>Safety Awareness</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/safetytalk" title="Safety Talk"><i class="ion-ios7-volume-high"></i>Safety Talk</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/safetymeeting" title="Safety Meeting"><i class="ion-ios7-chatboxes-outline"></i>Safety Meeting</a>
                </li>
<!--                <li>-->
<!--                    <a href="--><?php //echo BASE_URL;?><!--index.php?/training" title="Safety Training"><i class="ion-mic-c"></i>Safety Training</a>-->
<!--                </li>-->
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/refresh" title="Safety Refresh"><i class="ion-ios7-contact-outline"></i>Safety Refresh</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/emergencyresponse" title="Emergency Response and Simulation"><i class="ion-ios7-telephone-outline"></i>Emergency Response & Simulation</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/accidentreport" title="Accident Record"><i class="ion-ios7-medkit-outline"></i>Accident Records</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/inspeksi" title="Safety Inspection"><i class="ion-ios7-checkmark-outline"></i>Inspection</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/audit" title="Safety Audit"><i class="ion-ios7-paper-outline"></i>Audit</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:void(0);" class="navtoggle"><i class="ion-chevron-right"></i></a>
            <h3 class="title">Safety Monev</h3>
            <ul class="">
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/program" title="Program"><i class="ion-ios7-navigate-outline"></i>Program</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/profile"><i class="ion-ios7-compose-outline"></i>Realization</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:void(0);" class="navtoggle"><i class="ion-chevron-right"></i></a>
            <h3 class="title">Safety Improvement</h3>
            <ul class="">
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/dashboard"><i class="ion-ios7-home-outline"></i>Home</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/profile"><i class="ion-ios7-contact-outline"></i>Profile</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/mail"><i class="ion-ios7-email-outline"></i>Internal Message</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/dashboard/logout"><i class="ion-ios7-redo"></i>Sign out</a>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:void(0);" class="navtoggle"><i class="ion-chevron-right"></i></a>
            <h3 class="title">System Standard (Comparison)</h3>
            <ul class="">
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/dashboard"><i class="ion-ios7-home-outline"></i>Home</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/profile"><i class="ion-ios7-contact-outline"></i>Profile</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/mail"><i class="ion-ios7-email-outline"></i>Internal Message</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/dashboard/logout"><i class="ion-ios7-redo"></i>Sign out</a>
                </li>
            </ul>
        </li>
    </ul>
</div>