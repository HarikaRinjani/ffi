<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('settings/settings.menu.php');?>

<div class="content padded">
    <div class="breadcrumb">
        <ul class="tab">
            <li >
                <a href="javascript:void(0);" id="addnew-kelompok">add new</a>
            </li>
            <li class="active">
                <a href="<?php echo BASE_URL;?>index.php?/kelompok/setting" disabled="disabled">Farmers Group</a>
            </li>
        </ul>
        <div class="pull-right">
            <ul class="breadcrumb-nav">
                <li>
                    <a href="">Apps Settings</a>
                </li>
                <li>
                    <a href=""><?php echo _('Farmers Group');?></a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row padding-bottom-40">
            <div class="ribbon border-bottom text-right">
                <div class="menu">
                    <a href="javascript:void(0);" id="__edit" class="fg-lightgray editbtn"><i class="ion-ios7-compose-outline"></i>edit</a>
                    <a href="javascript:void(0);" id="__delete" class="fg-lightgray editbtn"><i class="ion-ios7-trash-outline"></i>delete</a>
                </div>
                <form action="javascript:void(0);" id="searchForm" onsubmit="return filter();">
                    <?php if(!in_array(session::get('user_group'),Config::$application['super_group'])){ ?>
                    <input type="hidden" name="koperasi" id="koperasi" value="<?php echo session::get('user_koperasi');?>">
                    <?php }else{ ?>
                    <select name="koperasi" id="koperasi" onchange="return filter();">
                        <option value="">All</option>
                        <?php foreach($koperasi['records'] as $k=>$v){ ?>
                            <option value="<?php echo $v['koperasi_id'];?>" <?php if($v['koperasi_id'] === $_GET['koperasi']){ echo 'selected="selected"'; } ?>><?php echo $v['koperasi_nama'];?></option>
                        <?php } ?>
                    </select>
                    <?php } ?>
                    <input type="text" placeholder="Search..." id="keyword" style="width:200px" value="<?php echo $kelompok['keyword'];?>">
                </form>
            </div>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th style="width:24px;">#</th>
                    <th><?php echo _('Cooperative');?></th>
                    <th><?php echo _('Collection Point');?></th>
                    <th><?php echo _('Group Name');?></th>
                    <th><?php echo _('Status');?></th>
                </tr>
                </thead>
                <tbody>
                <?php if($kelompok['total_data'] == 0){ ?>
                    <tr>
                        <td colspan="5"><?php echo _('No record found');?></td>
                    </tr>
                <?php }else{ ?>
                    <?php foreach($kelompok['records'] as $k=>$v){ ?>
                        <tr class="tr_selector cursor-pointer" data-id="<?php echo $v['kelompok_id'];?>" id="tr<?php echo $v['kelompok_id'];?>">
                            <td>
                                <?php
                                if($kelompok['page'] == 1){
                                    echo ($k+1);
                                }else{
                                    echo ($k + 1 + ($kelompok['limit'] * ($kelompok['page'] - 1)));
                                }
                                ?>
                            </td>
                            <td><?php echo $v['koperasi_nama'];?></td>
                            <td><?php echo $v['tpk_nama'];?></td>
                            <td><?php echo $v['kelompok_nama'];?></td>
                            <td><span class="status"><?php echo $v['kelompok_status'];?></span></td>
                        </tr>
                    <?php } ?>
                <?php } ?>
                </tbody>
            </table>
            <div class="grid-pagging border-top margin-bottom-30"></div>
        </div>
    </div>
</div>
<div class="modal draggable" id="modal-add-department">
    <div class="modal-dialog modal-sm">
        <div class="modal-header">
            <h3 class="modal-title">Add new Group</h3>
            <a href="javascript:void(0);" class="close">&times;</a>
        </div>
        <div class="modal-body">
            <form action="<?php echo BASE_URL;?>index.php?/kelompok/setting/save" method="post">
                <label for="add-department_name" class=""><?php echo _('Group Name');?></label>
                <div class="">
                    <input type="text" id="add-department_name" class="input-block" name="kelompok_nama" required/>
                </div>
                <!-- <label for="add-tpk_kode" class="padding-top-5"><?php echo _('Group Code');?></label>
                <div class="">
                    <input type="text" id="add-tpk_kode" class="input-block" maxlength="3" name="kelompok_kode" required/>
                </div> -->
                <label class="margin-top-5" for="add-kelompok_koperasi"><?php echo _('Cooperative');?></label>
                <div class="">
                  <select name="kelompok_koperasi" id="add-kelompok_koperasi" class="koperasi input-block" required >
                    <option value="">loading koperasi data...</option>
                  </select>
                </div>
                <label class="margin-top-5" for=""><?php echo _('Collection Point');?></label>
                <div class="">
                    <select name="kelompok_tpk" id="add-kelompok_tpk" class="input-block">
                        <option value="">loading tpk data...</option>
                    </select>
                </div>
                <label class="margin-top-5" for="add-department-status">Status</label>
                <div class="">
                    <select name="kelompok_status" id="kelompok_status" class="input-block status"></select>
                </div>
                <div class="padding-top-25 text-right">
                    <button type="button" data-dismiss="modal">close</button>
                    <button type="submit" class="default">save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal draggable" id="modal-edit-department">
    <div class="modal-dialog modal-sm">
        <div class="modal-header">
            <h3 class="modal-title">Edit Group</h3>
            <a href="javascript:void(0);" class="close">&times;</a>
        </div>
        <div class="modal-body">
            <form action="<?php echo BASE_URL;?>index.php?/kelompok/setting/save" method="post">
                <input type="hidden" name="kelompok_id" id="edit-kelompok_id"/>
                <label for="add-department_name" class=""><?php echo _('Group Name');?></label>
                <div class="">
                    <input type="text" id="edit-kelompok_nama" class="input-block" name="kelompok_nama" required/>
                </div>
               <!--  <label for="add-tpk_kode" class="padding-top-5"><?php echo _('Group Code');?></label>
                <div class="">
                    <input type="text" id="edit-kelompok_kode" class="input-block" name="kelompok_kode" required/>
                </div> -->
                <label class="margin-top-5" for="edit-kelompok_koperasi"><?php echo _('Cooperative');?></label>
                <div class="">
                    <select name="kelompok_koperasi" id="edit-kelompok_koperasi" class="koperasi input-block">
                        <option value="">loading koperasi data...</option>
                    </select>
                </div>
                <label class="margin-top-5" for=""><?php echo _('Collection Point');?></label>
                <div class="">
                    <select name="kelompok_tpk" id="edit-kelompok_tpk" class="input-block">
                        <option value="">loading tpk data...</option>
                    </select>
                </div>
                <label class="margin-top-5" for="add-department-status">Status</label>
                <div class="">
                    <select name="kelompok_status" id="edit-kelompok_status" class="input-block status"></select>
                </div>
                <div class="padding-top-25 text-right">
                    <button type="button" data-dismiss="modal">close</button>
                    <button type="submit" class="default">save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.grid-pagging').transpaginator({
            totalPages: <?php echo $kelompok['total_page'];?>,
            totalData: <?php echo $kelompok['total_data'];?>,
            currentPage: <?php echo $kelompok['page'];?>,
            pageUrl: '<?php echo BASE_URL;?>index.php?/kelompok/setting/&koperasi=<?php echo $_GET['koperasi'];?>&keyword=<?php echo $kelompok['keyword'];?>&page='
        });
        $('.tr_selector').on('click',function(){
            var id = $(this).attr('data-id');
            $('.editbtn').removeClass('fg-lightgray').removeAttr('disabled').attr('data-id',id);
            $('.tr_selector').removeClass('active');
            $(this).addClass('active');
        });
        $(document).on('change','#add-kelompok_koperasi',function(){
            var id = $(this).val();
            $.ajax({
                url:'<?php echo BASE_URL;?>index.php?/tpk/findByKoperasi/'+id,
                dataType:'json',
                success:function(res){
                    var opsi = '';
                    for(var k in res.records){
                        opsi += '<option value="'+res.records[k].tpk_id+'">'+res.records[k].tpk_nama+'</option>';
                    }
                    $('#add-kelompok_tpk').html(opsi);
                }
            });
        });
        $(document).on('change','#edit-kelompok_koperasi',function(){
            var id = $(this).val();
            $.ajax({
                url:'<?php echo BASE_URL;?>index.php?/tpk/findByKoperasi/'+id,
                dataType:'json',
                success:function(res){
                    var opsi = '';
                    for(var k in res.records){
                        opsi += '<option value="'+res.records[k].tpk_id+'">'+res.records[k].tpk_nama+'</option>';
                    }
                    $('#edit-kelompok_tpk').html(opsi);
                }
            });
        });
        $('#__edit').on('click',function(){
            var id = $(this).attr('data-id');
            if(typeof id !== 'undefined' && id !== ''){
                $.ajax({
                    url:'<?php echo BASE_URL;?>index.php?/kelompok/setting/edit/'+id,
                    dataType:'json',
                    success:function(res){
                        $('#edit-kelompok_id').val(res.kelompok_id);
                        $('#edit-kelompok_nama').val(res.kelompok_nama);
                        $('#edit-kelompok_status').val(res.kelompok_status);
                        $('#edit-kelompok_kode').val(res.kelompok_kode);
                        createSelectTpk(res.kelompok_koperasi,res.tpk_id);
                        $('#modal-edit-department').addClass('fadein');
                    }
                });
            }
        });
		$('#addnew-kelompok').on('click',function(){
			var id = $('#add-kelompok_koperasi').val();
			$.ajax({
				url:'<?php echo BASE_URL;?>index.php?/tpk/findByKoperasi/'+id,
				dataType:'json',
				success:function(res){
					var opsi = '';
					for(var k in res.records){
						opsi += '<option value="'+res.records[k].tpk_id+'">'+res.records[k].tpk_nama+'</option>';
					}
					//console.log(opsi);
					$('#add-kelompok_tpk').html(opsi);
					$('#modal-add-department').addClass('fadein');
				}
			});
		});
        $('#__delete').on('click',function(){
            var id = $(this).attr('data-id');
            if(typeof id !== 'undefined' && id !== ''){
                if(confirm('<?php echo _('Are you sure want to delete this data?');?>')){
                    $.ajax({
                        url:'<?php echo BASE_URL;?>index.php?/kelompok/setting/delete',
                        type:'post',
                        data:{
                            kelompok_id:id
                        },
                        success:function(){
                            location.reload();
                        }
                    });
                }
            }
        });


    });

    function createSelectTpk(koperasi_id,tpk_id){
        $.ajax({
            url:'<?php echo BASE_URL;?>index.php?/tpk/findByKoperasi/'+koperasi_id,
            dataType:'json',
            success:function(res){
                var opsi = '';
                for(var k in res.records){
                    opsi += '<option value="'+res.records[k].tpk_id+'" ';
                    if(typeof tpk_id !== 'undefined' && tpk_id === res.records[k].tpk_id){
                        opsi += ' selected="selected"';
                    }
                    opsi += '>'+res.records[k].tpk_nama+'</option>';
                }
                //console.log(opsi);
                $('#edit-kelompok_tpk').html(opsi);
            }
        });
    }
    function initAddKelompokTpk(){
        var id = $('#add-kelompok_koperasi').val();
        $.ajax({
            url:'<?php echo BASE_URL;?>index.php?/tpk/findByKoperasi/'+id,
            dataType:'json',
            success:function(res){
                var opsi = '';
                for(var k in res.records){
                    opsi += '<option value="'+res.records[k].tpk_id+'">'+res.records[k].tpk_nama+'</option>';
                }
                //console.log(opsi);
                $('#add-kelompok_tpk').html(opsi);
            }
        });
    }
    function filter(){
        var keyword = $('#keyword').val();
        var koperasi= $('#koperasi').val();
        window.location.href = '<?php echo BASE_URL;?>index.php?/kelompok/setting/&koperasi='+koperasi+'&keyword='+keyword+'&page=1';
    }
</script>
<?php View::inc('footer.php');?>
