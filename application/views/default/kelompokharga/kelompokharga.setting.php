<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('settings/settings.menu.php');?>

<div class="content padded">
    <div class="breadcrumb">
        <ul class="tab">
            <li >
                <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-add-department">add new</a>
            </li>
            <li class="active">
                <a href="<?php echo BASE_URL;?>index.php?/kelompokharga/setting" disabled="disabled">Kelompok Harga</a>
            </li>
        </ul>
        <div class="pull-right">
            <ul class="breadcrumb-nav">
                <li>
                    <a href="">Apps Settings</a>
                </li>
                <li>
                    <a href="">Kelompok</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row padding-bottom-40">
            <div class="ribbon padding-left-15 padding-right-15 padding-bottom-10 padding-top-10 border-bottom text-right">
                <h1 class="pull-left fg-gray" style="margin-top:-10px;">
                    <a href="" class="fg-gray padding-right-10">lists</a>
                    <a href="javascript:void(0);" id="__edit" class="padding-right-10 fg-lightgray editbtn">edit</a>
                    <a href="javascript:void(0);" id="__delete" class="padding-right-10 fg-lightgray editbtn">delete</a>
                </h1>
                <form action="javascript:void(0);" id="searchForm" onsubmit="return filter();">
                    <input type="text" placeholder="Search..." id="keyword" style="width:200px" value="<?php echo $kelompokharga['keyword'];?>">
                </form>
            </div>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th style="width:24px;">#</th>
                    <th><?php echo _('Koperasi');?></th>
                    <th><?php echo _('Kode');?></th>
                    <th><?php echo _('Nama');?></th>
                    <th><?php echo _('TPK');?></th>
                    <th><?php echo _('Kelompok');?></th>
                    <th><?php echo _('Status');?></th>
                </tr>
                </thead>
                <tbody>
                <?php if($kelompokharga['total_data'] == 0){ ?>
                    <tr>
                        <td colspan="7"><?php echo _('No record found');?></td>
                    </tr>
                <?php }else{ ?>
                    <?php foreach($kelompokharga['records'] as $k=>$v){ ?>
                        <tr class="tr_selector cursor-pointer" data-id="<?php echo $v['kelompokharga_id'];?>" id="tr<?php echo $v['kelompokharga_id'];?>">
                            <td>
                                <?php
                                if($kelompokharga['page'] == 1){
                                    echo ($k+1);
                                }else{
                                    echo ($k + 1 + $kelompokharga['limit']);
                                }
                                ?>
                            </td>
                            <td><?php echo $v['koperasi_nama'];?></td>
                            <td><?php echo $v['kelompokharga_kode'];?></td>
                            <td><?php echo $v['kelompokharga_nama'];?></td>
                            <td><?php echo $v['tpk_nama'];?></td>
                            <td><?php echo $v['kelompok_nama'];?></td>
                            <td><span class="status"><?php echo $v['kelompokharga_status'];?></span></td>
                        </tr>
                    <?php } ?>
                <?php } ?>
                </tbody>
            </table>
            <div class="grid-pagging border-top margin-bottom-30"></div>
        </div>
    </div>
</div>
<div class="modal draggable" id="modal-add-department">
    <div class="modal-dialog">
        <div class="modal-header">
            <h3 class="modal-title">Add Kelompok Harga</h3>
            <a href="javascript:void(0);" class="close">&times;</a>
        </div>
        <div class="modal-body">
            <form action="<?php echo BASE_URL;?>index.php?/kelompokharga/setting/save" method="post">
                <div class="form-group">
                    <label for="add-department_name" class="col-md-5">Nama Kelompok Harga</label>
                    <div class="col-md-7">
                        <input type="text" id="add-kelompokharga_name" class="input-block" name="kelompokharga_nama" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="add-tpk_kode" class="col-md-5">Kode Kelompok Harga</label>
                    <div class="col-md-7">
                        <input type="text" id="add-kelompokharga_kode" class="input-block" name="kelompokharga_kode" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-5" for="">Koperasi</label>
                    <div class="col-md-7">
                        <select name="kelompokharga_koperasi" id="add-kelompokharga_koperasi" class="koperasi input-block">
                            <option value="">loading koperasi data...</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-5" for="">TPK</label>
                    <div class="col-md-7">
                        <select name="kelompokharga_tpk" id="add-kelompokharga_tpk" class="input-block">
                            <option value="">loading tpk data...</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-5" for="">Kelompok</label>
                    <div class="col-md-7">
                        <select name="kelompokharga_kelompok" id="add-kelompok_id" class="input-block">
                            <option value="">please select TPK first...</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="add-tpk_kode" class="col-md-5">Harga Dasar</label>
                    <div class="col-md-7">
                        <input type="text" id="add-kelompokharga_hargadasar" class="input-block" onkeydown="return validateNumber(event);" name="kelompokharga_hargadasar" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-5" for="add-department-status">Status</label>
                    <div class="col-md-7">
                        <select name="kelompokharga_status" id="add-kelompokharga_status" class="input-block status"></select>
                    </div>
                </div>

                <div class="padding-top-25 text-right">
                    <button type="button" data-dismiss="modal">close</button>
                    <button type="submit" class="default">save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal draggable" id="modal-edit-department">
    <div class="modal-dialog">
        <div class="modal-header">
            <h3 class="modal-title">Edit Kelompok Harga</h3>
            <a href="javascript:void(0);" class="close">&times;</a>
        </div>
        <div class="modal-body">
            <form action="<?php echo BASE_URL;?>index.php?/kelompokharga/setting/save" method="post">
                <input type="hidden" name="kelompokharga_id" id="edit-kelompokharga_id"/>
                <div class="form-group">
                    <label for="edit-department_name" class="col-md-5">Nama Kelompok Harga</label>
                    <div class="col-md-7">
                        <input type="text" id="edit-kelompokharga_nama" class="input-block" name="kelompokharga_nama" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit-kelompokharga_kode" class="col-md-5">Kode Kelompok Harga</label>
                    <div class="col-md-7">
                        <input type="text" id="edit-kelompokharga_kode" class="input-block" name="kelompokharga_kode" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-5" for="">Koperasi</label>
                    <div class="col-md-7">
                        <select name="kelompokharga_koperasi" id="edit-kelompokharga_koperasi" class="koperasi input-block">
                            <option value="">loading tpk data...</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-5" for="">TPK</label>
                    <div class="col-md-7">
                        <select name="kelompokharga_tpk" id="edit-kelompokharga_tpk" class="tpk input-block">
                            <option value="">loading tpk data...</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-5" for="">Kelompok</label>
                    <div class="col-md-7">
                        <select name="kelompokharga_kelompok" id="edit-kelompok_id" class="input-block">
                            <option value="">please select TPK first...</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit-kelompokharga_hargadasar" class="col-md-5">Harga Dasar</label>
                    <div class="col-md-7">
                        <input type="text" id="edit-kelompokharga_hargadasar" class="input-block" onkeydown="return validateNumber(event);" name="kelompokharga_hargadasar" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-5" for="edit-department-status">Status</label>
                    <div class="col-md-7">
                        <select name="kelompokharga_status" id="edit-kelompokharga_status" class="input-block status"></select>
                    </div>
                </div>

                <div class="padding-top-25 text-right">
                    <button type="button" data-dismiss="modal">close</button>
                    <button type="submit" class="default">save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.grid-pagging').transpaginator({
            totalPages: <?php echo $kelompokharga['total_page'];?>,
            currentPage: <?php echo $kelompokharga['page'];?>,
            pageUrl: '<?php echo BASE_URL;?>index.php?/kelompokharga/setting/&keyword=<?php echo $kelompokharga['keyword'];?>&page='
        });
        $('.tr_selector').on('click',function(){
            var id = $(this).attr('data-id');
            $('.editbtn').removeClass('fg-lightgray').removeAttr('disabled').attr('data-id',id);
            $('.tr_selector').removeClass('active');
            $(this).addClass('active');
        });
        $(document).on('change','#add-kelompokharga_koperasi',function(){
            var koperasi_id = $(this).val();
            initTpkSelect('#add-kelompokharga_tpk',koperasi_id);
        });
        $(document).on('change','#add-kelompokharga_tpk',function(){
            var tpk_id = $(this).val();
            initKelompokSelect('#add-kelompok_id',tpk_id);
        });
        $(document).on('change','#edit-kelompokharga_tpk',function(){
            var tpk_id = $(this).val();
            initKelompokSelect('#edit-kelompok_id',tpk_id);
        });
        $('#__edit').on('click',function(){
            var id = $(this).attr('data-id');
            if(typeof id !== 'undefined' && id !== ''){
                $.ajax({
                    url:'<?php echo BASE_URL;?>index.php?/kelompokharga/setting/edit/'+id,
                    dataType:'json',
                    success:function(res){
                        $('#edit-kelompokharga_id').val(res.kelompokharga_id);
                        $('#edit-kelompokharga_nama').val(res.kelompokharga_nama);
                        $('#edit-kelompokharga_status').val(res.kelompokharga_status);
                        $('#edit-kelompokharga_kode').val(res.kelompokharga_kode);
                        $('#edit-kelompokharga_hargadasar').val(res.kelompokharga_hargadasar);
                        $('#edit-kelompokharga_koperasi').val(res.kelompokharga_koperasi);
                        initTpkSelect('#edit-kelompokharga_tpk',res.kelompokharga_koperasi,res.kelompokharga_tpk);
                        initKelompokSelect('#edit-kelompok_id',res.kelompokharga_tpk,res.kelompok_id);
                        $('#modal-edit-department').addClass('fadein');
                    }
                });
            }
        });

        $('#__delete').on('click',function(){
            var id = $(this).attr('data-id');
            if(typeof id !== 'undefined' && id !== ''){
                if(confirm('<?php echo _('Are you sure want to delete this data?');?>')){
                    $.ajax({
                        url:'<?php echo BASE_URL;?>index.php?/kelompokharga/setting/delete',
                        type:'post',
                        data:{
                            kelompokharga_id:id
                        },
                        success:function(){
                            location.reload();
                        }
                    });
                }
            }
        });

    });
    $(window).load(function(){
        // Kasi delay biar select TPK nya load dulu
        delay(function(){
            var koperasi_id = $('#add-kelompokharga_koperasi').val();
            $.ajax({
                url:'<?php echo BASE_URL;?>index.php?/tpk/findByKoperasi/'+koperasi_id,
                dataType:'json',
                success:function(res){
                    var opt = '';
                    var data = res.records;

                    for(var k in data){
                        opt += '<option value="'+data[k].tpk_id+'"';
                        opt += ' data-tpk_kode="'+data[k].tpk_kode+'"';

                        opt += ' data-tpk_id="'+data[k].tpk_id+'"';
                        opt += '">'+data[k].tpk_nama+'</option>';
                    }
                    $('#add-kelompokharga_tpk').html(opt);
                    initKelompokSelect('#add-kelompok_id',data[0].tpk_id);
                }
            });
        },500);


    });
    function initKelompokSelect(target,tpk_id,value){
        $.ajax({
            url:'<?php echo BASE_URL;?>index.php?/kelompok/findByTPK/'+tpk_id,
            dataType:'json',
            success:function(res){
                var opt = '';
                var data = res.records;
                if(typeof value === 'undefined'){
                    value = '';
                }
                for(var k in data){
                    opt += '<option value="'+data[k].kelompok_id+'"';
                    opt += ' data-kelompok_kode="'+data[k].kelompok_kode+'"';
                    if(data[k].kelompok_id === value){
                        opt += ' selected="selected"';
                    }
                    opt += ' data-tpk_id="'+data[k].tpk_id+'"';
                    opt += '">'+data[k].kelompok_nama+'</option>';
                }
                $(target).html(opt);
            }
        });
    }
    function initTpkSelect(target,koperasi_id,value){
        $.ajax({
            url:'<?php echo BASE_URL;?>index.php?/tpk/findByKoperasi/'+koperasi_id,
            dataType:'json',
            success:function(res){
                var opt = '';
                var data = res.records;
                if(typeof value === 'undefined'){
                    value = '';
                }
                for(var k in data){
                    opt += '<option value="'+data[k].tpk_id+'"';
                    opt += ' data-tpk_kode="'+data[k].tpk_kode+'"';
                    if(data[k].tpk_id === value){
                        opt += ' selected="selected"';
                    }
                    opt += ' data-tpk_id="'+data[k].tpk_id+'"';
                    opt += '">'+data[k].tpk_nama+'</option>';
                }
                $(target).html(opt);
            }
        });
    }
    function filter(){
        var keyword = $('#keyword').val();
        window.location.href = '<?php echo BASE_URL;?>index.php?/kelompokharga/setting/&keyword='+keyword+'&page=1';
    }
</script>
<?php View::inc('footer.php');?>
