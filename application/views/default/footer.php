</div>
<audio id="chatAudio" style="width:1px;height:1px;"><source src="<?php echo BASE_URL;?>themes/default/sound/notify.ogg" type="audio/ogg"><source src="<?php echo BASE_URL;?>themes/default/sound/notify.mp3" type="audio/mpeg"><source src="<?php echo BASE_URL;?>themes/default/sound/notify.wav" type="audio/wav"></audio>
<div class="modal drop" id="modal-sinkronisasi">
    <div class="modal-dialog">
        <div class="modal-header">
            <h3 class="modal-title">Synchronize data</h3>
        </div>
        <div class="modal-body">
            <div class="" id="modal-sync-status" style="min-height:100px;">
                <label >Click start to begin synchronize data or cancel to abort</label>
            </div>
            <div class="padding-top-10 text-right">
            	<button class="" data-dismiss="modal" id="modal-sync-abort">cancel</button>
            	<button class="default" id="modal-sync-btn">Start</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$('#modal-sync-btn').click(function(){
			$.ajax({
				url:'<?php echo BASE_URL;?>index.php?/sync/transaksi/',
				beforeSend:function(){
					$('#modal-sync-abort').hide();
					$('#modal-sync-status').html('<p>Synchronizing Milk Collection Point Data. </p><p>Please do not close or reload window</p>');
					$('#modal-sync-btn').attr('disabled','disabled');
				},
				success:function(res){
					if(res === 'success'){
						$('#modal-sync-status').append('<p>Milk Collection Point Data synchronized.</p><p>Performing System Database Update</p>')
						$.ajax({
							url:'<?php echo BASE_URL;?>index.php?/sync/updatedb/',
							success:function(res){
								$('#modal-sync-status').append('<p>'+res+'</p>');
								$('#modal-sync-abort').html('close').show();
								$('#modal-sync-btn').hide();
							}
						});
					}
				}
			});
		});
	});
</script>
<?php if(session::check('errorMsg')){;?>
<script type="text/javascript">
    $(document).ready(function(){
        notification('<?php echo session::get('errorMsg');?>');
    });
</script>
<?php
session::set('errorMsg','');
} ?>
<script type="text/javascript" src="<?php echo BASE_URL;?>themes/default/js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL;?>themes/default/js/desktop-notify-min.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL;?>themes/default/js/paginator.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL;?>themes/default/js/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL;?>themes/default/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL;?>themes/default/jqueryui/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL;?>themes/default/timepicker/dist/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL;?>themes/default/timepicker/dist/jquery-ui-sliderAccess.js"></script>
</body>
</html>
<!-- execution time <?php echo Microtime::microtime_stop();?> -->
