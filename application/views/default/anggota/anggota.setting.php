<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('settings/settings.menu.php');?>
<div class="content padded" xmlns="http://www.w3.org/1999/html">
    <div class="breadcrumb">
        <ul class="tab">
            <li class="active">
                <a href="<?php echo BASE_URL;?>index.php?/anggota/setting" disabled="disabled">farmers data</a>
            </li>
        </ul>
        <div class="pull-right">
            <ul class="breadcrumb-nav">
                <li>
                    <a href="">Apps Settings</a>
                </li>
                <li>
                    <a href="">Farmers</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="ribbon text-right">
                <div class="menu">
                    <a href="javascript:void(0);" class="add" id="__add"><i class="ion-ios7-plus-empty"></i>add new</a>
                    <a href="javascript:void(0);" id="__edit" class="edit fg-lightgray editbtn"><i class="ion-ios7-compose-outline"></i>edit</a>
                    <a href="javascript:void(0);" id="__delete" class="delete fg-lightgray editbtn"><i class="ion-ios7-trash-outline"></i>delete</a>
                    <a href="<?php echo BASE_URL;?>index.php?/anggota/setting/generateUser"><i class="ion-ios7-loop"></i> Generate User Data</a>
                </div>
                <!-- <h1 class="pull-left fg-gray" style="margin-top:-10px;">
                    <a href="" class="fg-gray padding-right-10">lists</a>
                    <a href="javascript:void(0);" id="__edit" class="padding-right-10 fg-lightgray editbtn">edit</a>
                    <a href="javascript:void(0);" id="__delete" class="padding-right-10 fg-lightgray editbtn">delete</a>
                </h1> -->
                <form action="javascript:void(0);" id="searchForm" onsubmit="return filter();">
                    <?php if(!in_array(session::get('user_group'),Config::$application['super_group'])){ ?>
                    <input type="hidden" name="koperasi" id="koperasi" value="<?php echo session::get('user_koperasi');?>">
                    <?php }else{ ?>
                    <select name="koperasi" id="koperasi" onchange="return filter();">
                        <option value="">All</option>
                        <?php foreach($koperasi['records'] as $k=>$v){ ?>
                            <option value="<?php echo $v['koperasi_id'];?>" <?php if($v['koperasi_id'] === $_GET['koperasi']){ echo 'selected="selected"'; } ?>><?php echo $v['koperasi_nama'];?></option>
                        <?php } ?>
                    </select>
                    <?php } ?>
                    <input type="text" placeholder="Search..." id="keyword" style="width:200px" value="<?php echo $anggota['keyword'];?>">
                </form>
            </div>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th style="width:24px;">#</th>
                    <th><?php echo _('Farmer ID');?></th>
                    <th><?php echo _('Card Number');?></th>
                    <th><?php echo _('Name');?></th>
                    <th><?php echo _('Mobile Phone');?></th>
                    <th><?php echo _('Address');?></th>
                    <th><?php echo _('Cooperative');?></th>
                    <th><?php echo _('Collection Point');?></th>
                    <th><?php echo _('Group');?></th>
                    <th><?php echo _('Status');?></th>
                </tr>
                </thead>
                <tbody>
                <?php if($anggota['total_data'] == 0){ ?>
                    <tr>
                        <td colspan="9"><?php echo _('No record found');?></td>
                    </tr>
                <?php }else{ ?>
                    <?php foreach($anggota['records'] as $k=>$v){ ?>
                        <tr class="tr_selector cursor-pointer" data-id="<?php echo $v['anggota_id'];?>" id="tr<?php echo $v['anggota_id'];?>">
                            <td>
                                <?php
                                if($anggota['page'] == 1){
                                    echo ($k+1);
                                }else{
                                    echo ($k + 1 + ($anggota['limit'] * ($anggota['page'] - 1)));
                                }
                                if($v['anggota_photo'] == '' || $v['anggota_photo'] == null){
                                    if(file_exists(Config::$application['file_path'].'images'.DS.'PASFOTO'.DS.$v['anggota_nomor'].'.JPG')){
                                        $photo = 'index.php?/load/file/'.urlencode('images/PASFOTO/'.$v['anggota_nomor'].'.JPG');
                                    }else{
                                        $photo = 'index.php?/load/file/'.urlencode('images/avatar.jpg');
                                    }
                                }else{
                                    $photo = $v['anggota_photo'];
                                }
                                ?>
                            </td>
                            <td><?php echo $v['anggota_nomor'];?></td>
                            <td><?php echo $v['card_number'];?></td>
                            <td><img src="<?php echo $photo; ?>" alt="" style="float:left;margin-right:5px;width:28px;height:28px;"/><?php echo $v['anggota_nama'];?></td>
                            <td><?php echo $v['anggota_nohp'];?></td>
                            <td><?php echo $v['anggota_alamat'];?></td>
                            <td><?php echo $v['koperasi_nama'];?></td>
                            <td><?php echo $v['tpk_nama'];?></td>
                            <td><?php echo $v['kelompok_nama'];?></td>
                            <td><span class="status"><?php echo $v['anggota_status'];?></span></td>
                        </tr>
                    <?php } ?>
                <?php } ?>
                </tbody>
            </table>
            <div class="grid-pagging border-top margin-bottom-30"></div>
        </div>
    </div>
</div>
<div class="modal draggable-header" id="modal-add-anggota">
    <div class="modal-dialog">
        <div class="modal-header">
            <h3 class="modal-title"><?php echo _('Add Farmer');?></h3>
            <a href="javascript:void(0);" class="close">&times;</a>
        </div>
        <div class="modal-body no-padding">
            <form action="<?php echo BASE_URL;?>index.php?/anggota/setting/save" method="post">
                <div class="tabs-line">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-anggota1" data-toggle="tab">Farmer Data</a></li>
                        <li><a href="#tab-anggota2" data-toggle="tab">Cow Population</a></li>
                        <li><a href="#tab-anggota3" data-toggle="tab">Photo</a></li>
                    </ul>
                </div>
                <div class="tab-content border-top padding-15">
                     <div id="tab-anggota1" class="tab-pane tab-modal fade in active">
                        <div class="form-group">
                            <label for="add-anggota_nomor" class="col-md-4"><?php echo _('Farmer ID');?></label>
                            <div class="col-md-8">
                                <input type="text" id="add-anggota_nomor" class="input-block" name="anggota_nomor" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-card_number" class="col-md-4"><?php echo _('Card Number');?></label>
                            <div class="col-md-8">
                                <input type="text" id="add-card_number" class="input-block number" name="card_number" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-anggota_nama" class="col-md-4"><?php echo _('Name');?> </label>
                            <div class="col-md-8">
                                <input type="text" id="add-anggota_nama" class="input-block" name="anggota_nama" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-anggota_nohp" class="col-md-4"><?php echo _('Mobile Phone');?></label>
                            <div class="col-md-8">
                                <input type="text" id="add-anggota_nohp" class="input-block number" name="anggota_nohp" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-anggota_alamat" class="col-md-4"><?php echo _('Address');?> </label>
                            <div class="col-md-8">
                                <textarea id="add-anggota_alamat" class="input-block" name="anggota_alamat" required></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-tpk_id" class="col-md-4"><?php echo _('Cooperative');?></label>
                            <div class="col-md-8">
                                <select name="anggota_koperasi" id="add-anggota_koperasi" class="input-block" required="required">
                                    <option value="">-- Select Cooperative --</option>
                                    <?php foreach($koperasi['records'] as $k=>$v){ ?>
                                        <option value="<?php echo $v['koperasi_id']; ?>"><?php echo $v['koperasi_nama'];?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-anggota_tpk" class="col-md-4"><?php echo _('Collection Point');?></label>
                            <div class="col-md-8">
                                <select name="anggota_tpk" id="add-anggota_tpk" class="input-block"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-kelompok_id" class="col-md-4"><?php echo _('Group');?></label>
                            <div class="col-md-8">
                                <select name="anggota_kelompok" id="add-kelompok_id" class="input-block"></select>
                            </div>
                        </div>
                        <!-- <div class="form-group">
                            <label for="add-kelompokharga_id" class="col-md-4">Kelompok Harga</label>
                            <div class="col-md-8">
                                <select name="anggota_kelompokharga" id="add-kelompokharga_id" class="input-block"></select>
                            </div>
                        </div> -->
                        <div class="form-group">
                            <label class="col-md-4" for="add-anggota-status">Status</label>
                            <div class="col-md-8">
                                <select name="anggota_status" id="anggota_status" class="input-block status"></select>
                            </div>
                        </div>
                    </div>
                    <div id="tab-anggota2" class="tab-pane tab-modal">
                        <div class="form-group">
                            <label for="cow_population" class="col-md-4">Population</label>
                            <div class="col-md-8">
                                <input type="text" class="input-block number" id="cow_population" name="cow_population" value="1" required />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="cow_lactating" class="col-md-4">Lactating</label>
                            <div class="col-md-8">
                                <input type="text" class="input-block number" id="cow_lactating" name="cow_lactating" value="1" required />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="cow_antibiotic" class="col-md-4">Antibiotic</label>
                            <div class="col-md-8">
                                <select name="cow_antibiotic" id="cow_antibiotic">
                                    <option value="0" selected="selected">Negative</option>
                                    <option value="1">Positive</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4" for="cow_antibioticstartdate">Actibiotic Date</label>
                            <div class="col-md-4">
                                <input type="text" class="input-block datepicker" name="cow_antibioticstartdate" id="cow_antibioticstartdate" placeholder="From">
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="input-block datepicker" name="cow_antibioticenddate" id="cow_antibioticenddate" placeholder="To">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="cow_antibioticremark" class="col-md-4">Remark</label>
                            <div class="col-md-8">
                                <textarea name="cow_antibioticremark" id="cow_antibioticremark" class="input-block"></textarea>
                            </div>
                        </div>
                    </div>
                    <div id="tab-anggota3" class="tab-pane tab-modal">
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <input type="file" name="anggota_photo" id="add-anggota_photo" class="image-upload"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="padding-top-25 padding-bottom-15 padding-left-15 padding-right-15 text-right">
                    <button type="button" data-dismiss="modal">close</button>
                    <button type="submit" class="default">save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal draggable-header" id="modal-edit-anggota">
    <div class="modal-dialog">
        <div class="modal-header">
            <h3 class="modal-title"><?php echo _('Edit Farmer');?></h3>
            <a href="javascript:void(0);" class="close">&times;</a>
        </div>
        <div class="modal-body no-padding">
            <form action="<?php echo BASE_URL;?>index.php?/anggota/setting/save" method="post">
                <input name="anggota_id" type="hidden" id="edit-anggota_id"/>
                <div class="tabs-line">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-editanggota1" data-toggle="tab">Farmer Data</a></li>
                        <li><a href="#tab-editanggota2" data-toggle="tab">Cow Population</a></li>
                        <li><a href="#tab-editanggota3" data-toggle="tab">Photo</a></li>
                    </ul>
                </div>
                <div class="tab-content border-top padding-15">
                    <div id="tab-editanggota1" class="tab-pane tab-modal fade in active">
                        <div class="form-group">
                            <label for="edit-anggota_nomor" class="col-md-4"><?php echo _('Farmer ID');?></label>
                            <div class="col-md-8">
                                <input type="text" id="edit-anggota_nomor" class="input-block" name="anggota_nomor" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="edit-card_number" class="col-md-4"><?php echo _('Card Number');?></label>
                            <div class="col-md-8">
                                <input type="text" id="edit-card_number" class="input-block" name="card_number" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="edit-anggota_nama" class="col-md-4"><?php echo _('Name');?> </label>
                            <div class="col-md-8">
                                <input type="text" id="edit-anggota_nama" class="input-block" name="anggota_nama" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="edit-anggota_nohp" class="col-md-4"><?php echo _('Mobile Phone');?> </label>
                            <div class="col-md-8">
                                <input type="text" id="edit-anggota_nohp" class="input-block number" name="anggota_nohp" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="edit-anggota_alamat" class="col-md-4"><?php echo _('Address');?> </label>
                            <div class="col-md-8">
                                <textarea id="edit-anggota_alamat" class="input-block" name="anggota_alamat" required></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="edit-tpk_id" class="col-md-4"><?php echo _('Cooperative');?></label>
                            <div class="col-md-8">
                                <select name="anggota_koperasi" id="edit-anggota_koperasi" class="input-block koperasi" required="required"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="edit-tpk_id" class="col-md-4"><?php echo _('Collection Point');?></label>
                            <div class="col-md-8">
                                <select name="anggota_tpk" id="edit-anggota_tpk" class="input-block" required="required"></select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="edit-kelompok_id" class="col-md-4"><?php echo _('Group');?></label>
                            <div class="col-md-8">
                                <select name="anggota_kelompok" id="edit-kelompok_id" class="input-block"></select>
                            </div>
                        </div>
                        <!-- <div class="form-group">
                            <label for="edit-kelompokharga_id" class="col-md-4">Kelompok Harga</label>
                            <div class="col-md-8">
                                <select name="anggota_kelompokharga" id="edit-kelompokharga_id" class="input-block"></select>
                            </div>
                        </div> -->
                        <div class="form-group">
                            <label class="col-md-4" for="edit-anggota-status">Status</label>
                            <div class="col-md-8">
                                <select name="anggota_status" id="edit-anggota_status" class="input-block status"></select>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-editanggota2">
                        <div class="form-group">
                            <label for="edit-cow_population" class="col-md-4">Population</label>
                            <div class="col-md-8">
                                <input type="text" class="input-block number" id="edit-cow_population" name="cow_population" required />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="edit-cow_lactating" class="col-md-4">Lactating</label>
                            <div class="col-md-8">
                                <input type="text" class="input-block number" id="edit-cow_lactating" name="cow_lactating" required />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="edit-cow_antibiotic" class="col-md-4">Antibiotic</label>
                            <div class="col-md-8">
                                <select name="cow_antibiotic" id="edit-cow_antibiotic">
                                    <option value="0" selected="selected">Negative</option>
                                    <option value="1">Positive</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4" for="edit-cow_antibioticstartdate">Actibiotic Date</label>
                            <div class="col-md-4">
                                <input type="text" class="input-block datepicker" name="cow_antibioticstartdate" id="edit-cow_antibioticstartdate" placeholder="From">
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="input-block datepicker" name="cow_antibioticenddate" id="edit-cow_antibioticenddate" placeholder="To">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="edit-cow_antibioticremark" class="col-md-4">Remark</label>
                            <div class="col-md-8">
                                <textarea name="cow_antibioticremark" id="edit-cow_antibioticremark" class="input-block"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-editanggota3">
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-3">
                                <input type="hidden" name="anggota_photo" id="edit-anggota_photo"/>
                                <img id="edit-photo-preview" src="index.php?/load/file/<?php echo urlencode('images/photo.png');?>" alt="" class="img-responsive"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="padding-top-25 padding-right-15 padding-bottom-15 text-right">
                    <button type="button" data-dismiss="modal">close</button>
                    <button type="submit" class="default">save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<input type="file" name="" id="edit-uploader" style="width:1px;height:1px;visibility: hidden"/>

<script type="text/javascript">
    $(document).ready(function(){
        $('.grid-pagging').transpaginator({
            totalPages: <?php echo $anggota['total_page'];?>,
            currentPage: <?php echo $anggota['page'];?>,
            totalData: <?php echo $anggota['total_data'];?>,
            pageUrl: '<?php echo BASE_URL;?>index.php?/anggota/setting/&keyword=<?php echo $anggota['keyword'];?>&koperasi=<?php echo $_GET['koperasi'];?>&page='
        });
        $('.tr_selector').on('click',function(){
            var id = $(this).attr('data-id');
            $('.editbtn').removeClass('fg-lightgray').removeAttr('disabled').attr('data-id',id);
            $('.tr_selector').removeClass('active');
            $(this).addClass('active');
        });
        $('#__add').on('click',function(){
            $.get('<?php echo BASE_URL;?>index.php?/load/generateCardNumber',function(res){
                $('#add-card_number').val(res);
                $('#modal-add-anggota').addClass('fadein');
            });
        });
        $('#__edit').on('click',function(){
            var id = $(this).attr('data-id');
            if(typeof id !== 'undefined' && id !== ''){
                $.ajax({
                    url:'<?php echo BASE_URL;?>index.php?/anggota/setting/edit/'+id,
                    dataType:'json',
                    success:function(res){
                        $('#edit-anggota_id').val(res.anggota_id);
                        $('#edit-anggota_nama').val(res.anggota_nama);
                        $('#edit-card_number').val(res.card_number).attr('disabled','disabled');
                        $('#edit-anggota_nomor').val(res.anggota_nomor);
                        $('#edit-anggota_status').val(res.anggota_status);
                        $('#edit-anggota_alamat').val(res.anggota_alamat);
                        $('#edit-anggota_nohp').val(res.anggota_nohp);
                        $('#edit-anggota_koperasi').val(res.anggota_koperasi);
                        $('#edit-anggota_photo').val(res.anggota_photo);
                        $('#edit-cow_population').val(res.cow_population).attr('disabled','disabled');
                        $('#edit-cow_antibioticremark').val(res.cow_antibioticremark).attr('disabled','disabled');
                        $('#edit-cow_antibiotic').val(res.cow_antibiotic).attr('disabled','disabled');
                        $('#edit-cow_lactating').val(res.cow_lactating).attr('disabled','disabled');
                        $('#edit-cow_antibioticstartdate').val(moment(res.cow_antibioticstartdate).format('DD/MM/YYYY')).attr('disabled','disabled');
                        $('#edit-cow_antibioticenddate').val(moment(res.cow_antibioticenddate).format('DD/MM/YYYY')).attr('disabled','disabled');
                        if(res.anggota_photo == '' || typeof res.anggota_photo === 'undefined' || res.anggota_photo == null){
                            $('#edit-photo-preview').attr('src','index.php?/load/file/<?php echo urlencode('images/photo.png');?>');

                        }else{
                            $('#edit-photo-preview').attr('src',res.anggota_photo);
                        }
                        initTpkSelect('#edit-anggota_tpk',res.anggota_koperasi,res.anggota_tpk);
                        initKelompokSelect('#edit-kelompok_id',res.tpk_id,res.kelompok_id);
                        //initKelompokhargaSelect('#edit-kelompokharga_id',res.kelompok_id,res.kelompokharga_id);

                        $('#modal-edit-anggota').addClass('fadein');
                    }
                });
            }
        });
        $('#edit-photo-preview').on('click',function(){
            $('#edit-uploader').trigger('click');
        });
        $('#edit-uploader').uploader({
            resize:true,
            onSuccess:function(res){
                $('#edit-anggota_photo').val(res);
                $('#edit-photo-preview').attr('src',res);
            }
        });
        $(document).on('change','#add-anggota_koperasi',function(){
            var koperasi_id = $(this).val();
            initTpkSelect('#add-anggota_tpk',koperasi_id,'','#add-kelompok_id','#add-kelompokharga_id');
        });
        $(document).on('change','#add-anggota_tpk',function(){
            var tpk_id = $(this).val();
            initKelompokSelect('#add-kelompok_id',tpk_id,'','#add-kelompokharga_id');
        });
        $(document).on('change','#add-kelompok_id',function(){
            var kelompok_id = $(this).val();
            //initKelompokhargaSelect('#add-kelompokharga_id',kelompok_id);
        });

        // EDIT
        $(document).on('change','#edit-anggota_koperasi',function(){
            var koperasi_id = $(this).val();
            initTpkSelect('#edit-anggota_tpk',koperasi_id,'','#edit-kelompok_id','#edit-kelompokharga_id');
        });
        $(document).on('change','#edit-anggota_tpk',function(){
            var tpk_id = $(this).val();
            initKelompokSelect('#edit-kelompok_id',tpk_id,'','#edit-kelompokharga_id');
        });
        $(document).on('change','#edit-kelompok_id',function(){
            var kelompok_id = $(this).val();
            //initKelompokhargaSelect('#edit-kelompokharga_id',kelompok_id);
        });

        $('#__delete').on('click',function(){
            var id = $(this).attr('data-id');
            if(typeof id !== 'undefined' && id !== ''){
                if(confirm('<?php echo _('Are you sure want to delete this data?');?>')){
                    $.ajax({
                        url:'<?php echo BASE_URL;?>index.php?/anggota/setting/delete',
                        type:'post',
                        data:{
                            anggota_id:id
                        },
                        success:function(){
                            location.reload();
                        }
                    });
                }
            }
        });

    });

    function initTpkSelect(target,koperasi_id,value,targetKelompok,targetKelompokHarga){
        $.ajax({
            url:'<?php echo BASE_URL;?>index.php?/tpk/findByKoperasi/'+koperasi_id,
            dataType:'json',
            success:function(res){
                var opt = '';
                var data = res.records;
                if(typeof value === 'undefined'){
                    value = '';
                }
                for(var k in data){
                    opt += '<option value="'+data[k].tpk_id+'"';
                    if(data[k].tpk_id === value){
                        opt += ' selected="selected"';
                    }
                    opt += '">'+data[k].tpk_nama+'</option>';
                }
                $(target).html(opt);
                var currentvalue = '';
                if(value === ''){
                    currentvalue = data[0].tpk_id;
                }else{
                    currentvalue = value;
                }
                if(typeof targetKelompok !== 'undefined'){
                    initKelompokSelect(targetKelompok,currentvalue,'',targetKelompokHarga);
                }

            }
        });
    }

    function initKelompokSelect(target,tpk_id,value,target2){
        $.ajax({
            url:'<?php echo BASE_URL;?>index.php?/kelompok/findByTPK/'+tpk_id,
            dataType:'json',
            success:function(res){
                var opt = '';
                var data = res.records;
                if(typeof value === 'undefined'){
                    value = '';
                }
                for(var k in data){
                    opt += '<option value="'+data[k].kelompok_id+'"';
                    opt += ' data-kelompok_kode="'+data[k].kelompok_kode+'"';
                    if(data[k].kelompok_id === value){
                        opt += ' selected="selected"';
                    }
                    opt += ' data-tpk_id="'+data[k].tpk_id+'"';
                    opt += '">'+data[k].kelompok_nama+'</option>';
                }
                $(target).html(opt);
                var currentvalue = '';
                if(value === ''){
                    currentvalue = data[0].kelompok_id;
                }else{
                    currentvalue = value;
                }
                if(typeof target2 !== 'undefined'){
                    initKelompokhargaSelect(target2,currentvalue);
                }

            }
        });
    }
    function initKelompokhargaSelect(target,kelompok_id,value){
        $.ajax({
            url:'<?php echo BASE_URL;?>index.php?/kelompokharga/findByKelompok/'+kelompok_id,
            dataType:'json',
            success:function(res){
                var opt = '';
                var data = res.records;
                if(typeof value === 'undefined'){
                    value = '';
                }
                for(var k in data){
                    opt += '<option value="'+data[k].kelompokharga_id+'"';
                    opt += ' data-kelompok_kode="'+data[k].kelompokharga_kode+'"';
                    if(data[k].kelompokharga_id === value){
                        opt += ' selected="selected"';
                    }
                    opt += ' data-tpk_id="'+data[k].tpk_id+'"';
                    opt += '">'+data[k].kelompokharga_nama+'</option>';
                }
                $(target).html(opt);
            }
        });
    }

    function filter(){
        var keyword = $('#keyword').val();
        var koperasi = $('#koperasi').val();
        window.location.href = '<?php echo BASE_URL;?>index.php?/anggota/setting/&koperasi='+koperasi+'&keyword='+keyword+'&page=1';
    }
</script>
<?php View::inc('footer.php');?>
