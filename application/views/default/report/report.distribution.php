<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('report/report.menu.php');?>
    <div class="content padded">
        <div class="breadcrumb">
            <ul class="tab">
                <li <?php if($periode == 'daterange'){ echo 'class="active"'; } ?>>
                    <a href="<?php echo BASE_URL;?>index.php?/report/distribution/&amp;periode=daterange" class="">date range</a>
                </li>
                <li <?php if($periode == 'monthly'){ echo 'class="active"'; } ?>>
                    <a href="<?php echo BASE_URL;?>index.php?/report/distribution/&amp;periode=monthly" class="">monthly</a>
                </li>
                <li <?php if($periode == 'yearly'){ echo 'class="active"'; } ?>>
                    <a href="<?php echo BASE_URL;?>index.php?/report/distribution/&amp;periode=yearly" class="">yearly</a>
                </li>
            </ul>
            <div class="pull-right">
                <ul class="breadcrumb-nav">
                    <li>
                        <a href="">Report</a>
                    </li>
                    <li>
                        <a href="">Delivery Order</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="ribbon col-md-12">
                    <div class="menu">
                        <a href="javascript:void(0);"><i class="ion-ios7-gear-outline"></i> Coop</a>
                        <span class="margin-left-5">
                           <select name="filterkoperasi" id="filterkoperasi" onchange="reloadReport();">
                               <option value="">All</option>
                               <?php 
                               foreach($koperasi['records'] as $k=>$v){
                                    echo '<option value="'.$v['koperasi_id'].'"';
                                    if($v['koperasi_id'] == $_GET['koperasi']){
                                        echo ' selected="selected"';
                                    }
                                    echo '>'.$v['koperasi_nama'].'</option>';
                               }
                               ?>
                           </select>
                        </span>
                    </div>
                    <div class="pull-right ">
                        <span class="padding-right-10"><i class="ion-ios7-calendar-outline" style="font-size:18px;vertical-align:middle;"></i> Periode:</span>
                        <?php if($periode == 'daterange'){ ?>
                            <input type="text" id="date1" value="<?php echo $_GET['date1'];?>" class="datepicker" style="width:100px" onchange="reloadReport();"/>
                            <span class="padding-left-5 padding-right-5">:</span>
                            <input type="text" id="date2" value="<?php echo $_GET['date2'];?>" class="datepicker"  style="width:100px" onchange="reloadReport();"/>
                            <input name="year" id="year" value="<?php echo $_GET['year'];?>" type="hidden"/>
                        <?php }elseif($periode == 'monthly'){ ?>
                            <select name="date1" id="date1" class="month" data-value="<?php echo $_GET['date1'];?>"  style="width:auto" onchange="reloadReport();">
                                <option value="">-- loading data --</option>
                            </select>
                            <select name="date2" id="date2" class="month" data-value="<?php echo $_GET['date2'];?>"  style="width:auto" onchange="reloadReport();">
                                <option value="">-- loading data --</option>
                            </select>
                            <select name="year" id="year" class="year" data-value="<?php echo $_GET['year'];?>"  style="width:auto" onchange="reloadReport();">
                                <option value="">-- loading data --</option>
                            </select>
                        <?php }else{ ?>
                            <select name="year" id="date1" class="year" data-value="<?php echo $_GET['date1'];?>"  style="width:auto" onchange="reloadReport();">
                                <option value="">-- loading data --</option>
                            </select>
                            <select name="year" id="date2" class="year" data-value="<?php echo $_GET['date2'];?>"  style="width:auto" onchange="reloadReport();">
                                <option value="">-- loading data --</option>
                            </select>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="padding-10"><?php echo $transaksi_chart;?></div>
                </div>
            </div>
            <div class="row margin-top-20">
            	<div class="col-md-12">
            		<h2 class="pull-left"><i class="ion-ios7-folder-outline"></i> Data Detail</h2>
                    <h3 class="pull-right">
                        <a href="<?php echo BASE_URL; ?>index.php?/report/export/distribution/&periode=<?php echo $periode;?>&year=<?php echo $_GET['year'];?>&date1=<?php echo $_GET['date1'];?>&date2=<?php echo $_GET['date2'];?>" title="Synchronize" target="_blank">
                            <i class="ion-ios7-download-outline"></i> export
                        </a>
                    </h3>
            	</div>
                <div class="clearfix"></div>
            	<div class="padding-bottom-40 ">
                    <table class="table table-hover border" style="border-left:0;border-right:0;">
                        <tr>
                            <th>No</th>
                            <th>Date</th>
                            <th>Truck</th>
                            <th>Consumer</th>
                            <th style="width:80px;">Weight</th>
                            <th>Status</th>
                            <th></th>
                        </tr>
                        <?php if($data['total_data'] === 0){ ?>
                        <tr>
                            <td colspan="100%"><?php echo _('No record found');?></td>
                        </tr>
                        <?php } ?>
                        <?php foreach($data['records'] as $k=>$v){ ?>
                        <tr class="trselector" data-id="<?php echo $v['distribution_id'];?>" id="tr<?php echo $v['distribution_id'];?>">
                            <td><?php echo $v['distribution_nomor'];?></td>
                            <td class="format-date"><?php echo $v['distribution_tanggal'];?></td>
                            <td><?php echo $v['truck_nopol'];?></td>
                            <td><?php echo $v['konsumen_nama'];?></td>
                            <td class="text-right"><?php echo $v['distribution_berat'];?> Kg</td>
                           <td class="distribution-status"><?php echo $v['distribution_status'];?></td>
                            <td></td>
                        </tr>
                        <?php } ?>
                    </table>
                    <div class="grid-pagging border-top margin-bottom-30"></div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.grid-pagging').transpaginator({
                totalPages: <?php echo $data['total_page'];?>,
                totalData: <?php echo $data['total_data'];?>,
                currentPage: <?php echo $data['page'];?>,
                pageUrl: '<?php echo BASE_URL;?>index.php?/report/distribution/&periode=<?php echo $periode;?>&year=<?php echo $_GET['year'];?>&date1=<?php echo $_GET['date1'];?>&date2=<?php echo $_GET['date2'];?>&page='
            });
        });
        function reloadReport(){
            var date1 = $('#date1').val();
            var date2 = $('#date2').val();
            var year = $('#year').val();
            var koperasi = $('#filterkoperasi').val();
            openUrl('report/distribution/&koperasi='+koperasi+'&periode=<?php echo $periode;?>&year='+year+'&date1='+date1+'&date2='+date2);
        }
    </script>
<?php View::inc('footer.php');?>
