<?php View::inc('header.php');?>
<?php View::inc('navbarFarmer.php');?>
<?php View::inc('report/report.menuFarmer.php');?>
    <div class="content padded">
        <div class="breadcrumb">
            <ul class="tab">
                <li class="active">
                    <a href="">Report</a>
                </li>

            </ul>
            <div class="pull-right">
                <ul class="breadcrumb-nav">
                    <li>
                        <a href="">Report</a>
                    </li>
                    <li>
                        <a href="">Sumarry</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h1><i class="ion-ios7-folder-outline"></i> Transaksi</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="padding-10 border"><?php echo $transaksi_chart;?></div>
                </div>
            </div>
            <div class="row">
            	<div class="col-md-12">
            		<h2><i class="ion-ios7-folder-outline"></i> Detail Transaksi</h2>
            	</div>
            	<div class="clearfix"></div>
            	<div class="padding-bottom-40 ">
                    <table class="table border-top">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>TPK</th>
                                <th>Tanggal</th>
                                <th>ID Transaksi</th>
                                <th>ID Member</th>
                                <th>Nama</th>
                                <th>Total Berat (Kg)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if($transaksi['total_data'] === 0){ ?>
                            <tr>
                                <td colspan="100%">No record found</td>
                            </tr>                                   
                            <?php } ?>
                            <?php foreach($transaksi['records'] as $k=>$v){ ?>
                            <tr>
                                <td>
                                    <?php 
                                    if($transaksi['page'] == 1){
                                        echo ($k+1);    
                                    }else{
                                        $no = ($transaksi['limit'] * $transaksi['page']) + ($k+1);
                                        echo $no;
                                    }
                                    ?>
                                </td>
                                <td><?php echo $v['tpk_nama'];?></td>
                                <td><?php echo date('d F Y H:i',strtotime($v['transaksi_waktu']));?></td>
                                <td><?php echo $v['transaksi_id'];?></td>
                                <td><?php echo $v['transaksi_nomor'];?></td>
                                <td><?php echo $v['transaksi_nama'];?></td>
                                <td><?php echo $v['transaksi_totalberat'];?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <div class="grid-pagging border-top margin-bottom-30"></div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.grid-pagging').transpaginator({
                totalPages: <?php echo $transaksi['total_page'];?>,
                currentPage: <?php echo $transaksi['page'];?>,
                pageUrl: '<?php echo BASE_URL;?>index.php?/report/farmer/&page='
            }); 
        });
    </script>
<?php View::inc('footer.php');?>