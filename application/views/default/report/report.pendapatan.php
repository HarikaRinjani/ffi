<?php View::inc('header.php');?>
<?php View::inc('navbarFarmer.php');?>
<?php View::inc('report/report.menuFarmer.php');?>
<link rel="stylesheet" href="<?php echo BASE_URL;?>themes/default/jquery-tokeninput/styles/token-input-facebook.css"/>
    <div class="content padded">
        <div class="breadcrumb">
        	<ul class="tab">
                <li class="active">
                    <a href="">Laporan Pendapatan</a>
                </li>
            </ul>
            <div class="pull-right">
                <ul class="breadcrumb-nav">
                    <li>
                        <a href="">Report</a>
                    </li>
                    <li>
                        <a href="">Pendapatan</a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="container-fluid">
            
            <div class="row">
                <div class="ribbon col-md-12">
                    <div class="menu">
						<h3 class="no-margin">Laporan Pendapatan</h3>
                        <!--<a href="javascript:void(0);" class="no-hover"><i class="ion-ios7-gear-outline"></i>Coop:</a>
                        <div class="pull-left">
                            <select name="koperasi" id="inputfilter-koperasi" class="" data-value="<?php echo $_GET['koperasi'];?>">
                                <?php 
                                foreach($koperasi['records'] as $k=>$v){
                                    echo '<option value="'.$v['koperasi_id'].'"';
                                    if($v['koperasi_id'] == $_GET['koperasi']){
                                        echo ' selected="selected"';
                                    }
                                    echo '>'.$v['koperasi_nama'].'</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <a href="javascript:void(0);" class="no-hover"><i class="ion-ios7-contact-outline"></i>Farmer:</a>
                        <div class="pull-left" style="width:200px;">
                            <input type="text" class="input-block" id="inputfilter-angggota">
                        </div>
                        <a href="javascript:void(0);" onclick="reloadFilter();" class="margin-left-5"><i class="ion-ios7-search"></i>Filter</a>-->
                    </div>
                    <div class="pull-right ">
                        <span class="padding-right-10"><i class="ion-ios7-calendar-outline" style="font-size:18px;vertical-align:middle;"></i> Periode:</span>
                        <select name="date1" id="date1" class="month" data-value="<?php echo $_GET['month'];?>"  style="width:auto" onchange="reloadReport();">
                            <option value="">-- loading data --</option>
                        </select>
                        <select name="year" id="year" class="year" data-value="<?php echo $_GET['year'];?>"  style="width:auto" onchange="reloadReport();">
                            <option value="">-- loading data --</option>
                        </select>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="row" style="">
            	<div class="padding-bottom-40 ">
                    <table class="table table-hover border" style="border-left:0;border-right:0;" id="periode">
                        <thead>
                        <tr>
                            <th class="border-right dotted">Tanggal</th>
                            <th class="border-right dotted">Produksi Pagi</th>
                            <th class="border-right dotted">Produksi Sore</th>
                            <!-- <th class="border-right dotted">IBC</th> -->
                            <th class="border-right dotted">FAT</th>
                            <th class="border-right dotted">SNF</th>
                            <th class="border-right dotted">Resazurin</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if($data['total_data'] == 0){ ?>
                            <tr>
                                <td colspan="100%"><?php echo _('No record found');?></td>
                            </tr>
                        <?php }else{ ?>
                            <?php foreach ($arr as $key => $value) {
                            	?>
                                <tr class="tr_selector cursor-pointer" data-id="" id="tr">
                                    <td class="border-right dotted format-date"><?php echo $key; ?></td>
                                    <td class="border-right dotted"><?php echo $value['p']['transaksi_totalberat'] ?></td>
                                    <td class="border-right dotted"><?php echo $value['s']['transaksi_totalberat'] ?></td>
                                    <td class="border-right dotted"><?php echo $value['p']['qcparameter_fat'] ?></td>
                                    <td class="border-right dotted"><?php echo $value['p']['qcparameter_snf'] ?></td>
                                    <td class="border-right dotted"><?php echo $resazur['rezgradedetail_price']; ?></td>
                                </tr>
                            <?php } ?>
                        <?php } ?>
                        </tbody>
                    </table>
                    <br>
                    <div class="col-md-12">
                    	<div class="col-md-4"> </div>
                    	<div class="col-md-4">Harga Standar</div>
                    	<div class="col-md-4">Rp <span class="pull-right"><?php echo number_format($hargadasar); ?></span></div>
                    </div>
                    <br>
                    <hr>
                    <?php
                     for ($i=1; $i <= $jumlah_periode; $i++) {?>
                    <div class="col-md-12">
                    	<div class="col-md-4"><strong>Periode <?php echo $i; ?></strong></div>
                    	<div class="col-md-4">Total Berat</div>
                    	<div class="col-md-4"><?php echo $totalberat[$i]['totalberat'] ?> Ltr (10 Hari)</div>
                    </div>
                    <div class="col-md-12">
                    	<div class="col-md-4"></div>
                    	<div class="col-md-4">Bonus FAT</div>
                    	<div class="col-md-4">Rp <span class="pull-right"><?php echo ${'bonus_fat_p'.$i} ?></span></div>
                    </div>
                    <div class="col-md-12">
                    	<div class="col-md-4"></div>
                    	<div class="col-md-4">Bonus SNF</div>
                    	<div class="col-md-4">Rp <span class="pull-right"><?php echo ${'bonus_snf_p'.$i} ?></span></div>
                    </div>
                    <div class="col-md-12">
                    	<div class="col-md-4"></div>
                    	<div class="col-md-4">Bonus Resazurin</div>
                    	<div class="col-md-4">Rp <span class="pull-right"><?php echo ${'bonus_res_p'.$i}; ?></span></div>
                    </div>
                    <div class="col-md-12">
                    	<div class="col-md-4"></div>
                    	<div class="col-md-4">Harga / Liter</div>
                    	<div class="col-md-4">
                    		Rp <span class="pull-right">
                    			<?php 
                    			$hasil = ${'bonus_fat_p'.$i}+${'bonus_snf_p'.$i}+${'bonus_res_p'.$i}+$hargadasar;
                    			echo number_format($hasil);  
                    			?>
                    			</span>
                    	</div>
                    </div>
                    <div class="col-md-12">
                    	<div class="col-md-4"></div>
                    	<div class="col-md-4">Total Pendapatan Periode <?php echo $i; ?></div>
                    	<div class="col-md-4">
                    		Rp <span class="pull-right">
                    			<?php  
                    			${'pendapatan'.$i} = $hasil * $totalberat[$i]['totalberat'];
                    			echo number_format(${'pendapatan'.$i});
                    			 ?>
                    			</span>
                		</div>
                    </div>
                    <hr class="col-md-8" style="float:right;">
                    <?php } ?>
                    <br>
                    <div class="col-md-12">
                    	<div class="col-md-4"><strong>Grand Total</strong></div>
                    	<div class="col-md-4">Produksi</div>
                    	<div class="col-md-4">
                    		Rp <span class="pull-right">
                    			<?php  
                    			$berat = $totalberat['1']['totalberat'] + $totalberat['2']['totalberat'] + $totalberat['3']['totalberat'];
                    			echo number_format($berat);
                    			 ?>
                    			</span>
                		</div>
                    </div>
                    <div class="col-md-12">
                    	<div class="col-md-4"></div>
                    	<div class="col-md-4">Pendapatan</div>
                    	<div class="col-md-4">
                    		Rp <span class="pull-right">
                    			<?php  
                    			$total_pendapatan = $pendapatan1 + $pendapatan2 + $pendapatan3;
                    			echo number_format($total_pendapatan);
                    			 ?>
                    			</span>
                		</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="<?php echo BASE_URL;?>themes/default/jquery-tokeninput/src/jquery.tokeninput.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.tab-selector').on('click',function(){
                var id = $(this).attr('data-id');
                $('.tab-selector:not(.fg-lightgray)').addClass('fg-lightgray');
                $(this).removeClass('fg-lightgray');
                $('.chartContainer').addClass('hidden');
                $('#chartContainer'+id).removeClass('hidden');
                var chartnya = $('#widget_transaksi_chart'+id).highcharts();
                chartnya.setSize($('.chartContainer').width(),$('.chartContainer').height(),true);
            });
            $('.filter-select').click(function(){
                var id = $(this).attr('data-href');
                $('.filter-select').removeClass('fg-lightgray');
                $('.filter-select').not(this).addClass('fg-lightgray');
                $('.filter-container').not(id).hide();
                $(id).show();
            });
            

            $('.grid-pagging').transpaginator({
                totalPages: <?php echo $data['total_page'];?>,
                currentPage: <?php echo $data['page'];?>,
                pageUrl: '<?php echo BASE_URL;?>index.php?/report/pendapatan/&koperasi=<?php echo $_GET['koperasi'];?>&anggota=<?php echo $_GET['anggota'];?>&periode=<?php echo $_GET['periode'];?>&year=<?php echo $_GET['year'];?>&month=<?php echo $_GET['month'];?>&page='
            });
            
        });
        function reloadFilter(){
            var koperasi = $('#inputfilter-koperasi').val();
            var anggota = $('#inputfilter-angggota').val();
            var date1 = $('#date1').val();
            var year = $('#year').val();
            openUrl('report/pendapatan/&koperasi='+koperasi+'&anggota='+anggota+'&periode=<?php echo $_GET['periode'];?>&year='+year+'&month='+date1);
        }
        function reloadReport(){
            var date1 = $('#date1').val();
            var year = $('#year').val();
            openUrl('report/pendapatan/&koperasi=<?php echo $_GET['koperasi'];?>&anggota=<?php echo $_GET['anggota'];?>&periode=<?php echo $_GET['periode'];?>&year='+year+'&month='+date1);
        }

    </script>
<?php View::inc('footer.php');?>