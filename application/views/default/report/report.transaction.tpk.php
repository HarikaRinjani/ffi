<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('report/report.menu.php');?>
    <div class="content padded">
        <div class="breadcrumb">
            <ul class="tab">
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/report/transaction/total">Total</a>
                </li>
                <li class="active">
                    <a href="">TPK</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/report/transaction/koperasi">Koperasi</a>
                </li>
            </ul>
            <div class="pull-right">
                <ul class="breadcrumb-nav">
                    <li>
                        <a href="">Report</a>
                    </li>
                    <li>
                        <a href="">Milk Collection</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="ribbon col-md-12">
                    <h1 class="pull-left">
                        <a href="<?php echo BASE_URL;?>index.php?/report/transaction/tpk/&periode=daterange" class="padding-right-20 <?php if($periode != 'daterange'){ echo 'fg-lightgray'; } ?>">date-range</a>
                        <a href="<?php echo BASE_URL;?>index.php?/report/transaction/tpk/&periode=monthly" class="padding-right-20 <?php if($periode != 'monthly'){ echo 'fg-lightgray'; } ?>">monthly</a>
                        <a href="<?php echo BASE_URL;?>index.php?/report/transaction/tpk/&periode=yearly" class="padding-right-20 <?php if($periode != 'yearly'){ echo 'fg-lightgray'; } ?>">yearly</a>
                    </h1>
                    <div class="pull-right padding-top-15">
                        <span class="padding-right-10"><h3 style="display:inline;">Periode: </h3></span>
                        <?php if($periode == 'daterange'){ ?>
                            <input type="text" id="date1" value="<?php echo $_GET['date1'];?>" class="datepicker"/>
                            <span class="padding-left-5 padding-right-5">:</span>
                            <input type="text" id="date2" value="<?php echo $_GET['date2'];?>" class="datepicker"/>
                            <input name="year" id="year" value="<?php echo $_GET['year'];?>" type="hidden"/>
                        <?php }elseif($periode == 'monthly'){ ?>
                            <select name="date1" id="date1" class="month" data-value="<?php echo $_GET['date1'];?>">
                                <option value="">-- loading data --</option>
                            </select>
                            <select name="date2" id="date2" class="month" data-value="<?php echo $_GET['date2'];?>">
                                <option value="">-- loading data --</option>
                            </select>
                            <select name="year" id="year" class="year" data-value="<?php echo $_GET['year'];?>">
                                <option value="">-- loading data --</option>
                            </select>
                        <?php }else{ ?>
                            <select name="year" id="date1" class="year" data-value="<?php echo $_GET['date1'];?>">
                                <option value="">-- loading data --</option>
                            </select>
                            <select name="year" id="date2" class="year" data-value="<?php echo $_GET['date2'];?>">
                                <option value="">-- loading data --</option>
                            </select>
                        <?php } ?>
                        <button id="reloadReport" class="default margin-left-5">submit</button>
                    </div>
                </div>
               <div class="col-md-12">
                    <div class="padding-10 margin-top-10 border"><?php echo $transaksi_chart1;?></div>
                </div>
                <div class="col-md-12">
                    <div class="padding-10 margin-top-10 border"><?php echo $transaksi_chart2;?></div>
                </div>
            </div>
            <div class="row margin-top-30">
            	<div class="col-md-12">
            		<h2 class="pull-left"><i class="ion-ios7-folder-outline"></i> Detail Transaksi</h2>
                    <h3 class="pull-right">
                        <a href="<?php echo BASE_URL; ?>index.php?/report/sinkronisasi" title="Synchronize" class="padding-right-15">
                            <i class="ion-ios7-loop-strong"></i> sync
                        </a>
                        <a href="<?php echo BASE_URL; ?>index.php?/report/export/transaction/&periode=<?php echo $periode;?>&year=<?php echo $_GET['year'];?>&date1=<?php echo $_GET['date1'];?>&date2=<?php echo $_GET['date2'];?>" title="Synchronize" target="_blank">
                            <i class="ion-ios7-download-outline"></i> export
                        </a>
                    </h3>
            	</div>
                <div class="clearfix"></div>
            	<div class="padding-bottom-40">
                    <table class="table border-top">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>TPK</th>
                                <th>Tanggal</th>
                                <th>ID Transaksi</th>
                                <th>ID Member</th>
                                <th>Nama</th>
                                <th>Total Berat (Kg)</th>
                                <th>Data Sync</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if($transaksi['total_data'] === 0){ ?>
                            <tr>
                                <td colspan="100%">No record found</td>
                            </tr>
                            <?php } ?>
                            <?php foreach($transaksi['records'] as $k=>$v){ ?>
                            <tr>
                                <td>
                                    <?php
                                    if($transaksi['page'] == 1){
                                        echo ($k+1);
                                    }else{
                                        $no = ($transaksi['limit'] * $transaksi['page']) + ($k+1);
                                        echo $no;
                                    }
                                    ?>
                                </td>
                                <td><?php echo $v['tpk_nama'];?></td>
                                <td><?php echo date('d F Y H:i',strtotime($v['transaksi_waktu']));?></td>
                                <td><?php echo $v['transaksi_id'];?></td>
                                <td><?php echo $v['transaksi_nomor'];?></td>
                                <td><?php echo $v['transaksi_nama'];?></td>
                                <td><?php echo $v['transaksi_totalberat'];?></td>
                                <td><span class="syncstatus"><?php echo $v['transaksi_sync'];?></span></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <div class="grid-pagging border-top margin-bottom-30"></div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.grid-pagging').transpaginator({
                totalPages: <?php echo $transaksi['total_page'];?>,
                currentPage: <?php echo $transaksi['page'];?>,
                pageUrl: '<?php echo BASE_URL;?>index.php?/report/transaction/tpk/&periode=<?php echo $periode;?>&year=<?php echo $_GET['year'];?>&date1=<?php echo $_GET['date1'];?>&date2=<?php echo $_GET['date2'];?>&page='
            });
            $('#reloadReport').on('click',function(){
                var date1 = $('#date1').val();
                var date2 = $('#date2').val();
                var year = $('#year').val();
                openUrl('report/transaction/tpk/&periode=<?php echo $periode;?>&year='+year+'&date1='+date1+'&date2='+date2);
            });
        });
    </script>
<?php View::inc('footer.php');?>
