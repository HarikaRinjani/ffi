<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('report/report.menu.php');?>
<link rel="stylesheet" href="<?php echo BASE_URL;?>themes/default/jquery-tokeninput/styles/token-input-facebook.css"/>
    <div class="content padded">
        <div class="breadcrumb">
            <!-- <ul class="tab">

            </ul> -->
            <ul class="tab">
                <li <?php if($periode == 'daterange'){ echo 'class="active"'; } ?>>
                    <a href="<?php echo BASE_URL;?>index.php?/report/qcparameter/&amp;filterby=<?php echo $_GET['filterby'];?>&amp;koperasi=<?php echo $_GET['koperasi'];?>&amp;anggota=<?php echo $_GET['anggota'];?>&amp;periode=daterange" class="">date range</a>
                </li>
                <li <?php if($periode == 'monthly'){ echo 'class="active"'; } ?>>
                    <a href="<?php echo BASE_URL;?>index.php?/report/qcparameter/&amp;filterby=<?php echo $_GET['filterby'];?>&amp;koperasi=<?php echo $_GET['koperasi'];?>&amp;anggota=<?php echo $_GET['anggota'];?>&amp;periode=monthly" class="">monthly</a>
                </li>
                <li <?php if($periode == 'yearly'){ echo 'class="active"'; } ?>>
                    <a href="<?php echo BASE_URL;?>index.php?/report/qcparameter/&amp;filterby=<?php echo $_GET['filterby'];?>&amp;koperasi=<?php echo $_GET['koperasi'];?>&amp;anggota=<?php echo $_GET['anggota'];?>&amp;periode=yearly" class="">yearly</a>
                </li>
            </ul>
            <div class="pull-right">
                <ul class="breadcrumb-nav">
                    <li>
                        <a href="">Report</a>
                    </li>
                    <li>
                        <a href="">Quality</a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="ribbon col-md-12">
                    <div class="menu">
                        <a href="javascript:void(0);" class="no-hover"><i class="ion-ios7-gear-outline"></i>Coop:</a>
                        <div class="pull-left">
                            <select name="koperasi" id="inputfilter-koperasi" class="" onChange="reloadFilter();">
                                <option value="">All</option>
                                <?php
                                foreach($koperasi['records'] as $k=>$v){
                                    echo '<option value="'.$v['koperasi_id'].'"';
                                    if($v['koperasi_id'] == $_GET['koperasi']){
                                        echo ' selected="selected"';
                                    }
                                    echo '>'.$v['koperasi_nama'].'</option>';
                                }
                                ?>
                            </select>
                            <select name="tpk" id="inputfilter-tpk" class="" onChange="reloadFilter();">
                                <option value="">All</option>
                                <?php
                                foreach ($tpk['records'] as $k=>$v) {
                                    echo '<option value="'.$v['tpk_id'].'"';
                                    if ($v['tpk_id'] == $_GET['tpk']) {
                                        echo ' selected="selected"';
                                    }
                                    echo '>'.$v['tpk_nama'].'</option>';
                                }
                                ?>
                            </select>
                        </div>
                        <a href="javascript:void(0);" class="no-hover"><i class="ion-ios7-contact-outline"></i>Farmer:</a>
                        <div class="pull-left" style="width:200px;">
                            <input type="text" class="input-block" id="inputfilter-angggota" onChange="reloadFilter();">
                        </div>
                        <!-- <a href="javascript:void(0);" onclick="reloadFilter();" class="margin-left-5"><i class="ion-ios7-search"></i>Filter</a> -->
                    </div>
                    <div class="pull-right ">
                        <span class="padding-right-10"><i class="ion-ios7-calendar-outline" style="font-size:18px;vertical-align:middle;"></i> Periode:</span>
                        <?php if($periode == 'daterange'){ ?>
                            <input type="text" id="date1" value="<?php echo $_GET['date1'];?>" class="datepicker" style="width:100px" onchange="reloadReport();"/>
                            <span class="padding-left-5 padding-right-5">:</span>
                            <input type="text" id="date2" value="<?php echo $_GET['date2'];?>" class="datepicker"  style="width:100px" onchange="reloadReport();"/>
                            <input name="year" id="year" value="<?php echo $_GET['year'];?>" type="hidden"/>
                        <?php }elseif($periode == 'monthly'){ ?>
                            <select name="date1" id="date1" class="month" data-value="<?php echo $_GET['date1'];?>"  style="width:auto" onchange="reloadReport();">
                                <option value="">-- loading data --</option>
                            </select>
                            <select name="date2" id="date2" class="month" data-value="<?php echo $_GET['date2'];?>"  style="width:auto" onchange="reloadReport();">
                                <option value="">-- loading data --</option>
                            </select>
                            <select name="year" id="year" class="year" data-value="<?php echo $_GET['year'];?>"  style="width:auto" onchange="reloadReport();">
                                <option value="">-- loading data --</option>
                            </select>
                        <?php }else{ ?>
                            <select name="year" id="date1" class="year" data-value="<?php echo $_GET['date1'];?>"  style="width:auto" onchange="reloadReport();">
                                <option value="">-- loading data --</option>
                            </select>
                            <select name="year" id="date2" class="year" data-value="<?php echo $_GET['date2'];?>"  style="width:auto" onchange="reloadReport();">
                                <option value="">-- loading data --</option>
                            </select>
                        <?php } ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="border-top dotted margin-top-10 padding-top-5" style="margin-left:-15px;margin-right:-15px;">
                        <div class="padding-left-15 padding-right-15">
                            <div class="menu">
                                <a href="javascript:void(0)" class="no-hover">Parameter:</a>
                                <a href="" class="separator"></a>
                                <?php foreach($chartfields as $k=>$v){ ?>
                                <a href="javascript:void(0);" class="tab-selector <?php if($k != 'FAT'){ echo 'fg-lightgray'; }?>" data-id="<?php echo $k;?>"><?php if($k == 'CFU'){ echo 'TPC'; }else{ echo $k; } ?></a>
                                <a href="" class="separator"></a>
                                <?php } ?>
                            </div>
                            &nbsp;
                        </div>
                    </div>
                </div>
                <?php foreach($chartfields as $k=>$v){ ?>
                <div class="col-md-12 chartContainer <?php if($k != 'FAT'){ echo 'hidden'; } ?>" id="chartContainer<?php echo $k;?>">
                    <div class="padding-10">
                        <?php echo $transaksi_chart[$k];?>
                    </div>
                </div>
                <?php } ?>
            </div>
            <div class="row margin-top-20">
            	<div class="col-md-12">
            		<h2 class="pull-left"><i class="ion-ios7-folder-outline"></i> Data Detail</h2>
                    <h3 class="pull-right">
                        <a href="<?php echo BASE_URL; ?>index.php?/report/export/qcparameter/&koperasi=<?php echo $_GET['koperasi'];?>&tpk=<?php echo $_GET['tpk'];?>&periode=<?php echo $periode;?>&year=<?php echo $_GET['year'];?>&date1=<?php echo $_GET['date1'];?>&date2=<?php echo $_GET['date2'];?>" title="Export" target="_blank">
                            <i class="ion-ios7-download-outline"></i> export
                        </a>
                    </h3>
            	</div>
                <div class="clearfix"></div>
            	<div class="padding-bottom-40 ">
                    <table class="table table-hover border" style="border-left:0;border-right:0;">
                        <thead>
                        <tr>
                            <th class="border-right dotted">Transaction ID</th>
                            <th class="border-right dotted">Farmer ID</th>
                            <th class="border-right dotted">Name</th>
                            <th class="border-right dotted">Date</th>
                            <th class="border-right dotted">Session</th>
                            <th class="border-right dotted">TPC</th>
                            <!-- <th class="border-right dotted">IBC</th> -->
                            <th class="border-right dotted">FAT</th>
                            <th class="border-right dotted">Protein</th>
                            <th class="border-right dotted">Lactose</th>
                            <th class="border-right dotted">TS</th>
                            <th class="border-right dotted">FP</th>
                            <th class="border-right dotted">Acidity</th>
                            <th class="border-right dotted">Urea</th>
                            <th>SNF</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if($data['total_data'] == 0){ ?>
                            <tr>
                                <td colspan="100%"><?php echo _('No record found');?></td>
                            </tr>
                        <?php }else{ ?>
                            <?php foreach($data['records'] as $k=>$v){ ?>
                                <tr class="tr_selector cursor-pointer" data-id="<?php echo $v['qcparameter_id'];?>" id="tr<?php echo $v['qcparameter_id'];?>">
                                    <td class="border-right dotted"><?php echo $v['qcparameter_transaksi'];?></td>
                                    <td class="border-right dotted"><?php echo $v['transaksi_nomor'];?></td>
                                    <td class="border-right dotted"><?php echo $v['transaksi_nama'];?></td>
                                    <td class="border-right dotted"><?php echo $v['qcparameter_scandate'];?></td>
                                    <td class="border-right dotted samplingsesi"><?php echo $v['transaksi_sesi'];?></td>
                                    <td class="border-right dotted"><?php echo (float) $v['qcparameter_cfu'];?></td>

                                    <!-- <td class="border-right dotted"><?php echo (float) $v['qcparameter_ibc'];?></td> -->
                                    <td class="border-right dotted"><?php echo (float) $v['qcparameter_fat'];?></td>
                                    <td class="border-right dotted"><?php echo (float) $v['qcparameter_protein'];?></td>
                                    <td class="border-right dotted"><?php echo (float) $v['qcparameter_lactose'];?></td>
                                    <td class="border-right dotted"><?php echo (float) $v['qcparameter_ts'];?></td>
                                    <td class="border-right dotted"><?php echo (float) $v['qcparameter_fp'];?></td>
                                    <td class="border-right dotted"><?php echo (float) $v['qcparameter_acidity'];?></td>
                                    <td class="border-right dotted"><?php echo (float) $v['qcparameter_urea'];?></td>
                                    <td><?php echo (float) $v['qcparameter_snf'];?></td>
                                </tr>
                            <?php } ?>
                        <?php } ?>
                        </tbody>
                    </table>
                    <div class="grid-pagging border-top margin-bottom-30"></div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="<?php echo BASE_URL;?>themes/default/jquery-tokeninput/src/jquery.tokeninput.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.tab-selector').on('click',function(){
                var id = $(this).attr('data-id');
                $('.tab-selector:not(.fg-lightgray)').addClass('fg-lightgray');
                $(this).removeClass('fg-lightgray');
                $('.chartContainer').addClass('hidden');
                $('#chartContainer'+id).removeClass('hidden');
                var chartnya = $('#widget_transaksi_chart'+id).highcharts();
                chartnya.setSize($('.chartContainer').width(),$('.chartContainer').height(),true);
            });
            $('.filter-select').click(function(){
                var id = $(this).attr('data-href');
                $('.filter-select').removeClass('fg-lightgray');
                $('.filter-select').not(this).addClass('fg-lightgray');
                $('.filter-container').not(id).hide();
                $(id).show();
            });
            $('#btnfilter-anggota').click(function(){
                var anggota = $('#inputfilter-angggota').val();
                window.location.href = '<?php echo BASE_URL;?>index.php?/report/qcparameter/&filterby=anggota&anggota='+anggota+'&periode=<?php echo $periode;?>&year=<?php echo $_GET['year'];?>&date1=<?php echo $_GET['date1'];?>&date2=<?php echo $_GET['date2'];?>&page=';
            });
            $('#btnfilter-koperasi').click(function(){
                var koperasi = $('#inputfilter-koperasi').val();
                window.location.href = '<?php echo BASE_URL;?>index.php?/report/qcparameter/&filterby=koperasi&koperasi='+koperasi+'&periode=<?php echo $periode;?>&year=<?php echo $_GET['year'];?>&date1=<?php echo $_GET['date1'];?>&date2=<?php echo $_GET['date2'];?>&page=';
            });

            $('#inputfilter-angggota').tokenInput('<?php echo BASE_URL;?>index.php?/load/autocompleteanggota/'+$('#inputfilter-koperasi').val(),{
                'placeholder': 'farmer: ',
                method:'POST',
                tokenLimit:1,
                theme: "facebook",
                prePopulate:<?php echo json_encode($prepopulate);?>
            });
            $('.grid-pagging').transpaginator({
                totalPages: <?php echo $data['total_page'];?>,
                currentPage: <?php echo $data['page'];?>,
                pageUrl: '<?php echo BASE_URL;?>index.php?/report/qcparameter/&filterby=<?php echo $_GET['filterby'];?>&koperasi=<?php echo $_GET['koperasi'];?>&anggota=<?php echo $_GET['anggota'];?>&periode=<?php echo $periode;?>&year=<?php echo $_GET['year'];?>&date1=<?php echo $_GET['date1'];?>&date2=<?php echo $_GET['date2'];?>&page='
            });

        });
        function reloadFilter(){
            var koperasi = $('#inputfilter-koperasi').val();
            var tpk = $('#inputfilter-tpk').val();
            var anggota = $('#inputfilter-angggota').val();
            var date1 = $('#date1').val();
            var date2 = $('#date2').val();
            var year = $('#year').val();
            openUrl('report/qcparameter/&filterby=<?php echo $_GET['filterby'];?>&koperasi='+koperasi+'&tpk='+tpk+'&anggota='+anggota+'&periode=<?php echo $periode;?>&year='+year+'&date1='+date1+'&date2='+date2);
        }
        function reloadReport(){
            var date1 = $('#date1').val();
            var date2 = $('#date2').val();
            var year = $('#year').val();
            openUrl('report/qcparameter/&filterby=<?php echo $_GET['filterby'];?>&koperasi=<?php echo $_GET['koperasi'];?>&tpk=<?php echo $_GET['tpk'];?>&anggota=<?php echo $_GET['anggota'];?>&periode=<?php echo $periode;?>&year='+year+'&date1='+date1+'&date2='+date2);
        }
    </script>
<?php View::inc('footer.php');?>
