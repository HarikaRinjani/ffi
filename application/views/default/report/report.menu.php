<div class="sidebar">
    <span id="resize-handler" class="hidden-xs hidden-sm ui-resizable-handle ui-resizable-e">&nbsp;</span>
    <a href="javascript:void(0);" class="visible-sm toggleSidebar"><i class="ion-ios7-more"></i></a>
    <?php View::inc('koperasilogo.php');?>
    <ul class="navigation icon">
        <?php View::inc('bookmark/bookmark.php');?>
        <li>
            <a href="javascript:void(0);" class="navtoggle"><i class="ion-chevron-right"></i></a>
            <h3 class="title">Report</h3>
            <ul>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/report/index"><i class="ion-ios7-information-outline"></i>Overview</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/report/transaction"><i class="ion-ios7-cart-outline"></i>Milk Collection</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/report/qcparameter"><i class="ion-ios7-lightbulb-outline"></i>Quality</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/report/distribution"><i class="ion-ios7-navigate-outline"></i>Distribution</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/report/datalog"><i class="ion-ios7-loop"></i>Sync</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/samplingschedule/index"><i class="ion-ios7-calendar-outline"></i>Sampling Schedule</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/milkprice/report"><i class="ion-ios7-calculator-outline"></i>Milk Price</a>
                </li>
            </ul>
        </li>
    </ul>

</div>
