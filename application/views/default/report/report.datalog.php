<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('report/report.menu.php');?>
    <div class="content padded">
        <div class="breadcrumb">
            <ul class="tab">
                <li class="active">
                    <a href="">Sync Logs</a>
                </li>
            </ul>
            <div class="pull-right">
                <ul class="breadcrumb-nav">
                    <li>
                        <a href="">Report</a>
                    </li>
                    <li>
                        <a href="">Sync Logs</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="ribbon border-bottom">
                    <div class="menu">
                        <a href=""><i class="ion-ios7-loop"></i>logs</a>
                    </div>
                </div>
            	<div class="padding-bottom-40 ">
                    <table class="table">
                        <tr>
                            <th>Date</th>
                            <th>Client ID</th>
                            <th>Messages</th>
                            <th></th>
                        </tr>
                        <?php if($log['total_data'] === 0){ ?>
                        <tr>
                            <td colspan="100%"><?php echo _('No record found');?></td>
                        </tr>
                        <?php } ?>
                        <?php foreach($log['records'] as $k=>$v){ ?>
                        <tr>
                            <td class="format-datetime"><?php echo $v['datalog_time'];?></td>
                            <td><?php echo $v['datalog_client'];?></td>
                            <td><?php echo $v['datalog_data'];?></td>
                            <td></td>
                        </tr>
                        <?php } ?>
                    </table>
                    <div class="grid-pagging border-top margin-bottom-30"></div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.grid-pagging').transpaginator({
                totalPages: <?php echo $log['total_page'];?>,
                currentPage: <?php echo $log['page'];?>,
                pageUrl: '<?php echo BASE_URL;?>index.php?/report/datalog/&page='
            });

        });
    </script>
<?php View::inc('footer.php');?>
