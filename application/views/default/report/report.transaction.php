<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('report/report.menu.php');?>
<link rel="stylesheet" href="<?php echo BASE_URL;?>themes/default/jquery-tokeninput/styles/token-input-facebook.css"/>
    <div class="content padded">
        <div class="breadcrumb">
            <ul class="tab">
                <li class="<?php if($periode == 'daterange'){ echo 'active'; } ?>">
                    <a href="<?php echo BASE_URL;?>index.php?/report/transaction/total/&amp;periode=daterange" class="">date-range</a>
                </li>
                <li class="<?php if($periode == 'monthly'){ echo 'active'; } ?>">
                    <a href="<?php echo BASE_URL;?>index.php?/report/transaction/total/&amp;periode=monthly" class="">monthly</a>
                </li>
                <li class="<?php if($periode == 'yearly'){ echo 'active'; } ?>">
                    <a href="<?php echo BASE_URL;?>index.php?/report/transaction/total/&amp;periode=yearly" class="">Yearly</a>
                </li>
                <!-- <li class="active">
                    <a href="">Total</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/report/transaction/tpk">TPK</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/report/transaction/koperasi">Koperasi</a>
                </li> -->
            </ul>
            <div class="pull-right">
                <ul class="breadcrumb-nav">
                    <li>
                        <a href="">Report</a>
                    </li>
                    <li>
                        <a href="">Milk Collection</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="ribbon col-md-12">
                    <div class="menu">
                        <a href="javascript:void(0);" class="no-hover"><i class="ion-ios7-gear-outline"></i> Coop</a>
                        <select name="filterkoperasi" id="filterkoperasi" class="pull-left" style="width:100px;" onchange="reloadReport();">
                            <option value="">All</option>
                            <?php
                            foreach($koperasi as $k=>$v){
                                echo '<option value="'.$v['koperasi_id'].'"';
                                if($v['koperasi_id'] == $_GET['koperasi']){
                                    echo ' selected="selected"';
                                }
                                echo '>'.$v['koperasi_nama'].'</option>';
                            }
                            ?>
                        </select>
                        <a href="javascript:void(0);" class="no-hover"><i class="ion-ios7-home-outline"></i> Collection Point</a>
                        <select name="filtertpk" id="filtertpk" class="pull-left" style="width:100px;" onchange="reloadReport();">
                            <option value="">All</option>
                            <?php
                            foreach($tpk as $k=>$v){
                                echo '<option value="'.$v['tpk_id'].'"';
                                if($v['tpk_id'] == $_GET['tpk']){
                                    echo ' selected="selected"';
                                }
                                echo '>'.$v['tpk_nama'].'</option>';
                            }
                            ?>
                        </select>
                        <a href="javascript:void(0);" class="no-hover"><i class="ion-ios7-contact-outline"></i>Farmer:</a>
                        <div class="pull-left" style="width:150px;">
                            <input type="text" class="input-block" id="inputfilter-angggota" onchange="reloadReport();">
                        </div>
                    </div>
                    <div class="pull-right">
                        <span class="padding-right-10"><i class="ion-ios7-calendar-outline" style="font-size:18px;vertical-align:middle;"></i> Periode:</span>
                        <?php if($periode == 'daterange'){ ?>
                            <input type="text" id="date1" value="<?php echo $_GET['date1'];?>" class="datepicker" style="width:100px" onchange="reloadReport();"/>
                            <span class="padding-left-5 padding-right-5">:</span>
                            <input type="text" id="date2" value="<?php echo $_GET['date2'];?>" class="datepicker"  style="width:100px" onchange="reloadReport();"/>
                            <input name="year" id="year" value="<?php echo $_GET['year'];?>" type="hidden"/>
                        <?php }elseif($periode == 'monthly'){ ?>
                            <select name="date1" id="date1" class="month" data-value="<?php echo $_GET['date1'];?>"  style="width:100px" onchange="reloadReport();">
                                <option value="">-- loading data --</option>
                            </select>
                            <select name="date2" id="date2" class="month" data-value="<?php echo $_GET['date2'];?>"  style="width:100px" onchange="reloadReport();">
                                <option value="">-- loading data --</option>
                            </select>
                            <select name="year" id="year" class="year" data-value="<?php echo $_GET['year'];?>"  style="width:auto" onchange="reloadReport();">
                                <option value="">-- loading data --</option>
                            </select>
                        <?php }else{ ?>
                            <select name="year" id="date1" class="year" data-value="<?php echo $_GET['date1'];?>"  style="width:auto" onchange="reloadReport();">
                                <option value="">-- loading data --</option>
                            </select>
                            <select name="year" id="date2" class="year" data-value="<?php echo $_GET['date2'];?>"  style="width:auto" onchange="reloadReport();">
                                <option value="">-- loading data --</option>
                            </select>
                        <?php } ?>
                        <select name="filtersesi" id="filtersesi" style="width:80px;" onchange="reloadReport();">
                            <option value="">All</option>
                            <option value="p" <?php if($_GET['sesi'] == 'p'){ echo 'selected="selected"'; } ?>>Morning</option>
                            <option value="s" <?php if($_GET['sesi'] == 's'){ echo 'selected="selected"'; } ?>>Afternoon</option>
                        </select>
                    </div>
                </div>
               <div class="col-md-12">
                   <div class="padding-10 margin-top-10 border"><?php echo $transaksi_chart1;?></div>
               </div>
               <div class="col-md-12">
                   <div class="padding-10 margin-top-10 border"><?php echo $transaksi_chart2;?></div>
               </div>
            </div>
            <div class="row margin-top-20">
            	<div class="col-md-12">
            		<h2 class="pull-left"><i class="ion-ios7-folder-outline"></i> Detail Transaksi</h2>
                    <h3 class="pull-right">
                        <a href="<?php echo BASE_URL; ?>index.php?/report/export/transaction/&periode=<?php echo $periode;?>&year=<?php echo $_GET['year'];?>&date1=<?php echo $_GET['date1'];?>&date2=<?php echo $_GET['date2'];?>&anggota=<?php echo $_GET['anggota'];?>" title="Export" target="_blank">
                            <i class="ion-ios7-download-outline"></i> export
                        </a>
                    </h3>
            	</div>
                <div class="clearfix"></div>
            	<div class="padding-bottom-40 ">
                    <table class="table border-top">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>TPK</th>
                                <th>Tanggal</th>
                                <th>Session</th>
                                <th>ID Transaksi</th>
                                <th>ID Member</th>
                                <th>Nama</th>
                                <th>Total Berat (Kg)</th>
                                <th>Data Sync</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if($transaksi['total_data'] === 0){ ?>
                            <tr>
                                <td colspan="100%">No record found</td>
                            </tr>
                            <?php } ?>
                            <?php foreach($transaksi['records'] as $k=>$v){ ?>
                            <tr>
                                <td>
                                    <?php
                                    if($transaksi['page'] == 1){
                                        echo ($k+1);
                                    }else{
                                        $no = ($transaksi['limit'] * ($transaksi['page'] - 1)) + ($k+1);
                                        echo $no;
                                    }
                                    ?>
                                </td>
                                <td><?php echo $v['tpk_nama'];?></td>
                                <td><?php echo date('d F Y H:i',strtotime($v['transaksi_waktu']));?></td>
                                <td class="samplingsesi"><?php echo $v['transaksi_sesi'];?></td>
                                <td><?php echo $v['transaksi_id'];?></td>
                                <td><?php echo $v['transaksi_nomor'];?></td>
                                <td><?php echo $v['transaksi_nama'];?></td>
                                <td><?php echo $v['transaksi_totalberat'];?></td>
                                <td><span class="syncstatus"><?php echo $v['transaksi_sync'];?></span></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <div class="grid-pagging border-top margin-bottom-30"></div>
                </div>
            </div>
        </div>
    </div>
     <script type="text/javascript" src="<?php echo BASE_URL;?>themes/default/jquery-tokeninput/src/jquery.tokeninput.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.grid-pagging').transpaginator({
                totalPages: <?php echo $transaksi['total_page'];?>,
                currentPage: <?php echo $transaksi['page'];?>,
                pageUrl: '<?php echo BASE_URL;?>index.php?/report/transaction/&sesi=<?php echo $_GET['sesi'];?>&koperasi=<?php echo $_GET['koperasi'];?>&tpk=<?php echo $_GET['tpk'];?>&periode=<?php echo $periode;?>&year=<?php echo $_GET['year'];?>&date1=<?php echo $_GET['date1'];?>&date2=<?php echo $_GET['date2'];?>&anggota=<?php echo $_GET['anggota'];?>&page='
            });
            $('#reloadReport').on('click',function(){

            });
            $('#inputfilter-angggota').tokenInput('<?php echo BASE_URL;?>index.php?/load/autocompleteanggota/'+$('#filterkoperasi').val(),{
                'placeholder': 'farmer: ',
                method:'POST',
                tokenLimit:1,
                theme: "facebook",
                prePopulate:<?php echo json_encode($prepopulate);?>
            });
        });
        function reloadReport(){
            var date1    = $('#date1').val();
            var date2    = $('#date2').val();
            var koperasi = $('#filterkoperasi').val();
            var tpk      = $('#filtertpk').val();
            var year     = $('#year').val();
            var anggota  = $('#inputfilter-angggota').val();
            var sesi = $('#filtersesi').val();
            openUrl('report/transaction/&sesi='+sesi+'&koperasi='+koperasi+'&tpk='+tpk+'&periode=<?php echo $periode;?>&year='+year+'&date1='+date1+'&date2='+date2+'&anggota='+anggota);
        }
    </script>
<?php View::inc('footer.php');?>
