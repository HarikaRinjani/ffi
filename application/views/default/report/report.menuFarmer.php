<div class="sidebar">
    <span id="resize-handler" class="hidden-xs hidden-sm ui-resizable-handle ui-resizable-e">&nbsp;</span>
    <a href="javascript:void(0);" class="visible-sm toggleSidebar"><i class="ion-ios7-more"></i></a>
    <?php View::inc('koperasilogo.php');?>
    <ul class="navigation icon">
        <?php View::inc('bookmark/bookmark.php');?>
        <li>
            <a href="javascript:void(0);" class="navtoggle"><i class="ion-chevron-right"></i></a>
            <h3 class="title">Report</h3>
            <ul class="active">
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/report/farmer"><i class="ion-ios7-information-outline"></i>Overview</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/report/transactionFarmer"><i class="ion-ios7-cart-outline"></i>Milk Collection</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/milkprice/reportFarmer"><i class="ion-ios7-calculator-outline"></i>Milk Price</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/report/pendapatan"><i class="ion-ios7-folder-outline"></i>Pendapatan</a>
                </li>
            </ul>
        </li>
    </ul>

</div>

