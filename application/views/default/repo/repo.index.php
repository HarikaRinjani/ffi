<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('system/system.menu.php');?>
<div class="content padded">
    <div class="breadcrumb">
        <ul class="tab">
            <li class="active">
                <a href="">Repository</a>
            </li>

        </ul>
        <div class="pull-right">
            <ul class="breadcrumb-nav">
                <li>
                    <a href="">System Settings</a>
                </li>
                <li>
                    <a href="">Repository</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="ribbon border-bottom">
                <div class="menu">
                    <a href="index.php?/repo/update"><i class="ion-ios7-loop"></i> Update Repository</a>
                </div>
            </div>
             <table class="table border dotted no-hover fixedheader" style="border:0;">
                <thead>
                    <tr>
                        <th>Repository</th>
                        <th>Total Data</th>
                        <th>Last Update</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Cooperative</td>
                        <td><?php echo $koperasi['total_data'];?></td>
                        <td>
                            <span class="format-datetime"><?php echo @$koperasi['records'][0]['updated_at'];?></span>
                        </td>
                    </tr><tr>
                        <td>USER</td>
                        <td><?php echo $user['total_data'];?></td>
                        <td>
                            <span class="format-datetime"><?php echo @$user['records'][0]['updated_at'];?></span>
                        </td>
                    </tr><tr>
                        <td>FARMER</td>
                        <td><?php echo $peternak['total_data'];?></td>
                        <td>
                            <span class="format-datetime"><?php echo @$peternak['records'][0]['updated_at'];?></span>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="padding-15">
                <p>*) <strong>Cooperative</strong> include: Cooperative and Collection Point Data</p>
                <p>*) <strong>Farmer</strong> include: Farmer, Card and Cow Population Data</p>
                <p style="color:red">*) All repository has active, non active and deleted data</p>
            </div>
        </div>
    </div>
</div>
<?php View::inc('footer.php');?>
