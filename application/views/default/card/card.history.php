<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('settings/settings.menu.php');?>
<div class="content padded" xmlns="http://www.w3.org/1999/html">
    <div class="breadcrumb">
        <ul class="tab">
            <li class="active">
                <a href="<?php echo BASE_URL;?>index.php?/card/index" disabled="disabled">CARDS HISTORY</a>
            </li>
            <li>
                <a href="<?php echo BASE_URL;?>index.php?/card/inactive">INACTIVE CARDS</a>
            </li>
        </ul>
        <div class="pull-right">
            <ul class="breadcrumb-nav">
                <li>
                    <a href="">Apps Settings</a>
                </li>
                <li>
                    <a href="">Card Management</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="ribbon text-right">
                <div class="menu">
                    <a href="<?php echo BASE_URL;?>index.php?/card/index"><i class="ion-ios7-arrow-left"></i> Back to Card Lists</a>
                </div>
                <!-- <h1 class="pull-left fg-gray" style="margin-top:-10px;">
                    <a href="" class="fg-gray padding-right-10">lists</a>
                    <a href="javascript:void(0);" id="__edit" class="padding-right-10 fg-lightgray editbtn">edit</a>
                    <a href="javascript:void(0);" id="__delete" class="padding-right-10 fg-lightgray editbtn">delete</a>
                </h1> -->
                <form action="javascript:void(0);" id="searchForm" onsubmit="return filter();">
                    <input type="text" placeholder="Search..." id="keyword" style="width:200px" value="<?php echo $card['keyword'];?>">
                </form>
            </div>
            <div class="padding-left-15 padding-right-15">
                <h3 class="no-padding margin-bottom-10">Farmer Detail:</h3>
                <div class="row">
                    <div class="col-md-2">
                        <img src="<?php echo $anggota['anggota_photo'];?>" alt="" class="img-responsive">
                    </div>
                    <div class="col-md-10">
                        <table class="table no-border no-hover" style="margin-top:-10px;">
                            <tr>
                                <td style="width:150px;">Farmer ID</td>
                                <td><?php echo $anggota['anggota_nomor'];?></td>
                            </tr>
                            <tr>
                                <td>Name</td>
                                <td><?php echo $anggota['anggota_nama'];?></td>
                            </tr>
                            <tr>
                                <td>Coop</td>
                                <td><?php echo $anggota['koperasi_nama'];?></td>
                            </tr>
                            <tr>
                                <td>Collection Point</td>
                                <td><?php echo $anggota['tpk_nama'];?></td>
                            </tr>
                            <tr>
                                <td>Group</td>
                                <td><?php echo $anggota['kelompok_nama'];?></td>
                            </tr>
                            <tr>
                                <td>Phone</td>
                                <td><?php echo $anggota['anggota_nohp'];?></td>
                            </tr>
                            <tr>
                                <td>Address</td>
                                <td><?php echo $anggota['anggota_alamat'];?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <table class="table table-hover border-top margin-top-20">
                <thead>
                <tr>
                    <th class="border-right dotted" style="width:24px;">#</th>
                    <th class="border-right dotted">Card Number</th>
                    <th class="border-right dotted">Modified By</th>
                    <th class="border-right dotted">Remark</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody>
                <?php if($card['total_data'] == 0){ ?>
                    <tr>
                        <td colspan="9"><?php echo _('No record found');?></td>
                    </tr>
                <?php }else{ ?>
                    <?php foreach($card['records'] as $k=>$v){ ?>
                        <tr class="tr_selector cursor-pointer" data-id="<?php echo $v['card_id'];?>" id="tr<?php echo $v['card_id'];?>">
                            <td  class="border-right dotted">
                                <?php
                                if($card['page'] == 1){
                                    echo ($k+1);
                                }else{
                                    echo ($k + 1 + ($card['limit'] * ($card['page'] - 1)));
                                }
                                ?>
                            </td>
                            <td class="border-right dotted"><?php echo $v['card_number'];?></td>
                            <td class="border-right dotted"><?php echo $v['user_fullname'];?>, <span class="format-datetime"><?php echo $v['card_entrydate'];?></span></td>
                            <td class="border-right dotted" ><?php echo $v['card_remark'];?></td>
                            <td class="status" style="width:100px;"><?php echo $v['card_status'];?></td>
                        </tr>
                    <?php } ?>
                <?php } ?>
                </tbody>
            </table>
            <div class="grid-pagging border-top margin-bottom-30"></div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('.grid-pagging').transpaginator({
            totalPages: <?php echo $card['total_page'];?>,
            currentPage: <?php echo $card['page'];?>,
            totalData: <?php echo $card['total_data'];?>,
            pageUrl: '<?php echo BASE_URL;?>index.php?/card/history/<?php echo $card_id;?>/&keyword=<?php echo $card['keyword'];?>&page='
        });
        $('.tr_selector').on('click',function(){
            var id = $(this).attr('data-id');
            $('.tr_selector').removeClass('active');
            $(this).addClass('active');
        });
        
    })

    function filter(){
        var keyword = $('#keyword').val();
        window.location.href = '<?php echo BASE_URL;?>index.php?/card/history/<?php echo $card_id;?>/&keyword='+keyword+'&page=1';
    }
</script>
<?php View::inc('footer.php');?>
