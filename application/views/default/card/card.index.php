<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('settings/settings.menu.php');?>
<div class="content padded" xmlns="http://www.w3.org/1999/html">
    <div class="breadcrumb">
        <ul class="tab">
            <li class="active">
                <a href="<?php echo BASE_URL;?>index.php?/card/index" disabled="disabled">ACTIVE CARDS</a>
            </li>
            <li>
                <a href="<?php echo BASE_URL;?>index.php?/card/inactive">INACTIVE CARDS</a>
            </li>
        </ul>
        <div class="pull-right">
            <ul class="breadcrumb-nav">
                <li>
                    <a href="">Apps Settings</a>
                </li>
                <li>
                    <a href="">Card Management</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="ribbon text-right">
                <div class="menu">
                    <a href="javascript:void(0);" id="__generate" class="edit fg-lightgray editbtn"><i class="ion-ios7-reload"></i>Regenerate Card Number</a>
                    <a href="javascript:void(0);" id="__history" class="edit fg-lightgray editbtn"><i class="ion-ios7-time-outline"></i>History</a>
                </div>
                <!-- <h1 class="pull-left fg-gray" style="margin-top:-10px;">
                    <a href="" class="fg-gray padding-right-10">lists</a>
                    <a href="javascript:void(0);" id="__edit" class="padding-right-10 fg-lightgray editbtn">edit</a>
                    <a href="javascript:void(0);" id="__delete" class="padding-right-10 fg-lightgray editbtn">delete</a>
                </h1> -->
                <form action="javascript:void(0);" id="searchForm" onsubmit="return filter();">
                    <?php if(!in_array(session::get('user_group'),Config::$application['super_group'])){ ?>
                    <input type="hidden" name="koperasi" id="koperasi" value="<?php echo session::get('user_koperasi');?>">
                    <?php }else{ ?>
                    <select name="koperasi" id="koperasi" onchange="return filter();">
                        <option value="">All</option>
                        <?php foreach($koperasi['records'] as $k=>$v){ ?>
                            <option value="<?php echo $v['koperasi_id'];?>" <?php if($v['koperasi_id'] === $_GET['koperasi']){ echo 'selected="selected"'; } ?>><?php echo $v['koperasi_nama'];?></option>
                        <?php } ?>
                    </select>
                    <?php } ?>
                    <input type="text" placeholder="Search..." id="keyword" style="width:200px" value="<?php echo $card['keyword'];?>">
                </form>
            </div>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th style="width:24px;">#</th>
                    <th><?php echo _('Farmer ID');?></th>
                    <th><?php echo _('Card Number');?></th>
                    <th><?php echo _('Name');?></th>
                    <th><?php echo _('Cooperative');?></th>
                    <th><?php echo _('Collection Point');?></th>
                    <th><?php echo _('Group');?></th>
                </tr>
                </thead>
                <tbody>
                <?php if($card['total_data'] == 0){ ?>
                    <tr>
                        <td colspan="9"><?php echo _('No record found');?></td>
                    </tr>
                <?php }else{ ?>
                    <?php foreach($card['records'] as $k=>$v){ ?>
                        <tr class="tr_selector cursor-pointer" data-id="<?php echo $v['card_id'];?>" id="tr<?php echo $v['card_id'];?>">
                            <td>
                                <?php
                                if($card['page'] == 1){
                                    echo ($k+1);
                                }else{
                                    echo ($k + 1 + ($card['limit'] * ($card['page'] - 1)));
                                }
                                if($v['anggota_photo'] == '' || $v['anggota_photo'] == null){
                                    if(file_exists(Config::$application['file_path'].'images'.DS.'PASFOTO'.DS.$v['anggota_nomor'].'.JPG')){
                                        $photo = 'index.php?/load/file/'.urlencode('images/PASFOTO/'.$v['anggota_nomor'].'.JPG');
                                    }else{
                                        $photo = 'index.php?/load/file/'.urlencode('images/avatar.jpg');
                                    }
                                }else{
                                    $photo = $v['anggota_photo'];
                                }
                                ?>
                            </td>
                            <td><?php echo $v['anggota_nomor'];?></td>
                            <td><?php echo $v['card_number'];?></td>
                            <td><img src="<?php echo $photo; ?>" alt="" style="float:left;margin-right:5px;width:28px;height:28px;"/><?php echo $v['anggota_nama'];?></td>
                            <td><?php echo $v['koperasi_nama'];?></td>
                            <td><?php echo $v['tpk_nama'];?></td>
                            <td><?php echo $v['kelompok_nama'];?></td>
                        </tr>
                    <?php } ?>
                <?php } ?>
                </tbody>
            </table>
            <div class="grid-pagging border-top margin-bottom-30"></div>
        </div>
    </div>
</div>
<div class="modal" id="generatenumber">
    <div class="modal-dialog modal-sm">
        <div class="modal-header">
            <h3 class="modal-title">Enter Remark!</h3>
            <a href="javascript:void(0);" class="close">&times;</a>
        </div>
        <div class="modal-body">
            <form action="<?php echo BASE_URL;?>index.php?/card/generate/" method="post">
                <div class="form-group">
                    <input type="hidden" name="card_id" id="generate-card_id">
                    <div class="col-md-12">
                        <input type="radio" class="card_remark" name="card_remark" id="card_remark_1" value="lost">
                        <label for="card_remark_1">Lost</label>
                    </div> 
                    <div class="col-md-12">
                        <input type="radio" class="card_remark" name="card_remark" id="card_remark_2" value="broken">
                        <label for="card_remark_2">Broken</label>
                    </div> 
                    <div class="col-md-12">
                        <input type="radio" class="card_remark" name="card_remark" id="card_remark_3" value="other">
                        <label for="card_remark_3">Other</label>
                    </div> 
                    <div class="col-md-12 padding-left-20">
                        <textarea class="input-block" id="card_remark_other" name="card_remark_other" placeholder="Other remark" disabled="disabled"></textarea>
                    </div>
                </div>
                <div class="padding-top-15 text-right">
                    <button type="submit" class="default">Generate</button>
                </div>
            </form>
        </div>
    </div>
    
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.grid-pagging').transpaginator({
            totalPages: <?php echo $card['total_page'];?>,
            currentPage: <?php echo $card['page'];?>,
            totalData: <?php echo $card['total_data'];?>,
            pageUrl: '<?php echo BASE_URL;?>index.php?/card/index/&koperasi=<?php echo $_GET['koperasi'];?>&keyword=<?php echo $card['keyword'];?>&page='
        });
        $('.tr_selector').on('click',function(){
            var id = $(this).attr('data-id');
            $('.editbtn').removeClass('fg-lightgray').removeAttr('disabled').attr('data-id',id);
            $('.tr_selector').removeClass('active');
            $(this).addClass('active');
        });
        $('#__history').on('click',function(){
            var id = $(this).attr('data-id');
            if(typeof id !== 'undefined' && id !== ''){
                location.href = '<?php echo BASE_URL;?>index.php?/card/history/'+id;
            }
        });
        $('.card_remark').on('click',function(){
            var remark = $(this).val();
            if(remark === 'other'){
                $('#card_remark_other').removeAttr('disabled').focus();
            }else{
                $('#card_remark_other').attr('disabled','disabled');
            }
        });
        $('#__generate').on('click',function(){
            var id = $(this).attr('data-id');
            if(typeof id !== 'undefined' && id !== ''){
                $('#generate-card_id').val(id);
                $('#generatenumber').addClass('fadein');
            }
        });
    })

    function filter(){
        var keyword = $('#keyword').val();
        var koperasi=  $('#koperasi').val();
        window.location.href = '<?php echo BASE_URL;?>index.php?/card/index/&koperasi='+koperasi+'&keyword='+keyword+'&page=1';
    }
</script>
<?php View::inc('footer.php');?>
