<?php View::inc('header.php');?>
<?php if(Config::$application['is_client'] === true && Config::$application['enable_onscreenkeyboard'] === true){ ?>
<link rel="stylesheet" href="<?php echo BASE_URL;?>themes/default/css/onScreenKeyboard.css">
<script type="text/javascript" src="<?php echo BASE_URL;?>themes/default/js/jquery.onScreenKeyboard.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.input-osk').onScreenKeyboard({
            rewireReturn : 'submit',
            rewireTab : true
        });
    });
</script>
<?php } ?>
<script type="text/javascript" src="<?php echo BASE_URL;?>themes/default/js/jquery.backstretch.min.js"></script>
    <div class="blur" id="wallpaper" style="position:fixed;top:0;left:0;bottom:0;right:0;width:100%;height:100%;z-index:-1;">
        &nbsp;
    </div>
    <div style="position: fixed;top:40px;left:40px;z-index:999999;">
		<h1 class="clock fg-white  no-margin" id="clock" style="text-shadow:1px 2px 5px #999;font-size:84px;"><?php echo date('H:i');?></h1>
        <h5 class="fg-white no-margin padding-left-10" style="text-shadow:1px 2px 5px #999;font-size:18px;"><?php echo date('l d F Y');?></h5>
	</div>
    <div class="modal drop fadein">
        <div class="modal-dialog" style="width:350px;top:80px;margin-left:-175px;">
            <div class="modal-body">
                <form method="post" action="<?php echo BASE_URL;?>index.php?/login/process">
                    <div class="padding-5">
                        <?php
                        if(empty(Config::$application['login_logo'])){
                            $logo = BASE_URL.'themes/default/images/frieslandcampina.png';
                        }else{
                            $logo = Config::$application['login_logo'];
                        }
                        ?>
                        <img src="<?php echo $logo;?>" alt=""/>
                    </div>
                    <div class="padding-5">
                        <small>Please enter your username and password</small>
                    </div>
                    <div class="padding-5">
                        <input type="text" placeholder="Username" name="user_name" id="user_name" class="input-block input-osk" required/>
                    </div>
                    <div class="padding-5">
                        <input type="password" placeholder="Password" name="user_password" id="user_password" class="input-block input-osk" required/>
                    </div>
                    <div class="padding-5">
                        <input type="checkbox" name="remember" id="remember"/>
                        <label for="remember">Remember me</label>
                    </div>
                    <?php if(session::check('errorMsg')){ ?>
                        <div class="fg-red padding-5 margin-bottom-10 margin-top-5" ng-click="error.status=false">
                            <?php echo session::get('errorMsg');?>
                        </div>
                    <?php } ?>
                    <div class="padding-5">
                        <button type="submit" class="btn default">login</button>
                    </div>
                </form>
                <div class="padding-5">
                    <p class="margin-bottom-5">
                        <a href="javascript:void(0)" data-toggle="modal" data-target="#modal-forgotpassword"><small>Forgot username or password</small></a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="modal border" id="modal-forgotpassword">
        <div class="modal-dialog modal-sm" style="top:120px;border-color:#DDD !important;">
            <div class="modal-header" style="background:#DDD !important;">
                <h3 class="modal-title fg-gray">Forgot Password</h3>
                <a href="javascript:void(0);" class="close">&times;</a>
            </div>
            <div class="modal-body">
                <div class="text-center">
                    <img src="<?php echo BASE_URL;?>themes/default/images/logo-ffi.png" alt="" />
                </div>
                <p>
                    If you have problem  with your username or password please contact your system Administrator or FFI ICT Dept.
                </p>
                <div class="text-right padding-top-15">
                    <button data-dismiss="modal">close</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $('body').css('background','transparent');
            $('#wallpaper').backstretch(["<?php echo BASE_URL;?>files/images/cow.jpg"],{duration: 2000, fade:750});
            GetClock();
        });
        function GetClock() {
            d = new Date();
            nday = d.getDay();
            nmonth = d.getMonth();
            ndate = d.getDate();
            nyear = d.getYear();
            nhour = d.getHours();
            nmin = d.getMinutes();
            nsec = d.getSeconds();

            if (nyear < 1000) nyear = nyear + 1900;

            if (nhour == 0) {
                ap = " AM";
                nhour = 12;
            }
            else if (nhour <= 11) {
                ap = " AM";
            }
            else if (nhour == 12) {
                ap = " PM";
            }
            else if (nhour >= 13) {
                ap = " PM";
                nhour -= 12;
            }

            if (nmin <= 9) {
                nmin = "0" + nmin;
            }
            if (nsec <= 9) {
                nsec = "0" + nsec;
            }

            document.getElementById('clock').innerHTML = nhour+ ":" + nmin + ap + "";
            setTimeout("GetClock()", 1000);
        }
    </script>
<?php View::inc('footer.php');?>
