<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('settings/settings.menu.php');?>
<div class="content padded" xmlns="http://www.w3.org/1999/html">
    <div class="breadcrumb">
        <ul class="tab">
            <li class="active">
                <a href="<?php echo BASE_URL;?>index.php?/card/index" disabled="disabled">Cow Population</a>
            </li>
        </ul>
        <div class="pull-right">
            <ul class="breadcrumb-nav">
                <li>
                    <a href="">Apps Settings</a>
                </li>
                <li>
                    <a href="">Cow Population</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="ribbon text-right">
                <div class="menu">
                    <a href="javascript:void(0);" onclick="window.history.back();" id="__edit" class="edit"><i class="ion-ios7-arrow-left"></i>Back</a>
                </div>
                <!-- <h1 class="pull-left fg-gray" style="margin-top:-10px;">
                    <a href="" class="fg-gray padding-right-10">lists</a>
                    <a href="javascript:void(0);" id="__edit" class="padding-right-10 fg-lightgray editbtn">edit</a>
                    <a href="javascript:void(0);" id="__delete" class="padding-right-10 fg-lightgray editbtn">delete</a>
                </h1> -->
            </div>
            <div class="padding-left-15 padding-right-15">
                <h3 class="no-padding margin-bottom-10">Farmer Detail:</h3>
                <div class="row">
                    <div class="col-md-2">
                        <img src="<?php echo $anggota['anggota_photo'];?>" alt="" class="img-responsive">
                    </div>
                    <div class="col-md-10">
                        <table class="table no-border no-hover" style="margin-top:-10px;">
                            <tr>
                                <td style="width:150px;">Farmer ID</td>
                                <td><?php echo $anggota['anggota_nomor'];?></td>
                            </tr>
                            <tr>
                                <td>Name</td>
                                <td><?php echo $anggota['anggota_nama'];?></td>
                            </tr>
                            <tr>
                                <td>Coop</td>
                                <td><?php echo $anggota['koperasi_nama'];?></td>
                            </tr>
                            <tr>
                                <td>Collection Point</td>
                                <td><?php echo $anggota['tpk_nama'];?></td>
                            </tr>
                            <tr>
                                <td>Group</td>
                                <td><?php echo $anggota['kelompok_nama'];?></td>
                            </tr>
                            <tr>
                                <td>Phone</td>
                                <td><?php echo $anggota['anggota_nohp'];?></td>
                            </tr>
                            <tr>
                                <td>Address</td>
                                <td><?php echo $anggota['anggota_alamat'];?></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <table class="table border-top margin-top-15">
                <thead>
                    <tr>
                        <th class="border-right text-center" colspan="2">Population</th>
                        <th class="border-right text-center" colspan="2">Antibiotic</th>
                        <th class="text-center border-right" rowspan="2">Remark</th>
                        <th class="text-center" rowspan="2">Status</th>
                    </tr>
                    <tr>
                        <th class="border-right text-center">Lactating</th>
                        <th class="border-right text-center">Total</th>
                        <th class="border-right text-center">Status</th>
                        <th class="border-right text-center">Date</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if($data['total_data'] === 0){ ?>
                    <tr>
                        <td colspan="5">no record found</td>
                    </tr>
                    <?php } ?>
                    <?php foreach($data['records'] as $k=>$v){  ?>
                    <tr>
                        <td class="text-center border-right dotted"><?php echo $v['cow_lactating'];?></td>
                        <td class="text-center border-right dotted"><?php echo $v['cow_population'];?></td>
                        <td class="text-center border-right dotted">
                            <?php if($v['cow_antibiotic'] === 0){ ?>
                                <span class="bg-green fg-white padding-left-5 padding-right-5 padding-bottom-3 rounded">Negative</span>
                            <?php }else{ ?>
                                <span class="bg-red fg-white padding-left-5 padding-right-5 padding-bottom-3 rounded">Positive</span>
                            <?php } ?>
                        </td>
                        <td class="text-center border-right dotted">
                            <?php if($v['cow_antibiotic'] === 0){ ?>
                                -
                            <?php }else{ ?>
                                <div>
                                    <?php echo date('d/m/Y',strtotime($v['cow_antibioticstartdate'])).' - '.date('d/m/Y',strtotime($v['cow_antibioticenddate']));?>
                                </div>
                            <?php } ?>
                        </td>
                        <td class="border-right dotted">
                            <?php if(empty($v['cow_antibioticremark'])){ ?>
                            No remark
                            <?php }else{ echo $v['cow_antibioticremark']; } ?>
                        </td>
                        <td class="status text-center"><?php echo $v['cow_status'];?></td>
                    </tr>
                    <?php } ?>
                </tbody>
                
            </table>
            <div class="grid-pagging border-top margin-bottom-30"></div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('.grid-pagging').transpaginator({
            totalPages: <?php echo $data['total_page'];?>,
            currentPage: <?php echo $data['page'];?>,
            totalData: <?php echo $data['total_data'];?>,
            pageUrl: '<?php echo BASE_URL;?>index.php?/cow/management/&keyword=<?php echo $data['keyword'];?>&page='
        });
        
    })

    function filter(){
        var keyword = $('#keyword').val();
        window.location.href = '<?php echo BASE_URL;?>index.php?/cow/management/&keyword='+keyword+'&page=1';
    }
</script>
<?php View::inc('footer.php');?>
