<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('settings/settings.menu.php');?>
<div class="content padded" xmlns="http://www.w3.org/1999/html">
    <div class="breadcrumb">
        <ul class="tab">
            <li class="active">
                <a href="<?php echo BASE_URL;?>index.php?/card/index" disabled="disabled">Cow Population</a>
            </li>
        </ul>
        <div class="pull-right">
            <ul class="breadcrumb-nav">
                <li>
                    <a href="">Apps Settings</a>
                </li>
                <li>
                    <a href="">Cow Population</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="ribbon text-right">
                <div class="menu">
                    <a href="javascript:void(0);" id="__edit" class="edit fg-lightgray editbtn"><i class="ion-ios7-compose-outline"></i>Edit</a>
                    <a href="javascript:void(0);" id="__history" class="edit fg-lightgray editbtn"><i class="ion-ios7-time-outline"></i>History</a>
                </div>
                <!-- <h1 class="pull-left fg-gray" style="margin-top:-10px;">
                    <a href="" class="fg-gray padding-right-10">lists</a>
                    <a href="javascript:void(0);" id="__edit" class="padding-right-10 fg-lightgray editbtn">edit</a>
                    <a href="javascript:void(0);" id="__delete" class="padding-right-10 fg-lightgray editbtn">delete</a>
                </h1> -->
                <form action="javascript:void(0);" id="searchForm" onsubmit="return filter();">
                    <?php if(!in_array(session::get('user_group'),Config::$application['super_group'])){ ?>
                    <input type="hidden" name="koperasi" id="koperasi" value="<?php echo session::get('user_koperasi');?>">
                    <?php }else{ ?>
                    <select name="koperasi" id="koperasi" onchange="return filter();">
                        <option value="">All</option>
                        <?php foreach($koperasi['records'] as $k=>$v){ ?>
                            <option value="<?php echo $v['koperasi_id'];?>" <?php if($v['koperasi_id'] === $_GET['koperasi']){ echo 'selected="selected"'; } ?>><?php echo $v['koperasi_nama'];?></option>
                        <?php } ?>
                    </select>
                    <?php } ?>
                    <input type="text" placeholder="Search..." id="keyword" style="width:200px" value="<?php echo $data['keyword'];?>">
                </form>
            </div>
            <table class="table border dotted table-hover" style="border:none;">
                <thead>
                <tr>
                    <th class="border-right " rowspan="2" style="width:24px;">#</th>
                    <th class="border-right " rowspan="2"><?php echo _('Farmer ID');?></th>
                    <th class="border-right " rowspan="2"><?php echo _('Name');?></th>
                    <th class="border-right " rowspan="2"><?php echo _('Cooperative');?></th>
                    <th class="border-right " rowspan="2"><?php echo _('Collection Point');?></th>
                    <th class="border-right " rowspan="2"><?php echo _('Group');?></th>
                    <th class="border-right text-center" colspan="2"><?php echo _('Cow Population');?></th>
                    <th class="border-right text-center" colspan="2">Antibiotic</th>
                    <th rowspan=2 class="text-center"><i class="ion-ios7-email-outline" style="font-size:24px;"></i></th>
                </tr>
                <tr>
                    <th class="border-right text-center">Lactating</th>
                    <th class="border-right text-center">Total</th>
                    <th class="border-right text-center">Status</th>
                    <th class="text-center">Date</th>
                </tr>
                </thead>
                <tbody>
                <?php if($data['total_data'] == 0){ ?>
                    <tr>
                        <td colspan="9"><?php echo _('No record found');?></td>
                    </tr>
                <?php }else{ ?>
                    <?php foreach($data['records'] as $k=>$v){ ?>
                        <tr class="tr_selector cursor-pointer" data-id="<?php echo $v['anggota_id'];?>" id="tr<?php echo $v['anggota_id'];?>">
                            <td class="border-right dotted">
                                <?php
                                if($data['page'] == 1){
                                    echo ($k+1);
                                }else{
                                    echo ($k + 1 + ($data['limit'] * ($data['page'] - 1)));
                                }
                                if($v['anggota_photo'] == '' || $v['anggota_photo'] == null){
                                    if(file_exists(Config::$application['file_path'].'images'.DS.'PASFOTO'.DS.$v['anggota_nomor'].'.JPG')){
                                        $photo = 'index.php?/load/file/'.urlencode('images/PASFOTO/'.$v['anggota_nomor'].'.JPG');
                                    }else{
                                        $photo = 'index.php?/load/file/'.urlencode('images/avatar.jpg');
                                    }
                                }else{
                                    $photo = $v['anggota_photo'];
                                }
                                ?>
                            </td>
                            <td class="border-right dotted"><?php echo $v['anggota_nomor'];?></td>
                            <td class="border-right dotted"><img src="<?php echo $photo; ?>" alt="" style="float:left;margin-right:5px;width:28px;height:28px;"/><?php echo $v['anggota_nama'];?></td>
                            <td class="border-right dotted"><?php echo $v['koperasi_nama'];?></td>
                            <td class="border-right dotted"><?php echo $v['tpk_nama'];?></td>
                            <td class="border-right dotted"><?php echo $v['kelompok_nama'];?></td>
                            <td class="text-center border-right dotted"><?php echo $v['cow_lactating'];?></td>
                            <td class="text-center border-right dotted"><?php echo $v['cow_population'];?></td>
                            <td class="text-center border-right dotted">
                                <?php if($v['cow_antibiotic'] === 0){ ?>
                                    <span class="bg-green fg-white padding-left-5 padding-right-5 padding-bottom-3 rounded">Negative</span>
                                <?php }else{ ?>
                                    <span class="bg-red fg-white padding-left-5 padding-right-5 padding-bottom-3 rounded">Positive</span>
                                <?php } ?>
                            </td>
                            <td class="text-center border-right dotted">
                                <?php if($v['cow_antibiotic'] === 0){ ?>
                                    -
                                <?php }else{ ?>
                                    <div>
                                        <?php echo date('d/m/Y',strtotime($v['cow_antibioticstartdate'])).' - '.date('d/m/Y',strtotime($v['cow_antibioticenddate']));?>
                                    </div>
                                <?php } ?>
                            </td>
                            <td class="text-center border-right dotted" style="position:relative;">
                                <a href="javascript:void(0);" onclick="$(this).next().toggleClass('fadein');">
                                    <i class="ion-ios7-email-outline" style="font-size:24px;"></i>
                                </a>
                                <div class="popover bottom" style="right:3px;top:35px;left:auto;width:200px;">
                                    <div class="arrow" style="right:10px;left:auto !important;"></div>
                                    <div class="popover-content">
                                        <?php echo $v['cow_antibioticremark'];?>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    <?php } ?>
                <?php } ?>
                </tbody>
            </table>
            <div class="grid-pagging border-top margin-bottom-30"></div>
        </div>
    </div>
</div>
<div id="modal-cow-edit" class="modal draggable">
    <div class="modal-dialog">
        <div class="modal-header">
            <h3 class="modal-title">Edit Cow Population</h3>
            <a href="javascript:void(0);" class="close">&times;</a>
        </div>
        <div class="modal-body">
            <form action="<?php echo BASE_URL;?>index.php?/cow/save" method="post">
                <input type="hidden" name="cow_anggota" id="edit-cow_anggota">
                <div class="form-group">
                    <label for="edit-cow_population" class="col-md-4">Population</label>
                    <div class="col-md-8">
                        <input type="text" class="input-block number" name="cow_population" id="edit-cow_population" required />
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit-cow_lactating" class="col-md-4">Lactating</label>
                    <div class="col-md-8">
                        <input type="text" class="input-block number" name="cow_lactating" id="edit-cow_lactating" required />
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit-cow_antibiotic" class="col-md-4">Antibiotic</label>
                    <div class="col-md-8">
                        <select name="cow_antibiotic" id="edit-cow_antibiotic">
                            <option value="0">Negative</option>
                            <option value="1">Positive</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit-cow_antibioticstartdate" class="col-md-4">Date</label>
                    <div class="col-md-4">
                        <input type="text" class="input-block datepicker" id="edit-cow_antibioticstartdate" value="<?php echo date('d/m/Y');?>" name="cow_antibioticstartdate">
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="input-block datepicker" id="edit-cow_antibioticenddate" value="<?php echo date('d/m/Y',strtotime('+1 week'));?>" name="cow_antibioticenddate">
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit-cow_antibioticremark" class="col-md-4">Remark</label>
                    <div class="col-md-8">
                        <textarea name="cow_antibioticremark" id="edit-cow_antibioticremark" class="input-block"></textarea>
                    </div>
                </div>
                <div class="text-right padding-top-15">
                    <button type="button" data-dismiss="modal">Close</button>
                    <button type="submit" class="default">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.grid-pagging').transpaginator({
            totalPages: <?php echo $data['total_page'];?>,
            currentPage: <?php echo $data['page'];?>,
            totalData: <?php echo $data['total_data'];?>,
            pageUrl: '<?php echo BASE_URL;?>index.php?/cow/management/&koperasi=<?php echo $_GET['koperasi'];?>&keyword=<?php echo $data['keyword'];?>&page='
        });
        $('.tr_selector').on('click',function(){
            var id = $(this).attr('data-id');
            $('.editbtn').removeClass('fg-lightgray').removeAttr('disabled').attr('data-id',id);
            $('.tr_selector').removeClass('active');
            $(this).addClass('active');
        });
        $('#__history').on('click',function(){
            var id = $(this).attr('data-id');
            if(typeof id !== 'undefined' && id !== ''){
                location.href = '<?php echo BASE_URL;?>index.php?/cow/history/'+id;
            }
        });
        $('.card_remark').on('click',function(){
            var remark = $(this).val();
            if(remark === 'other'){
                $('#card_remark_other').removeAttr('disabled').focus();
            }else{
                $('#card_remark_other').attr('disabled','disabled');
            }
        });
        $('#__edit').on('click',function(){
            var id = $(this).attr('data-id');
            if(typeof id !== 'undefined' && id !== ''){
                $.ajax({
                    url:'<?php echo BASE_URL;?>index.php?/cow/edit/'+id,
                    dataType:'json',
                    beforeSend:function(){
                        $('#loading').show();
                    },
                    success:function(res){
                        $('#edit-cow_anggota').val(res.cow_anggota);
                        $('#edit-cow_antibioticremark').val(res.cow_antibioticremark);
                        $('#edit-cow_population').val(res.cow_population);
                        $('#edit-cow_lactating').val(res.cow_lactating);
                        $('#edit-cow_antibiotic').val(res.cow_antibiotic);
                        //$('#edit-cow_antibioticstartdate').val(moment(res.cow_antibioticstartdate).format('DD/MM/YYYY'));
                        //$('#edit-cow_antibioticenddate').val(moment(res.cow_antibioticenddate).format('DD/MM/YYYY'));
                        $('#modal-cow-edit').addClass('fadein');
                        $('#loading').hide();
                    }
                });
            }
        });
    })

    function filter(){
        var keyword = $('#keyword').val();
        var koperasi= $('#koperasi').val();
        window.location.href = '<?php echo BASE_URL;?>index.php?/cow/management/&koperasi='+koperasi+'&keyword='+keyword+'&page=1';
    }
</script>
<?php View::inc('footer.php');?>
