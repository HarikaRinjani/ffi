<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('settings/settings.menu.php');?>

<div class="content padded">
    <div class="breadcrumb">
        <ul class="tab">
            <li >
                <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-add-department">add new</a>
            </li>
            <li class="active">
                <a href="<?php echo BASE_URL;?>index.php?/tpk/setting" disabled="disabled">TPK</a>
            </li>
        </ul>
        <div class="pull-right">
            <ul class="breadcrumb-nav">
                <li>
                    <a href="">Apps Settings</a>
                </li>
                <li>
                    <a href=""><?php echo _('Collection Point');?></a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row padding-bottom-40">
            <div class="ribbon text-right">
                <div class="menu">
                    <a href="javascript:void(0);" id="__edit" class="fg-lightgray editbtn"><i class="ion-ios7-compose-outline"></i>edit</a>
                    <a href="javascript:void(0);" id="__delete" class=" fg-lightgray editbtn"><i class="ion-ios7-trash-outline"></i>delete</a>
                </div>
                <form action="javascript:void(0);" id="searchForm" onsubmit="return filter();">
                    <?php if(!in_array(session::get('user_group'),Config::$application['super_group'])){ ?>
                    <input type="hidden" name="koperasi" id="koperasi" value="<?php echo session::get('user_koperasi');?>">
                    <?php }else{ ?>
                    <select name="koperasi" id="koperasi" onchange="return filter();">
                        <option value="">All</option>
                        <?php foreach($koperasi['records'] as $k=>$v){ ?>
                            <option value="<?php echo $v['koperasi_id'];?>" <?php if($v['koperasi_id'] === $_GET['koperasi']){ echo 'selected="selected"'; } ?>><?php echo $v['koperasi_nama'];?></option>
                        <?php } ?>
                    </select>
                    <?php } ?>
                    <input type="text" placeholder="Search..." id="keyword" style="width:200px" value="<?php echo $tpk['keyword'];?>">
                </form>
            </div>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th style="width:24px;">#</th>
                    <!-- <th><?php echo _('Code');?></th> -->
                    <th><?php echo _('Collection Point Name');?></th>
                    <th><?php echo _('Cooperative');?></th>
                    <th><?php echo _('Address');?></th>
                    <th><?php echo _('Status');?></th>
                </tr>
                </thead>
                <tbody>
                <?php if($tpk['total_data'] == 0){ ?>
                    <tr>
                        <td colspan="6"><?php echo _('No record found');?></td>
                    </tr>
                <?php }else{ ?>
                    <?php foreach($tpk['records'] as $k=>$v){ ?>
                        <tr class="tr_selector cursor-pointer" data-id="<?php echo $v['tpk_id'];?>" id="tr<?php echo $v['tpk_id'];?>">
                            <td>
                                <?php
                                if($tpk['page'] == 1){
                                    echo ($k+1);
                                }else{
                                    echo ($k + 1 + ($tpk['limit'] * $tpk['page'] -1));
                                }
                                ?>
                            </td>
                            <!-- <td><?php echo $v['tpk_kode'];?></td> -->
                            <td><?php echo $v['tpk_nama'];?></td>
                            <td><?php echo $v['koperasi_nama'];?></td>
                            <td><?php echo $v['tpk_alamat'];?></td>
                            <td><span class="status"><?php echo $v['tpk_status'];?></span></td>
                        </tr>
                    <?php } ?>
                <?php } ?>
                </tbody>
            </table>
            <div class="grid-pagging border-top margin-bottom-30"></div>
        </div>
    </div>
</div>
<div class="modal draggable" id="modal-add-department">
    <div class="modal-dialog modal-sm">
        <div class="modal-header">
            <h3 class="modal-title">Add new Collection Point</h3>
            <a href="javascript:void(0);" class="close">&times;</a>
        </div>
        <div class="modal-body">
            <form action="<?php echo BASE_URL;?>index.php?/tpk/setting/save" method="post">
                <label for="add-koperasi_nama" class=""><?php echo _('Cooperative');?></label>
                <div class="">
                    <select name="tpk_koperasi" id="add-tpk_koperasi" class="input-block koperasi"></select>
                </div>
                <label for="add-department_name" class="padding-top-5"><?php echo _('Collection Point Name');?></label>
                <div class="">
                    <input type="text" id="add-department_name" class="input-block" name="tpk_nama" required/>
                </div>
                <!-- <label for="add-tpk_kode" class="padding-top-5"><?php echo _('Collection Point Code');?></label>
                <div class="">
                    <input type="text" id="add-tpk_kode" class="input-block"  maxlength="3" name="tpk_kode" required/>
                </div> -->
                <label class="margin-top-5" for=""><?php echo _('Address');?></label>
                <div class="">
                    <textarea name="tpk_alamat" id="tpk_alamat" class="input-block" rows="3"></textarea>
                </div>
                <label class="margin-top-5" for="add-department-status">Status</label>
                <div class="">
                    <select name="tpk_status" data-value="y" id="add-tpk-status" class="input-block status"></select>
                </div>
                <div class="padding-top-25 text-right">
                    <button type="button" data-dismiss="modal">close</button>
                    <button type="submit" class="default">save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal draggable" id="modal-edit-department">
    <div class="modal-dialog modal-sm">
        <div class="modal-header">
            <h3 class="modal-title">Edit Collection Point</h3>
            <a href="javascript:void(0);" class="close">&times;</a>
        </div>
        <div class="modal-body">
            <form action="<?php echo BASE_URL;?>index.php?/tpk/setting/save" method="post">
                <input type="hidden" name="tpk_id" id="edit-tpk_id"/>
                <label for="add-tpk_koperasi" class=""><?php echo _('Cooperative');?></label>
                <div class="">
                    <select name="tpk_koperasi" id="edit-tpk_koperasi" class="input-block koperasi"></select>
                </div>
                <label for="add-department_name" class=""><?php echo _('Collection Point Name');?></label>
                <div class="">
                    <input type="text" id="edit-tpk_nama" class="input-block" name="tpk_nama" required/>
                </div>
                <!-- <label for="add-tpk_kode" class="padding-top-5"><?php echo _('Collection Point Code');?></label>
                <div class="">
                    <input type="text" id="edit-tpk_kode" class="input-block" maxlength="3" name="tpk_kode" readonly="readonly" required/>
                </div> -->
                <label class="margin-top-5" for=""><?php echo _('Address');?></label>
                <div class="">
                    <textarea name="tpk_alamat" id="edit-tpk_alamat" class="input-block" rows="3"></textarea>
                </div>
                <label class="margin-top-5" for="edit-tpk_status">Status</label>
                <div class="">
                    <select name="tpk_status" id="edit-tpk_status" class="input-block status"></select>
                </div>
                <div class="padding-top-25 text-right">
                    <button type="button" data-dismiss="modal">close</button>
                    <button type="submit" class="default">save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.grid-pagging').transpaginator({
            totalPages: <?php echo $tpk['total_page'];?>,
            totalData: <?php echo $tpk['total_data'];?>,
            currentPage: <?php echo $tpk['page'];?>,
            pageUrl: '<?php echo BASE_URL;?>index.php?/tpk/setting/&koperasi=<?php echo $_GET['koperasi'];?>&keyword=<?php echo $tpk['keyword'];?>&page='
        });
        $('.tr_selector').on('click',function(){
            var id = $(this).attr('data-id');
            $('.editbtn').removeClass('fg-lightgray').removeAttr('disabled').attr('data-id',id);
            $('.tr_selector').removeClass('active');
            $(this).addClass('active');
        });
        $('#__edit').on('click',function(){
            var id = $(this).attr('data-id');
            if(typeof id !== 'undefined' && id !== ''){
                $.ajax({
                    url:'<?php echo BASE_URL;?>index.php?/tpk/setting/edit/'+id,
                    dataType:'json',
                    success:function(res){
                        $('#edit-tpk_id').val(res.tpk_id);
                        $('#edit-tpk_nama').val(res.tpk_nama);
                        $('#edit-tpk_koperasi').val(res.tpk_koperasi);
                        $('#edit-tpk_status').val(res.tpk_status);
                        $('#edit-tpk_kode').val(res.tpk_kode);
                        $('#edit-tpk_alamat').text(res.tpk_alamat);
                        $('#modal-edit-department').addClass('fadein');
                    }
                });
            }
        });

        $('#__delete').on('click',function(){
            var id = $(this).attr('data-id');
            if(typeof id !== 'undefined' && id !== ''){
                if(confirm('<?php echo _('Are you sure want to delete this data?');?>')){
                    $.ajax({
                        url:'<?php echo BASE_URL;?>index.php?/tpk/setting/delete',
                        type:'post',
                        data:{
                            tpk_id:id
                        },
                        success:function(){
                            location.reload();
                        }
                    });
                }
            }
        });

    });

    function filter(){
        var keyword = $('#keyword').val();
        var koperasi= $('#koperasi').val();
        window.location.href = '<?php echo BASE_URL;?>index.php?/tpk/setting/&koperasi='+koperasi+'&keyword='+keyword+'&page=1';
    }
</script>
<?php View::inc('footer.php');?>
