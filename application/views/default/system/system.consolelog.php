<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('system/system.menu.php');?>
    <div class="content padded">
        <div class="breadcrumb">
            <ul class="tab">
                <li class="active">
                    <a href="">Console</a>
                </li>

            </ul>
            <div class="pull-right">
                <ul class="breadcrumb-nav">
                    <li>
                        <a href="">System </a>
                    </li>
                    <li>
                        <a href="">Console</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 padding-top-15">
                    <div class="panel bg-black">
                        <div class="panel-body bg-black fg-white" style="height:400px;overflow:auto;">
                            <div id="consoleLogContent" style="font-family:monospace !important"><?php echo $log;?></div>
                        </div>
                        <div class="panel-footer bg-dark" style="border-color:#888;">
                            <input type="text" id="consoleLogCmd_input" class="input-block bg-black fg-white padding-5" style="font-size:16px;font-family:'Monaco','Consolas','Ubuntu Mono',monospaced !important,border:none !important;box-shadow:none !important;outline:none !important;" placeholder="Enter your command here...">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            var heg = $('#consoleLogContent').height();
            $('#consoleLogContent').parent().scrollTop((parseInt(heg) + 100));
            $('#consoleLogCmd_input').val('').focus();
            $(document).on('keyup','#consoleLogCmd_input',function(e){
                if(e.keyCode == 13){
                    var valu = $(this).val();
                    if(valu == ''){
                        return false;
                    }
                    if(valu == 'cls' || valu == 'clear'){
                        $('#consoleLogContent').html('');
                        $('#consoleLogCmd_input').val('').focus();
                        return false;
                    }
                    $.ajax({
                        url:'<?php echo BASE_URL;?>index.php?/system/consolelog',
                        data:'command='+valu,
                        type:'post',
                        success:function(res){
                            $('#consoleLogContent').html(res);
                            $('#consoleLogCmd').html(valu);
                            $('#consoleLogCmd_input').val('').focus();
                            var heg = $('#consoleLogContent').height();
                            $('#consoleLogContent').parent().scrollTop((parseInt(heg) + 100));
                        }
                    });
                }
            });
        });
       
    </script>
<?php View::inc('footer.php');?>
