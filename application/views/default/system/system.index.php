<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('system/system.menu.php');?>
    <div class="content padded">
        <div class="breadcrumb">
            <ul class="tab">
                <li class="active">
                    <a href="">mcp system</a>
                </li>

            </ul>
            <div class="pull-right">
                <ul class="breadcrumb-nav">
                    <li>
                        <a href="">System Settings</a>
                    </li>
                    <li>
                        <a href="">SysInfo</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <h1>System Information</h1>
                </div>
                <div class="col-md-2">
                    <img class="img-responsive" src="<?php echo BASE_URL;?>themes/default/images/K3.png" alt="">
                </div>
                <div class="col-md-6">
                    <h3><?php echo Config::$application['app_name'];?></h3>
                    <div class="padding-left-20">
                        <p>Apps Version: <?php echo Config::$application['app_version'];?></p>
                        <p>Registered to: <?php echo Config::$application['company_name'];?></p>
                        <p>Expired date: <?php echo session::get('license_expired');?></p>
                        <p class="text-info">&copy; 2014 PT Daya Transformatika</p>
                        <p class="text-info"><a href="http://www.transformatika.co.id" target="_blank">www.transformatika.co.id</a></p>
                    </div>
                    <h3>System</h3>
                    <div class="padding-left-20">
                        <p>PHP version <?php echo phpversion();?></p>
                        <p>Web Server: <?php echo $_SERVER['SERVER_SOFTWARE'];?></p>
                        <p>Memory Usage: <?php echo fn::formatBytes(memory_get_usage(),2);?></p>
                        <p>Operating System: <?php echo php_uname('s');?></p>
                    </div>
                    <h3 class="fg-red">Send Logs</h3>
                    <div class="padding-left-20">
                        <div class="fg-red padding-bottom-15">
                            If you find some errors on this Apps, You can send apps logs using the following button.
                        </div>
                        <div>
                            <button class="btn-lg bg-red fg-white" id="sendLog">send log</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            $('#sendLog').on('click',function(){
                $.ajax({
                    url:'<?php echo BASE_URL;?>index.php?/dashboard/errorReporting/&json=yes',
                    beforeSend:function(){
                        $('#sendLog').html('sending mail.please wait...');
                    },
                    success:function(res){
                        notification(res);
                        $('#sendLog').html('send log')
                    }
                });
            });
        });
    </script>
<?php View::inc('footer.php');?>
