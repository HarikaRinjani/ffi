<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('system/system.menu.php');?>
    <div class="content padded">
        <div class="breadcrumb">
            <ul class="tab">
                <li class="active">
                    <a href="">System Parameter</a>
                </li>

            </ul>
            <div class="pull-right">
                <ul class="breadcrumb-nav">
                    <li>
                        <a href="">System Settings</a>
                    </li>
                    <li>
                        <a href="">Blocked IP Address</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="ribbon col-md-12">
                    <h1 class="no-margin no-padding">Blocked IP Address</h1>
                </div>
                <div class="col-md-12">
                    <form action="<?php echo BASE_URL;?>index.php?/system/securitysave" enctype="multipart/form-data" method="post">
                        <table class="table no-border no-hover margin-top-30" style="width:100%">

                            <tr>
                                <td style="width:200px;vertical-align:top;">Blocked IP Address</td>
                                <td>
                                    <input type="text" class="input-block" name="denied_host" value="<?php echo implode(',',Config::$application['denied_host']);?>" />
                                    <small>separate ip address with "," ex: 123.123.123.123, 124.124.124.124 </small>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <button class="default">save change</button>
                                </td>
                                <td></td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php View::inc('footer.php');?>
