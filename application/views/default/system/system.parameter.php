<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('system/system.menu.php');?>
    <div class="content padded">
        <div class="breadcrumb">
            <ul class="tab">
                <li class="active">
                    <a href="">System Parameter</a>
                </li>

            </ul>
            <div class="pull-right">
                <ul class="breadcrumb-nav">
                    <li>
                        <a href="">System Settings</a>
                    </li>
                    <li>
                        <a href="">Parameter</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="ribbon col-md-12">
                    <h1 class="no-margin no-padding">System Parameter</h1>
                </div>
                <div class="col-md-12">
                    <form action="<?php echo BASE_URL;?>index.php?/system/parametersave" enctype="multipart/form-data" method="post">
                        <table class="table no-border no-hover" style="width:auto">
                            <tr>
                                <td colspan="2">
                                    <h3 class="padding-top-15">General Configuration</h3>
                                </td>
                            </tr>
                            <tr>
                                <td>Start Year</td>
                                <td>
                                    <input type="text" class="number" name="start_year" value="<?php echo Config::$application['start_year'];?>">
                                </td>
                            </tr>
                            <tr>
                                <td>Last Morning Time (0-23)</td>
                                <td>
                                    <input type="text" class="number" name="batas_pagi" value="<?php echo Config::$application['batas_pagi'];?>">
                                </td>
                            </tr>
                            <tr>
                                <td>Display Limit</td>
                                <td>
                                    <input type="text" class="number" name="display_limit" value="<?php echo Config::$application['display_limit'];?>">
                                </td>
                            </tr>
                            <tr>
                                <td>Locale</td>
                                <td>
                                    <input type="text" name="locale" value="<?php echo $locale;?>">
                                </td>
                            </tr>

                            <tr>
                                <td>CSV Scan Path (Server Only)</td>
                                <td>
                                    <input type="text" name="csv_path" value="<?php echo Config::$application['csv_path'];?>">
                                </td>
                            </tr>
                            <tr>
                                <td>Login logo</td>
                                <td>
                                    <input type="file" name="login_logo">
                                </td>
                            </tr>
                            <tr>
                                <td>Display Cooperative Logo</td>
                                <td>
                                    <input type="radio" id="display_cooperativelogo1" <?php if(Config::$application['display_cooperativelogo'] === true){ echo 'checked="checked"'; } ?>  name="display_cooperativelogo" value="1">
                                    <label for="display_cooperativelogo1">Yes</label>
                                    <input type="radio" id="display_cooperativelogo0" <?php if(Config::$application['display_cooperativelogo'] === false){ echo 'checked="checked"'; } ?>  name="display_cooperativelogo" value="0">
                                    <label for="display_cooperativelogo0">No</label>
                                </td>
                            </tr>
                            <tr>
                                <td>Cooperative Logo Size</td>
                                <td>
                                    <input type="radio" id="cooperativelogo_size1" <?php if(Config::$application['cooperativelogo_size'] === 1){ echo 'checked="checked"'; } ?>  name="cooperativelogo_size" value="1">
                                    <label for="cooperativelogo_size1">Small</label>
                                    <input type="radio" id="cooperativelogo_size2" <?php if(Config::$application['cooperativelogo_size'] === 2){ echo 'checked="checked"'; } ?>  name="cooperativelogo_size" value="2">
                                    <label for="cooperativelogo_size2">Big</label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <h3 class="padding-top-15">Collection Point Configuration</h3>
                                </td>
                            </tr>
                            <tr>
                                <td>This is Collection Point?</td>
                                <td>
                                    <input type="radio" id="is_client1" <?php if(Config::$application['is_client'] === true){ echo 'checked="checked"'; } ?>  name="is_client" value="1">
                                    <label for="is_client1">Yes</label>
                                    <input type="radio" id="is_client0" <?php if(Config::$application['is_client'] === false){ echo 'checked="checked"'; } ?>  name="is_client" value="0">
                                    <label for="is_client0">No</label>
                                </td>
                            </tr>

                            <tr>
                                <td>On Screen Keyboard</td>
                                <td>
                                    <input type="radio" id="enable_onscreenkeyboard1" <?php if(Config::$application['enable_onscreenkeyboard'] === true){ echo 'checked="checked"'; } ?>  name="enable_onscreenkeyboard" value="1">
                                    <label for="enable_onscreenkeyboard1">enable</label>
                                    <input type="radio" id="enable_onscreenkeyboard0" <?php if(Config::$application['enable_onscreenkeyboard'] === false){ echo 'checked="checked"'; } ?>  name="enable_onscreenkeyboard" value="0">
                                    <label for="enable_onscreenkeyboard0">disable</label>
                                </td>
                            </tr>
                            <tr>
                                <td>Cooperative</td>
                                <td>
                                    <select name="mcp_koperasi" id="mcp_koperasi" class="koperasi" data-value="<?php echo Config::$application['mcp_koperasi'];?>"></select>
                                </td>
                            </tr>
                            <tr>
                                <td>Collection Point</td>
                                <td>
                                    <select name="mcp_tpk" id="mcp_tpk" class="tpk" data-value="<?php echo Config::$application['mcp_tpk'];?>"></select>
                                </td>
                            </tr>
                            <tr>
                                <td>Public Key</td>
                                <td>
                                    <input type="text" name="mcp_key" value="<?php echo Config::$application['mcp_key'];?>">
                                </td>
                            </tr>
                            <tr>
                                <td>Max Printer Character</td>
                                <td>
                                    <input type="text" class="number" name="maximal_printer_karakter" value="<?php echo Config::$application['maximal_printer_karakter'];?>">
                                </td>
                            </tr>
                            <tr>
                                <td>Printer Name</td>
                                <td>
                                    <input type="text" name="printername" value="<?php echo Config::$application['printername'];?>">
                                </td>
                            </tr>
                            <tr>
                                <td>Zebra Printer Name</td>
                                <td>
                                    <input type="text" name="zebra_printername" value="<?php echo Config::$application['zebra_printername'];?>">
                                </td>
                            </tr>
                            <tr>
                                <td>Sync URL</td>
                                <td>
                                    <input type="text" name="mcp_server_url" value="<?php echo Config::$application['mcp_server_url'];?>">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <h3 class="padding-top-15">Logging</h3>
                                </td>
                            </tr>
                            <tr>
                                <td>Error Log</td>
                                <td>
                                    <input type="radio" id="log_error1" <?php if(Config::$application['log_error'] === true){ echo 'checked="checked"'; } ?> name="log_error" value="1">
                                    <label for="log_error1">enable</label>
                                    <input type="radio" id="log_error0" <?php if(Config::$application['log_error'] === false){ echo 'checked="checked"'; } ?> name="log_error" value="0">
                                    <label for="log_error0">disable</label>
                                </td>
                            </tr>
                            <tr>
                                <td>Access Log</td>
                                <td>
                                    <input type="radio" id="log_access1" <?php if(Config::$application['log_access'] === true){ echo 'checked="checked"'; } ?>  name="log_access" value="1">
                                    <label for="log_access1">enable</label>
                                    <input type="radio" id="log_access0" <?php if(Config::$application['log_access'] === false){ echo 'checked="checked"'; } ?>  name="log_access" value="0">
                                    <label for="log_access0">disable</label>
                                </td>
                            </tr>
                            <tr>
                                <td>Rotate Log</td>
                                <td>
                                    <input type="radio" id="log_rotate1" <?php if(Config::$application['log_rotate'] === true){ echo 'checked="checked"'; } ?>  name="log_rotate" value="1">
                                    <label for="log_rotate1">enable</label>
                                    <input type="radio" id="log_rotate0" <?php if(Config::$application['log_rotate'] === false){ echo 'checked="checked"'; } ?>  name="log_rotate" value="0">
                                    <label for="log_rotate0">disable</label>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <button class="default">save change</button>
                                </td>
                                <td></td>
                            </tr>
                        </table>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            $(document).on('change','#mcp_koperasi',function(){
                var id = $(this).val();
                $.ajax({
                    url:'<?php echo BASE_URL;?>index.php?/tpk/findByKoperasi/'+id,
                    dataType:'json',
                    success:function(res){
                        var opsi = '';
                        for(var k in res.records){
                            opsi += '<option value="'+res.records[k].tpk_id+'">'+res.records[k].tpk_nama+'</option>';
                        }
                        $('#mcp_tpk').html(opsi);
                    }
                });
            });
        });
    </script>
<?php View::inc('footer.php');?>
