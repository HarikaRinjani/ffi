<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('system/system.menu.php');?>
    <div class="content padded">
        <div class="breadcrumb">
            <ul class="tab">
                <li>
                    <a href="javascript:void(0);" data-toggle="modal" data-target="#modal">install/update</a>
                </li>
                <li class="active">
                    <a href="">package manager</a>
                </li>

            </ul>
            <div class="pull-right">
                <ul class="breadcrumb-nav">
                    <li>
                        <a href="">System Settings</a>
                    </li>
                    <li>
                        <a href="">Package Manager</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="ribbon border-bottom">
                    <div class="menu">
                        <a href="" class=""><i class="ion-ios7-photos-outline"></i>packages</a>
                        <a href="javascript:void(0);" id="repairPackage" disabled="disabled" class=" fg-lightgray"><i class="ion-ios7-gear-outline"></i> repair</a>
                        <a href="javascript:void(0);" id="backupPackage" disabled="disabled" class=" fg-lightgray"><i class="ion-ios7-cloud-download-outline"></i> backup</a>
                        <a href="#" id="checkUpdate"><i class="ion-ios7-loop"></i> Check Update</a>
                    </div>
                </div>
                <table class="table border dotted no-hover fixedheader" style="border:0;">
                    <thead>
                    <tr>
                        <th>package</th>
                        <th>description</th>
                        <th class="text-center">version</th>
                        <th>publisher</th>
                        <th class="text-center">Update<th>
                    </tr>
                    </thead>
                    <tbody class="border-bottom" style="height:340px;">
                    <?php foreach($package as $k=>$v){ ?>
                        <tr data-id="<?php echo $v['package'];?>" id="tr<?php echo $v['package'];?>" class="__select">
                            <td style="vertical-align:top;"><?php echo $v['package'];?></td>
                            <td style="vertical-align:top;padding:0 10px;margin:0;"><?php echo $v['description'];?></td>
                            <td style="vertical-align:top;" class="text-center"><?php echo $v['version'];?></td>
                            <td style="vertical-align:top;"><?php echo $v['publisher'];?></td>
                            <td style="vertical-align:top;" class="text-center">
                                <a href="javascript:void(0);" class="updatebtn fg-lightgray" id="updatebtn<?php echo $v['package'];?>" data-package="<?php echo $v['package'];?>" style="margin:0;padding:0;font-size:24px;"><i class="ion-ios7-download-outline"></i></a>
                            </td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
                <div class="col-md-12">
                    <p>
                        <small>
                            * Nama file Package harus dengan format &lt;package&gt;.zip <br/>
                            * Lakukan backup sebelum installasi untuk menghindari hal-hal yang tidak diinginkan
                        </small>
                    </p>
                </div>
            </div>
        </div>
    </div>
<div id="modal" class="modal">
    <div class="modal-dialog">
        <div class="modal-header">
            <h3 class="modal-title">install package</h3>
            <a href="javascript:void(0);" class="close">&times;</a>
        </div>
        <div class="modal-body no-padding">
            <form id="form-install-package" action="<?php echo BASE_URL; ?>index.php?/system/installprocess" method="post" enctype="multipart/form-data" class="padding-left-15 padding-right-15">
                <h3 class="padding-bottom-10">Browse package to install</h3>
                <input type="file" name="package" id="package"/>
                <p class="margin-top-15 fg-red">
                    <strong>Warning </strong>
                    <small>this will replace your existing package.</small>
                    <br/>
                    <span class="small"><small><?php echo _('package file name must be &lt;packagename&gt.zip format');?></small></span>
                </p>
                <p class="margin-top-40 text-right">
                    <button type="button" data-dismiss="modal">cancel</button>
                    <button type="button" id="install-btn" class="default">install package</button>
                </p>
            </form>
        </div>
    </div>
</div>
<div class="modal drop" id="modal-checkupdate">
    <div class="modal-dialog modal-sm">
        <div class="modal-body">
            <div class="text-center">
                <h2 class="text-center">updating...</h2>
                <div class="progress progress-striped active">
                    <div class="progress-bar" style="width:100%">
                        <span class="sr-only"></span>
                    </div>
                </div>
                Update in progress. please do not close or reload this page!
            </div>
        </div>
    </div>
</div>
<div class="modal drop" id="modal-installupdate">
    <div class="modal-dialog modal-sm">
        <div class="modal-body">
            <div class="text-center">
                <h2 class="text-center">installing update...</h2>
                <div class="progress progress-striped active">
                    <div class="progress-bar" style="width:100%">
                        <span class="sr-only"></span>
                    </div>
                </div>
                installing package <span id="package-installupdate"></span>. Please wait
            </div>
        </div>
    </div>
</div>
    <script>
        $(document).ready(function(){
            $('.modal-dialog').draggable();
            $(document).on('click','.__select',function(){
                var pak = $(this).attr('data-id');
                $('#backupPackage').attr('data-id',pak);
                $('#backupPackage').removeAttr('disabled');
                $('#backupPackage').removeClass('fg-lightgray');
                $('#repairPackage').attr('data-id',pak);
                $('#repairPackage').removeAttr('disabled');
                $('#repairPackage').removeClass('fg-lightgray');
                $('.__select').removeClass('bg-lightgray');
                $(this).addClass('bg-lightgray');
            });
            $('#install-btn').on('click',function(){
                var pak          = $('#form-install-package').find('input[type=file]').val();
                var splitpackage = pak.split('\\');
                var curpackage   = splitpackage[splitpackage.length-1];
                var splitcurpak  = curpackage.split('-');
                var curpak       = splitcurpak[0].replace('.zip','');
                //alert(curpak);
                $.ajax({
                    url:'<?php echo BASE_URL;?>index.php?/system/backup/'+curpak+'/json',
                    dataType:'json',
                    success:function(res){
                        if(res.status === 'success'){
                            $('#form-install-package').submit();
                        }else{
                            notification('cannot perform package backup!');
                        }
                    }
                });
            });
            $('#backupPackage').on('click',function(){
                if($(this).attr('disabled')){
                    return false;
                }else{
                    var url = '<?php echo BASE_URL;?>index.php?/system/backup/'+$(this).attr('data-id');
                    window.open(url);
                }

            });
            $(document).on('click','.updatebtn',function(){
                if($(this).hasClass('fg-lightgray')){
                    return false;
                }else{
                    var pak = $(this).attr('data-package');
                    $.ajax({
                        url:'<?php echo BASE_URL;?>index.php?/system/installupdate/'+pak,
                        beforeSend:function(){
                            $('#loading').show();
                            $('#modal-installupdate').show();
                            $('#package-installupdate').html(pak);
                        },
                        success:function(res){
                            if(res === 'success'){
                                $('#tr'+pak).css('background','rgba(0,255,0,.2)');
                                $('#updatebtn'+pak).addClass('fg-lightgray').attr('disabled','disabled');
                                
                            }else{
                                notification('Update Failed!. pleas try again later');
                            }
                            $('#loading').hide();
                            $('#modal-installupdate').hide();
                        }
                    });
                }
            });
            $(document).on('click','#checkUpdate',function(){
                $.ajax({
                    url:'<?php echo BASE_URL;?>index.php?/system/checkupdate',
                    dataType:'json',
                    beforeSend:function(){
                        notification('Checking for new update. please wait...');
                        $('#loading').show();
                        $('#modal-checkupdate').show();
                    },
                    success:function(res){
                        var i = 0;
                        var firstpackage = '';
                        var kloning = '';
                        for(var k in res){
                            i++;
                            if(firstpackage === ''){
                                firstpackage = res[k].package;
                            }
                            $('#tr'+res[k].package).css('background','rgba(0,255,0,.2)');
                            $('#updatebtn'+res[k].package).removeClass('fg-lightgray').removeAttr('disabled');
                            kloning = $('#tr'+res[k].package)[0].outerHTML;
                            $('#tr'+res[k].package).remove();
                            $('table.fixedheader').find('tbody').prepend(kloning);
                        }
                        if(i > 0){

                            notification(i+' Update available!');
                            var h = $('#tr'+res[k].package).offset().top;
                            $('table.fixedheader').find('tbody').animate({
                                scrollTop: $("#tr"+res[k].package).offset().top - 200
                            },300);
                        }else{
                            notification('Your system is up to date');
                        }
                        $('#modal-checkupdate').hide();
                        $('#loading').hide();
                    }
                });
            });
            $('#repairPackage').on('click',function(){
                if($(this).attr('disabled')){
                    return false;
                }else{
                    var url = '<?php echo BASE_URL;?>index.php?/system/repair/'+$(this).attr('data-id');
                    window.location.href = url;
                }
            });
        });
    </script>
<?php View::inc('footer.php');?>
