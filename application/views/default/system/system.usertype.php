<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('system/system.menu.php');?>
    <div class="content padded">
        <div class="breadcrumb">
            <ul class="tab" id="usertype-tab">
                <li class="active">
                    <a href="">user type</a>
                </li>

            </ul>
            <div class="pull-right">
                <ul class="breadcrumb-nav">
                    <li>
                        <a href="">System Settings</a>
                    </li>
                    <li>
                        <a href="">user type</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="ribbon col-md-12 border-bottom">
                    <h1 class="pull-left no-margin no-padding"><?php echo Config::$application['user_group'][$_GET['user_group']];?></h1>
                    <div class="pull-right margin-top-10">
                        <select name="user_group" id="select_user_group">
                            <?php
                            foreach(Config::$application['user_group'] as $k=>$v){
                                echo '<option value="'.$k.'" ';
                                if($k == $_GET['user_group']){
                                    echo ' selected="selected"';
                                }
                                echo '>'.$v.'</option>';
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="padding-top-15 padding-bottom-10 border-bottom">
                        <strong class="fg-red">Warning!</strong>
                        <div>
                            Please becarefull, Wrong configuration may break your system!
                        </div>
                    </div>
                    <form action="<?php echo BASE_URL;?>index.php?/system/usertypesave" method="post">
                        <input type="hidden" name="usertype_id" value="<?php echo $_GET['user_group'];?>">
                        <ul style="list-style:none;margin:15px 0 0 0;padding:0;">
                            <?php
                            $i = 1;
                            foreach(Config::$application['user_grouproles'] as $k=>$v){
                                ?>
                            <li class="col-md-3" style="margin-bottom:15px;">
                                <input type="checkbox" id="__selectAll<?php echo $k;?>" value="<?php echo $k;?>" name="user_group" class="__selectAll" data-id="<?php echo $k;?>">
                                <label for="__selectAll<?php echo $k;?>"><strong><?php echo $k;?></strong></label>
                                <ul style="list-style:none;">
                                    <?php foreach($v as $item=>$value){ ?>
                                    <li style="margin-bottom:15px;">
                                        <input type="checkbox" id="<?php echo $k .'-'. $item;?>" class="__select<?php echo $k;?> __selectorx" <?php if($_GET['user_group'] == 'administrator'){ echo 'checked="checked"'; }else{ if(strpos($role,$k .'-'. $item) !== false){ echo 'checked="checked"'; }  }?> name="roles[]" value="<?php echo $k .'-'. $item;?>" data-id="<?php echo $k;?>">
                                        <label for="<?php echo $k .'-'. $item;?>"><?php echo $value;?></label>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </li>
                            <?php
                            if($i % 4 === 0){
                                echo '<div class="clearfix"></div>';
                            }
                            $i++;
                            } ?>
                        </ul>
                        <div class="clearfix"></div>
                        <div class="padding-top-10 padding-left-15">
                            <button class="default">SAVE CHANGE</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            initCheckbox();
            $('#select_user_group').on('change',function(){
                var user_group = $(this).val();
                openUrl('system/usertype/&user_group='+user_group);
            });
            $('.__selectAll').on('click',function(){
                var id = $(this).attr('data-id');
                $('.__select'+id).prop('checked', this.checked);
            });
            $('.__selectorx').on('click',function(){
                var id = $(this).attr('data-id');
                if($(this).prop('checked') === true){
                    var totalcb = $('.__select'+id).length;
                    var totalcbs = $('.__select'+id+':checked').length;
                    if(totalcb === totalcbs){
                        $('#__selectAll'+id).prop('checked',true);
                    }else{
                        $('#__selectAll'+id).prop('checked',false);
                    }
                }else{
                    $('#__selectAll'+id).prop('checked',false);
                }
            });
        });
        function initCheckbox(){
            $('.__selectAll').each(function(){
                var id = $(this).attr('data-id');
                var totalcb = $('.__select'+id).length;
                var totalcbs = $('.__select'+id+':checked').length;
                if(totalcb === totalcbs){
                    $(this).prop('checked',true);
                }
            });
        }
    </script>
<?php View::inc('footer.php');?>
