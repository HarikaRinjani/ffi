<div class="sidebar">
    <span id="resize-handler" class="hidden-xs hidden-sm ui-resizable-handle ui-resizable-e">&nbsp;</span>
    <a href="javascript:void(0);" class="visible-sm toggleSidebar"><i class="ion-ios7-more"></i></a>
    <?php View::inc('koperasilogo.php');?>
    <ul class="navigation icon">
        <?php View::inc('bookmark/bookmark.php');?>
        <li>
            <a href="javascript:void(0);" class="navtoggle"><i class="ion-chevron-right"></i></a>
            <h3 class="title">System</h3>
            <ul>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/system/index"><i class="ion-ios7-information-outline"></i>System Information</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL; ?>index.php?/system/package"><i class="ion-ios7-photos-outline"></i> Packages Manager</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL; ?>index.php?/user/usermanager"><i class="ion-ios7-people-outline"></i> User Manager</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL; ?>index.php?/system/usertype"><i class="ion-key"></i> User Type / Role</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL; ?>index.php?/system/parameter"><i class="ion-ios7-settings"></i> System Parameter</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL; ?>index.php?/system/security"><i class="ion-ios7-locked-outline"></i> Blocked IP Address</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL; ?>index.php?/system/consolelog"><i class="ion-ios7-monitor"></i> Console</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/repo/index"><i class="ion-ios7-box-outline"></i>Repository</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/client/index"><i class="ion-ios7-monitor-outline"></i>Client Credential</a>
                </li>
            </ul>
        </li>
    </ul>

</div>
