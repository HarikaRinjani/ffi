<div class="sidebar">
    <span id="resize-handler" class="hidden-xs hidden-sm ui-resizable-handle ui-resizable-e">&nbsp;</span>
    <a href="javascript:void(0);" class="visible-sm toggleSidebar"><i class="ion-ios7-more"></i></a>
    <?php View::inc('koperasilogo.php');?>
    <ul class="navigation icon">
        <?php View::inc('bookmark/bookmark.php');?>
        <li>
            <h3 class="title">Help &amp; Support</h3>
            <a href="javascript:void(0);" class="navtoggle"><i class="ion-chevron-down"></i></a>
            <ul>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/help/index"><i class="ion-ios7-copy-outline"></i>Documentation</a>
                </li>
            </ul>
        </li>
    </ul>
</div>
