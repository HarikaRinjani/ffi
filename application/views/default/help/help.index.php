<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('help/help.menu.php');?>

<div class="content padded">
    <div class="breadcrumb">
        <ul class="tab">
            <li class="active">
                <a href="<?php echo BASE_URL;?>index.php?/help/index"  disabled="disabled">help &amp; support</a>
            </li>
        </ul>
        <div class="pull-right">
            <ul class="breadcrumb-nav">
                <li>
                    <a href="">Milk Collection System</a>
                </li>
                <li>
                    <a href="">Help &amp; Support</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row padding-bottom-40">
            <div class="ribbon padding-left-15 padding-right-15 border-bottom">
                <div class="menu">
                    <a href="javascript:void(0);"><i class="ion-ios7-help-outline"></i> FAQ</a>
                    <a href="javascript:void(0);"><i class="ion-ios7-copy-outline"></i> Documentation</a>
                </div>
            </div>
            <div class="padding-40">
                <img src="<?php echo BASE_URL;?>themes/default/images/K3.png" alt="">
                <h2>Documentation</h2>
                <div>
                    <?php echo $help;?>
                </div>

            </div>

        </div>
    </div>
</div>
<div class="padding-50">&nbsp;</div>
<script type="text/javascript">
    $(document).ready(function(){

    });
</script>
<?php View::inc('footer.php');?>
