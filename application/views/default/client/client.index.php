<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('system/system.menu.php');?>
<div class="content padded">
    <div class="breadcrumb">
        <ul class="tab">
            <li class="active">
                <a href="">Client</a>
            </li>

        </ul>
        <div class="pull-right">
            <ul class="breadcrumb-nav">
                <li>
                    <a href="">System Settings</a>
                </li>
                <li>
                    <a href="">Client</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="ribbon border-bottom">
                <div class="menu">
                    <a href="javascript:void(0);" data-toggle="modal" data-target="#generate-modal"><i class="ion-ios7-plus-outline"></i> Add new</a>
                </div>
            </div>
             <table class="table border dotted no-hover fixedheader" style="border:0;">
                <thead>
                    <tr>
                        <th>Client ID</th>
                        <th>Client Secret</th>
                        <th>Cooperative</th>
                        <th>Collection Point</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($clients['total_data'] === 0) { ?>
                    <tr>
                        <td colspan="4">No record found</td>
                    </tr>
                    <?php } ?>
                    <?php foreach ($clients['records'] as $k => $v) { ?>
                    <tr>
                        <td><?php echo $v['client_id'];?></td>
                        <td><?php echo $v['client_secret'];?></td>
                        <td><?php echo $v['koperasi_nama'];?></td>
                        <td><?php echo $v['tpk_nama'];?></td>
                    </tr>
                    <?php  } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal draggable" id="generate-modal">
    <div class="modal-dialog modal-sm">
        <div class="modal-header">
            <h3 class="modal-title">Add new client</h3>
        </div>
        <div class="modal-content">
            <form action="<?php echo BASE_URL;?>index.php?/client/save" method="post">
                <div class="form-group">
                    <label class="col-md-12" for="samplingschedule_koperasi">Cooperative</label>
                    <div class="col-md-12">
                        <select class="input-block" id="samplingschedule_koperasi" name="client_koperasi" required="required">
                            <option value="">-- Please select Cooperative --</option>
                            <?php foreach($koperasi['records'] as $k=>$v){ ?>
                                <option value="<?php echo $v['koperasi_id'];?>"><?php echo $v['koperasi_nama'];?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" for="samplingschedule_tpk">Collection Point</label>
                    <div class="col-md-12">
                        <select name="client_tpk" id="samplingschedule_tpk" class="input-block" required="required">
                            <option value="">-- please select cooperative --</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12" form="client_id">Client ID</label>
                    <div class="col-md-12">
                        <input type="text" class="input-block" name="client_id" required="required"/>
                    </div>
                </div><div class="form-group">
                    <label class="col-md-12" form="client_secret">Client Secret</label>
                    <div class="col-md-12">
                        <input type="text" class="input-block" name="client_secret" required="required"/>
                    </div>
                </div>
                <div class="text-right margin-top-25">
                    <button type="button" data-dismiss="modal">Close</button>
                    <button type="submit" class="default">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#samplingschedule_koperasi').on('change',function(){
            var el = $(this);
            var valu = el.val();
            var opt = '';
            if(valu !== ''){
                $.ajax({
                    url:'<?php echo BASE_URL;?>index.php?/tpk/findByKoperasi/'+valu,
                    dataType:'json',
                    success:function(res){
                        var data = res.records;
                        for(var k in data){
                            opt += '<option value="'+data[k].tpk_id+'">';
                            opt += data[k].tpk_nama;
                            opt += '</option>';
                        }
                        $('#samplingschedule_tpk').html(opt);
                        opt = null;
                    }
                });
            }else{
                $('#samplingschedule_tpk').html('').val('');
            }
        });
    });
</script>
<?php View::inc('footer.php');?>
