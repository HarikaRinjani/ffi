<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('settings/settings.menu.php');?>

<div class="content padded" xmlns="http://www.w3.org/1999/html">
    <div class="breadcrumb">
        <ul class="tab">
            <li>
                <a href="javascript:void(0);" class="add" data-toggle="modal" data-target="#modal-add-periode">add new</a>
            </li>
            <li class="active">
                <a href="<?php echo BASE_URL;?>index.php?/anggota/setting" disabled="disabled">periode data</a>
            </li>
        </ul>
        <div class="pull-right">
            <ul class="breadcrumb-nav">
                <li>
                    <a href="">Apps Settings</a>
                </li>
                <li>
                    <a href="">Periode</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="ribbon text-right">
                <div class="menu">
                    <a href="javascript:void(0);" id="__edit" class="edit fg-lightgray editbtn"><i class="ion-ios7-compose-outline"></i>edit</a>
                    <a href="javascript:void(0);" id="__delete" class="delete fg-lightgray editbtn"><i class="ion-ios7-trash-outline"></i>delete</a>
                </div>
                <!-- <h1 class="pull-left fg-gray" style="margin-top:-10px;">
                    <a href="" class="fg-gray padding-right-10">lists</a>
                    <a href="javascript:void(0);" id="__edit" class="padding-right-10 fg-lightgray editbtn">edit</a>
                    <a href="javascript:void(0);" id="__delete" class="padding-right-10 fg-lightgray editbtn">delete</a>
                </h1> -->
                <form action="javascript:void(0);" id="searchForm" onsubmit="return filter();">
                    <input type="text" placeholder="Search..." id="keyword" style="width:200px" value="<?php echo $periode['keyword'];?>">
                </form>
            </div>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th style="width:24px;">#</th>
                    <th><?php echo _('Cooperative');?></th>
                    <th><?php echo _('Periode');?></th>
                    <th><?php echo _('Effective Date');?></th>
                   <!--  <th><?php echo _('Status');?></th> -->
                </tr>
                </thead>
                <tbody>
                <?php if($periode['total_data'] == 0){ ?>
                    <tr>
                        <td colspan="100%"><?php echo _('No record found');?></td>
                    </tr>
                <?php }else{ ?>
                    <?php foreach($periode['records'] as $k=>$v){ ?>
                        <tr class="tr_selector cursor-pointer" data-id="<?php echo $v['periode_id'];?>" id="tr<?php echo $v['periode_id'];?>">
                            <td style="vertical-align:top;">
                                <?php
                                if($periode['page'] == 1){
                                    echo ($k+1);
                                }else{
                                    echo ($k + 1 + $periode['limit']);
                                }
                                ?>
                            </td>
                            <td style="vertical-align:top;"><?php echo $v['koperasi_nama'];?></td>
                            <td style="vertical-align:top;">
                                <?php
                                    foreach($v['periodedetail'] as $key=>$val){
                                        echo '<div>'.$val['periodedetail_name'].'. Days '.$val['periodedetail_startdate'].' - '.$val['periodedetail_enddate'].'</div>';
                                    }
                                ?>
                            </td>
                            <td class="format-date" style="vertical-align:top;"><?php echo $v['periode_effectivedate'];?></td>
                            <!-- <td style="vertical-align:top;"<span class="status"><?php //echo $v['periode_status']; ?></span></td> -->
                        </tr>
                    <?php } ?>
                <?php } ?>
                </tbody>
            </table>
            <div class="grid-pagging border-top margin-bottom-30"></div>
        </div>
    </div>
</div>
<div class="modal draggable-header" id="modal-add-periode">
    <div class="modal-dialog">
        <div class="modal-header">
            <h3 class="modal-title"><?php echo _('Add Periode');?></h3>
            <a href="javascript:void(0);" class="close">&times;</a>
        </div>
        <div class="modal-body no-padding">
            <form action="<?php echo BASE_URL;?>index.php?/periode/setting/save" method="post" enctype="multipart/form-data">
                <div class="tabs-line">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-addperiode1" data-toggle="tab">Periode</a></li>
                        <li><a href="#tab-addperiode2" data-toggle="tab">Detail</a></li>
                    </ul>
                </div>
                <div class="tab-content border-top padding-15">
                     <div id="tab-addperiode1" class="tab-pane tab-modal fade in active">
                        <div class="form-group">
                            <label for="add-tpk_id" class="col-md-4"><?php echo _('Cooperative');?></label>
                            <div class="col-md-8">
                                <select name="periode_koperasi" id="add-periode_koperasi" class="input-block" required="required">
                                    <option value="">-- select Cooperative --</option>
                                    <?php foreach($koperasi['records'] as $k=>$v){ ?>
                                        <option value="<?php echo $v['koperasi_id']; ?>"><?php echo $v['koperasi_nama'];?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-periode_effectivedate" class="col-md-4"><?php echo _('Effective Date');?> </label>
                            <div class="col-md-8">
                                <input type="text" id="add-periode_effectivedate" class="input-block datepicker" name="periode_effectivedate" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4" for="add-periode_status">Status</label>
                            <div class="col-md-8">
                                <select name="periode_status" id="add-periode_status" class="input-block status"></select>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-addperiode2">
                        <table class="table border">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Periode</th>
                                    <th>Start</th>
                                    <th>End</th>
                                </tr>
                            </thead>    
                            <tbody>
                                <?php for($i=1;$i<=3;$i++){ ?>
                                <tr>
                                    <td>
                                        <input type="checkbox" name="periodedetail_check[<?php echo $i;?>]" value="<?php echo $i;?>"><label for=""></label>
                                    </td>
                                    <td>
                                        <input type="text" class="input-block text-center number" value="<?php echo $i;?>" name="periodedetail_name[<?php echo $i;?>]">
                                    </td>
                                    <td>
                                        <input type="text" class="input-block text-center number" name="periodedetail_startdate[<?php echo $i;?>]">
                                    </td>
                                    <td>
                                        <input type="text" class="input-block text-center number" name="periodedetail_enddate[<?php echo $i;?>]">
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="padding-top-25 padding-right-15 padding-bottom-15 text-right">
                    <button type="button" data-dismiss="modal">close</button>
                    <button type="submit" class="default">save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal draggable-header" id="modal-edit-periode">
    <div class="modal-dialog">
        <div class="modal-header">
            <h3 class="modal-title"><?php echo _('Add Periode');?></h3>
            <a href="javascript:void(0);" class="close">&times;</a>
        </div>
        <div class="modal-body no-padding">
            <form action="<?php echo BASE_URL;?>index.php?/periode/setting/save" method="post">
                <input type="hidden" name="periode_id" id="edit-periode_id">
                <div class="tabs-line">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-editperiode1" data-toggle="tab">Periode</a></li>
                        <li><a href="#tab-editperiode2" data-toggle="tab">Detail</a></li>
                    </ul>
                </div>
                <div class="tab-content border-top padding-15">
                     <div id="tab-editperiode1" class="tab-pane tab-modal fade in active">
                        <div class="form-group">
                            <label for="add-tpk_id" class="col-md-4"><?php echo _('Cooperative');?></label>
                            <div class="col-md-8">
                                <select name="periode_koperasi" id="edit-periode_koperasi" class="input-block" required="required">
                                    <option value="">-- select Cooperative --</option>
                                    <?php foreach($koperasi['records'] as $k=>$v){ ?>
                                        <option value="<?php echo $v['koperasi_id']; ?>"><?php echo $v['koperasi_nama'];?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="edit-periode_effectivedate" class="col-md-4"><?php echo _('Effective Date');?> </label>
                            <div class="col-md-8">
                                <input type="text" id="edit-periode_effectivedate" class="input-block datepicker" name="periode_effectivedate" required/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4" for="edit-periode_status">Status</label>
                            <div class="col-md-8">
                                <select name="periode_status" id="edit-periode_status" class="input-block status"></select>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab-editperiode2">
                        <table class="table border">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Periode</th>
                                    <th>Start</th>
                                    <th>End</th>
                                </tr>
                            </thead>    
                            <tbody>
                                <?php for($i=1;$i<=3;$i++){ ?>
                                <tr>
                                    <td>
                                        <input type="checkbox" class="editcheckbox" id="edit-periodedetail_check<?php echo $i;?>" name="periodedetail_check[<?php echo $i;?>]" value="<?php echo $i;?>"><label for=""></label>
                                    </td>
                                    <td>
                                        <input type="text" id="edit-periodedetail_name<?php echo $i;?>" class="input-block text-center number" value="<?php echo $i;?>" name="periodedetail_name[<?php echo $i;?>]">
                                    </td>
                                    <td>
                                        <input type="text" id="edit-periodedetail_startdate<?php echo $i;?>" class="input-block text-center number" name="periodedetail_startdate[<?php echo $i;?>]">
                                    </td>
                                    <td>
                                        <input type="text" id="edit-periodedetail_enddate<?php echo $i;?>" class="input-block text-center number" name="periodedetail_enddate[<?php echo $i;?>]">
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="padding-top-25 padding-right-15 padding-bottom-15 text-right">
                    <button type="button" data-dismiss="modal">close</button>
                    <button type="submit" class="default">save</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('.grid-pagging').transpaginator({
            totalPages: <?php echo $periode['total_page'];?>,
            currentPage: <?php echo $periode['page'];?>,
            totalData: <?php echo $periode['total_data'];?>,
            pageUrl: '<?php echo BASE_URL;?>index.php?/periode/setting/&keyword=<?php echo $periode['keyword'];?>&page='
        });
        $('.tr_selector').on('click',function(){
            var id = $(this).attr('data-id');
            $('.editbtn').removeClass('fg-lightgray').removeAttr('disabled').attr('data-id',id);
            $('.tr_selector').removeClass('active');
            $(this).addClass('active');
        });
        $('#__edit').on('click',function(){
            var id = $(this).attr('data-id');
            if(typeof id !== 'undefined' && id !== ''){
                $.ajax({
                    url:'<?php echo BASE_URL;?>index.php?/periode/setting/edit/'+id,
                    dataType:'json',
                    success:function(res){
                        $('#edit-periode_id').val(res.periode_id);
                        $('#edit-periode_status').val(res.periode_status);
                        $('#edit-periode_effectivedate').val(moment(res.periode_effectivedate).format('DD/MM/YYYY'));
                        $('#edit-periode_interval').val(res.periode_interval);
                        $('#edit-periode_koperasi').val(res.periode_koperasi);
                        
                        $('.editcheckbox').prop('checked',false);
                        var detail = res.periodedetail;
                        var i = 1;
                        for(var k in detail){
                            $('#edit-periodedetail_check'+i).prop('checked',true);
                            $('#edit-periodedetail_name'+i).val(detail[k].periodedetail_name);
                            $('#edit-periodedetail_startdate'+i).val(detail[k].periodedetail_startdate);
                            $('#edit-periodedetail_enddate'+i).val(detail[k].periodedetail_enddate);
                            i++;
                        }
                        $('#modal-edit-periode').addClass('fadein');
                    }
                });
            }
        });
        
        $('#__delete').on('click',function(){
            var id = $(this).attr('data-id');
            if(typeof id !== 'undefined' && id !== ''){
                if(confirm('<?php echo _('Are you sure want to delete this data?');?>')){
                    $.ajax({
                        url:'<?php echo BASE_URL;?>index.php?/anggota/setting/delete',
                        type:'post',
                        data:{
                            anggota_id:id
                        },
                        success:function(){
                            location.reload();
                        }
                    });
                }
            }
        });

    });

    function filter(){
        var keyword = $('#keyword').val();
        window.location.href = '<?php echo BASE_URL;?>index.php?/anggota/setting/&keyword='+keyword+'&page=1';
    }
</script>
<?php View::inc('footer.php');?>
