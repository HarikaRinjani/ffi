<li>
    <a href="javascript:void(0);" class="navtoggle"><i class="ion-chevron-down"></i></a>
    <h3 class="title">Bookmarks</h3>
    <ul class="active" id="bookmarks-content">
        <li id="bookmark-btn-li">
            <a href="javascript:void(0);" id="bookmark-btn"><i class="ion-ios7-star-outline"></i>Bookmark this page</a>
        </li>
    </ul>
</li>
<script type="text/javascript">
    $(document).ready(function(){
        $.ajax({
            url:'<?php echo BASE_URL;?>index.php?/bookmark/load',
            dataType:'json',
            success:function(res){
                if(res.total_data > 0){
                    for(var k in res.records){
                        var bookmark = '<li><a href="<?php echo BASE_URL;?>'+res.records[k].bookmark_url+'" title="'+res.records[k].bookmark_title+'"><i class="ion-ios7-copy-outline"></i>'+res.records[k].bookmark_title+'</a></li>';
                        $(bookmark).insertBefore('#bookmark-btn-li');

                    }
                }
            }
        });
        $('#bookmark-btn').on('click',function(e){
            e.preventDefault();
            var url = window.location.href;
            var title = document.title;
            $.ajax({
                url:'<?php echo BASE_URL;?>index.php?/bookmark/save',
                type:'post',
                data:{
                    'url':url,
                    'title':title
                },
                success:function(res){
                    if(res == 'success'){
                        var bookmark = '<li><a href="'+url+'" title="'+title+'"><i class="ion-ios7-copy-outline"></i>'+title+'</a></li>';
                        $(bookmark).insertBefore('#bookmark-btn-li');
                    }else{
                        var notifikasi = '<div id="bookmark-notification" class="notification window no-border" >';
                        notifikasi += '<div class="window-header no-border" ><a href="javascript:void(0);" class="close fg-lightgray" onclick="$(this).closest(\'.window\').fadeOut();" >&times;</a></div>';
                        notifikasi += '<div class="window-body">'+res+'</div>';
                        notifikasi += '</div>';
                        $('body').append(notifikasi);
                        setTimeout(function(){
                            $('#bookmark-notification').fadeOut();
                        },5000);
                    }
                    return false;
                }
            });
        });
    });
</script>