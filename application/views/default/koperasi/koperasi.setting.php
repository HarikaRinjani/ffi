<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('settings/settings.menu.php');?>

<div class="content padded">
    <div class="breadcrumb">
        <ul class="tab">
            <li >
                <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-add-department">add new</a>
            </li>
            <li class="active">
                <a href="<?php echo BASE_URL;?>index.php?/koperasi/setting" disabled="disabled">Cooperative</a>
            </li>
        </ul>
        <div class="pull-right">
            <ul class="breadcrumb-nav">
                <li>
                    <a href="">Apps Settings</a>
                </li>
                <li>
                    <a href="">koperasi</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row padding-bottom-40">
            <div class="ribbon  border-bottom text-right">
                <div class="menu">
                    <a href="javascript:void(0);" id="__edit" class="padding-right-10 fg-lightgray editbtn"><i class="ion-ios7-compose-outline"></i>edit</a>
                    <a href="javascript:void(0);" id="__delete" class="padding-right-10 fg-lightgray editbtn"><i class="ion-ios7-trash-outline"></i>delete</a>
                </div>
                <form action="javascript:void(0);" id="searchForm" onsubmit="return filter();">
                    <input type="text" placeholder="Search..." id="keyword" style="width:200px" value="<?php echo $koperasi['keyword'];?>">
                </form>
            </div>
            <table class="table table-hover">
                <thead>
                <tr>
                    <th style="width:24px;">#</th>
                    <th><?php echo _('Coop Name');?></th>
                    <th><?php echo _('Coop Code');?></th>
                    <th><?php echo _('Address');?></th>
                    <th><?php echo _('Phone');?></th>
                    <th><?php echo _('Status');?></th>
                </tr>
                </thead>
                <tbody>
                <?php if($koperasi['total_data'] == 0){ ?>
                    <tr>
                        <td colspan="5"><?php echo _('No record found');?></td>
                    </tr>
                <?php }else{ ?>
                    <?php foreach($koperasi['records'] as $k=>$v){ ?>
                        <tr class="tr_selector cursor-pointer" data-id="<?php echo $v['koperasi_id'];?>" id="tr<?php echo $v['koperasi_id'];?>">
                            <td>
                                <?php
                                if($koperasi['page'] == 1){
                                    echo ($k+1);
                                }else{
                                    echo ($k + 1 + $koperasi['limit']);
                                }

                                if($v['koperasi_logo'] == '' || $v['koperasi_logo'] == null){
                                    $logo = 'index.php?/load/file/'.urlencode('images/photo.png');
                                }else{
                                    $logo = $v['koperasi_logo'];
                                }
                                ?>
                            </td>
                            <td style="vertical-align:middle;min-width:200px;">
                                <img src="<?php echo $logo;?>" alt="" class="pull-left" style="margin-right:5px;width:32px;height:32px;vertical-align:middle">
                                <?php echo $v['koperasi_nama'];?>
                            </td>
                            <td><?php echo $v['koperasi_kode'];?></td>
                            <td><?php echo $v['koperasi_alamat'];?></td>
                            <td><?php echo $v['koperasi_telp'];?></td>
                            <td><span class="status"><?php echo $v['koperasi_status'];?></span></td>
                        </tr>
                    <?php } ?>
                <?php } ?>
                </tbody>
            </table>
            <div class="grid-pagging border-top margin-bottom-30"></div>
        </div>
    </div>
</div>
<div class="modal draggable" id="modal-add-department">
    <div class="modal-dialog">
        <div class="modal-header">
            <h3 class="modal-title">Add new Cooperative</h3>
            <a href="javascript:void(0);" class="close">&times;</a>
        </div>
        <div class="modal-body">
            <form action="<?php echo BASE_URL;?>index.php?/koperasi/setting/save" method="post">
                <div class="form-group">
                    <label for="add-department_name" class="col-md-4"><?php echo _('Coop Name');?></label>
                    <div class="col-md-8">
                        <input type="text" id="add-department_name" class="input-block" name="koperasi_nama" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="add-koperasi_kode" class="col-md-4"><?php echo _('Coop Code');?></label>
                    <div class="col-md-8">
                        <input type="text" id="add-koperasi_kode" maxlength="3" class="input-block" name="koperasi_kode" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4" for=""><?php echo _('Address');?></label>
                    <div class="col-md-8">
                        <textarea name="koperasi_alamat" id="koperasi_alamat" class="input-block" rows="3"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="add-koperasi_telp" class="col-md-4"><?php echo _('Phone');?></label>
                    <div class="col-md-8">
                        <input type="text" id="add-koperasi_telp" class="input-block number" name="koperasi_telp" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4" for="add-department-status">Status</label>
                    <div class="col-md-8">
                        <select name="koperasi_status" data-value="y" id="add-koperasi-status" class="input-block status"></select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4 col-md-offset-4">
                        <input type="file" name="koperasi_logo" id="add-koperasi_logo" class="image-upload">
                    </div>
                </div>
                <div class="padding-top-25 text-right">
                    <button type="button" data-dismiss="modal">close</button>
                    <button type="submit" class="default">save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal draggable" id="modal-edit-department">
    <div class="modal-dialog">
        <div class="modal-header">
            <h3 class="modal-title">Edit Cooperative</h3>
            <a href="javascript:void(0);" class="close">&times;</a>
        </div>
        <div class="modal-body">
            <form action="<?php echo BASE_URL;?>index.php?/koperasi/setting/save" method="post">
                <input type="hidden" name="koperasi_id" id="edit-koperasi_id"/>
                <div class="form-group">
                    <label for="add-department_name" class="col-md-4"><?php echo _('Coop Name');?></label>
                    <div class="col-md-8">
                        <input type="text" id="edit-koperasi_nama" class="input-block" name="koperasi_nama" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="edit-koperasi_kode" class="col-md-4"><?php echo _('Coop Code');?></label>
                    <div class="col-md-8">
                        <input type="text" id="edit-koperasi_kode" maxlength="3" class="input-block" name="koperasi_kode" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4" for=""><?php echo _('Address');?></label>
                    <div class="col-md-8">
                        <textarea name="koperasi_alamat" id="edit-koperasi_alamat" class="input-block" rows="3"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="add-koperasi_telp" class="col-md-4"><?php echo _('Phone');?></label>
                    <div class="col-md-8">
                        <input type="text" id="edit-koperasi_telp" class="input-block number" name="koperasi_telp" required/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-4" for="edit-koperasi_status">Status</label>
                    <div class="col-md-8">
                        <select name="koperasi_status" id="edit-koperasi_status" class="input-block status"></select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4 col-md-offset-4">
                        <input type="hidden" name="koperasi_logo" id="edit-koperasi_logo"/>
                        <img id="edit-photo-preview" src="index.php?/load/file/<?php echo urlencode('images/photo.png');?>" alt="" class="img-responsive"/>
                    </div>
                </div>
                <div class="padding-top-25 text-right">
                    <button type="button" data-dismiss="modal">close</button>
                    <button type="submit" class="default">save</button>
                </div>
            </form>
        </div>
    </div>
</div>
<input type="file" name="" id="edit-uploader" style="width:1px;height:1px;visibility: hidden"/>
<script type="text/javascript">
    $(document).ready(function(){
        $('#edit-photo-preview').on('click',function(){
            $('#edit-uploader').trigger('click');
        });
        $('#edit-uploader').uploader({
            resize:true,
            onSuccess:function(res){
                $('#edit-koperasi_logo').val(res);
                $('#edit-photo-preview').attr('src',res);
            }
        });
        $('.grid-pagging').transpaginator({
            totalPages: <?php echo $koperasi['total_page'];?>,
            totalData: <?php echo $koperasi['total_data'];?>,
            currentPage: <?php echo $koperasi['page'];?>,
            pageUrl: '<?php echo BASE_URL;?>index.php?/koperasi/setting/&keyword=<?php echo $koperasi['keyword'];?>&page='
        });
        $('.tr_selector').on('click',function(){
            var id = $(this).attr('data-id');
            $('.editbtn').removeClass('fg-lightgray').removeAttr('disabled').attr('data-id',id);
            $('.tr_selector').removeClass('active');
            $(this).addClass('active');
        });
        $('#__edit').on('click',function(){
            var id = $(this).attr('data-id');
            if(typeof id !== 'undefined' && id !== ''){
                $.ajax({
                    url:'<?php echo BASE_URL;?>index.php?/koperasi/setting/edit/'+id,
                    dataType:'json',
                    success:function(res){
                        $('#edit-koperasi_id').val(res.koperasi_id);
                        $('#edit-koperasi_nama').val(res.koperasi_nama);
                        $('#edit-koperasi_status').val(res.koperasi_status);
                        $('#edit-koperasi_telp').val(res.koperasi_telp);
                        $('#edit-koperasi_kode').val(res.koperasi_kode);
                        if(res.koperasi_logo === '' || res.koperasi_logo === null){
                            $('#edit-koperasi_logo').val('');
                            $('#edit-photo-preview').attr('src','index.php?/load/file/<?php echo urlencode('images/photo.png');?>');
                        }else{
                            $('#edit-koperasi_logo').val(res.koperasi_logo);
                            $('#edit-photo-preview').attr('src',res.koperasi_logo);
                        }

                        $('#edit-koperasi_alamat').text(res.koperasi_alamat);
                        $('#modal-edit-department').addClass('fadein');
                    }
                });
            }
        });

        $('#__delete').on('click',function(){
            var id = $(this).attr('data-id');
            if(typeof id !== 'undefined' && id !== ''){
                if(confirm('<?php echo _('Are you sure want to delete this data?');?>')){
                    $.ajax({
                        url:'<?php echo BASE_URL;?>index.php?/koperasi/setting/delete',
                        type:'post',
                        data:{
                            koperasi_id:id
                        },
                        success:function(){
                            location.reload();
                        }
                    });
                }
            }
        });

    });

    function filter(){
        var keyword = $('#keyword').val();
        window.location.href = '<?php echo BASE_URL;?>index.php?/koperasi/setting/&keyword='+keyword+'&page=1';
    }
</script>
<?php View::inc('footer.php');?>
