<?php
View::inc('header.php');
View::inc('navbar.php');
View::inc('qcparameter/qcparameter.menu.php');

?>
<div class="content padded">
    <div class="breadcrumb">
        <ul class="tab">
            <li class="active">
                <a href="<?php echo BASE_URL;?>index.php?/qcparameter/index" disabled="disabled">Quality Data</a>
            </li>
        </ul>
        <div class="pull-right">
            <ul class="breadcrumb-nav">
                <li>
                    <a href="">Quality</a>
                </li>
                <li>
                    <a href="">Data</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row padding-bottom-40">
            
            <div style="overflow-x:auto;width:100%;display:block;">
            	<table class="table table-hover border-bottom" style="width:auto;min-width:100%;">
	                <thead>
	                <tr>
                        <th class="border-right dotted">#</th>
                        <th class="border-right dotted">Farmer ID</th>
                        <th class="border-right dotted">Farmer Name</th>
						<th class="border-right dotted">Transaction ID</th>
                        <th class="border-right dotted">Transaction Date</th>
	                    <th class="border-right dotted">Scan Date</th>
	                    <th class="border-right dotted">CFU</th>
	                    <!-- <th class="border-right dotted">IBC</th> -->
	                    <th class="border-right dotted">FAT</th>
	                    <th class="border-right dotted">Protein</th>
	                    <th class="border-right dotted">Lactose</th>
	                    <th class="border-right dotted">TS</th>
	                    <th class="border-right dotted">FP</th>
	                    <th class="border-right dotted">Acidity</th>
	                    <th class="border-right dotted">Urea</th>
	                    <th>SNF</th>
	                </tr>
	                </thead>
	                <tbody>
	                <?php if($data['total_data'] == 0){ ?>
	                    <tr>
	                        <td colspan="100%"><?php echo _('No record found');?></td>
	                    </tr>
	                <?php }else{ ?>
	                    <?php 
                        $tempID = '';
                        $no = 0;
                        $light = true;
                        foreach($data['records'] as $k=>$v){ 
                            if($tempID !== $v['transaksi_nomor']){
                                $tempID = $v['transaksi_nomor'];
                                $nomor = $v['transaksi_nomor'];
                                $nama = $v['transaksi_nama'];
                                $no++;
                                $index = $no;
                                if($light === true){
                                    $color = 'bg-white';
                                    $light = false;
                                }else{
                                    $color = 'bg-lightgray';
                                    $light = true;
                                }
                            }else{
                                $nomor = '';
                                $nama = '';
                                $index = '';
                            }
                            ?>
	                        <tr class="tr_selector cursor-pointer <?php echo $color;?>" data-id="<?php echo $v['qcparameter_id'];?>" id="tr<?php echo $v['qcparameter_id'];?>">
                                <td class="border-right dotted"><?php echo $index;?></td>
                                <td class="border-right dotted"><?php echo $nomor;?></td>
                                <td class="border-right dotted"><?php echo $nama;?></td>
	                            <td class="border-right dotted"><?php echo $v['transaksi_id'];?></td>
								<td class="border-right dotted format-datetime"><?php echo $v['transaksi_waktu'];?></td>
								<td class="border-right dotted format-datetime"><?php echo $v['qcparameter_scandate'];?></td>
	                            <td class="border-right dotted"><?php echo (float) $v['qcparameter_cfu'];?></td>
	                            <!-- <td class="border-right dotted"><?php echo (float) $v['qcparameter_ibc'];?></td> -->
	                            <td class="border-right dotted"><?php echo (float) $v['qcparameter_fat'];?></td>
	                            <td class="border-right dotted"><?php echo (float) $v['qcparameter_protein'];?></td>
	                            <td class="border-right dotted"><?php echo (float) $v['qcparameter_lactose'];?></td>
	                            <td class="border-right dotted"><?php echo (float) $v['qcparameter_ts'];?></td>
	                            <td class="border-right dotted"><?php echo (float) $v['qcparameter_fp'];?></td>
	                            <td class="border-right dotted"><?php echo (float) $v['qcparameter_acidity'];?></td>
	                            <td class="border-right dotted"><?php echo (float) $v['qcparameter_urea'];?></td>
	                            <td><?php echo (float) $v['qcparameter_snf'];?></td>
	                        </tr>
	                    <?php } ?>
	                <?php } ?>
	                </tbody>
	            </table>
               
            </div>
        </div>
    </div>
</div>
<?php View::inc('footer.php');?>
