<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('qcparameter/qcparameter.menu.php');?>

<div class="content padded">
    <div class="breadcrumb">
        <ul class="tab">
            <li class="">
                <a href="<?php echo BASE_URL;?>index.php?/qcparameter/index">Quality Data</a>
            </li>
            <li class="active">
                <a href="<?php echo BASE_URL;?>index.php?/qcparameter/log"  disabled="disabled">Logs</a>
            </li>
        </ul>
        <div class="pull-right">
            <ul class="breadcrumb-nav">
                <li>
                    <a href="">Quality</a>
                </li>
                <li>
                    <a href="">Logs</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
             <div class="col-md-12 padding-top-15">
                <div class="padding-20 bg-black fg-green" style="height:400px;overflow:auto;">
                    <pre id="log-content" style="display:block"></pre>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        var timenya = '';
        reloadLog();
    });
    function reloadLog(time){
        if(typeof time === 'undefined'){
            time = '';
        }
        $.ajax({
            url:'<?php echo BASE_URL;?>index.php?/qcparameter/refreshLog/&date='+time,
            dataType:'json',
            type:'get',
            success:function(res){
                if(res.time !== ''){
                    $('#log-content').append(res.content);
                    $('#log-content').parent().scrollTop($("#log-content").height());
                }
                setTimeout(function(){
                    reloadLog(res.time);
                },300);
            }
        });
    }
</script>
<?php View::inc('footer.php');?>
