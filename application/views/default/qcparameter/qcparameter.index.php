<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('qcparameter/qcparameter.menu.php');?>
<link rel="stylesheet" href="<?php echo BASE_URL;?>themes/default/jquery-tokeninput/styles/token-input-facebook.css"/>
<style type="text/css">
    div.token-input-dropdown-facebook {
        z-index:999999;
    }
</style>
<div class="content padded" xmlns="http://www.w3.org/1999/html">
    <div class="breadcrumb">
        <ul class="tab">
		    <?php if($enableAdd == 'y'){ ?>
            <li>
                <a href="javascript:void(0);" data-toggle="modal" data-target="#manualinput">Add New</a>
            </li>
			<?php  } ?>
            <li class="active">
                <a href="<?php echo BASE_URL;?>index.php?/qcparameter/index" disabled="disabled">Quality Data</a>
            </li>
            <li>
            	<a href="<?php echo BASE_URL;?>index.php?/qcparameter/log">Logs</a>
            </li>
        </ul>
        <div class="pull-right">
            <ul class="breadcrumb-nav">
                <li>
                    <a href="">Quality</a>
                </li>
                <li>
                    <a href="">Data</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row padding-bottom-40">
            <div class="ribbon border-bottom text-right">
                <div class="menu">
                    <a href="" class="fg-gray padding-right-10"><i class="ion-ios7-folder-outline"></i> lists</a>
					<?php if($enableEdit == 'y') { ?>
                    <a href="" class="separator"></a>
                    <a href="javascript:void(0);" id="__edit" class="fg-lightgray editbtn"><i class="ion-ios7-compose-outline"></i>edit</a>
                    <a href="javascript:void(0);" id="__cancel" class=" fg-lightgray savebtn"><i class="ion-ios7-undo-outline"></i>cancel</a>
                    <a href="javascript:void(0);" id="__save" class="fg-lightgray savebtn"><i class="ion-ios7-save"></i>save</a>
					<?php } ?>
                </div>
                <form action="javascript:void(0);" id="searchForm" onsubmit="return filter();" style="display:inline-block;">
                    <select name="koperasi" id="inputfilter-koperasi" class="" onChange="filter();">
                        <option value="">All</option>
                        <?php
                        foreach($koperasi as $k=>$v){
                            echo '<option value="'.$v['koperasi_id'].'"';
                            if($v['koperasi_id'] == $_GET['koperasi']){
                                echo ' selected="selected"';
                            }
                            echo '>'.$v['koperasi_nama'].'</option>';
                        }
                        ?>
                    </select>
                    <select name="tpk" id="inputfilter-tpk" class="" onChange="filter();">
                        <option value="">All</option>
                        <?php
                        foreach($tpk as $k=>$v){
                            echo '<option value="'.$v['tpk_id'].'"';
                            if($v['tpk_id'] == $_GET['tpk']){
                                echo ' selected="selected"';
                            }
                            echo '>'.$v['tpk_nama'].'</option>';
                        }
                        ?>
                    </select>
                	<input type="text" id="qcdate" class="datepicker" value="<?php echo $tanggal;?>">
                    <input type="text" placeholder="Search..." id="keyword" style="width:200px" value="<?php echo $data['keyword'];?>">
                </form>
            </div>
            <div style="overflow-x:auto;width:100%;display:block;">
            	<table class="table table-hover border" style="width:auto;min-width:100%;border-top:0;border-left:0;border-right:0;">
	                <thead>
	                <tr>
	                    <th class="border-right dotted">ID</th>
                        <th class="border-right dotted">Farmer ID</th>
                        <th class="border-right dotted">Name</th>
                        <th class="border-right dotted">Group</th>
	                    <th class="border-right dotted">Date</th>
	                    <th class="border-right dotted">CFU</th>
	                    <!-- <th class="border-right dotted">IBC</th> -->
	                    <th class="border-right dotted">FAT</th>
	                    <th class="border-right dotted">Protein</th>
	                    <th class="border-right dotted">Lactose</th>
	                    <th class="border-right dotted">TS</th>
	                    <th class="border-right dotted">FP</th>
	                    <th class="border-right dotted">Acidity</th>
	                    <th class="border-right dotted">Urea</th>
	                    <th>SNF</th>
	                </tr>
	                </thead>
	                <tbody>
	                <?php if($data['total_data'] == 0){ ?>
	                    <tr>
	                        <td colspan="100%"><?php echo _('No record found');?></td>
	                    </tr>
	                <?php }else{ ?>
	                    <?php foreach($data['records'] as $k=>$v){ ?>
	                        <tr class="tr_selector cursor-pointer" data-id="<?php echo $v['qcparameter_id'];?>" id="tr<?php echo $v['qcparameter_id'];?>">
	                            <td class="border-right dotted"><?php echo $v['qcparameter_transaksi'];?></td>
                                <td class="border-right dotted"><?php echo $v['transaksi_nomor'];?></td>
                                <td class="border-right dotted"><?php echo $v['transaksi_nama'];?></td>
                                <td class="border-right dotted"><?php echo $v['kelompok_nama'];?></td>
	                            <td class="border-right dotted"><?php echo $v['qcparameter_scandate'];?></td>
	                            <td class="border-right dotted"><?php echo (float) $v['qcparameter_cfu'];?></td>
	                            <!-- <td class="border-right dotted"><?php echo (float) $v['qcparameter_ibc'];?></td> -->
	                            <td class="border-right dotted"><?php echo (float) $v['qcparameter_fat'];?></td>
	                            <td class="border-right dotted"><?php echo (float) $v['qcparameter_protein'];?></td>
	                            <td class="border-right dotted"><?php echo (float) $v['qcparameter_lactose'];?></td>
	                            <td class="border-right dotted"><?php echo (float) $v['qcparameter_ts'];?></td>
	                            <td class="border-right dotted"><?php echo (float) $v['qcparameter_fp'];?></td>
	                            <td class="border-right dotted"><?php echo (float) $v['qcparameter_acidity'];?></td>
	                            <td class="border-right dotted"><?php echo (float) $v['qcparameter_urea'];?></td>
	                            <td><?php echo (float) $v['qcparameter_snf'];?></td>
	                        </tr>
							<?php if($enableEdit == 'y') { ?>
	                        <tr class="tr_selector cursor-pointer" data-id="<?php echo $v['qcparameter_id'];?>" id="tredit<?php echo $v['qcparameter_id'];?>" style="display:none;">
	                            <td class="border-right dotted" style="padding:2px !important;"><?php echo $v['qcparameter_transaksi'];?></td>
                                <td class="border-right dotted" style="padding:2px !important;"><?php echo $v['transaksi_nomor'];?></td>
                                <td class="border-right dotted" style="padding:2px !important;"><?php echo $v['transaksi_nama'];?></td>
                                <td class="border-right dotted" style="padding:2px !important;"><?php echo $v['kelompok_nama'];?></td>
	                            <td class="border-right dotted" style="padding:2px !important;"><?php echo $v['qcparameter_scandate'];?></td>
	                            <td class="border-right dotted" style="padding:2px !important;"><input type="text" class="qcparameterinput<?php echo $v['qcparameter_id'];?> input-block" name="qcparameter_cfu" id="qcparameter_cfu<?php echo $v['qcparameter_id'];?>" value="<?php echo $v['qcparameter_cfu'];?>"></td>
	                            <!-- <td class="border-right dotted" style="padding:2px !important;"><input type="text" class="qcparameterinput<?php echo $v['qcparameter_id'];?> input-block" name="qcparameter_ibc" id="qcparameter_ibc<?php echo $v['qcparameter_id'];?>" value="<?php echo $v['qcparameter_ibc'];?>"></td> -->
	                            <td class="border-right dotted" style="padding:2px !important;"><input type="text" class="qcparameterinput<?php echo $v['qcparameter_id'];?> input-block" name="qcparameter_fat" id="qcparameter_fat<?php echo $v['qcparameter_id'];?> __fatinput" value="<?php echo $v['qcparameter_fat'];?>"></td>
	                            <td class="border-right dotted" style="padding:2px !important;"><input type="text" class="qcparameterinput<?php echo $v['qcparameter_id'];?> input-block" name="qcparameter_protein" id="qcparameter_protein<?php echo $v['qcparameter_id'];?>" value="<?php echo $v['qcparameter_protein'];?>"></td>
	                            <td class="border-right dotted" style="padding:2px !important;"><input type="text" class="qcparameterinput<?php echo $v['qcparameter_id'];?> input-block" name="qcparameter_lactose" id="qcparameter_lactose<?php echo $v['qcparameter_id'];?>" value="<?php echo $v['qcparameter_lactose'];?>"></td>
	                            <td class="border-right dotted" style="padding:2px !important;"><input type="text" class="qcparameterinput<?php echo $v['qcparameter_id'];?> input-block" name="qcparameter_ts" id="qcparameter_ts<?php echo $v['qcparameter_id'];?> __tsinput" value="<?php echo $v['qcparameter_ts'];?>"></td>
	                            <td class="border-right dotted" style="padding:2px !important;"><input type="text" class="qcparameterinput<?php echo $v['qcparameter_id'];?> input-block" name="qcparameter_fp" id="qcparameter_fp<?php echo $v['qcparameter_id'];?>" value="<?php echo $v['qcparameter_fp'];?>"></td>
	                            <td class="border-right dotted" style="padding:2px !important;"><input type="text" class="qcparameterinput<?php echo $v['qcparameter_id'];?> input-block" name="qcparameter_acidity" id="qcparameter_acidity<?php echo $v['qcparameter_id'];?>" value="<?php echo $v['qcparameter_acidity'];?>"></td>
	                            <td class="border-right dotted" style="padding:2px !important;"><input type="text" class="qcparameterinput<?php echo $v['qcparameter_id'];?> input-block" name="qcparameter_urea" id="qcparameter_urea<?php echo $v['qcparameter_id'];?>" value="<?php echo $v['qcparameter_urea'];?>"></td>
	                            <td  style="padding:2px !important;"><input type="text" class="qcparameterinput<?php echo $v['qcparameter_id'];?> input-block" name="" id="qcparameter_snf<?php echo $v['qcparameter_id'];?>" value="<?php echo $v['qcparameter_snf'];?>" disabled="disabled"></td>
	                        </tr>
							<?php } ?>
	                    <?php } ?>
	                <?php } ?>
	                </tbody>
	            </table>
            </div>
            <div class="grid-pagging border-top margin-bottom-30"></div>
        </div>
    </div>
</div>
<div class="modal" id="manualinput">
    <div class="modal-dialog modal-lg">
        <div class="modal-header">
            <h3 class="modal-title">New Quality Data</h3>
            <a href="javascript:void(0)" class="close">&times;</a>
        </div>
        <div class="modal-body">
            <form action="<?php echo BASE_URL;?>index.php?/qcparameter/addnew" method="post">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="add-qcparameter_scandate" class="col-md-4">Date</label>
                            <div class="col-md-8">
                                <input type="text" class="input-block datepicker" name="qcparameter_scandate" required />
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-qcparameter_transaksi" class="col-md-4">Transaction ID</label>
                            <div class="col-md-8">
                                <input type="text" id="add-qcparameter_transaksi" class="input-block number" name="qcparameter_transaksi" required />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="padding-10 border-top" id="add-transaksi_detail">
                                    <table class="table no-border">
                                        <tr>
                                            <td>
                                                Farmer ID
                                            </td>
                                            <td id="detail-transaksi_nomor"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Name
                                            </td>
                                            <td id="detail-transaksi_nama"></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Date
                                            </td>
                                            <td id="detail-transaksi_waktu"></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="add-qcparameter_cfu" class="col-md-4">CFU</label>
                            <div class="col-md-8">
                                <input type="text" name="qcparameter_cfu" class="input-block numeric">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-qcparameter_ibc" class="col-md-4">IBC</label>
                            <div class="col-md-8">
                                <input type="text" name="qcparameter_ibc" class="input-block numeric">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-qcparameter_fat" class="col-md-4">FAT</label>
                            <div class="col-md-8">
                                <input type="text" name="qcparameter_fat" class="input-block numeric">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-qcparameter_protein" class="col-md-4">PROTEIN</label>
                            <div class="col-md-8">
                                <input type="text" name="qcparameter_protein" class="input-block numeric">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-qcparameter_lactose" class="col-md-4">LACTOSE</label>
                            <div class="col-md-8">
                                <input type="text" name="qcparameter_lactose" class="input-block numeric">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-qcparameter_ts" class="col-md-4">TS</label>
                            <div class="col-md-8">
                                <input type="text" name="qcparameter_ts" class="input-block numeric">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-qcparameter_fp" class="col-md-4">FP</label>
                            <div class="col-md-8">
                                <input type="text" name="qcparameter_fp" class="input-block numeric">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-qcparameter_acidity" class="col-md-4">ACIDITY</label>
                            <div class="col-md-8">
                                <input type="text" name="qcparameter_acidity" class="input-block numeric">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-qcparameter_urea" class="col-md-4">UREA</label>
                            <div class="col-md-8">
                                <input type="text" name="qcparameter_urea" class="input-block numeric">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 text-right">
                        <button type="button" data-dismiss="modal">Close</button>
                        <button type="submit" type="submit" class="default">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo BASE_URL;?>themes/default/jquery-tokeninput/src/jquery.tokeninput.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#add-qcparameter_transaksi').tokenInput('<?php echo BASE_URL;?>index.php?/load/transaksi',{
            'placeholder': 'transaction id: ',
            method:'POST',
            theme: "facebook",
            onAdd: function (item) {
                $('#detail-transaksi_nama').html(item.transaksi_nama);
                $('#detail-transaksi_nomor').html(item.transaksi_nomor);
                $('#detail-transaksi_waktu').html(moment(item.transaksi_waktu).format('DD MMM YYYY'));
                $('#add-qcparameter_transaksi').val(item.transaksi_id);
            }
        });
        $('.grid-pagging').transpaginator({
            totalPages: <?php echo $data['total_page'];?>,
            currentPage: <?php echo $data['page'];?>,
            pageUrl: '<?php echo BASE_URL;?>index.php?/qcparameter/index/&keyword=<?php echo $data['keyword'];?>&date=<?php echo $tanggal;?>&page='
        });
        $('.tr_selector').on('click',function(){
            var editting = $('.editbtn').attr('editting');
            if(typeof editting === 'undefined' || editting === 'n'){
            	var id = $(this).attr('data-id');
	            $('.editbtn').removeClass('fg-lightgray').removeAttr('disabled').attr('data-id',id);
	            $('.savebtn').attr('data-id',id);
	            $('.tr_selector').removeClass('active');
	            $(this).addClass('active');
            }

        });
        $('#qcdate').on('change',function(){
        	filter();
        });
        $('#__edit').on('click',function(){
        	var id = $(this).attr('data-id');
            if(typeof id !== 'undefined' && id !== ''){
                $('#tr'+id).hide();
                $('#tredit'+id).show();
                $('.savebtn').removeClass('fg-lightgray');
                $('.editbtn').addClass('fg-lightgray');
                $('.editbtn').attr('editting','y');
            }

        });
        $('#__cancel').on('click',function(){
        	var id = $(this).attr('data-id');
            if(typeof id !== 'undefined' && id !== ''){
            	$('#tr'+id).show();
            	$('#tredit'+id).hide();
            	$('.savebtn').addClass('fg-lightgray');
            	$('.editbtn').removeClass('fg-lightgray');
            	$('.editbtn').attr('editting','n');
            }
        });
        $('#__save').on('click',function(){
        	var id = $(this).attr('data-id');
            if(typeof id !== 'undefined' && id !== ''){
            	var data = {};
            	$('.qcparameterinput'+id).each(function(){
            		var key = $(this).attr('name');
            		var value = $(this).val();
            		data[key] = value;
            	});
            	data['qcparameter_id'] = id;
            	$.ajax({
            		url:'<?php echo BASE_URL;?>index.php?/qcparameter/save',
            		type:'post',
            		data:data,
            		success:function(){
            			location.reload();
            		}
            	});
            }
        });
   });

    function filter(){
        var keyword = $('#keyword').val();
        var tanggal = $('#qcdate').val();
        var koperasi = $('#inputfilter-koperasi').val();
        var tpk = $('#inputfilter-tpk').val();
        window.location.href = '<?php echo BASE_URL;?>index.php?/qcparameter/index/&koperasi='+koperasi+'&tpk='+tpk+'&keyword='+keyword+'&date='+tanggal+'&page=1';
    }
</script>
<?php View::inc('footer.php');?>
