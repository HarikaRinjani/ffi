<div class="sidebar">
    <span id="resize-handler" class="hidden-xs hidden-sm ui-resizable-handle ui-resizable-e">&nbsp;</span>
    <a href="javascript:void(0);" class="visible-sm toggleSidebar"><i class="ion-ios7-more"></i></a>
    <?php View::inc('koperasilogo.php');?>
    <ul class="navigation icon">
        <?php View::inc('bookmark/bookmark.php');?>
        <li>
            <h3 class="title">Quality</h3>
            <a href="javascript:void(0);" class="navtoggle"><i class="ion-chevron-down"></i></a>
            <ul>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/qcparameter/index"><i class="ion-ios7-folder-outline"></i>Quality Data</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/qcparameter/log"><i class="ion-ios7-time-outline"></i>Data Logs</a>
                </li>
                <li>
                    <a href="<?php echo BASE_URL;?>index.php?/samplingschedule/data"><i class="ion-ios7-calendar-outline"></i>Sampling Schedule</a>
                </li>
            </ul>
        </li>
    </ul>
</div>
