<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('distribution/distribution.menu.php');?>
<div class="content padded bg-lightgray">
    <div class="breadcrumb">
        <ul class="tab">
            <li class="active">
                <a href="<?php echo BASE_URL;?>index.php?/distribution/addnew" disabled="disabled">view detail</a>
            </li>
            <li>
                <a href="<?php echo BASE_URL;?>index.php?/distribution/index/&amp;status=n">On Delivery</a>
            </li>
            <li>
                <a href="<?php echo BASE_URL;?>index.php?/distribution/index/&amp;status=y">Completed</a>
            </li>
            <li>
                <a href="<?php echo BASE_URL;?>index.php?/distribution/index/&amp;status=r">Rejected</a>
            </li>
        </ul>
        <div class="pull-right">
            <ul class="breadcrumb-nav">
                <li>
                    <a href="">Distribution</a>
                </li>
                <li>
                    <a href="">Add New</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="ribbon border-bottom">
                <div class="menu">
                    <a class="" href="<?php echo BASE_URL;?>index.php?/distribution/index"><i class="ion-ios7-arrow-left"></i> back</a>
                </div>
            </div>
            <div class="col-md-10 col-md-offset-1 padding-top-20">
                <div class="window padding-40">
                    <div class="window-body">
                        <form action="javascript:void(0);" method="post">
                            <div class="padding-bottom-20" style="border-bottom:4px solid #888;margin-bottom:10px;">
                                <div class="col-xs-2">
                                    <img src="<?php echo $data['koperasi_logo'];?>" alt="" class="img-responsive">
                                </div>
                                <div class="col-xs-10">
                                    <div style="">
                                        <div class="text-center">
                                            <h1 class="no-margin no-padding"><?php echo $data['koperasi_nama'];?></h1>
                                            <h4><?php echo $data['koperasi_alamat'];?></h4>
                                            <div><?php echo $data['koperasi_telp'];?></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label for="" class="col-md-4"><?php echo _('DO Number');?></label>
                                    <div class="col-md-8">
                                        <?php echo $data['distribution_nomor'];?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-md-4"><?php echo _('Customer');?></label>
                                    <div class="col-md-8">
                                        <?php echo $data['konsumen_nama'];?><br>
                                        <?php echo $data['konsumen_alamat'];?><br>
                                        <?php echo $data['konsumen_telp'];?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label for="" class="col-md-4"><?php echo _('Date');?></label>
                                    <div class="col-md-8 format-date">
                                        <?php echo $data['distribution_tanggal'];?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-md-4"><?php echo _('Truck Number');?></label>
                                    <div class="col-md-8">
                                        <?php echo $data['truck_nopol'];?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-md-4"><?php echo _('Leave');?></label>
                                    <div class="col-md-8 format-time">
                                        <?php echo $data['distribution_berangkatjam'];?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-md-4"><?php echo _('Check in');?> I.P.S</label>
                                    <div class="col-md-8">
                                        <?php echo $data['distribution_masukips'];?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-md-4"><?php echo _('Check out');?> I.P.S</label>
                                    <div class="col-md-8">
                                        <?php echo $data['distribution_keluarips'];?>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="padding-top-15">
                                <p class="text-center" style="font-size:32px;"><?php echo _('Delivery Order Form');?></p>
                                <div class="padding-bottom-15 text-center">
                                    <h3><?php echo _('Along with this, we delivered fresh milk as follows:');?></h3>
                                </div>
                                <table class="table border full-border no-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><?php echo _('Analysis');?></th>
                                            <th class="text-center"><?php echo _('Description');?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="width:50%">
                                                <div class="form-group">
                                                    <label for="" class="col-md-4"><?php echo _('Weight');?></label>
                                                    <div class="col-md-8">
                                                        <?php echo $data['distribution_berat'];?> Kg
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4"><?php echo _('Density');?></label>
                                                    <div class="col-md-8">
                                                        <?php echo $data['distribution_beratjenis'];?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4"><?php echo _('Temperature');?></label>
                                                    <div class="col-md-8">
                                                        <?php echo $data['distribution_temperature'];?>
                                                        <span class="append-text"><sup>o</sup>C</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4"><?php echo _('Alcohol Test');?></label>
                                                    <div class="col-md-8">
                                                        <?php 
                                                        if($data['distribution_alkohol'] == 0){
                                                            echo 'Negative';
                                                        }else{
                                                            echo 'Positive';
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4"><?php echo _('FAT');?></label>
                                                    <div class="col-md-8">
                                                        <?php echo $data['distribution_fat'];?>
                                                        <span class="append-text">%</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4">Protein</label>
                                                    <div class="col-md-8">
                                                        <?php echo $data['distribution_protein'];?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4">Lactose</label>
                                                    <div class="col-md-8">
                                                        <?php echo $data['distribution_lactose'];?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4">Freezing Point</label>
                                                    <div class="col-md-8">
                                                        <?php echo $data['distribution_fp'];?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4">Total Solid</label>
                                                    <div class="col-md-8">
                                                        <?php echo $data['distribution_ts'];?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4">Acidity</label>
                                                    <div class="col-md-8">
                                                        <?php echo $data['distribution_acidity'];?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4">Urea</label>
                                                    <div class="col-md-8">
                                                        <?php echo $data['distribution_urea'];?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4">SNF</label>
                                                    <div class="col-md-8">
                                                        <?php echo $data['distribution_snf'];?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4">TPC</label>
                                                    <div class="col-md-8">
                                                        <?php echo $data['distribution_tpc'];?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4"><?php echo _('Adulteration');?></label>
                                                    <div class="col-md-8">
                                                        <?php 
                                                        if($data['distribution_pemalsuan'] == 0){
                                                            echo 'Negative';
                                                        }else{
                                                            echo 'Positive';
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4"></label>
                                                </div>
                                            </td>
                                            <td style="width:50%;vertical-align:top;">
                                                <textarea name="distribution_remark" id="distribution_remark" class="input-block" rows="12"><?php echo $data['distribution_remark'];?></textarea>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="table border full-border no-hover">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="text-center padding-bottom-40">
                                                    <?php echo _('laboratory officer');?>
                                                </div>
                                                <div class="text-center handwriting">
                                                    &nbsp;<?php echo $data['distribution_userlab'];?>&nbsp;
                                                </div>
                                            </td>
                                            <td>
                                                <div class="text-center padding-bottom-40">
                                                    <?php echo _('Production Officer');?>
                                                </div>
                                                <div class="text-center handwriting">
                                                    &nbsp;<?php echo $data['distribution_userinput'];?>&nbsp;
                                                </div>
                                            </td>
                                            <td>
                                                 <div class="text-center padding-bottom-40">
                                                    <?php echo _('Goods Carrier Officer');?>
                                                </div>
                                                <div class="text-center handwriting">
                                                    &nbsp;<?php echo $data['distribution_pembawabarang'];?>&nbsp;
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <?php if($data['distribution_status'] != 'n'){ ?>
                                <div class="text-center padding-top-25 padding-bottom-15">
                                    <h3>
                                        <?php 
                                        if($data['distribution_status'] == 'y'){
                                            echo _('The goods has been received very well, with the following details');
                                        }elseif($data['distribution_status'] == 'r'){
                                            echo _('The goods has been REJECTED, with the following details');
                                        }else{
                                            echo _('The goods is being shipped, with the following details');
                                        }
                                        ?>
                                    </h3>
                                </div>
                                <table class="table border full-border no-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center" style="width:50%"><?php echo _('Analysis');?></th>
                                            <th class="text-center" style="width:50%"><?php echo _('Description');?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="padding-10">
                                                            <?php echo _('Volume');?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="padding-10">
                                                            <?php echo (float) $data['distribution_volumediterima'];?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="padding-10">
                                                            <?php echo _('Density');?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="padding-10">
                                                            <?php echo (float) $data['distribution_beratjenisditerima'];?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="padding-10">
                                                            <?php echo _('FAT');?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="padding-10">
                                                            <?php echo (float) $data['distribution_fatditerima'];?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="padding-10">
                                                            <?php echo _('Protein');?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="padding-10">
                                                            <?php echo (float) $data['distribution_proteinditerima'];?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="padding-10">
                                                            <?php echo _('Lactose');?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="padding-10">
                                                            <?php echo (float) $data['distribution_lactosediterima'];?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="padding-10">
                                                            <?php echo _('Freezing Point');?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="padding-10">
                                                            <?php echo (float) $data['distribution_fpditerima'];?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="padding-10">
                                                            <?php echo _('Total Solid');?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="padding-10">
                                                            <?php echo (float) $data['distribution_tsditerima'];?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="padding-10">
                                                            <?php echo _('Acidity');?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="padding-10">
                                                            <?php echo (float) $data['distribution_acidityditerima'];?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="padding-10">
                                                            <?php echo _('Urea');?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="padding-10">
                                                            <?php echo (float) $data['distribution_ureaditerima'];?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="padding-10">
                                                            <?php echo _('SNF');?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="padding-10">
                                                            <?php echo (float) $data['distribution_snfditerima'];?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="padding-10">
                                                            <?php echo _('TPC');?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <div class="padding-10">
                                                            <?php echo (float) $data['distribution_tpcditerima'];?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                <div class="padding-10">
                                                    <?php echo $data['distribution_remarkditerima'];?>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <div class="text-center padding-10 handwriting" style="width:250px;min-height:50px;">
                                                    &nbsp;<?php echo $data['distribution_diterimaoleh'];?>&nbsp;
                                                </div>
                                                <div class="text-center padding-10 " style="width:250px;">
                                                    <?php echo _('Recipient');?>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <?php } ?>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        var top = $('.ribbon').offset().top - parseFloat($('.ribbon').css('marginTop').replace(/auto/, 100));
            $('.content').scroll(function (event) {
            // what the y position of the scroll is
            var y = $(this).scrollTop();
            console.log(y);
            var sidebarWIdth = $('.sidebar').width();
            // whether that's below the form
            if (y >= top) {
                // if so, ad the fixed class
                $('.ribbon').addClass('fixed');

                $('.ribbon').css({
                    left:sidebarWIdth
                });
            } else {
              // otherwise remove it
              $('.ribbon').removeClass('fixed');
            }
          });
    });
</script>
<?php View::inc('footer.php');?>
