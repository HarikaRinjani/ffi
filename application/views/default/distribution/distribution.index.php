<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('distribution/distribution.menu.php');?>
<div class="content padded">
    <div class="breadcrumb">
        <ul class="tab">
            <li class="">
                <a href="<?php echo BASE_URL;?>index.php?/distribution/addnew">new</a>
            </li>
            <li class="<?php if($_GET['status'] == 'n'){ echo 'active'; } ?>">
                <a href="<?php echo BASE_URL;?>index.php?/distribution/index/&amp;status=n">on delivery</a>
            </li>
            <li class="<?php if($_GET['status'] == 'y'){ echo 'active'; } ?>">
                <a href="<?php echo BASE_URL;?>index.php?/distribution/index/&amp;status=y">Completed</a>
            </li>
            <li class="<?php if($_GET['status'] == 'r'){ echo 'active'; } ?>">
                <a href="<?php echo BASE_URL;?>index.php?/distribution/index/&amp;status=r">Rejected</a>
            </li>
        </ul>
        <div class="pull-right">
            <ul class="breadcrumb-nav">
                <li>
                    <a href="">Distribution</a>
                </li>
                <li>
                    <a href="">Lists</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row ">
            <div class="ribbon padding-left-15 padding-right-15 padding-bottom-10 padding-top-10 border-bottom text-right">
                <div class="menu">
                    <a href="<?php echo BASE_URL;?>index.php?/distribution/addnew" class="padding-right-10"><i class="ion-ios7-copy-outline"></i> add</a>
                    <a href="javascript:void(0);" class="editbtn fg-lightgray padding-right-10" data-id="" id="__view"><i class="ion-ios7-folder-outline"></i>view</a>
                    
                    <a href="javascript:void(0);" class="<?php if($_GET['status'] != 'r'){ echo 'editbtn'; } ?> fg-lightgray padding-right-10" data-id="" id="__reprint"><i class="ion-ios7-printer-outline"></i>reprint</a>
                    <a href="javascript:void(0);" class="<?php if($_GET['status'] == 'n'){ echo 'editbtn'; } ?> fg-lightgray padding-right-10" data-id="" id="__setstatus"><i class="ion-ios7-navigate-outline"></i>GR-status</a>
                </div>
                <form action="javascript:void(0);" id="searchForm" onsubmit="return filter();">
                    <input type="text" placeholder="Search..." id="keyword" style="width:300px" value="<?php echo $distribution['keyword'];?>">
                </form>
            </div>
            <table class="table">
                <tr>
                    <th>No</th>
                    <th>Date</th>
                    <th>Truck</th>
                    <th>Consumer</th>
                    <th style="width:80px;">Weight</th>
                    <th>status</th>
                    <th></th>
                </tr>
                <?php if($distribution['total_data'] === 0){ ?>
                <tr>
                    <td colspan="100%"><?php echo _('No record found');?></td>
                </tr>
                <?php } ?>
                <?php foreach($distribution['records'] as $k=>$v){ ?>
                <tr class="trselector" data-id="<?php echo $v['distribution_id'];?>" id="tr<?php echo $v['distribution_id'];?>">
                    <td><?php echo $v['distribution_nomor'];?></td>
                    <td class="format-date"><?php echo $v['distribution_tanggal'];?></td>
                    <td><?php echo $v['truck_nopol'];?></td>
                    <td><?php echo $v['konsumen_nama'];?></td>
                    <td class="text-right"><?php echo $v['distribution_berat'];?> Kg</td>
                    <td class="distribution-status"><?php echo $v['distribution_status'];?></td>
                    <td></td>
                </tr>
                <?php } ?>
            </table>
            <div class="grid-pagging border-top margin-bottom-30"></div>
        </div>
    </div>
</div>
<div class="modal" id="modal-reprint">
    <div class="modal-dialog modal-sm">
        <div class="modal-header">
            <h3 class="modal-title">Reprint</h3>
            <a href="javascript:void(0);" class="close">&times;</a>
        </div>
        <div class="modal-body">
            <label for="remark__">please enter a remark</label>
            <input type="text" id="remark__" class="input-block input-lg">
            <div class="text-center margin-top-10">
                <button id="reprint-btn" data-id="">reprint</button>
            </div>
        </div>
    </div>
</div>
<div class="modal draggable" id="modal-setstatus">
    <div class="modal-dialog modal-lg">
        <div class="modal-header">
            <h3 class="modal-title">Good Receive Status</h3>
            <a href="javascript:void(0);" class="close">&times;</a>
        </div>
        <div class="modal-body">
            <form action="<?php echo BASE_URL;?>index.php?/distribution/setstatus" method="post">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label for="edit-distribution_status" class="col-md-4">Status</label>
                            <div class="col-md-8">
                                <select name="distribution_status" id="edit-distribution_status">
                                    <option value="y">Completed</option>
                                    <option value="r">Rejected</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="edit-distribution_volumediterima" class="col-md-4">Volume</label>
                            <div class="col-md-8">
                                <input type="text" name="distribution_volumediterima" id="edit-distribution_volumediterima" class="input-block numeric">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="edit-distribution_beratjenisditerima" class="col-md-4">Density</label>
                            <div class="col-md-8">
                                <input type="text" name="distribution_beratjenisditerima" id="edit-distribution_beratjenisditerima" class="input-block numeric">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="edit-distribution_remarkditerima" class="col-md-4">Remark</label>
                            <div class="col-md-8">
                                <textarea name="distribution_remarkditerima" id="edit-distribution_remarkditerima" rows="3" class="input-block"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="edit-distribution_grn" class="col-md-4">Good Receive Number (Optional)</label>
                            <div class="col-md-8">
                                <input type="text" name="distribution_grn" id="edit-distribution_grn" class="input-block">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="edit-distribution_diterimaoleh" class="col-md-4">Received by:</label>
                            <div class="col-md-8">
                                <input type="text" name="distribution_diterimaoleh" id="edit-distribution_diterimaoleh" class="input-block">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="edit-distribution_diterimatanggal" class="col-md-4">Received date:</label>
                            <div class="col-md-8">
                                <input type="text" name="distribution_diterimatanggal" id="edit-distribution_diterimatanggal" class="input-block datepicker" required >
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 border-left">
                        <div class="form-group">
                            <label for="edit-distribution_fatditerima" class="col-md-4">FAT</label>
                            <div class="col-md-8">
                                <input type="text" name="distribution_fatditerima" id="edit-distribution_fatditerima" class="input-block numeric">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="edit-distribution_proteinditerima" class="col-md-4">Protein</label>
                            <div class="col-md-8">
                                <input type="text" name="distribution_proteinditerima" id="edit-distribution_proteinditerima" class="input-block numeric">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="edit-distribution_lactosediterima" class="col-md-4">Lactose</label>
                            <div class="col-md-8">
                                <input type="text" name="distribution_lactosediterima" id="edit-distribution_lactosediterima" class="input-block numeric">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="edit-distribution_fpditerima" class="col-md-4">Freezing Point</label>
                            <div class="col-md-8">
                                <input type="text" name="distribution_fpditerima" id="edit-distribution_fpditerima" class="input-block numeric">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="edit-distribution_tsditerima" class="col-md-4">Total Solid</label>
                            <div class="col-md-8">
                                <input type="text" name="distribution_tsditerima" id="edit-distribution_tsditerima" class="input-block numeric">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="edit-distribution_acidityditerima" class="col-md-4">Acidity</label>
                            <div class="col-md-8">
                                <input type="text" name="distribution_acidityditerima" id="edit-distribution_acidityditerima" class="input-block numeric">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="edit-distribution_ureaditerima" class="col-md-4">Urea</label>
                            <div class="col-md-8">
                                <input type="text" name="distribution_ureaditerima" id="edit-distribution_ureaditerima" class="input-block numeric">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="edit-distribution_snfditerima" class="col-md-4">SNF</label>
                            <div class="col-md-8">
                                <input type="text" name="distribution_snfditerima" id="edit-distribution_snfditerima" class="input-block numeric">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4">TPC</label>
                            <div class="col-md-8">
                                <input class="input-block" name="distribution_tpcditerima" class="numeric" >
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" name="distribution_id" id="edit-distribution_id">
                <div class="text-right padding-top-10">
                    <button class="default">save change</button>
                </div>
            </form>
        </div>
    </div>
</div>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.grid-pagging').transpaginator({
                totalPages: <?php echo $distribution['total_page'];?>,
                totalData: <?php echo $distribution['total_data'];?>,
                currentPage: <?php echo $distribution['page'];?>,
                pageUrl: '<?php echo BASE_URL;?>index.php?/distribution/index/&status=<?php echo $_GET['status'];?>&keyword=<?php echo $keyword;?>&page='
            });
            $('.trselector').on('click',function(){
                var id = $(this).attr('data-id');
                $('.trselector').removeClass('active');
                $('.editbtn').attr('data-id',id).removeClass('fg-lightgray');
                $(this).addClass('active');
            });
            $('#__edit').on('click',function(){
                var id = $(this).attr('data-id');
                if(typeof id !== 'undefined' && id !== ''){
                    openUrl('distribution/edit/'+id);
                }
            });
            $('#__setstatus').on('click',function(){
                var id = $(this).attr('data-id');
                if(typeof id !== 'undefined' && id !== ''){
                    $.ajax({
                        url:'<?php echo BASE_URL;?>index.php?/distribution/edit/'+id+'/&json=true',
                        dataType:'json',
                        success:function(res){
                            if(typeof res.distribution_id !== 'undefined'){
                                // $('#edit-distribution_id').val(res.distribution_id);
                                // $('#edit-distribution_volumediterima').val(res.distribution_volumediterima);
                                // $('#edit-distribution_beratjenisditerima').val(res.distribution_beratjenisditerima);
                                // $('#edit-distribution_remarkditerima').val(res.distribution_remarkditerima);
                                // $('#edit-distribution_status').val(res.distribution_status);
                                for(var k in res){
                                    if(k === 'distribution_diterimaoleh' && (res.distribution_diterimaoleh === '' || res.distribution_diterimaoleh === null)){
                                        res.distribution_diterimaoleh = '<?php echo session::get('user_fullname');?>';
                                    }
                                    $('#edit-'+k).val(res[k]);
                                }
                                $('#modal-setstatus').addClass('fadein');
                            }
                        }
                    });
                }
            });
            $('#__view').on('click',function(){
                var id = $(this).attr('data-id');
                if(typeof id !== 'undefined' && id !== ''){
                    openUrl('distribution/detail/'+id);
                }
            });
            $('#__reprint').on('click',function(){
                var id = $(this).attr('data-id');
                if(typeof id !== 'undefined' && id !== ''){
                    $('#reprint-btn').attr('data-id',id);
                    $('#modal-reprint').addClass('fadein');
                }
            });
            $('#reprint-btn').on('click',function(){
                var id = $(this).attr('data-id');
                var remark = $('#remark__').val();
                if(typeof id !== 'undefined' && id !== ''){
                    if(remark === ''){
                        notification('please enter a remark!');
                        $('#remark__').focus();
                    }else{
                        myWindow = window.open('<?php echo BASE_URL;?>index.php?/distribution/reprint/'+id+'/&printlog_remark='+remark,'Delivery Order','width=1024,height=768');
                        //myWindow.document.close();
                        myWindow.focus();
                        myWindow.print();
                        $('#reprint-btn').attr('data-id','');
                        $('#modal-reprint').removeClass('fadein');
                        $('.editbtn').attr('data-id','').addClass('fg-lightgray');
                        $('.trselector').removeClass('active');
                    }
                }
            });
            <?php if(isset($_GET['print'])){ ?>
                $(window).load(function(){
                    myWindow = window.open('<?php echo BASE_URL;?>index.php?/distribution/print_/<?php echo $print_distribution_id;?>','Delivery Order','width=1024,height=768');
                    //myWindow.document.close();
                    myWindow.focus();
                    myWindow.print();
                });
            <?php } ?>
        });
        function goto(distribution_id){
            //window.location.href = '<?php echo BASE_URL;?>index.php?/distribution/read/'+mail_id;

        }
        function filter(){
            var keyword = $('#keyword').val();
            window.location.href = '<?php echo BASE_URL;?>index.php?/distribution/index/&status=<?php echo $_GET['status'];?>&keyword='+keyword+'&page=1';
        }
    </script>
<?php View::inc('footer.php');?>
