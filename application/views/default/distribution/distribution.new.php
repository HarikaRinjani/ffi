<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('distribution/distribution.menu.php');?>
<link rel="stylesheet" href="<?php echo BASE_URL;?>themes/default/jquery-tokeninput/styles/token-input-facebook.css"/>

<div class="content padded bg-lightgray">
    <div class="breadcrumb">
        <ul class="tab">
            <li class="active">
                <a href="<?php echo BASE_URL;?>index.php?/distribution/addnew" disabled="disabled">new</a>
            </li>
            <li class="">
               <a href="<?php echo BASE_URL;?>index.php?/distribution/index/&amp;status=n">On Delivery</a>
            </li>
            <li>
               <a href="<?php echo BASE_URL;?>index.php?/distribution/index/&amp;status=y">Completed</a>
            </li>
            <li>
               <a href="<?php echo BASE_URL;?>index.php?/distribution/index/&amp;status=r">Rejected</a>
            </li>
        </ul>
        <div class="pull-right">
            <ul class="breadcrumb-nav">
                <li>
                    <a href="">Distribution</a>
                </li>
                <li>
                    <a href="">Add New</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="ribbon padding-left-10 padding-bottom-5 border-bottom">
                <div class="menu">
                    <a href="<?php echo BASE_URL;?>index.php?/distribution/index"><i class="ion-ios7-arrow-left"></i>back</a>
                    <a href="javascript:void(0);" onclick="$('#form-distribution-submit-btn').trigger('click');"><i class="ion-ios7-save"></i>save</a>
                </div>
            </div>
            <div class="col-md-10 col-md-offset-1 padding-top-20">
                <div class="window padding-40" id="#topwindows">
                    <div class="window-body">
                        <form action="<?php echo BASE_URL;?>index.php?/distribution/save" method="post">
                            <?php if(session::get('user_koperasi') == 'none'){ ?>
                            <div class="col-xs-12 margin-bottom-20">
                                <div style="border-bottom:4px solid #888;">
                                    <div class="text-center padding-bottom-20">
                                        <h3 class="no-margin no-padding">Milk Collection Software</h3>
                                        <h1>Delivery Oder Form</h1>
                                    </div>
                                </div>
                            </div>
                            <?php }else{ ?>
                            <div class="padding-bottom-20" style="border-bottom:4px solid #888;margin-bottom:10px;">
                                <div class="col-xs-2">
                                    <img src="<?php echo session::get('user_koperasi_logo');?>" alt="" class="img-responsive">
                                </div>
                                <div class="col-xs-10">
                                    <div style="">
                                        <div class="text-center">
                                            <h1 class="no-margin no-padding"><?php echo session::get('user_koperasi_nama');?></h1>
                                            <h4><?php echo session::get('user_koperasi_alamat');?></h4>
                                            <div><?php echo session::get('user_koperasi_telp');?></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <?php } ?>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label for="" class="col-md-4"><?php echo _('DO Number');?></label>
                                    <div class="col-md-8">
                                        <input type="text" class="input-block" name="distribution_nomor" required />
                                    </div>
                                </div>
                                <?php if(session::get('user_koperasi') == 'none'){ ?>
                                <div class="form-group">
                                    <label for="" class="col-md-4"><?php echo _('Cooperative');?></label>
                                    <div class="col-md-8">
                                        <select name="distribution_koperasi" id="distribution_koperasi" class="input-block koperasi" required></select>
                                    </div>
                                </div>
                                <?php }else{ ?>
                                <input type="hidden" name="distribution_koperasi" value="<?php echo session::get('user_koperasi');?>">
                                <?php } ?>
                                <div class="form-group">
                                    <label for="" class="col-md-4"><?php echo _('Customer');?></label>
                                    <div class="col-md-8">
                                        <input type="text" name="distribution_konsumen" class="input-block" id="distribution_konsumen" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                <label for="" class="col-md-4"><?php echo _('Date');?></label>
                                    <div class="col-md-8">
                                        <input type="text" class="datepicker input-block" name='distribution_tanggal' required>
                                    </div>
                                </div>
                                <div class="form-group">
                                <label for="" class="col-md-4"><?php echo _('Truck Number');?></label>
                                    <div class="col-md-8">
                                        <input type="text" id="distribution_truck" class="input-block" name="distribution_truck" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-md-4"><?php echo _('Leave');?></label>
                                    <div class="col-md-8">
                                        <input type="text" class="input-block input-time" value="<?php echo date('H:i');?>" name="distribution_berangkatjam" required>
                                    </div> 
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-md-4"><?php echo _('Check In');?> I.P.S</label>
                                    <div class="col-md-8">
                                        <input type="text" class="input-block input-time" value="<?php echo date('H:i');?>" name="distribution_masukips" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-md-4"><?php echo _('Check Out');?> I.P.S</label>
                                    <div class="col-md-8">
                                        <input type="text" class="input-block input-time" value="<?php echo date('H:i');?>" name="distribution_keluarips" required>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="padding-top-15">
                                <p class="text-center" style="font-size:32px;"><?php echo _('Delivery Order Form');?></p>
                                <div class="padding-bottom-15 text-center">
                                    <h3><?php echo _('Along with this, we delivered fresh milk as follows:');?></h3>
                                </div>
                                <table class="table border full-border no-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><?php echo _('Analysis');?></th>
                                            <th class="text-center"><?php echo _('Description');?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="width:50%">
                                                <div class="form-group">
                                                    <label for="" class="col-md-4"><?php echo _('Weight');?></label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="input-block numeric append" name="distribution_berat">
                                                        <span class="append-text">Kg</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4"><?php echo _('Density');?></label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="input-block numeric" name="distribution_beratjenis">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4">Temperature</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="input-block append numeric" name="distribution_temperature">
                                                        <span class="append-text"><sup>o</sup>C</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4"><?php echo _('Alcohol Test');?></label>
                                                    <div class="col-md-8">
                                                        <input type="radio" name="distribution_alkohol" id="distribution_alkohol_0" value="0" checked="checked" />
                                                        <label for="distribution_alkohol_0"><?php echo _('Negative');?></label>
                                                        <input type="radio" name="distribution_alkohol" id="distribution_alkohol_1" value="1" />
                                                        <label for="distribution_alkohol_1"><?php echo _('Positive');?></label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4">FAT</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="input-block decimal numeric" name="distribution_fat">
                                                        <span class="append-text">%</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4">Protein</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="input-block numeric" name="distribution_protein">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4">Lactose</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="input-block numeric" name="distribution_lactose">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4">Freezing Point</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="input-block numeric" name="distribution_fp">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4">Total Solid</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="input-block numeric" name="distribution_ts">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4">Acidity</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="input-block numeric" name="distribution_acidity">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4">Urea</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="input-block numeric" name="distribution_urea">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4">SNF</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="input-block numeric" name="distribution_snf">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4">TPC</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="input-block numeric" name="distribution_tpc">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4"><?php echo _('Adulteration');?></label>
                                                    <div class="col-md-8">
                                                        <input type="radio" name="distribution_pemalsuan" id="distribution_pemalsuan_0" value="0" checked="checked" />
                                                        <label for="distribution_pemalsuan_0"><?php echo _('Negative');?></label>
                                                        <input type="radio" name="distribution_pemalsuan" id="distribution_pemalsuan_1" value="1" />
                                                        <label for="distribution_pemalsuan_1"><?php echo _('Positive');?></label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td style="width:50%;vertical-align:top;">
                                                <textarea name="distribution_remark" id="distribution_remark" class="input-block" rows="12"></textarea>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="table border full-border no-hover">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="text-center padding-bottom-40">
                                                    <?php echo _('laboratory officer');?>
                                                </div>
                                                <input type="text" class="input-block handwriting text-center" name="distribution_userlab">
                                            </td>
                                            <td>
                                                <div class="text-center padding-bottom-40">
                                                    <?php echo _('Production Officer');?>
                                                </div>
                                                <input type="text" class="input-block handwriting text-center" name="distribution_userinput" value="<?php echo session::get('user_fullname');?>">
                                            </td>
                                            <td>
                                                <div class="text-center padding-bottom-40">
                                                    <?php echo _('Goods Carrier Officer');?>
                                                </div>
                                                <input type="text" class="input-block handwriting text-center" name="distribution_pembawabarang">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <button id="form-distribution-submit-btn" type="submit" style="visibility:hidden;width:1px;height:1px;">save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo BASE_URL;?>themes/default/jquery-tokeninput/src/jquery.tokeninput.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#distribution_konsumen').tokenInput('<?php echo BASE_URL;?>index.php?/load/konsumen/',{
            'placeholder': ' ',
            method:'POST',
            tokenLimit:1,
            theme: "facebook"
        });
        $('#distribution_truck').tokenInput('<?php echo BASE_URL;?>index.php?/load/truck/',{
            'placeholder': ' ',
            method:'POST',
            tokenLimit:1,
            theme: "facebook"
        });
    });
</script>
<?php View::inc('footer.php');?>
