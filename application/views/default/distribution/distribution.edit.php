<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('distribution/distribution.menu.php');?>

<div class="content padded bg-lightgray">
    <div class="breadcrumb">
        <ul class="tab">
            <li class="active">
                <a href="<?php echo BASE_URL;?>index.php?/distribution/addnew" disabled="disabled">edit</a>
            </li>
            <li>
                  <a href="<?php echo BASE_URL;?>index.php?/distribution/index/&amp;status=n">On Delivery</a>
            </li>
            <li>
                <a href="<?php echo BASE_URL;?>index.php?/distribution/index/&amp;status=y">Completed</a>
            </li>
            <li>
                <a href="<?php echo BASE_URL;?>index.php?/distribution/index/&amp;status=r">Rejected</a>
            </li>
        </ul>
        <div class="pull-right">
            <ul class="breadcrumb-nav">
                <li>
                    <a href="">Distribution</a>
                </li>
                <li>
                    <a href="">Edit Delivery Order</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="ribbon border-bottom">
                <div class="menu">
                    <a class="" href="<?php echo BASE_URL;?>index.php?/distribution/index"><i class="ion-ios7-arrow-left"></i>back</a>
                    <a class="" href="javascript:void(0);" onclick="$('#form-distribution-edit-btn').trigger('click');"><i class="ion-ios7-save"></i>save</a>
                </div>
            </div>
            <div class="col-md-10 col-md-offset-1 padding-top-20">
                <div class="window padding-40">
                    <div class="window-body">
                        <form action="<?php echo BASE_URL;?>index.php?/distribution/save" method="post" id="form-distribution-edit">
                            <input type="hidden" name="distribution_id" value="<?php echo $data['distribution_id'];?>">
                            <?php if(session::get('user_koperasi') == 'none'){ ?>
                                <div class="col-xs-12 margin-bottom-20">
                                    <div style="border-bottom:4px solid #888;">
                                        <div class="text-center padding-bottom-20">
                                            <h3 class="no-margin no-padding">Milk Collection Software</h3>
                                            <h1>Delivery Oder Form</h1>
                                        </div>
                                    </div>
                                </div>
                            <?php }else{ ?>
                                <div class="padding-bottom-20" style="border-bottom:4px solid #888;margin-bottom:10px;">
                                    <div class="col-xs-2">
                                        <img src="<?php echo session::get('user_koperasi_logo');?>" alt="" class="img-responsive">
                                    </div>
                                    <div class="col-xs-10">
                                        <div style="">
                                            <div class="text-center">
                                                <h1 class="no-margin no-padding"><?php echo session::get('user_koperasi_nama');?></h1>
                                                <h4><?php echo session::get('user_koperasi_alamat');?></h4>
                                                <div><?php echo session::get('user_koperasi_telp');?></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            <?php } ?>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label for="" class="col-md-4"><?php echo _('DO Number');?></label>
                                    <div class="col-md-8">
                                        <input type="text" value="<?php echo $data['distribution_nomor'];?>" class="input-block" name="distribution_nomor" required />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-md-4"><?php echo _('Customer');?></label>
                                    <div class="col-md-8">
                                        <select class="input-block combobox" data-value="<?php echo $data['distribution_konsumen'];?>" data-model="konsumen" name="distribution_konsumen"></select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="form-group">
                                    <label for="" class="col-md-4"><?php echo _('Date');?></label>
                                    <div class="col-md-8">
                                        <input type="text" class="datepicker input-block" value="<?php echo date('d/m/Y',strtotime($data['distribution_tanggal']));?>" name='distribution_tanggal'>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-md-4"><?php echo _('Truck Number');?></label>
                                    <div class="col-md-8">
                                        <select class="input-block combobox" data-value="<?php echo $data['distribution_truck'];?>" data-key="truck_nopol" data-model="truck" name="distribution_truck"></select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-md-4"><?php echo _('Leave');?></label>
                                    <div class="col-md-8">
                                        <input type="text" class="input-block input-time" value="<?php echo date('H:i',strtotime($data['distribution_berangkatjam']));?>" name="distribution_berangkatjam">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-md-4"><?php echo _('Check in');?> I.P.S</label>
                                    <div class="col-md-8">
                                        <input type="text" class="input-block input-time" value="<?php echo date('H:i',strtotime($data['distribution_masukips']));?>" name="distribution_masukips">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="" class="col-md-4"><?php echo _('Check out');?> I.P.S</label>
                                    <div class="col-md-8">
                                        <input type="text" class="input-block input-time" value="<?php echo date('H:i',strtotime($data['distribution_keluarips']));?>" name="distribution_keluarips">
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="padding-top-15">
                                <p class="text-center" style="font-size:32px;"><?php echo _('Delivery Order Form');?></p>
                                <div class="padding-bottom-15 text-center">
                                    <h3><?php echo _('Along with this, we delivered fresh milk as follows:');?></h3>
                                </div>
                                <table class="table border full-border no-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center"><?php echo _('Analysis');?></th>
                                            <th class="text-center"><?php echo _('Description');?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="width:50%">
                                                <div class="form-group">
                                                    <label for="" class="col-md-4"><?php echo _('Weight');?></label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="input-block decimal append" value="<?php echo $data['distribution_berat'];?>" name="distribution_berat">
                                                        <span class="append-text">Kg</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4"><?php echo _('Density');?></label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="input-block decimal" value="<?php echo $data['distribution_beratjenis'];?>" name="distribution_beratjenis">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4"><?php echo _('Temperature');?></label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="input-block append decimal" value="<?php echo $data['distribution_temperature'];?>" name="distribution_temperature">
                                                        <span class="append-text"><sup>o</sup>C</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4"><?php echo _('Alcohol Test');?></label>
                                                    <div class="col-md-8">
                                                        <input type="radio" name="distribution_alkohol" id="distribution_alkohol_0" value="0" <?php if($data['distribution_alkohol'] == 0){ echo 'checked="checked"'; } ?>/>
                                                        <label for="distribution_alkohol_0"><?php echo _('Negative');?></label>
                                                        <input type="radio" name="distribution_alkohol" id="distribution_alkohol_1" value="1" <?php if($data['distribution_alkohol'] == 1){ echo 'checked="checked"'; } ?>/>
                                                        <label for="distribution_alkohol_1"><?php echo _('Positive');?></label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4">FAT</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="input-block decimal append" value="<?php echo $data['distribution_fat'];?>" name="distribution_fat">
                                                        <span class="append-text">%</span>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4">Protein</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="input-block numeric" value="<?php echo $data['distribution_protein'];?>" name="distribution_protein">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4">Lactose</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="input-block numeric" value="<?php echo $data['distribution_lactose'];?>" name="distribution_lactose">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4">Freezing Point</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="input-block numeric" value="<?php echo $data['distribution_fp'];?>" name="distribution_fp">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4">Total Solid</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="input-block numeric" value="<?php echo $data['distribution_ts'];?>" name="distribution_ts">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4">Acidity</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="input-block numeric" value="<?php echo $data['distribution_acidity'];?>" name="distribution_acidity">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4">Urea</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="input-block numeric" value="<?php echo $data['distribution_urea'];?>" name="distribution_urea">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4">SNF</label>
                                                    <div class="col-md-8">
                                                        <input type="text" class="input-block numeric" value="<?php echo $data['distribution_snf'];?>" name="distribution_snf">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="" class="col-md-4"><?php echo _('Adulteration');?></label>
                                                    <div class="col-md-8">
                                                        <input type="radio" name="distribution_pemalsuan" id="distribution_pemalsuan_0" value="0" <?php if($data['distribution_pemalsuan'] == 0){ echo 'checked="checked"'; } ?>/>
                                                        <label for="distribution_pemalsuan_0"><?php echo _('Negative');?></label>
                                                        <input type="radio" name="distribution_pemalsuan" id="distribution_pemalsuan_1" value="1" <?php if($data['distribution_pemalsuan'] == 1){ echo 'checked="checked"'; } ?>/>
                                                        <label for="distribution_pemalsuan_1"><?php echo _('Positive');?></label>
                                                    </div>
                                                </div>
                                            </td>
                                            <td style="width:50%;vertical-align:top;">
                                                <textarea name="distribution_remark" id="distribution_remark" class="input-block" rows="12"><?php echo $data['distribution_remark'];?></textarea>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="table border full-border no-hover">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <div class="text-center padding-bottom-40">
                                                    <?php echo _('laboratory officer');?>
                                                </div>
                                                <input type="text" class="input-block handwriting text-center" value="<?php echo $data['distribution_userlab'];?>" name="distribution_userlab">
                                            </td>
                                            <td>
                                                <div class="text-center padding-bottom-40">
                                                    <?php echo _('Production Officer');?>
                                                </div>
                                                <input type="text" class="input-block handwriting text-center" value="<?php echo $data['distribution_userinput'];?>" name="distribution_userinput">
                                            </td>
                                            <td>
                                                <div class="text-center padding-bottom-40">
                                                    <?php echo _('Goods Carrier Officer');?>
                                                </div>
                                                <input type="text" class="input-block handwriting text-center" value="<?php echo $data['distribution_pembawabarang'];?>" name="distribution_pembawabarang">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <button id="form-distribution-edit-btn" type="submit" style="visibility:hidden;width:1px;height:1px;"></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        var top = $('.ribbon').offset().top - parseFloat($('.ribbon').css('marginTop').replace(/auto/, 100));
          $('.content').scroll(function (event) {
            // what the y position of the scroll is
            var y = $(this).scrollTop();
            console.log(y);
            var sidebarWIdth = $('.sidebar').width();
            // whether that's below the form
            if (y >= top) {
              // if so, ad the fixed class
              $('.ribbon').addClass('fixed');

              $('.ribbon').css({
                left:sidebarWIdth
              });
            } else {
              // otherwise remove it
              $('.ribbon').removeClass('fixed');
            }
          });
    });
</script>
<?php View::inc('footer.php');?>
