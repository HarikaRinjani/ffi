<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('distribution/distribution.menu.php');?>

<div class="content padded">
    <div class="breadcrumb">
        <ul class="tab">
            <li class="active">
                <a href="">REPRINT LOGS</a>
            </li>
        </ul>
        <div class="pull-right">
            <ul class="breadcrumb-nav">
                <li>
                    <a href="">Distribution</a>
                </li>
                <li>
                    <a href="">RePrint Log</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row ">
            <div class="ribbon padding-left-15 padding-right-15 padding-bottom-10 padding-top-10 border-bottom text-right">
                <div class="menu">
                    <a href="javascript:void(0);"><i class="ion-ios7-printer-outline"></i>reprint log</a>
                </div>
                <form action="javascript:void(0);" id="searchForm" onsubmit="return filter();">
                    <input type="text" placeholder="Search..." id="keyword" style="width:300px" value="<?php echo $keyword;?>">
                </form>
            </div>
            <table class="table">
                <tr>
                    <th>Date</th>
                    <th>DO Number</th>
                    <th>Printed By</th>
                    <th>Remark</th>
                    <th></th>
                </tr>
                <?php if($distribution['total_data'] === 0){ ?>
                <tr>
                    <td colspan="100%"><?php echo _('No record found');?></td>
                </tr>
                <?php } ?>
                <?php foreach($distribution['records'] as $k=>$v){ 
                    if(empty($v['user_avatar'])){
                        $avatar = BASE_URL.'themes/default/images/avatar.jpg';
                    }else{
                        $avatar = Config::$application['file_url'].rawurlencode($v['user_avatar']);
                    }
                    ?>
                <tr class="trselector" data-id="<?php echo $v['printlog_id'];?>" id="tr<?php echo $v['printlog_id'];?>">
                    <td class="format-datetime"><?php echo $v['printlog_entrydate'];?></td>
                    <td><?php echo $v['distribution_nomor'];?></td>
                    <td style="width:300px;"><img src="<?php echo $avatar;?>" style="float:left;margin-right:5px;width:32px;height:32px;"><?php echo $v['user_fullname'];?></td>
                    <td><?php echo $v['printlog_remark'];?></td>
                    <td></td>
                </tr>
                <?php } ?>
            </table>
            <div class="grid-pagging border-top margin-bottom-30"></div>
        </div>
    </div>
</div>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.grid-pagging').transpaginator({
                totalPages: <?php echo $distribution['total_page'];?>,
                totalData: <?php echo $distribution['total_data'];?>,
                currentPage: <?php echo $distribution['page'];?>,
                pageUrl: '<?php echo BASE_URL;?>index.php?/distribution/printlog/&keyword=<?php echo $keyword;?>&page='
            });
            
        });
        function filter(){
            var keyword = $('#keyword').val();
            window.location.href = '<?php echo BASE_URL;?>index.php?/distribution/printlog/&keyword='+keyword+'&page=1';
        }
    </script>
<?php View::inc('footer.php');?>