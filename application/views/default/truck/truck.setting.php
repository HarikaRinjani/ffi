<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('settings/settings.menu.php');?>

<div class="content padded">
    <div class="breadcrumb">
        <ul class="tab">
            <li >
                <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-add-department">add new</a>
            </li>
            <li class="active">
                <a href="<?php echo BASE_URL;?>index.php?/truck/setting" disabled="disabled">Truck</a>
            </li>
        </ul>
        <div class="pull-right">
            <ul class="breadcrumb-nav">
                <li>
                    <a href="">Apps Settings</a>
                </li>
                <li>
                    <a href="">Truck</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row padding-bottom-40">
            <div class="ribbon text-right">
                <div class="menu">
                    <a href="javascript:void(0);" id="__edit" class="fg-lightgray editbtn"><i class="ion-ios7-compose-outline"></i>edit</a>
                    <a href="javascript:void(0);" id="__delete" class="fg-lightgray editbtn"><i class="ion-ios7-trash-outline"></i>delete</a>
                </div>
                <form action="javascript:void(0);" id="searchForm" onsubmit="return filter();">
                    <?php if(!in_array(session::get('user_group'),Config::$application['super_group'])){ ?>
                    <input type="hidden" name="koperasi" id="koperasi" value="<?php echo session::get('user_koperasi');?>">
                    <?php }else{ ?>
                    <select name="koperasi" id="koperasi" onchange="return filter();">
                        <option value="">All</option>
                        <?php foreach($koperasi['records'] as $k=>$v){ ?>
                            <option value="<?php echo $v['koperasi_id'];?>" <?php if($v['koperasi_id'] === $_GET['koperasi']){ echo 'selected="selected"'; } ?>><?php echo $v['koperasi_nama'];?></option>
                        <?php } ?>
                    </select>
                    <?php } ?>
                    <input type="text" placeholder="Search..." id="keyword" style="width:200px" value="<?php echo $truck['keyword'];?>">
                </form>
            </div>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th style="width:24px;">#</th>
                        <!-- <th><?php //echo _('Truck Name');?></th> -->
                        <th><?php echo _('Cooperative');?></th>
                        <th><?php echo _('Truck Number');?></th>
                        <!-- <th><?php // echo _('Kapasitas');?></th> -->
                        <th><?php echo _('Status');?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if($truck['total_data'] == 0){ ?>
                        <tr>
                            <td colspan="6"><?php echo _('No record found');?></td>
                        </tr>
                        <?php }else{ ?>
                            <?php foreach($truck['records'] as $k=>$v){ ?>
                                <tr class="tr_selector cursor-pointer" data-id="<?php echo $v['truck_id'];?>" id="tr<?php echo $v['truck_id'];?>">
                                    <td>
                                        <?php
                                        if($truck['page'] == 1){
                                            echo ($k+1);
                                        }else{
                                            echo ($k + 1 + ($truck['limit'] * ($truck['page'] - 1)));
                                        }
                                        ?>
                                    </td>
                                    <!-- <td><?php //echo $v['truck_nama'];?></td> -->
                                    <td><?php echo $v['koperasi_nama'];?></td>
                                    <td><?php echo $v['truck_nopol'];?></td>
                                    <!-- <td><?php //echo $v['truck_kapasitas'];?></td> -->
                                    <td><span class="status"><?php echo $v['truck_status'];?></span></td>
                                </tr>
                                <?php } ?>
                                <?php } ?>
                            </tbody>
                        </table>
                        <div class="grid-pagging border-top margin-bottom-30"></div>
                    </div>
                </div>
            </div>
            <div class="modal draggable" id="modal-add-department">
                <div class="modal-dialog modal-sm">
                    <div class="modal-header">
                        <h3 class="modal-title">new truck</h3>
                        <a href="javascript:void(0);" class="close">&times;</a>
                    </div>
                    <div class="modal-body">
                        <form action="<?php echo BASE_URL;?>index.php?/truck/setting/save" method="post">
                            <label for="add-department_name" class=""><?php echo _('Truck Name');?></label>
                            <!-- <div class="">
                                <input type="text" id="add-department_name" class="input-block" name="truck_nama" required/>
                            </div> -->
<!--                            <label for="add-truck_model" class="padding-top-5">Model</label>-->
<!--                            <div class="">-->
<!--                                <input type="text" id="add-truck_model" class="input-block" name="truck_model" required/>-->
<!--                            </div>-->
                            <label for="add-truck_koperasi" class="padding-top-5"><?php echo _('Cooperative');?></label>
                            <div>
                                <select name="truck_koperasi" id="add-truck_koperasi" class="input-block koperasi"></select>
                            </div>
                            <label class="margin-top-5" for=""><?php echo _('Truck Number');?></label>
                            <div class="">
                                <input type="text" id="add-truck_nopol" class="input-block" name="truck_nopol" required/>
                            </div>
                            <!-- <label class="margin-top-5" for="">Kapasitas</label>
                            <div class="">
                                <input type="text" id="add-truck_kapasitas" onkeydown="return validateNumber(event);" class="input-block" name="truck_kapasitas" required/>
                            </div> -->
                            <label class="margin-top-5" for="add-department-status">Status</label>
                            <div class="">
                                <select name="truck_status" data-value="y" id="add-truck-status" class="input-block status"></select>
                            </div>
                            <div class="padding-top-25 text-right">
                                <button type="button" data-dismiss="modal">close</button>
                                <button type="submit" class="default">save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal draggable" id="modal-edit-department">
                <div class="modal-dialog modal-sm">
                    <div class="modal-header">
                        <h3 class="modal-title">Edit truck</h3>
                        <a href="javascript:void(0);" class="close">&times;</a>
                    </div>
                    <div class="modal-body">
                        <form action="<?php echo BASE_URL;?>index.php?/truck/setting/save" method="post">
                            <input type="hidden" name="truck_id" id="edit-truck_id"/>
                            <label for="edit-department_name" class=""><?php echo _('Truck Name');?></label>
                            <!-- <div class="">
                                <input type="text" id="edit-truck_nama" class="input-block" name="truck_nama" required/>
                            </div> -->
<!--                            <label for="edit-truck_model" class="padding-top-5">Model</label>-->
<!--                            <div class="">-->
<!--                                <input type="text" id="edit-truck_model" class="input-block" name="truck_model" required/>-->
<!--                            </div>-->
                            <label for="edit-truck_koperasi" class="padding-top-5"><?php echo _('Cooperative');?></label>
                            <div>
                                <select name="truck_koperasi" id="edit-truck_koperasi" class="input-block koperasi"></select>
                            </div>
                            <label class="margin-top-5" for=""><?php echo _('Truck Number');?></label>
                            <div class="">
                                <input type="text" id="edit-truck_nopol" class="input-block" name="truck_nopol" required/>
                            </div>
                            <!-- <label class="margin-top-5" for="">Kapasitas</label>
                            <div class="">
                                <input type="text" id="edit-truck_kapasitas" onkeydown="return validateNumber(event);" class="input-block" name="truck_kapasitas" required/>
                            </div> -->
                            <label class="margin-top-5" for="edit-department-status">Status</label>
                            <div class="">
                                <select name="truck_status" data-value="y" id="edit-truck_status" class="input-block status"></select>
                            </div>
                            <div class="padding-top-25 text-right">
                                <button type="button" data-dismiss="modal">close</button>
                                <button type="submit" class="default">save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                $(document).ready(function(){
                    $('.grid-pagging').transpaginator({
                        totalPages: <?php echo $truck['total_page'];?>,
                        totalData: <?php echo $truck['total_data'];?>,
                        currentPage: <?php echo $truck['page'];?>,
                        pageUrl: '<?php echo BASE_URL;?>index.php?/truck/setting/&koperasi=<?php echo $_GET['koperasi'];?>&keyword=<?php echo $truck['keyword'];?>&page='
                    });
                    $('.tr_selector').on('click',function(){
                        var id = $(this).attr('data-id');
                        $('.editbtn').removeClass('fg-lightgray').removeAttr('disabled').attr('data-id',id);
                        $('.tr_selector').removeClass('active');
                        $(this).addClass('active');
                    });
                    $('#__edit').on('click',function(){
                        var id = $(this).attr('data-id');
                        if(typeof id !== 'undefined' && id !== ''){
                            $.ajax({
                                url:'<?php echo BASE_URL;?>index.php?/truck/setting/edit/'+id,
                                dataType:'json',
                                success:function(res){
                                    $('#edit-truck_id').val(res.truck_id);
                                    $('#edit-truck_nama').val(res.truck_nama);
                                    $('#edit-truck_status').val(res.truck_status);
                                    $('#edit-truck_nopol').val(res.truck_nopol);
                                    $('#edit-truck_koperasi').val(res.truck_koperasi);
                                    $('#edit-truck_kapasitas').val(res.truck_kapasitas);
                                    $('#modal-edit-department').addClass('fadein');
                                }
                            });
                        }
                    });

                    $('#__delete').on('click',function(){
                        var id = $(this).attr('data-id');
                        if(typeof id !== 'undefined' && id !== ''){
                            if(confirm('<?php echo _('Are you sure want to delete this data?');?>')){
                                $.ajax({
                                    url:'<?php echo BASE_URL;?>index.php?/truck/setting/delete',
                                    type:'post',
                                    data:{
                                        truck_id:id
                                    },
                                    success:function(){
                                        location.reload();
                                    }
                                });
                            }
                        }
                    });

                });

                function filter(){
                    var keyword = $('#keyword').val();
                    var koperasi = $('#koperasi').val();
                    window.location.href = '<?php echo BASE_URL;?>index.php?/truck/setting/&koperasi='+koperasi+'&keyword='+keyword+'&page=1';
                }
            </script>
            <?php View::inc('footer.php');?>
