<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>

<div class="transaksi left">
    <div class="padding-left-15 padding-right-15">
        <div class="row padding-bottom-10 padding-top-10 bg-gray fg-white">
            <div class="col-md-4">
                <h2 class="text-ellipsis">
                    Operator : <?php echo session::get('user_fullname');?>
                </h2>
            </div>
            <div class="col-md-4 text-center">
                <div class="text-center border">
                    <h4 class="text-ellipsis no-padding no-margin"><?php echo date('d F Y');?></h4>
                    <h4 id="clock" class="no-padding no-margin">00:00</h4>
                </div>
            </div>
            <div class="col-md-4 text-right padding-top-10">
<!--                <button class="bg-green fg-white" id="start-record">Start</button>-->
<!--                <button class="bg-red fg-white" id="stop-record">Stop</button>-->
                <button id="accept-record" class="fg-white">Accept</button>
            </div>
        </div>
        <div class="row padding-bottom-10 padding-top-10 bg-lightgray fg-gray border-right">
            <h1 style="font-size:180px;" class="text-center">
                <span id="display-kg">0.00</span><sub style="font-size:50px;">Kg</sub>
            </h1>
        </div>
    </div>
    <div>
        <div class="border-top border-bottom" style="width:100%;height:200px;position:relative;">
            <table class="table table-condensed table-stripped">
                <tr>
                    <th class="border-right" style="width:50px !important;">No.</th>
                    <th class="border-right">Berat Susu (Kg)</th>
                    <th style="width:100px !important" class="text-center">action
                        <input type="hidden" id="input-timbangan" value="0.00" class="input-block"/>
                    </th>
                </tr>
            </table>
            <div class="border-top" style="position:absolute;right:0;bottom:0;top:35px;width:100%;overflow: auto;" >
                <form id="formTransaksi" action="javascript:void(0);" method="post">
                    <table class="table" id="transaksi-detail-log">

                    </table>
                    <input type="hidden" name="transaksi_totalberat" id="totalberat-form">
                    <input type="hidden" name="transaksi_id" value="">
                    <input type="hidden" name="anggota_nomor" value="">
                    <input type="hidden" name="anggota_nama" value="">
                    <input type="hidden" name="anggota_alamat" value="">
                    <input type="hidden" name="kelompok_nama" value="">
                    <input type="hidden" name="kelompok_kode" value="">
                    <input type="hidden" name="tpk_nama" value="">
                    <input type="hidden" name="anggota_nohp" value="">
                </form>
            </div>

        </div>
        <div class="padding-right-10 text-right">
            <i class="ion-ios7-printer-outline"> </i><span id="printerStatusBar"></span>
        </div>

    </div>
</div>
<div class="transaksi right bg-lightgray">
    <div class="padding-15">
        <h1 class="margin-bottom-15"><i class="ion-ios7-contact-outline"></i> Data Anggota</h1>
        <div class="form-group">
            <label class="col-md-4" for="">No. Anggota</label>
            <div class="col-md-8">
                <input type="text" class="input-block" id="data-anggota_nomor" value=""/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4" for="">Nama</label>
            <div class="col-md-8">
                <input type="text" class="input-block" id="data-anggota_nama" value=""/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4" for="">TPK</label>
            <div class="col-md-8">
                <input type="text" class="input-block" id="data-anggota_tpk" value=""/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4" for="">Kelompok</label>
            <div class="col-md-8">
                <input type="text" class="input-block" id="data-anggota_kelompok" value=""/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4" for="">Kelompok Harga</label>
            <div class="col-md-8">
                <input type="text" class="input-block" id="data-anggota_kelompokharga" value=""/>
            </div>
        </div>
    </div>
    <div class="padding-left-15 padding-right-15">
        <div class="panel bg">
            <div class="panel-header no-border">
                <h3 class="panel-title text-center fg-white">Berat susu (Kg)</h3>
            </div>
            <div class="panel-body">
                <div class="padding-10">
                    <h1 id="totalberatsusu" class="text-center fg-white" style="font-size:70px;">0.00</h1>
                </div>
            </div>
        </div>
        <div class="text-center">
            <div>
                <button id="cancel-transaction" class="bg-darkred fg-white" style="margin:5px;font-size:20px;">
                    <i class="ion-android-close"></i>
                    Cancel
                </button>
                <button id="show-barcode" style="margin:5px;font-size:20px;">
                    <i class="ion-ios7-barcode"></i>
                    Barcode
                </button>
                <button id="submit-transaction" class="default fg-white" style="margin:5px;font-size:20px;">
                    <i class="ion-android-send"></i>
                    Submit
                </button>
            </div>
        </div>
    </div>

<!--        <div class="panel bg">-->
<!--            <div class="panel-header no-border">-->
<!--                <h3 class="panel-title text-center fg-white">Total Berat susu (Kg)</h3>-->
<!--            </div>-->
<!--            <div class="panel-body">-->
<!--                <div class="padding-20">-->
<!--                    <h1 class="text-center fg-white" id="totalberatsusu" style="font-size:70px;">0.00</h1>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
    <canvas id="hidden_screenshot" style="display: none;"></canvas>
    <applet name="jZebra" code="jzebra.PrintApplet.class" archive="<?php echo BASE_URL;?>themes/default/jzebra/jzebra.jar" width="1px" height="1px" style="visibility: hidden;">
        <!-- Optional, searches for printer with "zebra" in the name on load -->
        <!-- Note:  It is recommended to use applet.findPrinter() instead for ajax heavy applications -->
        <param name="printer" value="zebra">
        <!-- ALL OF THE CACHE OPTIONS HAVE BEEN REMOVED DUE TO A BUG WITH JAVA 7 UPDATE 25 -->
        <!-- Optional, these "cache_" params enable faster loading "caching" of the applet -->
        <!-- <param name="cache_option" value="plugin"> -->
        <!-- Change "cache_archive" to point to relative URL of jzebra.jar -->
        <!-- <param name="cache_archive" value="./jzebra.jar"> -->
        <!-- Change "cache_version" to reflect current jZebra version -->
        <!-- <param name="cache_version" value="1.4.9.1"> -->
    </applet>
</div>
<div class="modal drop draggable border" id="modal-new-transaction">
    <div class="modal-dialog modal-sm">
        <div class="modal-header">
            <h3 class="modal-title">New Transaction</h3>
            <a href="" class="close">&times;</a>
        </div>
        <div class="modal-body">
            <h2 class="fg-gray no-margin padding-bottom-10">No. Anggota</h2>

            <input type="text" id="newtransaction-anggota_nomor" name="anggota_nomor" class="input-block" style="font-size:24px;" placeholder="Input / Scan ID Card Number" required />
            <div class="text-right padding-top-30">
                <button type="button" onclick="openUrl('dashboard');">EXIT</button>
                <button type="button" class="default" onclick="newTransaction();">SUBMIT</button>
            </div>
        </div>
    </div>
</div>

    <script>
        $(document).ready(function(){
            var currentSession = $.cookie('transaksi_id');
            var totalTR = $.cookie('total_tr');
            var totalKG = $.cookie('total_kg');
            if(typeof totalTR === 'undefined' || totalTR === ''){
                totalTR = 0;
            }
            if(typeof totalKG === 'undefined' || totalKG === ''){
                totalKG = 0;
            }

            var trxDetail = $.cookie('transaksi_detail');
            var transaksiDetail = [];
            if(typeof trxDetail !== 'undefined' && trxDetail !== ''){
                transaksiDetail = JSON.parse(trxDetail);
            }
            $('#show-barcode').on('click',function(){
                var str = "CT~~CD,~CC^~CT~\n";
                    str += "^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR3,3~SD15^JUS^LRN^CI0^XZ\n";
                    str += "^XA\n";
                    str += "^MMT\n";
                    str += "^PW400\n";
                    str += "^LL0160\n";
                    str += "^LS0\n";
                    //str += "\\^"+ $.cookie('transaksi_id')+"^FS\n";
                    str += "^BY3,3,77^FT32,101^BCN,,Y,N\n";
                    str += "^FD>;"+ $.cookie('transaksi_id')+"^FS\n";
                    str += "^PQ1,0,1,Y^XZ\n";
                    
                    //alert(str);
                    var transaksi_id = $.cookie('transaksi_id');
                    printBarcode(transaksi_id);
                window.open('<?php echo BASE_URL;?>barcode-php.php?text='+$.cookie('transaksi_id'),'Show BarCode','width=240px,height=80px');
            });
            if(typeof currentSession === 'undefined' || currentSession === ''){
                //window.location.href = '<?php echo BASE_URL;?>index.php?/transaksi';
                $('#modal-new-transaction').addClass('fadein',function(){
                    $('#newtransaction-anggota_nomor').focus();
                });
            }else{
                var anggota = <?php echo json_encode($anggota);?>;

                $('#data-anggota_nomor').val(anggota.anggota_nomor);
                $('#data-anggota_nama').val(anggota.anggota_nama);
                $('#data-anggota_tpk').val(anggota.tpk_nama);
                $('#data-anggota_kelompok').val(anggota.kelompok_nama);
                $('#data-anggota_kelompokharga').val(anggota.kelompokharga_nama);
                $('#formTransaksi').find('input[name=anggota_nama]').val(anggota.anggota_nama);
                $('#formTransaksi').find('input[name=tpk_nama]').val(anggota.tpk_nama);
                $('#formTransaksi').find('input[name=kelompok_nama]').val(anggota.kelompok_nama);
                $('#formTransaksi').find('input[name=kelompok_kode]').val(anggota.kelompok_kode);
                $('#formTransaksi').find('input[name=anggota_nomor]').val(anggota.anggota_nomor);
                $('#formTransaksi').find('input[name=anggota_alamat]').val(anggota.anggota_alamat);
                $('#formTransaksi').find('input[name=anggota_nohp]').val(anggota.anggota_nohp);
                $('#formTransaksi').find('input[name=transaksi_id]').val(currentSession);
            }
            $('#submit-transaction').on('click',function(){
                $('#formTransaksi').submit();
            });
            $(document).on('keyup','#newtransaction-anggota_nomor',function(e){
                console.log(e.keyCode);
                if(e.keyCode === 13 || e.keyCode === 9){
                    newTransaction();
                }
            });
            $(document).on('submit','#formTransaksi',function(){
                var ser = $(this).serialize();
                ser = ser.replace(/%5B/g,"[");
                ser = ser.replace(/%5D/g,"]");

                $.ajax({
                    url:'<?php echo BASE_URL;?>index.php?/transaksi/save',
                    type:'post',
                    data:ser,
                    dataType:'json',
                    success:function(res){
                        if(res.status === 'success'){

                            var maxChar = <?php echo Config::$application['maximal_printer_karakter'];?>;
                            var nota = "\n";
                            nota += plainTextDivider("=",maxChar);
                            nota += "\n";
                            nota += plainTextCenter("STRUK PENERIMAAN SUSU HARIAN",maxChar);
                            nota += "\n";
                            nota += plainTextCenter("KPBS PANGALENGAN",maxChar);
                            nota += "\n";
                            //nota += plainTextCenter("Operator "+res.data.transaksi_user,maxChar);
                            //nota += "\n";
                            nota += plainTextDivider("=",maxChar);
                            nota += "\n";
                            //nota += plainTextDivider("-",maxChar);
                            //nota += "\n";
                            nota += plainTextFormat("No Penimbangan",""+res.data.transaksi_id+"",maxChar);
                            nota += "\n";
                            nota += plainTextFormat("Tanggal / Jam",""+res.data.transaksi_waktu+"",maxChar);
                            nota += "\n";
                            nota += plainTextFormat("ID Peternak",""+res.data.anggota_nomor+"",maxChar);
                            nota += "\n";
                            nota += plainTextFormat("Nama Peternak",""+res.data.anggota_nama+"",maxChar);
                            nota += "\n";
                            nota += plainTextFormat("TPK / Kelompok",""+res.data.tpk_nama+"/"+res.data.kelompok_nama+"",maxChar);
                            nota += "\n";
                            nota += "\n";
                            //nota += plainTextDivider("-",maxChar);
                            nota += "\n";
                            nota += "Detail Transaksi:";
                            nota += "\n";
                            var detil = res.data.detail_transaksi;
                            for(var k in detil){
                                nota += plainTextFormat("", ""+detil[k].detail+"",maxChar);
                                nota += "\n";
                            }
                            nota += plainTextFormat("","________+",maxChar);
                            nota += "\n";
                            nota += plainTextFormat("","Total (Kg):    "+res.data.transaksi_totalberat+"",maxChar);
                            nota += "\n";
                            nota += "\n";
                            nota += "Operator";
                            nota += "\n";
                            nota += ""+res.data.transaksi_user;
                            nota += "\n";
                            nota += plainTextDivider("=",maxChar);
                            nota += "\n";
                            nota += plainTextCenter("Mohon disimpan struk penerimaan susu ini",maxChar);
                            nota += "\n";

                            var total_h = (detil.length + 13);
                            var maksimal_h = 23;

                            if(total_h < 23){
                                var total_enter = (23 - total_h);
                                for(index=1;index<=total_enter;index++){
                                    nota += "\n";
                                }
                            }

                            //console.log(nota);

                            //alert(detil.length);
                            $.removeCookie('transaksi_id');
                            $.removeCookie('transaksi_detail');
                            $.removeCookie('total_tr');
                            $.removeCookie('total_kg');
                            totalKG = 0;
                            totalTR = 0;
                            $('#transaksi-detail-log').html('');
                            $('#data-anggota_nomor').val('');
                            $('#data-anggota_nama').val('');
                            $('#data-anggota_tpk').val('');
                            $('#data-anggota_kelompok').val('');
                            $('#data-anggota_kelompokharga').val('');
                            $('#formTransaksi').find('input[name=anggota_nama]').val('');
                            $('#formTransaksi').find('input[name=tpk_nama]').val('');
                            $('#formTransaksi').find('input[name=kelompok_nama]').val('');
                            $('#formTransaksi').find('input[name=kelompok_kode]').val('');
                            $('#formTransaksi').find('input[name=anggota_nomor]').val('');
                            $('#formTransaksi').find('input[name=anggota_alamat]').val('');
                            $('#formTransaksi').find('input[name=anggota_nohp]').val('');
                            $('#formTransaksi').find('input[name=transaksi_id]').val('');
                            $('#totalberatsusu').html('0.00');
                            $('#totalberat-bawahtabel').val(0.00);
                            $('#totalberat-form').val(0.00);

                            printStruk(nota);
                            $('#modal-new-transaction').addClass('fadein',function(){
                                $('#newtransaction-anggota_nomor').val('').focus();
                            });
                        }else{
                            alert(res.status);
                        }
                    }
                });
            });
            $('#cancel-transaction').on('click',function(){
                if(confirm('Are you sure want to cancel this transaction?')){
                    $.ajax({
                        url:'<?php echo BASE_URL;?>index.php?/transaksi/batal/'+currentSession,
                        success:function(res){
                            if(res === 'success'){
                                $.removeCookie('transaksi_id');
                                $.removeCookie('transaksi_detail');
                                $.removeCookie('total_tr');
                                $.removeCookie('total_kg');
                                totalKG = 0;
                                totalTR = 0;
                                $('#transaksi-detail-log').html('');
                                $('#data-anggota_nomor').val('');
                                $('#data-anggota_nama').val('');
                                $('#data-anggota_tpk').val('');
                                $('#data-anggota_kelompok').val('');
                                $('#data-anggota_kelompokharga').val('');
                                $('#formTransaksi').find('input[name=anggota_nama]').val('');
                                $('#formTransaksi').find('input[name=tpk_nama]').val('');
                                $('#formTransaksi').find('input[name=kelompok_nama]').val('');
                                $('#formTransaksi').find('input[name=kelompok_kode]').val('');
                                $('#formTransaksi').find('input[name=anggota_nomor]').val('');
                                $('#formTransaksi').find('input[name=anggota_alamat]').val('');
                                $('#formTransaksi').find('input[name=anggota_nohp]').val('');
                                $('#formTransaksi').find('input[name=transaksi_id]').val('');
                                $('#totalberatsusu').html('0.00');
                                $('#totalberat-bawahtabel').val(0.00);
                                $('#totalberat-form').val(0.00);
                                $('#modal-new-transaction').addClass('fadein',function(){
                                    $('#newtransaction-anggota_nomor').val('').focus();
                                });
                                //window.location.href = '<?php echo BASE_URL;?>index.php?/transaksi';
                            }else{
                                alert('Failed cancelling transaction!');
                            }
                        }
                    });
                }
            });
            var tes;
//            $('#start-record').on('click',function(){
//                if(typeof tes !== 'undefined'){
//                    clearInterval(tes);
//                }
//                tes = setInterval(function(){
//                    $.get('<?php //echo BASE_URL;?>//index.php?/transaksi/randomNumber',function(res){
//                        var responnya = parseFloat(res.replace(',','.'));
//                        $('#input-timbangan').val(responnya.toFixed(2));
//                        $('#display-kg').html(responnya.toFixed(2));
//                    });
//                },200);
//                $(this).removeClass('bg-green');
//                //$('#accept-record').addClass('bg-green');
//            });
//            $('#stop-record').on('click',function(e){
//                if(typeof tes !== 'undefined'){
//                    clearInterval(tes);
//                    $('#start-record').addClass('bg-green');
//                    var valu = $('#input-timbangan').val();
//                    if(valu !== '' && valu !== 0){
//                        $('#accept-record').addClass('bg-lightblue');
//                    }else{
//                        $('#accept-record').removeClass('bg-lightblue');
//                    }
//                }
//            });


            $(document).on('click','#accept-record',function(){
                var kg = $('#input-timbangan').val();
                totalTR++;
                var uuid = generate_id();
                var tr = '<tr class="border-bottom" id="transaksidetail_tr'+uuid+'"><input type="hidden" name="transaksidetail_berat[]" value="'+kg+'"><td class="border-right" style="width:50px !important;;">'+totalTR+'</td><td class="border-right">'+kg+'</td><td class="text-center" style="width:100px !important;"><a href="javascript:void(0);" onclick="removeTR(\''+uuid+'\');" class="icon-delete fg-red" title="Delete"></a></td></tr>';
                $('#transaksi-detail-log').append(tr);
                var tempKG = (parseFloat(totalKG) + parseFloat(kg));
                totalKG = tempKG.toFixed(2);
                $('#totalberatsusu').html(totalKG);
                $('#totalberat-bawahtabel').val(totalKG);
                $('#totalberat-form').val(totalKG);
                $('#input-timbangan').val('');
                $('#display-kg').html('0.00');
                transaksiDetail.push(kg);
                $.cookie('transaksi_detail',JSON.stringify(transaksiDetail),{ expires: 30 });
                $.cookie('total_kg',totalKG,{ expires: 30 });
                $.cookie('total_tr',totalTR,{ expires: 30 });
                $('#submit-transaction').addClass('bg-blue');
                $(this).removeClass('bg-lightblue');

            });
        });
        function newTransaction(){
            var anggota_nomor = $('#newtransaction-anggota_nomor').val();
            $.ajax({
                url:'<?php echo BASE_URL;?>index.php?/transaksi/addnew',
                type:'post',
                data:{
                    anggota_nomor:anggota_nomor
                },
                dataType:'json',
                success:function(res){
                    if(res.error === ''){
                        var anggota = res.anggota;
                        $('#data-anggota_nomor').val(anggota.anggota_nomor);
                        $('#data-anggota_nama').val(anggota.anggota_nama);
                        $('#data-anggota_tpk').val(anggota.tpk_nama);
                        $('#data-anggota_kelompok').val(anggota.kelompok_nama);
                        $('#data-anggota_kelompokharga').val(anggota.kelompokharga_nama);
                        $('#formTransaksi').find('input[name=anggota_nama]').val(anggota.anggota_nama);
                        $('#formTransaksi').find('input[name=tpk_nama]').val(anggota.tpk_nama);
                        $('#formTransaksi').find('input[name=kelompok_nama]').val(anggota.kelompok_nama);
                        $('#formTransaksi').find('input[name=kelompok_kode]').val(anggota.kelompok_kode);
                        $('#formTransaksi').find('input[name=anggota_nomor]').val(anggota.anggota_nomor);
                        $('#formTransaksi').find('input[name=anggota_alamat]').val(anggota.anggota_alamat);
                        $('#formTransaksi').find('input[name=anggota_nohp]').val(anggota.anggota_nohp);
                        $('#formTransaksi').find('input[name=transaksi_id]').val(res.transaksi_id);
                        $.cookie('transaksi_id',res.transaksi_id);
                        $('#modal-new-transaction').removeClass('fadein');
                        return false;
                    }
                }
            });
        }
        function GetClock() {
            d = new Date();
            nday = d.getDay();
            nmonth = d.getMonth();
            ndate = d.getDate();
            nyear = d.getYear();
            nhour = d.getHours();
            nmin = d.getMinutes();
            nsec = d.getSeconds();

            if (nyear < 1000) nyear = nyear + 1900;

            if (nhour == 0) {
                ap = " AM";
                nhour = 12;
            }
            else if (nhour <= 11) {
                ap = " AM";
            }
            else if (nhour == 12) {
                ap = " PM";
            }
            else if (nhour >= 13) {
                ap = " PM";
                nhour -= 12;
            }

            if (nmin <= 9) {
                nmin = "0" + nmin;
            }
            if (nsec <= 9) {
                nsec = "0" + nsec;
            }


            document.getElementById('clock').innerHTML = nhour+ ":" + nmin + ":" + nsec + ap + "";
            setTimeout("GetClock()", 1000);
        }
        function plainTextFormat(leftText,rightText,panjangMaximal){
            if(typeof leftText === 'undefined'){
                leftText = "";
            }
            if(typeof rightText === 'undefined'){
                rightText = "";
            }
            var res = leftText;
            if(leftText === ""){
                var panjangLeft = 0;
            }else{
                var panjangLeft = leftText.length;
            }

            var panjangRight = rightText.length;
            var totalPanjang = parseInt(panjangLeft) + parseInt(panjangRight);
            var selisihPanjang = parseInt(panjangMaximal) - parseInt(totalPanjang);

            for(i=0;i<selisihPanjang;i++){
                res += " ";
            }
            res += rightText;
            return res;
        }
        function plainTextCenter(str,panjangMaksimal){
            var panjangStr = str.length;
            var selisih = parseInt(panjangMaksimal) - parseInt(panjangStr);
            var sepasi = (selisih / 2);
            var res = '';
            for(i=0;i<parseInt(sepasi);i++){
                res += ' ';
            }
            res += str;
            return res;
        }
        function plainTextDivider(str,panjangText){
            var res = '';
            for(i=0;i<panjangText;i++){
                res += str;
            }
            return res;
        }
        function formatNumber(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
        function detectPrinter()
        {
            var applet = document.jZebra;
            //console.log(applet);
            if (applet != null)
            {
                applet.findPrinter();
                while (!applet.isDoneFinding())
                {
// Wait
                }
                var ps = applet.getPrintService();
                if (ps == null) var info="Printer belum siap";
                else var info="Printer: siap";
            }
            else
                var info="Java Runtime belum siap!";
            document.getElementById("printerStatusBar").innerHTML=info;
            //window.setTimeout('detectPrinter()',5000);
            //alert(chr(27));
        }
        function chr(i){
            return String.fromCharCode(i);
        }
        function printBarcode(str){
            var applet = document.jZebra;
            if (applet != null)
            {
                applet.findPrinter('zebra');
                applet.append("^XA~TA000~JSN^LT0^MNW^MTT^PON^PMN^LH0,0^JMA^PR3,3~SD15^JUS^LRN^CI0^XZ\n");
                applet.append("^XA\n");
                applet.append("^MMT\n");
                applet.append("^PW400\n");
                applet.append("^LL0160\n");
                applet.append("^LS0\n");
                applet.append("^BY3,3,77^FT32,101^BCN,,Y,N\n");
                applet.append("^FD>;"+ str +"^FS\n");
                applet.append("^PQ1,0,1,Y^XZ\n");
                applet.print();
                while (!applet.isDonePrinting())
                {
// Wait
                }
                var e = applet.getException();
                if (e == null) var info="Printed Successfully";
                else var info="Error: " + e.getLocalizedMessage();
            }
            else
            {
                var info="Printer belum siap";
            }
            document.getElementById("printerStatusBar").innerHTML=info;
        }
        function printStruk(str)
        {
            var applet = document.jZebra;
            if (applet != null)
            {
                applet.findPrinter();
// Plain Text
                str=returnEnter(str);
                applet.append(str);
                applet.append("\x1D\x56\x41");
// Send to the printer
                applet.print();
                while (!applet.isDonePrinting())
                {
// Wait
                }
                var e = applet.getException();
                if (e == null) var info="Printed Successfully";
                else var info="Error: " + e.getLocalizedMessage();
            }
            else
            {
                var info="Printer belum siap";
            }
            document.getElementById("printerStatusBar").innerHTML=info;
            //notification(info);
        }

        function returnEnter(dataStr){
            return dataStr.replace(/(\r\n|\r|\n)/g, "\n");
        }
        window.onload = GetClock;
        $(window).load(function(){
            detectPrinter();
            var transaksi_detail = $.cookie('transaksi_detail');
            var total_kg = $.cookie('total_kg');
            var array_transaksi;
            if(typeof transaksi_detail !== 'undefined'){
                 var array_transaksi = JSON.parse(transaksi_detail);
            }
            if(typeof array_transaksi !== 'undefined' && array_transaksi != ''){
                var totalTR = 0;
                for(var k in array_transaksi){
                    totalTR++;
                    var uuid = generate_id();
                    var tr = '<tr class="border-bottom" id="transaksidetail_tr'+uuid+'"><input type="hidden" name="transaksidetail_berat[]" value="'+array_transaksi[k]+'"><td class="border-right" style="width:50px !important;;">'+totalTR+'</td><td class="border-right">'+array_transaksi[k]+'</td><td class="text-center" style="width:100px !important;"><a href="javascript:void(0);" onclick="removeTR(\''+uuid+'\');" class="icon-delete" title="delete"></a></td></tr>';
                    $('#transaksi-detail-log').append(tr);
                    $('#totalberatsusu').html(total_kg);
                    $('#totalberat-bawahtabel').val(total_kg);
                    $('#totalberat-form').val(total_kg);
                }
            }
            setInterval(function(){
                $.get('<?php echo BASE_URL;?>index.php?/transaksi/randomNumber',function(res){
                    var responnya = parseFloat(res.replace(',','.'));
                    $('#input-timbangan').val(responnya.toFixed(2));
                    $('#display-kg').html(responnya.toFixed(2));
                });
            },200);
        });
    </script>
<?php View::inc('footer.php');?>
