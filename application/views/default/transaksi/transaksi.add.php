<?php
include path('base').DS.'webclientprint'.DS.'Source'.DS.'WebClientPrint.php';
use Neodynamic\SDK\Web\WebClientPrint;
use Neodynamic\SDK\Web\Utils;
use Neodynamic\SDK\Web\DefaultPrinter;
use Neodynamic\SDK\Web\InstalledPrinter;
use Neodynamic\SDK\Web\ClientPrintJob;
?>
<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php if(Config::$application['enable_onscreenkeyboard'] === true){ ?>
<link rel="stylesheet" href="<?php echo BASE_URL;?>themes/default/css/onScreenKeyboard.css">
<script type="text/javascript" src="<?php echo BASE_URL;?>themes/default/js/jquery.onScreenKeyboard.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.input-osk').onScreenKeyboard({
            rewireReturn : 'submit',
            rewireTab : true
        });
    });
</script>
<?php } ?>
<?php echo WebClientPrint::createScript(Utils::getRoot().'/rawprint.php');?>

    <script type="text/javascript">
        $(document).ready(function(){
            var timer = null;
            /**
             * Create new transaction
             * @return void
             */
            function newTransaction(){
                var anggota_nomor = $('#newtransaction-anggota_nomor').val();
                $.ajax({
                    url:'<?php echo BASE_URL;?>index.php?/transaksi/addnew',
                    type:'post',
                    data:{
                        anggota_nomor:anggota_nomor
                    },
                    dataType:'json',
                    success:function(res){
                        if(res.error === ''){
                            var anggota = res.anggota;

                            $('#data-anggota_nomor').val(anggota.anggota_nomor);
                            $('#data-anggota_nama').val(anggota.anggota_nama);
                            $('#data-anggota_tpk').val(anggota.tpk_nama);
                            $('#data-anggota_kelompok').val(anggota.kelompok_nama);
                            $('#data-anggota_photo').attr('src',anggota.anggota_photo);
                            $('#data-anggota_sapi_population').html(anggota.cow_population);
                            $('#data-anggota_sapi_lactating').html(anggota.cow_lactating);
                            if(anggota.cow_antibiotic > 0){
                                $('#data-anggota_sapi_antibiotic').html('<span class="bg-red fg-white">Positive</span> ('+moment(anggota.cow_antibioticstartdate).format('DD/MM/YYYY')+'-'+moment(anggota.cow_antibioticstartdate).format('DD/MM/YYYY')+')');
                            }else{
                                $('#data-anggota_sapi_antibiotic').html('<span class="fg-green">Negative</span>');
                            }
                            //$('#data-anggota_kelompokharga').val(anggota.kelompokharga_nama);
                            $('#formTransaksi').find('input[name=anggota_nama]').val(anggota.anggota_nama);
                            $('#formTransaksi').find('input[name=tpk_nama]').val(anggota.tpk_nama);
                            $('#formTransaksi').find('input[name=kelompok_nama]').val(anggota.kelompok_nama);
                            $('#formTransaksi').find('input[name=kelompok_kode]').val(anggota.kelompok_kode);
                            $('#formTransaksi').find('input[name=anggota_nomor]').val(anggota.anggota_nomor);
                            $('#formTransaksi').find('input[name=anggota_alamat]').val(anggota.anggota_alamat);
                            $('#formTransaksi').find('input[name=anggota_nohp]').val(anggota.anggota_nohp);
                            $('#formTransaksi').find('input[name=transaksi_id]').val(res.transaksi_id);
                            $.cookie('transaksi_id',res.transaksi_id);
                            $('#modal-new-transaction').removeClass('fadein');
                            
                            if(res.notifikasi !== ''){
                                notification(res.notifikasi);
                                $('#notifikasi_sample').html(res.notifikasi).show();
                            }
                            if(res.notifikasi_antibiotic !== ''){
                                notification(res.notifikasi_antibiotic);
                                $('#notifikasi_antibiotic').html(res.notifikasi_antibiotic).show();
                            }
                            $('.transaksi').removeClass('blur');
                            $('.navbar').removeClass('blur');
                            //if (timer !== null) return;
                            timer = setInterval(function(){
                                $.get('<?php echo BASE_URL;?>index.php?/sync/randomNumber',function(res){
                                    var responnya = parseFloat(res.replace(',','.'));
                                    $('#input-timbangan').val(responnya.toFixed(2));
                                    $('#display-kg').html(responnya.toFixed(2));
                                });
                            },400);
                            return false;
                        }else{
                            notification('Card Number ['+anggota_nomor+'] not Found');
                            $('#newtransaction-anggota_nomor').val('').focus();
                            return false;
                        }
                    }
                });
            }
            var currentSession = $.cookie('transaksi_id');
            var totalTR = $.cookie('total_tr');
            var totalKG = $.cookie('total_kg');
            if(typeof totalTR === 'undefined' || totalTR === ''){
                totalTR = 0;
            }
            if(typeof totalKG === 'undefined' || totalKG === ''){
                totalKG = 0;
            }

            var trxDetail = $.cookie('transaksi_detail');
            var transaksiDetail = [];
            if(typeof trxDetail !== 'undefined' && trxDetail !== ''){
                transaksiDetail = JSON.parse(trxDetail);
            }
            $('#show-barcode').on('click',function(){
                //alert(str);
                var transaksi_id = $.cookie('transaksi_id');
                barcodePrint(transaksi_id);
            });
            if(typeof currentSession === 'undefined' || currentSession === ''){
                //window.location.href = '<?php echo BASE_URL;?>index.php?/transaksi';
                $('#modal-new-transaction').addClass('fadein',function(){
                    $('#newtransaction-anggota_nomor').focus();
                    <?php if(Config::$application['enable_onscreenkeyboard'] === true){ ?>
                        $('#newtransaction-anggota_nomor').trigger('click');
                    <?php } ?>
                    $('.transaksi').addClass('blur');
                    $('.navbar').addClass('blur');
                });
            }else{
                var anggota = <?php echo json_encode($anggota);?>;

                $('#data-anggota_nomor').val(anggota.anggota_nomor);
                $('#data-anggota_nama').val(anggota.anggota_nama);
                $('#data-anggota_tpk').val(anggota.tpk_nama);
                $('#data-anggota_kelompok').val(anggota.kelompok_nama);
                $('#data-anggota_photo').attr('src',anggota.anggota_photo);
                $('#data-anggota_sapi_population').html(anggota.cow_population);
                $('#data-anggota_sapi_lactating').html(anggota.cow_lactating);
                if(anggota.cow_antibiotic > 0){
                    $('#data-anggota_sapi_antibiotic').html('<span class="bg-red fg-white">Positive</span> ('+moment(anggota.cow_antibioticstartdate).format('DD/MM/YYYY')+'-'+moment(anggota.cow_antibioticstartdate).format('DD/MM/YYYY')+')');
                }else{
                    $('#data-anggota_sapi_antibiotic').html('<span class="fg-green">Negative</span>');
                }
                //$('#data-anggota_sapi_antibiotic').val(anggota.cow_antibiotic);
                //$('#data-anggota_kelompokharga').val(anggota.kelompokharga_nama);
                $('#formTransaksi').find('input[name=anggota_nama]').val(anggota.anggota_nama);
                $('#formTransaksi').find('input[name=tpk_nama]').val(anggota.tpk_nama);
                $('#formTransaksi').find('input[name=kelompok_nama]').val(anggota.kelompok_nama);
                $('#formTransaksi').find('input[name=kelompok_kode]').val(anggota.kelompok_kode);
                $('#formTransaksi').find('input[name=anggota_nomor]').val(anggota.anggota_nomor);
                $('#formTransaksi').find('input[name=anggota_alamat]').val(anggota.anggota_alamat);
                $('#formTransaksi').find('input[name=anggota_nohp]').val(anggota.anggota_nohp);
                $('#formTransaksi').find('input[name=transaksi_id]').val(currentSession);
                if (timer !== null) return;
                timer = setInterval(function(){
                    $.get('<?php echo BASE_URL;?>index.php?/sync/randomNumber',function(res){
                        var responnya = parseFloat(res.replace(',','.'));
                        $('#input-timbangan').val(responnya.toFixed(2));
                        $('#display-kg').html(responnya.toFixed(2));
                    });
                },400);
                $('.transaksi').removeClass('blur');
                $('.navbar').removeClass('blur');
            }
            $('#submit-transaction').on('click',function(){
                $('#formTransaksi').submit();
            });
            $(document).on('keyup','#newtransaction-anggota_nomor',function(e){
                //console.log(e.keyCode);
                if(e.keyCode === 13 || e.keyCode === 9){
                    newTransaction();
                }
            });
            $(document).on('click','#newTransaction-btn',function(e){
                e.preventDefault();
                newTransaction();
            });
            $(document).on('submit','#formTransaksi',function(){
                var ser = $(this).serialize();
                ser = ser.replace(/%5B/g,"[");
                ser = ser.replace(/%5D/g,"]");

                $.ajax({
                    url:'<?php echo BASE_URL;?>index.php?/transaksi/save',
                    type:'post',
                    data:ser,
                    dataType:'json',
                    success:function(res){
                        if(res.status === 'success'){
                            clearInterval(timer);
                            timer = null
                            var maxChar = <?php echo Config::$application['maximal_printer_karakter'];?>;
                            var nota = "";
                            nota += plainTextDivider("=",maxChar);
                            nota += "[newline]";
                            nota += plainTextCenter("STRUK PENERIMAAN SUSU HARIAN",maxChar);
                            nota += "[newline]";
                            nota += plainTextCenter("KPBS PANGALENGAN",maxChar);
                            nota += "[newline]";
                            //nota += plainTextCenter("Operator "+res.data.transaksi_user,maxChar);
                            //nota += "\n";
                            nota += plainTextDivider("=",maxChar);
                            nota += "[newline]";
                            //nota += plainTextDivider("-",maxChar);
                            //nota += "\n";
                            nota += plainTextFormat("No Penimbangan",""+res.data.transaksi_id+"",maxChar);
                            nota += "[newline]";
                            nota += plainTextFormat("Tanggal / Jam",""+res.data.transaksi_waktu+"",maxChar);
                            nota += "[newline]";
                            nota += plainTextFormat("ID Peternak",""+res.data.anggota_nomor+"",maxChar);
                            nota += "[newline]";
                            nota += plainTextFormat("Nama Peternak",""+res.data.anggota_nama+"",maxChar);
                            nota += "[newline]";
                            nota += plainTextFormat("TPK / Kelompok",""+res.data.tpk_nama+"/"+res.data.kelompok_nama+"",maxChar);
                            nota += "[newline]";
                            nota += "[newline]";
                            //nota += plainTextDivider("-",maxChar);
                            nota += "[newline]";
                            nota += "Detail Penimbangan:";
                            nota += "[newline]";
                            var detil = res.data.detail_transaksi;
                            for(var k in detil){
                                nota += plainTextFormat("", ""+detil[k].detail+"",maxChar);
                                nota += "[newline]";
                            }
                            nota += plainTextFormat("","----------",maxChar);
                            nota += "[newline]";
                            nota += plainTextFormat("","Total (Kg):    "+res.data.transaksi_totalberat+"",maxChar);
                            nota += "[newline]";
                            nota += "[newline]";
                            nota += "Operator";
                            nota += "[newline]";
                            nota += ""+res.data.transaksi_user;
                            nota += "[newline]";
                            nota += plainTextDivider("=",maxChar);
                            nota += "[newline]";
                            nota += plainTextCenter("Mohon disimpan struk penerimaan susu ini",maxChar);
                            nota += "[newline]";
                            nota += "[newline]";
                            nota += "[newline]";
                            nota += "[newline]";
                            nota += "[newline]";
                            nota += "[newline]";
                            // //nota += "\x1D\x56\x41"; // POTONG KERTAS
                            // var total_h = (detil.length + 13);
                            // var maksimal_h = 23;

                            // if(total_h < 23){
                            //     var total_enter = (23 - total_h);
                            //     for(index=1;index<=total_enter;index++){
                            //         nota += "\n";
                            //     }
                            // }

                            //console.log(nota);
                            //
                            //
                            
                            
                            //alert(detil.length);
                            $.removeCookie('transaksi_id');
                            $.removeCookie('transaksi_detail');
                            $.removeCookie('total_tr');
                            $.removeCookie('total_kg');
                            totalKG = 0;
                            totalTR = 0;
                            $('#transaksi-detail-log').html('');
                            $('#data-anggota_nomor').val('');
                            $('#data-anggota_nama').val('');
                            $('#data-anggota_tpk').val('');
                            $('#data-anggota_sapi_population').html('');
                            $('#data-anggota_sapi_lactating').html('');
                            $('#data-anggota_sapi_antibiotic').html('');
                            $('#data-anggota_kelompok').val('');
                            $('#data-anggota_photo').attr('src','themes/default/images/avatar.jpg');
                            $('#formTransaksi').find('input[name=anggota_nama]').val('');
                            $('#formTransaksi').find('input[name=tpk_nama]').val('');
                            $('#formTransaksi').find('input[name=kelompok_nama]').val('');
                            $('#formTransaksi').find('input[name=kelompok_kode]').val('');
                            $('#formTransaksi').find('input[name=anggota_nomor]').val('');
                            $('#formTransaksi').find('input[name=anggota_alamat]').val('');
                            $('#formTransaksi').find('input[name=anggota_nohp]').val('');
                            $('#formTransaksi').find('input[name=transaksi_id]').val('');
                            $('#totalberatsusu').html('0.00');
                            $('#totalberat-bawahtabel').val(0.00);
                            $('#totalberat-form').val(0.00);
                            //rawPrintFile(res.data);
                            rawPrint(nota);
                            $('#modal-new-transaction').addClass('fadein',function(){
                                $('#notifikasi_sample').hide();
                                $('#newtransaction-anggota_nomor').val('').focus();
                                $('.transaksi').addClass('blur');
                                $('.navbar').addClass('blur');
                            });

                        }else{
                            alert(res.status);
                        }
                    }
                });
            });
            $('#cancel-transaction').on('click',function(){
                if(confirm('Are you sure want to cancel this transaction?')){
                    $.ajax({
                        url:'<?php echo BASE_URL;?>index.php?/transaksi/batal/'+currentSession,
                        success:function(res){
                            if(res === 'success'){
                                $.removeCookie('transaksi_id');
                                $.removeCookie('transaksi_detail');
                                $.removeCookie('total_tr');
                                $.removeCookie('total_kg');
                                totalKG = 0;
                                totalTR = 0;
                                $('#transaksi-detail-log').html('');
                                $('#data-anggota_nomor').val('');
                                $('#data-anggota_nama').val('');
                                $('#data-anggota_tpk').val('');
                                $('#data-anggota_sapi_population').html('');
                                $('#data-anggota_sapi_lactating').html('');
                                $('#data-anggota_sapi_antibiotic').html('');
                                $('#data-anggota_kelompok').val('');
                                $('#data-anggota_photo').attr('src','themes/default/images/avatar.jpg');
                                $('#formTransaksi').find('input[name=anggota_nama]').val('');
                                $('#formTransaksi').find('input[name=tpk_nama]').val('');
                                $('#formTransaksi').find('input[name=kelompok_nama]').val('');
                                $('#formTransaksi').find('input[name=kelompok_kode]').val('');
                                $('#formTransaksi').find('input[name=anggota_nomor]').val('');
                                $('#formTransaksi').find('input[name=anggota_alamat]').val('');
                                $('#formTransaksi').find('input[name=anggota_nohp]').val('');
                                $('#formTransaksi').find('input[name=transaksi_id]').val('');
                                $('#totalberatsusu').html('0.00');
                                $('#totalberat-bawahtabel').val(0.00);
                                $('#totalberat-form').val(0.00);
                                $('#modal-new-transaction').addClass('fadein',function(){
                                    $('#notifikasi_sample').hide();
                                    $('#newtransaction-anggota_nomor').val('').focus();
                                    $('.transaksi').addClass('blur');
                                    $('.navbar').addClass('blur');
                                });
                                clearInterval(timer);
                                timer = null
                                //window.location.href = '<?php echo BASE_URL;?>index.php?/transaksi';
                            }else{
                                alert('Failed cancelling transaction!');
                            }
                        }
                    });
                }
            });
            var tes;
            $(document).on('click','#accept-record',function(){
                var kg = $('#input-timbangan').val();
                if(!isNaN(kg)){
                    totalTR++;
                    var uuid = generate_id();
                    var tr = '<tr class="border-bottom" id="transaksidetail_tr'+uuid+'"><input type="hidden" name="transaksidetail_berat[]" value="'+kg+'"><td class="border-right" style="width:50px !important;;">'+totalTR+'</td><td class="border-right">'+kg+'</td><td class="text-center" style="width:100px !important;"><a href="javascript:void(0);" data-id="'+uuid+'" class="icon-delete fg-red __deleteLog" title="Delete"></a></td></tr>';
                    $('#transaksi-detail-log').append(tr);
                    var tempKG = (parseFloat(totalKG) + parseFloat(kg));
                    totalKG = tempKG.toFixed(2);
                    $('#totalberatsusu').html(totalKG);
                    $('#totalberat-bawahtabel').val(totalKG);
                    $('#totalberat-form').val(totalKG);
                    $('#input-timbangan').val('');
                    $('#display-kg').html('0.00');
                    transaksiDetail.push(kg);
                    $.cookie('transaksi_detail',JSON.stringify(transaksiDetail),{ expires: 30 });
                    $.cookie('total_kg',totalKG,{ expires: 30 });
                    $.cookie('total_tr',totalTR,{ expires: 30 });
                    $('#submit-transaction').addClass('bg-blue');
                    $(this).removeClass('bg-lightblue');
                }
            });

            $(document).on('click','.__deleteLog',function(){
                var uid = $(this).attr('data-id');
                var trnya = $('#transaksidetail_tr'+uid);
                var beratnya = trnya.find('input[type=hidden]').val();
                var tempKG = (parseFloat(totalKG) - parseFloat(beratnya));
                var tempTransaksiDetail = [];
                totalKG = tempKG.toFixed(2);

                $('#transaksi-detail-log').find('input[type=hidden]').each(function(){
                    tempTransaksiDetail.push($(this).val());
                });
                transaksiDetail = tempTransaksiDetail;
                totalTR--;
                $('#totalberatsusu').html(totalKG);
                $('#totalberat-bawahtabel').val(totalKG);
                $('#totalberat-form').val(totalKG);
                $('#input-timbangan').val('');
                $('#display-kg').html('0.00');
                $.cookie('transaksi_detail',JSON.stringify(transaksiDetail),{ expires: 30 });
                $.cookie('total_kg',totalKG,{ expires: 30 });
                $.cookie('total_tr',totalTR,{ expires: 30 });

                $('#transaksidetail_tr'+uid).remove();
                //console.log(transaksiDetail);
            });

            $(document).on('click','#overrideBtn',function(){
                var datanya = {
                    user_name:$('#override-user_name').val(),
                    user_password:$('#override-user_password').val(),
                    anggota_nomor:$('#override-anggota_nomor').val()
                }
                $.ajax({
                    url:'<?php echo BASE_URL;?>index.php?/transaksi/addnew/&override=true',
                    data:datanya,
                    type:'post',
                    dataType:'json',
                    success:function(res){
                        if(res.error === ''){
                            var anggota = res.anggota;

                            $('#data-anggota_nomor').val(anggota.anggota_nomor);
                            $('#data-anggota_nama').val(anggota.anggota_nama);
                            $('#data-anggota_tpk').val(anggota.tpk_nama);
                            $('#data-anggota_kelompok').val(anggota.kelompok_nama);
                            $('#data-anggota_photo').attr('src',anggota.anggota_photo);
                            $('#data-anggota_sapi_population').html(anggota.cow_population);
                            $('#data-anggota_sapi_lactating').html(anggota.cow_lactating);
                            if(anggota.cow_antibiotic > 0){
                                $('#data-anggota_sapi_antibiotic').html('<span class="bg-red fg-white">Positive</span> ('+moment(anggota.cow_antibioticstartdate).format('DD/MM/YYYY')+'-'+moment(anggota.cow_antibioticstartdate).format('DD/MM/YYYY')+')');
                            }else{
                                $('#data-anggota_sapi_antibiotic').html('<span class="fg-green">Negative</span>');
                            }
                            //$('#data-anggota_kelompokharga').val(anggota.kelompokharga_nama);
                            $('#formTransaksi').find('input[name=anggota_nama]').val(anggota.anggota_nama);
                            $('#formTransaksi').find('input[name=tpk_nama]').val(anggota.tpk_nama);
                            $('#formTransaksi').find('input[name=kelompok_nama]').val(anggota.kelompok_nama);
                            $('#formTransaksi').find('input[name=kelompok_kode]').val(anggota.kelompok_kode);
                            $('#formTransaksi').find('input[name=anggota_nomor]').val(anggota.anggota_nomor);
                            $('#formTransaksi').find('input[name=anggota_alamat]').val(anggota.anggota_alamat);
                            $('#formTransaksi').find('input[name=anggota_nohp]').val(anggota.anggota_nohp);
                            $('#formTransaksi').find('input[name=transaksi_id]').val(res.transaksi_id);
                            $.cookie('transaksi_id',res.transaksi_id);
                            $('#modal-new-transaction').removeClass('fadein');
                            
                            if(res.notifikasi !== ''){
                                notification(res.notifikasi);
                                $('#notifikasi_sample').html(res.notifikasi).show();
                            }
                            if(res.notifikasi_antibiotic !== ''){
                                notification(res.notifikasi_antibiotic);
                                $('#notifikasi_antibiotic').html(res.notifikasi_antibiotic).show();
                            }
                            $('.transaksi').removeClass('blur');
                            $('.navbar').removeClass('blur');
                            $('#override-anggota_nomor').val('');
                            $('#override-user_password').val('');
                            $('#override-user_name').val('');
                            //if (timer !== null) return;
                            timer = setInterval(function(){
                                $.get('<?php echo BASE_URL;?>index.php?/sync/randomNumber',function(res){
                                    var responnya = parseFloat(res.replace(',','.'));
                                    $('#input-timbangan').val(responnya.toFixed(2));
                                    $('#display-kg').html(responnya.toFixed(2));
                                });
                            },400);
                            return false;
                        }else{
                            notification(res.error);
                            $('#override-anggota_nomor').val('').focus();
                            $('#override-user_password').val('');
                            $('#override-user_name').val('');
                            return false;
                        }
                    }
                });
            });

            $(document).on('keyup','#override-anggota_nomor',function(e){
                if(e.keyCode === 13){
                   $('#overrideBtn').trigger('click');
                }
            });
            
            $(window).load(function(){
                //detectPrinter();
                GetClock();
                var transaksi_detail = $.cookie('transaksi_detail');
                var total_kg = $.cookie('total_kg');
                var array_transaksi;
                if(typeof transaksi_detail !== 'undefined'){
                     var array_transaksi = JSON.parse(transaksi_detail);
                }
                if(typeof array_transaksi !== 'undefined' && array_transaksi != ''){
                    var totalTR = 0;
                    for(var k in array_transaksi){
                        totalTR++;
                        var uuid = generate_id();
                        var tr = '<tr class="border-bottom" id="transaksidetail_tr'+uuid+'"><input type="hidden" name="transaksidetail_berat[]" value="'+array_transaksi[k]+'"><td class="border-right" style="width:50px !important;;">'+totalTR+'</td><td class="border-right">'+array_transaksi[k]+'</td><td class="text-center" style="width:100px !important;"><a href="javascript:void(0);" data-id="'+uuid+'" class="icon-delete __deleteLog" title="delete"></a></td></tr>';
                        $('#transaksi-detail-log').append(tr);
                        $('#totalberatsusu').html(total_kg);
                        $('#totalberat-bawahtabel').val(total_kg);
                        $('#totalberat-form').val(total_kg);
                    }
                }
            });
        });
        /**
         * Clock JS
         */
        function GetClock() {
            d = new Date();
            nday = d.getDay();
            nmonth = d.getMonth();
            ndate = d.getDate();
            nyear = d.getYear();
            nhour = d.getHours();
            nmin = d.getMinutes();
            nsec = d.getSeconds();

            if (nyear < 1000) nyear = nyear + 1900;

            if (nhour == 0) {
                ap = " AM";
                nhour = 12;
            }
            else if (nhour <= 11) {
                ap = " AM";
            }
            else if (nhour == 12) {
                ap = " PM";
            }
            else if (nhour >= 13) {
                ap = " PM";
                nhour -= 12;
            }

            if (nmin <= 9) {
                nmin = "0" + nmin;
            }
            if (nsec <= 9) {
                nsec = "0" + nsec;
            }


            document.getElementById('clock').innerHTML = nhour+ ":" + nmin + ":" + nsec + ap + "";
            setTimeout("GetClock()", 1000);
        }
        /**
         * Format Plain Text
         * @param  {[type]} leftText       [description]
         * @param  {[type]} rightText      [description]
         * @param  {[type]} panjangMaximal [description]
         * @return {[type]}                [description]
         */
        function plainTextFormat(leftText,rightText,panjangMaximal){
            if(typeof leftText === 'undefined'){
                leftText = "";
            }
            if(typeof rightText === 'undefined'){
                rightText = "";
            }
            var res = leftText;
            if(leftText === ""){
                var panjangLeft = 0;
            }else{
                var panjangLeft = leftText.length;
            }

            var panjangRight = rightText.length;
            var totalPanjang = parseInt(panjangLeft) + parseInt(panjangRight);
            var selisihPanjang = parseInt(panjangMaximal) - parseInt(totalPanjang);

            for(i=0;i<selisihPanjang;i++){
                res += " ";
            }
            res += rightText;
            return res;
        }

        /**
         * Format Plain text center alignment
         * @param  {[type]} str             [description]
         * @param  {[type]} panjangMaksimal [description]
         * @return {[type]}                 [description]
         */
        function plainTextCenter(str,panjangMaksimal){
            var panjangStr = str.length;
            var selisih = parseInt(panjangMaksimal) - parseInt(panjangStr);
            var sepasi = (selisih / 2);
            var res = '';
            for(i=0;i<parseInt(sepasi);i++){
                res += ' ';
            }
            res += str;
            return res;
        }

        /**
         * Create Plain text divider
         * @param  {[type]} str         [description]
         * @param  {[type]} panjangText [description]
         * @return {[type]}             [description]
         */
        function plainTextDivider(str,panjangText){
            var res = '';
            for(i=0;i<panjangText;i++){
                res += str;
            }
            return res;
        }

        /**
         * Format Number
         * @param  {[type]} x [description]
         * @return {[type]}   [description]
         */
        function formatNumber(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        function chr(i){
            return String.fromCharCode(i);
        }

        /**
         * Print Barcode
         * @param  string str [description]
         * @return {[type]}     [description]
         */
        function barcodePrint(str){
            var kode = $.cookie('transaksi_id')+'-<?php echo $transaksi_sesi;?>';
            jsWebClientPrint.print('printerName=<?php echo Config::$application['zebra_printername'];?>&str='+kode+'&barcode=yes&farmer_id='+$('#data-anggota_nomor').val());
            setTimeout(function(){
                $.get('<?php echo BASE_URL;?>index.php?/dashboard/killprocess');
            },3000);
        }

        /**
         * Sending Raw Data to Printer
         * @param  string str [description]
         * @return {[type]}     [description]
         */
        function rawPrint(str){
            jsWebClientPrint.print('printerName=<?php echo Config::$application['printer_name'];?>&str='+str);
            setTimeout(function(){
                $.get('<?php echo BASE_URL;?>index.php?/dashboard/killprocess');
            },4000);
        }


        function returnEnter(dataStr){
            return dataStr.replace(/(\r\n|\r|\n)/g, "\n");
        }
    </script>
<div class="transaksi left">
    <div class="padding-left-15 padding-right-15">
        <div class="row padding-bottom-10 padding-top-10 bg-gray fg-white">
            <div class="col-md-4">
                <h2 class="text-ellipsis">
                    Operator : <?php echo session::get('user_fullname');?>
                </h2>
            </div>
            <div class="col-md-4 text-center">
                <div class="text-center border">
                    <h4 class="text-ellipsis no-padding no-margin"><?php echo date('d F Y');?></h4>
                    <h4 id="clock" class="no-padding no-margin">00:00</h4>
                </div>
            </div>
            <div class="col-md-4 text-right padding-top-10">
<!--                <button class="bg-green fg-white" id="start-record">Start</button>-->
<!--                <button class="bg-red fg-white" id="stop-record">Stop</button>-->
                <button id="accept-record" class="fg-white">Accept</button>
            </div>
        </div>
        <div class="row padding-bottom-10 padding-top-10 bg-lightgray fg-gray border-right" style="position:relative;">
            <div style="position:absolute;top:0;left:0;right:0;width:100%;" id="">
                <div id="notifikasi_sample" class="bg-pink fg-white" style="display:none;"></div>
                <div id="notifikasi_antibiotic" class="bg-orange fg-white" style="display:none;"></div>
            </div>
            <h1 style="font-size:180px;" class="text-center">
                <span id="display-kg">0.00</span><sub style="font-size:50px;">Kg</sub>
            </h1>
        </div>
    </div>
    <div>
        <div class="border-top border-bottom" style="width:100%;height:200px;position:relative;">
            <table class="table table-condensed table-stripped">
                <tr>
                    <th class="border-right" style="width:50px !important;">No.</th>
                    <th class="border-right">Weight (Kg)</th>
                    <th style="width:100px !important" class="text-center">action
                        <input type="hidden" id="input-timbangan" value="0.00" class="input-block"/>
                    </th>
                </tr>
            </table>
            <div class="border-top" style="position:absolute;right:0;bottom:0;top:35px;width:100%;overflow: auto;" >
                <form id="formTransaksi" action="javascript:void(0);" method="post">
                    <table class="table" id="transaksi-detail-log">

                    </table>
                    <input type="hidden" name="transaksi_totalberat" id="totalberat-form">
                    <input type="hidden" name="transaksi_id" value="">
                    <input type="hidden" name="anggota_nomor" value="">
                    <input type="hidden" name="anggota_nama" value="">
                    <input type="hidden" name="anggota_alamat" value="">
                    <input type="hidden" name="kelompok_nama" value="">
                    <input type="hidden" name="kelompok_kode" value="">
                    <input type="hidden" name="tpk_nama" value="">
                    <input type="hidden" name="anggota_nohp" value="">
                </form>
            </div>

        </div>
        <div class="padding-right-10 text-right">
            <i class="ion-ios7-printer-outline"> </i><span id="printerStatusBar"></span>
        </div>

    </div>
</div>
<div class="transaksi right bg-lightgray">
    <div class="padding-15">
        <div class="form-group">
            <div class="col-md-12">
                <table style="width:100%;">
                    <tr>
                        <td style="width:100px;">
                            <img id="data-anggota_photo" src="themes/default/images/avatar.jpg" alt="" style="width:100%;padding-right:10px;">
                        </td>
                        <td style="vertical-align:top;">
                            <table style="width:100%;">
                                <tr>
                                    <td>
                                        ID
                                    </td>
                                    <td>:</td>
                                    <td>
                                        <input type="text" style="background:transparent;border:0;" readonly="readonly" class="input-block" id="data-anggota_nomor" value=""/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Nama</td>
                                    <td>:</td>
                                    <td>
                                        <input type="text" style="background:transparent;border:0;" readonly="readonly" class="input-block" id="data-anggota_nama" value=""/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>TPK</td>
                                    <td>:</td>
                                    <td>
                                        <input type="text" style="background:transparent;border:0;" readonly="readonly" class="input-block" id="data-anggota_tpk" value=""/>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Kelompok</td>
                                    <td>:</td>
                                    <td>
                                        <input type="text" style="background:transparent;border:0;" readonly="readonly" class="input-block" id="data-anggota_kelompok" value=""/>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        
        <div class="form-group border-top padding-top-10">
            <label class="col-md-4" for="">Cow Lactating</label>
            <div class="col-md-8">
                <span id="data-anggota_sapi_lactating">0</span> / <span id="data-anggota_sapi_population">0</span>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4" for="">Cow Antibiotic</label>
            <div class="col-md-8">
                <span id="data-anggota_sapi_antibiotic">Negative</span>
            </div>
        </div>
        <!-- <div class="form-group">
            <label class="col-md-4" for="">Kelompok Harga</label>
            <div class="col-md-8">
                <input type="text" class="input-block" id="data-anggota_kelompokharga" value=""/>
            </div>
        </div> -->
    </div>
    <div class="padding-left-15 padding-right-15">
        <div class="panel bg">
            <div class="panel-header no-border">
                <h3 class="panel-title text-center fg-white">Total Weight (Kg)</h3>
            </div>
            <div class="panel-body">
                <div class="padding-10">
                    <h1 id="totalberatsusu" class="text-center fg-white" style="font-size:70px;">0.00</h1>
                </div>
            </div>
        </div>
        <div class="text-center">
            <div>
                <button id="cancel-transaction" class="bg-darkred fg-white" style="margin:5px;font-size:20px;">
                    <i class="ion-android-close"></i>
                    Cancel
                </button>
                <button id="show-barcode" style="margin:5px;font-size:20px;">
                    <i class="ion-ios7-barcode"></i>
                    Barcode
                </button>
                <button id="submit-transaction" class="default fg-white" style="margin:5px;font-size:20px;">
                    <i class="ion-android-send"></i>
                    Submit
                </button>
            </div>
        </div>
    </div>
</div>
<div class="modal drop draggable border" id="modal-new-transaction">
    <div class="modal-dialog" style="border-color:#DDD !important;width:350px;margin-left:-175px;">
        <div class="modal-header" style="background:#DDD !important;">
            <h3 class="modal-title fg-gray">New Transaction</h3>
            <a href="" class="close">&times;</a>
        </div>
        <div class="modal-body">
            <div id="defaultscanmethod">
                <form action="javascript:void(0);">
                    <h2 class="fg-gray no-margin padding-bottom-10"><?php echo _('ID Card Number');?></h2>
                    <input type="text" id="newtransaction-anggota_nomor" autocomplete="off" name="anggota_nomor" class="input-block input-osk" style="font-size:24px;" placeholder="Input / Scan ID Card Number" required />
                    <div class="text-right padding-top-30">
                        <button type="button" onclick="openUrl('dashboard');">EXIT</button>
                        <button type="button" onclick="$('#defaultscanmethod').slideUp('fast',function(){ $('#overridescanmethod').show(); });" class="bg-orange fg-white">USE ID</button>
                        <button class="default" id="newTransaction-btn" type="button">SUBMIT</button>
                    </div>
                </form>
            </div>
            <div id="overridescanmethod" style="display:none;">
                <form action="javascript:void(0);">
                    <div class="text-center">
                        <img src="themes/default/images/avatar.jpg" alt="" class="padding-bottom-15">
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="text" id="override-user_name" class="input-block" style="font-size:24px;" autocomplete="off" name="user_name" placeholder="Username">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="password" id="override-user_password" class="input-block" style="font-size:24px;" autocomplete="off" name="user_password" placeholder="Password">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12">
                            <input type="text" id="override-anggota_nomor" class="input-block number" style="font-size:24px;" autocomplete="off" name="anggota_nomor" placeholder="Farmer ID">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 text-right">
                            <button onclick="$('#overridescanmethod').slideUp('fast',function(){ $('#defaultscanmethod').show(); });">back</button>
                            <button class="default" id="overrideBtn">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<?php View::inc('footer.php');?>
