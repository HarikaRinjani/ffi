<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<div class="transaksi left">
    <div class="padding-left-15 padding-right-15">
        <div class="row padding-bottom-10 padding-top-10 bg-gray fg-white">
            <div class="col-md-4">
                <h2 class="text-ellipsis">
                    Operator : <?php echo session::get('user_fullname');?>
                </h2>
            </div>
            <div class="col-md-4 text-center">
                <div class="text-center border">
                    <h4 class="text-ellipsis no-padding no-margin"><?php echo date('d F Y');?></h4>
                    <h4 id="clock" class="no-padding no-margin">00:00</h4>
                </div>
            </div>
            <div class="col-md-4 text-right padding-top-10">
                <button class="bg-green fg-white" id="start-record">Start</button>
                <button class="bg-red fg-white" id="stop-record">Stop</button>
                <button id="accept-record" class="fg-white">Accept</button>
            </div>
        </div>
        <div class="row padding-bottom-10 padding-top-10 bg-lightgray fg-gray border-right">
            <h1 style="font-size:180px;" class="text-center">
                <span id="display-kg">0.00</span><sub style="font-size:50px;">Kg</sub>
            </h1>
        </div>
    </div>
    <div class="border-top border-bottom" style="width:100%;height:300px;position:relative;">
        <table class="table table-condensed table-stripped">
            <tr>
                <th class="border-right" style="width:50px !important;">No.</th>
                <th class="border-right" style="width:200px !important;">Berat Susu (Kg)</th>
                <th style="">Action</th>
            </tr>
        </table>
        <div class="border-top" style="position:absolute;right:1px;bottom:0;top:70px;width:100%;overflow: auto;" id="transaksi-detail-log"></div>

    </div>
</div>
<div class="transaksi right bg-lightgray">
    <div class="padding-15">
        <h1 class="fg-orange margin-bottom-15">Data Anggota</h1>
        <div class="form-group">
            <label class="col-md-4" for="">No. Anggota</label>
            <div class="col-md-8">
                <input type="text" class="input-block"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4" for="">Nama</label>
            <div class="col-md-8">
                <input type="text" class="input-block"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4" for="">TPK</label>
            <div class="col-md-8">
                <input type="text" class="input-block"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4" for="">Kelompok</label>
            <div class="col-md-8">
                <input type="text" class="input-block"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4" for="">Kelompok Harga</label>
            <div class="col-md-8">
                <input type="text" class="input-block"/>
            </div>
        </div>
    </div>
    <div class="padding-left-15 padding-right-15">
        <div class="panel bg">
            <div class="panel-header no-border">
                <h3 class="panel-title text-center fg-white">Berat susu (Kg)</h3>
            </div>
            <div class="panel-body">
                <div class="padding-10">
                    <h1 id="totalberatsusu" class="text-center fg-white" style="font-size:70px;">0.00</h1>
                </div>
            </div>
        </div>
        <div class="text-center">
            <div>
                <button id="cancel-transaction" class="bg-darkred fg-white" style="margin:5px;">
                    <i class="ion-android-close"></i>
                    Cancel
                </button>
                <button id="submit-transaction" class="fg-white" style="margin:5px;">
                    <i class="ion-android-send"></i>
                    Submit
                </button>
            </div>
        </div>
    </div>

</div>
<div class="modal drop draggable border" id="modal-new-transaction">
    <div class="modal-dialog modal-sm">
        <div class="modal-header">
            <h3 class="modal-title">New Transaction</h3>
            <a href="" class="close">&times;</a>
        </div>
        <div class="modal-body">
            <form action="<?php echo BASE_URL; ?>index.php?/transaksi/add" method="post">
                <h2 class="fg-gray no-margin padding-bottom-10">No. Anggota</h2>

                <input type="text" id="newtransaction-anggota_nomor" name="anggota_nomor" class="input-block" style="font-size:24px;" placeholder="Input / Scan ID Card Number" required />
                <div class="text-right padding-top-30">
                    <button type="button" onclick="openUrl('dashboard');">EXIT</button>
                    <button class="default">SUBMIT</button>
                </div>
            </form>
        </div>
    </div>
</div>
    <script>
        $(document).ready(function(){
            var currentSession = $.cookie('transaksi_id');
            if(typeof currentSession === 'undefined' || currentSession === ''){
                $('#modal-new-transaction').addClass('fadein',function(){
                    $('#newtransaction-anggota_nomor').focus();
                });

            }else{
                var transaksiNomor = $.cookie('transaksi_nomor');
                if(typeof transaksiNomor !== 'undefined' && transaksiNomor !== ''){

                }

            }
        });
    </script>
<?php View::inc('footer.php');?>