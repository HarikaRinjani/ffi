<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('settings/settings.menu.php');?>

<div class="content padded">
    <div class="breadcrumb">
        <ul class="tab">
            <li>
                <a href="<?php echo BASE_URL;?>index.php?/milkprice/add">new</a>
            </li>
            <li>
                <a href="<?php echo BASE_URL;?>index.php?/milkprice/index">formula</a>
            </li>
            <li class="active">
                <a href="<?php echo BASE_URL;?>index.php?/milkprice/test"  disabled="disabled">test</a>
            </li>
        </ul>
        <div class="pull-right">
            <ul class="breadcrumb-nav">
                <li>
                    <a href="">Apps Settings</a>
                </li>
                <li>
                    <a href="">Milkprice Formula</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row padding-bottom-40">
            <div class="ribbon border-bottom">
                <div class="menu">
                    <a href="javascript:void(0);" <?php if(isset($formuladetail) && count($formuladetail) > 0){ ?> onclick="calculate();" <?php }else{ ?> class="fg-lightgray" disabled="disabled" <?php } ?> ><i class="ion-ios7-play-outline"></i> begin test</a>
                    <a href="javascript:void(0);"><i class="ion-ios7-help-outline"></i>help</a>
                </div>
            </div>
             <div>
                <?php if(in_array(session::get('user_group'),Config::$application['super_group'])){ ?>
                <table class="table no-border no-hover" style="width:auto">
                    <tr>
                        <td>
                            <h3>Select Cooperative</h3>
                        </td>
                        <td>:</td>
                        <td>
                            <select name="milkpriceformula_koperasi" id="milkpriceformula_koperasi">
                                <option value="">-- Select Cooperative --</option>
                                <?php foreach($koperasi['records'] as $k=>$v) { ?>
                                <option value="<?php echo $v['koperasi_id'];?>" <?php if(isset($_GET['milkpriceformula_koperasi']) && $v['koperasi_id'] == $_GET['milkpriceformula_koperasi']){ echo 'selected="selected"'; } ?> ><?php echo $v['koperasi_nama'];?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                </table>
                <?php } ?>
                <?php if(count($formula) == 0){ ?>
                <h4 class="padding-10">No Formulation Data</h4>
                <?php }else{ ?>
                <table class="table table-hover border-top border-bottom">
                    <thead>
                    <tr>
                        <th style="width:24px;">#</th>
                        <?php if(in_array(session::get('user_group'),Config::$application['super_group'])){ ?>
                        <th><?php echo _('Cooperative');?></th>
                        <?php } ?>
                        <th><?php echo _('Milk Price');?></th>
                        <th><?php echo _('Effective Date');?></th>
                        <th><?php echo _('Condition');?></th>
                        <th><?php echo _('Status');?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if($formula['total_data'] == 0){ ?>
                        <tr>
                            <td colspan="100%"><?php echo _('No record found');?></td>
                        </tr>
                    <?php }else{ ?>
                        <?php foreach($formula['records'] as $k=>$v){ ?>
                            <tr class="tr_selector cursor-pointer <?php if(isset($_GET['milkpriceformula_id']) && $v['milkpriceformula_id'] == $_GET['milkpriceformula_id']){ echo 'active'; } ?>" onclick="openUrl('milkprice/test/&amp;milkpriceformula_koperasi=<?php echo $_GET['milkpriceformula_koperasi'];?>&amp;milkpriceformula_id=<?php echo $v['milkpriceformula_id'];?>');" id="tr<?php echo $v['milkpriceformula_id'];?>">
                                <td>
                                    <?php
                                    if($formula['page'] == 1){
                                        echo ($k+1);
                                    }else{
                                        echo ($k + 1 + $formula['limit']);
                                    }
                                    ?>
                                </td>
                                <?php if(in_array(session::get('user_group'),Config::$application['super_group'])){ ?>
                                <td><?php echo $v['koperasi_nama'];?></td>
                                <?php } ?>
                                <td><?php echo $v['milkpriceformula_hargadasar'];?></td>
                                <td class="format-date"><?php echo $v['milkpriceformula_effectivedate'];?></td>
                                <td>
                                    <?php 
                                    $kondisi = array();
                                    foreach($v['milkpricecondition'] as $key=>$val){
                                        if($val['milkpricecondition_element'] == 'cfu'){
                                            $val['milkpricecondition_element'] = 'TPC';
                                        }
                                        $kondisi[] = strtoupper($val['milkpricecondition_element']).' = '.$val['milkpricecondition_standardvalue'];
                                    }
                                    echo implode(' & ',$kondisi);
                                    ?>
                                </td>
                                <td><span class="status"><?php echo $v['milkpriceformula_status'];?></span></td>
                            </tr>
                        <?php } ?>
                    <?php } ?>
                    </tbody>
                </table>
                <?php } ?>
                <?php if(count($formuladetail) > 0){ ?>
                <div class="col-md-12 margin-top-15">
                    <form action="javascript:void(0);" id="calculate-form">
                        <table class="table no-border no-hover" style="width:auto">
                            <tr class="">
                                <th><?php echo _('Milk Price');?></th>
                                <th><strong><?php echo number_format($formuladetail['milkpriceformula_hargadasar']);?></strong></th>
                                <th>Correlation</th>
                                <th>Additional Price</th>
                                <th>interval</th>
                            </tr>
                            <?php foreach($formuladetail['milkpricecondition'] as $k=>$v){ ?>
                            <tr>
                                <td><?php echo $element[$v['milkpricecondition_element']];?></td>
                                <td>
                                    <input type="text" class="numeric" name="milkpricecondition_element[<?php echo $v['milkpricecondition_element'];?>]" value="<?php echo $v['milkpricecondition_standardvalue'];?>">
                                </td>
                                <td>
                                    <?php 
                                    if($v['milkpricecondition_korelasi'] == 'p'){
                                        echo 'Positive';
                                    }else{
                                        echo 'Negative';
                                    }
                                    ?>
                                </td>
                                <td class="text-right">
                                    <?php echo $v['milkpricecondition_addprice'];?>
                                </td>
                                <td class="text-right">
                                    <?php echo $v['milkpricecondition_interval'];?>
                                </td>
                            </tr>
                            <?php } ?>
                            <tr>
                                <td> RESULT:</td>
                                <td colspan="2">
                                    <div class="border padding-10 bg-gray">
                                        <h1 id="calculation-result" class="fg-white font-weight-normal"><?php echo number_format($formuladetail['milkpriceformula_hargadasar'],2);?></h1>
                                    </div>
                                </td>
                                <td></td>
                                <td></td>
                            </tr>
                        </table>
                       
                    </form>
                    <div class="padding-left-30">
                       
                    </div>
                </div>
                <script type="text/javascript">
                    function calculate(){
                        var data = $('#calculate-form').serialize();
                        data = data.replace(/%5B/g,"[");
                        data = data.replace(/%5D/g,"]");
                        $.ajax({
                            url:'<?php echo BASE_URL;?>index.php?/milkprice/calculate/<?php echo $_GET['milkpriceformula_id'];?>',
                            type:'post',
                            data:data,
                            success:function(res){
                                $('#calculation-result').html(res);
                            }
                        });
                    }
                </script>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<div class="padding-50">&nbsp;</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#milkpriceformula_koperasi').on('change',function(){
            var koperasi_id = $(this).val();
            if(koperasi_id != ''){
                window.location.href = '<?php echo BASE_URL;?>index.php?/milkprice/test/&milkpriceformula_koperasi='+koperasi_id;
            }
        });
    });
</script>
<?php View::inc('footer.php');?>
