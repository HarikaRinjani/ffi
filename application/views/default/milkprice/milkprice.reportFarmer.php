<?php View::inc('header.php');?>
<?php View::inc('navbarFarmer.php');?>
<?php View::inc('report/report.menuFarmer.php');?>
<link rel="stylesheet" href="<?php echo BASE_URL;?>themes/default/jquery-tokeninput/styles/token-input-facebook.css"/>
    <div class="content padded">
        <div class="breadcrumb">
            <!-- <ul class="tab">
                
            </ul> -->
            <ul class="tab">
                <li>
                    <a href="">Milk Price Report</a>
                </li>
                <?php foreach($periode['records'][0]['periodedetail'] as $key=>$val){ ?>
                    <li>
                        <a href="javascript:void(0);" class="tab-periode" data-target="#periode<?php echo $val['periodedetail_name'];?>">
                            Periode: <?php echo $val['periodedetail_name'];?>
                        </a>
                    </li>
                <?php } ?>
            </ul>
            <div class="pull-right">
                <ul class="breadcrumb-nav">
                    <li>
                        <a href="">Report</a>
                    </li>
                    <li>
                        <a href="">Milk Price</a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="container-fluid">
            <div class="row">
                <div class="ribbon col-md-12">
                    <div class="pull-right ">
                        <span class="padding-right-10"><i class="ion-ios7-calendar-outline" style="font-size:18px;vertical-align:middle;"></i> Periode:</span>
                        <select name="date1" id="date1" class="month" data-value="<?php echo $_GET['month'];?>"  style="width:auto" onchange="reloadReport();">
                            <option value="">-- loading data --</option>
                        </select>
                        <select name="year" id="year" class="year" data-value="<?php echo $_GET['year'];?>"  style="width:auto" onchange="reloadReport();">
                            <option value="">-- loading data --</option>
                        </select>
                    </div>
                    <div class="clearfix"></div>
                    
                </div>
            </div>
            <div class="row">
            	<!-- <div class="col-md-12">
            		<h2 class="pull-left"><i class="ion-ios7-folder-outline"></i> Milk Price Report</h2>
                    <h3 class="pull-right">
                        <a href="<?php echo BASE_URL; ?>index.php?/report/export/qcparameter/&periode=<?php echo $periode;?>&year=<?php echo $_GET['year'];?>&date1=<?php echo $_GET['date1'];?>&date2=<?php echo $_GET['date2'];?>" title="Synchronize" target="_blank">
                            <i class="ion-ios7-download-outline"></i> export
                        </a>
                    </h3>
            	</div>
                <div class="clearfix"></div> -->
            	<div class="padding-bottom-40 ">
                    <?php foreach($periode['records'][0]['periodedetail'] as $key=>$val){ ?>
                    <table class="table table-hover border table-periode" style="border-left:0;border-right:0;" id="periode<?php echo $val['periodedetail_name'];?>">
                        <thead>
                        <tr>
                            <th class="border-right dotted">Farmer ID</th>
                            <th class="border-right dotted">Name</th>
                            <th class="border-right dotted">TPC</th>
                            <!-- <th class="border-right dotted">IBC</th> -->
                            <th class="border-right dotted">FAT</th>
                            <th class="border-right dotted">Protein</th>
                            <th class="border-right dotted">Lactose</th>
                            <th class="border-right dotted">TS</th>
                            <th class="border-right dotted">FP</th>
                            <th class="border-right dotted">Acidity</th>
                            <th class="border-right dotted">Urea</th>
                            <th class="border-right dotted">SNF</th>
                            <th class="border-right dotted">STD Price</th>
                            <th class="border-right dotted">Rez Penalty</th>
                            <th class="border-right dotted">FP Penalty</th>
                            <th>Milk Price</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if($data['total_data'] == 0){ ?>
                            <tr>
                                <td colspan="100%"><?php echo _('No record found');?></td>
                            </tr>
                        <?php }else{ ?>
                            <?php foreach($data['records'] as $k=>$v){ ?>
                                <tr class="tr_selector cursor-pointer" data-id="<?php echo $v[$val['periodedetail_name']]['anggota_id'];?>" id="tr<?php echo $v['anggota_id'];?>">
                                    <td class="border-right dotted"><?php echo $v[$val['periodedetail_name']]['anggota_nomor'];?></td>
                                    <td class="border-right dotted"><?php echo $v[$val['periodedetail_name']]['anggota_nama'];?></td>
                                    <td class="border-right dotted"><?php echo (float) $v[$val['periodedetail_name']]['qcparameter_cfu'];?></td>
                                    
                                    <!-- <td class="border-right dotted"><?php echo (float) $v[$val['periodedetail_name']]['qcparameter_ibc'];?></td> -->
                                    <td class="border-right dotted"><?php echo (float) $v[$val['periodedetail_name']]['qcparameter_fat'];?></td>
                                    <td class="border-right dotted"><?php echo (float) $v[$val['periodedetail_name']]['qcparameter_protein'];?></td>
                                    <td class="border-right dotted"><?php echo (float) $v[$val['periodedetail_name']]['qcparameter_lactose'];?></td>
                                    <td class="border-right dotted"><?php echo (float) $v[$val['periodedetail_name']]['qcparameter_ts'];?></td>
                                    <td class="border-right dotted"><?php echo (float) $v[$val['periodedetail_name']]['qcparameter_fp'];?></td>
                                    <td class="border-right dotted"><?php echo (float) $v[$val['periodedetail_name']]['qcparameter_acidity'];?></td>
                                    <td class="border-right dotted"><?php echo (float) $v[$val['periodedetail_name']]['qcparameter_urea'];?></td>
                                    <td class="border-right dotted"><?php echo (float) $v[$val['periodedetail_name']]['qcparameter_snf'];?></td>
                                    <td class="border-right dotted"><?php echo number_format($v[$val['periodedetail_name']]['hargadasar']);?></td>
                                    <td class="border-right dotted"><?php echo number_format($v[$val['periodedetail_name']]['hargarez']);?></td>
                                    <td class="border-right dotted"><?php echo number_format($v[$val['periodedetail_name']]['hargafp']);?></td>
                                    <?php $milkprice = $v[$val['periodedetail_name']]['hargadasar']+ $v[$val['periodedetail_name']]['hargarez'] + $v[$val['periodedetail_name']]['hargafp'];?>
                                    <td class=""><?php echo number_format($milkprice);?></td>
                                </tr>
                            <?php } ?>
                        <?php } ?>
                        </tbody>
                    </table>
                    <?php } ?>
                    <div class="grid-pagging border-top margin-bottom-30"></div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="<?php echo BASE_URL;?>themes/default/jquery-tokeninput/src/jquery.tokeninput.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.tab-selector').on('click',function(){
                var id = $(this).attr('data-id');
                $('.tab-selector:not(.fg-lightgray)').addClass('fg-lightgray');
                $(this).removeClass('fg-lightgray');
                $('.chartContainer').addClass('hidden');
                $('#chartContainer'+id).removeClass('hidden');
                var chartnya = $('#widget_transaksi_chart'+id).highcharts();
                chartnya.setSize($('.chartContainer').width(),$('.chartContainer').height(),true);
            });
            $('.filter-select').click(function(){
                var id = $(this).attr('data-href');
                $('.filter-select').removeClass('fg-lightgray');
                $('.filter-select').not(this).addClass('fg-lightgray');
                $('.filter-container').not(id).hide();
                $(id).show();
            });
            

            $('#inputfilter-angggota').tokenInput('<?php echo BASE_URL;?>index.php?/load/autocompleteanggota/'+$('#inputfilter-koperasi').val(),{
                'placeholder': 'farmer: ',
                method:'POST',
                tokenLimit:1,
                theme: "facebook",
                prePopulate:<?php echo json_encode($prepopulate);?>
            });
            
            //console.log(arrayURL[1]);
            var currentTab = $.cookie('tabPeriodeSelected');
            if(typeof currentTab === 'undefined' || currentTab === ''){
                $('ul.tab').find('li').removeClass('active');
                var linkelement = $('.tab-periode[data-target=#periode1]');
                linkelement.parent().addClass('active');
                $('.table-periode').hide();
                $('#periode1').show();
            }else{
                $('ul.tab').find('li').removeClass('active');
                var linkelement = $('.tab-periode[data-target='+currentTab+']');
                linkelement.parent().addClass('active');
                $('.table-periode').hide();
                $(currentTab).show();
            }
            $('.tab-periode').on('click',function(){
                var tabtarget = $(this).attr('data-target');
                $.cookie('tabPeriodeSelected',tabtarget);
                $('ul.tab').find('li').removeClass('active');
                $(this).parent().addClass('active');
                $('.table-periode').hide();
                $(tabtarget).show();
            });
            $('.grid-pagging').transpaginator({
                totalPages: <?php echo $data['total_page'];?>,
                currentPage: <?php echo $data['page'];?>,
                pageUrl: '<?php echo BASE_URL;?>index.php?/milkprice/reportFarmer/&koperasi=<?php echo $_GET['koperasi'];?>&anggota=<?php echo $_GET['anggota'];?>&periode=<?php echo $_GET['periode'];?>&year=<?php echo $_GET['year'];?>&month=<?php echo $_GET['month'];?>&page='
            });
            
        });
        function reloadFilter(){
            var koperasi = $('#inputfilter-koperasi').val();
            var anggota = $('#inputfilter-angggota').val();
            var date1 = $('#date1').val();
            var year = $('#year').val();
            openUrl('milkprice/reportFarmer/&koperasi='+koperasi+'&anggota='+anggota+'&periode=<?php echo $_GET['periode'];?>&year='+year+'&month='+date1);
        }
        function reloadReport(){
            var date1 = $('#date1').val();
            var year = $('#year').val();
            openUrl('milkprice/reportFarmer/&koperasi=<?php echo $_GET['koperasi'];?>&anggota=<?php echo $_GET['anggota'];?>&periode=<?php echo $_GET['periode'];?>&year='+year+'&month='+date1);
        }

    </script>
<?php View::inc('footer.php');?>