<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('settings/settings.menu.php');?>

<div class="content padded">
    <div class="breadcrumb">
        <ul class="tab">
            <li>
                <a href="<?php echo BASE_URL;?>index.php?/milkprice/add">new</a>
            </li>
            <li class="active">
                <a href="<?php echo BASE_URL;?>index.php?/milkprice/index"  disabled="disabled">formula</a>
            </li>
            <li>
                <a href="<?php echo BASE_URL;?>index.php?/milkprice/test">test</a>
            </li>
        </ul>
        <div class="pull-right">
            <ul class="breadcrumb-nav">
                <li>
                    <a href="">Apps Settings</a>
                </li>
                <li>
                    <a href="">Milkprice Formula</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row padding-bottom-40">
            <div class="ribbon border-bottom">
                <div class="menu">
                    <a href="javascript:void(0);" id="__edit" class="fg-lightgray editbtn"><i class="ion-ios7-compose-outline"></i>edit</a>
                    <a href="javascript:void(0);" id="__delete" class="fg-lightgray editbtn"><i class="ion-ios7-trash-outline"></i>delete</a>
                </div>
                
            </div>

            <table class="table table-hover">
                <thead>
                <tr>
                    <th style="width:24px;">#</th>
                    <?php if(in_array(session::get('user_group'),Config::$application['super_group'])){ ?>
                    <th><?php echo _('Cooperative');?></th>
                    <?php } ?>
                    <th><?php echo _('Milk Price');?></th>
                    <th><?php echo _('Effective Date');?></th>
                    <th><?php echo _('Condition');?></th>
                    <th><?php echo _('Status');?></th>
                </tr>
                </thead>
                <tbody>
                <?php if($formula['total_data'] == 0){ ?>
                    <tr>
                        <td colspan="100%"><?php echo _('No record found');?></td>
                    </tr>
                <?php }else{ ?>
                    <?php foreach($formula['records'] as $k=>$v){ ?>
                        <tr class="tr_selector cursor-pointer" data-id="<?php echo $v['milkpriceformula_id'];?>" id="tr<?php echo $v['milkpriceformula_id'];?>">
                            <td>
                                <?php
                                if($formula['page'] == 1){
                                    echo ($k+1);
                                }else{
                                    echo ($k + 1 + $formula['limit']);
                                }
                                ?>
                            </td>
                            <?php if(in_array(session::get('user_group'),Config::$application['super_group'])){ ?>
                            <td><?php echo $v['koperasi_nama'];?></td>
                            <?php } ?>
                            <td><?php echo $v['milkpriceformula_hargadasar'];?></td>
                            <td class="format-date"><?php echo $v['milkpriceformula_effectivedate'];?></td>
                            <td>
                                <?php 
                                $kondisi = array();
                                foreach($v['milkpricecondition'] as $key=>$val){
                                    if($val['milkpricecondition_element'] == 'cfu'){
                                        $val['milkpricecondition_element'] = 'TPC';
                                    }
                                    $kondisi[] = strtoupper($val['milkpricecondition_element']).' = '.$val['milkpricecondition_standardvalue'];
                                }
                                echo implode(' & ',$kondisi);
                                ?>
                            </td>
                            <td><span class="status"><?php echo $v['milkpriceformula_status'];?></span></td>
                        </tr>
                    <?php } ?>
                <?php } ?>
                </tbody>
            </table>
            <div class="grid-pagging border-top margin-bottom-30"></div>
        </div>
    </div>
</div>
<div class="padding-50">&nbsp;</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.grid-pagging').transpaginator({
            totalPages: <?php echo $formula['total_page'];?>,
            totalData: <?php echo $formula['total_data'];?>,
            currentPage: <?php echo $formula['page'];?>,
            pageUrl: '<?php echo BASE_URL;?>index.php?/milkprice/index/&keyword=<?php echo $formula['keyword'];?>&page='
        });
        $('.tr_selector').on('click',function(){
            var id = $(this).attr('data-id');
            $('.editbtn').attr('data-id',id).removeClass('fg-lightgray');
        });
        $('#__edit').on('click',function(){
            var id = $(this).attr('data-id');
            location.href = '<?php echo BASE_URL;?>index.php?/milkprice/edit/'+id;
        });
    });
</script>
<?php View::inc('footer.php');?>
