<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('settings/settings.menu.php');?>

<div class="content padded">
    <div class="breadcrumb">
        <ul class="tab">
            <li  class="active">
                <a href="<?php echo BASE_URL;?>index.php?/milkprice/new" disabled="disabled">new</a>
            </li>
            <li>
                <a href="<?php echo BASE_URL;?>index.php?/milkprice/index">formula</a>
            </li>
            <li>
                <a href="<?php echo BASE_URL;?>index.php?/milkprice/test">Test</a>
            </li>
        </ul>
        <div class="pull-right">
            <ul class="breadcrumb-nav">
                <li>
                    <a href="">Apps Settings</a>
                </li>
                <li>
                    <a href="">Milkprice Formula</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row padding-bottom-40">
            <div class="ribbon border-bottom">
                <div class="menu">
                    <a href="javascript:void(0);" onclick="$('#milkprice-save-btn').trigger('click');"><i class="ion-ios7-save"></i>save</a>
                    <a href="javascript:void(0);"><i class="ion-ios7-help-outline"></i>help</a>
                </div>
                
            </div>
            <div class="col-md-7  margin-top-25">
                <form action="<?php echo BASE_URL;?>index.php?/milkprice/save" method="post">
                    <table class="table no-border no-hover">
                        <tr>
                            <td tyle="width:100px;">
                                <label for="milkprice_koperasi"><?php echo _('Cooperative');?></label>
                            </td>
                            <td>
                                <select name="milkpriceformula_koperasi" id="milkprice_koperasi" class="koperasi"></select>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="milkprice_hargadasar"><?php echo _('Milk Price');?> (<?php echo _('Standard Price');?>)</label>
                            </td>
                            <td>
                                <input type="text" name="milkpriceformula_hargadasar" id="milkprice_hargadasar" class="number margin-top-5" required>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="milkprice_effectivedate"><?php echo _('Effective Date');?></label>
                            </td>
                            <td>
                                <input type="text" id="milkpriceformula_effectivedate" name="milkpriceformula_effectivedate" class="datepicker margin-top-5" required>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <label for="kondisi"><h3><?php echo _('Condition');?></h3></label>
                            </td>
                        </tr>
                    </table>
                    <table class="table no-border no-hover">
                        <thead>
                            <th>Element</th>
                            <th>Standard Value</th>
                            <th>Additional Price</th>
                            <th>Interval</th>
                            <th>Corellation</th>
                        </thead>
                        <tbody>
                            <?php foreach($element as $k=>$v){ ?>
                            <tr>
                                <td>
                                    <input type="checkbox" name="milkpricecondition_element[<?php echo $k;?>]" value="<?php echo $k;?>" id="condition_<?php echo $k;?>">
                                    <label for="condition_<?php echo $k;?>"><?php echo $v;?></label>
                                </td>
                                <td>
                                    <input type="text" class="numeric input-block" name="milkpricecondition_standardvalue[<?php echo $k;?>]" />
                                </td>
                               <td>
                                   <input type="text" class="numeric input-block" name="milkpricecondition_addprice[<?php echo $k;?>]" />
                               </td>
                               <td>
                                   <input type="text" class="numeric input-block" name="milkpricecondition_interval[<?php echo $k;?>]" />
                               </td>
                               <td>
                                   <select name="milkpricecondition_korelasi[<?php echo $k;?>]" id="">
                                        <option value="p">Positive</option>
                                        <option value="n">Negative</option>
                                   </select>
                               </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                    <button class="" type="submit" id="milkprice-save-btn" style="visibility:hidden;width:1px;height:1px;"></button>
                </form>
            </div>
            <div class="col-md-6  margin-top-25">

            </div>
        </div>
    </div>
</div>
<div class="padding-50">&nbsp;</div>
<script type="text/javascript">
    $(document).ready(function(){

    });
</script>
<?php View::inc('footer.php');?>
