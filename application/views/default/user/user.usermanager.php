<?php View::inc('header.php');?>
<?php View::inc('navbar.php');?>
<?php View::inc('system/system.menu.php');?>

    <div class="content padded">
        <div class="breadcrumb">
            <ul class="tab">
                <li>
                    <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-add-user">new user</a>
                </li>
                <li class="active">
                    <a href="">user lists</a>
                </li>
            </ul>
            <div class="pull-right">
                <ul class="breadcrumb-nav">
                    <li>
                        <a href="">System Settings</a>
                    </li>
                    <li>
                        <a href="">User Manager</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row ">
                <div class="ribbon text-right">
                    <div class="menu">
                        <a href="javascript:void(0:" id="edituser" disabled="disabled" class="fg-lightgray"><i class="ion-ios7-compose-outline"></i>edit user</a>
                    </div>
                    <form action="javascript:void(0);" id="searchForm" onsubmit="return filter();">
                        <?php if(!in_array(session::get('user_group'),Config::$application['super_group'])){ ?>
                        <input type="hidden" name="koperasi" id="koperasi" value="<?php echo session::get('user_koperasi');?>">
                        <?php }else{ ?>
                        <select name="koperasi" id="koperasi" onchange="return filter();">
                            <option value="">All</option>
                            <option value="none" <?php if($_GET['koperasi'] === 'none'){ echo 'selected="selected"'; } ?>>Administrator / FFI</option>
                            <?php foreach($koperasi['records'] as $k=>$v){ ?>
                                <option value="<?php echo $v['koperasi_id'];?>" <?php if($v['koperasi_id'] === $_GET['koperasi']){ echo 'selected="selected"'; } ?>><?php echo $v['koperasi_nama'];?></option>
                            <?php } ?>
                        </select>
                        <?php } ?>
                        <input type="text" placeholder="Search user..." id="keyword" style="width:300px" value="<?php echo $user['keyword'];?>">
                    </form>
                </div>
                <table class="table border dotted stripped" style="border:none;">
                    <tr>
                        <th>Username</th>
                        <th>Fullname</th>
                        <th>Email</th>
                        <th>Group</th>
                        <th>Last Login</th>
                        <th>Status</th>
                    </tr>
                    <?php if($user['total_data'] === 0){ ?>
                        <tr>
                            <td colspan="5"><?php echo _('No record found');?></td>
                        </tr>
                    <?php } ?>
                    <?php foreach($user['records'] as $k=>$v){ 
                        if($v['user_avatar'] == '' || $v['user_avatar'] == null){
                            $photo = 'index.php?/load/file/'.urlencode('images/avatar.jpg');
                        }else{
							if (strpos($v['user_avatar'], 'data:image') !== false) {
								$photo = $v['user_avatar'];
							} else {
								$photo = 'index.php?/load/file/'.urlencode($v['user_avatar']);
							}
                            
                        }
                        ?>
                        <tr data-id="<?php echo $v['user_id'];?>" class="tr_selector">
                            <td><img src="<?php echo $photo; ?>" alt="" style="float:left;margin-right:5px;width:28px;height:28px;"/><?php echo $v['user_name'];?></td>
                            <td><?php echo $v['user_fullname'];?></td>
                            <td><?php echo $v['user_email'];?></td>
                            <td><?php echo Config::$application['user_group'][$v['user_group']];?></td>
                            <td class="format-datetime"><?php echo $v['user_lastactivity'];?></td>
                            <td class="status"><?php echo $v['user_status'];?></td>
                        </tr>
                    <?php } ?>
                </table>
                <div class="grid-pagging border-top margin-bottom-30"></div>
            </div>
        </div>
    </div>
<div class="modal draggable" id="modal-add-user">
    <div class="modal-dialog">
        <div class="modal-header">
            <h3 class="modal-title">add new user</h3>
            <a href="javascript:void(0);" class="close">&times;</a>
        </div>
        <div class="modal-body">
            <form method="post" action="<?php echo BASE_URL; ?>index.php?/user/saveuser" enctype="multipart/form-data">
                <div class="form-group">
                    <div class="col-md-4">
                        <label for="add-user_name">Username</label>
                    </div>
                    <div class="col-md-8">
                        <input type="text" name="user_name" id="add-user_name" class="input-block" required=""/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4">
                        <label for="add-user_password">Password</label>
                    </div>
                    <div class="col-md-8">
                        <input type="password" name="user_password" id="add-user_password" class="input-block" required=""/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4">
                        <label for="add-user_fullname">Full Name</label>
                    </div>
                    <div class="col-md-8">
                        <input type="text" class="input-block" name="user_fullname" id="add-user_fullname" required=""/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4">
                        <label for="add-user_email">Email Address</label>
                    </div>
                    <div class="col-md-8">
                        <input type="text" class="input-block" name="user_email" id="add-user_email" required=""/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4">
                        <label for="add-user_koperasi">Koperasi</label>
                    </div>
                    <div class="col-md-8">
                        <select class="input-block" id="add-user_koperasi" name="user_koperasi">
                            <option value="none">---</option>
                            <?php foreach($koperasi['records'] as $k=>$v){ ?>
                                <option value="<?php echo $v['koperasi_id'];?>"><?php echo $v['koperasi_nama'];?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4">
                        <label for="add-user_group">Group</label>
                    </div>
                    <div class="col-md-8">
                        <select name="user_group" id="add-user_group">
                            <?php foreach(Config::$application['user_group'] as $k=>$v){ ?>
                                <option value="<?php echo $k; ?>"><?php echo $v;?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-4">
                        <label for="add-user_avatar">Upload a photo</label>
                    </div>
                    <div class="col-md-8">
                        <input type="file" class="input-block" name="user_avatar" id="add-user_avatar"/>
                    </div>
                </div>
                <div class="text-right padding-top-15">
                    <button type="button" data-dismiss="modal">cancel</button>
                    <button class="default" type="submit">save</button>
                </div>
            </form>
        </div>
    </div>
</div>
    <div class="modal draggable" id="modal-edit-user">
        <div class="modal-dialog">
            <div class="modal-header">
                <h3 class="modal-title">edit user</h3>
                <a href="javascript:void(0);" class="close">&times;</a>
            </div>
            <div class="modal-body">
                <form method="post" action="<?php echo BASE_URL; ?>index.php?/user/saveuser" enctype="multipart/form-data">
                    <input name="user_id" type="hidden"/>
                    <div class="form-group">
                        <div class="col-md-4">
                            <label for="edit-user_name">Username</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" name="user_name" id="edit-user_name" class="input-block" readonly="readonly" required=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4">
                            <label for="edit-user_password">Password</label>
                        </div>
                        <div class="col-md-8">
                            <input type="password" name="user_password" id="edit-user_password" class="input-block" />
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4">
                            <label for="edit-user_fullname">Full Name</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" class="input-block" name="user_fullname" id="edit-user_fullname" required=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4">
                            <label for="edit-user_email">Email Address</label>
                        </div>
                        <div class="col-md-8">
                            <input type="text" class="input-block" name="user_email" id="edit-user_email" required=""/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4">
                            <label for="edit-user_koperasi">Koperasi</label>
                        </div>
                        <div class="col-md-8">
                            <select class="input-block" id="edit-user_koperasi" name="user_koperasi">
                                <option value="none">---</option>
                                <?php foreach($koperasi['records'] as $k=>$v){ ?>
                                    <option value="<?php echo $v['koperasi_id'];?>"><?php echo $v['koperasi_nama'];?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4">
                            <label for="edit-user_group">Group</label>
                        </div>
                        <div class="col-md-8">
                            <select name="user_group" id="edit-user_group">
                                <?php foreach(Config::$application['user_group'] as $k=>$v){ ?>
                                    <option value="<?php echo $k; ?>"><?php echo $v;?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-4">
                            <label for="edit-user_avatar">Upload a photo</label>
                        </div>
                        <div class="col-md-8">
                            <input type="file" class="input-block" name="user_avatar" id="edit-user_avatar"/>
                        </div>
                    </div>
                    <div class="text-right padding-top-15">
                        <button type="button" data-dismiss="modal">cancel</button>
                        <button class="default" type="submit">save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.grid-pagging').transpaginator({
                totalPages: <?php echo $user['total_page'];?>,
                currentPage: <?php echo $user['page'];?>,
                pageUrl: '<?php echo BASE_URL;?>index.php?/user/usermanager/&koperasi=<?php echo $_GET['koperasi'];?>&keyword=<?php echo $user['keyword'];?>&page='
            });
            $('.tr_selector').on('click',function(){
                var id = $(this).attr('data-id');
                $('#edituser').removeAttr('disabled').attr('data-id',id).removeClass('fg-lightgray');
                $('.tr_selector').removeClass('active');
                $(this).addClass('active');
            });
            $('#edituser').on('click',function(){
                var disabled = $(this).attr('disabled');
                var id = $(this).attr('data-id');
                var editfield = ['user_id','user_name','user_fullname','user_email','user_group','user_koperasi'];
                if(typeof disabled == '' || typeof disabled == 'undefined'){
                    $.ajax({
                        url:'<?php echo BASE_URL;?>index.php?/system/jsondata/user/'+id,
                        type:'get',
                        dataType:'json',
                        success:function(res){
                            for(var k in res){
                                if(editfield.indexOf(k) !== -1){
                                    if(k == 'user_group' || k == 'user_koperasi'){
                                        $('#modal-edit-user').find('select[name='+k+']').val(res[k]);
                                    }else{
                                        $('#modal-edit-user').find('input[name='+k+']').val(res[k]);
                                    }
                                }
                            }
                            $('#modal-edit-user').addClass('fadein');
                        }
                    });
                }

            });
        });

        function filter(){
            var keyword = $('#keyword').val();
            var koperasi = $('#koperasi').val();
            window.location.href = '<?php echo BASE_URL;?>index.php?/user/usermanager/&koperasi='+koperasi+'&keyword='+keyword+'&page=1';
        }
    </script>
<?php View::inc('footer.php');?>