<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 22/01/2015
 * Time: 8:45
 */
class tcurl{
    static function request($url='',$params=array()){
        $ch = curl_init();
        //var_dump($ch);exit();
        //echo $url;exit();
        $datanya = array();
        foreach($params as $k=>$v){
            $datanya[] = $k.'='.urlencode($v);
        }
        $post_data = implode('&',$datanya);
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        //curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
        //curl_setopt($ch, CURLOPT_PORT , 8089);
        curl_setopt($ch, CURLOPT_TIMEOUT, 15);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);

        $output=curl_exec($ch);
        //var_dump($output);exit();
        curl_close($ch);
        $res =  json_decode($output,true);
        if (is_object($res)) {
            $res = get_object_vars($res);
        }

        return $res;
    }
    public static function escapeKeyword($string){
        return preg_replace('/[^a-zA-Z0-9\s]/', '', $string);
    }
}