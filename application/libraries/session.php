<?php
/**
 * (c)2013 Transformatika
 * User: ag <agung@transformatika.com>
 * Date: 6/14/13
 * Time: 3:26 PM
 *
 */
class session{
    public static function start($session_name=''){
        if($session_name == ''){
            $session_name = 'TransformatikaMVC';
        }
        session_name($session_name);
        session_start();
    }
    public static function destroy(){
    	if (isset($_SERVER['HTTP_COOKIE'])) {
			$cookies = explode(';', $_SERVER['HTTP_COOKIE']);
			foreach ($cookies as $cookie) {
				$parts = explode('=', $cookie);
				$name  = trim($parts[0]);
				setcookie($name, '', time() - 1000);
				setcookie($name, '', time() - 1000, '/');
			}
		}
        session_destroy();
    }
    public static function check($key){
        if(isset($_SESSION[$key]) && !empty($_SESSION[$key])){
            return true;
        }else{
            return false;
        }
    }
    public static function set($key,$value){
        if(Config::$application['encrypt_session'] === true){
            load::library('security');
            if(empty($value)){
                $_SESSION[$key] = '';
            }else{
                $_SESSION[$key] = security::encrypt($value);
            }
        }else{
            $_SESSION[$key] = $value;
        }
    }
    public static function remove($key){
        unset($_SESSION[$key]);
    }
    public static function delete($key){
        unset($_SESSION[$key]);
    }
    public static function get($key){
        if(Config::$application['encrypt_session'] === true){
            load::library('security');
            if(isset($_SESSION[$key])){
                return security::decrypt($_SESSION[$key]);
            }

        }else{
            if(isset($_SESSION[$key])){
                return $_SESSION[$key];
            }
        }
    }

}
