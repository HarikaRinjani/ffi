<?php
if ( strpos(strtolower($_SERVER['SCRIPT_NAME']),strtolower(basename(__FILE__))) ){ die(header('HTTP/1.0 403 Forbidden')); }
class security{
    
    private static $unlocker = "transformatikahmalif31ck3okebanget";
    private static $salt = "123asdfasdij409sdfj12-0sdfgj";
    private static $idle_time = 1440;

    static function checkSession($session_key='user_id'){  
        if(!session::check($session_key)){
            if(isset($_COOKIE['user_id']) && isset($_COOKIE['user_token'])){
                $user_id = security::decrypt($_COOKIE['user_id']);
                $user_token = security::decrypt($_COOKIE['user_token']);
                $user = load::model('user');
                $user->setCondition('user_id = \''.$user_id.'\' AND user_status = \'y\'');
                $user->setLimit(1);
                $dtUser = $user->read();
                if($dtUser['total_data'] > 0){
                    $avatar = BASE_URL.'themes/default/images/avatar.jpg';
                    if(strlen($dtUser['records'][0]['user_avatar']) > 3){
                        $avatar = Config::$application['file_url'].rawurlencode($dtUser['records'][0]['user_avatar']);
                    }
                    $user_token = SHA1(session_id());
                    if(strlen($dtUser['records'][0]['user_theme']) > 2){
                        session::set('user_theme',$dtUser['records'][0]['user_theme']);
                    }else{
                        session::set('user_theme','blue');
                    }
                    session::set('user_id',$dtUser['records'][0]['user_id']);
                    session::set('user_name',$dtUser['records'][0]['user_name']);
                    session::set('user_fullname',$dtUser['records'][0]['user_fullname']);
                    session::set('user_email',$dtUser['records'][0]['user_email']);
                    session::set('user_id',$dtUser['records'][0]['user_id']);
                    session::set('user_avatar',$avatar);
                    session::set('user_su',$dtUser['records'][0]['user_su']);
                    session::set('user_group',$dtUser['records'][0]['user_group']);
                    session::set('user_token',$user_token);
                    session::set('user_firstuse',$dtUser['records'][0]['user_firstuse']);
                    session::set('user_role',$dtUser['records'][0]['usertype_role']);
                    session::set('user_koperasi',$dtUser['records'][0]['user_koperasi']);
                    session::set('user_koperasi_nama',$dtUser['records'][0]['koperasi_nama']);
                    session::set('user_koperasi_logo',$dtUser['records'][0]['koperasi_logo']);
                    session::set('user_koperasi_telp',$dtUser['records'][0]['koperasi_telp']);
                    session::set('user_koperasi_alamat',$dtUser['records'][0]['koperasi_alamat']);
                    session::set('user_koperasi_kode',$dtUser['records'][0]['koperasi_kode']);
                    setcookie('user_token',$_COOKIE['user_token'],strtotime('+30 days'));
                    
                    $last_activity = date('Y-m-d H:i:s');
                    $user->setId($dtUser['records'][0]['user_id']);
                    $user->setData('user_lastactivity',$last_activity);
                    $user->update();
                    session::set('user_lastactivity',$last_activity);
                    return true;
                }else{
                	session::destroy();
                    if(isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER'])){
                        header('location:'.$_SERVER['HTTP_REFERER']);
                        exit();
                    }else{
                        header('location:'.BASE_URL);
                        exit();
                    }
                }
            }else{
                if(isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER'])){
                    header('location:'.$_SERVER['HTTP_REFERER']);
                    exit();
                }else{
                    header('location:'.BASE_URL);
                    exit();
                }
            }
        }else{
            load::library('fn');
            $controller = session::get('controller');
            $method = session::get('method');

            /**
            *   disable logging for some controller
            */
            $disabledlog_controller = array(
                'load',
                'bookmark',
                'login',
                'dashboard',
                'synchandler',
            	'sync'
                );
            if(!in_array($controller,$disabledlog_controller)){
                $log = session::get('user_name') . ' Accessing:  '.$controller.'/'.$method ;
                fn::log($log);
            }
            if(session::get('user_group') == 'administrator'){
                $last_activity = date('Y-m-d H:i:s');
                session::set('user_lastactivity',$last_activity);
            }else{
                if(!in_array($controller,Config::$application['unused_package'])){
                    if(strpos(session::get('user_role'),$controller .'-'. $method) !== false){
                        $last_activity = date('Y-m-d H:i:s');
                        session::set('user_lastactivity',$last_activity);
                    }else{
                        session::set('errorMsg','You dont have permission to access this page!');
                        if(isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER'])){
                            header('location:'.$_SERVER['HTTP_REFERER']);
                            exit();
                        }else{
                            header('location:'.BASE_URL);
                            exit();
                        }
                    }
                }else{
                    $last_activity = date('Y-m-d H:i:s');
                    session::set('user_lastactivity',$last_activity);
                }
            }
            
        }
    }

    public static function hostInit(){
        $client_addr = $_SERVER['REMOTE_ADDR'];
        if(!in_array($client_addr,Config::$application['denied_host'])){
            return true;
        }else{
            header($_SERVER["SERVER_PROTOCOL"]." 403 Access Denied");
            header("Status: 403 Access Denied");
            $_SERVER['REDIRECT_STATUS'] = 403;
            echo "<html><head><title>ERROR #403 - Access Denied</title></head>";
            echo "<body style='background: #a20025;text-align:center;padding:50px;font-family: \"Helvetica Neue\", Helvetica, Arial, sans-serif;font-size:14px;color:white;'>";
            echo "<h1 style='font-weight:300;font-size:60px;'>403 Access Denied</h1>";
            echo "Sorry. You dont have permission to access this page!";
            echo "<p><small>&copy; Transformatika MVC Framework</small></p>";
            echo "</body>";
            echo "</html>";
            exit();
        }
    }
    static function require_role($role='',$redirect=true){
        if(!session::check('user_id')){
            header('location:'.BASE_URL.'index.php?/'.Config::App('default_controller'));
            exit();
        }else{
            if(session::get('user_su') === 'y'){
                return true;
            }else{
                if($role == ''){
                    session::set('errorMsg','Invalid Role Access');
                    if($redirect === false){
                        return false;
                    }else{
                        if(isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER'])){
                            header('location:'.$_SERVER['HTTP_REFERER']);
                            exit();
                        }else{
                            header('location:'.BASE_URL);
                            exit();
                        }
                    }

                }else{
                    if(is_array($role)){
                        if(in_array(session::get('user_group'),$role)){
                            return true;
                        }else{
                            session::set('errorMsg',_('You dont have permissions to access this page'));
                            if($redirect === false){
                                return false;
                            }else{
                                if(isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER'])){
                                    header('location:'.$_SERVER['HTTP_REFERER']);
                                    exit();
                                }else{
                                    header('location:'.BASE_URL);
                                    exit();
                                }
                            }
                        }
                    }else{
                        if($role == session::get('user_group')){
                            return true;
                        }else{
                            if($redirect === false){
                                return false;
                            }else{
                                if(isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER'])){
                                    header('location:'.$_SERVER['HTTP_REFERER']);
                                    exit();
                                }else{
                                    header('location:'.BASE_URL);
                                    exit();
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static function encrypt($password = "") {
        if((self::$unlocker == "") || (self::$salt == "")){
            die("<strong>ERROR: </strong>Please initialize the class");
        }else{
            if($password == ""){
                return "";
            }else{
                $key = hash('SHA256', self::$salt . self::$unlocker, true);
                srand();
                $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC), MCRYPT_RAND);
                if (strlen($iv_base64 = rtrim(base64_encode($iv), '=')) != 22) return false;
                $encrypted = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $password . md5($password), MCRYPT_MODE_CBC, $iv));
                return $iv_base64 . $encrypted;
            }
        }
    }

    public static function decrypt($password = "") {
        if((self::$unlocker == "") || (self::$salt == "")){
            //die("<strong>ERROR: </strong>Please initialize the class");
            return 'ERROR: Please initialize the class';
        }
        if($password == ""){
            //die("<strong>ERROR: </strong>No password to decrypt");
            return '';
        }
        $key = hash('SHA256', self::$salt . self::$unlocker, true);
        $iv = base64_decode(substr($password, 0, 22) . '==');
        $password = substr($password, 22);
        $decrypted = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, base64_decode($password), MCRYPT_MODE_CBC, $iv), "\0\4");
        $hash = substr($decrypted, -32);
        $decrypted = substr($decrypted, 0, -32);
        if (md5($decrypted) != $hash) return false;
        return $decrypted;
    }

    public static function checkLisence(){
        $server_addr = self::getServerAddress();

        $license = file_get_contents(path('base').DS.'license.ini');
        $decrypt = self::decrypt($license);
        $dt = explode('____',$decrypt);
        $private_key = $dt[0];
        $company_name = $dt[1];
        $host_ip = $dt[2];
        if($company_name != Config::App('company_name')){
            header('location:activate.php');
            exit();
        }
        if($host_ip != $server_addr){
            header('location:activate.php');
            exit();
        }
        return true;
    }

    private static function getServerAddress() {
        if(array_key_exists('SERVER_ADDR', $_SERVER))
            return $_SERVER['SERVER_ADDR'];
        elseif(array_key_exists('LOCAL_ADDR', $_SERVER))
            return $_SERVER['LOCAL_ADDR'];
        elseif(array_key_exists('SERVER_NAME', $_SERVER))
            return gethostbyname($_SERVER['SERVER_NAME']);
        else {
            // Running CLI
            if(stristr(PHP_OS, 'WIN')) {
                return gethostbyname(php_uname("n"));
            } else {
                $ifconfig = shell_exec('/sbin/ifconfig eth0');
                preg_match('/addr:([\d\.]+)/', $ifconfig, $match);
                return $match[1];
            }
        }
    }
}