<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 12/27/2014
 * Time: 10:54 AM
 */
class client{
    static function check(){
        $clientFile = path('base').DS.'client.ini';
        $clientID = file_get_contents($clientFile);
        if(empty($clientID)){
            die('client.ini not configured properly.');
        }
        $clientHDUUID = security::decrypt($clientID);
        $currentHDUUID = self::HDUUID();
        if($clientHDUUID != $currentHDUUID){
            $tmp = path('base').DS.'client.error.txt';
            if(is_file($tmp)){
                unlink($tmp);
            }
            $log = "Hardware changed!\n";
            $log .= "DETAIL INFORMATION:\n";
            $log .= "Date: ".date('Y-m-d H:i:s')."\n";
            $log .= "Client Info: ".security::encrypt($currentHDUUID);
            file_put_contents($tmp,$log);
            die('Hardware change detected!. please contact Transformatika Support to get new client configuration. Send <a href="'.BASE_URL.'client.error.txt" download="client.error.txt" target="_blank">this file</a> to Transformatika support team');
        }
    }
    static function HDUUID() {
        if(session::check('client_uuid')){
            $uuidnya = session::get('client_uuid');
            return security::decrypt($uuidnya);
        }else{
            if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {

                $temp = sys_get_temp_dir().DIRECTORY_SEPARATOR."diskpartscript.txt";
                if(!file_exists($temp) && !is_file($temp)) file_put_contents($temp, "select disk 0\ndetail disk");
                $output = shell_exec("diskpart /s ".$temp);
                $lines = explode("\n",$output);
                $result = array_filter($lines,function($line) {
                    return stripos($line,"ID:")!==false;
                });
                if(count($result)>0) {
                    $result = array_shift(array_values($result));
                    $result = explode(":",$result);
                    $result = trim(end($result));
                } else $result = $output;
            } else {
                $result = shell_exec("blkid -o value -s UUID");
                if(!empty($result)){
                    if(stripos($result,"blkid")!==false) {
                        $result = $_SERVER['HTTP_HOST'];
                    }
                }else{
                    $ceklagi = shell_exec("ls /dev/disk/by-uuid/");
                    $explodeCek = explode(' ',trim($ceklagi));
                    $result = $explodeCek[0];
                }
            }
            session::set('client_uuid',security::encrypt($result));
            return $result;
        }

    }
}