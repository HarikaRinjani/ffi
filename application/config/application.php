<?php
if ( strpos(strtolower($_SERVER['SCRIPT_NAME']),strtolower(basename(__FILE__))) ){ die(header('HTTP/1.0 403 Forbidden')); }
$_CONFIG['app_name']           = 'MCP';
$_CONFIG['app_version']        = 0.1;
$_CONFIG['default_controller'] = 'login';
$_CONFIG['default_method']     = 'index';
$_CONFIG['timezone']           = 'Asia/Jakarta';
$_CONFIG['theme']              = 'default';
$_CONFIG['use_smarty']         = false;
$_CONFIG['language']           = 'id';
$_CONFIG['autoload']           = array('library'=>array('security','session'));
$_CONFIG['log_path']           = path('base').DS.'logs'.DS;
$_CONFIG['file_path']          = path('base').DS.'files'.DS;
$_CONFIG['file_url']           = BASE_URL.'index.php?/load/file/';
$_CONFIG['encrypt_session']    = true;
$_CONFIG['date_format']        = 'd F Y';
$_CONFIG['enable_builder']     = true;
$_CONFIG['auto_print']         = false;
$_CONFIG['status']             = array('y'=>'Active','n'=>'Not Active','p'=>'Pending','b'=>'Banned','a'=>'Approved','d'=>'Deleted');

foreach (glob(path('app').DS."config".DS."conf.d".DS."*.php") as $filename) {
    include_once $filename;
}
return $_CONFIG;
