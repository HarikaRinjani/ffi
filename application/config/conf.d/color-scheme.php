<?php
$_CONFIG["color_scheme"] = array(
    'blue'=>'Blue',
    'lightblue'=>'Light Blue',
    'darkblue'=>'Dark Blue',
    'red'=>'Red',
    'darkred'=>'Dark Red',
    'purple'=>'Purple',
    'darkpurple'=>'Dark Purple',
    'green'=>'Green',
    'lightgreen'=>'Light Green',
    'flatgreen'=>'Flat Green',
    'black'=>'Black',
    'cordovan'=>'Cordovan',
    'orange'=>'Orange',
    'darkorange'=>'Dark Orange',
    'pink'=>'Pink',
    'teal'=>'Teal',
    'lightteal'=>'Light Teal',
    'gray'=>'Gray'
);
