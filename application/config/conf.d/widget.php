<?php
$_CONFIG['widget_type']                 = array(
    //'kebijakan'=>'Kebijakan',
    'inbox'=>'Pesan'
    //'identifikasi'=>'Identifikasi',
    //'contact'=>'Potensi Bahaya'
);
$_CONFIG['widget_fields']               = array(
    //'kebijakan'=>array('kebijakan_name'=>_('Kebijakan'),'kebijakan_content'=>_('Isi')),
    'inbox'=>array('user_fullname'=>_('Pengirim'),'inbox_title'=>_('Judul Pesan'),'inbox_content'=>_('Pesan'))
    //'identifikasi'=>array('department_name'=>'Department','location'=>_('Lokasi'),'aspek'=>_('Aspek Bahaya')),
    //'contact'=>array('contact_name'=>_('Dilaporkan oleh'),'contact_content'=>_('Potensi Bahaya'))
);
$_CONFIG['widget_chart_fields']         = array(
    //'kebijakan'=>array('kebijakan_id'=>_('Total Kebijakan')),
    'inbox'=>array('inbox_id'=>_('Total Inbox'))
    //'identifikasi'=>array('identifikasi_id'=>_('Total Identifikasi')),
    //'contact'=>array('contact_id'=>_('Total Laporan Potensi Bahaya'))
);
$_CONFIG['widget_width'] = array(
    'col-md-6'=>'Half',
    'col-md-12'=>'Full'
);