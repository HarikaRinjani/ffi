<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 12/15/2014
 * Time: 6:26 PM
 */
$_CONFIG['user_group'] = array(
    'administrator'=>'Administrator',
    'coop_localadmin'=>'Coop. Local Administrator',
    'coop_manager'=>'Coop. Manager',
    'coop_qcdept'=>'Coop. QC Dept',
    'coop_operator'=>'Coop. Operator',
    'ffi_manager'=>'FFI Manager',
    'ffi_qc'=>'FFI QC',
    'farmer'=>'Farmers'
);
$_CONFIG['super_group'] = array(
                'administrator',
                'ffi_manager',
                'ffi_qc'
            );
$_CONFIG['user_grouproles'] = array(
            'anggota'=>array(
                'setting' => 'Settings'
                ),
            'cow'=>array(
                'management'=>'View Cow Population Data',
                'history'=>'View Cow Population History',
                'edit'=>'Edit Cow Population Data',
                'save'=>'Save Cow Population Data'
                ),
            'distribution'=>array(
                'index' => 'Distribution / Delivery Oder Lists',
                'detail' => 'View Detail',
                'edit' => 'Edit',
                'addnew' => 'Add New Entry',
                'delete' => 'Delete',
                'save' => 'Save',
                'print_' => 'Print',
                'reprint' => 'Re-Print',
                'printlog' => 'Print Logs',
                'setstatus'=>'Change Status'
                ),
            'fpgrade'=>array(
                'index'=>'Freezing Point Grade',
                'save'=>'Modify Freezing Point Grade Data',
                'edit'=>'Edit Freezing Point Grade',
                'delete'=>'Delete Freezing Point Grade'
                ),
            'kelompok'=>array(
                'index'=>'Kelompok List',
                'setting'=>'Settings',
                'load'=>'Load Data (recommended)',
                'findByTPK'=>'Find by TPK (recommended)',
                'loadAll'=>'Load All Data (recommended)'
                ),
            'konsumen'=>array(
                'setting'=>'Settings'
                ),
            'koperasi'=>array(
                'setting'=>'Settings',
                'loadAll'=>'Load All Data (recommended)'
                ),

            'milkprice'=>array(
                'index'=>'Milk Price Formula',
                'save'=>'Save Data',
                'edit'=>'Edit',
                'test' =>' Test Formula',
                'addnew'=>'Add New Entry',
                'calculate'=>'Calculation Test',
                'logs'=>'Logs',
                'report'=>'Report',
                'getStandardPrice'=>'Get Standard Price (required by Report)',
				'reportFarmer'=>'Report Farmer'
                ),
            'periode'=>array(
                'setting'=>'Periode Settings'
                ),
            'qcparameter'=>array(
                'index'=>'Quality',
                'save'=>'Modify Quality Data',
                'log'=>'Logs',
                'refreshLog'=>'Auto refresh log',
				'addnew'=>'Manual Entry Quality Data',
				'getData'=>'## Quality Data Check'
                ),
            'rezgrade'=>array(
                'index'=>'Resazurine Grade',
                'save'=>'Modify Resazurine Grade Data',
                'edit'=>'Edit Resazurine Grade Data'
                ),
            'report'=>array(
                'index'=>'Overview',
                'transaction'=>'Milk Collection',
                'sinkronisasi'=>'Synchronize',
                'export'=>'Export to Excel',
                'qcparameter'=>'Quality Report',
                'distribution'=>'Distribution Report',
                'datalog'=>'CSV Scan Logs',
				'transactionFarmer'=>'Milk Collection for Farmer',
                'pendapatan'=>'income report',
				'farmer'=>'Farmer Report'
                ),
            'samplingschedule'=>array(
                'index'=>'Sampling Schedule Report',
                'data'=>'Quality -> Sampling Schedule',
                'generate'=>'Generate Sampling Schedule'
                ),
            'settings'=>array(
                'index'=>'Apps Settings',
                ),
            'system'=>array(
                'index'=>'System Setttings',
                'package'=>'Package Manager',
                'jsondata'=>'JSON DATA (recommended)',
                'installprocess'=>'Install Package',
                'repair'=>'Repair Package',
                'openDir'=>'Scan Package',
                'parameter'=>'View System Parameter',
                'parametersave'=>'Modify System Parameter',
                'usertype'=>'View User Type',
                'usertypesave'=>'Modify User Type',
                'security'=>'View Security Settings',
                'securitysave'=>'Modify Security Settings',
                'consolelog'=>'Console Logs',
                'checkupdate'=>'System Update',
                'installupdate'=>'Installing Update'
                ),
            'tpk'=>array(
                'setting'=>'Settings',
                'load'=>'Load TPK Data (recommended)',
                'findByKoperasi'=>'Find by Koperasi (recommended)',
                'loadAll'=>'Load All Data (recommended)'
                ),
            'transaksi'=>array(
                'index'=>'Milk Collection Point',
                'batal'=>'Cancel Transaction',
                'save'=>'Submit Transaction',
                'addnew'=>'Add New Transaction',
                'randomNumber'=>'Scale Reader'
                ),
            'truck'=>array(
                'setting'=>'Truck Settings'
                ),
            'user'=>array(
                'usermanager'=>'View User Manager',
                'saveuser'=>'Modify User'
                ),
            'card'=>array(
                'index'=>'Card Management',
                'inactive'=>'View inactive Card',
                'history'=>'View Card History',
                'generate'=>'Generate New Card Number'
                )
            );
$_CONFIG['unused_package'] = array('sysupdate','login','filemanager','barcode','load','widget','csvlog','csvscan','dashboard','kelompokharga','synchandler','profile','bookmark','mail','help','samplingschedule');
