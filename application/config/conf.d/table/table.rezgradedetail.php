<?php
$_TABLE['t_rezgradedetail'] = array(
	'rezgradedetail_id'=>array(
		'type'=>'character varying',
		'length'=>64,
		'not null'=>true,
		'primary_key'=>true
		),
	'rezgradedetail_grade'=>array(
		'type'=>'integer'
		),
	'rezgradedetail_tpcstart'=>array(
		'type'=>'numeric'
		),
	'rezgradedetail_tpcend'=>array(
		'type'=>'numeric'
		),
	'rezgradedetail_price'=>array(
		'type'=>'numeric'
		),
	'rezgradedetail_rezgrade'=>array(
		'type'=>'character varying',
		'length'=>64
		)
);