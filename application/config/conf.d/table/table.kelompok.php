<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 12/24/2014
 * Time: 2:31 AM
 */
$_TABLE['t_kelompok'] = array(
    'kelompok_id'=>array(
        'type'=>'character varying',
        'length'=>64,
        'not null'=>true,
        'primary_key'=>true
    ),
    'kelompok_nama'=>array(
        'type'=>'character varying',
        'length'=>128
    ),
    'kelompok_kode'=>array(
        'type'=>'character varying',
        'length'=>64
    ),
    'kelompok_koperasi'=>array(
      'type'=>'character varying',
      'length'=>64
    ),
    'kelompok_tpk'=>array(
        'type'=>'character varying',
        'length'=>64
    ),
    'kelompok_status'=>array(
        'type'=>'character varying',
        'length'=>1,
        'not null'=>true,
        'default'=>'y'
    )
);
