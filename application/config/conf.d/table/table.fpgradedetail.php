<?php
$_TABLE['t_fpgradedetail'] = array(
	'fpgradedetail_id'=>array(
		'type'=>'character varying',
		'length'=>64,
		'not null'=>true,
		'primary_key'=>true
		),
	'fpgradedetail_grade'=>array(
		'type'=>'integer'
		),
	'fpgradedetail_fpstart'=>array(
		'type'=>'numeric'
		),
	'fpgradedetail_fpend'=>array(
		'type'=>'numeric'
		),
	'fpgradedetail_price'=>array(
		'type'=>'numeric'
		),
	'fpgradedetail_fpgrade'=>array(
		'type'=>'character varying',
		'length'=>64
		)
);	