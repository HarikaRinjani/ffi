<?php
$_TABLE['t_samplingschedule'] = array(
	'samplingschedule_id' => array(
		'type'        =>'character varying',
		'length'      =>64,
		'not null'    =>true,
		'primary_key' =>true
	),
	'samplingschedule_anggota'=>array(
		'type'   =>'character varying',
		'length' =>64
	),
	'samplingschedule_periode'=>array(
		'type'=>'integer'
	),
	'samplingschedule_date'=>array(
		'type'=>'date'
	),
	'samplingschedule_sesi'=>array(
		'type'=>'character varying',
		'length'=>1
		),
	'samplingschedule_koperasi'=>array(
		'type'   =>'character varying',
		'length' =>64
		),
	'samplingschedule_tpk'=>array(
		'type'   =>'character varying',
		'length' =>64
		),
	'samplingschedule_status'=>array(
		'type'     =>'character varying',
		'length'   =>1,
		'not null' =>true,
		'default'  =>'p'
		),
    'samplingschedule_takendate'=>array(
        'type'=>'timestamp without time zone'
    )
);