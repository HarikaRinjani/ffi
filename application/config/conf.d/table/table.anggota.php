<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 12/27/2014
 * Time: 1:27 PM
 */
$_TABLE['t_anggota'] = array(
    'anggota_id'=>array(
        'type'=>'character varying',
        'length'=>64,
        'not null'=>true,
        'primary_key'=>true
    ),
    'anggota_nama'=>array(
        'type'=>'character varying',
        'length'=>128
    ),
    'anggota_nomor'=>array(
        'type'=>'character varying',
        'length'=>64
    ),
    'anggota_koperasi'=>array(
        'type'=>'character varying',
        'length'=>64
    ),
    'anggota_tpk'=>array(
        'type'=>'character varying',
        'length'=>64
    ),
    'anggota_kelompok'=>array(
        'type'=>'character varying',
        'length'=>64
    ),
    'anggota_kelompokharga'=>array(
        'type'=>'character varying',
        'length'=>64
    ),
    'anggota_nohp'=>array(
        'type'=>'character varying',
        'length'=>128
    ),
    'anggota_alamat'=>array(
        'type'=>'text'
    ),
    'anggota_status'=>array(
        'type'=>'character varying',
        'length'=>1,
        'not null'=>true,
        'default'=>'y'
    ),
    'anggota_photo'=>array(
        'type'=>'text'
    )
);