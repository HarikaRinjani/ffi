<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 12/24/2014
 * Time: 2:35 AM
 */
$_TABLE['t_kelompokharga'] = array(
    'kelompokharga_id'=>array(
        'type'=>'character varying',
        'length'=>64,
        'not null'=>true,
        'primary_key'=>true
    ),
    'kelompokharga_kode'=>array(
        'type'=>'character varying',
        'length'=>64
    ),
    'kelompokharga_hargadasar'=>array(
        'type'=>'decimal'
    ),
    'kelompokharga_nama'=>array(
        'type'=>'character varying',
        'length'=>128
    ),
    'kelompokharga_kelompok'=>array(
        'type'=>'character varying',
        'length'=>64
    ),
    'kelompokharga_tpk'=>array(
        'type'=>'character varying',
        'length'=>64
    ),
    'kelompokharga_koperasi'=>array(
        'type'=>'character varying',
        'length'=>64
    ),
    'kelompokharga_fat'=>array(
        'type'=>'decimal'
    ),
    'kelompokharga_snf'=>array(
        'type'=>'decimal'
    ),
    'kelompokharga_status'=>array(
        'type'=>'character varying',
        'length'=>1,
        'not null'=>true,
        'default'=>'y'
    ),
);