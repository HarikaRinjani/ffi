<?php
$_TABLE['t_distributionprintlog'] = array(
	'printlog_id'=>array(
		'type'=>'character varying',
		'length'=>64,
		'not null'=>true,
		'primary_key'=>true
		),
	'printlog_remark'=>array(
		'type'=>'character varying',
		'length'=>255
		),
	'printlog_entryby'=>array(
		'type'=>'character varying',
		'length'=>64
		),
	'printlog_entrydate'=>array(
		'type'=>'timestamp without time zone'
		),
	'printlog_distribution'=>array(
		'type'=>'character varying',
		'length'=>64
		),
	'printlog_status'=>array(
		'type'=>'character varying',
		'length'=>1,
		'not null'=>true,
		'default'=>'y'
		)
	);