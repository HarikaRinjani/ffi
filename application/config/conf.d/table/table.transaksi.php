<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 12/29/2014
 * Time: 1:47 PM
 */
$_TABLE['t_transaksi']=array(
    'transaksi_id'=>array(
        'type'=>'character varying',
        'length'=>64,
        'not null'=>true,
        'primary_key'=>true
    ),
    'transaksi_tpk'=>array(
        'type'=>'character varying',
        'length'=>64
    ),
    'transaksi_anggota'=>array(
        'type'=>'character varying',
        'length'=>64
    ),
    'transaksi_kelompok'=>array(
        'type'=>'character varying',
        'length'=>64
    ),
    'transaksi_kelompokharga'=>array(
        'type'=>'character varying',
        'length'=>64
    ),
    'transaksi_waktu'=>array(
        'type'=>'timestamp without time zone'
    ),
    'transaksi_status'=>array(
        'type'=>'character varying',
        'length'=>1,
        'not null'=>true,
        'default'=>'y'
    ),
    'transaksi_sesi'=>array(
        'type'=>'character varying',
        'length'=>1
    ),
    'transaksi_totalberat'=>array(
        'type'=>'numeric'
    ),
    'transaksi_nama'=>array(
        'type'=>'character varying',
        'length'=>128
    ),
    'transaksi_nomor'=>array(
        'type'=>'character varying',
        'length'=>64
    ),
    'transaksi_user'=>array(
        'type'=>'character varying',
        'length'=>64
    ),
    'transaksi_sync'=>array(
        'type'=>'character varying',
        'length'=>1,
        'not null'=>true,
        'default'=>'n'
    ),
    'transaksi_uniqid'=>array(
        'type'=>'integer'
    ),
    'transaksi_koperasi'=>array(
        'type'=>'character varying',
        'length'=>64
    ),
    'transaksi_periode'=>array(
        'type'=>'integer'
        ),
    'transaksi_override'=>array(
        'type'=>'character varying',
        'length'=>1,
        'not null'=>true,
        'default'=>'n'
        ),
    'transaksi_overrideby'=>array(
        'type'=>'character varying',
        'length'=>64
        )
);