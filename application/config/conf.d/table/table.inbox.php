<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 12/13/2014
 * Time: 11:30 AM
 */
$_TABLE['t_inbox'] = array(
    'inbox_id'=>array(
        'type'=>'character varying',
        'length'=>64,
        'primary_key'=>true,
        'not null'=>true
    ),
    'inbox_user'=>array(
        'type'=>'character varying',
        'length'=>64
    ),
    'inbox_status'=>array(
        'length'=>1,
        'type'=>'character varying',
        'default'=>'y'
    ),
    'inbox_sender'=>array(
        'length'=>64,
        'type'=>'character varying'
    ),
    'inbox_date'=>array(
        'type'=>'timestamp without time zone'
    ),
    'inbox_content'=>array(
        'type'=>'text'
    ),
    'inbox_title'=>array(
        'type'=>'character varying',
        'length'=>255,
        'not null'=>true,
        'default'=>'untitled'
    ),
    'inbox_read'=>array(
        'type'=>'character varying',
        'length'=>1,
        'not null'=>true,
        'default'=>'n'
    ),
    'inbox_attachment'=>array(
        'type'=>'text'
    )
);