<?php
$_TABLE['t_periodedetail'] = array(
	'periodedetail_id'=>array(
		'type'=>'character varying',
		'length'=>64,
		'not null'=>true,
		'primary_key'=>true
		),
	'periodedetail_periode'=>array(
		'type'=>'character varying',
		'length'=>64
		),
	'periodedetail_name'=>array(
		'type'=>'integer'
		),
	'periodedetail_startdate'=>array(
		'type'=>'integer'
		),
	'periodedetail_enddate'=>array(
		'type'=>'integer'
		)
);