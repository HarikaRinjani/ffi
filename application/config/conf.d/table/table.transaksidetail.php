<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 12/29/2014
 * Time: 1:53 PM
 */
$_TABLE['t_transaksidetail'] = array(
    'transaksidetail_id'=>array(
        'type'=>'character varying',
        'length'=>64,
        'not null'=>true,
        'primary_key'=>true
    ),
    'transaksidetail_transaksi'=>array(
        'type'=>'character varying',
        'length'=>64,
        'not null'=>true
    ),
    'transaksidetail_urutan'=>array(
        'type'=>'integer'
    ),
    'transaksidetail_berat'=>array(
        'type'=>'numeric'
    ),
    'transaksidetail_status'=>array(
        'type'=>'character varying',
        'length'=>1,
        'not null'=>true,
        'default'=>'y'
    ),
    'transaksidetail_sync'=>array(
        'type'=>'character varying',
        'length'=>1,
        'not null'=>true,
        'default'=>'n'
    )
);