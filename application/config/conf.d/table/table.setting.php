<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 12/13/2014
 * Time: 11:30 AM
 */
$_TABLE['t_setting'] = array(
    'setting_name'=>array(
        'type'=>'character varying',
        'length'=>128,
        'primary_key'=>true,
        'not null'=>true
    ),
    'setting_value'=>array(
        'type'=>'text'
    ),
    'setting_status'=>array(
        'length'=>1,
        'type'=>'character varying',
        'default'=>'y'
    )
);