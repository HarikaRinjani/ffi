<?php
$_TABLE['t_truck'] = array(
    'truck_id'=>array(
        'type'=>'character varying',
        'length'=>64,
        'not null'=>true,
        'primary_key'=>true
    ),
    'truck_nama'=>array(
        'type'=>'character varying',
        'length'=>128
    ),
    'truck_status'=>array(
        'type'=>'character varying',
        'length'=>1,
        'not null'=>true,
        'default'=>'y'
    ),
    'truck_nopol'=>array(
        'type'=>'character varying',
        'length'=>16
    ),
    'truck_model'=>array(
        'type'=>'character varying',
        'length'=>128
    ),
    'truck_kapasitas'=>array(
        'type'=>'numeric'
    ),
    'truck_koperasi'=>array(
        'type'=>'character varying',
        'length'=>64
    )
);
