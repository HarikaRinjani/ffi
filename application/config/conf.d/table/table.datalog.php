<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 19/01/15
 * Time: 9:39
 */
$_TABLE['t_datalog'] = array(
    'datalog_id'=>array(
        'type'=>'character varying',
        'length'=>64,
        'not null'=>true,
        'primary_key'=>true
    ),
    'datalog_status'=>array(
        'type'=>'character varying',
        'length'=>1,
        'not null'=>true,
        'default'=>'p'
    ),
    'datalog_time'=>array(
        'type'=>'timestamp without time zone'
    ),
    'datalog_ip'=>array(
        'type'=>'character varying',
        'length'=>64
    ),
    'datalog_server'=>array(
        'type'=>'character varying',
        'length'=>128
    ),
    'datalog_data'=>array(
        'type'=>'text'
    ),
    'datalog_key'=>array(
        'type'=>'character varying',
        'length'=>255
    )
);