<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 12/24/2014
 * Time: 2:23 AM
 */
$_TABLE['t_tpk'] = array(
    'tpk_id'=>array(
        'type'=>'character varying',
        'length'=>64,
        'not null'=>true,
        'primary_key'=>true
    ),
    'tpk_kode'=>array(
        'type'=>'character varying',
        'length'=>24
    ),
    'tpk_nama'=>array(
        'type'=>'character varying',
        'length'=>128
    ),
    'tpk_alamat'=>array(
        'type'=>'character varying',
        'length'=>200
    ),
    'tpk_status'=>array(
        'type'=>'character varying',
        'length'=>1,
        'not null'=>true,
        'default'=>'y'
    ),
    'tpk_koperasi'=>array(
        'type'=>'character varying',
        'length'=>64,
        'not null'=>true
    )
);