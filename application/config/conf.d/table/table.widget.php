<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 12/17/2014
 * Time: 6:02 PM
 */
$_TABLE['t_widget'] = array(
    'widget_id'=>array(
        'type'=>'character varying',
        'length'=>64,
        'not null'=>true,
        'primary_key'=>true
    ),
    'widget_name'=>array(
        'type'=>'character varying',
        'length'=>200
    ),
    'widget_type'=>array(
        'type'=>'character varying',
        'length'=>64
    ),
    'widget_status'=>array(
        'type'=>'character varying',
        'length'=>1,
        'default'=>'y'
    ),
    'widget_urutan'=>array(
        'type'=>'integer'
    ),
    'widget_display_type'=>array(
        'type'=>'character varying',
        'length'=>100
    ),
    'widget_width'=>array(
        'type'=>'character varying',
        'length'=>64
    ),
    'widget_user_id'=>array(
        'type'=>'character varying',
        'length'=>64
    )
);