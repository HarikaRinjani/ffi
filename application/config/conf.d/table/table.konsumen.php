<?php
/**
 * Created by PhpStorm.
 * User: PrastowoaGung
 * Date: 12/30/2014
 * Time: 8:27 PM
 */
$_TABLE['t_konsumen'] = array(
    'konsumen_id'=>array(
        'type'=>'character varying',
        'length'=>64,
        'not null'=>true,
        'primary_key'=>true
    ),
    'konsumen_nama'=>array(
        'type'=>'character varying',
        'length'=>128
    ),
    'konsumen_alamat'=>array(
        'type'=>'character varying',
        'length'=>255
    ),
    'konsumen_telp'=>array(
        'type'=>'character varying',
        'length'=>64
    ),
    'konsumen_status'=>array(
        'type'=>'character varying',
        'length'=>1,
        'not null'=>true,
        'default'=>'y'
    ),
    'konsumen_koperasi'=>array(
        'type'=>'character varying',
        'length'=>64
    )
);
