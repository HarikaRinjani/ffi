<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 12/13/2014
 * Time: 11:30 AM
 */
$_TABLE['t_user'] = array(
    'user_id'=>array(
        'primary_key'=>true,
        'length'=>64,
        'type'=>'character varying',
        'not null'=>true
    ),
    'user_name'=>array(
        'length'=>64,
        'type'=>'character varying'
    ),
    'user_fullname'=>array(
        'type'=>'character varying',
        'length'=>128
    ),
    'user_email'=>array(
        'length'=>200,
        'type'=>'character varying'
    ),
    'user_password'=>array(
        'length'=>0,
        'type'=>'text'
    ),
    'user_status'=>array(
        'length'=>1,
        'type'=>'character varying',
        'default'=>'y'
    ),
    'user_group'=>array(
        'length'=>64,
        'type'=>'character varying'
    ),
    'user_lastactivity'=>array(
        'type'=>'timestamp without time zone'
    ),
    'user_registerdate'=>array(
        'type'=>'timestamp without time zone'
    ),
    'user_avatar'=>array(
        'type'=>'text'
    ),
    'user_theme'=>array(
        'type'=>'character varying',
        'length'=>50
    ),
    'user_expired'=>array(
        'type'=>'timestamp without time zone'
    ),
    'user_role'=>array(
        'type'=>'text'
    ),
    'user_su'=>array(
        'type'=>'character varying',
        'length'=>1,
        'default'=>'n'
    ),
    'user_token'=>array(
        'type'=>'text'
    ),
    'user_dept'=>array(
        'type'=>'character varying',
        'length'=>64
    ),
    'user_firstuse'=>array(
        'type'=>'character varying',
        'length'=>1,
        'not null'=>true,
        'default'=>'y'
    ),
    'user_koperasi'=>array(
        'type'=>'character varying',
        'length'=>64
    ),
    'user_lastsync'=>array(
        'type'=>'timestamp without time zone'
    )
);
