<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 12/16/2014
 * Time: 10:46 AM
 */
$_TABLE['t_bookmark'] = array(
    'bookmark_id'=>array(
        'type'=>'character varying',
        'length'=>64,
        'primary_key'=>true,
        'not null'=>true
    ),
    'bookmark_status'=>array(
        'type'=>'character varying',
        'length'=>1,
        'default'=>'y'
    ),
    'bookmark_url'=>array(
        'type'=>'character varying',
        'length'=>255
    ),
    'bookmark_title'=>array(
        'type'=>'character varying',
        'length'=>255
    ),
    'bookmark_user'=>array(
        'type'=>'character varying',
        'length'=>64
    )
);