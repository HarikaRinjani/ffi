<?php
$_TABLE['t_milkprice'] = array(
	'milkprice_id'=>array(
		'type'=>'character varying',
		'length'=>64,
		'not null'=>true,
		'primary_key'=>true
		),
	'milkprice_hargadasar'=>array(
		'type'=>'numeric'
		),
	'milkprice_koperasi'=>array(
		'type'=>'character varying',
		'length'=>64
		),
	'milkprice_anggota'=>array(
		'type'=>'date'
		),
	'milkprice_periode'=>array(
		'type'=>'character varying',
		'length'=>64
	),
	'milkprice_status'=>array(
		'type'=>'character varying',
		'length'=>1,
		'not null'=>true,
		'default'=>'y'
		)
);
