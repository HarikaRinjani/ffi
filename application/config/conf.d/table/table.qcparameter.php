<?php
$_TABLE['t_qcparameter'] = array(
	'qcparameter_id'=>array(
		'type'=>'character varying',
		'length'=>64,
		'not null'=>true,
		'primary_key'=>true
		),
	'qcparameter_transaksi'=>array(
		'type'=>'character varying',
		'length'=>64
		),
	'qcparameter_milkoscandate'=>array(
		'type'=>'timestamp without time zone'
		),
	'qcparameter_bactoscandate'=>array(
		'type'=>'timestamp without time zone'
		),
	'qcparameter_cfu'=>array(
		'type'=>'numeric'
		),
	'qcparameter_ibc'=>array(
		'type'=>'numeric'
		),
	'qcparameter_fat'=>array(
		'type'=>'numeric'
		),
	'qcparameter_protein'=>array(
		'type'=>'numeric'
		),
	'qcparameter_lactose'=>array(
		'type'=>'numeric'
		),
	'qcparameter_ts'=>array(
		'type'=>'numeric'
		),
	'qcparameter_fp'=>array(
		'type'=>'numeric'
		),
	'qcparameter_acidity'=>array(
		'type'=>'numeric'
		),
	'qcparameter_urea'=>array(
		'type'=>'numeric'
		),
	'qcparameter_lastmodified'=>array(
		'type'=>'timestamp without time zone'
		),
	'qcparameter_status'=>array(
		'type'=>'character varying',
		'length'=>1,
		'not null'=>true,
		'default'=>'y'
		),
	'qcparameter_snf'=>array(
		'type'=>'numeric'
		),
	'qcparameter_editby'=>array(
		'type'=>'character varying',
		'length'=>64
		),
	'qcparameter_scandate'=>array(
		'type'=>'timestamp without time zone'
		),
	'qcparameter_koperasi'=>array(
		'type'=>'character varying',
		'length'=>64
		),
	'qcparameter_anggota'=>array(
		'type'=>'character varying',
		'length'=>64
		),
	'qcparameter_kelompok'=>array(
	    'type'=>'character varying',
	    'length'=>64
	),
	'qcparameter_tpk'=>array(
		'type'=>'character varying',
		'length'=>64
		),
	'qcparameter_sesi'=>array(
		'type'=>'character varying',
		'length'=>1
		)
);