<?php
$_TABLE['t_card'] = array(
	'card_id'=>array(
		'type'=>'character varying',
		'length'=>64,
		'primary_key'=>true,
		'not null'=>true
		),
	'card_number'=>array(
		'type'=>'character varying',
		'length'=>64
		),
	'card_anggota'=>array(
		'type'=>'character varying',
		'length'=>64
		),
	'card_status'=>array(
		'type'=>'character varying',
		'length'=>1,
		'not null'=>true,
		'default'=>'y'
		),
	'card_entryby'=>array(
		'type'=>'character varying',
		'length'=>64
		),
	'card_entrydate'=>array(
		'type'=>'timestamp without time zone'
		),
	'card_koperasi'=>array(
		'type'=>'character varying',
		'length'=>64
		),
	'card_tpk'=>array(
		'type'=>'character varying',
		'length'=>64
		),
	'card_kelompok'=>array(
		'type'=>'character varying',
		'length'=>64
		),
	'card_remark'=>array(
		'type'=>'text'
		)
	);