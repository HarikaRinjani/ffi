<?php
$_TABLE['t_milkpriceformula']=array(
	'milkpriceformula_id'=>array(
		'type'=>'character varying',
		'length'=>64,
		'not null'=>true,
		'primary_key'=>true
		),
	'milkpriceformula_koperasi'=>array(
		'type'=>'character varying',
		'length'=>64
		),
	'milkpriceformula_effectivedate'=>array(
		'type'=>'date'
		),
	'milkpriceformula_hargadasar'=>array(
		'type'=>'numeric'
	),
	'milkpriceformula_entryby'=>array(
		'type'=>'character varying',
		'length'=>64
		),
	'milkpriceformula_entrydate'=>array(
		'type'=>'timestamp without time zone'
		),
	'milkpriceformula_editby'=>array(
		'type'=>'character varying',
		'length'=>64
		),
	'milkpriceformula_editdate'=>array(
		'type'=>'timestamp without time zone'
		),
	'milkpriceformula_status'=>array(
		'type'=>'character varying',
		'length'=>1,
		'not null'=>true,
		'default'=>'y'
		)
	);
