<?php
$_TABLE['t_rezgrade'] = array(
	'rezgrade_id'=>array(
		'type'=>'character varying',
		'length'=>64,
		'not null'=>true,
		'primary_key'=>true
		),
	'rezgrade_koperasi'=>array(
		'type'=>'character varying',
		'length'=>64
		),
	'rezgrade_tpcstart'=>array(
		'type'=>'numeric'
		),
	'rezgrade_tpcend'=>array(
		'type'=>'numeric'
		),
	'rezgrade_price'=>array(
		'type'=>'numeric'
		),
	'rezgrade_grade'=>array(
		'type'=>'integer'
		),
	'rezgrade_status'=>array(
		'type'=>'character varying',
		'length'=>1,
		'not null'=>true,
		'default'=>'y'
		),
	'rezgrade_effectivedate'=>array(
		'type'=>'date'
		)
	);