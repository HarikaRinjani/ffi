<?php
$_TABLE['t_csvlog'] = array(
	'csvlog_id'=>array(
		'type'=>'character varying',
		'length'=>64,
		'not null'=>true,
		'primary_key'=>true
		),
	'csvlog_content'=>array(
		'type'=>'text'
		),
	'csvlog_date'=>array(
		'type'=>'timestamp without time zone'
		)
);