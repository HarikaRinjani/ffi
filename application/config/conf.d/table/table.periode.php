<?php
$_TABLE['t_periode'] = array(
	'periode_id'=>array(
		'type'=>'character varying',
		'length'=>64,
		'not null'=>true,
		'primary_key'=>true
	),
	'periode_effectivedate'=>array(
		'type'=>'date'
	),
	'periode_koperasi'=>array(
		'type'=>'character varying',
		'length'=>64
		),
	'periode_status'=>array(
		'type'=>'character varying',
		'length'=>1,
		'not null'=>true,
		'default'=>'y'
	),
	'periode_name'=>array(
		'type'=>'character varying',
		'length'=>128
		)
);