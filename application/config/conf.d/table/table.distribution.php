<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 29/01/2015
 * Time: 9:51
 */
$_TABLE['t_distribution'] = array(
    'distribution_id'=>array(
        'type'=>'character varying',
        'length'=>64,
        'not null'=>true,
        'primary_key'=>true
    ),
    'distribution_tanggal'=>array(
        'type'=>'date'
    ),
    'distribution_truck'=>array(
        'type'=>'character varying',
        'length'=>64
    ),
    'distribution_konsumen'=>array(
        'type'=>'character varying',
        'length'=>64
    ),
    'distribution_berangkatjam'=>array(
        'type'=>'time'
    ),
    'distribution_masukips'=>array(
        'type'=>'time'
    ),
    'distribution_keluarips'=>array(
        'type'=>'time'
    ),
    'distribution_berat'=>array(
        'type'=>'numeric'
    ),
    'distribution_beratjenis'=>array(
        'type'=>'numeric'
    ),
    'distribution_temperature'=>array(
        'type'=>'numeric'
    ),
    'distribution_alkohol'=>array(
        'type'=>'numeric'
    ),
    'distribution_fat'=>array(
        'type'=>'numeric'
    ),
     'distribution_cfu'=>array(
        'type'=>'numeric'
    ),
     'distribution_ibc'=>array(
        'type'=>'numeric'
    ),
     'distribution_fatpercent'=>array(
        'type'=>'numeric'
    ),

     'distribution_protein'=>array(
        'type'=>'numeric'
    ),
     'distribution_lactose'=>array(
        'type'=>'numeric'
    ),
     'distribution_fp'=>array(
        'type'=>'numeric'
    ),
     'distribution_ts'=>array(
        'type'=>'numeric'
    ),
     'distribution_acidity'=>array(
        'type'=>'numeric'
    ),
     'distribution_urea'=>array(
        'type'=>'numeric'
    ),
    'distribution_remark'=>array(
        'type'=>'character varying',
        'length'=>200
    ),
    'distribution_pemalsuan'=>array(
        'type'=>'numeric'
    ),
    'distribution_pemalsuandesc'=>array(
        'type'=>'character varying',
        'length'=>200
    ),
    'distribution_userlab'=>array(
        'type'=>'character varying',
        'length'=>200
    ),
    'distribution_userinput'=>array(
        'type'=>'character varying',
        'length'=>200
    ),
    'distribution_pembawabarang'=>array(
        'type'=>'character varying',
        'length'=>200
    ),
    'distribution_status'=>array(
        'type'=>'character varying',
        'length'=>1,
        'not null'=>true,
        'default'=>'p'
    ),
    'distribution_entryby'=>array(
        'type'=>'character varying',
        'length'=>64
    ),
    'distribution_entrydate'=>array(
        'type'=>'timestamp without time zone'
    ),
    'distribution_nomor'=>array(
        'type'=>'character varying',
        'length'=>128
        ),
    'distribution_printstatus'=>array(
        'type'=>'character varying',
        'length'=>1,
        'not null'=>true,
        'default'=>'n'
        ),
    'distribution_koperasi'=>array(
        'type'=>'character varying',
        'length'=>64
        ),
    'distribution_volumediterima'=>array(
        'type'=>'numeric'
        ),
    'distribution_beratjenisditerima'=>array(
        'type'=>'numeric'
        ),
    'distribution_remarkditerima'=>array(
        'type'=>'text'
        ),
    'distribution_grn'=>array(
        'type'=>'character varying',
        'length'=>128
    ),
    'distribution_diterimaoleh'=>array(
        'type'=>'character varying',
        'length'=>128
    ),
    'distribution_diterimatanggal'=>array(
        'type'=>'date'
    ),
    'distribution_snf'=>array(
        'type'=>'numeric'
        ),
    'distribution_fatditerima'=>array(
        'type'=>'numeric'
    ),

    'distribution_proteinditerima'=>array(
        'type'=>'numeric'
    ),
     'distribution_lactosediterima'=>array(
        'type'=>'numeric'
    ),
     'distribution_fpditerima'=>array(
        'type'=>'numeric'
    ),
     'distribution_tsditerima'=>array(
        'type'=>'numeric'
    ),
     'distribution_acidityditerima'=>array(
        'type'=>'numeric'
    ),
     'distribution_ureaditerima'=>array(
        'type'=>'numeric'
    ),
      'distribution_snfditerima'=>array(
         'type'=>'numeric'
     ),
      'distribution_tpc'=>array(
            'type'=>'numeric'
        ),
      'distribution_tpcditerima'=>array(
        'type'=>'numeric'
        )
);
