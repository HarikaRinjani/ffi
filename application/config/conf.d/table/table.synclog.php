<?php
$_TABLE['t_synclog'] = array(
	'synclog_id'=>array(
		'type'=>'character varying',
		'length'=>64,
		'not null'=>true,
		'primary_key'=>true
		),
	'synclog_date'=>array(
		'type'=>'timestamp without time zone'
		),
	'synclog_client'=>array(
		'type'=>'character varying',
		'length'=>64
		),
	'synclog_content'=>array(
		'type'=>'text'
		)
);