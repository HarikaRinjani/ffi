<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 06/01/15
 * Time: 12:17
 */
$_TABLE['t_koperasi'] = array(
    'koperasi_id'=>array(
        'type'=>'character varying',
        'length'=>64,
        'not null'=>true,
        'primary_key'=>true
    ),
    'koperasi_nama'=>array(
        'type'=>'character varying',
        'length'=>128
    ),
    'koperasi_status'=>array(
        'type'=>'character varying',
        'length'=>1,
        'not null'=>true,
        'default'=>'y'
    ),
    'koperasi_alamat'=>array(
        'type'=>'text'
    ),
    'koperasi_telp'=>array(
        'type'=>'character varying',
        'length'=>64
    ),
    'koperasi_logo'=>array(
        'type'=>'text'
        ),
    'koperasi_kode'=>array(
        'type'=>'character varying',
        'length'=>5,
        'not null'=>true,
        'default'=>'000'
        )
);