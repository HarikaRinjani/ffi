<?php
$_TABLE['t_fpgrade'] = array(
	'fpgrade_id'=>array(
		'type'=>'character varying',
		'length'=>64,
		'not null'=>true,
		'primary_key'=>true
		),
	'fpgrade_fpstart'=>array(
		'type'=>'numeric'
		),
	'fpgrade_fpend'=>array(
		'type'=>'numeric'
		),
	'fpgrade_price'=>array(
		'type'=>'numeric'
		),
	'fpgrade_koperasi'=>array(
		'type'=>'character varying',
		'length'=>64
		),
	'fpgrade_status'=>array(
		'type'=>'character varying',
		'length'=>1,
		'not null'=>true,
		'default'=>'y'
		),
	'fpgrade_grade'=>array(
		'type'=>'numeric'
		),
	'fpgrade_effectivedate'=>array(
		'type'=>'date'
		)
	);