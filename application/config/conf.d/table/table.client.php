<?php
$_TABLE['t_client'] = array(
    'client_id'=>array(
        'type'=>'character varying',
        'length'=>64,
        'primary_key'=>true,
        'not null'=>true
    ),
    'client_tpk'=>array(
        'type'=>'character varying',
        'length'=>64
    ),
    'client_key'=>array(
        'type'=>'character varying',
        'length'=>24
    ),
    'client_status'=>array(
        'type'=>'character varying',
        'length'=>1,
        'not null'=>true,
        'default'=>'y'
    ),
    'client_entryby'=>array(
        'type'=>'character varying',
        'length'=>64
    ),
    'client_entrydate'=>array(
        'type'=>'timestamp without time zone'
    )
);
