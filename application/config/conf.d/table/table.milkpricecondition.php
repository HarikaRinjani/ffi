<?php
$_TABLE['t_milkpricecondition'] = array(
	'milkpricecondition_id'=>array(
		'type'=>'character varying',
		'length'=>64,
		'not null'=>true,
		'primary_key'=>true
		),
	'milkpricecondition_milkpriceformula'=>array(
		'type'=>'character varying',
		'length'=>64
		),
	'milkpricecondition_element'=>array(
		'type'=>'character varying',
		'length'=>64
		),
	'milkpricecondition_standardvalue'=>array(
		'type'=>'numeric'
		),
	'milkpricecondition_interval'=>array(
		'type'=>'numeric'
		),
	'milkpricecondition_status'=>array(
		'type'=>'character varying',
		'length'=>1,
		'not null'=>true,
		'default'=>'y'
		),
	'milkpricecondition_addprice'=>array(
		'type'=>'numeric'
		),
	'milkpricecondition_korelasi'=>array(
		'type'=>'character varying',
		'length'=>1,
		'not null'=>true,
		'default'=>'p'
		),
	);
