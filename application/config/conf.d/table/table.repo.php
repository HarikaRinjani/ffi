<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 12/27/2014
 * Time: 1:27 PM
 */
$_TABLE['t_repo'] = array(
    'repo_id'=>array(
        'type'=>'character varying',
        'length'=>64,
        'not null'=>true,
        'primary_key'=>true
    ),
    'repo_name'=>array(
        'type'=>'character varying',
        'length'=>128
    ),
    'repo_type'=>array(
        'type'=>'character varying',
        'length'=>64
    ),
    'repo_koperasi'=>array(
        'type'=>'character varying',
        'length'=>64
    ),
    'repo_tpk'=>array(
        'type'=>'character varying',
        'length'=>64
    ),
    'repo_data'=>array(
        'type'=>'text'
    ),
    'created_at'=>array(
        'type'=>'timestamp without timezone'
    ),
    'updated_at'=>array(
        'type'=>'timestamp without timezone'
    ),
    'repo_remark'=>array(
        'type'=>'text'
    )
);
