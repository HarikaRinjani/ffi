<?php
$_TABLE['t_cow'] = array(
	'cow_id'=>array(
		'type'=>'character varying',
		'length'=>64,
		'not null'=>true,
		'primary_key'=>true
		),
	'cow_anggota'=>array(
		'type'=>'character varying',
		'length'=>64
		),
	'cow_population'=>array(
		'type'=>'integer'
		),
	'cow_antibiotic'=>array(
		'type'=>'integer'
		),
	'cow_lactating'=>array(
		'type'=>'integer'
		),
	'cow_antibioticstartdate'=>array(
		'type'=>'date'
		),
	'cow_antibioticenddate'=>array(
		'type'=>'date'
		),
	'cow_antibioticremark'=>array(
		'type'=>'text'
		),
	'cow_status'=>array(
		'type'=>'character varying',
		'length'=>1,
		'not null'=>true,
		'default'=>'y'
		),
	'cow_entryby'=>array(
		'type'=>'character varying',
		'length'=>64
		),
	'cow_entrydate'=>array(
		'type'=>'timestamp without time zone'
		),
	'cow_koperasi'=>array(
		'type'=>'character varying',
		'length'=>64
		),
	'cow_tpk'=>array(
		'type'=>'character varying',
		'length'=>64
		),
	'cow_kelompok'=>array(
		'type'=>'character varying',
		'length'=>64
		)
);