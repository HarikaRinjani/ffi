<?php
if ( strpos(strtolower($_SERVER['SCRIPT_NAME']),strtolower(basename(__FILE__))) ){ die(header('HTTP/1.0 403 Forbidden')); }
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 12/14/2014
 * Time: 6:39 PM
 */
$_TABLE = array();
foreach (glob(path('app').DS."config".DS."conf.d".DS."table".DS."*.php") as $filename) {
    include_once $filename;
}
return $_TABLE;