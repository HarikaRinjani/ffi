<?php
class Filemanager_Controller extends Controller{
	public $version = .5;
	public $description = 'File Manager (Beta)';

	function __construct(){
		security::checkSession();
	}
	function index(){

	}
	function reload(){
		$subdir = !isset($_GET['dir']) ? '' : rawurldecode($_GET['dir']);
		$subdir = str_replace(array('/','\\'),DS,$subdir);
		$opendir = fn::open_folder($subdir);
		echo json_encode($opendir);
	}
}