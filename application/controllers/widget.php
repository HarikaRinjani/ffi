<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 12/17/2014
 * Time: 6:07 PM
 */
class Widget_Controller extends Controller{
    public $buildConfiguration = array(
        'table'=>array(
            'widget'
        ),
        'models'=>array(
            'widget'
        )
    );
    public $description = 'Add widget to your dashboard';
    public $dependencies = array(
        array(
            'name'=>'mail',
            'version'=>1
        )
    );
    function __construct(){
        parent::__construct();
    }
    function index(){

    }
}