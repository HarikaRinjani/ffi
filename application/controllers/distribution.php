<?php

class Distribution_Controller extends Controller{
	/**
	 * Build Configuration
	 * @var array
	 */
	public $buildConfiguration = array(
		'table'=>array(
			'distribution',
			'distributionprintlog'
			),
		'models'=>array(
			'distribution',
			'distributionprintlog'
			),
		'views'=>array(
				'distribution.index',
				'distribution.menu',
				'distribution.print',
				'distribution.detail'
			)
	);

	public $version = 1.1;
	public $description = "Distribution Module v1.1";

	function __construct(){
		security::checkSession();
	}

	/**
	 * Delivery Order Lists
	 * @return [type] [description]
	 */
	function index(){
		session::set('title','Delivery Order Lists');
		$dt = load::model('distribution');

		$_GET['status'] = !isset($_GET['status']) ? 'n' : $_GET['status'];
		$_GET['page']   = !isset($_GET['page']) ? 1 : $_GET['page'];

		if(isset($_GET['print']) && isset($_GET['distribution_id'])){
			$dt->setId($_GET['distribution_id']);
			$printData = $dt->read();
			if($printData['total_data'] == 0){
				session::set('errorMsg',_('Not valid ID'));
			}else{
				if($printData['records']['distribution_printstatus'] == 'y'){
					session::set('errorMsg',_('Delivery Order already printed'));
				}else{
					$data['print_distribution_id'] = $printData['records']['distribution_id'];
				}
			}
		}
		
		
		$dt->setPage($_GET['page']);
		$additionalSQL  = '';
		$keyword        = '';

		if(isset($_GET['keyword'])){
			$dt->setKeyword($_GET['keyword']);
			$dt->setSearchIn(array(
				'distribution_nomor',
				't_truck.truck_nopol',
				't_konsumen.konsumen_nama'
				));
		}

		if(in_array(session::get('user_group'),Config::$application['super_group'])){
			$dt->setCondition('t_distribution.distribution_status = \''.$_GET['status'].'\''.$additionalSQL);
		}else{
			$dt->setCondition('t_distribution.distribution_status = \''.$_GET['status'].'\' AND t_distribution.distribution_koperasi = \''.session::get('user_koperasi').'\''.$additionalSQL);
		}
		
		$dt->setOrderBy('distribution_tanggal');
		$dt->setLimit(Config::$application['display_limit']);
		
		$data['distribution'] = $dt->read();
		$data['keyword']      = $keyword;
		View::display('distribution/distribution.index',$data);
	}
	function detail($id){
		$dt = load::model('distribution');
		$dt->setId($id);
		$record = $dt->read();
		$data['data'] = $record['records'];
		View::display('distribution/distribution.detail',$data);
	}
	function edit($id){
		//die('upsss.... ');
		$dt = load::model('distribution');
		$dt->setId($id);
		$record = $dt->read();
		$data['data'] = $record['records'];
		if(isset($_GET['json'])){
			echo json_encode($record['records']);
		}else{
			View::display('distribution/distribution.edit',$data);
		}
	}
	function addnew(){
		session::set('title','New Delivery Order');
		View::display('distribution/distribution.new');
	}
	function delete(){
		if(isset($_POST['distribution_id'])){
			$dt = load::model('distribution');
			$dt->setId($_POST['distribution_id']);
			$cek = $dt->read();
			if($cek['records']['distribution_status'] == 'y'){
				session::set('errorMsg',_('Delivery Order has been completed!'));
				exit();
			}else{
				$dt->setId($_POST['distribution_id']);
				if($dt->delete()){
					session::set('errorMsg',_('Data has been successfully deleted'));
				}else{
					session::set('errorMsg',_('Failed while deleting data'));
				}
			}
			
		}
	}
	function save(){
		$dt = load::model('distribution');
		
		$_POST['distribution_tanggal']            = date('Y-m-d',fn::dateToUnix($_POST['distribution_tanggal']));
		$_POST['distribution_berat']              = (float) $_POST['distribution_berat'];
		$_POST['distribution_beratjenis']         = (float) $_POST['distribution_beratjenis'];
		$_POST['distribution_temperature']        = (float) $_POST['distribution_temperature'];
		$_POST['distribution_alkohol']            = (float) $_POST['distribution_alkohol'];
		$_POST['distribution_fat']                = (float) $_POST['distribution_fat'];
		$_POST['distribution_pemalsuan']          = (float) $_POST['distribution_pemalsuan'];
		$_POST['distribution_protein']            = (float) $_POST['distribution_protein'];
		$_POST['distribution_lactose']            = (float) $_POST['distribution_lactose'];
		$_POST['distribution_fp']                 = (float) $_POST['distribution_fp'];
		$_POST['distribution_ts']                 = (float) $_POST['distribution_ts'];
		$_POST['distribution_acidity']            = (float) $_POST['distribution_acidity'];
		$_POST['distribution_urea']               = (float) $_POST['distribution_urea'];
		$_POST['distribution_snf']                = (float) $_POST['distribution_snf'];
		$_POST['distribution_tpc']                = (float) $_POST['distribution_tpc'];
		$_POST['distribution_volumediterima']     = 0.00;
		$_POST['distribution_beratjenisditerima'] = 0.00;
		
		if(isset($_POST['distribution_id'])){
			$dt->setId($_POST['distribution_id']);
			$cek = $dt->read();
			if($cek['records']['distribution_status'] == 'y'){
				session::set('errorMsg',_('Delivery Order has been completed!'));
				header('location:'.BASE_URL.'index.php?/distribution/index');
				exit();
			}
			$dt->setId($_POST['distribution_id']);
			$dt->setDataArray($_POST);
			$redirect = BASE_URL.'index.php?/distribution/index';
			$query = $dt->update();
		}else{
			$dt->setCondition('distribution_nomor = \''.$_POST['distribution_nomor'].'\'');
			$cek = $dt->read();
			if($cek['total_data'] > 0){
				session::set('errorMsg',_('DO Number already Exists'));
				if(isset($_SERVER['HTTP_REFERER'])){
					header('location:'.$_SERVER['HTTP_REFERER']);
				}else{
					header('location:'.BASE_URL.'index.php?/distribution/index');
				}
				exit();
			}
			$_POST['distribution_status'] 	 = 'n';
			$_POST['distribution_id']     	 = fn::generate_id(true);
            $_POST['distribution_entryby']   = session::get('user_id');
            $_POST['distribution_entrydate'] = date('Y-m-d H:i:s');
			$dt->setDataArray($_POST);
			$redirect = BASE_URL.'index.php?/distribution/index/&print=yes&distribution_id='.$_POST['distribution_id'];
			$query = $dt->insert();
		}
		if($query){
			session::set('errorMsg',_('Data has been successfully saved'));
			header('location:'.$redirect);
		}else{
			session::set('errorMsg',_('Failed while saving data into database'));
			header('location:'.BASE_URL.'index.php?/distribution/index');
		}
		
	}
	function print_($distribution_id=''){
		if(!empty($distribution_id)){
			$dt = load::model('distribution');
			$dt->setId($distribution_id);
			$data = $dt->read();
			if($data['records']['distribution_printstatus'] == 'n'){
				$dt->setId($distribution_id);
				$dt->setData('distribution_printstatus','y');
				$dt->update();
				$res['data'] = $data['records'];
				View::display('distribution/distribution.print',$res);
			}else{
				die('Delivery Order already printed');
			}
		}
	}
	function reprint($distribution_id){
		if(isset($_GET['printlog_remark']) && !empty($_GET['printlog_remark'])){
			$dt                            = load::model('distributionprintlog');
			$data['printlog_id']           = fn::generate_id(true);
			$data['printlog_entryby']      = session::get('user_id');
			$data['printlog_entrydate']    = date('Y-m-d H:i:s');
			$data['printlog_distribution'] = $distribution_id;
			$data['printlog_remark']       = $_GET['printlog_remark'];
			$dt->setDataArray($data);
			$dt->insert();

			$distribution = load::model('distribution');
			$distribution->setId($distribution_id);
			$datas = $distribution->read();
			$res['data'] = $datas['records'];
			$res['remark'] = 'reprinted by : '.session::get('user_name').' on '.date('d/m/Y H:i');
			View::display('distribution/distribution.print',$res);
		}else{
			die('Please input remark!');
		}
	}
	function setstatus(){
		if(isset($_POST['distribution_id'])){
			$dt                                       = load::model('distribution');
			$_POST['distribution_diterimatanggal']    = date('Y-m-d',fn::dateToUnix($_POST['distribution_diterimatanggal']));
			$_POST['distribution_volumediterima']     = (float) $_POST['distribution_volumediterima'];
			$_POST['distribution_beratjenisditerima'] = (float) $_POST['distribution_beratjenisditerima'];
			$_POST['distribution_proteinditerima']    = (float) $_POST['distribution_proteinditerima'];
			$_POST['distribution_lactosediterima']    = (float) $_POST['distribution_lactosediterima'];
			$_POST['distribution_fpditerima']         = (float) $_POST['distribution_fpditerima'];
			$_POST['distribution_tsditerima']         = (float) $_POST['distribution_tsditerima'];
			$_POST['distribution_acidityditerima']    = (float) $_POST['distribution_acidityditerima'];
			$_POST['distribution_ureaditerima']       = (float) $_POST['distribution_ureaditerima'];
			$_POST['distribution_snfditerima']        = (float) $_POST['distribution_snfditerima'];
			$_POST['distribution_tpcditerima'] 		  = (float) $_POST['distribution_tpcditerima'];
			$dt->setDataArray($_POST);
			$dt->setId($_POST['distribution_id']);
			if($dt->update()){
				session::set('errorMsg',_('Data has been successfully saved'));
			}else{
				session::set('errorMsg',_('Failed while saving data into database'));
			}
			header('location:'.$_SERVER['HTTP_REFERER']);
		}
	}
	function printlog(){
		$dt = load::model('distributionprintlog');
		$_GET['page'] = !isset($_GET['page']) ? 1 : $_GET['page'];
		$dt->setPage($_GET['page']);
		$additionalSQL = '';
		$keyword = '';
		if(isset($_GET['keyword'])){
			// $dt->setKeyword($_GET['keyword']);
			// $dt->setSearchIn('distribution_nomor');
			$keyword = $_GET['keyword'];
			$additionalSQL = ' AND LOWER(t_distribution.distribution_nomor) LIKE \'%'.strtolower($_GET['keyword']).'%\' ';
		}
		if(in_array(session::get('user_group'),Config::$application['super_group'])){
			$dt->setCondition('t_distribution.distribution_status != \'d\''.$additionalSQL);
		}else{
			$dt->setCondition('t_distribution.distribution_status != \'d\' AND t_distribution.distribution_koperasi = \''.session::get('user_koperasi').'\''.$additionalSQL);
		}
		$data['keyword'] = $keyword;
		$dt->setOrderBy('printlog_entrydate');
		$dt->setLimit(Config::$application['display_limit']);
		$data['distribution'] = $dt->read();
		View::display('distribution/distribution.printlog',$data);
	}
	/*
	function tester(){
		$array = array(
			'160158',
			'005700',
			'160425',
			'290215',
			'100434',
			'160741',
			'160107',
			'160121',
			'170124'
		);
		$dt = load::model('samplingschedule');
		$dt->setCondition('samplingschedule_sesi = \'s\' AND t_anggota.anggota_nomor IN (\''.implode('\',\'',$array).'\')');
		$record = $dt->read();
		foreach($record['records'] as $k=>$v){
			$dt->setId($v['samplingschedule_id'])->setData('samplingschedule_status','y')->update();
		}
	}*/
}