<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 12/16/2014
 * Time: 10:41 AM
 */
class Bookmark_Controller extends Controller{
    public $buildConfiguration = array(
        'models'=>array(
            'bookmark'
        ),
        'table'=>array(
            'bookmark'
        ),
        'views'=>array(
            'bookmark'
        )
    );
    public $description = 'Bookmark your favorites module to get instant access';
    function __construct(){
        parent::__construct();
        security::checkSession();
    }

    function index(){

    }
    function save(){
        load::library('fn');
        $dt = load::model('bookmark');
        // CHECK BOOKMARK
        $_POST['url'] = str_replace(BASE_URL,'',$_POST['url']);
        $dt->setCondition('bookmark_url = \''.$_POST['url'].'\' AND bookmark_user = \''.session::get('user_id').'\'');
        $cek = $dt->read();
        if($cek['total_data']>0){
            echo 'bookmark already exists!';
        }else{
            $data['bookmark_id'] = fn::generate_id();
            $data['bookmark_title'] = $_POST['title'];
            $data['bookmark_url'] = $_POST['url'];
            $data['bookmark_user'] = session::get('user_id');
            $dt->setDataArray($data);
            if($dt->insert()){
                echo 'success';
            }else{
                echo _('Failed saving your bookmarks');
            }
        }

    }
    function delete(){
        if(isset($_POST['bookmark_id'])){
            $dt = load::model('bookmark');
            $dt->setCondition('bookmark_id = \''.$_POST['bookmark_id'].'\' AND bookmark_user = \''.session::get('user_id').'\'');
            $cek = $dt->read();
            if($cek['total_data']>0){
                $dt->setId($_POST['bookmark_id']);
                if($dt->delete()){
                    session::set('errorMsg',_('Bookmark deleted'));
                }else{
                    session::set('errorMsg',_('Failed deleting bookmark'));
                }
            }else{
                session::set('errorMsg',_('You dont have permission to delete this bookmark'));
            }
        }
    }
    function load(){
        $dt = load::model('bookmark');
        $dt->setCondition('bookmark_user = \''.session::get('user_id').'\' AND bookmark_status = \'y\'');
        $dt->setLimit(100);
        $bookmark = $dt->read();
        echo json_encode($bookmark);
    }
}