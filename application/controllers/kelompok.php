<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 12/27/2014
 * Time: 1:31 PM
 */
class Kelompok_Controller extends Controller{
    public $buildConfiguration = array(
        'table'=>'kelompok',
        'models'=>'kelompok',
        'views'=>array(
            'kelompok.setting'
        )
    );
    public $description = 'Setting Kelompok (MCP)';
    public $version = 1.1;
    // Kelompok membutuhkan package tpk
    public $dependencies = array(
        array(
            'name'=>'tpk',
            'version'=>1
        )
    );
    function __construct(){
        parent::__construct();
        security::checkSession();
    }
    function index(){

    }
    function setting($operation='',$id=''){
        
        session::set('title','Kelompok Settings');
        load::library('fn');
        $kelompok = load::model('kelompok');
        switch($operation){
            case 'save':
                if(isset($_POST['kelompok_id'])){
                    $kelompok->setId($_POST['kelompok_id']);
                    $kelompok->setDataArray($_POST);
                    $query = $kelompok->update();
                }else{
                    $_POST['kelompok_id'] = fn::generate_id(true);
                    $kelompok->setCondition('kelompok_tpk = \''.$_POST['kelompok_tpk'].'\'');
                    $kelompok->setLimit(1);
                    $kelompok->setOrderBy('kelompok_kode');
                    $kelompok->setOrderType('DESC');
                    $cek = $kelompok->read();
                    if($cek['total_data'] > 0){
                        $kkode = (int) $cek['records'][0]['kelompok_kode'];
                        $nkode = ($kkode + 1);
                        $_POST['kelompok_kode'] = fn::displayZeroNumber($nkode,3);
                    }else{
                        $_POST['kelompok_kode'] = '001';
                    }
                    $kelompok->setDataArray($_POST);
                    $query = $kelompok->insert();
                }
                if($query){
                    session::set('errorMsg',_('Data has been successfully saved'));
                }else{
                    session::set('errorMsg',_('Failed while saving data into database'));
                }
                header('location:'.BASE_URL.'index.php?/kelompok/setting');

                break;
            case 'edit':
                if(!empty($id)){
                    if(!in_array(session::get('user_group'),Config::$application['super_group'])){
                        $kelompok->setCondition('kelompok_id = \''.$id.'\' AND kelompok_koperasi = \''.session::get('user_koperasi').'\'');
                        $kelompok->setLimit(1);
                        $dept = $kelompok->read();
                        echo json_encode($dept['records'][0]);
                    }else{
                        $kelompok->setId($id);
                        $dept = $kelompok->read();
                        echo json_encode($dept['records']);
                    }

                }

                break;
            case 'delete':
                if(isset($_POST['kelompok_id'])){
                    if(!in_array(session::get('user_group'),Config::$application['super_group'])){
                        //$koperasi->setCondition('koperasi_id = \''.$_POST['koperasi_id'].'\' AND koperasi_id = \''.session::get('user_koperasi').'\'');
                        $kelompok->setCondition('kelompok_id = \''.$_POST['kelompok_id'].'\' AND kelompok_koperasi = \''.session::get('user_koperasi').'\'');
                    }else{
                        $kelompok->setId($_POST['kelompok_id']);
                    }
                    if($kelompok->delete()){
                        session::set('errorMsg',_('Data has been successfully deleted'));
                    }else{
                        session::set('errorMsg',_('Failed while deleting data'));
                    }
                }
                header('location:'.BASE_URL.'index.php?/kelompok/setting');

                break;
            default:
                $koperasi = load::model('koperasi');
                $_GET['page'] = !isset($_GET['page']) ? 1 : $_GET['page'];
                $kelompok->setPage($_GET['page']);
                if(isset($_GET['keyword'])){
                    $kelompok->setKeyword($_GET['keyword']);
                    $kelompok->setSearchIn(array(
                            't_kelompok.kelompok_nama',
                            't_koperasi.koperasi_nama',
                            't_tpk.tpk_nama')
                        );
                }
                $kelompok->setLimit(Config::$application['display_limit']);
                if(!in_array(session::get('user_group'),Config::$application['super_group'])){
                    $_GET['koperasi'] = session::get('user_koperasi');
                    $kelompok->setCondition('kelompok_koperasi = \''.session::get('user_koperasi').'\' AND tpk_status != \'d\'');
                    $data['koperasi'] = $koperasi->setCondition('koperasi_id = \''.session::get('user_koperasi').'\'')->read();
                }else{
                    $_GET['koperasi'] = !isset($_GET['koperasi']) ? '' : $_GET['koperasi'];
                    $addSQL = '';
                    if(!empty($_GET['koperasi'])){
                        $addSQL = ' AND kelompok_koperasi = \''.$_GET['koperasi'].'\'';
                    }
                    $kelompok->setCondition('kelompok_status != \'d\''.$addSQL);
                    $data['koperasi'] = $koperasi->setCondition('koperasi_status = \'y\'')->setOrderBy('koperasi_nama')->setLimit(0)->read();
                }
                $kelompok->setOrderBy('t_koperasi.koperasi_nama');
                $data['kelompok'] = $kelompok->read();
                View::display('kelompok/kelompok.setting',$data);

                break;
        }
    }
    function load(){
        $dt   = load::model('kelompok');
        $page = !isset($_GET['page']) ? 1 : $_GET['page'];
        $dt->setPage($page);
        if(isset($_GET['keyword'])){
            $dt->setKeyword($_GET['keyword']);
            $dt->setSearchIn(array(
                'kelompok_kode',
                'kelompok_nama'
            ));
        }
        $dt->setLimit(Config::$application['display_limit']);
        if(!in_array(session::get('user_group'),Config::$application['super_group'])){
            $dt->setCondition('kelompok_koperasi = \''.session::get('user_koperasi').'\' AND kelompok_status = \'y\'');
        }else{
            $dt->setCondition('kelompok_status = \'y\'');
        }
        
        $dt->setOrderBy('kelompok_nama');
        $data = $dt->read();
        echo json_encode($data);
    }
    function findByTPK($tpk_id=''){
        $dt   = load::model('kelompok');
        $page = !isset($_GET['page']) ? 1 : $_GET['page'];
        $dt->setPage($page);
        $dt->setCondition('kelompok_status = \'y\' AND kelompok_tpk = \''.$tpk_id.'\'');
        $dt->setLimit(1000);
        $dt->setOrderBy('kelompok_nama');
        $data = $dt->read();
        echo json_encode($data);
    }
    function loadAll(){
        $dt = load::model('kelompok');
        $dt->setLimit(0);
        if(!in_array(session::get('user_group'),Config::$application['super_group'])){
            $dt->setCondition('kelompok_koperasi = \''.session::get('user_koperasi').'\' AND kelompok_status = \'y\'');
        }else{
            $dt->setCondition('kelompok_status = \'y\'');
        }
        $dt->setOrderBy('kelompok_nama');
        $data = $dt->read();
        echo json_encode($data);
    }
}
