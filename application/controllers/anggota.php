<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 12/28/2014
 * Time: 12:41 PM
 */
class Anggota_Controller extends Controller{
    public $buildConfiguration = array(
        'table'=>array('anggota','cow','card'),
        'models'=>'anggota',
        'views'=>'anggota.setting'
    );
    public $description = "Setting Anggota (MCP)v1.5\n- Fix Paging \n\nv1.4\n- Add generate user data \n\nv1.3\n- Add default value to cow population data";
    function __construct(){
        security::checkSession();
    }
    public $version = 1.5;

    function index(){

    }

    /**
     * Farmers Settings (Master Data)
     * @param  string $operation [description]
     * @param  string $id        [description]
     * @return [type]            [description]
     */
    function setting($operation='',$id=''){
        session::set('title','Anggota Settings');
        load::library('fn');
        $anggota = load::model('anggota');
        $card = load::model('card');
        $cow = load::model('cow');
        $repo = load::model('repo');
        switch($operation){
            case 'save':
                if(isset($_POST['anggota_id'])){
                    $anggota->setId($_POST['anggota_id']);
                    $anggota->setDataArray($_POST);
                    $query = $anggota->update();
                }else{
                    $_POST['anggota_id'] = fn::generate_id(true);
                    $anggota->setCondition('anggota_nomor = \''.$_POST['anggota_nomor'].'\'');
                    $cek = $anggota->read();
                    if($cek['total_data'] > 0){
                        session::set('errorMsg',_('Farmers Number already exists!'));
                        header('location:'.BASE_URL.'index.php?/anggota/setting');
                        exit();
                    }else{

                        $anggota->setDataArray($_POST);
                        if($anggota->insert()){
                            $card_id                    = fn::generate_id(true);
                            $carddata['card_id']        = $card_id;
                            $carddata['card_number']    = $_POST['card_number'];
                            $carddata['card_status']    = 'y';
                            $carddata['card_entryby']   = session::get('user_id');
                            $carddata['card_entrydate'] = date('Y-m-d H:i:s');
                            $carddata['card_anggota']   = $_POST['anggota_id'];
                            $carddata['card_koperasi']  = $_POST['anggota_koperasi'];
                            $carddata['card_kelompok']  = $_POST['anggota_kelompok'];
                            $carddata['card_tpk']       = $_POST['anggota_tpk'];
                            $card->setDataArray($carddata);
                            if($card->insert()){
                                $cow_id                             = fn::generate_id(true);
                                $cowdata['cow_id']                  = $cow_id;
                                $cowdata['cow_population']          = $_POST['cow_population'];
                                $cowdata['cow_lactating']           = $_POST['cow_lactating'];
                                $cowdata['cow_antibiotic']          = $_POST['cow_antibiotic'];
                                $cowdata['cow_antibioticstartdate'] = date('Y-m-d',fn::dateToUnix($_POST['cow_antibioticstartdate']));
                                $cowdata['cow_antibioticenddate']   = date('Y-m-d',fn::dateToUnix($_POST['cow_antibioticenddate']));
                                $cowdata['cow_status']              = 'y';
                                $cowdata['cow_anggota']             = $_POST['anggota_id'];
                                $cowdata['cow_antibioticremark']    = $_POST['cow_antibioticremark'];
                                $cowdata['cow_entryby']             = session::get('user_id');
                                $cowdata['cow_entrydate']           = date('Y-m-d H:i:s');
                                $cowdata['cow_koperasi']            = $_POST['anggota_koperasi'];
                                $cowdata['cow_tpk']                 = $_POST['anggota_tpk'];
                                $cowdata['cow_kelompok']            = $_POST['anggota_kelompok'];
                                if($cow->setDataArray($cowdata)->insert()){
                                    $query = true;
                                }else{
                                    $card->setId($cow_id);
                                    $card->delete();
                                    $anggota->setId($_POST['anggota_id']);
                                    $anggota->delete();
                                    $query = false;
                                }
                            }else{
                                $anggota->setId($_POST['anggota_id']);
                                $anggota->delete();
                                $query = false;
                            }
                        }else{
                            $query = false;
                        }

                    }
                }
                if($query){
                    if(isset($_POST['anggota_status']) && $_POST['anggota_status'] === 'n'){
                        $user = load::model('user');
                        $cek = $user->setCondition('user_name = \''.$_POST['anggota_nomor'].'\'')->setLimit(1)->read();
                        if($cek['total_data'] > 0){
                            $user->user_status = 'n';
                            $user->setId($cek['records'][0]['user_id']);
                            $user->update();
                        }
                    }

                    $repo->updateRepo('anggota', $_POST['anggota_id'], $_POST['anggota_koperasi'], $_POST['anggota_tpk']);

                    session::set('errorMsg',_('Data has been successfully saved'));
                }else{
                    session::set('errorMsg',_('Failed while saving data into database'));
                }
                header('location:'.BASE_URL.'index.php?/anggota/setting');

                break;
            case 'edit':
                if(!empty($id)){

                    if(!in_array(session::get('user_group'),Config::$application['super_group'])){
                        $anggota->setCondition('anggota_koperasi = \''.session::get('user_koperasi').'\' AND anggota_id = \''.$id.'\'');
                        $anggota->setLimit(1);
                        $dept = $anggota->read();
                        echo json_encode($dept['records'][0]);
                    }else{
                        $anggota->setId($id);
                        $dept = $anggota->read();
                        echo json_encode($dept['records']);
                    }
                }

                break;
            case 'delete':
                if(isset($_POST['anggota_id'])){
                    if(!in_array(session::get('user_group'),Config::$application['super_group'])){
                        $anggota->setCondition('anggota_koperasi = \''.session::get('user_koperasi').'\' AND anggota_id = \''.$_POST['anggota_id'].'\'');
                    }else{
                        $anggota->setId($_POST['anggota_id']);
                    }

                    if($anggota->delete()){
                        $anggota->setId($_POST['anggota_id']);
                        $cekAnggota = $anggota->read();

                        $repo->updateRepo('anggota', $cekAnggota['records'][0]['anggota_koperasi'], $cekAnggota['records'][0]['anggota_tpk']);

                        session::set('errorMsg',_('Data has been successfully deleted'));
                    }else{
                        session::set('errorMsg',_('Failed while deleting data'));
                    }
                }
                header('location:'.BASE_URL.'index.php?/anggota/setting');

                break;
            case 'generateUser':
                $user = load::model('user');
                $dataAnggota = $anggota->setLimit(0)->setCondition('anggota_status = \'y\'')->read();
                $totalgenerated = 0;
                $totalfailed = 0;

                foreach($dataAnggota['records'] as $k=>$v){
                    $cek = $user->setCondition('user_name = \''.$v['anggota_nomor'].'\'')->read();
                    if($cek['total_data'] === 0){
                        $user->resetValue();
                        $user->user_id       = fn::generate_id(true);
                        $user->user_name     = $v['anggota_nomor'];
                        $user->user_fullname = $v['anggota_nama'];
                        $user->user_email    = $v['anggota_nomor'].'@mcp.ddpffi.com';
                        $user->user_status   = 'y';
                        $user->user_password = security::encrypt($v['anggota_nomor']);
                        $user->user_koperasi =$v['anggota_koperasi'];
                        $user->user_group    = 'farmer';
                        if(!empty($v['anggota_photo'])){
                            $user->user_avatar = $v['anggota_photo'];
                        }else{
                            if(file_exists(Config::$application['file_path'].'images'.DS.'PASFOTO'.DS.$v['anggota_nomor'].'.JPG')){
                                $user->user_avatar = 'images/PASFOTO/'.$v['anggota_nomor'].'.JPG';
                            }else{
                                $user->user_avatar = '';
                            }
                        }
                        $user->user_registerdate = date('Y-m-d H:i:s');
                        if($user->insert()){
                            $totalgenerated++;
                        }else{
                            $totalfailed++;
                        }
                    }
                }

                $repo->updateRepo('user');
                session::set('errorMsg',$totalgenerated.' Farmer data successfully exported. '.$totalfailed.' Farmer data cant be processed');
                header('location:'.BASE_URL.'index.php?/anggota/setting');
                exit();
                break;
            default:

                $_GET['page'] = !isset($_GET['page']) ? 1 : $_GET['page'];
                $anggota->setPage($_GET['page']);

                if(isset($_GET['keyword'])){
                    $anggota->setKeyword($_GET['keyword']);
                    $anggota->setSearchIn(array(
                        'anggota_nama',
                        't_koperasi.koperasi_nama',
                        't_tpk.tpk_nama',
                        't_kelompok.kelompok_nama',
                        'anggota_alamat',
                        'anggota_nomor',
                        'anggota_nohp',
                        't_card.card_number'
                        ));
                }
                $anggota->setLimit(Config::$application['display_limit']);

                if(!in_array(session::get('user_group'),Config::$application['super_group'])){
                    $anggota->setCondition('anggota_koperasi = \''.session::get('user_koperasi').'\' AND anggota_status != \'d\' AND t_card.card_status = \'y\' AND t_cow.cow_status = \'y\'');
                    $_GET['koperasi'] = session::get('user_koperasi');
                }else{

                    $_GET['koperasi'] = !isset($_GET['koperasi']) ? '' : $_GET['koperasi'];
                    $koperasiSQL = '';
                    if(!empty($_GET['koperasi'])){
                        $koperasiSQL = ' AND anggota_koperasi = \''.trim($_GET['koperasi']).'\'';
                    }
                    $anggota->setCondition('anggota_status != \'d\' AND t_card.card_status = \'y\' AND t_cow.cow_status = \'y\''.$koperasiSQL);

                }
                $anggota->setOrderBy('anggota_nama');
                $data['anggota'] = $anggota->read();

                $koperasi = load::model('koperasi');
                if(!in_array(session::get('user_group'),Config::$application['super_group'])){
                    $koperasi->setCondition('koperasi_id = \''.session::get('user_koperasi').'\' AND koperasi_status = \'y\'');
                }else{
                    $koperasi->setCondition('koperasi_status = \'y\'');
                }

                $koperasi->setLimit(0);
                $koperasi->setOrderBy('koperasi_nama');
                $data['koperasi'] = $koperasi->read();
                // $repo->updateRepo('user');
                // $repo->updateRepo('anggota');
                // $repo->updateRepo('koperasi');
                // $repo->updateRepo('periode');
                View::display('anggota/anggota.setting',$data);

                break;
        }
    }

    function generateUser(){

    }
}
