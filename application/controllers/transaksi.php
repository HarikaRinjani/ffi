<?php

/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 12/29/2014
 * Time: 1:58 PM
 */
class Transaksi_Controller extends Controller {
    public $buildConfiguration = array(
        'table'  => array('transaksi', 'transaksidetail'),
        'views'  => array('transaksi.add','transaksi.index'),
        'models' => array('transaksi')
    );
    public $description = "v1.5\n- Fix Session \n\nTransaksi Modul (MCP) v1.3\n\n- Auto start / stop checking new weight value to improve load performance";
    public $version = 1.6;
    function __construct() {
        security::checkSession();
    }

    function index() {
        session::set('title', 'Transaksi');
        $_datas['transaksi'] = array();
        $_datas['anggota']   = array();
        $dt = load::model('transaksi');
        $dt->setCondition('transaksi_status = \'p\' AND transaksi_user = \'' . session::get('user_id') . '\'');
        $dt->setLimit(1);
        $cek = $dt->read();
        if ($cek['total_data'] > 0) {
            // RESTORE INCOMPLETE TRANSACTION
//            header('location:' . BASE_URL . 'index.php?/transaksi/add/&anggota_nomor=' . $cek['records'][0]['transaksi_nomor']);
//            exit();

            $dt->setCondition('anggota_nomor = \'' . $cek['records'][0]['transaksi_nomor'] . '\' AND anggota_status = \'y\'');
            $dt->setLimit(1);
            $data = $dt->read();
            if ($data['total_data'] > 0) {

//                load::library('fn');
//                $transaksi = load::model('transaksi');
//                // CEK DULU
//
//                $transaksi->setCondition('transaksi_nomor = \'' . $_REQUEST['anggota_nomor'] . '\' AND transaksi_status = \'p\'');
//                $transaksi->setLimit(1);
//                $cek = $transaksi->read();
//                if ($cek['total_data'] > 0) {
//                    $dtTransaksi = $cek['records'][0];
//                } else {
//                    $dtTransaksi['transaksi_id'] = fn::generate_id(true);
//                    $dtTransaksi['transaksi_waktu'] = date('Y-m-d H:i:s');
//                    $dtTransaksi['transaksi_anggota'] = $data['records'][0]['anggota_id'];
//                    $dtTransaksi['transaksi_kelompok'] = $data['records'][0]['anggota_kelompok'];
//                    $dtTransaksi['transaksi_kelompokharga'] = $data['records'][0]['anggota_kelompokharga'];
//                    $dtTransaksi['transaksi_nama'] = $data['records'][0]['anggota_nama'];
//                    $dtTransaksi['transaksi_nomor'] = $data['records'][0]['anggota_nomor'];
//                    $dtTransaksi['transaksi_tpk'] = $data['records'][0]['anggota_tpk'];
//                    $dtTransaksi['transaksi_status'] = 'p';
//                    $dtTransaksi['transaksi_user'] = session::get('user_id');
//                    $transaksi->setDataArray($dtTransaksi);
//                    $transaksi->insert();
//                }
//                
                 
                $anggota = load::model('anggota');
                $anggota->setCondition('anggota_id = \''.$data['records'][0]['transaksi_anggota'].'\' AND t_cow.cow_status = \'y\' AND t_card.card_status = \'y\'');
                $anggota->setLimit(1);
                $_datas = $anggota->read();
                 if($_datas['records'][0]['anggota_photo'] == '' || $_datas['records'][0]['anggota_photo'] == null){
                    if(file_exists(Config::$application['file_path'].'images'.DS.'PASFOTO'.DS.$_datas['records'][0]['anggota_nomor'].'.JPG')){
                        $photo = 'index.php?/load/file/'.urlencode('images/PASFOTO/'.$_datas['records'][0]['anggota_nomor'].'.JPG');
                    }else{
                        $photo = 'index.php?/load/file/'.urlencode('images/avatar.jpg');
                    }
                }else{
                    $photo = $_datas['records'][0]['anggota_photo'];
                }
                $_datas['records'][0]['anggota_photo'] = $photo;
                $dtTransaksi       = $cek['records'][0];
                $_datas['transaksi'] = $dtTransaksi;
                $_datas['anggota']   = $_datas['records'][0];
            }
        }
        $transaksi_waktu = date('Y-m-d H:i:s');
        if (date('G', strtotime($transaksi_waktu)) <= Config::$application['batas_pagi']) {
            $_datas['transaksi_sesi']    = 'P';
        } else {
            $_datas['transaksi_sesi']    = 'S';
        }
        View::display('transaksi/transaksi.add', $_datas);
    }

    function batal($id) {
        $dt = load::model('transaksi');
        $dt->setId($id);
        if($dt->delete()){
            $detail = load::model('transaksidetail');
            $detail->setCondition('transaksidetail_transaksi = \'' . $id . '\'');
            $detail->delete();

            $error = db::dbError();
            if (empty($error)) {
                echo 'success';
            } else {
                echo $id;
            }
        }else{
            echo $id;
        }
    }

    function save() {

        load::library('fn');
        if (isset($_POST['transaksi_totalberat']) && !empty($_POST['transaksi_totalberat']) && $_POST['transaksi_totalberat'] > 0) {
            $transaksi_waktu = date('Y-m-d H:i:s');
            if (date('G', strtotime($transaksi_waktu)) <= Config::$application['batas_pagi']) {
                $transaksi_sesi    = 'p';
                $transaksi_sesiDtl = 'Pagi';
            } else {
                $transaksi_sesi    = 's';
                $transaksi_sesiDtl = 'Sore';
            }
            $dt = load::model('transaksi');
            $dt->setId($_POST['transaksi_id']);
            $dt->setData('transaksi_waktu', $transaksi_waktu);
            $dt->setData('transaksi_totalberat', $_POST['transaksi_totalberat']);
            $dt->setData('transaksi_status', 'y');
            $dt->setData('transaksi_sesi', $transaksi_sesi);
            if ($dt->update()) {
                $detail = load::model('transaksidetail');
                $index  = 1;
                $error  = 0;
                $detail->setCondition('transaksidetail_transaksi = \'' . $_POST['transaksi_id'] . '\'');
                $detail->delete();

                $detailPrint = '';
                $arrayPrint = array();
                foreach ($_POST['transaksidetail_berat'] as $k => $v) {
                    $detaildata['transaksidetail_transaksi'] = $_POST['transaksi_id'];
                    $detaildata['transaksidetail_id']        = fn::generate_id(true);
                    $detaildata['transaksidetail_berat']     = $v;
                    $detaildata['transaksidetail_urutan']    = $index;
                    
                    $detailPrint .= '<tr><td>' . $index . '</td><td>' . $v . '</td></tr>';
                    $arrayPrint[$k]['urutan'] = $index;
                    $arrayPrint[$k]['detail'] = number_format($v,2);
                    $detail->setDataArray($detaildata);
                    $insertDetail = $detail->insert();
                    if (!$insertDetail) {
                        $error++;
                    }
                    $index++;
                }
                if ($error > 0) {
                    session::set('errorMsg', 'Failed while saving data into database');
                    $res['status'] = session::get('errorMsg');
                    //header('location:' . $_SERVER['HTTP_REFERER']);
                } else {
                    setcookie('transaksi_nomor', '');
                    setcookie('transaksi_id', '');
                    setcookie('total_tr', '');
                    setcookie('total_kg', '');
                    setcookie('transaksi_detail', '');
                    session::set('errorMsg', _('Data has been successfully saved'));

//                    $template                          = file_get_contents(path('app') . DS . 'views' . DS . Config::$application['theme'] . DS . 'print.template.html');
                    $dataPrint['transaksi_waktu']      = $transaksi_waktu;
                    $dataPrint['transaksi_user']       = session::get('user_fullname');
                    $dataPrint['transaksi_sesi']       = $transaksi_sesiDtl;
                    $dataPrint['anggota_nomor']        = $_POST['anggota_nomor'];
                    $dataPrint['anggota_nama']         = $_POST['anggota_nama'];
                    $dataPrint['anggota_alamat']       = $_POST['anggota_alamat'];
                    $dataPrint['anggota_nohp']         = $_POST['anggota_nohp'];
                    $dataPrint['tpk_nama']             = $_POST['tpk_nama'];
                    $dataPrint['kelompok_nama']        = $_POST['kelompok_nama'];
                    $dataPrint['kelompok_kode']        = $_POST['kelompok_kode'];
                    $dataPrint['transaksi_totalberat'] = $_POST['transaksi_totalberat'];
                    $dataPrint['detail_transaksi']     = $arrayPrint;
                    $dataPrint['transaksi_id']         = $_POST['transaksi_id'];
//
//                    foreach ($dataPrint as $k => $v) {
//                        $template = str_replace('{{' . $k . '}}', $v, $template);
//                    }
//
//                    $dompdf = load::dompdf();
//                    $dompdf->set_paper("A4");
//                    $dompdf->load_html($template);
//                    $dompdf->render();
//                    $pdf           = $dompdf->output();
//                    $file_location = Config::$application['file_path'] . 'transaksi' . DS . $_POST['tpk_nama'] . DS . date('Y', strtotime($transaksi_waktu)) . DS . date('n', strtotime($transaksi_waktu)) . DS . $_POST['transaksi_id'] . '.pdf';
//                    fn::createDirectory($file_location, true);
//                    file_put_contents($file_location, $pdf);
//                    if (Config::$application['auto_print'] === true) {
//                        $printcmd = "java -classpath " . path('base') . DS . "pdfbox.jar org.apache.pdfbox.PrintPDF -silentPrint -printerName " . Config::$application['printer_name'] . " " . $file_location;
//                        exec($printcmd);
//                    }
//                    header('location:' . BASE_URL . 'index.php?/transaksi');
                    $res['status'] = 'success';
                    $res['data'] = $dataPrint;
                }
            } else {
                session::set('errorMsg', _('Failed while saving data into database'));
                $res['status'] = session::get('errorMsg');
            }
            echo json_encode($res);
        }
    }

    function addnew(){
        if (isset($_REQUEST['anggota_nomor'])) {
            $dt = load::model('anggota');
            $__override = false;
            if(isset($_REQUEST['override'])){
                $user = load::model('user');
                $user->setCondition('user_name=\''.$_POST['user_name'].'\' AND user_status = \'y\' AND (user_group = \'administrator\' OR user_group = \'coop_localadmin\')');
                $cekuser = $user->read();
                if($cekuser['total_data'] > 0){
                    $__override = true;
                    $dt->setCondition('t_anggota.anggota_nomor = \'' . $_REQUEST['anggota_nomor'] . '\' AND t_anggota.anggota_status = \'y\' AND t_cow.cow_status = \'y\' AND t_card.card_status = \'y\'');
                }else{
                     $res['error'] = 'You dont have permission to use this function';
                     echo json_encode($res);
                     exit();
                }
            }else{
                $dt->setCondition('t_card.card_number = \'' . $_REQUEST['anggota_nomor'] . '\' AND t_anggota.anggota_status = \'y\' AND t_cow.cow_status = \'y\' AND t_card.card_status = \'y\'');
            }
            
            $dt->setLimit(1);
            $data = $dt->read();
            if ($data['total_data'] === 0) {
                $res['error'] = 'Farmer ID Not Found';
            } else {

                if (date('G') <= Config::$application['batas_pagi']) {
                    $sesi    = 'p';
                } else {
                    $sesi    = 's';
                }

                load::library('fn');
                $schedule = load::model('samplingschedule');
                $currentPeriode = $schedule->getCurrentPeriode($data['records'][0]['anggota_koperasi']);
                $schedule->setCondition('samplingschedule_anggota = \''.$data['records'][0]['anggota_id'].'\' AND samplingschedule_date = \''.date('Y-m-d').'\' AND samplingschedule_koperasi = \''.$data['records'][0]['anggota_koperasi'].'\' AND samplingschedule_tpk = \''.$data['records'][0]['anggota_tpk'].'\' AND samplingschedule_sesi = \''.$sesi.'\' AND samplingschedule_periode = \''.$currentPeriode.'\' AND samplingschedule_status = \'p\'');
                $dataSchedule = $schedule->read();

                if($dataSchedule['total_data'] > 0){
                    $res['notifikasi'] = 'Petani memiliki jadwal pengambilan sample';
                }else{
                    $res['notifikasi'] = '';
                }

                $cow = load::model('cow');
                $cow->setCondition('cow_anggota = \''.$data['records'][0]['anggota_id'].'\' AND cow_status = \'y\'');
                $dtcow = $cow->read();
                $res['notifikasi_antibiotic'] = '';
                if($dtcow['total_data'] > 0){
                    if($dtcow['records'][0]['cow_antibiotic'] > 0){
                        $cow_startdate = strtotime($dtcow['records'][0]['cow_antibioticstartdate']);
                        $cow_enddate = strtotime($dtcow['records'][0]['cow_antibioticenddate']);
                        $cow_now = time();
                        if($cow_now >= $cow_startdate && $cow_now <= $cow_enddate){
                            $res['notifikasi_antibiotic'] = 'Peternak memiliki status antibiotik! ('.date('d/m/Y',$cow_startdate).' - '.date('d/m/Y',$cow_enddate).')';
                        }
                    }
                }

                $transaksi = load::model('transaksi');

                // if(session::check('current_tpk')){
                //     $current_tpk = session::get('current_tpk');
                // }else{
                //     $current_tpk = $data['records'][0]['anggota_tpk'];
                // }
                // $current_tpk = $data['records'][0]['anggota_tpk'];
                $current_tpk = Config::$application['mcp_tpk'];
                $uniqid = fn::generate_id();
                $dtTransaksi['transaksi_id']            = strtoupper($data['records'][0]['koperasi_kode'].$data['records'][0]['tpk_kode'].dechex((float)$uniqid));
                $dtTransaksi['transaksi_waktu']         = date('Y-m-d H:i:s');
                $dtTransaksi['transaksi_anggota']       = $data['records'][0]['anggota_id'];
                $dtTransaksi['transaksi_kelompok']      = $data['records'][0]['anggota_kelompok'];
                //$dtTransaksi['transaksi_kelompokharga'] = $data['records'][0]['anggota_kelompokharga'];
                $dtTransaksi['transaksi_nama']          = $data['records'][0]['anggota_nama'];
                $dtTransaksi['transaksi_nomor']         = $data['records'][0]['anggota_nomor'];
                $dtTransaksi['transaksi_tpk']           = Config::$application['mcp_tpk'];
                $dtTransaksi['transaksi_koperasi']      = Config::$application['mcp_koperasi'];

                $dtTransaksi['transaksi_status']        = 'p';
                $dtTransaksi['transaksi_user']          = session::get('user_id');
                if($__override === true){
                    $dtTransaksi['transaksi_override'] = 'y';
                    $dtTransaksi['transaksi_overrideby'] = $cekuser['records'][0]['user_id'];
                }
                //$dtTransaksi['transaksi_uniqid']        = $uniqid;
                $transaksi->setDataArray($dtTransaksi);
                $transaksi->insert();

                if($data['records'][0]['anggota_photo'] == '' || $data['records'][0]['anggota_photo'] == null){
                    if(file_exists(Config::$application['file_path'].'images'.DS.'PASFOTO'.DS.$data['records'][0]['anggota_nomor'].'.JPG')){
                        $photo = 'index.php?/load/file/'.urlencode('images/PASFOTO/'.$data['records'][0]['anggota_nomor'].'.JPG');
                    }else{
                        $photo = 'index.php?/load/file/'.urlencode('images/avatar.jpg');
                    }
                }else{
                    $photo = $data['records'][0]['anggota_photo'];
                }
                $data['records'][0]['anggota_photo'] = $photo;
                $res['error'] = '';
                $res['anggota']   = $data['records'][0];
                $res['transaksi_id'] = $dtTransaksi['transaksi_id'];

            }
            echo json_encode($res);
        }
    }

    function test(){
        load::library('fn');
        $uniqid = fn::generate_id();
        echo strtoupper('TPK1'.dechex((float)$uniqid));
    }

    function add() {
        session::set('title', 'Transaksi');
        if (isset($_REQUEST['anggota_nomor'])) {
            $dt = load::model('anggota');
            $dt->setCondition('anggota_nomor = \'' . $_REQUEST['anggota_nomor'] . '\' AND anggota_status = \'y\'');
            $dt->setLimit(1);
            $data = $dt->read();
            if ($data['total_data'] === 0) {
                session::set('errorMsg', _('Farmer not found'));
                header('location:' . BASE_URL . 'index.php?/transaksi');
            } else {
                load::library('fn');
                $transaksi = load::model('transaksi');
                // CEK DULU

                $transaksi->setCondition('transaksi_nomor = \'' . $_REQUEST['anggota_nomor'] . '\' AND transaksi_status = \'p\'');
                $transaksi->setLimit(1);
                $cek = $transaksi->read();
                if ($cek['total_data'] > 0) {
                    $dtTransaksi = $cek['records'][0];
                } else {
                    $dtTransaksi['transaksi_id']            = fn::generate_id(true);
                    $dtTransaksi['transaksi_waktu']         = date('Y-m-d H:i:s');
                    $dtTransaksi['transaksi_anggota']       = $data['records'][0]['anggota_id'];
                    $dtTransaksi['transaksi_kelompok']      = $data['records'][0]['anggota_kelompok'];
                    //$dtTransaksi['transaksi_kelompokharga'] = $data['records'][0]['anggota_kelompokharga'];
                    $dtTransaksi['transaksi_nama']          = $data['records'][0]['anggota_nama'];
                    $dtTransaksi['transaksi_nomor']         = $data['records'][0]['anggota_nomor'];
                    $dtTransaksi['transaksi_tpk']           = $data['records'][0]['anggota_tpk'];
                    $dtTransaksi['transaksi_status']        = 'p';
                    $dtTransaksi['transaksi_user']          = session::get('user_id');
                    $transaksi->setDataArray($dtTransaksi);
                    $transaksi->insert();
                }
                if (date('G') <= Config::$application['batas_pagi']) {
                    $res['transaksi_sesi']    = 'p';
                } else {
                    $res['transaksi_sesi']    = 's';
                }
                $res['transaksi'] = $dtTransaksi;
                $res['anggota']   = $data['records'][0];

                View::display('transaksi/transaksi.add', $res);
            }
        } else {
            header('location:' . BASE_URL . 'index.php?/transaksi');
        }

    }

    function randomNumber() {
        echo $_SESSION['kg'];
        // $a = rand(2,7);
        // $b = rand(50,70);
        // $c = ($b*2) / $a;
        // echo number_format($c,2);
    }
}
