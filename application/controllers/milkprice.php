<?php

class Milkprice_Controller extends Controller
{
    public $buildConfiguration = array(
        'table' => array(
            'milkprice',
            'milkpriceformula',
            'milkpricecondition',
            'rezgrade',
            'fpgrade',
            ),
        'views' => array(
            'milkprice.add',
            'milkprice.index',
            'milkprice.report',
            'milkprice.test',
            ),
        );
    public $version = 1.9;
    public $description = "v1.9 - (2016-02-29) Fix Periode \n\nv1.8 - Add export function \n\nv1.7 - Fix duplicate loop\n\nv1.6 - Fix Empty report on Periode 2 & 3\n\nv1.5\n- Change periode based on quality scan date\n\nv1.4\n- Fix resazurine grading\n\nv1.3\n- Fix empty periode \n\nv1.2\n- Add Report handler";
    public function __construct()
    {
        security::checkSession();
    }

    public function index()
    {
        session::set('title', 'Milkprice Formulation');
        $_GET['page'] = !isset($_GET['page']) ? 1 : $_GET['page'];
        $dt = load::model('milkpriceformula');
        $dt->setCondition('milkpriceformula_status != \'d\'');
        $dt->setPage($_GET['page']);
        $data['formula'] = $dt->read();
        View::display('milkprice/milkprice.index', $data);
    }

    public function save()
    {
        $dt = load::model('milkpriceformula');
        $milkpriceData['milkpriceformula_koperasi'] = $_POST['milkpriceformula_koperasi'];
        $milkpriceData['milkpriceformula_effectivedate'] = date('Y-m-d', fn::dateToUnix($_POST['milkpriceformula_effectivedate']));
        $milkpriceData['milkpriceformula_hargadasar'] = (float) $_POST['milkpriceformula_hargadasar'];
        if (isset($_POST['milkpriceformula_id'])) {
            $milkpriceformula_id = $_POST['milkpriceformula_id'];
            $dt->setId($_POST['milkpriceformula_id']);
            $milkpriceData['milkpriceformula_editby'] = session::get('user_id');
            $milkpriceData['milkpriceformula_editdate'] = date('Y-m-d H:i:s');
            $dt->setDataArray($milkpriceData);
            $query1 = $dt->update();
        } else {
            $milkpriceformula_id = fn::generate_id(true);
            $milkpriceData['milkpriceformula_id'] = $milkpriceformula_id;
            $milkpriceData['milkpriceformula_entryby'] = session::get('user_id');
            $milkpriceData['milkpriceformula_entrydate'] = date('Y-m-d H:i:s');
            $dt->setDataArray($milkpriceData);
            $query1 = $dt->insert();
        }
        if ($query1) {
            $condition = load::model('milkpricecondition');
            if (isset($_POST['milkpriceformula_id'])) {
                $condition->setCondition('milkpricecondition_milkpriceformula = \''.$_POST['milkpriceformula_id']);
                $condition->delete();
            }
            $error = 0;
            foreach ($_POST['milkpricecondition_element'] as $k => $v) {
                if (!empty($v)) {
                    $dtcondition['milkpricecondition_id'] = fn::generate_id(true);
                    $dtcondition['milkpricecondition_element'] = $v;
                    $dtcondition['milkpricecondition_milkpriceformula'] = $milkpriceformula_id;
                    $dtcondition['milkpricecondition_standardvalue'] = $_POST['milkpricecondition_standardvalue'][$k];
                    $dtcondition['milkpricecondition_addprice'] = $_POST['milkpricecondition_addprice'][$k];
                    $dtcondition['milkpricecondition_interval'] = $_POST['milkpricecondition_interval'][$k];
                    $dtcondition['milkpricecondition_korelasi'] = $_POST['milkpricecondition_korelasi'][$k];
                    $condition->setDataArray($dtcondition);
                    if (!$condition->insert()) {
                        ++$error;
                    }
                }
            }
            if ($error > 0) {
                session::set('errorMsg', _('Failed while saving data into database'));
            } else {
                session::set('errorMsg', _('Data has been successfully saved'));
            }
        } else {
            session::set('errorMsg', _('Failed while saving data into database'));
        }
        header('location:'.BASE_URL.'index.php?/milkprice/index');
    }

    public function add()
    {
        session::set('title', 'Milkprice Formulation');
        $data['element'] = array(
            'fat' => 'FAT',
            'snf' => 'SNF',
            'fp' => 'FPD',
            'cfu' => 'TPC',
            'urea' => 'UREA',
            'acidity' => 'ACIDITY',
            'protein' => 'PROTEIN',
            'ibc' => 'IBC',
            );
        View::display('milkprice/milkprice.add', $data);
    }

    public function test()
    {
        $data['element'] = array(
            'fat' => 'FAT',
            'snf' => 'SNF',
            'fp' => 'FPD',
            'cfu' => 'TPC',
            'urea' => 'UREA',
            'acidity' => 'ACIDITY',
            'protein' => 'PROTEIN',
            'ibc' => 'IBC',
            );
        $dt = load::model('milkpriceformula');
        $koperasi = load::model('koperasi');
        $data['formula'] = array();
        if (!in_array(session::get('user_group'), Config::$application['super_group'])) {
            $_GET['milkpriceformula_koperasi'] = session::get('user_koperasi');
        } else {
            $koperasi->setCondition('koperasi_status = \'y\'');
            $data['koperasi'] = $koperasi->read();
        }
        if (isset($_GET['milkpriceformula_koperasi'])) {
            $dt->setCondition('milkpriceformula_status != \'d\' AND milkpriceformula_koperasi = \''.$_GET['milkpriceformula_koperasi'].'\'');
            $data['formula'] = $dt->read();
        }
        $data['formuladetail'] = array();
        if (isset($_GET['milkpriceformula_id'])) {
            if (empty($_GET['milkpriceformula_id'])) {
                session::set('errorMsg', _('Invalid Formulation ID'));
                header('location:'.BASE_URL.'index.php?/milkprice/test');
                exit();
            }
            $dt->setId($_GET['milkpriceformula_id']);
            $formula = $dt->read();
            if ($formula['total_data'] > 0) {
                $data['formuladetail'] = $formula['records'];
            } else {
                session::set('errorMsg', _('Invalid Formulation ID'));
                header('location:'.BASE_URL.'index.php?/milkprice/test');
                exit();
            }
        }
        View::display('milkprice/milkprice.test', $data);
    }
    public function calculate($milkpriceformula_id = '')
    {
        if (!empty($milkpriceformula_id)) {
            echo $this->getStandardPrice($milkpriceformula_id);
        }
    }

    public function getStandardPrice($milkpriceformula_id)
    {
        $dt = load::model('milkpriceformula');
        $dt->setId($milkpriceformula_id);
        $data = $dt->read();
        $hargadasar = (float) $data['records']['milkpriceformula_hargadasar'];

        $selisih = array();
        $beda = 0;
        foreach ($data['records']['milkpricecondition'] as $k => $v) {
            if ($v['milkpricecondition_standardvalue'] != $_POST['milkpricecondition_element'][$v['milkpricecondition_element']]) {
                ++$beda;

                if ($v['milkpricecondition_korelasi'] == 'p') {
                    if ($_POST['milkpricecondition_element'][$v['milkpricecondition_element']] > $v['milkpricecondition_standardvalue']) {
                        $pembagi = (float) ((float) $_POST['milkpricecondition_element'][$v['milkpricecondition_element']] - (float) $v['milkpricecondition_standardvalue']) / (float) $v['milkpricecondition_interval'];
                        $selisih[$v['milkpricecondition_element']] = ($pembagi * $v['milkpricecondition_addprice']);
                    } else {
                        $pembagi = (float) ((float) $v['milkpricecondition_standardvalue'] - (float) $_POST['milkpricecondition_element'][$v['milkpricecondition_element']])  / (float) $v['milkpricecondition_interval'];
                        $selisih[$v['milkpricecondition_element']] = -($pembagi * $v['milkpricecondition_addprice']);
                    }
                } else {
                    if ($_POST['milkpricecondition_element'][$v['milkpricecondition_element']] > $v['milkpricecondition_standardvalue']) {
                        $pembagi = (float) ((float) $_POST['milkpricecondition_element'][$v['milkpricecondition_element']] - (float) $v['milkpricecondition_standardvalue']) / (float) $v['milkpricecondition_interval'];
                        $selisih[$v['milkpricecondition_element']] = -($pembagi * $v['milkpricecondition_addprice']);
                    } else {
                        $pembagi = (float) ((float) $v['milkpricecondition_standardvalue'] - (float) $_POST['milkpricecondition_element'][$v['milkpricecondition_element']])  / (float) $v['milkpricecondition_interval'];
                        $selisih[$v['milkpricecondition_element']] = ($pembagi * $v['milkpricecondition_addprice']);
                    }
                }
            } else {
                $selisih[$v['milkpricecondition_element']] = 0;
            }
        }
        if ($beda === 0) {
            return number_format($hargadasar, 2);
        } else {
            foreach ($selisih as $k => $v) {
                $hargadasar = (float) ($hargadasar + $v);
            }

            return number_format($hargadasar, 2);
        }
    }

    public function report($overrideAnggota = 'none')
    {
        $anggota = load::model('anggota');
        $qcparameter = load::model('qcparameter');
        $milkpriceformula = load::model('milkpriceformula');
        $milkpricecondition = load::model('milkpricecondition');
        $coop = load::model('koperasi');
        $periode = load::model('periode');
        $rezgrade = load::model('rezgrade');
        $fpgrade = load::model('fpgrade');
        $tpk = load::model('tpk');

        $_GET['page'] = !isset($_GET['page']) ? 1 : $_GET['page'];
        $_GET['keyword'] = !isset($_GET['keyword']) ? '' : $_GET['keyword'];
        $_GET['month'] = !isset($_GET['month']) ? date('n') : $_GET['month'];
        $_GET['year'] = !isset($_GET['year']) ? date('Y') : $_GET['year'];
        $_GET['periode'] = !isset($_GET['periode']) ? '' : $_GET['periode'];
        $_GET['koperasi'] = !isset($_GET['koperasi']) ? '' : $_GET['koperasi'];
        $_GET['tpk'] = !isset($_GET['tpk']) ? '' : $_GET['tpk'];

        if ($overrideAnggota == 'override') {
            $dataAnggota = $anggota->setCondition('anggota_nomor = \''.session::get('user_name').'\' AND anggota_status = \'y\' AND t_card.card_status = \'y\' AND t_cow.cow_status = \'y\'')->setLimit(1)->read();
            $_GET['anggota'] = $dataAnggota['records'][0]['anggota_id'];
        } else {
            $_GET['anggota'] = !isset($_GET['anggota']) ? '' : $_GET['anggota'];
        }

        if (!in_array(session::get('user_group'), Config::$application['super_group'])) {
            $koperasi = session::get('user_koperasi');
            $dtKoperasi = $coop->setCondition('koperasi_id = \''.$koperasi.'\'')->read();
            $_GET['koperasi'] = $koperasi;
        } else {
            $dtKoperasi = $coop->setCondition('koperasi_status = \'y\'')->setLimit(0)->read();
            if (isset($_GET['koperasi']) && !empty($_GET['koperasi'])) {
                $koperasi = $_GET['koperasi'];
            } else {
                $koperasi = $dtKoperasi['records'][0]['koperasi_id'];
                $_GET['koperasi'] = $koperasi;
            }
        }
        $data['koperasi'] = $dtKoperasi;

        $tpkSQL = 'tpk_status = \'y\'';
        if (!empty($_GET['koperasi'])) {
            $tpkSQL .= ' AND tpk_koperasi = \''.$_GET['koperasi'].'\'';
        }

        $tpk->setCondition($tpkSQL);
        $tpk->setLimit(0);
        $dataTpk = $tpk->read();
        $data['tpk'] = $dataTpk;
        /* Data periode */
        /* $dtPeriode = $periode->setCondition('periode_status = \'y\' AND periode_koperasi = \''.$koperasi.'\' AND date_part(\'month\', periode_effectivedate) <= '.$_GET['month'].' AND date_part(\'year\', periode_effectivedate) <= '.$_GET['year'])
                             ->setLimit(1)
                             ->setOrderBy('periode_effectivedate')
                             ->setOrderType('ASC')
                             ->read(); */
        $tglCekPeriode = date('Y-m-t', strtotime($_GET['year'].'-'.$_GET['month'].'-01'));

        /* Data periode */
        $dtPeriode = $periode->setCondition('periode_status = \'y\' AND periode_koperasi = \''.$koperasi.'\' AND DATE(periode_effectivedate) <= \''.$tglCekPeriode.'\'')
                             ->setLimit(1)
                             ->setOrderBy('periode_effectivedate')
                             ->setOrderType('DESC')
                             ->read();
        if ($dtPeriode['total_data'] === 0) {
            session::set('errorMsg', 'Please setup periode!');
            header('location:'.BASE_URL.'index.php?/dashboard');
            exit();
        }

        $arrayPeriode = $dtPeriode['records'][0]['periodedetail'];
        if (!empty($_GET['anggota'])) {
            $anggota->setCondition('anggota_status = \'y\' AND anggota_koperasi = \''.$koperasi.'\' AND anggota_id = \''.$_GET['anggota'].'\' AND anggota_status != \'d\' AND t_card.card_status = \'y\' AND t_cow.cow_status = \'y\'');
        } else {
            if (!empty($_GET['tpk'])) {
                $anggota->setCondition('anggota_status = \'y\' AND anggota_koperasi = \''.$koperasi.'\' AND anggota_tpk = \''.$_GET['tpk'].'\' AND anggota_status != \'d\' AND t_card.card_status = \'y\' AND t_cow.cow_status = \'y\'');
            } else {
                $anggota->setCondition('anggota_status = \'y\' AND anggota_koperasi = \''.$koperasi.'\' AND anggota_status != \'d\' AND t_card.card_status = \'y\' AND t_cow.cow_status = \'y\'');
            }
        }

        /*
         * EXPORT
         */
        if (isset($_GET['export']) && !empty($_GET['export'])) {
            $anggota->setLimit(0);
            $anggota->setPage(1);
        } else {
            $anggota->setPage($_GET['page']);
        }

        $anggota->setOrderBy('anggota_nama');
        if (isset($_GET['keyword']) && !empty($_GET['keyword'])) {
            $anggota->setKeyword($_GET['keyword']);
            $anggota->setSearchIn('anggota_nama');
        }

        $dataAnggota = $anggota->read();
        $anggotaList = '';
        $dataNya = array();
        //echo count($dataAnggota['records']);exit();
        foreach ($dataAnggota['records'] as $key => $val) {
            if ($anggotaList != '') {
                $anggotaList .= ',';
            }
            $anggotaList .= '\''.$val['anggota_id'].'\'';
            //array_push($dataNya,$val['anggota_id'])
            foreach ($arrayPeriode as $k => $v) {
                $dataNya[$val['anggota_id']][$v['periodedetail_name']] = $val;
                $dataNya[$val['anggota_id']][$v['periodedetail_name']]['hargadasar'] = 0;
                $dataNya[$val['anggota_id']][$v['periodedetail_name']]['hargafp'] = 0;
                $dataNya[$val['anggota_id']][$v['periodedetail_name']]['hargarez'] = 0;
                $dataNya[$val['anggota_id']][$v['periodedetail_name']]['qcparameter_cfu'] = 0;
                $dataNya[$val['anggota_id']][$v['periodedetail_name']]['qcparameter_ibc'] = 0;
                $dataNya[$val['anggota_id']][$v['periodedetail_name']]['qcparameter_fat'] = 0;
                $dataNya[$val['anggota_id']][$v['periodedetail_name']]['qcparameter_protein'] = 0;
                $dataNya[$val['anggota_id']][$v['periodedetail_name']]['qcparameter_lactose'] = 0;
                $dataNya[$val['anggota_id']][$v['periodedetail_name']]['qcparameter_ts'] = 0;
                $dataNya[$val['anggota_id']][$v['periodedetail_name']]['qcparameter_fp'] = 0;
                $dataNya[$val['anggota_id']][$v['periodedetail_name']]['qcparameter_acidity'] = 0;
                $dataNya[$val['anggota_id']][$v['periodedetail_name']]['qcparameter_urea'] = 0;
                $dataNya[$val['anggota_id']][$v['periodedetail_name']]['qcparameter_snf'] = 0;
            }
        }

        /* Formula */
        /* $dataFormula = $milkpriceformula->setCondition('milkpriceformula_koperasi = \''.$koperasi.'\' AND milkpriceformula_status = \'y\' AND date_part(\'month\',milkpriceformula_effectivedate) <= \''.$_GET['month'].'\' AND date_part(\'year\',milkpriceformula_effectivedate) <= \''.$_GET['year'].'\'')
                                                ->setLimit(1)
                                                ->setOrderBy('milkpriceformula_effectivedate')
                                                ->setOrderType('ASC')
                                                ->read(); */
        $dataFormula = $milkpriceformula->setCondition('milkpriceformula_koperasi = \''.$koperasi.'\' AND milkpriceformula_status = \'y\' AND DATE(milkpriceformula_effectivedate) <= \''.$tglCekPeriode.'\'')
                                                ->setLimit(1)
                                                ->setOrderBy('milkpriceformula_effectivedate')
                                                ->setOrderType('DESC')
                                                ->read();

        /* $dataRezgrade = $rezgrade->setCondition('rezgrade_koperasi = \''.$koperasi.'\' AND date_part(\'month\',rezgrade_effectivedate) <= '.$_GET['month'].' AND date_part(\'year\',rezgrade_effectivedate) <= '.$_GET['year'])
                                 ->setLimit(1)
                                 ->setOrderBy('rezgrade_effectivedate')
                                 ->setOrderType('ASC')
                                 ->read(); */
        $dataRezgrade = $rezgrade->setCondition('rezgrade_koperasi = \''.$koperasi.'\' AND DATE(rezgrade_effectivedate) <= \''.$tglCekPeriode.'\'')
                                 ->setLimit(1)
                                 ->setOrderBy('rezgrade_effectivedate')
                                 ->setOrderType('DESC')
                                 ->read();

        /* $dataFpgrade = $fpgrade->setCondition('fpgrade_koperasi = \''.$koperasi.'\' AND date_part(\'month\',fpgrade_effectivedate) <= '.$_GET['month'].' AND date_part(\'year\',fpgrade_effectivedate) <= '.$_GET['year'])
                                 ->setLimit(1)
                                 ->setOrderBy('fpgrade_effectivedate')
                                 ->setOrderType('ASC')
                                 ->read(); */
        $dataFpgrade = $fpgrade->setCondition('fpgrade_koperasi = \''.$koperasi.'\' AND DATE(fpgrade_effectivedate) <= \''.$tglCekPeriode.'\'')
                                 ->setLimit(1)
                                 ->setOrderBy('fpgrade_effectivedate')
                                 ->setOrderType('DESC')
                                 ->read();

        if ($anggotaList != '') {
            /* Revisi 1.4: perhitungan periode berdasarkan tanggal transaksi */
            // $dataParameter = $qcparameter->setCondition('qcparameter_anggota IN ('.$anggotaList.') AND date_part(\'month\',t_transaksi.transaksi_waktu) = \''.$_GET['month'].'\' AND date_part(\'year\',t_transaksi.transaksi_waktu) = \''.$_GET['year'].'\' AND qcparameter_status = \'y\'')
            // 							 ->setLimit(0)
            // 							 ->read();
            /* End Revisi 1.4 */

            /* Revisi 1.5: perhitungan periode berdasarkan tanggal scan bacto */
            $dataParameter = $qcparameter->setCondition('qcparameter_anggota IN ('.$anggotaList.') AND date_part(\'month\',qcparameter_scandate) = \''.$_GET['month'].'\' AND date_part(\'year\',qcparameter_scandate) = \''.$_GET['year'].'\' AND qcparameter_status = \'y\'')
                                         ->setLimit(0)
                                         ->read();
            /* End Revisi 1.5*/

            /*
            * Fix double data
            **/

            $dataParameterFarmer = array();
            foreach ($dataParameter['records'] as $key => $val) {
                if (empty($val['qcparameter_scandate'])) {
                    $val['qcparameter_scandate'] = $val['transaksi_waktu'];
                }

                foreach ($arrayPeriode as $prk => $prv) {
                    if ($prv['periodedetail_startdate'] <= (int) date('j', strtotime($val['qcparameter_scandate'])) && $prv['periodedetail_enddate'] >= (int) date('j', strtotime($val['qcparameter_scandate']))) {
                        $val['transaksi_periode'] = $prv['periodedetail_name'];
                    }
                }

                if (!isset($dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']])) {
                    $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']] = $val;
                } else {
                    if ((float) $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_cfu'] <= 0) {
                        $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_cfu'] = $val['qcparameter_cfu'];
                    }
                    if ((float) $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_ibc'] <= 0) {
                        $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_ibc'] = $val['qcparameter_ibc'];
                    }
                    if ((float) $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_fat'] <= 0) {
                        $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_fat'] = $val['qcparameter_fat'];
                    }
                    if ((float) $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_protein'] <= 0) {
                        $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_protein'] = $val['qcparameter_protein'];
                    }
                    if ((float) $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_lactose'] <= 0) {
                        $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_lactose'] = $val['qcparameter_lactose'];
                    }
                    if ((float) $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_ts'] <= 0) {
                        $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_ts'] = $val['qcparameter_ts'];
                    }
                    if ((float) $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_fp'] <= 0) {
                        $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_fp'] = $val['qcparameter_fp'];
                    }
                    if ((float) $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_acidity'] <= 0) {
                        $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_acidity'] = $val['qcparameter_acidity'];
                    }
                    if ((float) $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_urea'] <= 0) {
                        $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_urea'] = $val['qcparameter_urea'];
                    }
                    if ((float) $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_snf'] <= 0) {
                        $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_snf'] = $val['qcparameter_snf'];
                    }
                }
            }

            foreach ($dataParameterFarmer as $pkey => $pval) {
                /* Revisi 1.6 */
                foreach ($pval as $key => $val) {
                    $hargadasar = (float) $dataFormula['records'][0]['milkpriceformula_hargadasar'];
                    $hargarez = 0;
                    $hargafp = 0;
                    $selisih = array();
                    $beda = 0;

                    /* Revisi 1.4: perhitungan periode berdasarkan tanggal transaksi */
                    /* Fix Empty Periode */
                    // if((int)$val['transaksi_periode'] < 1){
                    // 	foreach($arrayPeriode as $prk=>$prv){
                    // 		if($prv['periodedetail_startdate'] <= (int) date('j',strtotime($val['transaksi_waktu'])) && $prv['periodedetail_enddate'] >= (int) date('j',strtotime($val['transaksi_waktu']))){
                    // 			$val['transaksi_periode'] = $prv['periodedetail_name'];
                    // 		}

                    // 	}
                    // }
                    /* End Revisi 1.4 */

                    /* Revisi 1.5: Ganti perhitungan periode berdasarkan tanggal scan bacto / milko */
                    /* Kalau scandate kosong gunakan transaksi_waktunya */

                    /* Revisi 1.6: Dihapus karena sudah di define di atas */

                    // if(empty($val['qcparameter_scandate'])){
                    // 	$val['qcparameter_scandate'] = $val['transaksi_waktu'];
                    // }

                    // foreach($arrayPeriode as $prk=>$prv){
                    // 	if($prv['periodedetail_startdate'] <= (int) date('j',strtotime($val['qcparameter_scandate'])) && $prv['periodedetail_enddate'] >= (int) date('j',strtotime($val['qcparameter_scandate']))){
                    // 		$val['transaksi_periode'] = $prv['periodedetail_name'];
                    // 	}

                    // }
                    /* End Revisi 1.6 */
                    /* END Revisi 1.5 */

                    foreach ($dataFormula['records'][0]['milkpricecondition'] as $k => $v) {
                        if ($v['milkpricecondition_standardvalue'] != $val['qcparameter_'.$v['milkpricecondition_element']]) {
                            ++$beda;

                            if ($v['milkpricecondition_korelasi'] == 'p') {
                                if ($val['qcparameter_'.$v['milkpricecondition_element']] > $v['milkpricecondition_standardvalue']) {
                                    $pembagi = (float) ((float) $val['qcparameter_'.$v['milkpricecondition_element']] - (float) $v['milkpricecondition_standardvalue']) / (float) $v['milkpricecondition_interval'];
                                    $selisih[$v['milkpricecondition_element']] = ($pembagi * $v['milkpricecondition_addprice']);
                                } else {
                                    $pembagi = (float) ((float) $v['milkpricecondition_standardvalue'] - (float) $val['qcparameter_'.$v['milkpricecondition_element']])  / (float) $v['milkpricecondition_interval'];
                                    $selisih[$v['milkpricecondition_element']] = -($pembagi * $v['milkpricecondition_addprice']);
                                }
                            } else {
                                if ($val['qcparameter_'.$v['milkpricecondition_element']] > $v['milkpricecondition_standardvalue']) {
                                    $pembagi = (float) ((float) $val['qcparameter_'.$v['milkpricecondition_element']] - (float) $v['milkpricecondition_standardvalue']) / (float) $v['milkpricecondition_interval'];
                                    $selisih[$v['milkpricecondition_element']] = -($pembagi * $v['milkpricecondition_addprice']);
                                } else {
                                    $pembagi = (float) ((float) $v['milkpricecondition_standardvalue'] - (float) $val['qcparameter_'.$v['milkpricecondition_element']])  / (float) $v['milkpricecondition_interval'];
                                    $selisih[$v['milkpricecondition_element']] = ($pembagi * $v['milkpricecondition_addprice']);
                                }
                            }
                        } else {
                            $selisih[$v['milkpricecondition_element']] = 0;
                        }
                    }
                    if ($beda === 0) {
                        //return number_format($hargadasar,2);
                    } else {
                        foreach ($selisih as $k => $v) {
                            $hargadasar = (float) ($hargadasar + $v);
                        }
                    }

                    $currentRezGrade = 0;
                    $currentRezValue = ($val['qcparameter_cfu'] * 1000); // Dikali 1000 karena output dari bactoscan dikali 1000

                    foreach ($dataRezgrade['records'][0]['rezgradedetail'] as $rk => $rv) {
                        if ($rv['rezgradedetail_tpcstart'] <= $currentRezValue && $rv['rezgradedetail_tpcend'] >= $currentRezValue) {
                            $currentRezGrade = $rv['rezgradedetail_grade'];
                            $hargarez = $rv['rezgradedetail_price'];
                        }
                    }

                    $currentFpGrade = 0;
                    $currentFpValue = $val['qcparameter_fp'];

                    /**
                     * Fix issue #1 (http://git.transformatika.com/agung/mcp-server/issues/1)
                     */
                    if (is_array($dataFpgrade) && isset($dataFpgrade['records']) && count($dataFpgrade['records']) > 0) {
                        foreach ($dataFpgrade['records'][0]['fpgradedetail'] as $fk => $fv) {
                            if ($fv['fpgradedetail_fpstart'] <= $currentFpValue && $fv['fpgradedetail_fpend'] >= $currentFpValue) {
                                $currentFpGrade = $fv['fpgradedetail_grade'];
                                $hargafp = $fv['fpgradedetail_price'];
                            }
                        }
                    }

                    $dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_cfu'] = $val['qcparameter_cfu'];
                    $dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_ibc'] = $val['qcparameter_ibc'];
                    $dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_fat'] = $val['qcparameter_fat'];
                    $dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_protein'] = $val['qcparameter_protein'];
                    $dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_lactose'] = $val['qcparameter_lactose'];
                    $dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_ts'] = $val['qcparameter_ts'];
                    $dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_fp'] = $val['qcparameter_fp'];
                    $dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_acidity'] = $val['qcparameter_acidity'];
                    $dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_urea'] = $val['qcparameter_urea'];
                    $dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_snf'] = $val['qcparameter_snf'];
                    $dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['hargadasar'] = $hargadasar;
                    $dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['hargafp'] = $hargafp;
                    $dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['hargarez'] = $hargarez;
                }
            }
        }

        $res = $dataAnggota;
        $res['records'] = $dataNya;
        $data['data'] = $res;
        $data['periode'] = $dtPeriode;

        /*
         * EXPORT TO EXCEL
         */
        if (isset($_GET['export']) && !empty($_GET['export'])) {
            $header = array(
                'PERIODE',
                'FARMER ID',
                'NAME',
                'TPC',
                'FAT',
                'PROTEIN',
                'LACTOSE',
                'TS',
                'FP',
                'ACIDITY',
                'UREA',
                'SNF',
                'STANDARD PRICE',
                'REZ PENALTY',
                'FP PENALTY',
                'MILK PRICE',
                );
            $xlsrow = array();
            foreach ($dtPeriode['records'][0]['periodedetail'] as $key => $val) {
                foreach ($res['records'] as $k => $v) {
                    $row = '';
                    $row .= $val['periodedetail_name']."\t";
                    $row .= $v[$val['periodedetail_name']]['anggota_nomor']."\t";
                    $row .= $v[$val['periodedetail_name']]['anggota_nama']."\t";
                    $row .= ((float) $v[$val['periodedetail_name']]['qcparameter_cfu'])."\t";
                    $row .= ((float) $v[$val['periodedetail_name']]['qcparameter_fat'])."\t";
                    $row .= ((float) $v[$val['periodedetail_name']]['qcparameter_protein'])."\t";
                    $row .= ((float) $v[$val['periodedetail_name']]['qcparameter_lactose'])."\t";
                    $row .= ((float) $v[$val['periodedetail_name']]['qcparameter_ts'])."\t";
                    $row .= ((float) $v[$val['periodedetail_name']]['qcparameter_fp'])."\t";
                    $row .= ((float) $v[$val['periodedetail_name']]['qcparameter_acidity'])."\t";
                    $row .= ((float) $v[$val['periodedetail_name']]['qcparameter_urea'])."\t";
                    $row .= ((float) $v[$val['periodedetail_name']]['qcparameter_snf'])."\t";
                    $row .= round($v[$val['periodedetail_name']]['hargadasar'])."\t";
                    $row .= round($v[$val['periodedetail_name']]['hargarez'])."\t";
                    $row .= round($v[$val['periodedetail_name']]['hargafp'])."\t";
                    $row .= round((float) ($v[$val['periodedetail_name']]['hargadasar'] + $v[$val['periodedetail_name']]['hargarez'] + $v[$val['periodedetail_name']]['hargafp']));
                    $xlsrow[] = $row;
                }
            }

            $filename = 'MilkPrice-'.$_GET['year'].'-'.$_GET['month'].'.xls';

            header("Content-Disposition: attachment; filename=\"$filename\"");
            header('Content-Type: application/vnd.ms-excel');
            echo implode("\t", $header)."\n";
            echo implode("\n", $xlsrow);
            exit();
        }

        if (!empty($_GET['anggota'])) {
            $data['prepopulate'][] = array(
                        'id' => $_GET['anggota'],
                        'name' => $dataAnggota['records'][0]['anggota_nama'],
                    );
        } else {
            $data['prepopulate'] = array();
        }

        //print_r($data);exit();
        if ($overrideAnggota == 'override') {
            View::display('milkprice/milkprice.reportFarmer', $data);
        } else {
            View::display('milkprice/milkprice.report', $data);
        }
    }
    public function reportFarmer()
    {
        $this->report('override');
    }
}
