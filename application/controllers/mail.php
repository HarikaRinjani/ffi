<?php
/**
 * Created by PhpStorm.
 * User: ag
 * Date: 5/22/14
 * Time: 9:15 AM
 */
class Mail_Controller extends Controller{
    public $version = 1.2;
    public $publisher = 'Transformatika';
    public $description = 'Internal Mail';
    public $dependencies = array(
        array(
            'name'=>'dashboard',
            'version'=>1
        ),
        array(
            'name'=>'profile',
            'version'=>1
        )
    );
    public $buildConfiguration = array(
        'views'=>array(
            'mail.index',
            'mail.check',
            'mail.read',
            'mail.reply',
            'mail.compose'
        ),
        'models'=>'inbox',
        'table'=>'inbox'
    );
    function __construct(){
        security::checkSession();
    }
    function index(){
        session::set('title','My Inbox');
        $dt = load::model('inbox');
        $dt->setCondition('inbox_user = \''.session::get('user_id').'\' AND inbox_status != \'d\'');
        $dt->setOrderBy('inbox_date');
        $dt->setOrderType('DESC');
        $dt->setLimit(Config::$application['display_limit']);
        $data['mail'] = $dt->read();
        View::display('mail/mail.index',$data);
    }

    function trash(){
        session::set('title','My Inbox');
        $dt = load::model('inbox');
        $dt->setCondition('inbox_user = \''.session::get('user_id').'\' AND inbox_status = \'d\'');
        $dt->setOrderBy('inbox_date');
        $dt->setOrderType('DESC');
        $dt->setLimit(Config::$application['display_limit']);
        $data['mail'] = $dt->read();
        View::display('mail/mail.trash',$data);
    }

    function sent(){
        session::set('title','Sent Mail');
        $dt = load::model('inbox');
        $user = load::model('user');
        $dt->setCondition('inbox_sender = \''.session::get('user_id').'\'');
        $dt->setOrderBy('inbox_date');
        $dt->setOrderType('DESC');
        $dt->setLimit(Config::$application['display_limit']);
        $mail = $dt->read();
        $mailarray = array();
        foreach($mail['records'] as $k=>$v){
            $mailarray[$k] = $v;
            $userdata = $user->setId($v['inbox_user'])->read();
            $mailarray[$k]['inbox_userdata'] = $userdata['records'];
        }
        
        $data['mail'] = $mail;
        $data['mail']['records'] = $mailarray;
        View::display('mail/mail.sent',$data);
    }

    function sentread($mail_id = ''){
        $dt = load::model('inbox');
        $user = load::model('user');
        $dt->setId($mail_id);
        $data = $dt->read();
        $user->setId($data['records']['inbox_user']);
        $userdata = $user->read();
        $data['records']['userdata'] = $userdata['records'];
        echo json_encode($data['records']);
    }

    function inbox(){
        session::set('title','My Inbox');
        $page = !isset($_GET['page']) ? 1 : $_GET['page'];
        $dt = load::model('inbox');
        $dt->setPage($page);
        if(isset($_GET['keyword'])){
            $dt->setKeyword($_GET['keyword']);
            $dt->setSearchIn(array('inbox_title','inbox_content'));
        }
        $dt->setCondition('inbox_user = \''.session::get('user_id').'\' AND inbox_status != \'d\'');
        $dt->setOrderBy('inbox_date');
        $dt->setOrderType('DESC');
        $dt->setLimit(Config::$application['display_limit']);
        $data['mail'] = $dt->read();
        View::display('mail/mail.index',$data);
    }
    function check(){
        $dt = load::model('inbox');
        $dt->setCondition('inbox_user = \''.session::get('user_id').'\'');
        $dt->setOrderBy('inbox_date');
        $dt->setOrderType('DESC');
        $dt->setLimit(5);
        $data['mail'] = $dt->read();
        View::display('mail/mail.check',$data);
    }
    function searchUser() {
        $dt = load::model('user');
        $_POST['q'] = !isset($_POST['q']) ? '' : strtolower($_POST['q']);
        $dt->setKeyword($_POST['q']);
        $dt->setSearchIn('user_fullname');
        $dt->setCondition('user_status = \'y\'');
        $dt->setOrderBy('user_fullname');

        $members = $dt->read();

        $res        = array();
        $i          = 0;
        foreach ($members['records'] as $key => $val) {
            $res[$i]['id']   = $val['user_id'];
            if($val['user_koperasi'] == 'none'){
                $res[$i]['name'] = htmlentities($val['user_fullname'].' <FFI>');
            }else{
                $res[$i]['name'] = htmlentities($val['user_fullname'].' <'.$val['koperasi_nama'].'>');
            }
            
            $i++;
        }
        echo json_encode($res);
    }
    function compose(){
        session::set('title','Write mail');
        View::display('mail/mail.compose');
    }

    function send(){
        $dt = load::model('inbox');
        load::library('fn');
        $_POST['inbox_title'] = empty($_POST['inbox_title']) ? 'Untitled' : $_POST['inbox_title'];
        $_POST['inbox_date']   = date('Y-m-d H:i:s');
        $_POST['inbox_status'] = 'y';
        $_POST['inbox_read']   = 'n';
        $_POST['inbox_sender'] = session::get('user_id');
        $folder = 'mail/' . date('Y') . '/' . date('n');
        $real_folder = 'mail'.DS.date('Y').DS.date('n');
        if (!empty($_FILES['inbox_attachment']['name'])) {
            fn::createDirectory($folder);
            $filename = fn::getId() . $_FILES['inbox_attachment']['name'];
            move_uploaded_file($_FILES['inbox_attachment']['tmp_name'], Config::$application['file_path'].$real_folder . DS . $filename);
            $_POST['inbox_attachment'] = $folder . '/' . $filename;
        }
        $kepada                = explode(',', $_POST['inbox_user']);
        $data = $_POST;
        foreach ($kepada as $val) {
            $data['inbox_user'] = $val;
            $data['inbox_id']   = fn::getId();
            $dt->setDataArray($data);
            if ($dt->insert()) {
                $error = false;
            } else {
                $error = true;
                fn::log_error(_('sending mail failed'));
            }
        }
        if (!$error) {
            session::set('errorMsg', _('Your mail has been sent'));
        } else {
            session::set('errorMsg', _('Cannot sent your mail'));

        }
        header('location:'.BASE_URL.'index.php?/mail');
    }

    function read($id=''){
        if(!empty($id)){

            $dt = load::model('inbox');
            $dt->setId($dt->escapeKeyword($id));
            $dt->setData('inbox_read','y');
            $dt->update();
            $dt->setId($dt->escapeKeyword($id));
            $mail = $dt->read();
            if($mail['total_data'] > 0){
                $data['mail'] = $mail['records'];
                session::set('title','Read Mail: '.$mail['records']['inbox_title']);
                View::display('mail/mail.read',$data);
            }
        }
    }
    function trashread($id=''){
        if(!empty($id)){

            $dt = load::model('inbox');
            $dt->setId($dt->escapeKeyword($id));
            $dt->setData('inbox_read','y');
            $dt->update();
            $dt->setId($dt->escapeKeyword($id));
            $mail = $dt->read();
            echo json_encode($mail['records']);
        }
    }
    function reply($id=''){
        if(!empty($id)){
            $dt = load::model('inbox');
            $dt->setId($dt->escapeKeyword($id));
            $mail = $dt->read();
            if($mail['total_data'] > 0){
                $data['mail'] = $mail['records'];
                session::set('title','Reply Mail: '.$mail['records']['inbox_title']);
                View::display('mail/mail.reply',$data);
            }
        }
    }
    function delete(){
        if(isset($_POST['inbox_id'])){
            $dt = load::model('inbox');
            $dt->setId($_POST['inbox_id']);
            if($dt->delete()){
                echo 'success';
                session::set('errorMsg',_('Mail deleted'));
            }else{
                echo 'failed';
                session::set('errorMsg',_('Failed deleting mail'));
            }
        }
    }
}