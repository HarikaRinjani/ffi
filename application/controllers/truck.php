<?php
class Truck_Controller extends Controller{
    public $description = 'Truck Settings (MCP)';
    public $version = 1.2;
    public $buildConfiguration = array(
        'table'=>'truck',
        'models'=>'truck',
        'views'=>array(
            'truck.setting'
        )
    );
    function __construct(){
        security::checkSession();
    }
    function index(){

    }

    function setting($operation='',$id=''){
        session::set('title','Truck Settings');
        load::library('fn');
        $truck = load::model('truck');
        switch($operation){
            case 'save':
                if(isset($_POST['truck_id'])){
                    $truck->setId($_POST['truck_id']);
                    $truck->setDataArray($_POST);
                    $query = $truck->update();
                }else{
                    $_POST['truck_id'] = fn::generate_id(true);
                    $truck->setDataArray($_POST);
                    $query = $truck->insert();
                }
                if($query){
                    session::set('errorMsg',_('Data has been successfully saved'));
                }else{
                    session::set('errorMsg',_('Failed while saving data into database'));
                }
                header('location:'.BASE_URL.'index.php?/truck/setting');

                break;
            case 'edit':
                if(!empty($id)){
                    if(!in_array(session::get('user_group'),Config::$application['super_group'])){
                        $truck->setCondition('truck_koperasi = \''.session::get('user_koperasi').'\' AND truck_id = \''.$id.'\'');
                        $truck->setLimit(1);
                        $dept = $truck->read();
                        echo json_encode($dept['records'][0]);
                    }else{
                        $truck->setId($id);
                        $dept = $truck->read();
                        echo json_encode($dept['records']);
                    }
                    
                }

                break;
            case 'delete':
                if(isset($_POST['truck_id'])){
                    if(!in_array(session::get('user_group'),session::get('super_group'))){
                        $truck->setCondition('truck_koperasi = \''.session::get('user_koperasi').'\' AND truck_id = \''.$_POST['truck_id'].'\'');
                    }else{
                        $truck->setId($_POST['truck_id']);
                    }
                    
                    if($truck->delete()){
                        session::set('errorMsg',_('Data has been successfully deleted'));
                    }else{
                        session::set('errorMsg',_('Failed while deleting data'));
                    }
                }
                header('location:'.BASE_URL.'index.php?/truck/setting');

                break;
            default:
                $koperasi = load::model('koperasi');
                $_GET['page'] = !isset($_GET['page']) ? 1 : $_GET['page'];
                $truck->setPage($_GET['page']);
                if(isset($_GET['keyword'])){
                    $truck->setKeyword($_GET['keyword']);
                    $truck->setSearchIn(array(
                    't_koperasi.koperasi_nama',
                    'truck_nopol'
                    ));
                }
                $truck->setLimit(Config::$application['display_limit']);
                if(!in_array(session::get('user_group'),Config::$application['super_group'])){
                    $_GET['koperasi'] = session::get('user_koperasi');
                    $truck->setCondition('truck_koperasi = \''.session::get('user_koperasi').'\' AND truck_status != \'d\'');
                    $data['koperasi'] = $koperasi->setCondition('koperasi_id = \''.session::get('user_koperasi').'\'');
                }else{
                    $_GET['koperasi'] = !isset($_GET['koperasi']) ? '' : $_GET['koperasi'];
                    $addSQL = '';
                    if(!empty($_GET['koperasi'])){
                        $addSQL = ' AND truck_koperasi = \''.$_GET['koperasi'].'\'';
                    }
                    $truck->setCondition('truck_status != \'d\''.$addSQL);
                    $data['koperasi'] = $koperasi->setCondition('koperasi_status = \'y\'')->setLimit(0)->setOrderBy('koperasi_nama')->read();
                }

                $data['truck'] = $truck->read();
                View::display('truck/truck.setting',$data);

                break;
        }
    }
}
