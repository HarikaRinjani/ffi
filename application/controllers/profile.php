<?php
/**
 * Created by PhpStorm.
 * User: PrastowoaGung
 * Date: 6/8/14
 * Time: 9:46 PM
 */
class Profile_Controller extends Controller{
    public $version = 1.2;
    public $publisher = 'Transformatika';
    public $description = 'Change your personal data and application color scheme';
    public $dependencies = array(
        array(
            'name'=>'dashboard',
            'version'=>1
        ),
        array(
            'name'=>'bookmark',
            'version'=>1
        ),
        array(
            'name'=>'user',
            'version'=>1
        )
    );
    public $buildConfiguration = array(
        'views'=>array(
            'profile.index',
            'profile.bookmark',
            'profile.edit'
        ),
        'table'=>'user',
        'models'=>'user'
    );
    function __construct(){
        parent::__construct();
        security::checkSession();
        session::set('navbar','dashboard');
    }
    function index(){
        session::set('title','My Profile');
        $dt = load::model('user');
        $dt->setId(session::get('user_id'));
        $user = $dt->read();
        $data['user'] = $user['records'];
        View::display('profile/profile.index',$data);
    }
    function edit(){
        session::set('title','Edit Profile');
        $dt = load::model('user');
        $dt->setId(session::get('user_id'));
        $user = $dt->read();
        $data['user'] = $user['records'];
        View::display('profile/profile.edit',$data);
    }
    function bookmark(){
        session::set('title','My Bookmarks');
        $dt = load::model('bookmark');
        if(isset($_GET['page'])){
            $dt->setPage($_GET['page']);
        }
        if(isset($_GET['keyword'])){
            $dt->setKeyword($_GET['keyword']);
            $dt->setSearchIn(array(
                'bookmark_title',
                'bookmark_url'
            ));
        }
        $dt->setCondition('bookmark_user = \''.session::get('user_id').'\' AND bookmark_status = \'y\'');
        $dt->setLimit(Config::$application['display_limit']);
        $data['bookmark'] = $dt->read();
        View::display('profile/profile.bookmark',$data);
    }
    function save(){
        load::model('fn');
        $dt = load::model('user');
        if(isset($_POST['user_password']) && !empty($_POST['user_password'])){
            //$_POST['salt'] = fn::generate_id(true);
            $_POST['user_password'] = security::encrypt($_POST['user_password']);
        }else{
            unset($_POST['user_password']);
        }
        if(isset($_FILES['user_avatar']['name']) && !empty($_FILES['user_avatar']['name'])){
            $uniqid = fn::generate_id();
            move_uploaded_file($_FILES['user_avatar']['tmp_name'],Config::$application['file_path'].DS.'images'.DS.'avatar'.DS.$uniqid.$_FILES['user_avatar']['name']);
            $_POST['user_avatar'] = 'images/avatar/'.$uniqid.$_FILES['user_avatar']['name'];
        }else{
            unset($_POST['user_avatar']);
        }
        $dt->setId(session::get('user_id'));
        $dt->setDataArray($_POST);
        if($dt->update()){
            session::set('errorMsg',_('Data has been successfully saved'));
            if(isset($_POST['user_avatar'])){
                session::set('user_avatar',Config::$application['file_url'].rawurlencode($_POST['user_avatar']));
            }
            session::set('user_fullname',$_POST['user_fullname']);
            session::set('user_email',$_POST['user_email']);
            session::set('user_theme',$_POST['user_theme']);
        }else{
            session::set('errorMsg',_('Failed while saving data into database'));
        }
        header('location:'.BASE_URL.'index.php?/profile/edit');
        exit();
    }
}