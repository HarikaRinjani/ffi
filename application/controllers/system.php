<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 12/15/2014
 * Time: 2:09 PM
 */
class System_Controller extends Controller{
    public $buildConfiguration = array(
        'views'=>array(
            'system.index',
            'system.package',
            'system.usertype',
            'system.security',
            'system.consolelog',
            'system.parameter',
            'system.menu'
        ),
        'config'=>array('usergroup','system')
    );
    public $version = 2.9;
    public $description = "v2.9\n- Fix backup function \n\nv.2.8 (2015-05-07)\n- Fix Error: Cannot Redeclare Package\n\nv2.7 (2015-05-07)\n- Fix install package\n- Add backup procedure before install \n\nv2.6\n- Fix app.js \n\nv2.5\n- Fix Card Management Permission\n\nv2.4\nChange Logs: \n- Fix system repository \n\nv2.3\nChange Logs:\n- Fixed: PHP Fatal error: Cannot redeclare class System_Controller \n\nv2.2\nchange Logs:\n- Add: update function";
    protected $table = '';
    function __construct(){
        parent::__construct();
        security::checkSession();
    }


    function index(){
        session::set('title','System Information');
        View::display('system/system.index');
    }

    function package(){
        session::set('title','Package Manager');
        $package_dir = path('app').DS.'controllers';
        $package = $this->openDir($package_dir);
        $data = array();

        $i=0;
        foreach($package as $v){
            if($v != 'login'){
                require_once $package_dir.DS.$v.'.php';
                $controller = ucfirst($v) . '_Controller';
                $loadcontroller = new ReflectionClass($controller);
                $name = $loadcontroller->newInstanceWithoutConstructor();
                
                $data[$i]['package'] = $v;
                $data[$i]['version'] = $name->version;
                $data[$i]['publisher'] = $name->publisher;
                $data[$i]['description'] = fn::markdown($name->description);
                $data[$i]['dependencies'] = $name->dependencies;
                $data[$i]['type'] = 'package';
                $i++;
            }
        }
        $datas['package'] = $data;
        View::display('system/system.package',$datas);
    }

    function jsondata($model,$id){
        $dt = load::model($model);
        $dt->setId($id);
        $record = $dt->read();
        echo json_encode($record['records']);
    }

    function installprocess(){
        if(isset($_FILES['package']['name']) && !empty($_FILES['package']['name'])){
            $package_file = str_replace('.zip','',$_FILES['package']['name']);
            $split_package = explode('-',$package_file);
            if(isset($split_package[1])){
                $versionnya = (float) $split_package[1];
                $package = $split_package[0];
            }else{
                $package = $package_file;
            }
            $file_zip = Config::$application['file_path'].DS.'tmp'.DS.$_FILES['package']['name'];
            move_uploaded_file($_FILES['package']['tmp_name'],$file_zip);
            $zip = new ZipArchive;
            if (! $zip) {
                echo "<br>Could not make ZipArchive object.";
                exit;
            }
            if($zip->open("$file_zip") != "true") {
                echo "<br>"._("Failed installing package");
                exit();
            }

            // BACKUP DULU BUAT JAGA JAGA
            //@$this->backup($package,false);

            $zip->extractTo(path('base'));
            $zip->close();
            @unlink($file_zip);
            if($package == 'system'){
                $module = $this;
            }else{
                include path('app').DS.'controllers'.DS.$package.'.php';
                $controller = ucfirst($package).'_Controller';
                $loadcontroller = new ReflectionClass($controller);
                $module = $loadcontroller->newInstanceWithoutConstructor();
            }
            
            $configuration = $module->buildConfiguration;
            //print_r($configuration);exit();
            $table_conf_dir = path('app').DS.'config'.DS.'conf.d'.DS.'table';
            if(isset($configuration['table'])){
                load::library('db');
                if(is_array($configuration['table'])){
                    foreach($configuration['table'] as $tbl){
                        if(file_exists($table_conf_dir.DS.'table.'.$tbl.'.php')){
                            $_TABLE = array();
                            require_once $table_conf_dir.DS.'table.'.$tbl.'.php';
                            //print_r($_TABLE);exit();
                            foreach($_TABLE as $k=>$v){
                                $cekSQL = "SELECT relname FROM pg_class WHERE relname = '".$k."'";
                                $queryCek = db::query($cekSQL);
                                //var_dump($queryCek);exit();
                                if(db::numRows($queryCek) > 0){
                                    $fields = db::getFields($k);
                                    $sql = 'ALTER TABLE '.$k.' ';
                                    $primaryKey = '';
                                    $index = 0;
                                    foreach($v as $key=>$val){
                                        if(!in_array($key,$fields)){
                                            $notnull = '';
                                            $default = '';
                                            $length = '';
                                            if(isset($val['primary_key'])){
                                                $primaryKey = ', CONSTRAINT '.$k.'_'.$key.' PRIMARY KEY ('.$key.') ';
                                            }
                                            if(isset($val['length']) && $val['length'] != 0){
                                                $length = '('.$val['length'].')';
                                            }
                                            if(isset($val['not null'])){
                                                $notnull = ' NOT NULL';
                                            }
                                            if(isset($val['default'])){
                                                $default = ' DEFAULT \''.$val['default'].'\'::'.$val['type'];
                                            }
                                            if($index > 0){
                                                $sql .= ',';
                                            }
                                            $sql .= ' ADD COLUMN '.$key.' '.$val['type'].$length.$notnull.$default;
                                            $index++;
                                        }
                                    }
                                    $sql .= $primaryKey;
                                    $sql .= '';
                                    //echo $sql;exit();
                                    if($index > 0){
                                        db::query($sql);
                                    }
                                }else{
                                    $sql = 'CREATE TABLE '.$k.' (';
                                    $primaryKey = '';
                                    $index = 0;
                                    foreach($v as $key=>$val){
                                        $notnull = '';
                                        $default = '';
                                        $length = '';
                                        if(isset($val['primary_key'])){
                                            $primaryKey = ', CONSTRAINT '.$k.'_'.$key.' PRIMARY KEY ('.$key.') ';
                                        }
                                        if(isset($val['length']) && $val['length'] != 0){
                                            $length = '('.$val['length'].')';
                                        }
                                        if(isset($val['not null'])){
                                            $notnull = ' NOT NULL';
                                        }
                                        if(isset($val['default'])){
                                            $default = ' DEFAULT \''.$val['default'].'\'::'.$val['type'];
                                        }
                                        if($index > 0){
                                            $sql .= ',';
                                        }
                                        $sql .= ' '.$key.' '.$val['type'].$length.$notnull.$default;
                                        $index++;
                                    }
                                    $sql .= $primaryKey;
                                    $sql .= ')';
                                    //echo $sql;exit();
                                    if($index > 0){
                                        db::query($sql);
                                    }
                                }
                            }

                        }else{
                            die($table_conf_dir.DS.'table.'.$tbl.'.php');
                        }
                    }
                }else{
                    if(file_exists($table_conf_dir.DS.'table.'.$configuration['table'].'.php')){
                        $_TABLE = array();
                        require_once $table_conf_dir.DS.'table.'.$configuration['table'].'.php';

                        foreach($_TABLE as $k=>$v){
                            $cekSQL = "SELECT relname FROM pg_class WHERE relname = '".$k."'";
                            $queryCek = db::query($cekSQL);
                            if(db::numRows($queryCek) > 0){
                                $fields = db::getFields($k);
                                $sql = 'ALTER TABLE '.$k.' ';
                                $primaryKey = '';
                                $index = 0;
                                foreach($v as $key=>$val){
                                    if(!in_array($key,$fields)){
                                        $notnull = '';
                                        $default = '';
                                        $length = '';
                                        if(isset($val['primary_key'])){
                                            $primaryKey = ', CONSTRAINT '.$k.'_'.$key.' PRIMARY KEY ('.$key.') ';
                                        }
                                        if(isset($val['length']) && $val['length'] != 0){
                                            $length = '('.$val['length'].')';
                                        }
                                        if(isset($val['not null'])){
                                            $notnull = ' NOT NULL';
                                        }
                                        if(isset($val['default'])){
                                            $default = ' DEFAULT \''.$val['default'].'\'::'.$val['type'];
                                        }
                                        if($index > 0){
                                            $sql .= ',';
                                        }
                                        $sql .= ' ADD COLUMN '.$key.' '.$val['type'].$length.$notnull.$default;
                                        $index++;
                                    }
                                }
                                $sql .= $primaryKey;
                                $sql .= '';
                                //echo $sql;exit();
                                if($index > 0){
                                    db::query($sql);
                                }
                            }else{
                                $sql = 'CREATE TABLE '.$k.' (';
                                $primaryKey = '';
                                $index = 0;
                                foreach($v as $key=>$val){
                                    $notnull = '';
                                    $default = '';
                                    $length = '';
                                    if(isset($val['primary_key'])){
                                        $primaryKey = ', CONSTRAINT '.$k.'_'.$key.' PRIMARY KEY ('.$key.') ';
                                    }
                                    if(isset($val['length']) && $val['length'] != 0){
                                        $length = '('.$val['length'].')';
                                    }
                                    if(isset($val['not null'])){
                                        $notnull = ' NOT NULL';
                                    }
                                    if(isset($val['default'])){
                                        $default = ' DEFAULT \''.$val['default'].'\'::'.$val['type'];
                                    }
                                    if($index > 0){
                                        $sql .= ',';
                                    }
                                    $sql .= ' '.$key.' '.$val['type'].$length.$notnull.$default;
                                    $index++;
                                }
                                $sql .= $primaryKey;
                                $sql .= ')';
                                if($index > 0){
                                    db::query($sql);
                                }
                            }
                        }

                    }
                }
            }

            session::set('errorMsg','Package '.$package.' '._('Successfully installed'));
            header('location:'.BASE_URL.'index.php?/system/package');
            exit();
        }else{
            session::set('errorMsg',_('Please upload a package'));
            header('location:'.BASE_URL.'index.php?/system/package');
            exit();
        }
    }

    function backup($__package,$download='yes'){
        load::library('fn');
        if($__package == 'system'){
            $__configuration = $this->buildConfiguration;
            $__version = $this->version;
        }else{
            include_once path('app').DS.'controllers'.DS.$__package.'.php';
            $__controller = ucfirst($__package) . '_Controller';
            $loadcontroller = new ReflectionClass($__controller);
            $__module = $loadcontroller->newInstanceWithoutConstructor();
            $__configuration = $__module->buildConfiguration;
            $__version = $__module->version;
            $__module = null;
            unset($__module);
        }
        
       
        fn::createDirectory('/backup/'.$__package);
        $temp_dir = Config::$application['file_path'].'backup'.DS.$__package;
        //$this->createDir($temp_dir);

        $zip = new ZipArchive();
        $zip->open( $temp_dir.DS.$__package.'-'.$__version.'.zip', ZipArchive::CREATE );

        $zip->addEmptyDir('application');
        $zip->addEmptyDir('application'.DS.'controllers');
        $zip->addEmptyDir('application'.DS.'config');
        $zip->addEmptyDir('application'.DS.'config'.DS.'conf.d');
        $zip->addEmptyDir('application'.DS.'models');
        $zip->addEmptyDir('application'.DS.'views');
        $zip->addFile(path('app').DS.'controllers'.DS.$__package.'.php','application'.DS.'controllers'.DS.$__package.'.php');
        if(array_key_exists('config', $__configuration)){
            if(is_array($__configuration['config'])){
                foreach($__configuration['config'] as $v){
                    $zip->addFile(path('app').DS.'config'.DS.'conf.d'.DS.$v.'.php','application'.DS.'config'.DS.'conf.d'.DS.$v.'.php');
                }

            }else{
                $zip->addFile(path('app').DS.'config'.DS.'conf.d'.DS.$__configuration['config'].'.php','application'.DS.'config'.DS.'conf.d'.DS.$__configuration['config'].'.php');
            }
        }
        if(array_key_exists('table',$__configuration)){
            $zip->addEmptyDir('application'.DS.'config'.DS.'conf.d'.DS.'table');
            if(is_array($__configuration['table'])){
                foreach($__configuration['table'] as $v){
                    $zip->addFile(path('app').DS.'config'.DS.'conf.d'.DS.'table'.DS.'table.'.$v.'.php','application'.DS.'config'.DS.'conf.d'.DS.'table'.DS.'table.'.$v.'.php');
                }

            }else{
                $zip->addFile(path('app').DS.'config'.DS.'conf.d'.DS.'table'.DS.'table.'.$__configuration['table'].'.php','application'.DS.'config'.DS.'conf.d'.DS.'table'.DS.'table.'.$__configuration['table'].'.php');
            }

        }
        if(array_key_exists('models',$__configuration)){
            if(is_array($__configuration['models'])){
                foreach($__configuration['models'] as $v){
                    $zip->addFile(path('app').DS.'models'.DS.$v.'.php','application'.DS.'models'.DS.$v.'.php');
                }
            }else{
                $zip->addFile(path('app').DS.'models'.DS.$__package.'.php','application'.DS.'models'.DS.$__configuration['models'].'.php');
            }
        }
        if(array_key_exists('views',$__configuration)){
            $zip->addEmptyDir('application'.DS.'views'.DS.'default');
            $zip->addEmptyDir('application'.DS.'views'.DS.'default'.DS.$__package);
            if(is_array($__configuration['views'])){
                foreach($__configuration['views'] as $v){
                    $zip->addFile(path('app').DS.'views'.DS.'default'.DS.$__package.DS.$v.'.php','application'.DS.'views'.DS.'default'.DS.$__package.DS.$v.'.php');
                }
            }else{
                $zip->addFile(path('app').DS.'views'.DS.'default'.DS.$__package.DS.$__configuration['views'].'.php','application'.DS.'views'.DS.'default'.DS.$__package.DS.$__configuration['views'].'.php');
            }
        }

        $zip->close();
        if($download === 'yes'){
            header( "Pragma: public" );
            header( "Expires: 0" );
            header( "Cache-Control: must-revalidate, post-check=0, pre-check=0" );
            header( "Cache-Control: public" );
            header( "Content-Description: File Transfer" );
            header( "Content-type: application/zip" );
            header( "Content-Disposition: attachment; filename=\"" . $__package . "-".$__version.".zip\"" );
            header( "Content-Transfer-Encoding: binary" );
            header( "Content-Length: " . filesize( $temp_dir.DS.$__package.'-'.$__version.'.zip' ) );

            readfile( $temp_dir.DS.$__package.'-'.$__version.'.zip' );
            fn::removeDirectory($temp_dir);
            exit();
        }elseif($download === 'json'){
            echo json_encode(array('status'=>'success'));
        }else{
            return true;
        }
    }

    function repair($package){
        include path('app').DS.'controllers'.DS.$package.'.php';
        $controller = ucfirst($package).'_Controller';
        $module = new $controller;
        $configuration = $module->buildConfiguration;

        $table_conf_dir = path('app').DS.'config'.DS.'conf.d'.DS.'table';
        if(isset($configuration['table'])){
            load::library('db');
            if(is_array($configuration['table'])){
                foreach($configuration['table'] as $tbl){
                    if(file_exists($table_conf_dir.DS.'table.'.$tbl.'.php')){
                        $_TABLE = array();
                        require_once $table_conf_dir.DS.'table.'.$tbl.'.php';
                        //print_r($_TABLE);exit();
                        foreach($_TABLE as $k=>$v){
                            $cekSQL = "SELECT relname FROM pg_class WHERE relname = '".$k."'";
                            $queryCek = db::query($cekSQL);
                            //var_dump($queryCek);exit();
                            if(db::numRows($queryCek) > 0){
                                $fields = db::getFields($k);
                                $sql = 'ALTER TABLE '.$k.' ';

                                $primaryKey = '';
                                $index = 0;
                                foreach($v as $key=>$val){
                                    if(!in_array($key,$fields)){
                                        $notnull = '';
                                        $default = '';
                                        $length = '';
                                        if(isset($val['primary_key'])){
                                            $primaryKey = ', CONSTRAINT '.$k.'_'.$key.' PRIMARY KEY ('.$key.') ';
                                        }
                                        if(isset($val['length']) && $val['length'] != 0){
                                            $length = '('.$val['length'].')';
                                        }
                                        if(isset($val['not null'])){
                                            $notnull = ' NOT NULL';
                                        }
                                        if(isset($val['default'])){
                                            $default = ' DEFAULT \''.$val['default'].'\'::'.$val['type'];
                                        }
                                        if($index > 0){
                                            $sql .= ',';
                                        }
                                        $sql .= ' ADD COLUMN '.$key.' '.$val['type'].$length.$notnull.$default;
                                        $index++;
                                    }
                                }
                                $sql .= $primaryKey;
                                $sql .= '';

                                if($index > 0){
                                    db::query($sql);
                                }
                            }else{
                                $sql = 'CREATE TABLE '.$k.' (';
                                $primaryKey = '';
                                $index = 0;
                                foreach($v as $key=>$val){
                                    $notnull = '';
                                    $default = '';
                                    $length = '';
                                    if(isset($val['primary_key'])){
                                        $primaryKey = ', CONSTRAINT '.$k.'_'.$key.' PRIMARY KEY ('.$key.') ';
                                    }
                                    if(isset($val['length']) && $val['length'] != 0){
                                        $length = '('.$val['length'].')';
                                    }
                                    if(isset($val['not null'])){
                                        $notnull = ' NOT NULL';
                                    }
                                    if(isset($val['default'])){
                                        $default = ' DEFAULT \''.$val['default'].'\'::'.$val['type'];
                                    }
                                    if($index > 0){
                                        $sql .= ',';
                                    }
                                    $sql .= ' '.$key.' '.$val['type'].$length.$notnull.$default;
                                    $index++;
                                }
                                $sql .= $primaryKey;
                                $sql .= ')';
                                //echo $sql;exit();
                                if($index > 0){
                                    db::query($sql);
                                }
                            }
                        }

                    }else{
                        die($table_conf_dir.DS.'table.'.$tbl.'.php');
                    }
                }
            }else{
                if(file_exists($table_conf_dir.DS.'table.'.$configuration['table'].'.php')){
                    $_TABLE = array();
                    require_once $table_conf_dir.DS.'table.'.$configuration['table'].'.php';
                    //print_r($_TABLE);exit();
                    foreach($_TABLE as $k=>$v){
                        $cekSQL = "SELECT relname FROM pg_class WHERE relname = '".$k."'";
                        $queryCek = db::query($cekSQL);
                        if(db::numRows($queryCek) > 0){
                            $fields = db::getFields($k);
                            $sql = 'ALTER TABLE '.$k.' ';
                            $primaryKey = '';
                            $index = 0;
                            foreach($v as $key=>$val){
                                if(!in_array($key,$fields)){
                                    $notnull = '';
                                    $default = '';
                                    $length = '';
                                    if(isset($val['primary_key'])){
                                        $primaryKey = ', CONSTRAINT '.$k.'_'.$key.' PRIMARY KEY ('.$key.') ';
                                    }
                                    if(isset($val['length']) && $val['length'] != 0){
                                        $length = '('.$val['length'].')';
                                    }
                                    if(isset($val['not null'])){
                                        $notnull = ' NOT NULL';
                                    }
                                    if(isset($val['default'])){
                                        $default = ' DEFAULT \''.$val['default'].'\'::'.$val['type'];
                                    }
                                    if($index > 0){
                                        $sql .= ',';
                                    }
                                    $sql .= ' ADD COLUMN '.$key.' '.$val['type'].$length.$notnull.$default;
                                    $index++;
                                }
                            }
                            $sql .= $primaryKey;
                            $sql .= '';
                            //echo $sql;exit();
                            if($index > 0){
                                db::query($sql);
                            }
                        }else{
                            $sql = 'CREATE TABLE '.$k.' (';
                            $primaryKey = '';
                            $index = 0;
                            foreach($v as $key=>$val){
                                $notnull = '';
                                $default = '';
                                $length = '';
                                if(isset($val['primary_key'])){
                                    $primaryKey = ', CONSTRAINT '.$k.'_'.$key.' PRIMARY KEY ('.$key.') ';
                                }
                                if(isset($val['length']) && $val['length'] != 0){
                                    $length = '('.$val['length'].')';
                                }
                                if(isset($val['not null'])){
                                    $notnull = ' NOT NULL';
                                }
                                if(isset($val['default'])){
                                    $default = ' DEFAULT \''.$val['default'].'\'::'.$val['type'];
                                }
                                if($index > 0){
                                    $sql .= ',';
                                }
                                $sql .= ' '.$key.' '.$val['type'].$length.$notnull.$default;
                                $index++;
                            }
                            $sql .= $primaryKey;
                            $sql .= ')';
                            //echo $sql;exit();
                            if($index > 0){
                                db::query($sql);
                            }
                        }
                    }

                }
            }
        }
        session::set('errorMsg','Package '.$package.' '._('Successfully repaired'));
        if(isset($_GET['ref'])){
            header('location:'.BASE_URL.'index.php?/system/updatemanager');
        }else{
            header('location:'.BASE_URL.'index.php?/system/package');
        }
        exit();

    }
    protected function openDir($dir){
        $files = scandir($dir);
        $package = array();
        foreach($files as $k=>$v){
            if (preg_match('/^[^_].*\.(php)$/i', $v)){
                $package[] = str_replace('.php','',$v);
            }
        }
        return $package;
    }
    function parameter(){
        session::set('title','System Parameter');
        global $___language;
        $data['locale'] = $___language;
        View::display('system/system.parameter',$data);
    }
    function parametersave(){
        $temp_file = Config::$application['login_logo'];

        if(isset($_FILES['login_logo']['name']) && !empty($_FILES['login_logo']['name'])){
            $filenamenya = 'themes'.DS.'default'.DS.'images'.DS.uniqid().$_FILES['login_logo']['name'];
            $upload = move_uploaded_file($_FILES['login_logo']['tmp_name'],path('base').DS.$filenamenya);
            if($upload){
                $_POST['login_logo'] = $filenamenya;
            }else{
                $_POST['login_logo'] = $temp_file;
            }
        }else{
            $_POST['login_logo'] = $temp_file;
        }

        $language = $_POST['locale'];
        $language_file = '<?php ';
        $language_file .= '$locale="'.$language.'";';
        file_put_contents(path('base').DS.'locale.php', $language_file);
        unset($_POST['locale']);

        $str = '<?php ';
        $str .= "\n";
        foreach($_POST as $k=>$v){
            if($k == 'csv_path'){
                $v = preg_replace('/\\\\/','\\\\\\\\',$v);
            }
            if($k == 'start_year' || $k == 'batas_pagi' || $k == 'maximal_printer_karakter' || $k == 'display_limit' || $k== 'cooperativelogo_size'){
                $str .= '$_CONFIG["'.$k.'"] = '.$v.';';
            }elseif($k == 'log_rotate' || $k == 'log_access' || $k == 'log_error' || $k== 'is_client' || $k == 'display_cooperativelogo' || $k== 'enable_onscreenkeyboard'){
                if($v == 1){
                    $str .= '$_CONFIG["'.$k.'"] = true;';
                }else{
                    $str .= '$_CONFIG["'.$k.'"] = false;';
                }
            }else{
                $str .= '$_CONFIG["'.$k.'"] = "'.$v.'";';
            }

            $str .= "\n";
        }

        file_put_contents(path('app').DS.'config'.DS.'conf.d'.DS.'variable.php', $str);
        session::set('errorMsg',_('Configuration saved'));
        header('location:'.BASE_URL.'index.php?/system/parameter');
    }

    function usertype(){
        session::set('title','User Type Manager');
        $_GET['user_group'] = !isset($_GET['user_group']) ? 'administrator': $_GET['user_group'];
        $dt = load::model('usertype');
        $dt->setId($_GET['user_group']);
        $records = $dt->read();
        $data['role'] = $records['records']['usertype_role'];
        View::display('system/system.usertype',$data);
    }
    function usertypesave(){
        $dt = load::model('usertype');
        $data['usertype_id'] = $_POST['usertype_id'];
        $roles = array();
        foreach($_POST['roles'] as $k=>$v){
            $roles[] = $v;
        }
        $data['usertype_role'] = implode('|',$roles);
        $dt->setId($_POST['usertype_id']);
        $dt->setDataArray($data);
        if($dt->update()){
            session::set('errorMsg',_('Data has been successfully saved'));
        }else{
            session::set('errorMsg',_('Failed while saving into database'));
        }
        header('location:'.BASE_URL.'index.php?/system/usertype/&user_group='.$data['usertype_id']);
    }
    function security(){
        session::set('title','Blocked IP Address');
        View::display('system/system.security');
    }
    function securitysave(){
        if(isset($_POST['denied_host'])){
            if(empty($_POST['denied_host'])){
                $str = '<?php ';
                $str .= "\n";
                $str .= '$_CONFIG["denied_host"] = array();';
            }else{
                $deniedhost = explode(',',$_POST['denied_host']);
                $str = '<?php ';
                $str .= "\n";
                $str .= '$_CONFIG["denied_host"] = array(';
                $i = 1;
                $totalhost = count($deniedhost);
                foreach($deniedhost as $host){
                    if($i < $totalhost && $totalhost > 1){
                        $str .= '"'.$host.'",';
                    }else{
                        $str .= '"'.$host.'"';
                    }
                    $i++;
                }
                $str .= ');';
            }


            file_put_contents(path('app').DS.'config'.DS.'conf.d'.DS.'host.deny.php', $str);
            session::set('errorMsg',_('Configuration saved'));
            header('location:'.BASE_URL.'index.php?/system/security');
        }
    }
    function __consoleLog($type = 'access', $date = ''){


        $data['log'] = file_get_contents(path('app') . DS . 'views' . DS . Config::$application['theme'] . DS . 'admin' . DS . 'motd.txt');
        View::display('system/system.consoleLog', $data);
    }

    function consolelog(){
        session::set('title','Console');
        $command = !isset($_POST['command']) ? null : $_POST['command'];
        if (!empty($command)) {

            switch ($command) {
                case 'reload':
                    echo fn::openLog(session::get('consoleLog_type'), session::get('consoleLog_date'));
                    break;
                case 'motd':
                    $logs = file_get_contents(path('app') . DS . 'views' . DS . Config::$application['theme'] . DS . 'system' . DS . 'motd.txt');
                    echo '<pre style="overflow:hidden;">';
                    echo $logs;
                    echo '</pre>';
                    break;
                case 'help':
                    $logs = file_get_contents(path('app') . DS . 'views' . DS . Config::$application['theme'] . DS . 'system' . DS . 'help.txt');
                    echo nl2br($logs);
                    break;

                case 'version':
                    echo '<p class="fg-green"><strong>Software Version: ' . Config::$application['app_version'] . '</strong></p>';
                    echo '<p class="fg-green"><strong>Console Version: ' . $this->version . '</strong></p>';
                    echo '<div class="padding-top-5 margin-bottom-5" style="border-bottom:1px dotted #FFF;"></div>';
                    echo 'SERVER IP ADDRESS: '.$_SERVER['SERVER_ADDR'].'<br>';
                    echo 'SERVER SOFTWARE: '.$_SERVER['SERVER_SOFTWARE'].' <br>';
                    echo $_SERVER['SERVER_SIGNATURE'];
                    break;

                case 'ver':
                    echo '<p class="fg-green"><strong>Software Version: ' . Config::$application['app_version'] . '</strong></p>';
                    echo '<p class="fg-green"><strong>Console Version: ' . $this->version . '</strong></p>';
                    echo '<div class="padding-top-5 margin-bottom-5" style="border-bottom:1px dotted #FFF;"></div>';
                    echo 'SERVER IP ADDRESS: '.$_SERVER['SERVER_ADDR'].'<br>';
                    echo 'SERVER SOFTWARE: '.$_SERVER['SERVER_SOFTWARE'].' <br>';
                    echo $_SERVER['SERVER_SIGNATURE'];
                    break;

                case 'whoami':
                    echo '<p class="fg-yellow">Username: ' . session::get('user_name') . '</p>';
                    echo '<p class="fg-yellow">Fullname: ' . session::get('user_fullname') . '</p>';
                    echo '<p class="fg-yellow">Email: ' . session::get('user_email') . '</p>';
                    echo '<p class="fg-yellow">IP Address: ' . $_SERVER['REMOTE_ADDR'] . '</p>';
                    echo '<p class="fg-yellow">User Agent: ' . $_SERVER['HTTP_USER_AGENT'] . '</p>';
                    break;

                case 'phpversion':
                    echo phpversion();
                    break;

                case 'pwd':
                    echo Config::$application['log_path'];
                    break;

                case 'uname':
                    echo php_uname();
                    break;

                case 'uname -a':
                    echo php_uname();
                    break;
                /*case 'settings':
                    $dt = load::model('settings');
                    $dt->setId(Config::$application['company_name']);
                    $setting = $dt->read();
                    foreach($setting['records'] as $k=>$v){
                        echo '<p>'.$k.' : '.$v.'</p>';
                    }
                    break;*/
                case 'ifconfig':
                    echo 'IP ADDRESS: '.$_SERVER['SERVER_ADDR'].'<br>';
                    break;
                case 'blacklist':
                    $blacklist = Config::$application['denied_host'];
                    echo 'Blocked IP Address:<br><br>';
                    if(count($blacklist) === 0){
                        echo '<div class="padding-left-10">No blacklisted IP Address</div>';
                    }else{
                        foreach($blacklist as $ip){
                            echo '<div class="padding-left-10">'.$ip.'</div>';
                        }
                    }
                    break;
                default:
                    $cmd = explode(' ', $command);
                    if ($cmd[0] == 'open') {
                        if (!isset($cmd[1])) {
                            echo _('please use "open access" or "open error" command');
                            exit();
                        }
                        if ($cmd[1] == 'access') {
                            if (!isset($cmd[2])) {
                                $cmd[2] = date('Y-m-d');
                            }
                            if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $cmd[2])) {
                                $unix_date = strtotime($cmd[2]);
                                if ($unix_date > time()) {
                                    echo _('Operation failed. invalid date!');
                                } else {
                                    echo fn::openLog('access', $cmd[2]);
                                }
                            } else {
                                echo _('Invalid date format. please use yyyy-mm-dd. ex open access 2013-12-11');
                            }
                        } elseif ($cmd[1] == 'error') {
                            if (!isset($cmd[2])) {
                                $cmd[2] = date('Y-m-d');
                            }
                            if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $cmd[2])) {
                                $unix_date = strtotime($cmd[2]);
                                if ($unix_date > time()) {
                                    echo _('Operation failed. invalid date!');
                                } else {
                                    echo fn::openLog('error', $cmd[2]);
                                }
                            } else {
                                echo _('Invalid date format. please use yyyy-mm-dd. ex open access 2013-12-11');
                            }
                        } else {
                            echo _('please use "open access" or "open error" command');
                        }
                    } elseif($cmd[0] == 'config') {
                        $config = array(
                            'timezone',
                            'start_year',
                            'batas_pagi',
                            'display_limit',
                            'maximal_printer_karakter',
                            'printername',
                            'zebra_printername',
                            'mcp_server_url',
                            'csv_path',
                            'log_error',
                            'log_access',
                            'log_rotate',
                            'login_logo',
                            );
                        if(!isset($cmd[1])){
                            foreach($config as $v){
                                if($v == 'log_error' || $v == 'log_access' || $v == 'log_rotate'){
                                    if(Config::$application[$v] == 1){
                                        echo $v .' : true<br>';
                                    }else{
                                        echo $v .' : false<br>';
                                    }
                                }else{
                                    echo $v .' : '.(string) Config::$application[$v].'<br>';
                                }

                            }
                        }else{
                            if(in_array($cmd[1], $config)){
                                if($cmd[1] == 'log_error' || $cmd[1] == 'log_access' || $cmd[1] == 'log_rotate'){
                                    if(Config::$application[$cmd[1]] == 1){
                                        echo $cmd[1] .' : true<br>';
                                    }else{
                                        echo $cmd[1] .' : false<br>';
                                    }
                                }else{
                                    echo $cmd[1] .' : '.(string) Config::$application[$cmd[1]].'<br>';
                                }
                            }else{
                                echo _('Command not found. Type help to view help command');
                            }
                        }

                    }else{
                        echo _('Command not found. Type help to view help command');
                    }
                    break;
            }
        }else{
            $logs = file_get_contents(path('app') . DS . 'views' . DS . Config::$application['theme'] . DS . 'system' . DS . 'motd.txt');
            $data['log'] = '<pre style="overflow:hidden;">'.$logs.'</pre>';
            View::display('system/system.consolelog', $data);
        }
    }

    function checkupdate(){
        load::library('tcurl');
        $postdataDetail['user_id']      = security::encrypt(session::get('user_id'));
        $postdataDetail['mcp_key']      = security::encrypt(Config ::$application['mcp_key']);
        $postdataDetail['mcp_tpk']      = security::encrypt(Config ::$application['mcp_tpk']);
        $postdataDetail['mcp_koperasi'] = security::encrypt(Config ::$application['mcp_koperasi']);
        $package_dir = path('app').DS.'controllers';
        $package = $this->openDir($package_dir);
        $data = array();

        $i=0;
        $tes = array();
        foreach($package as $v){
            if($v != 'login'){
                if($v == 'system'){
                    $name = $this;
                }else{
                    require_once $package_dir.DS.$v.'.php';
                    $controller = ucfirst($v) . '_Controller';
                    $name = new $controller;
                }
                $cur_version = $name->version;
                $postdataDetail['package'] = $v;
                $postdataDetail['version'] = $cur_version;

                $requestDetail = tcurl::request(Config::$application['system_repo'].'index.php?/synchandler/checkpackage',$postdataDetail);
                if($requestDetail['version'] > $cur_version){
                    $data[$i] = $requestDetail;
                    $i++;
                }
            }
        }

        echo json_encode($data);
    }
    function installupdate($package){
       $target_url = Config::$application['system_repo'].'index.php?/synchandler/getpackage/'.$package;
       $postdataDetail['user_id']      = security::encrypt(session::get('user_id'));
       $postdataDetail['mcp_key']      = security::encrypt(Config ::$application['mcp_key']);
       $postdataDetail['mcp_tpk']      = security::encrypt(Config ::$application['mcp_tpk']);
       $postdataDetail['mcp_koperasi'] = security::encrypt(Config ::$application['mcp_koperasi']);
       $datanya = array();
       foreach($postdataDetail as $k=>$v){
           $datanya[] = $k.'='.urlencode($v);
       }
       $post_data = implode('&',$datanya);

       $userAgent = 'Googlebot/2.1 (http://www.googlebot.com/bot.html)';
       $file_zip = Config::$application['file_path'].'tmp'.DS.$package.'.zip';
       $ch = curl_init();
       $fp = fopen("$file_zip", "w");
       curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
       curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);
       curl_setopt($ch, CURLOPT_URL,$target_url);
       curl_setopt($ch, CURLOPT_FAILONERROR, true);
       curl_setopt($ch, CURLOPT_HEADER,0);
       curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
       curl_setopt($ch, CURLOPT_AUTOREFERER, true);
       curl_setopt($ch, CURLOPT_BINARYTRANSFER,true);
       curl_setopt($ch, CURLOPT_TIMEOUT, 3600);
       curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
       curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
       curl_setopt($ch, CURLOPT_FILE, $fp);
       curl_setopt($ch, CURLOPT_POST, 1);
       curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
       $page = curl_exec($ch);
       
       if (!$page) {
           echo "<br />cURL error number:" .curl_errno($ch);
           echo "<br />cURL error:" . curl_error($ch);
           exit;
       }
       curl_close($ch);

       $zip = new ZipArchive;
       if (! $zip) {
           echo "<br>Could not make ZipArchive object.";
           exit;
       }
       @$this->backup($package,false);
       //var_dump($zip);exit();
       if($zip->open("$file_zip") != "true") {
           echo "<br>Could not open $file_zip";
       }
       $zip->extractTo(path('base'));
       $zip->close();
       unlink($file_zip);
       echo 'success';
   }
}
