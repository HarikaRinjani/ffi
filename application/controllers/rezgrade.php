<?php
/**
 * Resazurine Grade Settings
 * 
 */
class Rezgrade_Controller extends Controller{
	public $buildConfiguration = array(
		'table'=>array('rezgrade','rezgradedetail'),
		'views'=>array(
			'rezgrade.index'
			),
		'models'=>array(
			'rezgrade',
			'rezgradedetail'
			)
		);
	public $version = 1.4;
	public $description = "v1.4\n - Add filter\n\nv1.3\n- Fix minor bugs \n\nv1.2 (2015-05-06)\n- Fix minor bugs (zero value fix)";

	function __construct(){
		security::checkSession();
	}


	function index(){
		$dt = load::model('rezgrade');
		session::set('title','Resazurine Grade');
		$koperasi = load::model('koperasi');

		if(!in_array(session::get('user_group'),Config::$application['super_group'])){
			$_GET['koperasi'] = session::get('user_koperasi');
			$koperasi->setCondition('koperasi_id = \''.session::get('user_koeprasi').'\'');
			$data['koperasi'] = $koperasi->read();
		}else{
			$_GET['koperasi'] = !isset($_GET['koperasi']) ? '' : $_GET['koperasi'];
			$koperasi->setCondition('koperasi_status = \'y\'')->setOrderBy('koperasi_nama')->setLimit(0);
			$data['koperasi'] = $koperasi->read();
		}

		$addSQL = '';
		if(!empty($_GET['koperasi'])){
			$addSQL = ' AND rezgrade_koperasi = \''.$_GET['koperasi'].'\'';
		}
		if(in_array(session::get('user_group'),Config::$application['super_group'])){
			$dt->setCondition('rezgrade_status != \'d\''.$addSQL);
		}else{
			$dt->setCondition('rezgrade_status != \'d\' AND rezgrade_koperasi = \''.session::get('user_koperasi').'\'');
		}
		$dt->setOrderBy('rezgrade_grade');
		$data['grade'] = $dt->read();
		View::display('rezgrade/rezgrade.index',$data);
	}
	function edit($id=''){
		if(!empty($id)){
			$dt = load::model('rezgrade');
			$dt->setId($id);
			$data = $dt->read();
			echo json_encode($data['records']);
		}
	}
	function delete(){
		if(isset($_POST['rezgrade_id'])){
			$dt = load::model('rezgrade');
			$dt->setId($_POST['rezgrade_id']);
			if($dt->delete()){
				session::set('errorMsg',_('Data successfully deleted'));
			}else{
				session::set('errorMsg',_('Failed while deleting data'));
			}
		}
	}

	/**
	 * Save Resazurine Grade Settings
	 * @return [type] [description]
	 */
	function save(){
		$dt = load::model('rezgrade');
		$rezgradedetail = load::model('rezgradedetail');
		$_POST['rezgrade_effectivedate'] = date('Y-m-d',fn::dateToUnix($_POST['rezgrade_effectivedate']));
		
		if(!isset($_POST['rezgrade_id'])){
			$_POST['rezgrade_id'] = fn::generate_id();
			$dt->setDataArray($_POST);
			$query = $dt->insert();
			if($query){
				foreach($_POST['rezgradedetail_check'] as $k=>$v){
					if(!empty($_POST['rezgradedetail_grade'][$k])){
						$detail['rezgradedetail_id']       = fn::generate_id(true);
						$detail['rezgradedetail_price']    = (float) $_POST['rezgradedetail_price'][$k];
						$detail['rezgradedetail_tpcstart'] = (float) $_POST['rezgradedetail_tpcstart'][$k];
						$detail['rezgradedetail_tpcend']   = (float) $_POST['rezgradedetail_tpcend'][$k];
						$detail['rezgradedetail_rezgrade'] = $_POST['rezgrade_id'];
						$detail['rezgradedetail_grade']    = (int)$_POST['rezgradedetail_grade'][$k];
						$rezgradedetail->setDataArray($detail);
						$rezgradedetail->insert();
					}
				}
			}
		}else{
			$dt->setId($_POST['rezgrade_id']);
			$dt->setDataArray($_POST);
			$query = $dt->update();
			if($query){
				$rezgradedetail->setCondition('rezgradedetail_rezgrade = \''.$_POST['rezgrade_id'].'\'');
				$rezgradedetail->delete();
				foreach($_POST['rezgradedetail_check'] as $k=>$v){
					if(!empty($_POST['rezgradedetail_tpcstart'][$k]) && !empty($_POST['rezgradedetail_tpcend'][$k]) && !empty($_POST['rezgradedetail_price'][$k]) && !empty($_POST['rezgradedetail_grade'][$k])){
						$detail['rezgradedetail_id']       = fn::generate_id(true);
						$detail['rezgradedetail_price']    = $_POST['rezgradedetail_price'][$k];
						$detail['rezgradedetail_tpcstart'] = $_POST['rezgradedetail_tpcstart'][$k];
						$detail['rezgradedetail_tpcend']   = $_POST['rezgradedetail_tpcend'][$k];
						$detail['rezgradedetail_rezgrade'] = $_POST['rezgrade_id'];
						$detail['rezgradedetail_grade']    = $_POST['rezgradedetail_grade'][$k];
						$rezgradedetail->setDataArray($detail);
						$rezgradedetail->insert();
					}
				}
			}
		}
		if($query){
			session::set('errorMsg',_('Data has been successfully saved'));
		}else{
			session::set('errorMsg',_('Failed while saving data into database'));
		}
		header('location:'.BASE_URL.'index.php?/rezgrade/index');
	}
}