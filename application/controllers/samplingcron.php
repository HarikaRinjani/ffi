<?php
class Samplingcron_Controller extends Controller{
	function index(){
        echo 'tes';
        $koperasi = load::model('koperasi');
        $koperasi->setCondition('koperasi_status = \'y\'')->setLimit(0);
        $dtKoperasi = $koperasi->read();
        $schedule = load::model('samplingschedule');
        $date = date('Y-m-d');
        foreach($dtKoperasi['records'] as $key=>$val){
            $dataPeriode = $schedule->getDataPeriode($val['koperasi_id'],date('Y-m-d'));

            $schedule->setCondition('samplingschedule_periode = '.$dataPeriode['current_periode'].' AND samplingschedule_date = \''.$date.'\' AND samplingschedule_koperasi = \''.$val['koperasi_id'].'\'');
            $schedule->setLimit(0);
            $dataSchedule = $schedule->read();
            //print_r($dataSchedule);exit();
            if($dataSchedule['total_data'] === 0){
                $schedule->setCondition('samplingschedule_periode = '.$dataPeriode['current_periode'].' AND date_part(\'month\',  samplingschedule_date) = '.date('n').' AND date_part(\'year\',  samplingschedule_date) = '.date('Y').'  AND samplingschedule_status = \'y\' AND samplingschedule_koperasi = \''.$val['koperasi_id'].'\'');
                $getlastschedule = $schedule->read();
                //print_r($getlastschedule);exit();
                $userlist = array();
                foreach($getlastschedule['records'] as $k=>$v){
                    //$userlist[] = '\''.$v['samplingschedule_anggota'].'\'';
					if(!in_array($v['samplingschedule_anggota'],$userlist)){
						array_push($userlist,$v['samplingschedule_anggota']);
					}
                }
                //print_r($userlist);exit();
                if(count($userlist) > 0){
                    $addSQL = ' AND anggota_id NOT IN (\''.implode('\',\'',$userlist).'\') ';
                }else{
                    $addSQL = '';
                }

                /**
                 * Generate Schedule
                 */
                $anggota = load::model('anggota');
                $anggota->setCondition('anggota_status = \'y\' AND anggota_koperasi = \''.$val['koperasi_id'].'\''.$addSQL);
                //var_dump($anggota);exit();
                //
                $totaldays = cal_days_in_month(CAL_GREGORIAN, date('n'), date('Y'));
                if($dataPeriode['periode_enddate'] >= $totaldays){
                    $dataPeriode['periode_enddate'] = $totaldays;
                }
                $anggota->setLimit(0);
                $dataAnggota = $anggota->read();
                $arrayAnggota = $dataAnggota['records'];
                $keyAnggota = array_keys($arrayAnggota);
                $sisaHariPeriode = ((int) $dataPeriode['periode_enddate'] + 1) - (int) date('d',strtotime($date)); // (tanggal terakhir + 1) - tanggal hari ini
                $limitArray = ceil($dataAnggota['total_data'] / $sisaHariPeriode);
                shuffle($keyAnggota);

                //print_r($keyAnggota); exit();
                $index = 1;
                $error = 0;

                foreach($keyAnggota as $v){
                    if($index <= $limitArray){
                        $schedule->samplingschedule_id       = fn::generate_id(true);
                        $schedule->samplingschedule_periode  = $dataPeriode['current_periode'];
                        $schedule->samplingschedule_koperasi = $val['koperasi_id'];
                        $schedule->samplingschedule_tpk      = $arrayAnggota[$v]['anggota_tpk'];
                        $schedule->samplingschedule_anggota  = $arrayAnggota[$v]['anggota_id'];
                        $schedule->samplingschedule_sesi     = 'p';
                        $schedule->samplingschedule_status   = 'p';
                        $schedule->samplingschedule_date     = date('Y-m-d',strtotime($date));
                        if(!$schedule->insert()){
                            $error++;
                        }
                        $schedule->samplingschedule_id       = fn::generate_id(true);
                        $schedule->samplingschedule_periode  = $dataPeriode['current_periode'];
                        $schedule->samplingschedule_koperasi = $val['koperasi_id'];
                        $schedule->samplingschedule_tpk      = $arrayAnggota[$v]['anggota_tpk'];
                        $schedule->samplingschedule_anggota  = $arrayAnggota[$v]['anggota_id'];
                        $schedule->samplingschedule_sesi     = 's';
                        $schedule->samplingschedule_status   = 'p';
                        $schedule->samplingschedule_date     = date('Y-m-d',strtotime($date));
                        if(!$schedule->insert()){
                            $error++;
                        }
                    }
                    $index++;
                }
			}
        }
    }
}