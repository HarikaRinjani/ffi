<?php
/**
 * Created by PhpStorm.
 * User: ag
 * Date: 6/2/14
 * Time: 11:40 AM
 */
class Load_Controller extends Controller{
    public $description = 'Load any file. You can use this package to protect your file with session';
    public $version = 1.2;
    public $buildConfiguration = array(
            'config'=>'load'
        );
    function __construct(){
        security::checkSession();
    }
    function file($filename=''){
        if(!empty($filename)){
            load::library('fn');
            $file = rawurldecode($filename);
            $realpath = str_replace('/',DS,$file);
            $filepath = Config::$application['file_path'].$realpath;
			if(file_exists($filepath)){
				$ext = strtolower(substr($filepath, -3));
				if($ext == 'jpg' || $ext == 'png'){
					if(Config::$application['load_image_resize'] === true){
						$cachepath = Config::$application['file_path'].'cache'.DS.$realpath;
						if(file_exists($cachepath)){
							header( 'Cache-Control: max-age=604800' );
							fn::read_file($cachepath);
						}else{
							fn::createDirectory('cache/'.$file,true);
							$filenya = file_get_contents($filepath);
							$src = imagecreatefromstring($filenya);
							list($width, $height) = getimagesize($filepath);

							$newwidth = Config::$application['load_image_maxwidth'];
							$newheight = ($height / $width) * $newwidth;
							$tmp = imagecreatetruecolor($newwidth, $newheight);
							imagecopyresampled($tmp, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
							imagejpeg($tmp,$cachepath,100);
							imagedestroy($src);
							imagedestroy($tmp);
							fn::read_file($cachepath);
						}
					}
					
				}else{
					header( 'Cache-Control: max-age=604800' );
					fn::read_file($filepath);
				}
			}else{
				return null;
			}
        }
    }
    function video($filename=''){
        if(!empty($filename)){
            load::library('fn');
            $file = rawurldecode($filename);
            $realpath = str_replace('/',DS,$file);
            $filepath = Config::$application['file_path'].$realpath;
            header( 'Cache-Control: max-age=604800' );
            fn::read_video_file($filepath);
        }
    }
    function koperasi(){
        $dt = load::model('koperasi');
        if(in_array(session::get('user_group'),Config::$application['super_group'])){
            $dt->setCondition('koperasi_status = \'y\'');
        }else{
            $dt->setCondition('koperasi_status = \'y\' AND koperasi_id = \''.session::get('user_koperasi').'\'');
        }
        $dt->setOrderBy('koperasi_nama');
        $datanya = $dt->read();
        if(isset($_REQUEST['json'])){
            echo json_encode($datanya['records']);
        }else{
            return $datanya;
        }
    }
    function tpk(){
        $dt = load::model('tpk');
        if(in_array(session::get('user_group'),Config::$application['super_group'])){
            $condition = 'tpk_status = \'y\'';
            if(isset($_REQUEST['tpk_koperasi'])){
                $condition .= ' AND tpk_koperaasi = \''.$_REQUEST['tpk_koperasi'].'\'';
            }
        }else{
            $condition = 'tpk_status = \'y\' AND tpk_koperasi = \''.session::get('user_koperasi').'\'';
        }
        $dt->setCondition($condition);
        $dt->setOrderBy('tpk_nama');
        $datanya = $dt->read();
        if(isset($_REQUEST['json'])){
            echo json_encode($datanya['records']);
        }else{
            return $datanya;
        }
    }
    function kelompok(){
        $dt = load::model('kelompok');
        if(in_array(session::get('user_group'),Config::$application['super_group'])){
            $condition = 'kelompok_status = \'y\'';
        }else{
            $condition = 'kelompok_status = \'y\' AND kelompok_koperasi = \''.session::get('user_koperasi').'\'';
        }
        if(isset($_REQUEST['kelompok_tpk'])){
            $condition .= ' AND kelompok_tpk = \''.$_REQUEST['kelompok_tpk'].'\'';
        }
        $dt->setCondition($condition);
        $dt->setOrderBy('kelompok_nama');
        $datanya = $dt->read();
        if(isset($_REQUEST['json'])){
            echo json_encode($datanya['records']);
        }else{
            return $datanya;
        }
    }
    function anggota(){
        $dt = load::model('anggota');
        if(in_array(session::get('user_group'),Config::$application['super_group'])){
            $condition = 'anggota_status = \'y\'';
        }else{
            $condition = 'anggota_status = \'y\' AND anggota_koperasi = \''.session::get('user_koperasi').'\'';
        }
        if(isset($_REQUEST['anggota_tpk'])){
            $condition .= ' AND anggota_tpk = \''.$_REQUEST['anggota_tpk'].'\'';
        }
        if(isset($_REQUEST['anggota_kelompok'])){
            $condition .= ' AND anggota_kelompok = \''.$_REQUEST['anggota_kelompok'].'\'';
        }

        $dt->setCondition($condition);
        $dt->setOrderBy('anggota_nama');
        $datanya = $dt->read();
        if(isset($_REQUEST['json'])){
            echo json_encode($datanya['records']);
        }else{
            return $datanya;
        }
    }
    function autocompleteanggota($koperasi=''){
        $dt = load::model('anggota');
        if(in_array(session::get('user_group'),Config::$application['super_group'])){
            $condition = 'anggota_status = \'y\'';
        }else{
            $condition = 'anggota_status = \'y\' AND anggota_koperasi = \''.session::get('user_koperasi').'\'';
        }
        if($koperasi != ''){
            $condition .= ' AND anggota_koperasi = \''.$koperasi.'\'';
        }
        $_POST['q'] = !isset($_POST['q']) ? '' : strtolower($_POST['q']);
        $dt->setKeyword($_POST['q']);
        $dt->setSearchIn(array(
            'anggota_nama',
            'anggota_nomor'
            ));

        $dt->setCondition($condition);
        $dt->setOrderBy('anggota_nama');
        $datanya = $dt->read();
        $data = array();
		$tempid = array();
		$index = 0;
        foreach($datanya['records'] as $k=>$v){
			if(!in_array($v['anggota_id'],$tempid)){
				$data[$index]['id'] = $v['anggota_id'];
				$data[$index]['name'] = $v['anggota_nomor'].' - '.$v['anggota_nama'];
				$tempid[] = $v['anggota_id'];
				$index++;
			}
        }
        echo json_encode($data);
    }
    function find(){
        $model = $_POST['model'];
        $dt = load::model($model);
        $pkey = $dt->primary_key;
        $status_field = $dt->status_field;
        if(!empty($status_field)){
            if(in_array(session::get('user_group'),Config::$application['super_group'])){
                $dt->setCondition(' '.$status_field.' = \'y\'');
            }else{
                $dt->setCondition(' '.$status_field.' = \'y\' AND '.$model.'_koperasi = \''.session::get('user_koperasi').'\'');
            }
        }else{
            if(!in_array(session::get('user_group'),Config::$application['super_group'])){
                $dt->setCondition(' '.$status_field.' = \'y\' AND '.$model.'_koperasi = \''.session::get('user_koperasi').'\'');
            }
        }
        $dt->setKeyword($_POST['keyword']);
        $dt->setSearchIn($_POST['field']);
        $datas = $dt->read();
        $data = array();
        foreach($datas['records'] as $k=>$v){
            $data[$k]['id'] = $v[$pkey];
            $data[$k]['text'] = $v[$_POST['field']];
        }
        echo json_encode($data);
    }
    function konsumen(){
        $data = $this->_search('konsumen','konsumen_id','konsumen_nama','konsumen_koperasi');
        echo json_encode($data);
    }
    function truck(){
        $data = $this->_search('truck','truck_id','truck_nopol','truck_koperasi');
        echo json_encode($data);
    }
    private function _search($model,$idfield,$namefield,$koperasifield){
        $dt = load::model($model);
        
        $status_field = $dt->status_field;
        if(!empty($status_field)){
            if(in_array(session::get('user_group'),Config::$application['super_group'])){
                $dt->setCondition(' '.$status_field.' = \'y\' AND LOWER('.$namefield.') LIKE \'%'.strtolower($_POST['q']).'%\'');
            }else{
                $dt->setCondition(' '.$status_field.' = \'y\' AND '.$koperasifield.' = \''.session::get('user_koperasi').'\' AND LOWER('.$namefield.') LIKE \'%'.strtolower($_POST['q']).'%\'');
            }
        }else{
            if(!in_array(session::get('user_group'),Config::$application['super_group'])){
                $dt->setCondition($koperasifield.' = \''.session::get('user_koperasi').'\' AND LOWER('.$namefield.') LIKE \'%'.strtolower($_POST['q']).'%\'');
            }
        }
        
        $datas = $dt->read();
        $data = array();
        foreach($datas['records'] as $k=>$v){
            $data[$k]['id'] = $v[$idfield];
            $data[$k]['name'] = $v[$namefield];
        }
        return $data;
    }
    function json($model,$id){
        $dt = load::model($model);
        $dt->setId($id);
        $record = $dt->read();
        echo json_encode($record['records']);
    }
    function transaksi(){
        $dt = load::model('transaksi');
        if(in_array(session::get('user_group'),Config::$application['super_group'])){
            $condition = 'transaksi_status = \'y\'';
        }else{
            $condition = 'transaksi_status = \'y\' AND transaksi_koperasi = \''.session::get('user_koperasi').'\'';
        }
        
        $_POST['q'] = !isset($_POST['q']) ? '' : strtolower($_POST['q']);
        $dt->setKeyword($_POST['q']);
        $dt->setSearchIn(array(
            'transaksi_id'
            ));

        $dt->setCondition($condition);
        $dt->setOrderBy('transaksi_waktu');
        $datanya = $dt->read();
        $data = array();
        foreach($datanya['records'] as $k=>$v){
            $data[$k] = $v;
            $data[$k]['id'] = $v['anggota_id'];
            $data[$k]['name'] = $v['transaksi_id'];
        }
        echo json_encode($data);
    }
    function createPdf($transaksi_id = ''){
        if(isset($_POST['html'])){
            require_once path('sys') . DIRECTORY_SEPARATOR . 'libraries' . DIRECTORY_SEPARATOR . 'dompdf' . DIRECTORY_SEPARATOR . 'dompdf_config.inc.php';
            $dompdf = new DOMPDF();
            $dompdf-> set_paper('a7','portrait');
            $dompdf->load_html($_POST['html']);
            $dompdf->render();
            $output = $dompdf->output();
            if(empty($transaksi_id)){
                $transaksi_id = fn::generate_id(true);
            }
            $tmp_file = 'transaksi/'.date('Y').'/'.date('m').'/'.date('d').'/'.$transaksi_id.'.pdf';
            fn::createDirectory($tmp_file,true);
            $original_file = Config::$application['file_path'].str_replace('/',DS,$tmp_file);
            touch($original_file);
            file_put_contents($original_file, $output);
        }
    }
    function generateCardNumber(){
        $dt = load::model('card');
        echo $dt->generateNumber();
    }
}