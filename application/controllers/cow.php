<?php

/**
 * Cow Management
 */
class Cow_Controller extends Controller{
	public $version = 1.1;
	public $description = 'Cow Population Module for MCP';
	public $buildConfiguration = array(
		'table'=>'cow',
		'models'=>'cow',
		'views'=>array('cow.index','cow.history')
		);
	function __construct(){
		security::checkSession();
	}
	function index(){
		header('location:'.BASE_URL.'index.php?/cow/management');
	}
	function management(){
		session::set('title','Cow Population Management');
		$dt              = load::model('cow');
		$koperasi        = load::model('koperasi');
		$_GET['page']    = !isset($_GET['page']) ? 1 : $_GET['page'];
		$_GET['keyword'] = !isset($_GET['keyword']) ? '' : $_GET['keyword'];

		if(!in_array(session::get('user_group'),Config::$application['super_group'])){
			$_GET['koperasi'] = session::get('user_koperasi');
			$koperasi->setCondition('koperasi_id = \''.session::get('user_koeprasi').'\'');
			$data['koperasi'] = $koperasi->read();
		}else{
			$_GET['koperasi'] = !isset($_GET['koperasi']) ? '' : $_GET['koperasi'];
			$koperasi->setCondition('koperasi_status = \'y\'')->setOrderBy('koperasi_nama')->setLimit(0);
			$data['koperasi'] = $koperasi->read();
		}

		$addSQL = '';
		if(!empty($_GET['koperasi'])){
			$addSQL = ' AND t_anggota.anggota_koperasi = \''.$_GET['koperasi'].'\'';
		}

		$cowdata = $dt->setCondition('cow_status = \'y\''.$addSQL)
						->setKeyword($_GET['keyword'])
						->setSearchIn(array(
							't_anggota.anggota_nama',
							't_koperasi.koperasi_nama',
							't_kelompok.kelompok_nama',
							't_tpk.tpk_nama'
							))
						->setPage($_GET['page'])
						->setOrderBy('t_anggota.anggota_nama')
						->read();

		$data['data'] = $cowdata;
		View::display('cow/cow.index',$data);
	}

	function history($id=''){
		if(!empty($id)){
			$dt = load::model('cow');
			$anggota = load::model('anggota');
			$farmerdata = $anggota->setId($id)->read();
			if($farmerdata['records']['anggota_photo'] == '' || $farmerdata['records']['anggota_photo'] == null){
			    if(file_exists(Config::$application['file_path'].'images'.DS.'PASFOTO'.DS.$farmerdata['records']['anggota_nomor'].'.JPG')){
			        $photo = 'index.php?/load/file/'.urlencode('images/PASFOTO/'.$farmerdata['records']['anggota_nomor'].'.JPG');
			    }else{
			        $photo = 'index.php?/load/file/'.urlencode('images/avatar.jpg');
			    }
			}else{
			    $photo = $farmerdata['records']['anggota_photo'];
			}
			$farmerdata['records']['anggota_photo'] = $photo;

			$_GET['page'] = !isset($_GET['page']) ? 1 : $_GET['page'];
			$cowdata = $dt->setCondition('cow_status != \'d\' AND cow_anggota = \''.$id.'\'')
							->setPage($_GET['page'])
							->setOrderBy('cow_entrydate')
							->setOrderType('DESC')
							->read();

			$data['data'] = $cowdata;
			$data['anggota'] = $farmerdata['records'];
			View::display('cow/cow.history',$data);
		}
	}
	function edit($id=''){
		if(!empty($id)){
			$dt = load::model('cow');
			$cowdata = $dt->setCondition('cow_status = \'y\' AND cow_anggota = \''.$id.'\'')
							->setLimit(1)
							->setOrderBy('cow_entrydate')
							->setOrderType('DESC')
							->read();

			echo json_encode($cowdata['records'][0]);
		}
	}
	function save(){
		$dt = load::model('cow');
		$cowdata = $dt->setCondition('cow_status = \'y\' AND cow_anggota = \''.$_POST['cow_anggota'].'\'')
							->setLimit(1)
							->setOrderBy('cow_entrydate')
							->setOrderType('DESC')
							->read();
		$lastid = $cowdata['records'][0]['cow_id'];
		$newid = fn::generate_id(true);
		if($_POST['cow_antibiotic'] > 0){
			if(empty($_POST['cow_antibioticstartdate'])){
				$antibioticstartdate = date('Y-m-d');
			}else{
				$antibioticstartdate = date('Y-m-d',fn::dateToUnix($_POST['cow_antibioticstartdate']));
			}
			if(empty($_POST['cow_antibioticenddate'])){
				$antibioticenddate = date('Y-m-d',strtotime('+1 week'));
			}else{
				$antibioticenddate = date('Y-m-d',fn::dateToUnix($_POST['cow_antibioticenddate']));
			}
		}else{
			$antibioticstartdate = date('Y-m-d');
			$antibioticenddate = date('Y-m-d');
		}
		$dt->setDataArray(
			array(
				'cow_id'                  =>$newid,
				'cow_anggota'             =>$_POST['cow_anggota'],
				'cow_status'              =>'y',
				'cow_entryby'             =>session::get('user_id'),
				'cow_entrydate'           =>date('Y-m-d H:i:s'),
				'cow_lactating'           =>$_POST['cow_lactating'],
				'cow_population'          =>$_POST['cow_population'],
				'cow_antibiotic'          =>$_POST['cow_antibiotic'],
				'cow_antibioticstartdate' =>$antibioticstartdate,
				'cow_antibioticenddate'   =>$antibioticenddate,
				'cow_antibioticremark'    =>$_POST['cow_antibioticremark'],
				'cow_koperasi'            =>$cowdata['records'][0]['cow_koperasi'],
				'cow_tpk'                 =>$cowdata['records'][0]['cow_tpk'],
				'cow_kelompok'            =>$cowdata['records'][0]['cow_kelompok']
				)
			);

		if($dt->insert()){
			$dt->resetValue();
			$dt->setId($lastid);
			$dt->cow_status = 'n';
			if($dt->update()){
				$repo = load::model('repo');
				$repo->updateRepo('anggota', $cowdata['records'][0]['cow_koperasi'], $cowdata['records'][0]['cow_tpk']);

				session::set('errorMsg','Data has been successfully saved');
			}else{
				$dt->setId($newid);
				$dt->delete();
				session::set('errorMsg','Failed while saving data into database');
			}
		}else{
			session::set('errorMsg','Failed while saving data into database');
		}
		header('location:'.BASE_URL.'index.php?/cow/management');
	}
}
