<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 06/01/15
 * Time: 12:20
 */
class Koperasi_Controller extends Controller{
    public $description = 'Cooperative Module (MCP)';
    public $buildConfiguration = array(
        'table'=>'koperasi',
        'views'=>array(
            'koperasi.setting'
        ),
        'models'=>'koperasi'
    );

    function __construct(){
        security::checkSession();
    }
    function index(){

    }
    function setting($operation='',$id=''){
        session::set('title','koperasi Settings');
        load::library('fn');
        $koperasi = load::model('koperasi');
        $repo = load::model('repo');
        switch($operation){
            case 'save':
                if(!in_array(session::get('user_group'),Config::$application['super_group'])){
                    //$koperasi->setCondition('koperasi_id = \''.$_POST['koperasi_id'].'\' AND koperasi_id = \''.session::get('user_koperasi').'\'');
                    session::set('errorMsg','You dont have permission to modify data');
                }else{
                    if(isset($_POST['koperasi_id'])){
                        $koperasi->setCondition('koperasi_kode = \''.$_POST['koperasi_kode'].'\' AND koperasi_status != \'d\' AND koperasi_id != \''.$_POST['koperasi_id'].'\'');
                        $cek = $koperasi->read();
                        if($cek['total_data'] > 0){
                            session::set('errorMsg',_('Coop Code already exists'));
                            header('location:'.BASE_URL.'index.php?/koperasi/setting');
                            exit();
                        }else{
                            $koperasi->setId($_POST['koperasi_id']);
                            $koperasi->setDataArray($_POST);
                            $query = $koperasi->update();
                        }

                    }else{
                        $_POST['koperasi_id'] = fn::generate_id(true);
                        $koperasi->setCondition('koperasi_kode = \''.$_POST['koperasi_kode'].'\' AND koperasi_status != \'d\'');
                        $cek = $koperasi->read();
                        if($cek['total_data'] > 0){
                            session::set('errorMsg',_('Coop Code already exists'));
                            header('location:'.BASE_URL.'index.php?/koperasi/setting');
                            exit();
                        }else{
                            $koperasi->setDataArray($_POST);
                            $query = $koperasi->insert();
                        }
                    }
                    if($query){
                        $repo->updateRepo('koperasi');
                        session::set('errorMsg',_('Data has been successfully saved'));
                    }else{
                        session::set('errorMsg',_('Failed while saving data into database'));
                    }
                }

                header('location:'.BASE_URL.'index.php?/koperasi/setting');

                break;
            case 'edit':
                if(!empty($id)){

                    if(!in_array(session::get('user_group'),Config::$application['super_group'])){
                        $koperasi->setCondition('koperasi_id = \''.$id.'\' AND koperasi_id = \''.session::get('user_koperasi').'\'');
                        $koperasi->setLimit(1);
                        $dept = $koperasi->read();
                        echo json_encode($dept['records'][0]);
                    }else{
                        $koperasi->setId($id);
                        $dept = $koperasi->read();
                        echo json_encode($dept['records']);
                    }

                }

                break;
            case 'delete':
                if(isset($_POST['koperasi_id'])){
                    if(!in_array(session::get('user_group'),Config::$application['super_group'])){
                        //$koperasi->setCondition('koperasi_id = \''.$_POST['koperasi_id'].'\' AND koperasi_id = \''.session::get('user_koperasi').'\'');
                        session::set('errorMsg','Cannot delete current data');
                    }else{
                        $koperasi->setId($_POST['koperasi_id']);
                        if($koperasi->delete()){
                            $repo->updateRepo('koperasi');
                            session::set('errorMsg',_('Data has been successfully deleted'));
                        }else{
                            session::set('errorMsg',_('Failed while deleting data'));
                        }
                    }
                }
                header('location:'.BASE_URL.'index.php?/koperasi/setting');

                break;
            default:

                $_GET['page'] = !isset($_GET['page']) ? 1 : $_GET['page'];
                $koperasi->setPage($_GET['page']);
                if(isset($_GET['keyword'])){
                    $koperasi->setKeyword($_GET['keyword']);
                    $koperasi->setSearchIn(array(
                        'koperasi_telp',
                        'koperasi_nama',
                        'koperasi_kode',
                        'koperasi_alamat'
                    ));
                }
                $koperasi->setLimit(Config::$application['display_limit']);
                if(!in_array(session::get('user_group'),Config::$application['super_group'])){
                    $koperasi->setCondition('koperasi_status != \'d\' AND koperasi_id = \''.session::get('user_koperasi').'\'');
                }else{
                    $koperasi->setCondition('koperasi_status != \'d\'');
                }

                $data['koperasi'] = $koperasi->read();
                View::display('koperasi/koperasi.setting',$data);

                break;
        }
    }
    function loadAll(){
        $dt = load::model('koperasi');
        if(!in_array(session::get('user_group'),Config::$application['super_group'])){
            $dt->setCondition('koperasi_status = \'y\' AND koperasi_id = \''.session::get('user_koperasi').'\'');
        }else{
            $dt->setCondition('koperasi_status = \'y\'');
        }
        $dt->setLimit(0);
        $dt->setOrderBy('koperasi_nama');
        $data = $dt->read();
        echo json_encode($data);
    }
}
