<?php
class Card_Controller extends Controller{
	public $buildConfiguration = array(
		'table'=>'card',
		'views'=>array(
				'card.history',
				'card.inactive',
				'card.index'
		),
		'models'=>'card'
	);

	public $dependencies = array(
		array(
			'name'=>'anggota',
			'version'=>1
			)
		);
	public $version = 1.3;
	public $description = "v1.3\n- Fix paging\n- Add Coop Filter\n\nv1.2\n- Add deppendencies";
	function __construct(){
		parent::__construct();
		security::checkSession();
	}
	function index(){
		session::set('title',_('Card Management'));
		$koperasi = load::model('koperasi');
		$_GET['page'] = !isset($_GET['page']) ? 1 : $_GET['page'];
		$_GET['keyword'] = !isset($_GET['keyword']) ? '' : $_GET['keyword'];
		if(!in_array(session::get('user_group'),Config::$application['super_group'])){
			$_GET['koperasi'] = session::get('user_koperasi');
			$koperasi->setCondition('koperasi_id = \''.session::get('user_koeprasi').'\'');
			$data['koperasi'] = $koperasi->read();
		}else{
			$_GET['koperasi'] = !isset($_GET['koperasi']) ? '' : $_GET['koperasi'];
			$koperasi->setCondition('koperasi_status = \'y\'')->setOrderBy('koperasi_nama')->setLimit(0);
			$data['koperasi'] = $koperasi->read();
		}
		$addSQL = '';
		if(!empty($_GET['koperasi'])){
			$addSQL = ' AND t_anggota.anggota_koperasi = \''.$_GET['koperasi'].'\'';
		}
		$dt = load::model('card');
		$carddata = $dt->setCondition('card_status = \'y\''.$addSQL)
					   ->setPage($_GET['page'])
					   ->setKeyword($_GET['keyword'])
					   ->setSearchIn(array(
					   						'card_number',
					   						't_anggota.anggota_nama',
					   						't_koperasi.koperasi_nama',
					   						't_tpk.tpk_nama',
					   						't_kelompok.kelompok_nama'))
					   ->read();
		$data['card'] = $carddata;
		View::display('card/card.index',$data);
 	}
 	function inactive(){
		session::set('title',_('Card Management'));
		$koperasi=  load::model('koperasi');
		$_GET['page'] = !isset($_GET['page']) ? 1 : $_GET['page'];
		$_GET['keyword'] = !isset($_GET['keyword']) ? '' : $_GET['keyword'];
		if(!in_array(session::get('user_group'),Config::$application['super_group'])){
			$_GET['koperasi'] = session::get('user_koperasi');
			$koperasi->setCondition('koperasi_id = \''.session::get('user_koeprasi').'\'');
			$data['koperasi'] = $koperasi->read();
		}else{
			$_GET['koperasi'] = !isset($_GET['koperasi']) ? '' : $_GET['koperasi'];
			$koperasi->setCondition('koperasi_status = \'y\'')->setOrderBy('koperasi_nama')->setLimit(0);
			$data['koperasi'] = $koperasi->read();
		}
		$addSQL = '';
		if(!empty($_GET['koperasi'])){
			$addSQL = ' AND t_anggota.anggota_koperasi = \''.$_GET['koperasi'].'\'';
		}
		$dt = load::model('card');
		$carddata = $dt->setCondition('card_status = \'n\''.$addSQL)
					   ->setPage($_GET['page'])
					   ->setKeyword($_GET['keyword'])
					   ->setSearchIn(array(
					   						'card_number',
					   						't_anggota.anggota_nama',
					   						't_koperasi.koperasi_nama',
					   						't_tpk.tpk_nama',
					   						't_kelompok.kelompok_nama'))
					   ->read();
		$data['card'] = $carddata;
		View::display('card/card.inactive',$data);
 	}
 	function history($card_id=''){
 		if(!empty($card_id)){
 			$_GET['keyword'] = !isset($_GET['keyword']) ? '' : $_GET['keyword'];
 			$_GET['page'] = !isset($_GET['page']) ? 1 : $_GET['page'];
 			$dt = load::model('card');
 			$anggota = load::model('anggota');
 			$cek = $dt->setId($card_id)->read();
 			$dt->resetValue();
 			$dt->setCondition('card_anggota = \''.$cek['records']['card_anggota'].'\' AND card_status != \'d\'');
 			$dt->setPage($_GET['page']);
 			$dt->setKeyword($_GET['keyword']);
 			$dt->setSearchIn('card_number');
 			$dt->setOrderBy('card_entrydate');
 			$dt->setOrderType('ASC');
 			$data['card'] = $dt->read();
 			$data['card_id'] = $card_id;
 			$farmerdata = $anggota->setId($cek['records']['card_anggota'])->read();
 			if($farmerdata['records']['anggota_photo'] == '' || $farmerdata['records']['anggota_photo'] == null){
			    if(file_exists(Config::$application['file_path'].'images'.DS.'PASFOTO'.DS.$farmerdata['records']['anggota_nomor'].'.JPG')){
			        $photo = 'index.php?/load/file/'.urlencode('images/PASFOTO/'.$farmerdata['records']['anggota_nomor'].'.JPG');
			    }else{
			        $photo = 'index.php?/load/file/'.urlencode('images/avatar.jpg');
			    }
			}else{
			    $photo = $farmerdata['records']['anggota_photo'];
			}
			$farmerdata['records']['anggota_photo'] = $photo;
 			$data['anggota'] = $farmerdata['records'];
 			View::display('card/card.history',$data);
 		}
 	}
 	function generate(){
 		if(isset($_POST['card_id'])){
 			//$anggota = load::model('anggota');
 			$dt = load::model('card');
 			$cek = $dt->setId($_POST['card_id'])->read();
 			if($cek['total_data'] > 0){
 				$newnumber = $dt->generateNumber();
 				$newid = fn::generate_id(true);
 				$datacard = $cek['records'];
 				$datacard['card_id'] = $newid;
 				$datacard['card_number'] = $newnumber;
 				$datacard['card_entryby'] = session::get('user_id');
 				$datacard['card_entrydate'] = date('Y-m-d H:i:s');
 				if($dt->setDataArray($datacard)->insert()){
 					$dt->resetValue();
 					$dt->setId($_POST['card_id']);
 					$dt->setData('card_status','n');
 					if($_POST['card_remark'] == 'other'){
 						$dt->setData('card_remark',$_POST['card_remark_other']);
 					}else{
 						$dt->setData('card_remark',$_POST['card_remark']);
 					}
 					if($dt->update()){
 						session::set('errorMsg','Card Number Generated');
 					}else{
 						$dt->setId($newid);
 						$dt->delete();
 						session::set('errorMsg','failed while updating card data');
 					}
 				}else{
 					session::set('errorMsg','Failed while generating new card number');
 				}
 				
 			}else{
 				session::set('errorMsg','Invalid ID');
 			}
 		}else{
 			session::set('errorMsg','Invalid ID');
 		}
 		header('location:'.BASE_URL.'index.php?/card/index');
 		exit();
 	}
	function settings(){

	}
}
