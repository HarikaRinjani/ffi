<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 07/01/15
 * Time: 11:30
 */
class User_Controller extends Controller{
    public $description = "v2.4\n- Add Filter\n\nv2.3\n- Fix error while adding new user as FFI QC or FFI Manager\n\n.Change Log v2.2\n- Add Card Permission Setting \n\nChange Log v2.1:\n- Show last login status on user manager";
    public $version = 2.4;
    public $publisher = 'Transformatika';
    public $buildConfiguration = array(
        'table'=>'user',
        'models'=>'user',
        'views'=>array(
            'user.usermanager'
        ),
        'config'=>'usergroup'
    );
    function __construct(){
        security::checkSession();
    }
    function index(){

    }

    function usermanager(){
        session::set('title','User Manager');
        $user = load::model('user');
        $koperasi = load::model('koperasi');
        $_GET['koperasi'] = !isset($_GET['koperasi']) ? '' : $_GET['koperasi'];
        $_GET['user_group'] = !isset($_GET['user_group']) ? '' : $_GET['user_group'];

        if(isset($_GET['page'])){
            $user->setPage($_GET['page']);
        }
        if(isset($_GET['keyword'])){
            $user->setKeyword($_GET['keyword']);
            $user->setSearchIn(array('user_fullname','user_name'));
        }
        $koperasiSQL = '';
        $userGroupSQL = '';

        if(!empty($_GET['koperasi'])){
            $koperasiSQL = ' AND user_koperasi = \''.$_GET['koperasi'].'\'';
        }

        if(!empty($_GET['user_group'])){
            $userGroupSQL = ' AND user_group = \''.$_GET['user_group'].'\'';
        }

        $user->setLimit(Config::$application['display_limit']);
        $user->setCondition('user_status != \'d\''.$koperasiSQL.$userGroupSQL);
        $user->setOrderBy('user_fullname');
        $koperasi = load::model('koperasi');
        $koperasi->setCondition('koperasi_status = \'y\'');
        $koperasi->setLimit(0);
        $data['koperasi'] = $koperasi->read();
        $data['user'] = $user->read();
        View::display('user/user.usermanager',$data);
    }

    function saveuser(){
        $repo = load::model('repo');
        if(isset($_POST['user_name'])){
            if(!in_array($_POST['user_group'],Config::$application['super_group']) && $_POST['user_koperasi'] == 'none'){
                session::set('errorMsg',_('Please select Cooperative'));
                header('location:'.BASE_URL.'index.php?/user/usermanager');
                exit();
            }else{
                $dt = load::model('user');
                if(isset($_POST['user_id'])){
                    $edit = true;
                }else{
                    $edit = false;
                    $dt->setCondition('user_name = \''.$_POST['user_name'].'\'');
                    $cek = $dt->read();
                    if($cek['total_data'] > 0){
                        session::set('errorMsg',_('Username already taken'));
                        header('location:'.BASE_URL.'index.php?/user/usermanager');
                        exit();
                    }
                    $_POST['user_id'] = fn::generate_id();
                }
                if(isset($_POST['user_password']) && !empty($_POST['user_password'])){
                    $_POST['user_password'] = security::encrypt($_POST['user_password']);
                }else{
                    unset($_POST['user_password']);
                }
                if(isset($_FILES['user_avatar']['name']) && !empty($_FILES['user_avatar']['name'])){
                    $uniqid = fn::generate_id();
                    move_uploaded_file($_FILES['user_avatar']['tmp_name'],Config::$application['file_path'].DS.'images'.DS.'avatar'.DS.$uniqid.$_FILES['user_avatar']['name']);
                    $_POST['user_avatar'] = 'images/avatar/'.$uniqid.$_FILES['user_avatar']['name'];
                }else{
                    unset($_POST['user_avatar']);
                }

                $dt->setDataArray($_POST);
                if($edit===true){
                    $dt->setId($_POST['user_id']);
                    $query = $dt->update();
                }else{
                    $query = $dt->insert();
                }
                if($query){
                    $repo->updateRepo('user', $_POST['user_id']);
                    session::set('errorMsg',_('Data successfully saved'));
                }else{
                    session::set('errorMsg',_('Failed while saving into database'));
                }
                header('location:'.BASE_URL.'index.php?/user/usermanager');
            }
        }

    }

}
