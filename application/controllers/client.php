<?php 

class Client_Controller extends Controller
{
	public function __construct()
	{
		parent::__construct();
		security::checkSession();
	}

	public function index()
	{
		session::set('title', 'Client Credential');
		$client = load::model('OauthClients');
		$data['clients'] = $client->setLimit(0)->read();
		$koperasi 	  = load::model('koperasi');

		$data['koperasi'] = $koperasi->setLimit(0)->setCondition('koperasi_status = \'y\'')->read();
		View::display('client/client.index', $data);
	}

	public function save()
	{
		$client = load::model('OauthClients');
		$save = $client->setDataArray($_POST)->insert();
		if ($save) {
			session::set('errorMsg', 'Client Saved');
		} else {
			session::set('errorMsg', 'Failed while saving data into database');
		}
		header('location:'.BASE_URL.'index.php?/client/index/');
		exit();
	}
}