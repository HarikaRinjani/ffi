<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 12/8/2014
 * Time: 9:53 AM
 */
class Dashboard_Controller extends Controller{
    public $buildConfiguration = array(
        'views'=>array(
            'dashboard.index',
            'dashboard.menu',
            'dashboard.farmer'
        )
    );
    public $description = "v1.4 - (2016-02-29) Fix Dashboard Farmer \n\nv1.3\n- Grafik Farmer Report \n\nv1.2\n- Farmer Dashboard \n\nv1.1 Change logs:\n\n- Add summary transaction report";
    public $version = 1.4;
    function __construct(){
        security::checkSession();
    }
    function index(){
        if(session::get('user_group') === 'farmer'){
            header('location:'.BASE_URL.'index.php?/dashboard/farmer');exit();
        }
        session::set('title','Dashboard');
        load::library('db');
        $notice = array();
        load::library('fn');
        
        if(!in_array(session::get('user_group'),Config::$application['super_group'])){
            $koperasi_id = session::get('user_koperasi');
            $jam = date('G');
            if($jam <= Config::$application['batas_pagi']){
                $sesi = 'p';
            }else{
                $sesi = 's';
            }
            $schedule = load::model('samplingschedule');
            $currentPeriode = $schedule->getCurrentPeriode($koperasi_id);
            $schedule->setCondition('samplingschedule_periode = '.$currentPeriode.' AND samplingschedule_date = \''.date('Y-m-d').'\' AND samplingschedule_sesi = \''.$sesi.'\' AND samplingschedule_koperasi = \''.$koperasi_id.'\'');     
            $dataSchedule = $schedule->read();
            
            if($dataSchedule['total_data'] == 0){
                $notice[] = 'Sampling Schedule has not been made! <a href="javascript:void(0);" onclick="generateSampling(\''.$koperasi_id.'\');">Click here to generate</a>';
            }
        }
       

        $data['TPK']              = db::count('t_tpk','WHERE tpk_status != \'d\'');
        $data['Kelompok']         = db::count('t_kelompok','WHERE kelompok_status != \'d\'');
        $data['Anggota']          = db::count('t_anggota','WHERE anggota_status != \'d\'');
        $data['Truck']            = db::count('t_truck','WHERE truck_status != \'d\'');
        $data['Konsumen']         = db::count('t_konsumen','WHERE konsumen_status != \'d\'');
        
        foreach($data as $k=>$v){
            if($v == 0){
                $notice[] = $k.' Module not configured properly. please go to <a href="'.BASE_URL.'index.php?/settings/index">Apps Settings</a> to fix it';
            }
        }
        if(Config::$application['mcp_tpk'] == ''){
            $notice[] = 'Client Configuration not configured properly';
        }

        //print_r($data);exit();
        $res['notice'] = $notice;
        $res['widget'] = array();
        //var_dump(Config::$table);exit();
        if(Config::$application['is_client'] === true){
            $transaksi = load::model('transaksi');
            $transaksi->setCondition('transaksi_user = \''.session::get('user_id').'\' AND transaksi_status = \'y\'');
            $transaksi->setChartType('column');
            $transaksi->setChartDate(date('Y-m-d'));
            $transaksi->setChartTitle('Today Report');
            $transaksi->setChartSubtitle('Date: '.date('d F Y'));
            $transaksi->setChartHeight('300px');
            $transaksi->setChartPeriode('date');
            $transaksi->setChartPeriodeField('transaksi_waktu');
            $transaksi->setChartFields(array(
                'transaksi_id'=>array(
                    'operation'=>'count',
                    'title'=>_('Transaction')
                ),
                'transaksi_totalberat'=>array(
                    'operation'=>'sum',
                    'title'=>_('Weight (Kg)')
                )
            ));
            $transaksi->setChartContainer('widget_transaksi_chart');
            $res['chart'] = $transaksi->generateChart();
        }

        View::display('dashboard/dashboard.index',$res);
    }

    /**
     * Farmer Dashboard
     * @return [type] [description]
     */
    function farmer(){
        if(session::get('user_group') !== 'farmer'){
            session::set('errorMsg','You dont have permission to access this page!');
            header('location:'.BASE_URL.'index.php?/dashboard');
            exit();
        }

        $anggota            = load::model('anggota');
        $qcparameter        = load::model('qcparameter');
        $milkpriceformula   = load::model('milkpriceformula');
        $milkpricecondition = load::model('milkpricecondition');
        $coop               = load::model('koperasi');
        $periode            = load::model('periode');
        $rezgrade           = load::model('rezgrade');
        $fpgrade            = load::model('fpgrade');
        $cow 				= load::model('cow');
        
        $_GET['page']       = !isset($_GET['page']) ? 1 : $_GET['page'];
        $_GET['keyword']    = !isset($_GET['keyword']) ? '' : $_GET['keyword'];
        $_GET['month']      = !isset($_GET['month']) ? date('n') : $_GET['month'];
        $_GET['year']       = !isset($_GET['year']) ? date('Y') : $_GET['year'];
        $_GET['periode']    = !isset($_GET['periode']) ? '' : $_GET['periode'];
        
        $dataAnggota         = $anggota->setCondition('anggota_nomor = \''.session::get('user_name').'\' AND anggota_status = \'y\' AND t_card.card_status = \'y\' AND t_cow.cow_status = \'y\'')->setLimit(1)->read();
		$_GET['anggota']    = $dataAnggota['records'][0]['anggota_id'];
        $koperasi = session::get('user_koperasi');
		
		$tglCekPeriode = date('Y-m-t', strtotime($_GET['year'].'-'.$_GET['month'].'-01'));
		
        /** Data periode */
        $dtPeriode = $periode->setCondition('periode_status = \'y\' AND periode_koperasi = \''.$koperasi.'\' AND DATE(periode_effectivedate) <= \''.$tglCekPeriode.'\'')
							 ->setLimit(1)
							 ->setOrderBy('periode_effectivedate')
							 ->setOrderType('DESC')
							 ->read();
		
        if($dtPeriode['total_data'] === 0){
            session::set('errorMsg','Please setup periode!');
            header('location:'.BASE_URL.'index.php?/dashboard');
            exit();
        }

        $arrayPeriode = $dtPeriode['records'][0]['periodedetail'];
   
        //$dataAnggota = $anggota->read();
        $anggotaList = '';
        $dataNya = array();
        foreach($dataAnggota['records'] as $key=>$val){
			if($anggotaList != ''){
				$anggotaList .= ',';
			}
			$anggotaList .= '\''.$val['anggota_id'].'\'';
			//array_push($dataNya,$val['anggota_id'])
			foreach($arrayPeriode as $k=>$v){
				$dataNya[$val['anggota_id']][$v['periodedetail_name']]               = $val;
				$dataNya[$val['anggota_id']][$v['periodedetail_name']]['hargadasar'] = 0;
				$dataNya[$val['anggota_id']][$v['periodedetail_name']]['hargafp']    = 0;
				$dataNya[$val['anggota_id']][$v['periodedetail_name']]['hargarez']   = 0;
				$dataNya[$val['anggota_id']][$v['periodedetail_name']]['qcparameter_cfu']        = 0;
				$dataNya[$val['anggota_id']][$v['periodedetail_name']]['qcparameter_ibc']        = 0;
				$dataNya[$val['anggota_id']][$v['periodedetail_name']]['qcparameter_fat']        = 0;
				$dataNya[$val['anggota_id']][$v['periodedetail_name']]['qcparameter_protein']    = 0;
				$dataNya[$val['anggota_id']][$v['periodedetail_name']]['qcparameter_lactose']    = 0;
				$dataNya[$val['anggota_id']][$v['periodedetail_name']]['qcparameter_ts']         = 0;
				$dataNya[$val['anggota_id']][$v['periodedetail_name']]['qcparameter_fp']         = 0;
				$dataNya[$val['anggota_id']][$v['periodedetail_name']]['qcparameter_acidity']    = 0;
				$dataNya[$val['anggota_id']][$v['periodedetail_name']]['qcparameter_urea']       = 0;
				$dataNya[$val['anggota_id']][$v['periodedetail_name']]['qcparameter_snf']        = 0;
			}

		}

        $dataFormula = $milkpriceformula->setCondition('milkpriceformula_koperasi = \''.$koperasi.'\' AND milkpriceformula_status = \'y\' AND date_part(\'month\',milkpriceformula_effectivedate) <= \''.$_GET['month'].'\' AND date_part(\'year\',milkpriceformula_effectivedate) <= \''.$_GET['year'].'\'')
												->setLimit(1)
												->setOrderBy('milkpriceformula_effectivedate')
												->setOrderType('ASC')
												->read();

		$dataRezgrade = $rezgrade->setCondition('rezgrade_koperasi = \''.$koperasi.'\' AND date_part(\'month\',rezgrade_effectivedate) <= '.$_GET['month'].' AND date_part(\'year\',rezgrade_effectivedate) <= '.$_GET['year'])
								 ->setLimit(1)
								 ->setOrderBy('rezgrade_effectivedate')
								 ->setOrderType('ASC')
								 ->read();

		$dataFpgrade = $fpgrade->setCondition('fpgrade_koperasi = \''.$koperasi.'\' AND date_part(\'month\',fpgrade_effectivedate) <= '.$_GET['month'].' AND date_part(\'year\',fpgrade_effectivedate) <= '.$_GET['year'])
								 ->setLimit(1)
								 ->setOrderBy('fpgrade_effectivedate')
								 ->setOrderType('ASC')
								 ->read();


		if($anggotaList != ''){
			/** Revisi 1.4: perhitungan periode berdasarkan tanggal transaksi */
			// $dataParameter = $qcparameter->setCondition('qcparameter_anggota IN ('.$anggotaList.') AND date_part(\'month\',t_transaksi.transaksi_waktu) = \''.$_GET['month'].'\' AND date_part(\'year\',t_transaksi.transaksi_waktu) = \''.$_GET['year'].'\' AND qcparameter_status = \'y\'')
			// 							 ->setLimit(0)
			// 							 ->read();
			/** End Revisi 1.4 */

			/** Revisi 1.5: perhitungan periode berdasarkan tanggal scan bacto */
			$dataParameter = $qcparameter->setCondition('qcparameter_anggota IN ('.$anggotaList.') AND date_part(\'month\',qcparameter_scandate) = \''.$_GET['month'].'\' AND date_part(\'year\',qcparameter_scandate) = \''.$_GET['year'].'\' AND qcparameter_status = \'y\'')
										 ->setLimit(0)
										 ->read();
			/** End Revisi 1.5*/

			/**
			* Fix double data
			**/
			$dataParameterFarmer = array();
			foreach($dataParameter['records'] as $key=>$val){
				if(empty($val['qcparameter_scandate'])){
					$val['qcparameter_scandate'] = $val['transaksi_waktu'];
				}

				foreach($arrayPeriode as $prk=>$prv){
					if($prv['periodedetail_startdate'] <= (int) date('j',strtotime($val['qcparameter_scandate'])) && $prv['periodedetail_enddate'] >= (int) date('j',strtotime($val['qcparameter_scandate']))){
						$val['transaksi_periode'] = $prv['periodedetail_name'];
					}

				}
				if(!isset($dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']])){
					$dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']] = $val;
				}else{
					if((float)$dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_cfu'] <= 0){
						$dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_cfu'] = $val['qcparameter_cfu'];
					}
					if((float)$dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_ibc'] <= 0){
						$dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_ibc'] = $val['qcparameter_ibc'];
					}
					if((float)$dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_fat'] <= 0){
						$dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_fat'] = $val['qcparameter_fat'];
					}
					if((float)$dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_protein'] <= 0){
						$dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_protein'] = $val['qcparameter_protein'];
					}
					if((float)$dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_lactose'] <= 0){
						$dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_lactose'] = $val['qcparameter_lactose'];
					}
					if((float)$dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_ts'] <= 0){
						$dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_ts'] = $val['qcparameter_ts'];
					}
					if((float)$dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_fp'] <= 0){
						$dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_fp'] = $val['qcparameter_fp'];
					}
					if((float)$dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_acidity'] <= 0){
						$dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_acidity'] = $val['qcparameter_acidity'];
					}
					if((float)$dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_urea'] <= 0){
						$dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_urea'] = $val['qcparameter_urea'];
					}
					if((float)$dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_snf'] <= 0){
						$dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_snf'] = $val['qcparameter_snf'];
					}
				}
			}
			
			foreach($dataParameterFarmer as $pkey=>$pval){
				/** Revisi 1.6 */
				foreach($pval as $key=>$val){
					$hargadasar = (float) $dataFormula['records'][0]['milkpriceformula_hargadasar'];
					$hargarez   = 0;
					$hargafp    = 0;
					$selisih    = array();
					$beda       = 0;

					/** Revisi 1.4: perhitungan periode berdasarkan tanggal transaksi */
					/** Fix Empty Periode */ 
					// if((int)$val['transaksi_periode'] < 1){
					// 	foreach($arrayPeriode as $prk=>$prv){
					// 		if($prv['periodedetail_startdate'] <= (int) date('j',strtotime($val['transaksi_waktu'])) && $prv['periodedetail_enddate'] >= (int) date('j',strtotime($val['transaksi_waktu']))){
					// 			$val['transaksi_periode'] = $prv['periodedetail_name'];
					// 		}

					// 	}
					// }
					/** End Revisi 1.4 */


					/** Revisi 1.5: Ganti perhitungan periode berdasarkan tanggal scan bacto / milko */
					/** Kalau scandate kosong gunakan transaksi_waktunya */

					/** Revisi 1.6: Dihapus karena sudah di define di atas */

					// if(empty($val['qcparameter_scandate'])){
					// 	$val['qcparameter_scandate'] = $val['transaksi_waktu'];
					// }

					// foreach($arrayPeriode as $prk=>$prv){
					// 	if($prv['periodedetail_startdate'] <= (int) date('j',strtotime($val['qcparameter_scandate'])) && $prv['periodedetail_enddate'] >= (int) date('j',strtotime($val['qcparameter_scandate']))){
					// 		$val['transaksi_periode'] = $prv['periodedetail_name'];
					// 	}

					// }
					/** End Revisi 1.6 */
					/** END Revisi 1.5 */

					foreach($dataFormula['records'][0]['milkpricecondition'] as $k=>$v){
						if($v['milkpricecondition_standardvalue'] != $val['qcparameter_'.$v['milkpricecondition_element']]){
							$beda++;

							if($v['milkpricecondition_korelasi'] == 'p'){
								if($val['qcparameter_'.$v['milkpricecondition_element']] > $v['milkpricecondition_standardvalue']){
									$pembagi = (float) ((float) $val['qcparameter_'.$v['milkpricecondition_element']] - (float) $v['milkpricecondition_standardvalue']) / (float) $v['milkpricecondition_interval'];
									$selisih[$v['milkpricecondition_element']] = ($pembagi * $v['milkpricecondition_addprice']);
								}else{
									$pembagi =  (float) ((float) $v['milkpricecondition_standardvalue'] - (float) $val['qcparameter_'.$v['milkpricecondition_element']])  / (float) $v['milkpricecondition_interval'];
									$selisih[$v['milkpricecondition_element']] = -($pembagi * $v['milkpricecondition_addprice']);
								}
							}else{
								if($val['qcparameter_'.$v['milkpricecondition_element']] > $v['milkpricecondition_standardvalue']){
									$pembagi = (float) ((float) $val['qcparameter_'.$v['milkpricecondition_element']] - (float) $v['milkpricecondition_standardvalue']) / (float) $v['milkpricecondition_interval'];
									$selisih[$v['milkpricecondition_element']] = -($pembagi * $v['milkpricecondition_addprice']);
								}else{
									$pembagi =  (float) ((float) $v['milkpricecondition_standardvalue'] - (float) $val['qcparameter_'.$v['milkpricecondition_element']])  / (float) $v['milkpricecondition_interval'];
									$selisih[$v['milkpricecondition_element']] = ($pembagi * $v['milkpricecondition_addprice']);
								}
							}


						}else{
							$selisih[$v['milkpricecondition_element']] = 0;
						}
					}
					if($beda === 0){
						//return number_format($hargadasar,2);
					}else{
						foreach($selisih as $k=>$v){
							$hargadasar = (float) ($hargadasar + $v);
						}
					}


					$currentRezGrade = 0;
					$currentRezValue = ($val['qcparameter_cfu'] * 1000); // Dikali 1000 karena output dari bactoscan dikali 1000

					foreach($dataRezgrade['records'][0]['rezgradedetail'] as $rk=>$rv){
						if($rv['rezgradedetail_tpcstart'] <= $currentRezValue && $rv['rezgradedetail_tpcend'] >= $currentRezValue){
							$currentRezGrade = $rv['rezgradedetail_grade'];
							$hargarez = $rv['rezgradedetail_price'];
						}
					}

					$currentFpGrade = 0;
					$currentFpValue = $val['qcparameter_fp'];

					foreach($dataFpgrade['records'][0]['fpgradedetail'] as $fk=>$fv){
						if($fv['fpgradedetail_fpstart'] <= $currentFpValue && $fv['fpgradedetail_fpend'] >= $currentFpValue){
							$currentFpGrade = $fv['fpgradedetail_grade'];
							$hargafp = $fv['fpgradedetail_price'];
						}
					}


					$dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_cfu']     = $val['qcparameter_cfu'];
					$dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_ibc']     = $val['qcparameter_ibc'];
					$dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_fat']     = $val['qcparameter_fat'];
					$dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_protein'] = $val['qcparameter_protein'];
					$dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_lactose'] = $val['qcparameter_lactose'];
					$dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_ts']      = $val['qcparameter_ts'];
					$dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_fp']      = $val['qcparameter_fp'];
					$dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_acidity'] = $val['qcparameter_acidity'];
					$dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_urea']    = $val['qcparameter_urea'];
					$dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_snf']     = $val['qcparameter_snf'];
					$dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['hargadasar']          = $hargadasar;
					$dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['hargafp']             = $hargafp;
					$dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['hargarez']            = $hargarez;
				}	
			}

		}

        /**
         * Added by Ziki
         */
		$dataCow = $cow->setCondition('cow_anggota = \''.$_GET['anggota'].'\' ')
						->setLimit(1)
						->read();
		foreach ($dataCow['records'] as $key => $value) {
			$data['cow_lactating'] = $value['cow_lactating'];
			$data['cow_population'] = $value['cow_population'];
			$data['cow_antibiotic'] = $value['cow_antibiotic'];
			$data['cow_antibioticstartdate'] = $value['cow_antibioticstartdate'];
			$data['cow_antibioticenddate'] = $value['cow_antibioticenddate'];
			$data['cow_antibioticremark'] = $value['cow_antibioticremark'];
		}
		
        load::library('db');
        session::set('title','Dashboard Farmer');
        $_GET['year'] = !isset($_GET['year']) ? date('Y') : $_GET['year'];
        $_GET['page'] = !isset($_GET['page']) ? 1 : $_GET['page'];
        /* Farmer REPORT */
        $penerimaan_susu = load::model('transaksi');
        $penerimaan_susu->setCondition('transaksi_status = \'y\' AND transaksi_anggota = \''.$_GET['anggota'].'\'');

        $penerimaan_susu->setChartType('spline');
        $penerimaan_susu->setChartYear(date('Y'));
        $penerimaan_susu->setChartSubtitle('Tahun: '.$_GET['year']);
        $penerimaan_susu->setChartHeight('400px');
        $penerimaan_susu->setChartPeriode('monthRange');
        $penerimaan_susu->setChartPeriodeField('transaksi_waktu');
        $penerimaan_susu->setChartFields(array(
            'transaksi_id'=>array(
                'operation'=>'count',
                'title'=>_('Transaction')
            ),
            'transaksi_totalberat'=>array(
                'operation'=>'sum',
                'title'=>_('Weight (Kg)')
            )
        ));
        $penerimaan_susu->setChartContainer('widget_transaksi_chart');
        $data['farmer_chart'] = $penerimaan_susu->generateChart();

        
        $res             = $dataAnggota;
        $res['records']  = $dataNya;
        $data['data']    = $res;
        $data['periode'] = $dtPeriode;
        View::display('dashboard/dashboard.farmer',$data);
        // 
		// print_r($dataCow['records']['0']);
    }

    function logout(){
        $username = session::get('user_name');
        session::destroy();
        fn::log($username.' Logged out from System');
        header('location:'.BASE_URL);
        exit();
    }
    function killprocess(){
        if(stristr(PHP_OS, 'WIN')) {
            shell_exec('TASKKILL /F /IM wcpp.exe');
        } else {
            shell_exec('pkill -f wcpp');
        }

    }
    function firstuser_confirm(){
        $user = load::model('user');
        $user->setId(session::get('user_id'));
        $user->setData('user_firstuse','n');
        $user->update();
        session::set('user_firstuse','n');
    }

    function errorReporting(){
        $msg = 'PHP VERSION:'.phpversion();
        $msg .= "\nSERVER SOFTWARE: ".$_SERVER['SERVER_SOFTWARE'];
        $msg .= "\nMEMORY USAGE BY APPS: ".fn::formatBytes(memory_get_usage(),2);
        $msg .= "\nAPPS VERSION: ".Config::$application['app_version'];
        $option['to'] = 'agung@transformatika.com';
        $option['subject'] = 'Error Reporting MCP - '.Config::$application['company_name'];

        $option['message'] = "This is Error Reporting From : ".Config::$application['company_name']."\n".$msg;
        $zipname = 'error-'.date('YmdHis').'.zip';
        $this->zipFile('logs/',$zipname);
        $option['attachment'] = $zipname;
        $sendmail = fn::sendMail($option);
        if($sendmail){
            $error = _('Thank you for your attention. an email was sent to Transformatika Team');
        }else{
            $error = _('Sorry. System Cannot sent your message to Transformatika Team');
        }
        unlink($zipname);
        if(isset($_GET['json'])){
            echo $error;
        }else{
            session::set('errorMsg',$error);
            if(isset($_SERVER['HTTP_REFERER'])){
                header('location:'.$_SERVER['HTTP_REFERER']);
            }else{
                header('location:'.BASE_URL);
            }
        }
    }

    function zipFile($source, $destination, $flag = ''){
        if (!extension_loaded('zip') || !file_exists($source)) {
            return false;
        }

        $zip = new ZipArchive();
        if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
            return false;
        }

        $source = str_replace('\\', '/', realpath($source));
        if($flag){
            $flag = basename($source) . '/';
            //$zip->addEmptyDir(basename($source) . '/');
        }

        if (is_dir($source) === true){
            $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);
            foreach ($files as $file){
                $file = str_replace('\\', '/', realpath($file));

                if (is_dir($file) === true)
                {
                    $zip->addEmptyDir(str_replace($source . '/', '', $flag.$file . '/'));
                }
                else if (is_file($file) === true)
                {
                    $zip->addFromString(str_replace($source . '/', '', $flag.$file), file_get_contents($file));
                }
            }
        }
        else if (is_file($source) === true){
            $zip->addFromString($flag.basename($source), file_get_contents($source));
        }

        return $zip->close();
    }

	function printReport(){
		load::library('db');
		$sql = 'SELECT SUM(transaksi_totalberat) AS jml where transaksi_status = \'y\' AND DATE(transaksi_waktu) = \''.date('Y-m-d').'\'';
		$cek = db::query($sql);
		if(db::numRows > 0){
			$res = db::fetchAssoc($cek);
			$total = (float) $res['jml'];
		}else{
			$total = 0;
		}
		
	}
}
