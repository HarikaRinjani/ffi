<?php

class Qcparameter_Controller extends Controller
{
    public $description = "v1.7 Add more filter\n\nv1.6\n\nv1.5\n\nv1.4\n - Add Sampling schedule menu \n\nv1.3\n - Fix Edit Quality data\n\nv.1.1: Enable Manual add quality data";
    public $version = 1.7;
    public $buildConfiguration = array(
        'table' => 'qcparameter',
        'views' => array(
            'qcparameter.index',
            'qcparameter.menu',
            'qcparameter.log',
            'qcparameter.getdata',
            ),
        'models' => 'qcparameter',
        );
    public function __construct()
    {
        security::checkSession();
    }
    public function getData($export = '')
    {
        session::set('title', '##Quality Data');
        $samplingschedule = load::model('qcparameter');
        $addSQL = '';
        if (isset($_GET['koperasi']) && !empty($_GET['koperasi'])) {
            $addSQL .= ' AND qcparameter_koperasi = \''.$_GET['koperasi'].'\'';
        }
        if (isset($_GET['tpk']) && !empty($_GET['tpk'])) {
            $addSQL .= ' AND qcparameter_tpk = \''.$_GET['tpk'].'\'';
        }
        if (isset($_GET['date1']) && isset($_GET['date2'])) {
            $addSQL .= ' AND qcparameter_scandate BETWEEN \''.$_GET['date1'].'\' AND \''.$_GET['date2'].'\'';
        }

        $data['data'] = $samplingschedule->setCondition('qcparameter_status = \'y\''.$addSQL)
                                 ->setLimit(0)
                                 ->setOrderBy('t_transaksi.transaksi_nomor')
                                 ->read();
        if ($export == 'export') {
            $header = array('FARMER ID', 'FARMER NAME', 'SCAN DATE', 'CFU', 'FAT', 'PROTEIN', 'LACTOSE', 'TS', 'FP', 'ACIDITY', 'UREA');
            $filename = 'MCP-QualityData.xls';

            header("Content-Disposition: attachment; filename=\"$filename\"");
            header('Content-Type: application/vnd.ms-excel');
            echo implode("\t", $header)."\n";
            $exportData = array();
            $tempData = array();
            foreach ($data['data']['records'] as $k => $v) {
                if (!isset($tempData[$v['transaksi_nomor']])) {
                    $tempData[$v['transaksi_nomor']] = $v;
                } else {
                    if ((float) $tempData[$v['transaksi_nomor']]['qcparameter_cfu'] > 0) {
                        $tempData[$v['transaksi_nomor']]['qcparameter_fat'] = (float) $v['qcparameter_fat']."\t";
                        $tempData[$v['transaksi_nomor']]['qcparameter_protein'] = (float) $v['qcparameter_protein']."\t";
                        $tempData[$v['transaksi_nomor']]['qcparameter_lactose'] = (float) $v['qcparameter_lactose']."\t";
                        $tempData[$v['transaksi_nomor']]['qcparameter_ts'] = (float) $v['qcparameter_ts']."\t";
                        $tempData[$v['transaksi_nomor']]['qcparameter_fp'] = (float) $v['qcparameter_fp']."\t";
                        $tempData[$v['transaksi_nomor']]['qcparameter_acidity'] = (float) $v['qcparameter_acidity']."\t";
                        $tempData[$v['transaksi_nomor']]['qcparameter_urea'] = (float) $v['qcparameter_urea'];
                    } else {
                        $tempData[$v['transaksi_nomor']]['qcparameter_cfu'] = (float) $v['qcparameter_cfu'];
                    }
                }
            }
            foreach ($tempData as $k => $v) {
                $strData = $v['transaksi_nomor']."\t";
                $strData .= $v['transaksi_nama']."\t";
                $strData .= $v['qcparameter_scandate']."\t";
                $strData .= (float) $v['qcparameter_cfu']."\t";
                $strData .= (float) $v['qcparameter_fat']."\t";
                $strData .= (float) $v['qcparameter_protein']."\t";
                $strData .= (float) $v['qcparameter_lactose']."\t";
                $strData .= (float) $v['qcparameter_ts']."\t";
                $strData .= (float) $v['qcparameter_fp']."\t";
                $strData .= (float) $v['qcparameter_acidity']."\t";
                $strData .= (float) $v['qcparameter_urea'];
                $exportData[] = $strData;
            }
            echo implode("\n", $exportData);
            exit();
        } else {
            View::display('qcparameter/qcparameter.getdata', $data);
        }
    }
    public function index()
    {
        session::set('title', 'Quality Data');
        $data['enableEdit'] = 'n';
        if (session::get('user_group') == 'administrator' || strpos(session::get('user_role'), 'qcparameter-save') !== false) {
            $data['enableEdit'] = 'y';
        }
        $data['enableAdd'] = 'n';
        if (session::get('user_group') == 'administrator' || strpos(session::get('user_role'), 'qcparameter-addnew') !== false) {
            $data['enableAdd'] = 'y';
        }
        $dt = load::model('qcparameter');
        $kelompok = load::model('kelompok');
        $_GET['page'] = !isset($_GET['page']) ? 1 : $_GET['page'];
        $_GET['date'] = !isset($_GET['date']) ? date('d/m/Y') : $_GET['date'];
        $date = date('Y-m-d', fn::dateToUnix($_GET['date']));

        $koperasi = load::model('koperasi');
        $tpk = load::model('tpk');

        if (in_array(session::get('user_group'), Config::$application['super_group'])) {
            $koperasi->setCondition('koperasi_status = \'y\'');
            $_GET['koperasi'] = !isset($_GET['koperasi']) ? '' : $_GET['koperasi'];
            $tpkcondition = '';
        } else {
            $koperasi->setCondition('koperasi_status = \'y\' AND koperasi_id = \''.session::get('user_koperasi').'\'');
            $_GET['koperasi'] = session::get('user_koperasi');
            $tpkcondition = ' AND tpk_koperasi = \''.session::get('user_koperasi').'\'';
        }

        /*
        *   Data Koperasi
        */
        $koperasi->setOrderBy('koperasi_nama');
        $koperasi->setLimit(0);
        $dtCoop = $koperasi->read();
        $data['koperasi'] = $dtCoop['records'];

        $addSQL = '';
        if (isset($_GET['koperasi']) && !empty($_GET['koperasi'])) {
            $addSQL .= ' AND qcparameter_koperasi = \''.$_GET['koperasi'].'\'';
            $tpk->setCondition('tpk_koperasi = \''.$_GET['koperasi'].'\' AND tpk_status = \'y\'');
        } else {
            $tpk->setCondition('tpk_status = \'y\' '.$tpkcondition);
        }

        $tpk->setLimit(0);
        $dtMC = $tpk->read();
        $data['tpk'] = $dtMC['records'];

        if (isset($_GET['tpk']) && !empty($_GET['tpk'])) {
            $addSQL .= ' AND qcparameter_tpk = \''.$_GET['tpk'].'\'';
        }
        if (isset($_GET['date1']) && isset($_GET['date2'])) {
            $addSQL .= ' AND qcparameter_scandate BETWEEN \''.$_GET['date1'].'\' AND \''.$_GET['date2'].'\'';
        }
        $dt->setCondition('qcparameter_status = \'y\' AND DATE(qcparameter_scandate) = \''.$date.'\''.$addSQL);
        $dt->setLimit(Config::$application['display_limit']);
        $dt->setPage($_GET['page']);
        if (isset($_GET['keyword'])) {
            $dt->setKeyword($_GET['keyword']);
            $dt->setSearchIn('qcparameter_transaksi');
        }
        $dt->setOrderBy('qcparameter_scandate');
        $dt->setOrderType('DESC');
        $datanya = $dt->read();
        $arraydata = array();
        foreach ($datanya['records'] as $k => $v) {
            $arraydata[$k] = $v;
            $cekkelompok = $kelompok->setId($v['transaksi_kelompok'])
                                    ->read();
            $arraydata[$k]['kelompok_nama'] = $cekkelompok['records']['kelompok_nama'];
        }
        $data['data'] = $datanya;
        $data['data']['records'] = $arraydata;
        $data['tanggal'] = $_GET['date'];

        View::display('qcparameter/qcparameter.index', $data);
    }
    public function save()
    {
        $dt = load::model('qcparameter');
        $log = load::model('csvlog');
        if (isset($_POST['qcparameter_id']) && !empty($_POST['qcparameter_id'])) {
            $id = $_POST['qcparameter_id'];
            $newid = fn::generate_id(true);
            $dt->setId($id);
            $cek = $dt->read();

            $postdata = $cek['records'];
            unset($_POST['qcparameter_id']);
            foreach ($_POST as $k => $v) {
                $postdata[$k] = (float) $v;
            }
            $postdata['qcparameter_snf'] = (float) ($postdata['qcparameter_ts'] - $postdata['qcparameter_fat']);
            $postdata['qcparameter_id'] = $newid;
            $postdata['qcparameter_editby'] = session::get('user_id');
            $postdata['qcparameter_lastmodified'] = date('Y-m-d H:i:s');
            $dt->setDataArray($postdata);
            if ($dt->insert()) {
                $dt->resetValue();
                $dt->setId($id);
                $dt->setData('qcparameter_status', 'n');
                if ($dt->update()) {
                    session::set('errorMsg', _('Data has been successfully saved'));
                } else {
                    $dt->setId($newid);
                    $dt->delete();
                    session::set('errorMsg', _('Failed while saving data into database'));
                }
            } else {
                $logdata['csvlog_content'] = session::get('user_name').' ('.session::get('user_id').') Editing Quality Parameter Data #ID '.$postdata['qcparameter_transaksi'].' ('.$id.')';
                $logdata['csvlog_id'] = fn::generate_id();
                $logdata['csvlog_date'] = date('Y-m-d H:i:s');
                $log->setDataArray($logdata);
                $log->insert();
                session::set('errorMsg', _('Failed while saving data into database'));
            }
        }
    }
    public function log()
    {
        View::display('qcparameter/qcparameter.log');
    }
    public function refreshLog()
    {
        if ($_GET['date'] === '') {
            $_GET['date'] = date('Y-m-d H:i:s', strtotime('-2 hours'));
        }
        $dt = load::model('csvlog');
        $dt->setCondition('csvlog_date > \''.$_GET['date'].'\'');
        $dt->setOrderBy('csvlog_date');
        $dt->setOrderType('ASC');
        $dt->setLimit(1);
        $data = $dt->read();
        if ($data['total_data'] > 0) {
            $res['content'] = '['.$data['records'][0]['csvlog_date'].'] - '.$data['records'][0]['csvlog_content']."\n";
            $res['time'] = $data['records'][0]['csvlog_date'];
        } else {
            $res['content'] = '';
            $res['time'] = '';
        }
        echo json_encode($res);
    }
    public function addnew()
    {
        if (isset($_POST['qcparameter_transaksi'])) {
            $dt = load::model('qcparameter');
            $newid = fn::generate_id(true);
            $dt->setCondition('qcparameter_transaksi = \''.$_POST['qcparameter_transaksi'].'\' AND qcparameter_status = \'y\'');
            $cek = $dt->read();
            if ($cek['total_data'] > 0) {
                $postdata = $cek['records'];
                //unset($_POST['qcparameter_id']);
                // foreach($_POST as $k=>$v){
                // 	if(empty($v)){
                // 		$v = 0.00;
                // 	}
                // 	$postdata[$k] = (float) $v;
                // }
                $postdata['qcparameter_cfu'] = !empty($_POST['qcparameter_cfu']) ? (float) $_POST['qcparameter_cfu'] : 0.00;
                $postdata['qcparameter_ibc'] = !empty($_POST['qcparameter_ibc']) ? (float) $_POST['qcparameter_ibc'] : 0.00;
                $postdata['qcparameter_fat'] = !empty($_POST['qcparameter_fat']) ? (float) $_POST['qcparameter_fat'] : 0.00;
                $postdata['qcparameter_protein'] = !empty($_POST['qcparameter_protein']) ? (float) $_POST['qcparameter_protein'] : 0.00;
                $postdata['qcparameter_lactose'] = !empty($_POST['qcparameter_lactose']) ? (float) $_POST['qcparameter_lactose'] : 0.00;
                $postdata['qcparameter_ts'] = !empty($_POST['qcparameter_ts']) ? (float) $_POST['qcparameter_ts'] : 0.00;
                $postdata['qcparameter_fp'] = !empty($_POST['qcparameter_fp']) ? (float) $_POST['qcparameter_fp'] : 0.00;
                $postdata['qcparameter_acidity'] = !empty($_POST['qcparameter_acidity']) ? (float) $_POST['qcparameter_acidity'] : 0.00;
                $postdata['qcparameter_snf'] = (float) ($postdata['qcparameter_ts'] - $postdata['qcparameter_fat']);
                $postdata['qcparameter_id'] = $newid;
                $postdata['qcparameter_editby'] = session::get('user_id');
                $postdata['qcparameter_lastmodified'] = date('Y-m-d H:i:s');
                $postdata['qcparameter_status'] = 'y';
                $postdata['qcparameter_scandate'] = $cek['records'][0]['qcparameter_scandate'];
            } else {
                $transaksi = load::model('transaksi');
                $transaksi->setId($_POST['qcparameter_transaksi']);
                $dtTransaksi = $transaksi->read();
                //print_r($_POST);exit();
                $postdata = $_POST;
                $postdata['qcparameter_id'] = $newid;
                $postdata['qcparameter_scandate'] = date('Y-m-d', fn::dateToUnix($_POST['qcparameter_scandate']));
                $postdata['qcparaemter_status'] = 'y';
                $postdata['qcparameter_koperasi'] = $dtTransaksi['records']['transaksi_koperasi'];
                $postdata['qcparameter_tpk'] = $dtTransaksi['records']['transaksi_tpk'];
                $postdata['qcparameter_kelompok'] = $dtTransaksi['records']['transaksi_kelompok'];
                $postdata['qcparameter_anggota'] = $dtTransaksi['records']['transaksi_anggota'];
                $postdata['qcparameter_snf'] = (float) ($postdata['qcparameter_ts'] - $postdata['qcparameter_fat']);
            }

            $dt->setDataArray($postdata);
            if ($dt->insert()) {
                if ($cek['total_data'] > 0) {
                    $dt->resetValue();
                    $dt->setId($cek['records'][0]['qcparameter_id']);
                    $dt->setData('qcparameter_status', 'n');
                    if ($dt->update()) {
                        session::set('errorMsg', _('Data has been successfully saved'));
                    } else {
                        $dt->setId($newid);
                        $dt->delete();
                        session::set('errorMsg', _('Failed while saving data into database'));
                    }
                } else {
                    session::set('errorMsg', _('Data has been successfully saved'));
                }
            } else {
                session::set('errorMsg', _('Failed while saving data into database'));
            }
            header('location:'.BASE_URL.'index.php?/qcparameter/index');
            exit();
        }
    }
}
