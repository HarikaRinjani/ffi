<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 22/01/2015
 * Time: 8:49
 */
class Synchandler_Controller extends Controller{
    public $version = 1.5;
    public $description = "v 1.3\nChange Logs:\n- Fix Synchronize Farmers Data\n- Add More table to Sync";
    public $buildConfiguration = array(

    );
    function __construct(){

    }
    function index(){
        echo 'tes curl';
    }
    function process(){
        //echo json_encode($_POST);exit();
		//print_r($_FILES);exit();
        load::library('db');
		load::library('fn');
        if(!isset($_FILES['file']['name']) || empty($_FILES['file']['name'])){
            die('error');
        }
        $file = Config::$application['file_path'].DS.'tmp'.DS.$_FILES['file']['name'];
        move_uploaded_file($_FILES['file']['tmp_name'], $file);
        $filejson = file_get_contents($file);
        $jsonarray = json_decode($filejson,true);
        $table = security::decrypt($jsonarray['table']);
        $data = security::decrypt($jsonarray['data']);
        $dataArray = json_decode($data,true);
        $error = 0;
		@unlink($file);
		//print_r($dataArray);exit();
        //echo json_encode($dataArray);exit();
		//echo $table;exit();
        if($table == 't_transaksi' || $table == 't_transaksidetail' || $table = 't_samplingschedule'){
            //db::beginTransaction();
            array_filter($dataArray);
            foreach($dataArray as $key=>$val){
                if($table == 't_transaksi'){
					$querycek = db::query('SELECT transaksi_id FROM t_transaksi WHERE transaksi_id = \''.$val['transaksi_id'].'\'');
                    $cek = (int) db::numRows($querycek);
                    if($cek > 0){
                        if($val['transaksi_sync'] == 'n'){
                            $val['transaksi_sync'] = 'y';
                            $query = db::update($table,$val,'WHERE transaksi_id = \''.$val['transaksi_id'].'\'');
							if(!empty(db::dbError())){
								$error++;
								$res['msg'] = db::dbError();
							}else{
								/** LOG DATA SCRIPT HERE **/
								
								/** END LOG DATA SCRIPT **/
							}
                        }
                    }else{
						$val['transaksi_sync'] = 'y';
                        $query = db::insert($table,$val);
						if(!empty(db::dbError())){
							$error++;
							$res['msg'] = db::dbError();
						}else{
							/** LOG DATA SCRIPT HERE **/
							
							/** END LOG DATA SCRIPT **/
						}
                    }
                }
                if($table == 't_transaksidetail'){
                    //echo json_encode($dataArray);exit();
					$querycek = db::query('SELECT transaksidetail_id FROM t_transaksidetail WHERE transaksidetail_id = \''.$val['transaksidetail_id'].'\'');
                    $cek = (int) db::numRows($querycek);
                    if($cek > 0){
                        if($val['transaksidetail_sync'] == 'n'){
                            $val['transaksidetail_sync'] = 'y';
                            $query = db::update($table,$val,'WHERE transaksidetail_id = \''.$val['transaksidetail_id'].'\'');
							if(!empty(db::dbError())){
								$error++;
								$res['msg'] = db::dbError();
							}
                        }
                    }else{
						$val['transaksidetail_sync'] = 'y';
                        $query = db::insert($table,$val);
						if(!empty(db::dbError())){
							$error++;
							$res['msg'] = db::dbError();
						}
                    }
                    if(!$query){
                        $error++;
                        $res['msg'] = db::dbError();
                    }else{
                        $res['msg'] = 'Sync success';
                    }
                }
				if($table == 't_samplingschedule'){ 
					$query = db::update($table,$val,'WHERE samplingschedule_id = \''.$val['samplingschedule_id'].'\'');
					if(!empty(db::dbError())){
						$error++;
						$res['msg'] = db::dbError();
					}else{
						$res['msg'] = 'Sync success';
					}
				}
            }
           
        }else{
            if($table == 't_user'){
                $dataUser = db::recordList('t_user');
                $res['msg'] = $dataUser;
            }
            if($table == 't_koperasi'){
                $dataUser = db::recordList('t_koperasi');
                $res['msg'] = $dataUser;
            }
            if($table == 't_tpk'){
                $dataUser = db::recordList('t_tpk');
                $res['msg'] = $dataUser;
            }
            if($table == 't_truck'){
                $dataUser = db::recordList('t_truck');
                $res['msg'] = $dataUser;
            }
        }
        if($error > 0){
            $res['status'] = 'error';
        }else{
            $res['status'] = 'success';
        }
        echo json_encode($res);
        exit();
    }
    function updatedb(){
        load::library('db');
        if(isset($_POST['onlysampling'])){
            $synctables = array(
                'samplingschedule'
                );
        }else{
            $synctables = array(
                'koperasi',
                'user',
                'anggota',
                'kelompok',
                'tpk',
                'usertype',
                'inbox',
                'truck',
                'cow',
                'periode',
                'konsumen',
                'card',
                'periode',
                'periodedetail',
                'samplingschedule'
                );
        }
        
        $_TABLE = array();
        foreach($synctables as $tbl){
            include path('app').DS."config".DS."conf.d".DS."table".DS."table.".$tbl.".php";
        }
        $data = array();
        foreach($_TABLE as $k=>$v){
            $data[$k] = db::recordList($k);
        }
        $jsondata = json_encode($data);
        $encryptdata = security::encrypt($jsondata);
        $uuid = security::decrypt($_POST['uuid']);
        //print_r($_POST);exit();
        if(!file_exists($uuid.'.txt')){
            touch($uuid.'.txt');
        }
        file_put_contents($uuid.'.txt', $encryptdata);
        $zipname = $uuid.'.zip';
        $zip = new ZipArchive();
        if (!$zip->open($zipname, ZIPARCHIVE::CREATE)) {
            header($_SERVER["SERVER_PROTOCOL"]." 500 Internal Server Error");
            header("Status: 500 Internal Server Error");
            $_SERVER['REDIRECT_STATUS'] = 500;
            die('error');
        }else{
            $zip->addFile($uuid.'.txt');
        }
        $zip->close();
        fn::read_file($uuid.'.zip');
        unlink($uuid.'.zip');
        unlink($uuid.'.txt');
    }

    function checkpackage(){
        $tmp_user_id = session::get('user_id');
        $tmp_user_group = session::get('user_group');

        session::set('user_id',security::decrypt($_POST['user_id']));
        session::set('user_group','administrator');
        //echo json_encode($_POST);
        $package_dir = path('app').DS.'controllers';
        if($_POST['package'] == 'synchandler'){
            $name = $this;
        }else{
            require_once $package_dir.DS.$_POST['package'].'.php';
            $controller = ucfirst($_POST['package']) . '_Controller';
            $name = new $controller;
        }

        $data['package'] = $_POST['package'];
        $data['version'] = $name->version;
        $data['publisher'] = $name->publisher;
        $data['description'] = $name->description;
        $data['dependencies'] = $name->dependencies;
        $data['type'] = 'package';
        session::set('user_id',$tmp_user_id);
        session::set('user_group',$tmp_user_group);
        echo json_encode($data);
    }
    function createDir($dir=''){
        $oldumask = umask(0);
        mkdir($dir,0777);
        umask($oldumask);
    }
    function getpackage($package=''){
        if(empty($package)){
            die('EMPTY PACKAGE');
        }
        if(file_exists(path('app').DS.'controllers'.DS.$package.'.php')){
            $tmp_user_id = session::get('user_id');
            $tmp_user_group = session::get('user_group');

            session::set('user_id',security::decrypt($_POST['user_id']));
            session::set('user_group','administrator');

            require_once path('app').DS.'controllers'.DS.$package.'.php';
            $controller = ucfirst($package) . '_Controller';
            $module = new $controller;
            $configuration = $module->buildConfiguration;
            session::set('user_id',$tmp_user_id);
            session::set('user_group',$tmp_user_group);
            $temp_dir = Config::$application['file_path'].'tmp'.DS.uniqid();
            $this->createDir($temp_dir);

            $zip = new ZipArchive();
            $zip->open( $temp_dir.DS.$package.'.zip', ZipArchive::CREATE );

            $zip->addEmptyDir('application');
            $zip->addEmptyDir('application'.DS.'controllers');
            $zip->addEmptyDir('application'.DS.'config');
            $zip->addEmptyDir('application'.DS.'config'.DS.'conf.d');
            $zip->addEmptyDir('application'.DS.'models');
            $zip->addEmptyDir('application'.DS.'views');
            $zip->addFile(path('app').DS.'controllers'.DS.$package.'.php','application'.DS.'controllers'.DS.$package.'.php');
            if(array_key_exists('config', $configuration)){
                if(is_array($configuration['config'])){
                    foreach($configuration['config'] as $v){
                        $zip->addFile(path('app').DS.'config'.DS.'conf.d'.DS.$v.'.php','application'.DS.'config'.DS.'conf.d'.DS.$v.'.php');
                    }

                }else{
                    $zip->addFile(path('app').DS.'config'.DS.'conf.d'.DS.$configuration['config'].'.php','application'.DS.'config'.DS.'conf.d'.DS.$configuration['config'].'.php');
                }
            }
            if(array_key_exists('table',$configuration)){
                $zip->addEmptyDir('application'.DS.'config'.DS.'conf.d'.DS.'table');
                if(is_array($configuration['table'])){
                    foreach($configuration['table'] as $v){
                        $zip->addFile(path('app').DS.'config'.DS.'conf.d'.DS.'table'.DS.'table.'.$v.'.php','application'.DS.'config'.DS.'conf.d'.DS.'table'.DS.'table.'.$v.'.php');
                    }

                }else{
                    $zip->addFile(path('app').DS.'config'.DS.'conf.d'.DS.'table'.DS.'table.'.$configuration['table'].'.php','application'.DS.'config'.DS.'conf.d'.DS.'table'.DS.'table.'.$configuration['table'].'.php');
                }

            }
            if(array_key_exists('models',$configuration)){
                if(is_array($configuration['models'])){
                    foreach($configuration['models'] as $v){
                        $zip->addFile(path('app').DS.'models'.DS.$v.'.php','application'.DS.'models'.DS.$v.'.php');
                    }
                }else{
                    $zip->addFile(path('app').DS.'models'.DS.$package.'.php','application'.DS.'models'.DS.$configuration['models'].'.php');
                }
            }
            if(array_key_exists('views',$configuration)){
                $zip->addEmptyDir('application'.DS.'views'.DS.'default');
                $zip->addEmptyDir('application'.DS.'views'.DS.'default'.DS.$package);
                if(is_array($configuration['views'])){
                    foreach($configuration['views'] as $v){
                        $zip->addFile(path('app').DS.'views'.DS.'default'.DS.$package.DS.$v.'.php','application'.DS.'views'.DS.'default'.DS.$package.DS.$v.'.php');
                    }
                }else{
                    $zip->addFile(path('app').DS.'views'.DS.'default'.DS.$package.DS.$configuration['views'].'.php','application'.DS.'views'.DS.'default'.DS.$package.DS.$configuration['views'].'.php');
                }
            }


            $zip->close();
            load::library('fn');
            //fn::removeDirectory($temp_dir.DS.'application');

            header( "Pragma: public" );
            header( "Expires: 0" );
            header( "Cache-Control: must-revalidate, post-check=0, pre-check=0" );
            header( "Cache-Control: public" );
            header( "Content-Description: File Transfer" );
            header( "Content-type: application/zip" );
            header( "Content-Disposition: attachment; filename=\"" . $package . ".zip\"" );
            header( "Content-Transfer-Encoding: binary" );
            header( "Content-Length: " . filesize( $temp_dir.DS.$package.'.zip' ) );

            readfile( $temp_dir.DS.$package.'.zip' );
            fn::removeDirectory($temp_dir);
            exit();
        }else{
            die('Package file not found!');
        }
    }
    function tes(){
        print_r($_FILES);
    }
}
