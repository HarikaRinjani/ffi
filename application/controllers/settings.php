<?php

/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 12/22/2014
 * Time: 3:12 PM
 */
class Settings_Controller extends Controller {
    public $buildConfiguration = array(
        'views'=>array(
    		'settings.index',
    		'settings.menu'
            ),
        'config'=>'usergroup'
        );
    public $description = "v1.4\n- Fix Cow Population Menu \n\nv.1.3\n- Rename Periode menu -> Sampling Periode \n\nv1.2\n- Add Cow Population Menu \n\nv1.1:\n- Add Card Management in Menu";
	public $version = 1.4;
    function __construct() {
        security::checkSession();
    }


    function index() {
        session::set('title', 'Settings');
        load::library('db');
        $data['TPK']            = db::count('t_tpk', 'WHERE tpk_status != \'d\'');
        $data['Kelompok']       = db::count('t_kelompok', 'WHERE kelompok_status != \'d\'');
        //$data['Kelompok Harga'] = db::count('t_kelompokharga', 'WHERE kelompokharga_status != \'d\'');
        $data['Anggota']        = db::count('t_anggota', 'WHERE anggota_status != \'d\'');
        $data['Truck']          = db::count('t_truck', 'WHERE truck_status != \'d\'');
        $data['Konsumen']       = db::count('t_konsumen', 'WHERE konsumen_status != \'d\'');
        $notice                 = array();
        foreach ($data as $k => $v) {
            if ($v == 0) {
                $notice[] = $k . ''._('Module not configured properly');
            }
        }
        //print_r($data);exit();
        $res['notice'] = $notice;
        View::display('settings/settings.index', $res);
    }

    function client(){
        View::display('settings/settings.client');
    }
}
