<?php
/**
 * Created by PhpStorm.
 * User: PrastowoaGung
 * Date: 12/30/2014
 * Time: 9:53 PM.
 */
class Report_Controller extends Controller
{
    public $buildConfiguration = array(
        'views' => array(
            'report.datalog',
            'report.menu',
            'report.index',
            'report.qcparameter',
            'report.transaction',
            'report.transaction.koperasi',
            'report.transaction.tpk',
            ),
        );
    public $version = 1.7;
    public $description = "v1.7 Add more filter \nv1.6\n Fix Milk Collection Report Paging\n\nv1.5\nFix export Milk Collection Report\n\nv1.4\n- Add Filter by Farmer (Milk Collection Report)\n\nv1.3\n- Change Export File Template\n\nv1.2\n- Add New Milk Price Report";
    public function __construct()
    {
        security::checkSession();
    }

    public function index()
    {
        load::library('db');
        session::set('title', 'Report Overview');
        $_GET['year'] = !isset($_GET['year']) ? date('Y') : $_GET['year'];
        $_GET['page'] = !isset($_GET['page']) ? 1 : $_GET['page'];
        /* transaksi REPORT */
        $transaksi = load::model('transaksi');
        $transaksi->setCondition('transaksi_status = \'y\'');

        $transaksi->setChartType('spline');
        $transaksi->setChartYear(date('Y'));
        $transaksi->setChartTitle('Transaction Report');
        $transaksi->setChartSubtitle('Year: '.$_GET['year']);
        $transaksi->setChartHeight('300px');
        $transaksi->setChartPeriode('monthRange');
        $transaksi->setChartPeriodeField('transaksi_waktu');
        $transaksi->setChartFields(array(
            'transaksi_id' => array(
                'operation' => 'count',
                'title' => _('Transaction'),
            ),
            'transaksi_totalberat' => array(
                'operation' => 'sum',
                'title' => _('Weight (Kg)'),
            ),
        ));
        $transaksi->setChartContainer('widget_transaksi_chart');
        $data['transaksi_chart'] = $transaksi->generateChart();

        $data['total_tpk'] = db::count('t_tpk', 'WHERE tpk_status != \'d\'');
        $data['total_kelompok'] = db::count('t_kelompok', 'WHERE kelompok_status != \'d\'');
        $data['total_kelompokharga'] = db::count('t_kelompokharga', 'WHERE kelompokharga_status != \'d\'');
        $data['total_anggota'] = db::count('t_anggota', 'WHERE anggota_status != \'d\'');
        $data['total_truck'] = db::count('t_truck', 'WHERE truck_status != \'d\'');
        $data['total_konsumen'] = db::count('t_konsumen', 'WHERE konsumen_status != \'d\'');

        $transaksi->setCondition('transaksi_status = \'y\' AND date_part(\'year\',transaksi_waktu) = \''.$_GET['year'].'\'');
        $transaksi->setLimit(Config::$application['display_limit']);
        $transaksi->setOrderBy('transaksi_waktu');
        $transaksi->setOrderType('DESC');
        $transaksi->setPage($_GET['page']);
        $data['transaksi'] = $transaksi->read();
        View::display('report/report.index', $data);
    }
    public function transaction()
    {
        session::set('title', 'Milk Collection Report');

        $_GET['page'] = !isset($_GET['page']) ? 1 : $_GET['page'];
        $_GET['periode'] = !isset($_GET['periode']) ? 'daterange' : $_GET['periode'];

        $_GET['tpk'] = !isset($_GET['tpk']) ? '' : $_GET['tpk'];
        $_GET['year'] = !isset($_GET['year']) ? date('Y') : $_GET['year'];
        $_GET['sesi'] = !isset($_GET['sesi']) ? '' : $_GET['sesi'];
        $_GET['anggota'] = !isset($_GET['anggota']) ? '' : $_GET['anggota'];

        $data['tpk'] = array();
        $data['koperasi'] = array();
        $prepopulate = array();
        $koperasi = load::model('koperasi');
        $tpk = load::model('tpk');
        $transaksi = load::model('transaksi');

        if (in_array(session::get('user_group'), Config::$application['super_group'])) {
            $koperasi->setCondition('koperasi_status = \'y\'');
            $_GET['koperasi'] = !isset($_GET['koperasi']) ? '' : $_GET['koperasi'];
            $tpkcondition = '';
        } else {
            $koperasi->setCondition('koperasi_status = \'y\' AND koperasi_id = \''.session::get('user_koperasi').'\'');
            $_GET['koperasi'] = session::get('user_koperasi');
            $tpkcondition = ' AND tpk_koperasi = \''.session::get('user_koperasi').'\'';
        }

        /*
        *   Data Koperasi
        */
        $koperasi->setOrderBy('koperasi_nama');
        $koperasi->setLimit(0);
        $dtCoop = $koperasi->read();
        $data['koperasi'] = $dtCoop['records'];

        if (isset($_GET['koperasi']) && !empty($_GET['koperasi'])) {
            $tpk->setCondition('tpk_koperasi = \''.$_GET['koperasi'].'\' AND tpk_status = \'y\'');
        } else {
            $tpk->setCondition('tpk_status = \'y\' '.$tpkcondition);
        }
        $arraySesi = array(
            'p' => 'Morning',
            's' => 'Afternoon',
            );
        if (!empty($_GET['sesi'])) {
            $addSqlSesi = ' AND transaksi_sesi = \''.$_GET['sesi'].'\' ';
            $addTitleSesi = ' - '.$arraySesi[$_GET['sesi']];
        } else {
            $addSqlSesi = '';
            $addTitleSesi = '';
        }
		
        /*
        *   Data TPK
        */
        $tpk->setLimit(0);
        $dtMC = $tpk->read();
        $data['tpk'] = $dtMC['records'];
        if (isset($_GET['anggota']) && !empty($_GET['anggota'])) {
            $anggota = load::model('anggota');
            $dtAnggota = $anggota->setId($_GET['anggota'])->read();
            $prepopulate[] = array(
                'id' => $_GET['anggota'],
                'name' => $dtAnggota['records']['anggota_nama'],
                );
            $fields['transaksi_id'] = array(
                'operation' => 'count',
                'title' => 'Total Transaction',
            );
            $fieldsberat['transaksi_totalberat'] = array(
                'operation' => 'sum',
                'title' => 'Weight',
            );

            $transaksi->setCondition('transaksi_status = \'y\' AND transaksi_anggota = \''.$_GET['anggota'].'\''.$addSqlSesi);

            $transaksi->setChartType('column');
            $transaksi->setChartYear(date('Y'));

            if ($_GET['periode'] == 'daterange') {
                $_GET['date1'] = !isset($_GET['date1']) ? date('d/m/Y', strtotime('-1 week')) : $_GET['date1'];
                $_GET['date2'] = !isset($_GET['date2']) ? date('d/m/Y') : $_GET['date2'];
                if (fn::dateToUnix($_GET['date1']) > fn::dateToUnix($_GET['date2'])) {
                    $_GET['date1'] = date('d/m/Y', strtotime('-1 week', fn::dateToUnix($_GET['date2'])));
                }
                $date1 = date('Y-m-d', fn::dateToUnix($_GET['date1']));
                $date2 = date('Y-m-d', fn::dateToUnix($_GET['date2']));
                $transaksi->setChartSubtitle('Date: '.$_GET['date1'].' - '.$_GET['date2']);
                $transaksi->setChartPeriode('dateRange');

                /* Condition buat list data */
                $cond = 'DATE(transaksi_waktu)';
            } elseif ($_GET['periode'] == 'monthly') {
                $_GET['date1'] = !isset($_GET['date1']) ? 1 : $_GET['date1'];
                $_GET['date2'] = !isset($_GET['date2']) ? 12 : $_GET['date2'];

                if ((int) $_GET['date2'] < (int) $_GET['date1']) {
                    $_GET['date2'] = $_GET['date1'];
                }
                $date1 = $_GET['date1'];
                $date2 = $_GET['date2'];
                $transaksi->setChartSubtitle(fn::namaBulan($_GET['date1']).' - '.fn::namaBulan($_GET['date2']).' '.$_GET['year']);
                $transaksi->setChartYear($_GET['year']);
                $transaksi->setChartPeriode('monthRange');
                $cond = 'date_part(\'month\', transaksi_waktu)';
            } else {
                $_GET['date1'] = !isset($_GET['date1']) ? date('Y', strtotime('-1 year')) : $_GET['date1'];
                $_GET['date2'] = !isset($_GET['date2']) ? date('Y') : $_GET['date2'];
                if ($_GET['date1'] > $_GET['date2']) {
                    $_GET['date1'] = $_GET['date2'];
                }
                $date1 = $_GET['date1'];
                $date2 = $_GET['date2'];
                $transaksi->setChartSubtitle('Year: '.$_GET['date1'].' - '.$_GET['date2']);
                $transaksi->setChartPeriode('yearRange');
                $cond = 'date_part(\'year\', transaksi_waktu)';
            }

            $transaksi->setChartHeight('300px');
            $transaksi->setChartDateRange(array(
                $date1,
                $date2,
            ));

            /* 1st Chart */

            $transaksi->setChartTitle(_('Transaction').$addTitleSesi);
            $transaksi->setChartSubtitle($dtAnggota['records']['anggota_nama']);
            $transaksi->setChartPeriodeField('transaksi_waktu');
            $transaksi->setChartFields($fields);
            $transaksi->setChartContainer('widget_transaksi_chart1');
            $data['transaksi_chart1'] = $transaksi->generateChart();

            /* 2nd Chart */
            $transaksi->setChartTitle(_('Weight (Kg)'));
            $transaksi->setChartSubtitle($dtAnggota['records']['anggota_nama']);
            $transaksi->setChartFields($fieldsberat);
            $transaksi->setChartContainer('widget_transaksi_chart2');
            $data['transaksi_chart2'] = $transaksi->generateChart();

            $transaksi->setCondition('transaksi_status = \'y\' AND transaksi_anggota = \''.$_GET['anggota'].'\' AND '.$cond.' BETWEEN \''.$date1.'\' AND \''.$date2.'\''.$addSqlSesi);
            $transaksi->setLimit(Config::$application['display_limit']);
            $transaksi->setOrderBy('transaksi_waktu');
            $transaksi->setOrderType('DESC');
            $transaksi->setPage($_GET['page']);
            $data['periode'] = $_GET['periode'];
            $data['transaksi'] = $transaksi->read();
        } else {
            if (!empty($_GET['tpk'])) {
                $fields = array();
                $fieldsberat = array();
                $namatpk = '';
                foreach ($dtMC['records'] as $k => $v) {
                    if ($v['tpk_id'] == $_GET['tpk']) {
                        $namatpk = $v['tpk_nama'];
                        $fields['transaksi_id'.$k] = array(
                            'field' => 'transaksi_id',
                            'operation' => 'count',
                            'alias' => 'tpk_'.$k,
                            'title' => $v['tpk_nama'],
                            'condition' => 'transaksi_tpk = \''.$v['tpk_id'].'\'',
                        );
                        $fieldsberat['transaksi_totalberat'.$k] = array(
                            'field' => 'transaksi_totalberat',
                            'operation' => 'sum',
                            'alias' => 'totaltpk'.$k,
                            'title' => $v['tpk_nama'],
                            'condition' => 'transaksi_tpk = \''.$v['tpk_id'].'\'',
                        );
                    }
                }

                $transaksi->setCondition('transaksi_status = \'y\''.$addSqlSesi);
                $transaksi->setChartType('column');
                $transaksi->setChartYear(date('Y'));

                if ($_GET['periode'] == 'daterange') {
                    $_GET['date1'] = !isset($_GET['date1']) ? date('d/m/Y', strtotime('-1 week')) : $_GET['date1'];
                    $_GET['date2'] = !isset($_GET['date2']) ? date('d/m/Y') : $_GET['date2'];
                    if (fn::dateToUnix($_GET['date1']) > fn::dateToUnix($_GET['date2'])) {
                        $_GET['date1'] = date('d/m/Y', strtotime('-1 week', fn::dateToUnix($_GET['date2'])));
                    }
                    $date1 = date('Y-m-d', fn::dateToUnix($_GET['date1']));
                    $date2 = date('Y-m-d', fn::dateToUnix($_GET['date2']));
                    $transaksi->setChartSubtitle('Date: '.$_GET['date1'].' - '.$_GET['date2']);
                    $transaksi->setChartPeriode('dateRange');

                    /* Condition buat list data */
                    $cond = 'DATE(transaksi_waktu)';
                } elseif ($_GET['periode'] == 'monthly') {
                    $_GET['date1'] = !isset($_GET['date1']) ? 1 : $_GET['date1'];
                    $_GET['date2'] = !isset($_GET['date2']) ? 12 : $_GET['date2'];

                    if ((int) $_GET['date2'] < (int) $_GET['date1']) {
                        $_GET['date2'] = $_GET['date1'];
                    }
                    $date1 = $_GET['date1'];
                    $date2 = $_GET['date2'];
                    $transaksi->setChartSubtitle(fn::namaBulan($_GET['date1']).' - '.fn::namaBulan($_GET['date2']).' '.$_GET['year']);
                    $transaksi->setChartYear($_GET['year']);
                    $transaksi->setChartPeriode('monthRange');
                    $cond = 'date_part(\'month\', transaksi_waktu)';
                } else {
                    $_GET['date1'] = !isset($_GET['date1']) ? date('Y', strtotime('-1 year')) : $_GET['date1'];
                    $_GET['date2'] = !isset($_GET['date2']) ? date('Y') : $_GET['date2'];
                    if ($_GET['date1'] > $_GET['date2']) {
                        $_GET['date1'] = $_GET['date2'];
                    }
                    $date1 = $_GET['date1'];
                    $date2 = $_GET['date2'];
                    $transaksi->setChartSubtitle('Year: '.$_GET['date1'].' - '.$_GET['date2']);
                    $transaksi->setChartPeriode('yearRange');
                    $cond = 'date_part(\'year\', transaksi_waktu)';
                }

                $transaksi->setChartHeight('300px');
                $transaksi->setChartDateRange(array(
                    $date1,
                    $date2,
                ));

                /* 1st Chart */
                $transaksi->setChartTitle(_('Transaction').' [ TPK: '.$namatpk.' ]'.$addTitleSesi);
                $transaksi->setChartPeriodeField('transaksi_waktu');
                $transaksi->setChartFields($fields);
                $transaksi->setChartContainer('widget_transaksi_chart1');
                $data['transaksi_chart1'] = $transaksi->generateChart();

                /* 2nd Chart */
                $transaksi->setChartTitle(_('Weight (Kg)'));
                $transaksi->setChartFields($fieldsberat);
                $transaksi->setChartContainer('widget_transaksi_chart2');
                $data['transaksi_chart2'] = $transaksi->generateChart();

                $transaksi->setCondition('transaksi_status = \'y\' AND transaksi_tpk = \''.$_GET['tpk'].'\' AND '.$cond.' BETWEEN \''.$date1.'\' AND \''.$date2.'\''.$addSqlSesi);
                $transaksi->setLimit(Config::$application['display_limit']);
                $transaksi->setOrderBy('transaksi_waktu');
                $transaksi->setOrderType('DESC');
                $transaksi->setPage($_GET['page']);
                $data['periode'] = $_GET['periode'];
                $data['transaksi'] = $transaksi->read();
            } else {
                if (!empty($_GET['koperasi'])) {
                    $fields = array();
                    $fieldsberat = array();
                    $namakoperasi = '';

                    foreach ($dtMC['records'] as $k => $v) {
                        $namakoperasi = $v['koperasi_nama'];
                        $fields['transaksi_id'.$k] = array(
                            'field' => 'transaksi_id',
                            'operation' => 'count',
                            'alias' => 'tpk_'.$k,
                            'title' => $v['tpk_nama'],
                            'condition' => 'transaksi_tpk = \''.$v['tpk_id'].'\'',
                        );
                        $fieldsberat['transaksi_totalberat'.$k] = array(
                            'field' => 'transaksi_totalberat',
                            'operation' => 'sum',
                            'alias' => 'totaltpk'.$k,
                            'title' => $v['tpk_nama'],
                            'condition' => 'transaksi_tpk = \''.$v['tpk_id'].'\'',
                        );
                    }

                    $transaksi->setCondition('transaksi_status = \'y\' AND transaksi_koperasi = \''.$_GET['koperasi'].'\''.$addSqlSesi);
                    $transaksi->setChartType('column');
                    $transaksi->setChartYear(date('Y'));

                    if ($_GET['periode'] == 'daterange') {
                        $_GET['date1'] = !isset($_GET['date1']) ? date('d/m/Y', strtotime('-1 week')) : $_GET['date1'];
                        $_GET['date2'] = !isset($_GET['date2']) ? date('d/m/Y') : $_GET['date2'];
                        if (fn::dateToUnix($_GET['date1']) > fn::dateToUnix($_GET['date2'])) {
                            $_GET['date1'] = date('d/m/Y', strtotime('-1 week', fn::dateToUnix($_GET['date2'])));
                        }
                        $date1 = date('Y-m-d', fn::dateToUnix($_GET['date1']));
                        $date2 = date('Y-m-d', fn::dateToUnix($_GET['date2']));
                        $transaksi->setChartSubtitle('Date: '.$_GET['date1'].' - '.$_GET['date2']);
                        $transaksi->setChartPeriode('dateRange');

                        /* Condition buat list data */
                        $cond = 'DATE(transaksi_waktu)';
                    } elseif ($_GET['periode'] == 'monthly') {
                        $_GET['date1'] = !isset($_GET['date1']) ? 1 : $_GET['date1'];
                        $_GET['date2'] = !isset($_GET['date2']) ? 12 : $_GET['date2'];

                        if ((int) $_GET['date2'] < (int) $_GET['date1']) {
                            $_GET['date2'] = $_GET['date1'];
                        }
                        $date1 = $_GET['date1'];
                        $date2 = $_GET['date2'];
                        $transaksi->setChartSubtitle(fn::namaBulan($_GET['date1']).' - '.fn::namaBulan($_GET['date2']).' '.$_GET['year']);
                        $transaksi->setChartYear($_GET['year']);
                        $transaksi->setChartPeriode('monthRange');
                        $cond = 'date_part(\'month\', transaksi_waktu)';
                    } else {
                        $_GET['date1'] = !isset($_GET['date1']) ? date('Y', strtotime('-1 year')) : $_GET['date1'];
                        $_GET['date2'] = !isset($_GET['date2']) ? date('Y') : $_GET['date2'];
                        if ($_GET['date1'] > $_GET['date2']) {
                            $_GET['date1'] = $_GET['date2'];
                        }
                        $date1 = $_GET['date1'];
                        $date2 = $_GET['date2'];
                        $transaksi->setChartSubtitle('Year: '.$_GET['date1'].' - '.$_GET['date2']);
                        $transaksi->setChartPeriode('yearRange');
                        $cond = 'date_part(\'year\', transaksi_waktu)';
                    }

                    $transaksi->setChartHeight('300px');
                    $transaksi->setChartDateRange(array(
                        $date1,
                        $date2,
                    ));
                    /* 1st Chart */
                    $transaksi->setChartTitle(_('Transaction').' [ '.$namakoperasi.' ]'.$addTitleSesi);
                    $transaksi->setChartPeriodeField('transaksi_waktu');
                    $transaksi->setChartFields($fields);
                    $transaksi->setChartContainer('widget_transaksi_chart1');
                    $data['transaksi_chart1'] = $transaksi->generateChart();

                    /* 2nd Chart */
                    $transaksi->setChartTitle(_('Weight (Kg)'));
                    $transaksi->setChartFields($fieldsberat);
                    $transaksi->setChartContainer('widget_transaksi_chart2');
                    $data['transaksi_chart2'] = $transaksi->generateChart();

                    $transaksi->setCondition('transaksi_status = \'y\' AND transaksi_koperasi = \''.$_GET['koperasi'].'\' AND '.$cond.' BETWEEN \''.$date1.'\' AND \''.$date2.'\''.$addSqlSesi);
                    $transaksi->setLimit(Config::$application['display_limit']);
                    $transaksi->setOrderBy('transaksi_waktu');
                    $transaksi->setOrderType('DESC');
                    $transaksi->setPage($_GET['page']);
                    $data['periode'] = $_GET['periode'];
                    $data['transaksi'] = $transaksi->read();
                } else {
                    $fields = array();
                    $fieldsberat = array();
                    foreach ($dtCoop['records'] as $k => $v) {
                        $fields['transaksi_id'.$k] = array(
                            'field' => 'transaksi_id',
                            'operation' => 'count',
                            'alias' => 'koperasi_'.$k,
                            'title' => $v['koperasi_nama'],
                            'condition' => 'transaksi_koperasi = \''.$v['koperasi_id'].'\'',
                        );
                        $fieldsberat['transaksi_totalberat'.$k] = array(
                            'field' => 'transaksi_totalberat',
                            'operation' => 'sum',
                            'alias' => 'totalkoperasi'.$k,
                            'title' => $v['koperasi_nama'],
                            'condition' => 'transaksi_koperasi = \''.$v['koperasi_id'].'\'',
                        );
                    }

                    /* transaksi REPORT */
                    $transaksi->setCondition('transaksi_status = \'y\''.$addSqlSesi);
                    $transaksi->setChartType('column');
                    $transaksi->setChartYear(date('Y'));

                    if ($_GET['periode'] == 'daterange') {
                        $_GET['date1'] = !isset($_GET['date1']) ? date('d/m/Y', strtotime('-1 week')) : $_GET['date1'];
                        $_GET['date2'] = !isset($_GET['date2']) ? date('d/m/Y') : $_GET['date2'];
                        if (fn::dateToUnix($_GET['date1']) > fn::dateToUnix($_GET['date2'])) {
                            $_GET['date1'] = date('d/m/Y', strtotime('-1 week', fn::dateToUnix($_GET['date2'])));
                        }
                        $date1 = date('Y-m-d', fn::dateToUnix($_GET['date1']));
                        $date2 = date('Y-m-d', fn::dateToUnix($_GET['date2']));
                        $transaksi->setChartSubtitle('Date: '.$_GET['date1'].' - '.$_GET['date2']);
                        $transaksi->setChartPeriode('dateRange');

                        /* Condition buat list data */
                        $cond = 'DATE(transaksi_waktu)';
                    } elseif ($_GET['periode'] == 'monthly') {
                        $_GET['date1'] = !isset($_GET['date1']) ? 1 : $_GET['date1'];
                        $_GET['date2'] = !isset($_GET['date2']) ? 12 : $_GET['date2'];

                        if ((int) $_GET['date2'] < (int) $_GET['date1']) {
                            $_GET['date2'] = $_GET['date1'];
                        }
                        $date1 = $_GET['date1'];
                        $date2 = $_GET['date2'];
                        $transaksi->setChartSubtitle(fn::namaBulan($_GET['date1']).' - '.fn::namaBulan($_GET['date2']).' '.$_GET['year']);
                        $transaksi->setChartYear($_GET['year']);
                        $transaksi->setChartPeriode('monthRange');
                        $cond = 'date_part(\'month\', transaksi_waktu)';
                    } else {
                        $_GET['date1'] = !isset($_GET['date1']) ? date('Y', strtotime('-1 year')) : $_GET['date1'];
                        $_GET['date2'] = !isset($_GET['date2']) ? date('Y') : $_GET['date2'];
                        if ($_GET['date1'] > $_GET['date2']) {
                            $_GET['date1'] = $_GET['date2'];
                        }
                        $date1 = $_GET['date1'];
                        $date2 = $_GET['date2'];
                        $transaksi->setChartSubtitle('Year: '.$_GET['date1'].' - '.$_GET['date2']);
                        $transaksi->setChartPeriode('yearRange');
                        $cond = 'date_part(\'year\', transaksi_waktu)';
                    }

                    $transaksi->setChartHeight('300px');
                    $transaksi->setChartDateRange(array(
                        $date1,
                        $date2,
                    ));
                    /* 1st Chart */
                    $transaksi->setChartTitle(_('Transaction').$addTitleSesi);
                    $transaksi->setChartPeriodeField('transaksi_waktu');
                    $transaksi->setChartFields($fields);
                    $transaksi->setChartContainer('widget_transaksi_chart1');
                    $data['transaksi_chart1'] = $transaksi->generateChart();

                    /* 2nd Chart */
                    $transaksi->setChartTitle(_('Weight (Kg)'));
                    $transaksi->setChartFields($fieldsberat);
                    $transaksi->setChartContainer('widget_transaksi_chart2');
                    $data['transaksi_chart2'] = $transaksi->generateChart();

                    $transaksi->setCondition('transaksi_status = \'y\' AND '.$cond.' BETWEEN \''.$date1.'\' AND \''.$date2.'\''.$addSqlSesi);
                    $transaksi->setLimit(Config::$application['display_limit']);
                    $transaksi->setOrderBy('transaksi_waktu');
                    $transaksi->setOrderType('DESC');
                    $transaksi->setPage($_GET['page']);
                    $data['periode'] = $_GET['periode'];
                    $data['transaksi'] = $transaksi->read();
                }
            }
        }
        $data['prepopulate'] = $prepopulate;
        View::display('report/report.transaction', $data);
    }

    public function farmer()
    {
        load::library('db');
        session::set('title', 'Report Overview');
        $anggota = load::model('anggota');
        $_GET['year'] = !isset($_GET['year']) ? date('Y') : $_GET['year'];
        $_GET['page'] = !isset($_GET['page']) ? 1 : $_GET['page'];
        $getanggota = $anggota->setCondition('anggota_nomor = \''.session::get('user_name').'\' AND anggota_status = \'y\'')->setLimit(1)->read();
        $_GET['anggota'] = $getanggota['records'][0]['anggota_id'];
        /* transaksi REPORT */
        $transaksi = load::model('transaksi');
        $transaksi->setCondition('transaksi_status = \'y\' AND transaksi_anggota = \''.$_GET['anggota'].'\'');

        $transaksi->setChartType('spline');
        $transaksi->setChartYear(date('Y'));
        $transaksi->setChartTitle('Laporan Transaksi');
        $transaksi->setChartSubtitle('Tahun: '.$_GET['year']);
        $transaksi->setChartHeight('300px');
        $transaksi->setChartPeriode('monthRange');
        $transaksi->setChartPeriodeField('transaksi_waktu');
        $transaksi->setChartFields(array(
            'transaksi_id' => array(
                'operation' => 'count',
                'title' => _('Transaksi'),
            ),
            'transaksi_totalberat' => array(
                'operation' => 'sum',
                'title' => _('Berat (Kg)'),
            ),
        ));
        $transaksi->setChartContainer('widget_transaksi_chart');
        $data['transaksi_chart'] = $transaksi->generateChart();

        $data['total_tpk'] = db::count('t_tpk', 'WHERE tpk_status != \'d\'');
        $data['total_kelompok'] = db::count('t_kelompok', 'WHERE kelompok_status != \'d\'');
        $data['total_kelompokharga'] = db::count('t_kelompokharga', 'WHERE kelompokharga_status != \'d\'');
        $data['total_anggota'] = db::count('t_anggota', 'WHERE anggota_status != \'d\'');
        $data['total_truck'] = db::count('t_truck', 'WHERE truck_status != \'d\'');
        $data['total_konsumen'] = db::count('t_konsumen', 'WHERE konsumen_status != \'d\'');

        $transaksi->setCondition('transaksi_status = \'y\'AND anggota_id = \''.$_GET['anggota'].'\' AND date_part(\'year\',transaksi_waktu) = \''.$_GET['year'].'\'');
        $transaksi->setLimit(Config::$application['display_limit']);
        $transaksi->setOrderBy('transaksi_waktu');
        $transaksi->setOrderType('DESC');
        $transaksi->setPage($_GET['page']);
        $data['transaksi'] = $transaksi->read();
        View::display('report/report.farmer', $data);
    }
    public function transactionFarmer()
    {
        session::set('title', 'Milk Collection Report Farmer');
        $anggota = load::model('anggota');

        $_GET['page'] = !isset($_GET['page']) ? 1 : $_GET['page'];
        $_GET['periode'] = !isset($_GET['periode']) ? 'daterange' : $_GET['periode'];
        $_GET['month'] = !isset($_GET['month']) ? date('n') : $_GET['month'];
        $_GET['tpk'] = !isset($_GET['tpk']) ? '' : $_GET['tpk'];
        $_GET['year'] = !isset($_GET['year']) ? date('Y') : $_GET['year'];
        $_GET['sesi'] = !isset($_GET['sesi']) ? '' : $_GET['sesi'];
        $getanggota = $anggota->setCondition('anggota_nomor = \''.session::get('user_name').'\' AND anggota_status = \'y\'')->setLimit(1)->read();
        $_GET['anggota'] = $getanggota['records'][0]['anggota_id'];

        $data['tpk'] = array();
        $data['koperasi'] = array();
        $prepopulate = array();
        $koperasi = load::model('koperasi');
        $tpk = load::model('tpk');
        $transaksi = load::model('transaksi');

        if (in_array(session::get('user_group'), Config::$application['super_group'])) {
            $koperasi->setCondition('koperasi_status = \'y\'');
            $_GET['koperasi'] = !isset($_GET['koperasi']) ? '' : $_GET['koperasi'];
            $tpkcondition = '';
        } else {
            $koperasi->setCondition('koperasi_status = \'y\' AND koperasi_id = \''.session::get('user_koperasi').'\'');
            $_GET['koperasi'] = session::get('user_koperasi');
            $tpkcondition = ' AND tpk_koperasi = \''.session::get('user_koperasi').'\'';
        }

        /*
        *   Data Koperasi
        */
        $koperasi->setOrderBy('koperasi_nama');
        $koperasi->setLimit(0);
        $dtCoop = $koperasi->read();
        $data['koperasi'] = $dtCoop['records'];

        if (isset($_GET['koperasi']) && !empty($_GET['koperasi'])) {
            $tpk->setCondition('tpk_koperasi = \''.$_GET['koperasi'].'\' AND tpk_status = \'y\'');
        } else {
            $tpk->setCondition('tpk_status = \'y\' '.$tpkcondition);
        }
        $arraySesi = array(
            'p' => 'Morning',
            's' => 'Afternoon',
            );
        if (!empty($_GET['sesi'])) {
            $addSqlSesi = ' AND transaksi_sesi = \''.$_GET['sesi'].'\' ';
            $addTitleSesi = ' - '.$arraySesi[$_GET['sesi']];
        } else {
            $addSqlSesi = '';
            $addTitleSesi = '';
        }
        /*
        *   Data TPK
        */
        $tpk->setLimit(0);
        $dtMC = $tpk->read();
        $data['tpk'] = $dtMC['records'];
        if (isset($_GET['anggota']) && !empty($_GET['anggota'])) {
            $anggota = load::model('anggota');
            $dtAnggota = $anggota->setId($_GET['anggota'])->read();
            $prepopulate[] = array(
                'id' => $_GET['anggota'],
                'name' => $dtAnggota['records']['anggota_nama'],
                );
            $fields['transaksi_id'] = array(
                'operation' => 'count',
                'title' => 'Total Transaction',
            );
            $fieldsberat['transaksi_totalberat'] = array(
                'operation' => 'sum',
                'title' => 'Weight',
            );

            $transaksi->setCondition('transaksi_status = \'y\' AND transaksi_anggota = \''.$_GET['anggota'].'\''.$addSqlSesi);

            $transaksi->setChartType('column');
            $transaksi->setChartYear(date('Y'));

            if ($_GET['periode'] == 'daterange') {
                $_GET['date1'] = !isset($_GET['date1']) ? date('d/m/Y', strtotime('-1 week')) : $_GET['date1'];
                $_GET['date2'] = !isset($_GET['date2']) ? date('d/m/Y') : $_GET['date2'];
                if (fn::dateToUnix($_GET['date1']) > fn::dateToUnix($_GET['date2'])) {
                    $_GET['date1'] = date('d/m/Y', strtotime('-1 week', fn::dateToUnix($_GET['date2'])));
                }
                $date1 = date('Y-m-d', fn::dateToUnix($_GET['date1']));
                $date2 = date('Y-m-d', fn::dateToUnix($_GET['date2']));
                $transaksi->setChartSubtitle('Date: '.$_GET['date1'].' - '.$_GET['date2']);
                $transaksi->setChartPeriode('dateRange');

                /* Condition buat list data */
                $cond = 'DATE(transaksi_waktu)';
            } elseif ($_GET['periode'] == 'monthly') {
                $_GET['date1'] = !isset($_GET['date1']) ? 1 : $_GET['date1'];
                $_GET['date2'] = !isset($_GET['date2']) ? 12 : $_GET['date2'];

                if ((int) $_GET['date2'] < (int) $_GET['date1']) {
                    $_GET['date2'] = $_GET['date1'];
                }
                $date1 = $_GET['date1'];
                $date2 = $_GET['date2'];
                $transaksi->setChartSubtitle(fn::namaBulan($_GET['date1']).' - '.fn::namaBulan($_GET['date2']).' '.$_GET['year']);
                $transaksi->setChartYear($_GET['year']);
                $transaksi->setChartPeriode('monthRange');
                $cond = 'date_part(\'month\', transaksi_waktu)';
            } else {
                $_GET['date1'] = !isset($_GET['date1']) ? date('Y', strtotime('-1 year')) : $_GET['date1'];
                $_GET['date2'] = !isset($_GET['date2']) ? date('Y') : $_GET['date2'];
                if ($_GET['date1'] > $_GET['date2']) {
                    $_GET['date1'] = $_GET['date2'];
                }
                $date1 = $_GET['date1'];
                $date2 = $_GET['date2'];
                $transaksi->setChartSubtitle('Year: '.$_GET['date1'].' - '.$_GET['date2']);
                $transaksi->setChartPeriode('yearRange');
                $cond = 'date_part(\'year\', transaksi_waktu)';
            }

            $transaksi->setChartHeight('300px');
            $transaksi->setChartDateRange(array(
                $date1,
                $date2,
            ));

            /* 1st Chart */

            $transaksi->setChartTitle(_('Transaction').$addTitleSesi);
            $transaksi->setChartSubtitle($dtAnggota['records']['anggota_nama']);
            $transaksi->setChartPeriodeField('transaksi_waktu');
            $transaksi->setChartFields($fields);
            $transaksi->setChartContainer('widget_transaksi_chart1');
            $data['transaksi_chart1'] = $transaksi->generateChart();

            /* 2nd Chart */
            $transaksi->setChartTitle(_('Weight (Kg)'));
            $transaksi->setChartSubtitle($dtAnggota['records']['anggota_nama']);
            $transaksi->setChartFields($fieldsberat);
            $transaksi->setChartContainer('widget_transaksi_chart2');
            $data['transaksi_chart2'] = $transaksi->generateChart();

            $transaksi->setCondition('transaksi_status = \'y\' AND transaksi_anggota = \''.$_GET['anggota'].'\' AND '.$cond.' BETWEEN \''.$date1.'\' AND \''.$date2.'\''.$addSqlSesi);
            $transaksi->setLimit(Config::$application['display_limit']);
            $transaksi->setOrderBy('transaksi_waktu');
            $transaksi->setOrderType('DESC');
            $transaksi->setPage($_GET['page']);
            $data['periode'] = $_GET['periode'];
            $data['transaksi'] = $transaksi->read();
        } else {
            if (!empty($_GET['tpk'])) {
                $fields = array();
                $fieldsberat = array();
                $namatpk = '';
                foreach ($dtMC['records'] as $k => $v) {
                    if ($v['tpk_id'] == $_GET['tpk']) {
                        $namatpk = $v['tpk_nama'];
                        $fields['transaksi_id'.$k] = array(
                            'field' => 'transaksi_id',
                            'operation' => 'count',
                            'alias' => 'tpk_'.$k,
                            'title' => $v['tpk_nama'],
                            'condition' => 'transaksi_tpk = \''.$v['tpk_id'].'\'',
                        );
                        $fieldsberat['transaksi_totalberat'.$k] = array(
                            'field' => 'transaksi_totalberat',
                            'operation' => 'sum',
                            'alias' => 'totaltpk'.$k,
                            'title' => $v['tpk_nama'],
                            'condition' => 'transaksi_tpk = \''.$v['tpk_id'].'\'',
                        );
                    }
                }

                $transaksi->setCondition('transaksi_status = \'y\''.$addSqlSesi);
                $transaksi->setChartType('column');
                $transaksi->setChartYear(date('Y'));

                if ($_GET['periode'] == 'daterange') {
                    $_GET['date1'] = !isset($_GET['date1']) ? date('d/m/Y', strtotime('-1 week')) : $_GET['date1'];
                    $_GET['date2'] = !isset($_GET['date2']) ? date('d/m/Y') : $_GET['date2'];
                    if (fn::dateToUnix($_GET['date1']) > fn::dateToUnix($_GET['date2'])) {
                        $_GET['date1'] = date('d/m/Y', strtotime('-1 week', fn::dateToUnix($_GET['date2'])));
                    }
                    $date1 = date('Y-m-d', fn::dateToUnix($_GET['date1']));
                    $date2 = date('Y-m-d', fn::dateToUnix($_GET['date2']));
                    $transaksi->setChartSubtitle('Date: '.$_GET['date1'].' - '.$_GET['date2']);
                    $transaksi->setChartPeriode('dateRange');

                    /* Condition buat list data */
                    $cond = 'DATE(transaksi_waktu)';
                } elseif ($_GET['periode'] == 'monthly') {
                    $_GET['date1'] = !isset($_GET['date1']) ? 1 : $_GET['date1'];
                    $_GET['date2'] = !isset($_GET['date2']) ? 12 : $_GET['date2'];

                    if ((int) $_GET['date2'] < (int) $_GET['date1']) {
                        $_GET['date2'] = $_GET['date1'];
                    }
                    $date1 = $_GET['date1'];
                    $date2 = $_GET['date2'];
                    $transaksi->setChartSubtitle(fn::namaBulan($_GET['date1']).' - '.fn::namaBulan($_GET['date2']).' '.$_GET['year']);
                    $transaksi->setChartYear($_GET['year']);
                    $transaksi->setChartPeriode('monthRange');
                    $cond = 'date_part(\'month\', transaksi_waktu)';
                } else {
                    $_GET['date1'] = !isset($_GET['date1']) ? date('Y', strtotime('-1 year')) : $_GET['date1'];
                    $_GET['date2'] = !isset($_GET['date2']) ? date('Y') : $_GET['date2'];
                    if ($_GET['date1'] > $_GET['date2']) {
                        $_GET['date1'] = $_GET['date2'];
                    }
                    $date1 = $_GET['date1'];
                    $date2 = $_GET['date2'];
                    $transaksi->setChartSubtitle('Year: '.$_GET['date1'].' - '.$_GET['date2']);
                    $transaksi->setChartPeriode('yearRange');
                    $cond = 'date_part(\'year\', transaksi_waktu)';
                }

                $transaksi->setChartHeight('300px');
                $transaksi->setChartDateRange(array(
                    $date1,
                    $date2,
                ));

                /* 1st Chart */
                $transaksi->setChartTitle(_('Transaction').' [ TPK: '.$namatpk.' ]'.$addTitleSesi);
                $transaksi->setChartPeriodeField('transaksi_waktu');
                $transaksi->setChartFields($fields);
                $transaksi->setChartContainer('widget_transaksi_chart1');
                $data['transaksi_chart1'] = $transaksi->generateChart();

                /* 2nd Chart */
                $transaksi->setChartTitle(_('Weight (Kg)'));
                $transaksi->setChartFields($fieldsberat);
                $transaksi->setChartContainer('widget_transaksi_chart2');
                $data['transaksi_chart2'] = $transaksi->generateChart();

                $transaksi->setCondition('transaksi_status = \'y\' AND transaksi_tpk = \''.$_GET['tpk'].'\' AND '.$cond.' BETWEEN \''.$date1.'\' AND \''.$date2.'\''.$addSqlSesi);
                $transaksi->setLimit(Config::$application['display_limit']);
                $transaksi->setOrderBy('transaksi_waktu');
                $transaksi->setOrderType('DESC');
                $transaksi->setPage($_GET['page']);
                $data['periode'] = $_GET['periode'];
                $data['transaksi'] = $transaksi->read();
            } else {
                if (!empty($_GET['koperasi'])) {
                    $fields = array();
                    $fieldsberat = array();
                    $namakoperasi = '';

                    foreach ($dtMC['records'] as $k => $v) {
                        $namakoperasi = $v['koperasi_nama'];
                        $fields['transaksi_id'.$k] = array(
                            'field' => 'transaksi_id',
                            'operation' => 'count',
                            'alias' => 'tpk_'.$k,
                            'title' => $v['tpk_nama'],
                            'condition' => 'transaksi_tpk = \''.$v['tpk_id'].'\'',
                        );
                        $fieldsberat['transaksi_totalberat'.$k] = array(
                            'field' => 'transaksi_totalberat',
                            'operation' => 'sum',
                            'alias' => 'totaltpk'.$k,
                            'title' => $v['tpk_nama'],
                            'condition' => 'transaksi_tpk = \''.$v['tpk_id'].'\'',
                        );
                    }

                    $transaksi->setCondition('transaksi_status = \'y\' AND transaksi_koperasi = \''.$_GET['koperasi'].'\''.$addSqlSesi);
                    $transaksi->setChartType('column');
                    $transaksi->setChartYear(date('Y'));

                    if ($_GET['periode'] == 'daterange') {
                        $_GET['date1'] = !isset($_GET['date1']) ? date('d/m/Y', strtotime('-1 week')) : $_GET['date1'];
                        $_GET['date2'] = !isset($_GET['date2']) ? date('d/m/Y') : $_GET['date2'];
                        if (fn::dateToUnix($_GET['date1']) > fn::dateToUnix($_GET['date2'])) {
                            $_GET['date1'] = date('d/m/Y', strtotime('-1 week', fn::dateToUnix($_GET['date2'])));
                        }
                        $date1 = date('Y-m-d', fn::dateToUnix($_GET['date1']));
                        $date2 = date('Y-m-d', fn::dateToUnix($_GET['date2']));
                        $transaksi->setChartSubtitle('Date: '.$_GET['date1'].' - '.$_GET['date2']);
                        $transaksi->setChartPeriode('dateRange');

                        /* Condition buat list data */
                        $cond = 'DATE(transaksi_waktu)';
                    } elseif ($_GET['periode'] == 'monthly') {
                        $_GET['date1'] = !isset($_GET['date1']) ? 1 : $_GET['date1'];
                        $_GET['date2'] = !isset($_GET['date2']) ? 12 : $_GET['date2'];

                        if ((int) $_GET['date2'] < (int) $_GET['date1']) {
                            $_GET['date2'] = $_GET['date1'];
                        }
                        $date1 = $_GET['date1'];
                        $date2 = $_GET['date2'];
                        $transaksi->setChartSubtitle(fn::namaBulan($_GET['date1']).' - '.fn::namaBulan($_GET['date2']).' '.$_GET['year']);
                        $transaksi->setChartYear($_GET['year']);
                        $transaksi->setChartPeriode('monthRange');
                        $cond = 'date_part(\'month\', transaksi_waktu)';
                    } else {
                        $_GET['date1'] = !isset($_GET['date1']) ? date('Y', strtotime('-1 year')) : $_GET['date1'];
                        $_GET['date2'] = !isset($_GET['date2']) ? date('Y') : $_GET['date2'];
                        if ($_GET['date1'] > $_GET['date2']) {
                            $_GET['date1'] = $_GET['date2'];
                        }
                        $date1 = $_GET['date1'];
                        $date2 = $_GET['date2'];
                        $transaksi->setChartSubtitle('Year: '.$_GET['date1'].' - '.$_GET['date2']);
                        $transaksi->setChartPeriode('yearRange');
                        $cond = 'date_part(\'year\', transaksi_waktu)';
                    }

                    $transaksi->setChartHeight('300px');
                    $transaksi->setChartDateRange(array(
                        $date1,
                        $date2,
                    ));
                    /* 1st Chart */
                    $transaksi->setChartTitle(_('Transaction').' [ '.$namakoperasi.' ]'.$addTitleSesi);
                    $transaksi->setChartPeriodeField('transaksi_waktu');
                    $transaksi->setChartFields($fields);
                    $transaksi->setChartContainer('widget_transaksi_chart1');
                    $data['transaksi_chart1'] = $transaksi->generateChart();

                    /* 2nd Chart */
                    $transaksi->setChartTitle(_('Weight (Kg)'));
                    $transaksi->setChartFields($fieldsberat);
                    $transaksi->setChartContainer('widget_transaksi_chart2');
                    $data['transaksi_chart2'] = $transaksi->generateChart();

                    $transaksi->setCondition('transaksi_status = \'y\' AND transaksi_koperasi = \''.$_GET['koperasi'].'\' AND '.$cond.' BETWEEN \''.$date1.'\' AND \''.$date2.'\''.$addSqlSesi);
                    $transaksi->setLimit(Config::$application['display_limit']);
                    $transaksi->setOrderBy('transaksi_waktu');
                    $transaksi->setOrderType('DESC');
                    $transaksi->setPage($_GET['page']);
                    $data['periode'] = $_GET['periode'];
                    $data['transaksi'] = $transaksi->read();
                } else {
                    $fields = array();
                    $fieldsberat = array();
                    foreach ($dtCoop['records'] as $k => $v) {
                        $fields['transaksi_id'.$k] = array(
                            'field' => 'transaksi_id',
                            'operation' => 'count',
                            'alias' => 'koperasi_'.$k,
                            'title' => $v['koperasi_nama'],
                            'condition' => 'transaksi_koperasi = \''.$v['koperasi_id'].'\'',
                        );
                        $fieldsberat['transaksi_totalberat'.$k] = array(
                            'field' => 'transaksi_totalberat',
                            'operation' => 'sum',
                            'alias' => 'totalkoperasi'.$k,
                            'title' => $v['koperasi_nama'],
                            'condition' => 'transaksi_koperasi = \''.$v['koperasi_id'].'\'',
                        );
                    }

                    /* transaksi REPORT */
                    $transaksi->setCondition('transaksi_status = \'y\''.$addSqlSesi);
                    $transaksi->setChartType('column');
                    $transaksi->setChartYear(date('Y'));

                    if ($_GET['periode'] == 'daterange') {
                        $_GET['date1'] = !isset($_GET['date1']) ? date('d/m/Y', strtotime('-1 week')) : $_GET['date1'];
                        $_GET['date2'] = !isset($_GET['date2']) ? date('d/m/Y') : $_GET['date2'];
                        if (fn::dateToUnix($_GET['date1']) > fn::dateToUnix($_GET['date2'])) {
                            $_GET['date1'] = date('d/m/Y', strtotime('-1 week', fn::dateToUnix($_GET['date2'])));
                        }
                        $date1 = date('Y-m-d', fn::dateToUnix($_GET['date1']));
                        $date2 = date('Y-m-d', fn::dateToUnix($_GET['date2']));
                        $transaksi->setChartSubtitle('Date: '.$_GET['date1'].' - '.$_GET['date2']);
                        $transaksi->setChartPeriode('dateRange');

                        /* Condition buat list data */
                        $cond = 'DATE(transaksi_waktu)';
                    } elseif ($_GET['periode'] == 'monthly') {
                        $_GET['date1'] = !isset($_GET['date1']) ? 1 : $_GET['date1'];
                        $_GET['date2'] = !isset($_GET['date2']) ? 12 : $_GET['date2'];

                        if ((int) $_GET['date2'] < (int) $_GET['date1']) {
                            $_GET['date2'] = $_GET['date1'];
                        }
                        $date1 = $_GET['date1'];
                        $date2 = $_GET['date2'];
                        $transaksi->setChartSubtitle(fn::namaBulan($_GET['date1']).' - '.fn::namaBulan($_GET['date2']).' '.$_GET['year']);
                        $transaksi->setChartYear($_GET['year']);
                        $transaksi->setChartPeriode('monthRange');
                        $cond = 'date_part(\'month\', transaksi_waktu)';
                    } else {
                        $_GET['date1'] = !isset($_GET['date1']) ? date('Y', strtotime('-1 year')) : $_GET['date1'];
                        $_GET['date2'] = !isset($_GET['date2']) ? date('Y') : $_GET['date2'];
                        if ($_GET['date1'] > $_GET['date2']) {
                            $_GET['date1'] = $_GET['date2'];
                        }
                        $date1 = $_GET['date1'];
                        $date2 = $_GET['date2'];
                        $transaksi->setChartSubtitle('Year: '.$_GET['date1'].' - '.$_GET['date2']);
                        $transaksi->setChartPeriode('yearRange');
                        $cond = 'date_part(\'year\', transaksi_waktu)';
                    }

                    $transaksi->setChartHeight('300px');
                    $transaksi->setChartDateRange(array(
                        $date1,
                        $date2,
                    ));
                    /* 1st Chart */
                    $transaksi->setChartTitle(_('Transaction').$addTitleSesi);
                    $transaksi->setChartPeriodeField('transaksi_waktu');
                    $transaksi->setChartFields($fields);
                    $transaksi->setChartContainer('widget_transaksi_chart1');
                    $data['transaksi_chart1'] = $transaksi->generateChart();

                    /* 2nd Chart */
                    $transaksi->setChartTitle(_('Weight (Kg)'));
                    $transaksi->setChartFields($fieldsberat);
                    $transaksi->setChartContainer('widget_transaksi_chart2');
                    $data['transaksi_chart2'] = $transaksi->generateChart();

                    $transaksi->setCondition('transaksi_status = \'y\' AND '.$cond.' BETWEEN \''.$date1.'\' AND \''.$date2.'\''.$addSqlSesi);
                    $transaksi->setLimit(Config::$application['display_limit']);
                    $transaksi->setOrderBy('transaksi_waktu');
                    $transaksi->setOrderType('DESC');
                    $transaksi->setPage($_GET['page']);
                    $data['periode'] = $_GET['periode'];
                    $data['transaksi'] = $transaksi->read();
                }
            }
        }
        $data['prepopulate'] = $prepopulate;
        View::display('report/report.transactionFarmer', $data);
    }
    public function sinkronisasi()
    {
        $logger = load::model('datalog');
        $transaksi = load::model('transaksi');
        $transaksi->setCondition('transaksi_status = \'y\' AND transaksi_sync != \'y\'');
        $transaksi->setOrderBy('transaksi_waktu');
        $tr = $transaksi->read();
        load::library('tcurl');
        if ($tr['total_data'] > 0) {
            $fields = $transaksi->getFields();
            $data = array();
            foreach ($tr['records'] as $k => $v) {
                foreach ($fields as $fk => $fv) {
                    $data[$k][$fv] = $v[$fv];
                }
            }
            //$data['tes'] = 'tes';
            $json_data = json_encode($data);

            $postdata['data'] = security::encrypt($json_data);
            $postdata['user_id'] = security::encrypt(session::get('user_id'));
            $postdata['public_key'] = security::encrypt(Config::$application['public_key']);
            $postdata['table'] = security::encrypt('t_transaksi');
            $request = tcurl::request(Config::$application['mcp_server_url'].'index.php?/synchandler/process', $postdata);
            //print_r($request);exit();
            if ($request['status'] == 'success') {
                $arrayID = array();
                foreach ($data as $dk => $dv) {
                    $transaksi->setCondition('');
                    $transaksi->setId($dv['transaksi_id']);
                    $transaksi->setData('transaksi_sync', 'y');
                    $transaksi->update();
//                    $updateData['transaksi_sync'] = 'y';
//                    db::update('t_transaksi',$updateData,'WHERE transaksi_id = \''.$dv['transaksi_id'].'\'');
                    $arrayID[] = $dv['transaksi_id'];
                }
                $detail = load::model('transaksidetail');
                $detail->setCondition('transaksidetail_sync != \'y\' AND transaksidetail_transaksi IN (\''.implode('\',', $arrayID).'\')');
                $detail->setLimit(0);
                $dataDetail = $detail->read();
                $detailFields = $detail->getFields();
                $dataStoreDetail = array();
                foreach ($dataDetail['records'] as $lk => $lv) {
                    foreach ($detailFields as $dfk => $dfv) {
                        $dataStoreDetail[$lk][$dfv] = $lv[$dfv];
                    }
                }
                $json_dataDetail = json_encode($dataStoreDetail);

                $postdataDetail['data'] = security::encrypt($json_dataDetail);
                $postdataDetail['user_id'] = security::encrypt(session::get('user_id'));
                $postdataDetail['public_key'] = security::encrypt(Config::$application['public_key']);
                $postdataDetail['table'] = security::encrypt('t_transaksidetail');
                $requestDetail = tcurl::request(Config::$application['mcp_server_url'].'index.php?/synchandler/process', $postdataDetail);
                //print_r($requestDetail);exit();
                if ($requestDetail['status'] == 'success') {
                    foreach ($dataStoreDetail as $dtk => $dtv) {
                        $detail->setCondition('');
                        $detail->setId($dv['transaksidetail_id']);
                        $detail->setData('transaksidetail_sync', 'y');
                        $detail->update();
//                        $updateDetailData['transaksidetail_sync'] = 'y';
//                        db::update('t_transaksidetail',$updateDetailData,'WHERE transaksidetail_id = \''.$dtv['transaksidetail_id'].'\'');
                    }
                    session::set('errorMsg', 'Sync success');
                    $datalog['datalog_id'] = fn::generate_id(true);
                    $datalog['datalog_time'] = date('Y-m-d H:i:s');
                    $datalog['datalog_ip'] = $_SERVER['REMOTE_ADDR'];
                    $datalog['datalog_key'] = 'sync';
                    $datalog['datalog_client'] = Config::$application['client_id'];
                    $datalog['datalog_data'] = 'Milk Collection Data synchronized by: '.session::get('user_fullname').' ('.session::get('user_id').') #ID: '.implode('\',', $arrayID);
                    $logger->setDataArray($datalog);
                    $logger->insert();
                } else {
                    session::set('errorMsg', $requestDetail['msg']);
                }
                //session::set('errorMsg','Sync success');
            } else {
                session::set('errorMsg', $request['msg']);
            }
        } else {
            session::set('errorMsg', 'Sync success');
        }
        // SYNC SETTINGS

        $postSettingsUser['data'] = security::encrypt(json_encode(array()));
        $postSettingsUser['user_id'] = security::encrypt(session::get('user_id'));
        $postSettingsUser['public_key'] = security::encrypt(Config::$application['public_key']);
        $postSettingsUser['table'] = security::encrypt('t_user');
        $requestUser = tcurl::request(Config::$application['mcp_server_url'].'index.php?/synchandler/process', $postSettingsUser);
        //print_r($requestUser);exit();
        $usermod = load::model('user');
        foreach ($requestUser['msg'] as $k => $v) {
            $usermod->setCondition('user_id = \''.$v['user_id'].'\'');
            $tes = $usermod->read();
            if ($tes['total_data'] === 0) {
                $usermod->setDataArray($v);
                $usermod->insert();
            } else {
                $usermod->setId($v['user_id']);
                $usermod->setDataArray($v);
                $usermod->update();
            }
        }
        $datalog['datalog_id'] = fn::generate_id(true);
        $datalog['datalog_time'] = date('Y-m-d H:i:s');
        $datalog['datalog_ip'] = $_SERVER['REMOTE_ADDR'];
        $datalog['datalog_key'] = 'sync';
        $datalog['datalog_client'] = Config::$application['client_id'];
        $datalog['datalog_data'] = 'User Data synchronized by: '.session::get('user_fullname').' ('.session::get('user_id').') ';
        $logger->setDataArray($datalog);
        $logger->insert();
        session::set('errorMsg', 'Sync success');
        header('location:'.$_SERVER['HTTP_REFERER']);
        exit();
    }

    public function export($type = '')
    {
        $_GET['periode'] = !isset($_GET['periode']) ? 'daterange' : $_GET['periode'];
        switch ($type) {
            case 'transaction':
                $periode = !isset($_GET['periode']) ? 'daterange' : $_GET['periode'];
                $_GET['year'] = !isset($_GET['year']) ? date('Y') : $_GET['year'];
                /* transaksi REPORT */
                $transaksi = load::model('transaksi');

                if ($periode == 'daterange') {
                    $_GET['date1'] = !isset($_GET['date1']) ? date('d/m/Y', strtotime('-1 week')) : $_GET['date1'];
                    $_GET['date2'] = !isset($_GET['date2']) ? date('d/m/Y') : $_GET['date2'];
                    if (fn::dateToUnix($_GET['date1']) > fn::dateToUnix($_GET['date2'])) {
                        $_GET['date1'] = date('d/m/Y', strtotime('-1 week', fn::dateToUnix($_GET['date2'])));
                    }
                    $date1 = date('Y-m-d', fn::dateToUnix($_GET['date1']));
                    $date2 = date('Y-m-d', fn::dateToUnix($_GET['date2']));

                    $cond = 'transaksi_status = \'y\' AND DATE(transaksi_waktu) BETWEEN \''.$date1.'\' AND \''.$date2.'\'';
                } elseif ($periode == 'monthly') {
                    $_GET['date1'] = !isset($_GET['date1']) ? 1 : $_GET['date1'];
                    $_GET['date2'] = !isset($_GET['date2']) ? 12 : $_GET['date2'];

                    if ((int) $_GET['date2'] < (int) $_GET['date1']) {
                        $_GET['date2'] = $_GET['date1'];
                    }
                    $date1 = $_GET['date1'];
                    $date2 = $_GET['date2'];

                    $cond = 'transaksi_status = \'y\' AND date_part(\'month\', transaksi_waktu) BETWEEN \''.$date1.'\' AND \''.$date2.'\'';
                } else {
                    $_GET['date1'] = !isset($_GET['date1']) ? date('Y', strtotime('-1 year')) : $_GET['date1'];
                    $_GET['date2'] = !isset($_GET['date2']) ? date('Y') : $_GET['date2'];
                    if ($_GET['date1'] > $_GET['date2']) {
                        $_GET['date1'] = $_GET['date2'];
                    }
                    $date1 = $_GET['date1'];
                    $date2 = $_GET['date2'];

                    $cond = 'transaksi_status = \'y\' AND date_part(\'year\', transaksi_waktu) BETWEEN \''.$date1.'\' AND \''.$date2.'\'';
                }
                if (isset($_GET['anggota']) && !empty($_GET['anggota'])) {
                    $cond .= ' AND transaksi_anggota = \''.$_GET['anggota'].'\'';
                }
                $transaksi->setCondition($cond);
                $transaksi->setLimit(0);
                $dtTransaksi = $transaksi->read();

                $header = array('NO', 'TPK', 'TANGGAL', 'SESSION', 'ID TRANSAKSI', 'ID MEMBER', 'NAMA', 'TOTAL BERAT (KG)');
                $filename = 'MCP-'.$periode.'-'.$date1.'to'.$date2.'.xls';

                header("Content-Disposition: attachment; filename=\"$filename\"");
                header('Content-Type: application/vnd.ms-excel');
                echo implode("\t", $header)."\n";
                $no = 1;
                $datanya = array();
                $totalBerat = 0;
                $__session = array(
                    'p' => 'Morning',
                    's' => 'Afternoon',
                    );
                foreach ($dtTransaksi['records'] as $k => $v) {
                    $str = $no."\t";
                    $str .= $v['tpk_nama'];
                    $str .= "\t";
                    $str .= $v['transaksi_waktu'];
                    $str .= "\t";
                    $str .= $__session[$v['transaksi_sesi']];
                    $str .= "\t";
                    $str .= $v['transaksi_id'];
                    $str .= "\t";
                    $str .= $v['transaksi_nomor'];
                    $str .= "\t";
                    $str .= $v['transaksi_nama'];
                    $str .= "\t";
                    $str .= (float) $v['transaksi_totalberat'];
                    $totalBerat += $v['transaksi_totalberat'];
                    $datanya[] = $str;
                    ++$no;
                }
                echo implode("\n", $datanya);
                echo "\n";
                echo " \t \t \t \t \t \t TOTAL: \t";
                echo (float) $totalBerat;
                exit();
                break;
            case 'qcparameter':
                $periode = !isset($_GET['periode']) ? 'daterange' : $_GET['periode'];
                $_GET['year'] = !isset($_GET['year']) ? date('Y') : $_GET['year'];
                /* transaksi REPORT */
                $qcparameter = load::model('qcparameter');

                if ($periode == 'daterange') {
                    $_GET['date1'] = !isset($_GET['date1']) ? date('d/m/Y', strtotime('-1 week')) : $_GET['date1'];
                    $_GET['date2'] = !isset($_GET['date2']) ? date('d/m/Y') : $_GET['date2'];
                    if (fn::dateToUnix($_GET['date1']) > fn::dateToUnix($_GET['date2'])) {
                        $_GET['date1'] = date('d/m/Y', strtotime('-1 week', fn::dateToUnix($_GET['date2'])));
                    }
                    $date1 = date('Y-m-d', fn::dateToUnix($_GET['date1']));
                    $date2 = date('Y-m-d', fn::dateToUnix($_GET['date2']));

                    $cond = 'qcparameter_status = \'y\' AND DATE(t_transaksi.transaksi_waktu) BETWEEN \''.$date1.'\' AND \''.$date2.'\'';
                } elseif ($periode == 'monthly') {
                    $_GET['date1'] = !isset($_GET['date1']) ? 1 : $_GET['date1'];
                    $_GET['date2'] = !isset($_GET['date2']) ? 12 : $_GET['date2'];

                    if ((int) $_GET['date2'] < (int) $_GET['date1']) {
                        $_GET['date2'] = $_GET['date1'];
                    }
                    $date1 = $_GET['date1'];
                    $date2 = $_GET['date2'];

                    $cond = 'qcparameter_status = \'y\' AND date_part(\'month\', t_transaksi.transaksi_waktu) BETWEEN \''.$date1.'\' AND \''.$date2.'\'';
                } else {
                    $_GET['date1'] = !isset($_GET['date1']) ? date('Y', strtotime('-1 year')) : $_GET['date1'];
                    $_GET['date2'] = !isset($_GET['date2']) ? date('Y') : $_GET['date2'];
                    if ($_GET['date1'] > $_GET['date2']) {
                        $_GET['date1'] = $_GET['date2'];
                    }
                    $date1 = $_GET['date1'];
                    $date2 = $_GET['date2'];

                    $cond = 'qcparameter_status = \'y\' AND date_part(\'year\', t_transaksi.transaksi_waktu) BETWEEN \''.$date1.'\' AND \''.$date2.'\'';
                }
                if (isset($_GET['koperasi']) && !empty($_GET['koperasi'])) {
                    $cond .= ' AND qcparameter_koperasi = \''.$_GET['koperasi'].'\'';
                }

                if (isset($_GET['tpk']) && !empty($_GET['tpk'])) {
                    $cond .= ' AND qcparameter_tpk = \''.$_GET['tpk'].'\'';
                }
				
				$qcparameter->setOrderBy('t_transaksi.transaksi_waktu');
                $qcparameter->setCondition($cond);
                $qcparameter->setLimit(0);
                $dtTransaksi = $qcparameter->read();

                $header = array('TRANSACTION ID', 'FARMER ID', 'NAME', 'DATE', 'SESSION', 'TPC', 'FAT', 'PROTEIN', 'LACTOSE', 'TS', 'FP', 'ACIDITY', 'UREA', 'SNF');
                $filename = 'QC-'.$periode.'-'.$date1.'to'.$date2.'.xls';

                header("Content-Disposition: attachment; filename=\"$filename\"");
                header('Content-Type: application/vnd.ms-excel');
                echo implode("\t", $header)."\n";

                $no = 1;
                $datanya = array();
                $totalBerat = 0;
                $_cfu = 0;
                $_fat = 0;
                $_protein = 0;
                $_lactose = 0;
                $_ts = 0;
                $_fp = 0;
                $_acidity = 0;
                $_urea = 0;
                $_snf = 0;
                $_totalrow = 0;
                foreach ($dtTransaksi['records'] as $k => $v) {
                    $_cfu       += (float) $v['qcparameter_cfu'];
                    $_fat       += (float) $v['qcparameter_fat'];
                    $_protein   += (float) $v['qcparameter_protein'];
                    $_lactose   += (float) $v['qcparameter_lactose'];
                    $_ts        += (float) $v['qcparameter_ts'];
                    $_fp        += (float) $v['qcparameter_fp'];
                    $_acidity   += (float) $v['qcparameter_acidity'];
                    $_urea      += (float) $v['qcparameter_urea'];
                    $_snf       += (float) $v['qcparameter_snf'];
                    ++$_totalrow;

                    $str = $v['qcparameter_transaksi'];
                    $str .= "\t";
                    $str .= $v['transaksi_nomor'];
                    $str .= "\t";
                    $str .= $v['transaksi_nama'];
                    $str .= "\t";
                    $str .= $v['transaksi_waktu'];
                    $str .= "\t";
                    $str .= $v['transaksi_sesi'];
                    $str .= "\t";
                    $str .= (float) $v['qcparameter_cfu'];
                    $str .= "\t";
                    $str .= (float) $v['qcparameter_fat'];
                    $str .= "\t";
                    $str .= (float) $v['qcparameter_protein'];
                    $str .= "\t";
                    $str .= (float) $v['qcparameter_lactose'];
                    $str .= "\t";
                    $str .= (float) $v['qcparameter_ts'];
                    $str .= "\t";
                    $str .= (float) $v['qcparameter_fp'];
                    $str .= "\t";
                    $str .= (float) $v['qcparameter_acidity'];
                    $str .= "\t";
                    $str .= (float) $v['qcparameter_urea'];
                    $str .= "\t";
                    $str .= (float) $v['qcparameter_snf'];

                    $datanya[] = $str;
                    ++$no;
                }
                echo implode("\n", $datanya);
                echo "\n";
                echo " \t \t \t \t AVERAGE: \t";
                if ($_totalrow === 0) {
                    echo 0;
                    echo "\t";
                    echo 0;
                    echo "\t";
                    echo 0;
                    echo "\t";
                    echo 0;
                    echo "\t";
                    echo 0;
                    echo "\t";
                    echo 0;
                    echo "\t";
                    echo 0;
                    echo "\t";
                    echo 0;
                    echo "\t";
                    echo 0;
                } else {
                    echo $_cfu / $_totalrow;
                    echo "\t";
                    echo $_fat / $_totalrow;
                    echo "\t";
                    echo $_protein / $_totalrow;
                    echo "\t";
                    echo $_lactose / $_totalrow;
                    echo "\t";
                    echo $_ts / $_totalrow;
                    echo "\t";
                    echo $_fp / $_totalrow;
                    echo "\t";
                    echo $_acidity / $_totalrow;
                    echo "\t";
                    echo $_urea / $_totalrow;
                    echo "\t";
                    echo $_snf / $_totalrow;
                }

                exit();
                break;
            case 'distribution':
                $periode = !isset($_GET['periode']) ? 'daterange' : $_GET['periode'];
                $_GET['year'] = !isset($_GET['year']) ? date('Y') : $_GET['year'];
                /* transaksi REPORT */
                $distribution = load::model('distribution');

                if ($periode == 'daterange') {
                    $_GET['date1'] = !isset($_GET['date1']) ? date('d/m/Y', strtotime('-1 week')) : $_GET['date1'];
                    $_GET['date2'] = !isset($_GET['date2']) ? date('d/m/Y') : $_GET['date2'];
                    if (fn::dateToUnix($_GET['date1']) > fn::dateToUnix($_GET['date2'])) {
                        $_GET['date1'] = date('d/m/Y', strtotime('-1 week', fn::dateToUnix($_GET['date2'])));
                    }
                    $date1 = date('Y-m-d', fn::dateToUnix($_GET['date1']));
                    $date2 = date('Y-m-d', fn::dateToUnix($_GET['date2']));

                    $cond = 'distribution_status != \'d\' AND DATE(distribution_tanggal) BETWEEN \''.$date1.'\' AND \''.$date2.'\'';
                } elseif ($periode == 'monthly') {
                    $_GET['date1'] = !isset($_GET['date1']) ? 1 : $_GET['date1'];
                    $_GET['date2'] = !isset($_GET['date2']) ? 12 : $_GET['date2'];

                    if ((int) $_GET['date2'] < (int) $_GET['date1']) {
                        $_GET['date2'] = $_GET['date1'];
                    }
                    $date1 = $_GET['date1'];
                    $date2 = $_GET['date2'];

                    $cond = 'distribution_status != \'d\' AND date_part(\'month\', distribution_tanggal) BETWEEN \''.$date1.'\' AND \''.$date2.'\'';
                } else {
                    $_GET['date1'] = !isset($_GET['date1']) ? date('Y', strtotime('-1 year')) : $_GET['date1'];
                    $_GET['date2'] = !isset($_GET['date2']) ? date('Y') : $_GET['date2'];
                    if ($_GET['date1'] > $_GET['date2']) {
                        $_GET['date1'] = $_GET['date2'];
                    }
                    $date1 = $_GET['date1'];
                    $date2 = $_GET['date2'];

                    $cond = 'distribution_status != \'d\' AND date_part(\'year\', distribution_tanggal) BETWEEN \''.$date1.'\' AND \''.$date2.'\'';
                }
                $distribution->setCondition($cond);
                $distribution->setLimit(0);
                $dtTransaksi = $distribution->read();

                $header = array('NO',
                                'DATE',
                                'CONSUMER',
                                'TRUCK',
                                'LEAVE',
                                'CHECK IN I.P.S',
                                'CHECK OUT I.P.S',
                                'WEIGHT',
                                'DENSITY',
                                'TEMPERATURE',
                                'ALCOHOL TEST',
                                'FAT',
                                'PROTEIN',
                                'LACTOSE',
                                'FREEZING POINT',
                                'TOTAL SOLID',
                                'ACIDITY',
                                'UREA',
                                'SNF',
                                'ADULTERATION',
                                'STATUS',
                            );
                $filename = 'DO-'.$periode.'-'.$date1.'to'.$date2.'.xls';

                header("Content-Disposition: attachment; filename=\"$filename\"");
                header('Content-Type: application/vnd.ms-excel');
                echo implode("\t", $header)."\n";
                $no = 1;
                $datanya = array();
                $totalBerat = 0;
                $distribitionstatus = array(
                    'n' => 'On Delivery',
                    'y' => 'Completed',
                    'r' => 'Rejected',
                    );
                foreach ($dtTransaksi['records'] as $k => $v) {
                    $str = $no."\t";
                    $str .= $v['distribution_tanggal'];
                    $str .= "\t";
                    $str .= $v['konsumen_nama'];
                    $str .= "\t";
                    $str .= $v['truck_nopol'];
                    $str .= "\t";
                    $str .= $v['distribution_berangkatjam'];
                    $str .= "\t";
                    $str .= $v['distribution_masukips'];
                    $str .= "\t";
                    $str .= $v['distribution_keluarips'];
                    $str .= "\t";
                    $str .= $v['distribution_berat'];
                    $str .= "\t";
                    $str .= $v['distribution_beratjenis'];
                    $str .= "\t";
                    $str .= $v['distribution_temperature'];
                    $str .= "\t";
                    if ($v['distribution_alkohol'] == 0) {
                        $str .= 'Negative';
                    } else {
                        $str .= 'Positive';
                    }
                    $str .= "\t";
                    $str .= $v['distribution_fat'];
                    $str .= "\t";
                    $str .= $v['distribution_protein'];
                    $str .= "\t";
                    $str .= $v['distribution_lactose'];
                    $str .= "\t";
                    $str .= $v['distribution_fp'];
                    $str .= "\t";
                    $str .= $v['distribution_ts'];
                    $str .= "\t";
                    $str .= $v['distribution_acidity'];
                    $str .= "\t";
                    $str .= $v['distribution_urea'];
                    $str .= "\t";
                    $str .= $v['distribution_snf'];
                    $str .= "\t";
                    if ($v['distribution_pemalsuan'] == 0) {
                        $str .= 'Negative';
                    } else {
                        $str .= 'Positive';
                    }
                    $str .= "\t";
                    $str .= $distribitionstatus[$v['distribution_status']];
                    //$str .= "\t";
                    // $str .= (float) $v['transaksi_totalberat'];
                    // $totalBerat += $v['transaksi_totalberat'];
                    $datanya[] = $str;
                    ++$no;
                }
                echo implode("\n", $datanya);
                // echo "\n";
                // echo " \t \t \t \t \t TOTAL: \t";
                // echo (float) $totalBerat;
                exit();
                break;
        }
    }

    public function qcparameter()
    {
        $dt = load::model('qcparameter');
        $data['tpk'] = array();
        $data['kelompok'] = array();
        $data['anggota'] = array();
        $data['prepopulate'] = array();
        $koperasi = load::model('koperasi');
        $tpk = load::model('tpk');
        $tpkCondition = 'tpk_status = \'y\'';
        $_GET['tpk'] = !isset($_GET['tpk']) ? null : $_GET['tpk'];
        if (!in_array(session::get('user_group'), Config::$application['super_group'])) {
            $data['koperasi']['records'][] = array(
                    'koperasi_id' => session::get('user_koperasi'),
                    'koperasi_nama' => session::get('user_koperasi_nama'),
                );
            $_GET['koperasi'] = session::get('user_koperasi');
            $tpkCondition .= ' AND tpk_koperasi =  \''.session::get('user_koperasi').'\'';
        } else {
            $koperasi->setLimit(0);
            $koperasi->setCondition('koperasi_status = \'y\'');
            $data['koperasi'] = $koperasi->read();
            $_GET['koperasi'] = !isset($_GET['koperasi']) ? null : $_GET['koperasi'];
        }

        $data['tpk'] = $tpk->setCondition($tpkCondition)
                            ->setLimit(0)
                            ->read();

        $_GET['anggota'] = !isset($_GET['anggota']) ? null : $_GET['anggota'];
        $_GET['periode'] = !isset($_GET['periode']) ? 'daterange' : $_GET['periode'];
        $_GET['year'] = !isset($_GET['year']) ? date('Y') : $_GET['year'];
        $_GET['page'] = !isset($_GET['page']) ? 1 : $_GET['page'];

        $dt->setChartType('spline');
        $dt->setChartYear(date('Y'));
        $chartTitle = 'Quality Report';
        if (!empty($_GET['anggota'])) {
            $_GET['filterby'] = 'anggota';
        } else {
            $_GET['filterby'] = 'koperasi';
        }
        if ($_GET['filterby'] == 'koperasi') {
            // $chartTitle .= ' All Cooperative';
            if (isset($_GET['koperasi']) && !empty($_GET['koperasi'])) {
                $koperasi->setId($_GET['koperasi']);
                $dtKop = $koperasi->read();
                $chartTitle .= ' '.$dtKop['records']['koperasi_nama'];
            }
            if (isset($_GET['tpk']) && !empty($_GET['tpk'])) {
                $dtTpk = $tpk->setId($_GET['tpk'])->read();
                $chartTitle .= ' '.$dtTpk['records']['tpk_nama'];
            }
        } else {
            if (!empty($_GET['anggota'])) {
                $anggota = load::model('anggota');
                $anggota->setId($_GET['anggota']);
                $dtAnggota = $anggota->read();
                $data['prepopulate'][] = array(
                        'id' => $_GET['anggota'],
                        'name' => $dtAnggota['records']['anggota_nama'],
                    );
                $chartTitle .= ' (Farmer: '.$dtAnggota['records']['anggota_nama'].')';
            }
        }
        $dt->setChartTitle($chartTitle);
        $dt->setChartGlobalAverage(true);
        switch ($_GET['periode']) {
            case 'daterange':
                $_GET['date1'] = !isset($_GET['date1']) ? date('d/m/Y', strtotime('-1 week')) : $_GET['date1'];
                $_GET['date2'] = !isset($_GET['date2']) ? date('d/m/Y') : $_GET['date2'];
                if (fn::dateToUnix($_GET['date1']) > fn::dateToUnix($_GET['date2'])) {
                    $_GET['date1'] = date('d/m/Y', strtotime('-1 week', fn::dateToUnix($_GET['date2'])));
                }
                $date1 = date('Y-m-d', fn::dateToUnix($_GET['date1']));
                $date2 = date('Y-m-d', fn::dateToUnix($_GET['date2']));

                $cond = 'DATE(t_transaksi.transaksi_waktu) ';
                $dt->setChartPeriode('dateRange');
                break;
            case 'monthly':
                $_GET['date1'] = !isset($_GET['date1']) ? 1 : $_GET['date1'];
                $_GET['date2'] = !isset($_GET['date2']) ? 12 : $_GET['date2'];

                if ((int) $_GET['date2'] < (int) $_GET['date1']) {
                    $_GET['date2'] = $_GET['date1'];
                }
                $date1 = $_GET['date1'];
                $date2 = $_GET['date2'];
                $cond = 'date_part(\'month\', t_transaksi.transaksi_waktu) ';
                $dt->setChartPeriode('monthRange');
                break;
            case 'yearly':
                $_GET['date1'] = !isset($_GET['date1']) ? date('Y', strtotime('-1 year')) : $_GET['date1'];
                $_GET['date2'] = !isset($_GET['date2']) ? date('Y') : $_GET['date2'];
                if ($_GET['date1'] > $_GET['date2']) {
                    $_GET['date1'] = $_GET['date2'];
                }
                $date1 = $_GET['date1'];
                $date2 = $_GET['date2'];

                $dt->setChartPeriode('yearRange');
                $cond = 'date_part(\'year\', t_transaksi.transaksi_waktu)';
                break;
        }
        $kondisional = 'qcparameter_status = \'y\'';
        if ($_GET['filterby'] == 'koperasi') {
            if (empty($_GET['koperasi'])) {
                if (!in_array(session::get('user_group'), Config::$application['super_group'])) {
                    $kondisional .= ' AND qcparameter_koperasi = \''.session::get('user_koperasi').'\'';
                }
            } else {
                $kondisional .= ' AND qcparameter_koperasi = \''.$_GET['koperasi'].'\'';
            }
            if (isset($_GET['tpk']) && !empty($_GET['tpk'])) {
                $kondisional .= ' AND qcparameter_tpk = \''.$_GET['tpk'].'\'';
            }
        } else {
            if (!empty($_GET['anggota'])) {
                $kondisional .= ' AND qcparameter_anggota = \''.$_GET['anggota'].'\'';
            }
        }
        $dt->setCondition($kondisional);
        $dt->setChartHeight('400px');
        $dt->setChartDateRange(array(
            $date1,
            $date2,
        ));
        $dt->setChartPeriodeField('qcparameter_bactoscandate');

        $chartfields = array(
                'FAT' => array(
                        'qcparameter_fat' => array(
                            'operation' => 'average',
                            'title' => 'FAT (Average)',
                        ),
                    ),
                'PROTEIN' => array(
                        'qcparameter_protein' => array(
                            'operation' => 'average',
                            'title' => 'Protein (Average)',
                        ),
                    ),
                'LACTOSE' => array(
                        'qcparameter_lactose' => array(
                            'operation' => 'average',
                            'title' => 'Lactose (Average)',
                        ),
                    ),
                'FP' => array(
                        'qcparameter_fp' => array(
                            'operation' => 'average',
                            'title' => 'FP (Average)',
                        ),
                    ),
                'ACIDITY' => array(
                        'qcparameter_acidity' => array(
                            'operation' => 'average',
                            'title' => 'Acidity (Average)',
                        ),
                    ),
                'UREA' => array(
                        'qcparameter_urea' => array(
                            'operation' => 'average',
                            'title' => 'Urea (Average)',
                        ),
                    ),
                'CFU' => array(
                        'qcparameter_cfu' => array(
                            'operation' => 'average',
                            'title' => 'CFU (Average)',
                        ),
                    ),
            );

        foreach ($chartfields as $k => $v) {
            $dt->setChartContainer('widget_transaksi_chart'.$k);
            $dt->setChartSubtitle($k);
            $dt->setChartFields($v);
            //print_r($v);
            $data['transaksi_chart'][$k] = $dt->generateChart();
        }
        $data['chartfields'] = $chartfields;

        $dt->setCondition($kondisional.' AND '.$cond.' BETWEEN \''.$date1.'\' AND \''.$date2.'\'');
        $dt->setOrderBy('t_transaksi.transaksi_waktu');
        $dt->setOrderType('DESC');
        $dt->setPage($_GET['page']);
        $data['periode'] = $_GET['periode'];
        $data['data'] = $dt->read();
        View::display('report/report.qcparameter', $data);
    }
    public function distribution()
    {
        $dt = load::model('distribution');
        $koperasi = load::model('koperasi');
        $_GET['periode'] = !isset($_GET['periode']) ? 'daterange' : $_GET['periode'];
        $_GET['year'] = !isset($_GET['year']) ? date('Y') : $_GET['year'];
        $_GET['page'] = !isset($_GET['page']) ? 1 : $_GET['page'];
        $_GET['koperasi'] = !isset($_GET['koperasi']) ? '' : $_GET['koperasi'];

        $dt->setChartType('spline');
        $dt->setChartYear(date('Y'));
        $dt->setChartTitle('Delivery Order Report');

        $kondisi = '';
        if (!in_array(session::get('user_group'), Config::$application['super_group'])) {
            $kondisi = ' AND distribution_koperasi = \''.session::get('user_koperasi').'\'';
            $koperasi->setCondition('koperasi_status = \'y\' AND koperasi_id = \''.session::get('user_koperasi').'\'');
        } else {
            $koperasi->setCondition('koperasi_status = \'y\'');
            if (!empty($_GET['koperasi'])) {
                $kondisi = ' AND distribution_koperasi = \''.$_GET['koperasi'].'\'';
            }
        }

        $koperasi->setLimit(0);
        $data['koperasi'] = $koperasi->read();

        /*
        *   tambah nama koperasi di subtitle
        */
        if (!empty($_GET['koperasi'])) {
            foreach ($data['koperasi']['records'] as $k => $v) {
                if ($v['koperasi_id'] == $_GET['koperasi']) {
                    $dt->setChartSubtitle($v['koperasi_nama']);
                }
            }
        }

        switch ($_GET['periode']) {
            case 'daterange':
                $_GET['date1'] = !isset($_GET['date1']) ? date('d/m/Y', strtotime('-1 week')) : $_GET['date1'];
                $_GET['date2'] = !isset($_GET['date2']) ? date('d/m/Y') : $_GET['date2'];
                if (fn::dateToUnix($_GET['date1']) > fn::dateToUnix($_GET['date2'])) {
                    $_GET['date1'] = date('d/m/Y', strtotime('-1 week', fn::dateToUnix($_GET['date2'])));
                }
                $date1 = date('Y-m-d', fn::dateToUnix($_GET['date1']));
                $date2 = date('Y-m-d', fn::dateToUnix($_GET['date2']));

                $cond = 'DATE(distribution_tanggal) ';
                $dt->setChartPeriode('dateRange');
                break;
            case 'monthly':
                $_GET['date1'] = !isset($_GET['date1']) ? 1 : $_GET['date1'];
                $_GET['date2'] = !isset($_GET['date2']) ? 12 : $_GET['date2'];

                if ((int) $_GET['date2'] < (int) $_GET['date1']) {
                    $_GET['date2'] = $_GET['date1'];
                }
                $date1 = $_GET['date1'];
                $date2 = $_GET['date2'];
                $cond = 'date_part(\'month\', distribution_tanggal) ';
                $dt->setChartPeriode('monthRange');
                break;
            case 'yearly':
                $_GET['date1'] = !isset($_GET['date1']) ? date('Y', strtotime('-1 year')) : $_GET['date1'];
                $_GET['date2'] = !isset($_GET['date2']) ? date('Y') : $_GET['date2'];
                if ($_GET['date1'] > $_GET['date2']) {
                    $_GET['date1'] = $_GET['date2'];
                }
                $date1 = $_GET['date1'];
                $date2 = $_GET['date2'];

                $dt->setChartPeriode('yearRange');
                $cond = 'date_part(\'year\', distribution_tanggal)';
                break;
        }

        $dt->setCondition('distribution_status != \'d\''.$kondisi);
        $dt->setChartHeight('400px');
        $dt->setChartDateRange(array(
            $date1,
            $date2,
        ));
        $dt->setChartPeriodeField('distribution_tanggal');
        $dt->setChartFields(array(
            'distribution_id1' => array(
                'field' => 'distribution_id',
                'alias' => 'distribution_id1',
                'operation' => 'count',
                'title' => 'On Delivery',
                'condition' => 'distribution_status = \'n\'',
            ),

            'distribution_id2' => array(
                'field' => 'distribution_id',
                'alias' => 'distribution_id2',
                'operation' => 'count',
                'title' => 'Completed',
                'condition' => 'distribution_status = \'y\'',
                ),
             'distribution_id3' => array(
                'field' => 'distribution_id',
                'alias' => 'distribution_id3',
                'operation' => 'count',
                'title' => 'Rejected',
                'condition' => 'distribution_status = \'r\'',
                ),

        ));
        $dt->setChartContainer('widget_transaksi_chart');
        $data['transaksi_chart'] = $dt->generateChart();

        $dt->setCondition('distribution_status != \'d\' '.$kondisi.' AND '.$cond.' BETWEEN \''.$date1.'\' AND \''.$date2.'\'');
        $dt->setLimit(Config::$application['display_limit']);
        $dt->setOrderBy('distribution_tanggal');
        $dt->setOrderType('DESC');
        $dt->setPage($_GET['page']);
        $data['periode'] = $_GET['periode'];
        $data['data'] = $dt->read();
        //print_r($data);exit();
        View::display('report/report.distribution', $data);
    }
    public function datalog()
    {
        $dt = load::model('datalog');
        $_GET['page'] = !isset($_GET['page']) ? 1 : $_GET['page'];
        $dt->setPage($_GET['page']);
        $dt->setOrderBy('datalog_time');
        $dt->setOrderType('DESC');
        $data['log'] = $dt->read();
        View::display('report/report.datalog', $data);
    }

    public function pendapatan($overrideAnggota = 'override')
    {
        session::set('title', 'Pendapatan Report');

        $anggota = load::model('anggota');
        $qcparameter = load::model('qcparameter');
        $milkpriceformula = load::model('milkpriceformula');
        $milkpricecondition = load::model('milkpricecondition');
        $coop = load::model('koperasi');
        $periode = load::model('periode');
        $rezgrade = load::model('rezgrade');
        $fpgrade = load::model('fpgrade');
        $transaksi = load::model('transaksi');

        $_GET['page'] = !isset($_GET['page']) ? 1 : $_GET['page'];
        $_GET['keyword'] = !isset($_GET['keyword']) ? '' : $_GET['keyword'];
        $_GET['month'] = !isset($_GET['month']) ? date('n') : $_GET['month'];
        $_GET['year'] = !isset($_GET['year']) ? date('Y') : $_GET['year'];
        $_GET['periode'] = !isset($_GET['periode']) ? '' : $_GET['periode'];

        if (!empty($overrideAnggota == 'override')) {
            $dataAnggota = $anggota->setCondition('anggota_nomor = \''.session::get('user_name').'\' AND anggota_status = \'y\' AND t_card.card_status = \'y\' AND t_cow.cow_status = \'y\'')->setLimit(1)->read();
            // print_r($dataAnggota);
            $_GET['anggota'] = $dataAnggota['records'][0]['anggota_id'];
        } else {
            $_GET['anggota'] = !isset($_GET['anggota']) ? '' : $_GET['anggota'];
        }

        if (!in_array(session::get('user_group'), Config::$application['super_group'])) {
            $koperasi = session::get('user_koperasi');
            $dtKoperasi = $coop->setCondition('koperasi_id = \''.$koperasi.'\'')->read();
            $_GET['koperasi'] = $koperasi;
        } else {
            $dtKoperasi = $coop->setCondition('koperasi_status = \'y\'')->setLimit(0)->read();
            if (isset($_GET['koperasi'])) {
                $koperasi = $_GET['koperasi'];
            } else {
                $koperasi = $dtKoperasi['records'][0]['koperasi_id'];
                $_GET['koperasi'] = $koperasi;
            }
            $data['koperasi'] = $dtKoperasi;
        }

        /* Data periode */
       /*  $dtPeriode = $periode->setCondition('periode_status = \'y\' AND periode_koperasi = \''.$koperasi.'\' AND date_part(\'month\', periode_effectivedate) <= '.$_GET['month'].' AND date_part(\'year\', periode_effectivedate) <= '.$_GET['year'])
                             ->setLimit(1)
                             ->setOrderBy('periode_effectivedate')
                             ->setOrderType('ASC')
                             ->read(); */

        $tglCekPeriode = date('Y-m-t', strtotime($_GET['year'].'-'.$_GET['month'].'-01'));

        $dtPeriode = $periode->setCondition('periode_status = \'y\' AND periode_koperasi = \''.$koperasi.'\' AND DATE(periode_effectivedate) <= \''.$tglCekPeriode.'\'')
                             ->setLimit(1)
                             ->setOrderBy('periode_effectivedate')
                             ->setOrderType('DESC')
                             ->read();
        if ($dtPeriode['total_data'] === 0) {
            session::set('errorMsg', 'Please setup periode!');
            header('location:'.BASE_URL.'index.php?/dashboard');
            exit();
        }

        $arrayPeriode = $dtPeriode['records'][0]['periodedetail'];
        // print_r($arrayPeriode);
        if (!empty($_GET['anggota'])) {
            $anggota->setCondition('anggota_status = \'y\' AND anggota_koperasi = \''.$koperasi.'\' AND anggota_id = \''.$_GET['anggota'].'\' AND anggota_status != \'d\' AND t_card.card_status = \'y\' AND t_cow.cow_status = \'y\'');
        } else {
            $anggota->setCondition('anggota_status = \'y\' AND anggota_koperasi = \''.$koperasi.'\' AND anggota_status != \'d\' AND t_card.card_status = \'y\' AND t_cow.cow_status = \'y\'');
        }

        /*
         * EXPORT
         */
        if (isset($_GET['export']) && !empty($_GET['export'])) {
            $anggota->setLimit(0);
            $anggota->setPage(1);
        } else {
            $anggota->setPage($_GET['page']);
        }

        $anggota->setOrderBy('anggota_nama');
        if (isset($_GET['keyword']) && !empty($_GET['keyword'])) {
            $anggota->setKeyword($_GET['keyword']);
            $anggota->setSearchIn('anggota_nama');
        }

        $dataAnggota = $anggota->read();
        $anggotaList = '';
        $dataNya = array();
        //echo count($dataAnggota['records']);exit();
        foreach ($dataAnggota['records'] as $key => $val) {
            if ($anggotaList != '') {
                $anggotaList .= ',';
            }
            $anggotaList .= '\''.$val['anggota_id'].'\'';
            //array_push($dataNya,$val['anggota_id'])
            foreach ($arrayPeriode as $k => $v) {
                $dataNya[$val['anggota_id']][$v['periodedetail_name']] = $val;
                $dataNya[$val['anggota_id']][$v['periodedetail_name']]['hargadasar'] = 0;
                $dataNya[$val['anggota_id']][$v['periodedetail_name']]['hargafp'] = 0;
                $dataNya[$val['anggota_id']][$v['periodedetail_name']]['hargarez'] = 0;
                $dataNya[$val['anggota_id']][$v['periodedetail_name']]['qcparameter_cfu'] = 0;
                $dataNya[$val['anggota_id']][$v['periodedetail_name']]['qcparameter_ibc'] = 0;
                $dataNya[$val['anggota_id']][$v['periodedetail_name']]['qcparameter_fat'] = 0;
                $dataNya[$val['anggota_id']][$v['periodedetail_name']]['qcparameter_protein'] = 0;
                $dataNya[$val['anggota_id']][$v['periodedetail_name']]['qcparameter_lactose'] = 0;
                $dataNya[$val['anggota_id']][$v['periodedetail_name']]['qcparameter_ts'] = 0;
                $dataNya[$val['anggota_id']][$v['periodedetail_name']]['qcparameter_fp'] = 0;
                $dataNya[$val['anggota_id']][$v['periodedetail_name']]['qcparameter_acidity'] = 0;
                $dataNya[$val['anggota_id']][$v['periodedetail_name']]['qcparameter_urea'] = 0;
                $dataNya[$val['anggota_id']][$v['periodedetail_name']]['qcparameter_snf'] = 0;
            }
        }

        /* Formula */
       /*  $dataFormula = $milkpriceformula->setCondition('milkpriceformula_koperasi = \''.$koperasi.'\' AND milkpriceformula_status = \'y\' AND date_part(\'month\',milkpriceformula_effectivedate) <= \''.$_GET['month'].'\' AND date_part(\'year\',milkpriceformula_effectivedate) <= \''.$_GET['year'].'\'')
                                                ->setLimit(1)
                                                ->setOrderBy('milkpriceformula_effectivedate')
                                                ->setOrderType('ASC')
                                                ->read();

        $dataRezgrade = $rezgrade->setCondition('rezgrade_koperasi = \''.$koperasi.'\' AND date_part(\'month\',rezgrade_effectivedate) <= '.$_GET['month'].' AND date_part(\'year\',rezgrade_effectivedate) <= '.$_GET['year'])
                                 ->setLimit(1)
                                 ->setOrderBy('rezgrade_effectivedate')
                                 ->setOrderType('ASC')
                                 ->read();

        $dataFpgrade = $fpgrade->setCondition('fpgrade_koperasi = \''.$koperasi.'\' AND date_part(\'month\',fpgrade_effectivedate) <= '.$_GET['month'].' AND date_part(\'year\',fpgrade_effectivedate) <= '.$_GET['year'])
                                 ->setLimit(1)
                                 ->setOrderBy('fpgrade_effectivedate')
                                 ->setOrderType('ASC')
                                 ->read(); */
        $dataFormula = $milkpriceformula->setCondition('milkpriceformula_koperasi = \''.$koperasi.'\' AND milkpriceformula_status = \'y\' AND DATE(milkpriceformula_effectivedate) <= \''.$tglCekPeriode.'\'')
                                                ->setLimit(1)
                                                ->setOrderBy('milkpriceformula_effectivedate')
                                                ->setOrderType('DESC')
                                                ->read();

        $dataRezgrade = $rezgrade->setCondition('rezgrade_koperasi = \''.$koperasi.'\' AND DATE(rezgrade_effectivedate) <= \''.$tglCekPeriode.'\'')
                                 ->setLimit(1)
                                 ->setOrderBy('rezgrade_effectivedate')
                                 ->setOrderType('DESC')
                                 ->read();

        $dataFpgrade = $fpgrade->setCondition('fpgrade_koperasi = \''.$koperasi.'\' AND DATE(fpgrade_effectivedate) <= \''.$tglCekPeriode.'\'')
                                 ->setLimit(1)
                                 ->setOrderBy('fpgrade_effectivedate')
                                 ->setOrderType('DESC')
                                 ->read();

        if ($anggotaList != '') {
            /* Revisi 1.4: perhitungan periode berdasarkan tanggal transaksi */
            // $dataParameter = $qcparameter->setCondition('qcparameter_anggota IN ('.$anggotaList.') AND date_part(\'month\',t_transaksi.transaksi_waktu) = \''.$_GET['month'].'\' AND date_part(\'year\',t_transaksi.transaksi_waktu) = \''.$_GET['year'].'\' AND qcparameter_status = \'y\'')
            //                           ->setLimit(0)
            //                           ->read();
            /* End Revisi 1.4 */

            /* Revisi 1.5: perhitungan periode berdasarkan tanggal scan bacto */
            $dataParameter = $qcparameter->setCondition('qcparameter_anggota IN ('.$anggotaList.') AND date_part(\'month\',qcparameter_scandate) = \''.$_GET['month'].'\' AND date_part(\'year\',qcparameter_scandate) = \''.$_GET['year'].'\' AND qcparameter_status = \'y\'')
                                         ->setLimit(0)
                                         ->read();
            /* End Revisi 1.5*/

            /*
            * Fix double data
            **/
            $dataParameterFarmer = array();
            foreach ($dataParameter['records'] as $key => $val) {
                if (empty($val['qcparameter_scandate'])) {
                    $val['qcparameter_scandate'] = $val['transaksi_waktu'];
                }

                foreach ($arrayPeriode as $prk => $prv) {
                    if ($prv['periodedetail_startdate'] <= (int) date('j', strtotime($val['qcparameter_scandate'])) && $prv['periodedetail_enddate'] >= (int) date('j', strtotime($val['qcparameter_scandate']))) {
                        $val['transaksi_periode'] = $prv['periodedetail_name'];
                    }
                }
                if (!isset($dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']])) {
                    $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']] = $val;
                } else {
                    if ((float) $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_cfu'] <= 0) {
                        $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_cfu'] = $val['qcparameter_cfu'];
                    }
                    if ((float) $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_ibc'] <= 0) {
                        $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_ibc'] = $val['qcparameter_ibc'];
                    }
                    if ((float) $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_fat'] <= 0) {
                        $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_fat'] = $val['qcparameter_fat'];
                    }
                    if ((float) $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_protein'] <= 0) {
                        $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_protein'] = $val['qcparameter_protein'];
                    }
                    if ((float) $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_lactose'] <= 0) {
                        $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_lactose'] = $val['qcparameter_lactose'];
                    }
                    if ((float) $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_ts'] <= 0) {
                        $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_ts'] = $val['qcparameter_ts'];
                    }
                    if ((float) $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_fp'] <= 0) {
                        $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_fp'] = $val['qcparameter_fp'];
                    }
                    if ((float) $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_acidity'] <= 0) {
                        $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_acidity'] = $val['qcparameter_acidity'];
                    }
                    if ((float) $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_urea'] <= 0) {
                        $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_urea'] = $val['qcparameter_urea'];
                    }
                    if ((float) $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_snf'] <= 0) {
                        $dataParameterFarmer[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_snf'] = $val['qcparameter_snf'];
                    }
                }
            }

            foreach ($dataParameterFarmer as $pkey => $pval) {
                /* Revisi 1.6 */
                foreach ($pval as $key => $val) {
                    $hargadasar = (float) $dataFormula['records'][0]['milkpriceformula_hargadasar'];
                    $hargarez = 0;
                    $hargafp = 0;
                    $selisih = array();
                    $beda = 0;

                    /* Revisi 1.4: perhitungan periode berdasarkan tanggal transaksi */
                    /* Fix Empty Periode */
                    // if((int)$val['transaksi_periode'] < 1){
                    //  foreach($arrayPeriode as $prk=>$prv){
                    //      if($prv['periodedetail_startdate'] <= (int) date('j',strtotime($val['transaksi_waktu'])) && $prv['periodedetail_enddate'] >= (int) date('j',strtotime($val['transaksi_waktu']))){
                    //          $val['transaksi_periode'] = $prv['periodedetail_name'];
                    //      }

                    //  }
                    // }
                    /* End Revisi 1.4 */

                    /* Revisi 1.5: Ganti perhitungan periode berdasarkan tanggal scan bacto / milko */
                    /* Kalau scandate kosong gunakan transaksi_waktunya */

                    /* Revisi 1.6: Dihapus karena sudah di define di atas */

                    // if(empty($val['qcparameter_scandate'])){
                    //  $val['qcparameter_scandate'] = $val['transaksi_waktu'];
                    // }

                    // foreach($arrayPeriode as $prk=>$prv){
                    //  if($prv['periodedetail_startdate'] <= (int) date('j',strtotime($val['qcparameter_scandate'])) && $prv['periodedetail_enddate'] >= (int) date('j',strtotime($val['qcparameter_scandate']))){
                    //      $val['transaksi_periode'] = $prv['periodedetail_name'];
                    //  }

                    // }
                    /* End Revisi 1.6 */
                    /* END Revisi 1.5 */

                    foreach ($dataFormula['records'][0]['milkpricecondition'] as $k => $v) {
                        if ($v['milkpricecondition_standardvalue'] != $val['qcparameter_'.$v['milkpricecondition_element']]) {
                            ++$beda;

                            if ($v['milkpricecondition_korelasi'] == 'p') {
                                if ($val['qcparameter_'.$v['milkpricecondition_element']] > $v['milkpricecondition_standardvalue']) {
                                    $pembagi = (float) ((float) $val['qcparameter_'.$v['milkpricecondition_element']] - (float) $v['milkpricecondition_standardvalue']) / (float) $v['milkpricecondition_interval'];
                                    $selisih[$v['milkpricecondition_element']] = ($pembagi * $v['milkpricecondition_addprice']);
                                } else {
                                    $pembagi = (float) ((float) $v['milkpricecondition_standardvalue'] - (float) $val['qcparameter_'.$v['milkpricecondition_element']])  / (float) $v['milkpricecondition_interval'];
                                    $selisih[$v['milkpricecondition_element']] = -($pembagi * $v['milkpricecondition_addprice']);
                                }
                            } else {
                                if ($val['qcparameter_'.$v['milkpricecondition_element']] > $v['milkpricecondition_standardvalue']) {
                                    $pembagi = (float) ((float) $val['qcparameter_'.$v['milkpricecondition_element']] - (float) $v['milkpricecondition_standardvalue']) / (float) $v['milkpricecondition_interval'];
                                    $selisih[$v['milkpricecondition_element']] = -($pembagi * $v['milkpricecondition_addprice']);
                                } else {
                                    $pembagi = (float) ((float) $v['milkpricecondition_standardvalue'] - (float) $val['qcparameter_'.$v['milkpricecondition_element']])  / (float) $v['milkpricecondition_interval'];
                                    $selisih[$v['milkpricecondition_element']] = ($pembagi * $v['milkpricecondition_addprice']);
                                }
                            }
                        } else {
                            $selisih[$v['milkpricecondition_element']] = 0;
                        }
                    }
                    if ($beda === 0) {
                        //return number_format($hargadasar,2);
                    } else {
                        foreach ($selisih as $k => $v) {
                            $hargadasar = (float) ($hargadasar + $v);
                        }
                    }

                    $currentRezGrade = 0;
                    $currentRezValue = ($val['qcparameter_cfu'] * 1000); // Dikali 1000 karena output dari bactoscan dikali 1000

                    foreach ($dataRezgrade['records'][0]['rezgradedetail'] as $rk => $rv) {
                        if ($rv['rezgradedetail_tpcstart'] <= $currentRezValue && $rv['rezgradedetail_tpcend'] >= $currentRezValue) {
                            $currentRezGrade = $rv['rezgradedetail_grade'];
                            $hargarez = $rv['rezgradedetail_price'];
                        }
                    }

                    $currentFpGrade = 0;
                    $currentFpValue = $val['qcparameter_fp'];

                    foreach ($dataFpgrade['records'][0]['fpgradedetail'] as $fk => $fv) {
                        if ($fv['fpgradedetail_fpstart'] <= $currentFpValue && $fv['fpgradedetail_fpend'] >= $currentFpValue) {
                            $currentFpGrade = $fv['fpgradedetail_grade'];
                            $hargafp = $fv['fpgradedetail_price'];
                        }
                    }

                    $dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_cfu'] = $val['qcparameter_cfu'];
                    $dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_ibc'] = $val['qcparameter_ibc'];
                    $dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_fat'] = $val['qcparameter_fat'];
                    $dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_protein'] = $val['qcparameter_protein'];
                    $dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_lactose'] = $val['qcparameter_lactose'];
                    $dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_ts'] = $val['qcparameter_ts'];
                    $dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_fp'] = $val['qcparameter_fp'];
                    $dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_acidity'] = $val['qcparameter_acidity'];
                    $dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_urea'] = $val['qcparameter_urea'];
                    $dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['qcparameter_snf'] = $val['qcparameter_snf'];
                    $dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['hargadasar'] = $hargadasar;
                    $dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['hargafp'] = $hargafp;
                    $dataNya[$val['qcparameter_anggota']][$val['transaksi_periode']]['hargarez'] = $hargarez;
                }
            }
        }

        $res = $dataAnggota;
        $res['records'] = $dataNya;
        $data['data'] = $res;
        $data['periode'] = $dtPeriode;

        // echo "<pre>";
        // print_r($dataParameter);
        // echo "</pre>";

        /*
         * EXPORT TO EXCEL
         */
        if (isset($_GET['export']) && !empty($_GET['export'])) {
            $header = array(
                'PERIODE',
                'FARMER ID',
                'NAME',
                'TPC',
                'FAT',
                'PROTEIN',
                'LACTOSE',
                'TS',
                'FP',
                'ACIDITY',
                'UREA',
                'SNF',
                'STANDARD PRICE',
                'REZ PENALTY',
                'FP PENALTY',
                'MILK PRICE',
                );
            $xlsrow = array();
            foreach ($dtPeriode['records'][0]['periodedetail'] as $key => $val) {
                foreach ($res['records'] as $k => $v) {
                    $row = '';
                    $row .= $val['periodedetail_name']."\t";
                    $row .= $v[$val['periodedetail_name']]['anggota_nomor']."\t";
                    $row .= $v[$val['periodedetail_name']]['anggota_nama']."\t";
                    $row .= ((float) $v[$val['periodedetail_name']]['qcparameter_cfu'])."\t";
                    $row .= ((float) $v[$val['periodedetail_name']]['qcparameter_fat'])."\t";
                    $row .= ((float) $v[$val['periodedetail_name']]['qcparameter_protein'])."\t";
                    $row .= ((float) $v[$val['periodedetail_name']]['qcparameter_lactose'])."\t";
                    $row .= ((float) $v[$val['periodedetail_name']]['qcparameter_ts'])."\t";
                    $row .= ((float) $v[$val['periodedetail_name']]['qcparameter_fp'])."\t";
                    $row .= ((float) $v[$val['periodedetail_name']]['qcparameter_acidity'])."\t";
                    $row .= ((float) $v[$val['periodedetail_name']]['qcparameter_urea'])."\t";
                    $row .= ((float) $v[$val['periodedetail_name']]['qcparameter_snf'])."\t";
                    $row .= round($v[$val['periodedetail_name']]['hargadasar'])."\t";
                    $row .= round($v[$val['periodedetail_name']]['hargarez'])."\t";
                    $row .= round($v[$val['periodedetail_name']]['hargafp'])."\t";
                    $row .= round((float) ($v[$val['periodedetail_name']]['hargadasar'] + $v[$val['periodedetail_name']]['hargarez'] + $v[$val['periodedetail_name']]['hargafp']));
                    $xlsrow[] = $row;
                }
            }

            $filename = 'MilkPrice-'.$_GET['year'].'-'.$_GET['month'].'.xls';

            header("Content-Disposition: attachment; filename=\"$filename\"");
            header('Content-Type: application/vnd.ms-excel');
            echo implode("\t", $header)."\n";
            echo implode("\n", $xlsrow);
            exit();
        }

        if (!empty($_GET['anggota'])) {
            $data['prepopulate'][] = array(
                        'id' => $_GET['anggota'],
                        'name' => $dataAnggota['records'][0]['anggota_nama'],
                    );
        } else {
            $data['prepopulate'] = array();
        }

        //custom query start

        $coba = array();
        $coba2 = array();
        foreach ($dtPeriode['records']['0']['periodedetail'] as $key => $value) {
            $key = $key + 1;
            $myQuery = Db::recordList('t_transaksi', '*', "LEFT JOIN t_qcparameter ON t_transaksi.transaksi_anggota = t_qcparameter.qcparameter_anggota LEFT JOIN t_rezgrade ON t_transaksi.transaksi_koperasi = t_rezgrade.rezgrade_koperasi LEFT JOIN t_rezgradedetail ON t_rezgrade.rezgrade_id=t_rezgradedetail.rezgradedetail_rezgrade WHERE t_transaksi.transaksi_anggota = '".$_GET['anggota']."' AND date_part('month',t_transaksi.transaksi_waktu) = '".$_GET['month']."' AND t_transaksi.transaksi_status = 'y' AND t_qcparameter.qcparameter_status = 'y' AND date_part('day', t_qcparameter.qcparameter_bactoscandate)>='".$value['periodedetail_startdate']."' AND date_part('day', t_qcparameter.qcparameter_bactoscandate)<='".$value['periodedetail_enddate']."' AND date_part('day', t_transaksi.transaksi_waktu)>='".$value['periodedetail_startdate']."' AND date_part('day', t_transaksi.transaksi_waktu)<='".$value['periodedetail_enddate']."' ORDER BY t_transaksi.transaksi_waktu ASC");
            $coba[$key] = $myQuery;

            // $myQuery2 = Db::recordList('');
            $coba2[$key] = $myQuery;
        }

        $arr = array();
        $arr2 = array();
        $totalberat = array();
        $tanggalperiode = array();
        foreach ($coba as $key => $value) {
            foreach ($value as $k => $v) {
                $tanggal = date('Y-m-d', strtotime($v['transaksi_waktu']));
                $arr[$tanggal][$v['transaksi_sesi']] = $v;
                $arr2[$key] = $v;
                $tanggalperiode[$tanggal] = $key;
            }
        }

        // $arr2 = array();
        foreach ($arr as $key => $value) {
            foreach ($value as $k => $v) {
                $tanggal = date('Y-m-d', strtotime($v['transaksi_waktu']));
                // $arr2[$tanggal][$v['transaksi_sesi']] = $v;
                $totalberat[$tanggalperiode[$key]]['totalberat'] += $v['transaksi_totalberat'];
            }
        }
        $data['arr'] = $arr;
        // echo "<pre>";
        // print_r($arr2);
        // echo "</pre>";
        //custom query end

        // echo "<pre>";
        // print_r($dataFormula['records'][0]['milkpricecondition']);
        $fatsnf = array();
        foreach ($dataFormula['records'] as $key => $value) {
            foreach ($value['milkpricecondition'] as $k => $v) {
                $fatsnf[$k][$v['milkpricecondition_element']] = $v;
            }
        }
        // print_r($tanggalperiode);
        $fat_standart = $fatsnf[0]['fat']['milkpricecondition_standardvalue'];
        $fat_additional = $fatsnf[0]['fat']['milkpricecondition_addprice'];
        $snf_standart = $fatsnf[1]['snf']['milkpricecondition_standardvalue'];
        $snf_additional = $fatsnf[1]['snf']['milkpricecondition_addprice'];

        $data['totalberat'] = $totalberat;
        //harga standar
        $data['hargadasar'] = $dataFormula['records'][0]['milkpriceformula_hargadasar'];

        $data['perperiode'] = $arr2;

        $i = 0;
        foreach ($arr2 as $key => $value) {
            ++$i;
            $data['bonus_fat_p'.$i] = (($value['qcparameter_fat'] - $fat_standart) * 10) * $fat_additional;
        }

        $i = 0;
        foreach ($arr2 as $key => $value) {
            ++$i;
            $data['bonus_snf_p'.$i] = (($value['qcparameter_snf'] - $snf_standart) * 10) * $snf_additional;
        }

        $tcp = array();
        $i = 0;
        foreach ($arr2 as $key => $value) {
            ++$i;
            $tcp[$key] = $value['qcparameter_cfu'] * 1000;
        }

        // echo "<pre>";
        // print_r($tcp);
        // echo "</pre>";
        $i = 0;
        foreach ($tcp as $key => $value) {
            ++$i;
            $tpcs = Db::recordList('t_rezgradedetail', '*', "WHERE rezgradedetail_tpcstart<='".$value."' AND rezgradedetail_tpcend>='".$value."'");
            foreach ($tpcs as $key => $value) {
                $data['bonus_res_p'.$i] = $value['rezgradedetail_price'];
                $data['resazur'] = $value;
            }
        }

        $data['jumlah_periode'] = count($tcp);

        // for ($i=0; $i < count($tcp); $i++) {
        //     $tpcs = Db::recordList('t_rezgradedetail', '*', "WHERE rezgradedetail_tpcstart<='".$tcp[$i]."' AND rezgradedetail_tpcend>='".$tcp[$i]."'");
        //     foreach ($tpcs as $key => $value) {
        //         $i++;
        //         $data['bonus_res_p'.$i] = $value;
        //     }
        // }

        // echo "<pre>";
        // print_r($data['bonus_res_p2']);
        // echo "</pre>";

        View::display('report/report.pendapatan', $data);
    }
}
