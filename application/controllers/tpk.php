<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 12/24/2014
 * Time: 8:01 AM
 */
class Tpk_Controller extends Controller{
    public $version = 1.1;
    public $description = 'TPK Setting (MCP)';
    public $buildConfiguration = array(
        'views'=>array(
            'tpk.setting'
        ),
        'models'=>array(
            'tpk'
        ),
        'table'=>'tpk'
    );
    function __construct(){
        security::checkSession();
    }
    function index(){

    }

    function setting($operation='',$id=''){

        session::set('title','Collection Point Settings');
        load::library('fn');
        $tpk = load::model('tpk');
        $repo = load::model('repo');
        switch($operation){
            case 'save':

                if(isset($_POST['tpk_id'])){
                    $tpk->setId($_POST['tpk_id']);
                    $tpk->setDataArray($_POST);
                    $query = $tpk->update();
                }else{
                    $_POST['tpk_id'] = fn::generate_id(true);
                    $tpk->setCondition('tpk_koperasi = \''.$_POST['tpk_koperasi'].'\'');
                    $tpk->setLimit(1);
                    $tpk->setOrderBy('tpk_kode');
                    $tpk->setOrderType('DESC');
                    $cek = $tpk->read();
                    if($cek['total_data'] > 0){
                        $tpkkode = (int) $cek['records'][0]['tpk_kode'];
                        $newtpkkode = ($tpkkode + 1);
                        $_POST['tpk_kode'] = fn::displayZeroNumber($newtpkkode,3);
                    }else{
                        $_POST['tpk_kode'] = '001';
                    }
                    $tpk->setDataArray($_POST);
                    $query = $tpk->insert();
                }
                if($query){
                    $repo->updateRepo('koperasi');
                    session::set('errorMsg',_('Data has been successfully saved'));
                }else{
                    session::set('errorMsg',_('Failed while saving data into database'));
                }
                header('location:'.BASE_URL.'index.php?/tpk/setting');

                break;
            case 'edit':
                if(!empty($id)){
                    if(!in_array(session::get('user_group'),Config::$application['super_group'])){
                        $tpk->setCondition('tpk_id = \''.$id.'\' AND tpk_koperasi = \''.session::get('user_koperasi').'\'');
                        $tpk->setLimit(1);
                        $dept = $tpk->read();
                        echo json_encode($dept['records'][0]);
                    }else{
                        $tpk->setId($id);
                        $dept = $tpk->read();
                        echo json_encode($dept['records']);
                    }
                }

                break;
            case 'delete':
                if(isset($_POST['tpk_id'])){
                    if(!in_array(session::get('user_group'),Config::$application['super_group'])){
                        //$koperasi->setCondition('koperasi_id = \''.$_POST['koperasi_id'].'\' AND koperasi_id = \''.session::get('user_koperasi').'\'');
                        $tpk->setCondition('tpk_id = \''.$_POST['tpk_id'].'\' AND tpk_koperasi = \''.session::get('user_koperasi').'\'');
                    }else{
                        $tpk->setId($_POST['tpk_id']);
                    }
                    if($tpk->delete()){
                        $repo->updateRepo('koperasi');
                        session::set('errorMsg',_('Data has been successfully deleted'));
                    }else{
                        session::set('errorMsg',_('Failed while deleting data'));
                    }
                }
                header('location:'.BASE_URL.'index.php?/tpk/setting');
                break;
            default:

                $_GET['page'] = !isset($_GET['page']) ? 1 : $_GET['page'];
                $koperasi = load::model('koperasi');
                $tpk->setPage($_GET['page']);
                if(isset($_GET['keyword'])){
                    $tpk->setKeyword($_GET['keyword']);
                    $tpk->setSearchIn(array(
                        'tpk_kode',
                        'tpk_nama',
                        'tpk_alamat',
                        't_koperasi.koperasi_nama'
                    ));
                }
                $tpk->setLimit(Config::$application['display_limit']);
                if(!in_array(session::get('user_group'),Config::$application['super_group'])){
                    $_GET['koperasi'] = session::get('user_koperasi');
                    $tpk->setCondition('tpk_koperasi = \''.session::get('user_koperasi').'\' AND tpk_status != \'d\'');
                    $data['koperasi'] = $koperasi->setCondition('koperasi_id = \''.session::get('user_koperasi').'\'')->read();
                }else{
                    $_GET['koperasi'] = !isset($_GET['koperasi']) ? '' : $_GET['koperasi'];
                    if(!empty($_GET['koperasi'])){
                        $tpk->setCondition('tpk_status != \'d\' AND tpk_koperasi = \''.$_GET['koperasi'].'\'');
                    }else{
                        $tpk->setCondition('tpk_status != \'d\'');
                    }
                    $data['koperasi'] = $koperasi->setCondition('koperasi_status = \'y\'')->setLimit(0)->setOrderBy('koperasi_nama')->read();
                }

                $data['tpk'] = $tpk->read();
                View::display('tpk/tpk.setting',$data);

                break;
        }
    }

    function load(){
        $dt = load::model('tpk');
        $page = !isset($_GET['page']) ? 1 : $_GET['page'];
        $dt->setPage($page);
        if(isset($_GET['keyword'])){
            $dt->setKeyword($_GET['keyword']);
            $dt->setSearchIn(array(
                'tpk_kode',
                'tpk_nama'
            ));
        }
        $dt->setLimit(Config::$application['display_limit']);
        if(!in_array(session::get('user_group'),Config::$application['super_group'])){
            $dt->setCondition('tpk_koperasi = \''.session::get('user_koperasi').'\' AND tpk_status = \'y\'');
        }else{
            $dt->setCondition('tpk_status = \'y\'');
        }
        $dt->setOrderBy('tpk_nama');
        $data = $dt->read();
        echo json_encode($data);
    }
    function findByKoperasi($id=''){
        if(!empty($id)){
            $dt = load::model('tpk');
            $dt->setCondition('tpk_status = \'y\' AND tpk_koperasi = \''.$id.'\'');
            $dt->setOrderBy('tpk_nama');
            $data = $dt->read();
            echo json_encode($data);
        }

    }
    function loadAll(){
        $dt = load::model('tpk');
        $dt->setLimit(0);
        if(!in_array(session::get('user_group'),Config::$application['super_group'])){
            $dt->setCondition('tpk_koperasi = \''.session::get('user_koperasi').'\' AND tpk_status = \'y\'');
        }else{
            $dt->setCondition('tpk_status = \'y\'');
        }
        $dt->setOrderBy('t_koperasi.koperasi_nama');
        $data = $dt->read();
        echo json_encode($data);
    }

}
