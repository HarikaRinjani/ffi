<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 12/27/2014
 * Time: 3:05 PM
 */
class Kelompokharga_Controller extends Controller{
    public $buildConfiguration = array(
        'table'=>'kelompokharga',
        'views'=>array(
            'kelompokharga.setting'
        ),
        'models'=>array(
            'kelompokharga'
        )
    );
    public $description = 'Setting Kelompok Harga (MCP)';
    // Kelompokharga membutuhkan package kelompok
    public $dependencies = array(
        array(
            'name'=>'kelompok',
            'version'=>1
        )
    );
    function __construct(){
        parent::__construct();
        security::checkSession();
    }
    function index(){

    }
    function setting($operation='',$id=''){
        security::require_role(array(
            'administrator'
        ));
        session::set('title','Kelompok Harga Settings');
        load::library('fn');
        $dt = load::model('kelompokharga');
        switch($operation){
            case 'save':
                if(isset($_POST['kelompokharga_id'])){
                    $dt->setId($_POST['kelompokharga_id']);
                    $dt->setDataArray($_POST);
                    $query = $dt->update();
                }else{
                    $_POST['kelompokharga_id'] = fn::generate_id(true);
                    $dt->setDataArray($_POST);
                    $query = $dt->insert();
                }
                if($query){
                    session::set('errorMsg',_('Data has been successfully saved'));
                }else{
                    session::set('errorMsg',_('Failed while saving data into database'));
                }
                header('location:'.BASE_URL.'index.php?/kelompokharga/setting');

                break;
            case 'edit':
                if(!empty($id)){
                    $dt->setId($id);
                    $dept = $dt->read();
                    echo json_encode($dept['records']);
                }

                break;
            case 'delete':
                if(isset($_POST['kelompokharga_id'])){
                    $dt->setId($_POST['kelompokharga_id']);
                    if($dt->delete()){
                        session::set('errorMsg',_('Data has been successfully deleted'));
                    }else{
                        session::set('errorMsg',_('Failed while deleting data'));
                    }
                }
                header('location:'.BASE_URL.'index.php?/kelompokharga/setting');

                break;
            default:

                $_GET['page'] = !isset($_GET['page']) ? 1 : $_GET['page'];
                
                $dt->setPage($_GET['page']);
                $dt->setLimit(Config::$application['display_limit']);
                if(isset($_GET['keyword'])){
                    $dt->setKeyword($_GET['keyword']);
                    $dt->setSearchIn(array(
                        't_kelompokharga.kelompokharga_kode',
                        't_kelompokharga.kelompokharga_nama',
                        't_kelompok.kelompok_nama',
                        't_tpk.tpk_nama'
                    ));
                }
                $dt->setOrderBy('kelompokharga_nama');
                $data['kelompokharga'] = $dt->read();

                View::display('kelompokharga/kelompokharga.setting',$data);

                break;
        }
    }

    function findByTPK($tpk_id=''){
        $dt = load::model('kelompokharga');
        $page = !isset($_GET['page']) ? 1 : $_GET['page'];
        $dt->setPage($page);
        $dt->setCondition('t_kelompokharga.kelompokharga_status = \'y\' AND t_kelompokharga.kelompokharga_tpk = \''.$tpk_id.'\'');
        $dt->setLimit(1000);
        $dt->setOrderBy('kelompokharga_nama');
        $data = $dt->read();
        echo json_encode($data);
    }

    function findByKelompok($kelompok_id=''){
        $dt = load::model('kelompokharga');
        $page = !isset($_GET['page']) ? 1 : $_GET['page'];
        $dt->setPage($page);
        $dt->setCondition('t_kelompokharga.kelompokharga_status = \'y\' AND t_kelompokharga.kelompokharga_kelompok = \''.$kelompok_id.'\'');
        $dt->setLimit(1000);
        $dt->setOrderBy('kelompokharga_nama');
        $data = $dt->read();
        echo json_encode($data);
    }
}