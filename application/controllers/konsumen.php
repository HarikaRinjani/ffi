<?php
/**
 * Created by PhpStorm.
 * User: PrastowoaGung
 * Date: 12/30/2014
 * Time: 9:32 PM
 */
class Konsumen_Controller extends Controller{
    public $buildConfiguration = array(
        'table'=>'konsumen',
        'models'=>'konsumen',
        'views'=>array(
            'konsumen.setting'
        )
    );
    public $description = 'Customer Module (MCP)';
    public $version = 1.1;
    function __construct(){
        security::checkSession();
    }
    function setting($operation='',$id=''){
        session::set('title','Customer Settings');
        load::library('fn');
        $konsumen = load::model('konsumen');
        switch($operation){
            case 'save':
                if(isset($_POST['konsumen_id'])){
                    $konsumen->setId($_POST['konsumen_id']);
                    $konsumen->setDataArray($_POST);
                    $query = $konsumen->update();
                }else{
                    $_POST['konsumen_id'] = fn::generate_id(true);
                    $konsumen->setDataArray($_POST);
                    $query = $konsumen->insert();
                }
                if($query){
                    session::set('errorMsg',_('Data has been successfully saved'));
                }else{
                    session::set('errorMsg',_('Failed while saving data into database'));
                }
                header('location:'.BASE_URL.'index.php?/konsumen/setting');

                break;
            case 'edit':
                if(!empty($id)){
                    $konsumen->setId($id);
                    $dept = $konsumen->read();
                    echo json_encode($dept['records']);
                }

                break;
            case 'delete':
                if(isset($_POST['konsumen_id'])){
                    $konsumen->setId($_POST['konsumen_id']);
                    if($konsumen->delete()){
                        session::set('errorMsg',_('Data has been successfully deleted'));
                    }else{
                        session::set('errorMsg',_('Failed while deleting data'));
                    }
                }
                header('location:'.BASE_URL.'index.php?/konsumen/setting');

                break;
            default:
                $koperasi = load::model('koperasi');
                $_GET['page'] = !isset($_GET['page']) ? 1 : $_GET['page'];
                $konsumen->setPage($_GET['page']);
                if(isset($_GET['keyword'])){
                    $konsumen->setKeyword($_GET['keyword']);
                    $konsumen->setSearchIn(array(
                        'konsumen_nama',
                        'konsumen_telp',
                        'konsumen_alamat',
                        't_koperasi.koperasi_nama'
                        ));
                }

                if(!in_array(session::get('user_group'),Config::$application['super_group'])){
                    $_GET['koperasi'] = session::get('user_koeprasi');
                    $konsumen->setCondition('konsumen_koperasi = \''.session::get('user_koperasi').'\' AND konsumen_status != \'d\'');
                    $data['koperasi'] = $koperasi->setCondition('koperasi_id = \''.session::get('user_koperasi').'\'')->read();
                }else{
                    $_GET['koperasi'] = !isset($_GET['koperasi']) ? '' : $koperasi->escapeKeyword($_GET['koperasi']);
                    $addSQL = '';
                    if(!empty($_GET['koperasi'])){
                        $addSQL = ' AND konsumen_koperasi = \''.$_GET['koperasi'].'\'';
                    }
                    $konsumen->setCondition('konsumen_status != \'d\''.$addSQL);
                    $data['koperasi'] = $koperasi->setCondition('koperasi_status = \'y\'')->setLimit(0)->setOrderBy('koperasi_nama')->read();
                }
                
                $konsumen->setOrderBy('konsumen_nama');
                $data['konsumen'] = $konsumen->read();

                View::display('konsumen/konsumen.setting',$data);

                break;
        }
    }
}
