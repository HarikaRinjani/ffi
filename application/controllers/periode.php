<?php
class Periode_Controller extends Controller{
	public $buildConfiguration = array(
        'table'=>array(
            'periode',
            'periodedetail'
            )
        );
	function __construct(){
		security::checkSession();
	}

	function index(){

	}
	function setting($operation='',$id=''){
        session::set('title','Periode Settings');
        load::library('fn');
        $periode = load::model('periode');
        $periodedetail = load::model('periodedetail');
        switch($operation){
            case 'save':
            	$_POST['periode_effectivedate'] = date('Y-m-d',fn::dateToUnix($_POST['periode_effectivedate']));
                if(isset($_POST['periode_id'])){
                    $periode->setId($_POST['periode_id']);
                    $periode->setDataArray($_POST);
                    $query = $periode->update();
                    if($query){
                        $periodedetail->setCondition('periodedetail_periode = \''.$_POST['periode_id'].'\'');
                        $periodedetail->delete();
                        foreach($_POST['periodedetail_check'] as $k=>$v){
                            if(!empty($_POST['periodedetail_startdate'][$k]) && !empty($_POST['periodedetail_enddate'][$k]) && !empty($_POST['periodedetail_name'][$k])){
                                $detail['periodedetail_id'] = fn::generate_id(true);
                                $detail['periodedetail_name'] = $_POST['periodedetail_name'][$k];
                                $detail['periodedetail_startdate'] = $_POST['periodedetail_startdate'][$k];
                                $detail['periodedetail_enddate'] = $_POST['periodedetail_enddate'][$k];
                                $detail['periodedetail_periode'] = $_POST['periode_id'];
                                $periodedetail->setDataArray($detail);
                                $periodedetail->insert();
                            }
                        }
                    }
                }else{
                    $_POST['periode_id'] = fn::generate_id(true);
                    $periode->setDataArray($_POST);
                    $query = $periode->insert();
                    if($query){
                        foreach($_POST['periodedetail_check'] as $k=>$v){
                            if(!empty($_POST['periodedetail_startdate'][$k]) && !empty($_POST['periodedetail_enddate'][$k]) && !empty($_POST['periodedetail_name'][$k])){
                                $detail['periodedetail_id'] = fn::generate_id(true);
                                $detail['periodedetail_name'] = $_POST['periodedetail_name'][$k];
                                $detail['periodedetail_startdate'] = $_POST['periodedetail_startdate'][$k];
                                $detail['periodedetail_enddate'] = $_POST['periodedetail_enddate'][$k];
                                $detail['periodedetail_periode'] = $_POST['periode_id'];
                                $periodedetail->setDataArray($detail);
                                $periodedetail->insert();
                            }
                        }
                    }
                }
                if($query){
                    session::set('errorMsg',_('Data has been successfully saved'));
                }else{
                    session::set('errorMsg',_('Failed while saving data into database'));
                }
                header('location:'.BASE_URL.'index.php?/periode/setting');

                break;
            case 'edit':
                if(!empty($id)){

                    if(!in_array(session::get('user_group'),Config::$application['super_group'])){
                        $periode->setCondition('periode_koperasi = \''.session::get('user_koperasi').'\' AND periode_id = \''.$id.'\'');
                        $periode->setLimit(1);
                        $dept = $periode->read();
                        echo json_encode($dept['records'][0]);
                    }else{
                        $periode->setId($id);
                        $dept = $periode->read();
                        echo json_encode($dept['records']);
                    }
                }

                break;
            case 'delete':
                if(isset($_POST['periode_id'])){
                    if(!in_array(session::get('user_group'),Config::$application['super_group'])){
                        $periode->setCondition('periode_koperasi = \''.session::get('user_koperasi').'\' AND periode_id = \''.$_POST['periode_id'].'\'');
                    }else{
                        $periode->setId($_POST['periode_id']);
                    }

                    if($periode->delete()){
                        session::set('errorMsg',_('Data has been successfully deleted'));
                    }else{
                        session::set('errorMsg',_('Failed while deleting data'));
                    }
                }
                header('location:'.BASE_URL.'index.php?/periode/setting');

                break;
            default:

                $_GET['page'] = !isset($_GET['page']) ? 1 : $_GET['page'];
                $periode->setPage($_GET['page']);
                if(isset($_GET['keyword'])){
                    $periode->setKeyword($_GET['keyword']);
                    $periode->setSearchIn('periode_effectivedate');
                }
                $periode->setLimit(Config::$application['display_limit']);

                if(!in_array(session::get('user_group'),Config::$application['super_group'])){
                    $periode->setCondition('periode_koperasi = \''.session::get('user_koperasi').'\' AND periode_status != \'d\'');
                }else{
                    $periode->setCondition('periode_status != \'d\'');
                }
                $periode->setOrderBy('periode_effectivedate');
                $periode->setOrderType('DESC');
                $data['periode'] = $periode->read();
                $koperasi = load::model('koperasi');
                if(!in_array(session::get('user_group'),Config::$application['super_group'])){
                    $koperasi->setCondition('koperasi_id = \''.session::get('user_koperasi').'\' AND koperasi_status = \'y\'');
                }else{
                    $koperasi->setCondition('koperasi_status = \'y\'');
                }

                $koperasi->setLimit(1000);
                $koperasi->setOrderBy('koperasi_nama');
                $data['koperasi'] = $koperasi->read();
				
                View::display('periode/periode.setting',$data);

                break;
        }
    }
}
