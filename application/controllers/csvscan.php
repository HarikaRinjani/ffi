<?php
class Csvscan_Controller extends Controller{
	public $version = 1.8;
	public $description = "v1.8 - Fix Missing First Line Data\n\nv1.7 - Fix empty additional data (coop, collection point, etc)\n\nv1.6 - Trim empty value\n\nv1.5 - Fix empty transaction id\n\nv1.4 - Fix Empty SNF\n\nv1.3 - Change folder scan method to lowercase filename \n\nv1.2\n- Change Milkoscan FT120 Folder path\n\nv1.1\n- Add compability to check milkoscan ft120 folder\n\n";
	public $buildConfiguration = array(
		'table'  => 'qcparameter',
		'models' => 'qcparameter'
	);
	function index(){
		load::library('parseCSV');

		$csv       = new parseCSV();
		$path      = Config::$application['csv_path'];
		$milkoscan = array();
		$dt        = load::model('qcparameter');
		$log       = load::model('csvlog');
		$transaksi = load::model('transaksi');
		// DEFINE ARRAY DATA
		$milkoscan_fields = array(
			'id'         => array('key'=>'','value'=>''),
			'product'    => array('key'=>'','value'=>''),
			'#sub'       => array('key'=>'','value'=>''),
			'date'       => array('key'=>'','value'=>''),
			'time'       => array('key'=>'','value'=>''),
			'fat'        => array('key'=>'','value'=>''),
			'protein'    => array('key'=>'','value'=>''),
			'lactose'    => array('key'=>'','value'=>''),
			'ts'         => array('key'=>'','value'=>''),
			'fpd'        => array('key'=>'','value'=>''),
			'acidity'    => array('key'=>'','value'=>''),
			'urea'       => array('key'=>'','value'=>''),
			'resulttype' => array('key'=>'','value'=>''),
			'bottletype' => array('key'=>'','value'=>''),
			'remark'     => array('key'=>'','value'=>'')
		);

		$bactoscan_fields = array(
			'cfu'  => array('key'=>'','value'=>''),
			'ibc'  => array('key'=>'','value'=>''),
			'date' => array('key'=>'','value'=>''),
			'time' => array('key'=>'','value'=>'')
		);
		while(1){
			echo "Starting Process\n";
			$tahun = date('Y');
			$bulan = date('n');
	
			if(!file_exists($path.DS.'archive'.DS.'bactoscan'.DS.$tahun)){
				@mkdir($path.DS.'archive'.DS.'bactoscan'.DS.$tahun,0777);
			}
			if(!file_exists($path.DS.'archive'.DS.'bactoscan'.$tahun.DS.$bulan)){
				@mkdir($path.DS.'archive'.DS.'bactoscan'.DS.$tahun.DS.$bulan,0777);
			}
			if(!file_exists($path.DS.'archive'.DS.'bactoscan'.$tahun.DS.$bulan.DS.'notfound')){
				@mkdir($path.DS.'archive'.DS.'bactoscan'.DS.$tahun.DS.$bulan.DS.'notfound',0777);
			}
			if(!file_exists($path.DS.'archive'.DS.'milkoscan'.DS.$tahun)){
				@mkdir($path.DS.'archive'.DS.'milkoscan'.DS.$tahun,0777);
			}
			if(!file_exists($path.DS.'archive'.DS.'milkoscan'.$tahun.DS.$bulan)){
				@mkdir($path.DS.'archive'.DS.'milkoscan'.DS.$tahun.DS.$bulan,0777);
			}
			if(!file_exists($path.DS.'archive'.DS.'milkoscan'.$tahun.DS.$bulan.DS.'notfound')){
				@mkdir($path.DS.'archive'.DS.'milkoscan'.DS.$tahun.DS.$bulan.DS.'notfound',0777);
			}
			/** Milko FT 120 **/
			if(!file_exists($path.DS.'archive'.DS.'ffimilkoscanft120')){
				@mkdir($path.DS.'archive'.DS.'ffimilkoscanft120',0777);
			}
			if(!file_exists($path.DS.'archive'.DS.'ffimilkoscanft120'.DS.$tahun)){
				@mkdir($path.DS.'archive'.DS.'ffimilkoscanft120'.DS.$tahun,0777);
			}
			if(!file_exists($path.DS.'archive'.DS.'ffimilkoscanft120'.$tahun.DS.$bulan)){
				@mkdir($path.DS.'archive'.DS.'ffimilkoscanft120'.DS.$tahun.DS.$bulan,0777);
			}
			if(!file_exists($path.DS.'archive'.DS.'ffimilkoscanft120'.$tahun.DS.$bulan.DS.'notfound')){
				@mkdir($path.DS.'archive'.DS.'ffimilkoscanft120'.DS.$tahun.DS.$bulan.DS.'notfound',0777);
			}

			echo "Processing Bactoscan Folder\n";
			//$bactoscanFiles = glob($path.DS.'bactoscan'.DS.'*.csv');
			foreach (glob($path.DS.'bactoscan'.DS.'*.csv') as $filename) {
				$tipe = substr($filename,-5,5);
				$file = substr($filename,-20,20);
				$logdata = array();
				//usleep(1000000);
				if($tipe == '0.csv'){
					rename($filename,$path.DS.'archive'.DS.'bactoscan'.DS.$tahun.DS.$bulan.DS.$file);
					$logdata['csvlog_id']      = $this->getToken(32);
					$logdata['csvlog_date']    = date('Y-m-d H:i:s');
					$logdata['csvlog_content'] = 'Moving '.$file.' to '.$path.DS.'archive'.DS.'bactoscan'.DS.$tahun.DS.$bulan;
					$log->setDataArray($logdata);
					$log->insert();

				}else{
					$bactoscan = array();
					$csv->delimiter = ",";
					$csv->parse($filename);
					$bactoscan[0] = $csv->data;
					//print_r($bactoscan);exit();
					foreach($bactoscan as $k=>$v){
						$keys = array_keys($v[0]);

						/* CHECK TRANSAKSI DATA */
						$transaksi->setId($keys[1]);
						$cek_transaksi = $transaksi->read();
						//print_r($cek_transaksi);exit();

						if($cek_transaksi['total_data'] > 0){
							foreach($v[11] as $k11=>$v11){
								foreach($bactoscan_fields as $kf=>$vf){
									$keynya = strtolower($v11);
									if($kf == $keynya){
										$bactoscan_fields[$kf]['key'] = $k11;
									}
								}
							}

							foreach($v[13] as $k13=>$v13){
								foreach($bactoscan_fields as $kf=>$vf){
									if($vf['key'] == $k13){
										$bactoscan_fields[$kf]['value'] = $v13;
									}
								}
							}
							//print_r($v);exit();
							if(!empty($bactoscan_fields['cfu']['value'])){

								/* CEK EXISTING DATA */
								$dt->setCondition('qcparameter_transaksi = \''.$keys[1].'\' AND qcparameter_status = \'y\'');
								$dt->setLimit(1);
								$cek = $dt->read();

								$bactodata = array();
								if(empty($bactoscan_fields['date']['value'])){
									$tahundarifile   = substr($file,0,4);
									$bulandarifile   = substr($file,4,2);
									$tanggaldarifile = substr($file,6,2);
									$jamdarifile     = substr($file,8,2);
									$menitdarifile   = substr($file,10,2);
									$detikdarifile   = substr($file,12,2);
									$bactoscanTgl    = $tahundarifile."-".$bulandarifile."-".$tanggaldarifile." ".$jamdarifile.":".$menitdarifile.":".$detikdarifile;
								}else{
									$bactoscanTgl = date('Y-m-d H:i:s',strtotime($bactoscan_fields['date']['value']." ".$bactoscan_fields['time']['value']));
								}

								//echo $bactoscanTgl;exit();
								$bactodata['qcparameter_transaksi']     = $keys[1];
								$bactodata['qcparameter_status']        = 'y';
								$bactodata['qcparameter_cfu']           = (float) $bactoscan_fields['cfu']['value'];
								$bactodata['qcparameter_ibc']           = (float) $bactoscan_fields['ibc']['value'];
								$bactodata['qcparameter_bactoscandate'] = $bactoscanTgl;
								$bactodata['qcparameter_koperasi']      = $cek_transaksi['records']['transaksi_koperasi'];
								$bactodata['qcparameter_kelompok']      = $cek_transaksi['records']['transaksi_kelompok'];
								$bactodata['qcparameter_anggota']       = $cek_transaksi['records']['transaksi_anggota'];
								$bactodata['qcparameter_sesi']          = $cek_transaksi['records']['transaksi_sesi'];
								$bactodata['qcparameter_tpk']           = $cek_transaksi['records']['transaksi_tpk'];

								if($cek['total_data'] === 0){
									$bactodata['qcparameter_id'] = $this->getToken(32);
									$bactodata['qcparameter_scandate'] = date('Y-m-d H:i:s');
									$dt->setDataArray($bactodata);
									$query = $dt->insert();
								}else{
									$dt->setId($cek['records'][0]['qcparameter_id']);
									$dt->setDataArray($bactodata);
									$query = $dt->update();
								}

								if($query){
									@rename($filename,$path.DS.'archive'.DS.'bactoscan'.DS.$tahun.DS.$bulan.DS.$file);
									echo $bactoscanTgl." - File: ".$file." successfully imported.\n";

									$logdata['csvlog_id']      = $this->getToken(32);
									$logdata['csvlog_date']    = date('Y-m-d H:i:s');
									$logdata['csvlog_content'] = $bactoscanTgl." - File: ".$file." successfully imported.";

									$log->setDataArray($logdata);
									$log->insert();

									$logdata['csvlog_id']      = $this->getToken(32);
									$logdata['csvlog_date']    = date('Y-m-d H:i:s');
									$logdata['csvlog_content'] = 'Moving '.$file.' to '.$path.DS.'archive'.DS.'bactoscan'.DS.$tahun.DS.$bulan;

									$log->setDataArray($logdata);
									$log->insert();
								}else{
									echo $bactoscanTgl." - File: ".$file." cannot be processed and will redo later.\n";
									$logdata['csvlog_id']      = $this->getToken(32);
									$logdata['csvlog_date']    = date('Y-m-d H:i:s');
									$logdata['csvlog_content'] = $bactoscanTgl." - File: ".$file." cannot be processed and will redo later.";

									$log->setDataArray($logdata);
									$log->insert();
								}
							}else{
								rename($filename,$path.DS.'archive'.DS.'bactoscan'.DS.$tahun.DS.$bulan.DS.$file);
								//echo 'Moving '.$file.' to '.$path.DS.'archive'.DS.'bactoscan'.DS.$tahun.DS.$bulan;
								$logdata['csvlog_id']      = $this->getToken(32);
								$logdata['csvlog_date']    = date('Y-m-d H:i:s');
								$logdata['csvlog_content'] = 'Moving '.$file.' to '.$path.DS.'archive'.DS.'bactoscan'.DS.$tahun.DS.$bulan;

								$log->setDataArray($logdata);
								$log->insert();
							}
						}else{
							rename($filename,$path.DS.'archive'.DS.'bactoscan'.DS.$tahun.DS.$bulan.DS.'notfound'.DS.$file);
							echo 'Transaction ID: '.$keys[1].' Not found. Moving '.$file.' to '.$path.DS.'archive'.DS.'bactoscan'.DS.$tahun.DS.$bulan.DS.'notfound';
							echo "\r\n";
							$logdata['csvlog_id']      = $this->getToken(32);
							$logdata['csvlog_date']    = date('Y-m-d H:i:s');
							$logdata['csvlog_content'] = 'Transaction ID: '.$keys[1].' Not found. Moving '.$file.' to '.$path.DS.'archive'.DS.'bactoscan'.DS.$tahun.DS.$bulan;

							$log->setDataArray($logdata);
							$log->insert();
						}
					}
				}
				sleep(5);
			}
			echo "Processing Milkoscan Folder\n";
			//usleep(1000);
			//exit();
			//$milkoscanFiles = glob($path.DS.'milkoscan'.DS.'*.csv');
			foreach (glob($path.DS.'milkoscan'.DS.'*.csv') as $filename) {

			    $tipe = substr($filename,-5,5);
				$file = substr($filename,-20,20);
				if($tipe == '0.csv' || $tipe == '1.csv'){
					rename($filename,$path.DS.'archive'.DS.'milkoscan'.DS.$tahun.DS.$bulan.DS.$file);
					$logdata['csvlog_id'] = $this->getToken(32);
					$logdata['csvlog_date'] = date('Y-m-d H:i:s');
					$logdata['csvlog_content'] = 'Moving '.$file.' to '.$path.DS.'archive'.DS.'milkoscan'.DS.$tahun.DS.$bulan;
					$log->setDataArray($logdata);
					$log->insert();
				}else{
					$milkoscan = array();
					$csv->auto($filename);
					$milkoscan[0] = $csv->data;
					//print_r($milkoscan);exit();
					foreach($milkoscan as $k=>$v){
						if(isset($v[22]) && count($v[22]) > 6){
							foreach($v[20] as $k20 => $v20){
								foreach($milkoscan_fields as $kf=>$vf){
									$keynya = explode(' ', strtolower($v20));
									if($keynya[0] == 'tds'){
										$keynya[0] = 'ts';
									}
									if($kf == $keynya[0]){
										$milkoscan_fields[$kf]['key'] = $k20;
									}
								}
							}
							foreach($v[22] as $k22=>$v22){
								foreach($milkoscan_fields as $kf=>$vf){
									if($vf['key'] == $k22){
										$milkoscan_fields[$kf]['value'] = $v22;
									}
								}
							}
							//print_r($milkoscan_fields);exit();
							if(!empty($milkoscan_fields['fat']['value'])){
								$keys = array_keys($v[0]);

								$transaksi->setId($keys[1]);
								$cek_transaksi = $transaksi->read();
								if($cek_transaksi['total_data'] > 0){
									$dt->setCondition('qcparameter_transaksi = \''.$keys[1].'\' AND qcparameter_status = \'y\'');
									$dt->setLimit(1);
									$cek = $dt->read();

									$milkoscanTgl                           = date('Y-m-d H:i:s',strtotime($milkoscan_fields['date']['value']." ".$milkoscan_fields['time']['value']));
									$milkodata                              = array();
									$milkodata['qcparameter_transaksi']     = $keys[1];
									$milkodata['qcparameter_status']        = 'y';
									$milkodata['qcparameter_ts']            = (float) $milkoscan_fields['ts']['value'];
									$milkodata['qcparameter_fat']           = (float) $milkoscan_fields['fat']['value'];
									$milkodata['qcparameter_protein']       = (float) $milkoscan_fields['protein']['value'];
									$milkodata['qcparameter_urea']          = (float) $milkoscan_fields['urea']['value'];
									$milkodata['qcparameter_acidity']       = (float) $milkoscan_fields['acidity']['value'];
									$milkodata['qcparameter_lactose']       = (float) $milkoscan_fields['lactose']['value'];
									$milkodata['qcparameter_fp']            = (float) $milkoscan_fields['fpd']['value'];
									$milkodata['qcparameter_snf']           = (float) ((float) $milkoscan_fields['ts']['value'] - (float) $milkoscan_fields['fat']['value']);
									$milkodata['qcparameter_milkoscandate'] = $milkoscanTgl;
									$milkodata['qcparameter_koperasi']      = $cek_transaksi['records']['transaksi_koperasi'];
									$milkodata['qcparameter_kelompok']      = $cek_transaksi['records']['transaksi_kelompok'];
									$milkodata['qcparameter_anggota']       = $cek_transaksi['records']['transaksi_anggota'];
									$milkodata['qcparameter_sesi']          = $cek_transaksi['records']['transaksi_sesi'];
									$milkodata['qcparameter_tpk']           = $cek_transaksi['records']['transaksi_tpk'];

									//print_r($milkodata);exit();
									if($cek['total_data'] === 0){
										$milkodata['qcparameter_id']       = $this->getToken(32);
										$milkodata['qcparameter_scandate'] = date('Y-m-d H:i:s');
										$dt->setDataArray($milkodata);
										$query = $dt->insert();
									}else{
										$dt->setId($cek['records'][0]['qcparameter_id']);
										$dt->setDataArray($milkodata);
										$query = $dt->update();
									}

									if($query){
										rename($filename,$path.DS.'archive'.DS.'milkoscan'.DS.$tahun.DS.$bulan.DS.$file);
										echo $milkoscanTgl." - File: ".$file." successfully imported.\n";

										$logdata['csvlog_id']      = $this->getToken(32);
										$logdata['csvlog_date']    = date('Y-m-d H:i:s');
										$logdata['csvlog_content'] = $milkoscanTgl." - File: ".$file." successfully imported.";
										$log->setDataArray($logdata);
										$log->insert();

										$logdata['csvlog_id']      = $this->getToken(32);
										$logdata['csvlog_date']    = date('Y-m-d H:i:s');
										$logdata['csvlog_content'] = 'Moving '.$file.' to '.$path.DS.'archive'.DS.'milkoscan'.DS.$tahun.DS.$bulan;
										$log->setDataArray($logdata);
										$log->insert();
									}else{
										echo $milkoscanTgl." - File: ".$file." cannot be processed and will redo later.\n";
										$logdata['csvlog_id']      = $this->getToken(32);
										$logdata['csvlog_date']    = date('Y-m-d H:i:s');
										$logdata['csvlog_content'] = $milkoscanTgl." - File: ".$file." cannot be processed and will redo later.";
										$log->setDataArray($logdata);
										$log->insert();
									}
								}else{
									rename($filename,$path.DS.'archive'.DS.'milkoscan'.DS.$tahun.DS.$bulan.DS.'notfound'.DS.$file);
									$logdata['csvlog_id']      = $this->getToken(32);
									$logdata['csvlog_date']    = date('Y-m-d H:i:s');
									$logdata['csvlog_content'] = 'Moving '.$file.' to '.$path.DS.'archive'.DS.'milkoscan'.DS.$tahun.DS.$bulan;
									$log->setDataArray($logdata);
									$log->insert();
								}
							}else{
								rename($filename,$path.DS.'archive'.DS.'milkoscan'.DS.$tahun.DS.$bulan.DS.$file);
								$logdata['csvlog_id']      = $this->getToken(32);
								$logdata['csvlog_date']    = date('Y-m-d H:i:s');
								$logdata['csvlog_content'] = 'Moving '.$file.' to '.$path.DS.'archive'.DS.'milkoscan'.DS.$tahun.DS.$bulan;
								$log->setDataArray($logdata);
								$log->insert();
							}

						}else{
							rename($filename,$path.DS.'archive'.DS.'milkoscan'.DS.$tahun.DS.$bulan.DS.$file);
							$logdata['csvlog_id']      = $this->getToken(32);
							$logdata['csvlog_date']    = date('Y-m-d H:i:s');
							$logdata['csvlog_content'] = 'Moving '.$file.' to '.$path.DS.'archive'.DS.'milkoscan'.DS.$tahun.DS.$bulan;
							$log->setDataArray($logdata);
							$log->insert();
						}
					}
				}
				sleep(2);
			}

			echo "Processing Milko FT 120\n";
			foreach (glob($path.DS.'ffimilkoscanft120'.DS.'*.csv') as $filename) {
				echo $filename."\n";
				$explodeFile = explode(DS, $filename);
				$file = end($explodeFile); // FIX truncate file name, karena nama filenya bisa apa aja.

				//$file = substr($filename,-12,12);
				$logdata = array();
				$csv->delimiter = "\r\n";
				$csv->parse($filename);
				$milcoft120 = $csv->data;
				$milcoFT120Error = 0;
                if (count($milcoft120) > 0) {
                    // FIRST RECORDS
                    $firstKeys = array_keys($milcoft120[0]);
                    $milkoscanData = explode(';', $firstKeys[0]);
                    $tglSQL 								= date('Y-m-d',fn::dateToUnix(trim($milkoscanData[9])));
                    $jamSQL 								= trim($milkoscanData[10]);
					$transaksi->setId(strtoupper(trim($milkoscanData[0])));
					$cek_transaksi = $transaksi->read();
					//echo $cek_transaksi['total_data'];exit();
					$query = true;
					if($cek_transaksi['total_data'] > 0){
						/* CEK EXISTING DATA */
						$dt->resetValue();
						$dt->setCondition('qcparameter_transaksi = \''.$cek_transaksi['records']['transaksi_id'].'\' AND qcparameter_status = \'y\'');
						$dt->setLimit(1);
						unset($cek);
						$cek = $dt->read();
						if(isset($milcoFT120Data)){
							unset($milcoFT120Data);
						}
						$milcoFT120Data = array();
						$milcoFT120Data['qcparameter_fat'] 		= (float) str_replace(',','.',trim($milkoscanData[3]));
						$milcoFT120Data['qcparameter_protein'] 	= (float) str_replace(',','.',trim($milkoscanData[4]));
						$milcoFT120Data['qcparameter_lactose'] 	= (float) str_replace(',','.',trim($milkoscanData[5]));
						$milcoFT120Data['qcparameter_ts'] 		= (float) str_replace(',','.',trim($milkoscanData[6]));
						$milcoFT120Data['qcparameter_fp'] 		= (float) str_replace(',','.',trim($milkoscanData[7]));
						$milcoFT120Data['qcparameter_acidity'] 	= (float) str_replace(',','.',trim($milkoscanData[8]));
						$milcoFT120Data['qcparameter_snf'] 	    = (float) ($milcoFT120Data['qcparameter_ts'] - $milcoFT120Data['qcparameter_fat']);
						//print_r($cek);
						//exit();
						if($cek['total_data'] > 0){
							$dt->resetValue();
							$dt->setId($cek['records'][0]['qcparameter_id']);
							$dt->setDataArray($milcoFT120Data);
							$query = $dt->update();
							$msgft120 = "Quality Data updated (Transaction ID ".strtoupper(trim($milkoscanData[0])).")\n";
						}else{
							$milcoFT120Data['qcparameter_id'] 		= $this->getToken(32);
							$tglSQL 								= date('Y-m-d',fn::dateToUnix(trim($milkoscanData[9])));
							$jamSQL 								= trim($milkoscanData[10]);
							$milcoFT120Data['qcparameter_scandate'] = date('Y-m-d H:i:s', strtotime($tglSQL.' '.$jamSQL));
							$milcoFT120Data['qcparameter_transaksi'] = $cek_transaksi['records']['transaksi_id'];
							$milcoFT120Data['qcparameter_koperasi']      = $cek_transaksi['records']['transaksi_koperasi'];
							$milcoFT120Data['qcparameter_kelompok']      = $cek_transaksi['records']['transaksi_kelompok'];
							$milcoFT120Data['qcparameter_anggota']       = $cek_transaksi['records']['transaksi_anggota'];
							$milcoFT120Data['qcparameter_sesi']          = $cek_transaksi['records']['transaksi_sesi'];
							$milcoFT120Data['qcparameter_tpk']           = $cek_transaksi['records']['transaksi_tpk'];
							//print_r($milcoFT120Data);exit();
							$dt->resetValue();
							$dt->setDataArray($milcoFT120Data);
							$query = $dt->insert();
							$msgft120 = "Quality Data added (Transaction ID ".strtoupper(trim($milkoscanData[0])).")\n";
						}
						if(!$query){
							$milcoFT120Error++;
							echo "Quality Data cannot be imported. Transaction ID: ".strtoupper(trim($milkoscanData[0]));
						}else{
							echo $msgft120;
						}
						$logdata['csvlog_id']      = $this->getToken(32);
						$logdata['csvlog_date']    = date('Y-m-d H:i:s');
						$logdata['csvlog_content'] = $msgft120;
						$log->setDataArray($logdata);
						$log->insert();
					}else{
						echo "Transaction ID ".strtoupper(trim($milkoscanData[0]))." not Found\n";
						$logdata['csvlog_id']      = $this->getToken(32);
						$logdata['csvlog_date']    = date('Y-m-d H:i:s');
						$logdata['csvlog_content'] = "Transaction ID ".strtoupper(trim($milkoscanData[0]))." not Found\n";
						$log->setDataArray($logdata);
						$log->insert();
					}
                }
				foreach($milcoft120 as $k=>$v){
					$milkoscanData = array();
					$keys = array_keys($v);
					$lineData = $v[$keys[0]];
					//print_r($lineData);exit();
					$milkoscanData = explode(';',$lineData);
					$transaksi->setId(strtoupper(trim($milkoscanData[0])));
					$cek_transaksi = $transaksi->read();
					//echo $cek_transaksi['total_data'];exit();
					$query = true;
					if($cek_transaksi['total_data'] > 0){
						/* CEK EXISTING DATA */
						$dt->resetValue();
						$dt->setCondition('qcparameter_transaksi = \''.$cek_transaksi['records']['transaksi_id'].'\' AND qcparameter_status = \'y\'');
						$dt->setLimit(1);
						unset($cek);
						$cek = $dt->read();
						if(isset($milcoFT120Data)){
							unset($milcoFT120Data);
						}
						$milcoFT120Data = array();
						$milcoFT120Data['qcparameter_fat'] 		= (float) str_replace(',','.',trim($milkoscanData[3]));
						$milcoFT120Data['qcparameter_protein'] 	= (float) str_replace(',','.',trim($milkoscanData[4]));
						$milcoFT120Data['qcparameter_lactose'] 	= (float) str_replace(',','.',trim($milkoscanData[5]));
						$milcoFT120Data['qcparameter_ts'] 		= (float) str_replace(',','.',trim($milkoscanData[6]));
						$milcoFT120Data['qcparameter_fp'] 		= (float) str_replace(',','.',trim($milkoscanData[7]));
						$milcoFT120Data['qcparameter_acidity'] 	= (float) str_replace(',','.',trim($milkoscanData[8]));
						$milcoFT120Data['qcparameter_snf'] 	    = (float) ($milcoFT120Data['qcparameter_ts'] - $milcoFT120Data['qcparameter_fat']);
						//print_r($cek);
						//exit();
						if($cek['total_data'] > 0){
							$dt->resetValue();
							$dt->setId($cek['records'][0]['qcparameter_id']);
							$dt->setDataArray($milcoFT120Data);
							$query = $dt->update();
							$msgft120 = "Quality Data updated (Transaction ID ".strtoupper(trim($milkoscanData[0])).")\n";
						}else{
							$milcoFT120Data['qcparameter_id'] 		= $this->getToken(32);
							$tglSQL 								= date('Y-m-d',fn::dateToUnix(trim($milkoscanData[9])));
							$jamSQL 								= trim($milkoscanData[10]);
							$milcoFT120Data['qcparameter_scandate'] = date('Y-m-d H:i:s', strtotime($tglSQL.' '.$jamSQL));
							$milcoFT120Data['qcparameter_transaksi'] = $cek_transaksi['records']['transaksi_id'];
							$milcoFT120Data['qcparameter_koperasi']      = $cek_transaksi['records']['transaksi_koperasi'];
							$milcoFT120Data['qcparameter_kelompok']      = $cek_transaksi['records']['transaksi_kelompok'];
							$milcoFT120Data['qcparameter_anggota']       = $cek_transaksi['records']['transaksi_anggota'];
							$milcoFT120Data['qcparameter_sesi']          = $cek_transaksi['records']['transaksi_sesi'];
							$milcoFT120Data['qcparameter_tpk']           = $cek_transaksi['records']['transaksi_tpk'];
							//print_r($milcoFT120Data);exit();
							$dt->resetValue();
							$dt->setDataArray($milcoFT120Data);
							$query = $dt->insert();
							$msgft120 = "Quality Data added (Transaction ID ".strtoupper(trim($milkoscanData[0])).")\n";
						}
						if(!$query){
							$milcoFT120Error++;
							echo "Quality Data cannot be imported. Transaction ID: ".strtoupper(trim($milkoscanData[0]));
						}else{
							echo $msgft120;
						}
						$logdata['csvlog_id']      = $this->getToken(32);
						$logdata['csvlog_date']    = date('Y-m-d H:i:s');
						$logdata['csvlog_content'] = $msgft120;
						$log->setDataArray($logdata);
						$log->insert();
					}else{
						echo "Transaction ID ".strtoupper(trim($milkoscanData[0]))." not Found\n";
						$logdata['csvlog_id']      = $this->getToken(32);
						$logdata['csvlog_date']    = date('Y-m-d H:i:s');
						$logdata['csvlog_content'] = "Transaction ID ".strtoupper(trim($milkoscanData[0]))." not Found\n";
						$log->setDataArray($logdata);
						$log->insert();
					}
					usleep(1000000);
				}
				if($milcoFT120Error === 0){
					@rename($filename,$path.DS.'archive'.DS.'ffimilkoscanft120'.DS.$tahun.DS.$bulan.DS.$file);
					echo date('Y-m-d H:i:s')." - File: ".$file." successfully imported.\n";

					$logdata['csvlog_id']      = $this->getToken(32);
					$logdata['csvlog_date']    = date('Y-m-d H:i:s');
					$logdata['csvlog_content'] = date('Y-m-d H:i:s')." - File: ".$file." successfully imported.";

					$log->setDataArray($logdata);
					$log->insert();

					$logdata['csvlog_id']      = $this->getToken(32);
					$logdata['csvlog_date']    = date('Y-m-d H:i:s');
					$logdata['csvlog_content'] = 'Moving '.$file.' to '.$path.DS.'archive'.DS.'ffimilkoscanft120'.DS.$tahun.DS.$bulan;

					$log->setDataArray($logdata);
					$log->insert();
					echo "Moving ".$file." to ".$path.DS."archive".DS."ffimilkoscanft120".DS.$tahun.DS.$bulan."\n";
				}else{
					echo date('Y-m-d H:i:s')." - File: ".$file." cannot be processed and will redo later.\n";
					$logdata['csvlog_id']      = $this->getToken(32);
					$logdata['csvlog_date']    = date('Y-m-d H:i:s');
					$logdata['csvlog_content'] = date('Y-m-d H:i:s')." - File: ".$file." cannot be processed and will redo later.";

					$log->setDataArray($logdata);
					$log->insert();
				}
				sleep(2);
			}
			echo "Done\n";
			echo "Restarting Process.. Wait for 5 seconds\n";
			sleep(5);

		}
	}
	function cryptoRandSecure($min, $max)
	{
		$range = $max - $min;
		if ($range < 1) return $min; // not so random...
		$log = ceil(log($range, 2));
		$bytes = (int) ($log / 8) + 1; // length in bytes
		$bits = (int) $log + 1; // length in bits
		$filter = (int) (1 << $bits) - 1; // set all lower bits to 1
		do {
			$rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
			$rnd = $rnd & $filter; // discard irrelevant bits
		} while ($rnd >= $range);
		return $min + $rnd;
	}

	function getToken($length)
	{
		$token = "";
		$codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
		$codeAlphabet.= "0123456789";
		$max = strlen($codeAlphabet) - 1;
		for ($i=0; $i < $length; $i++) {
			$token .= $codeAlphabet[$this->cryptoRandSecure(0, $max)];
		}
		return $token;
	}
}
