<?php
/**
 * Created by PhpStorm.
 * User: transformatika
 * Date: 12/7/2014
 * Time: 12:28 PM
 */
class Login_Controller extends Controller{
    public $version = 1.1;
    public $buildConfiguration = array(
        'views'=>array(
            'login.index'
        ),
        'models'=>array(
            'user'
        ),
        'table'=>'user'
    );
    public $description = 'System Authenticator';
    function __construct(){
        if(session::check('user_id')){
            header('location:'.BASE_URL.'index.php?/dashboard');
            exit();
        }else{
            if(isset($_COOKIE['user_id']) && isset($_COOKIE['user_token'])){
                $user_id = security::decrypt($_COOKIE['user_id']);
                $user_token = security::decrypt($_COOKIE['user_token']);
                $user = load::model('user');
                $user->setCondition('user_token = \''.$user_token.'\' AND user_id = \''.$user_id.'\' AND user_status = \'y\'');
                $user->setLimit(1);
                $dtUser = $user->read();
                if($dtUser['total_data'] > 0){
                    $avatar = BASE_URL.'themes/default/images/avatar.jpg';
                    if(strlen($dtUser['records'][0]['user_avatar']) > 3){
                        $avatar = Config::$application['file_url'].rawurlencode($dtUser['records'][0]['user_avatar']);
                    }
                    $user_token = SHA1(session_id());
                    if(strlen($dtUser['records'][0]['user_theme']) > 2){
                        session::set('user_theme',$dtUser['records'][0]['user_theme']);
                    }else{
                        session::set('user_theme','blue');
                    }
                    session::set('user_id',$dtUser['records'][0]['user_id']);
                    session::set('user_name',$dtUser['records'][0]['user_name']);
                    session::set('user_fullname',$dtUser['records'][0]['user_fullname']);
                    session::set('user_email',$dtUser['records'][0]['user_email']);
                    session::set('user_id',$dtUser['records'][0]['user_id']);
                    session::set('user_avatar',$avatar);
                    session::set('user_su',$dtUser['records'][0]['user_su']);
                    session::set('user_group',$dtUser['records'][0]['user_group']);
                    session::set('user_token',$user_token);
                    session::set('user_firstuse',$dtUser['records'][0]['user_firstuse']);
                    session::set('user_role',$dtUser['records'][0]['usertype_role']);
                    session::set('user_koperasi',$dtUser['records'][0]['user_koperasi']);
                    session::set('user_koperasi_nama',$dtUser['records'][0]['koperasi_nama']);
                    session::set('user_koperasi_logo',$dtUser['records'][0]['koperasi_logo']);
                    session::set('user_koperasi_telp',$dtUser['records'][0]['koperasi_telp']);
                    session::set('user_koperasi_alamat',$dtUser['records'][0]['koperasi_alamat']);
                    session::set('user_koperasi_kode',$dtUser['records'][0]['koperasi_kode']);
                    $user->setId($dtUser['records'][0]['user_id']);
                    $user->setData('user_lastactivity',date('Y-m-d H:i:s'));
                    $user->setData('user_token',$user_token);
                    $user->update();

                    setcookie('user_id',security::encrypt($dtUser['records'][0]['user_id']),strtotime('+30 days'));
                    setcookie('user_token',security::encrypt($user_token),strtotime('+30 days'));
                    header('location:'.BASE_URL.'index.php?/dashboard');
                    exit();
                }
            }
        }
    }
    function index(){
        $repo = load::model('repo');
        $repo->updateRepo('koperasi');
        session::set('title','Login');
        View::display('login/login.index');
    }
    function process(){
        if(isset($_POST['user_name']) && isset($_POST['user_password'])){
			if (!ctype_alnum(str_replace(['@', '.', '_', '-'], '', $_POST['user_name']))) {
				session::set('errorMsg',_('Not valid username!'));
                header('location:'.BASE_URL.'index.php?/login');
                exit();
			}
            $user = load::model('user');
            $user->setCondition('(user_name = \''.$_POST['user_name'].'\' OR user_email = \''.$_POST['user_name'].'\')');
            $user->setLimit(1);
            $dtUser = $user->read();
            //print_r($dtUser);exit();
            if($dtUser['total_data'] === 0){
                session::set('errorMsg',_('username not found'));
                header('location:'.BASE_URL.'index.php?/login');
                exit();
            }else{
                if($dtUser['records'][0]['user_status'] == 'y'){
                    $decrypt_pasword = security::decrypt($dtUser['records'][0]['user_password']);
                    if($decrypt_pasword == $_POST['user_password']){
                        $avatar = BASE_URL.'themes/default/images/avatar.jpg';
                        if(strlen($dtUser['records'][0]['user_avatar']) > 3){
                            $avatar = Config::$application['file_url'].rawurlencode($dtUser['records'][0]['user_avatar']);
                        }
                        $user_token = MD5(session_id());
                        if(strlen($dtUser['records'][0]['user_theme']) > 2){
                            session::set('user_theme',$dtUser['records'][0]['user_theme']);
                        }else{
                            session::set('user_theme','blue');
                        }
                        session::set('user_id',$dtUser['records'][0]['user_id']);
                        session::set('user_name',$dtUser['records'][0]['user_name']);
                        session::set('user_fullname',$dtUser['records'][0]['user_fullname']);
                        session::set('user_email',$dtUser['records'][0]['user_email']);
                        session::set('user_id',$dtUser['records'][0]['user_id']);
                        session::set('user_avatar',$avatar);
                        session::set('user_su',$dtUser['records'][0]['user_su']);
                        session::set('user_group',$dtUser['records'][0]['user_group']);
                        session::set('user_token',$user_token);
                        session::set('user_firstuse',$dtUser['records'][0]['user_firstuse']);
                        session::set('user_role',$dtUser['records'][0]['usertype_role']);
                        session::set('user_koperasi',$dtUser['records'][0]['user_koperasi']);
                        session::set('user_koperasi_nama',$dtUser['records'][0]['koperasi_nama']);
                        session::set('user_koperasi_logo',$dtUser['records'][0]['koperasi_logo']);
                        session::set('user_koperasi_telp',$dtUser['records'][0]['koperasi_telp']);
                        session::set('user_koperasi_alamat',$dtUser['records'][0]['koperasi_alamat']);
                        session::set('user_koperasi_kode',$dtUser['records'][0]['koperasi_kode']);
                        $user->setId($dtUser['records'][0]['user_id']);
                        $user->setData('user_lastactivity',date('Y-m-d H:i:s'));
                        $user->setData('user_token',$user_token);
                        $user->update();
                        if(isset($_POST['remember'])){
                            setcookie('user_id',security::encrypt($dtUser['records'][0]['user_id']),strtotime('+30 days'));
                            setcookie('user_token',security::encrypt($user_token),strtotime('+30 days'));
                        }else{
                            setcookie('user_id',security::encrypt($dtUser['records'][0]['user_id']));
                            setcookie('user_token',security::encrypt($user_token));
                        }
                        fn::log($_POST['user_name'].' Logged in to System');
                        header('location:'.BASE_URL.'index.php?/dashboard');
                        exit();
                    }else{
                        session::set('errorMsg',_('Password do not match'));
                        fn::log('Failed Login. '.$_POST['user_name']. 'trying to login. ERROR: Password not match');
                        fn::log_error('Failed Login. '.$_POST['user_name']. 'trying to login. ERROR: Password not match');
                        header('location:'.BASE_URL.'index.php?/login');
                        exit();
                    }
                }else{
                    session::set('errorMsg',_('your username is not active or banned'));
                    fn::log('Failed Login. '.$_POST['user_name']. 'trying to login. ERROR: Username not active / banned');
                    fn::log_error('Failed Login. '.$_POST['user_name']. 'trying to login. ERROR: Username not active / banned');
                    header('location:'.BASE_URL.'index.php?/login');
                    exit();
                }

            }
        }
    }
}
