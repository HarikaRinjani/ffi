<?php
class Fpgrade_Controller extends Controller{
	public $buildConfiguration = array(
		'table'=>array('fpgrade','fpgradedetail'),
		'views'=>array('fpgrade.index'),
		'models'=>array('fpgrade','fpgradedetail')
		);
	public $version = 1.4;
	public $description = "v1.4\n- Add Filter by Coop \n\nv1.3\n- Fix Minor Bugs \n\nv1.2 (2015-05-06)\n- Fix minor bugs";
	function __construct(){
		security::checkSession();
	}
	function index(){
		$dt = load::model('fpgrade');
		$koperasi = load::model('koperasi');

		if(!in_array(session::get('user_group'),Config::$application['super_group'])){
			$_GET['koperasi'] = session::get('user_koperasi');
			$koperasi->setCondition('koperasi_id = \''.session::get('user_koeprasi').'\'');
			$data['koperasi'] = $koperasi->read();
		}else{
			$_GET['koperasi'] = !isset($_GET['koperasi']) ? '' : $_GET['koperasi'];
			$koperasi->setCondition('koperasi_status = \'y\'')->setOrderBy('koperasi_nama')->setLimit(0);
			$data['koperasi'] = $koperasi->read();
		}

		$addSQL = '';
		if(!empty($_GET['koperasi'])){
			$addSQL = ' AND fpgrade_koperasi = \''.$_GET['koperasi'].'\'';
		}
		session::set('title','Freezing Point Grade');
		if(in_array(session::get('user_group'),Config::$application['super_group'])){
			$dt->setCondition('fpgrade_status != \'d\''.$addSQL);
		}else{
			$dt->setCondition('fpgrade_status != \'d\' AND fpgrade_koperasi = \''.session::get('user_koperasi').'\'');
		}
		$dt->setOrderBy('fpgrade_grade');
		$data['grade'] = $dt->read();
		View::display('fpgrade/fpgrade.index',$data);
	}
	function edit($id=''){
		if(!empty($id)){
			$dt = load::model('fpgrade');
			$dt->setId($id);
			$data = $dt->read();
			echo json_encode($data['records']);
		}
	}
	function delete(){
		if(isset($_POST['fpgrade_id'])){
			$dt = load::model('fpgrade');
			$dt->setId($_POST['fpgrade_id']);
			if($dt->delete()){
				session::set('errorMsg',_('Data successfully deleted'));
			}else{
				session::set('errorMsg',_('Failed while deleting data'));
			}
		}
	}
	function save(){
		$dt = load::model('fpgrade');
		$_POST['fpgrade_effectivedate'] = date('Y-m-d',fn::dateToUnix($_POST['fpgrade_effectivedate']));
		$fpgradedetail = load::model('fpgradedetail');
		if(!isset($_POST['fpgrade_id'])){
			$_POST['fpgrade_id'] = fn::generate_id();
			$dt->setDataArray($_POST);
			$query = $dt->insert();
			if($query){
				foreach($_POST['fpgradedetail_check'] as $k=>$v){
					if(!empty($_POST['fpgradedetail_grade'][$k])){
						$detail['fpgradedetail_id']      = fn::generate_id(true);
						$detail['fpgradedetail_price']   = (float) $_POST['fpgradedetail_price'][$k];
						$detail['fpgradedetail_fpstart'] = (float) $_POST['fpgradedetail_fpstart'][$k];
						$detail['fpgradedetail_fpend']   = (float) $_POST['fpgradedetail_fpend'][$k];
						$detail['fpgradedetail_fpgrade'] = $_POST['fpgrade_id'];
						$detail['fpgradedetail_grade']   = (int) $_POST['fpgradedetail_grade'][$k];
						$fpgradedetail->setDataArray($detail);
						$fpgradedetail->insert();
					}
				}
			}
		}else{
			$dt->setId($_POST['fpgrade_id']);
			$dt->setDataArray($_POST);
			$query = $dt->update();
		}
		if($query){
			session::set('errorMsg',_('Data has been successfully saved'));
		}else{
			session::set('errorMsg',_('Failed while saving data into database'));
		}
		header('location:'.BASE_URL.'index.php?/fpgrade/index');
	}
}