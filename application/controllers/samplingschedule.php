<?php
/**
 * Sampling Schedule Controller.
 */
class Samplingschedule_Controller extends Controller
{
    public $buildConfiguration = array(
        'table' => array(
            'samplingschedule',
            'periode',
        ),
        'views' => array(
            'samplingschedule.data',
            'samplingschedule.index',
        ),
        'models' => 'samplingschedule',
    );
    public $version = 1.5;
    public $description = "v1.5 - Add more Filter\n\n";
    public function __construct()
    {
        security::checkSession();
    }

    /**
     * Report.
     *
     * @return [type] [description]
     */
    public function index()
    {
        session::set('title', 'Sampling Schedule');
        $_GET['date'] = !isset($_GET['date']) ? date('d/m/Y') : $_GET['date'];
        $_GET['page'] = !isset($_GET['page']) ? 1 : $_GET['page'];
        $_GET['koperasi'] = !isset($_GET['koperasi']) ? '' : $_GET['koperasi'];
        $_GET['tpk'] = !isset($_GET['tpk']) ? '' : $_GET['tpk'];

        $schedule = load::model('samplingschedule');
        $koperasi = load::model('koperasi');
        $tpk = load::model('tpk');
        $tanggalSQL = date('Y-m-d', fn::dateToUnix($_GET['date']));
        $addSQL = '';


        if (!in_array(session::get('user_group'), Config::$application['super_group'])) {
            $koperasi->setCondition('koperasi_id = \''.session::get('user_koperasi').'\'');
            $_GET['koperasi'] = session::get('user_koperasi');
            $data['koperasi'] = $koperasi->setLimit(0)->read();
        } else {
            $data['koperasi'] = $koperasi->setCondition('koperasi_status = \'y\'')->setLimit(0)->read();
        }

        if (!empty($_GET['koperasi'])) {
            $tpk->setCondition('tpk_koperasi = \''.$_GET['koperasi'].'\' AND tpk_status = \'y\'');
            $data['tpk'] = $tpk->setLimit(0)->read();
            $addSQL .= ' AND samplingschedule_koperasi = \''.$_GET['koperasi'].'\'';
        } else {
            $data['tpk'] = $tpk->setCondition('tpk_status = \'y\'')->setLimit(0)->read();
        }

        if (!empty($_GET['tpk'])) {
            $addSQL .= ' AND samplingschedule_tpk = \''.$_GET['tpk'].'\'';
        }


        $schedule->setCondition('samplingschedule_date = \''.$tanggalSQL.'\''.$addSQL);
        if (isset($_GET['keyword'])) {
            $schedule->setKeyword($_GET['keyword']);
            $schedule->setSearchIn(array('t_anggota.anggota_nama', 't_anggota.anggota_nomor'));
        }
        $schedule->setOrderBy('samplingschedule_sesi');

        /*
         * EXPORT TO EXCEL
         */
        if (isset($_GET['export']) && !empty($_GET['export'])) {
            $schedule->setLimit(0);
            $schedule->setPage(1);
            $arraySchedule = $schedule->read();
            $header = array(
                'COOPERATIVE',
                'DATE',
                'FARMER ID',
                'FARMER NAME',
                'SESSION',
                'PERIODE',
                'STATUS',
                );
            $__SESI = array(
                'p' => 'Morning',
                's' => 'Afternoon',
                );
            $__SAMPLING_STATUS = array(
                'p' => 'PENDING',
                'y' => 'TAKEN',
                );
            $xlsrow = array();
            foreach ($arraySchedule['records'] as $k => $v) {
                $row = '';
                $row .= $v['koperasi_nama']."\t";
                $row .= $v['samplingschedule_date']."\t";
                $row .= $v['anggota_nomor']."\t";
                $row .= $v['anggota_nama']."\t";
                $row .= $__SESI[$v['samplingschedule_sesi']]."\t";
                $row .= $v['samplingschedule_periode']."\t";
                $row .= $__SAMPLING_STATUS[$v['samplingschedule_status']];
                $xlsrow[] = $row;
            }

            $filename = 'SamplingScedule-'.$_GET['date'].'.xls';

            header("Content-Disposition: attachment; filename=\"$filename\"");
            header('Content-Type: application/vnd.ms-excel');
            echo implode("\t", $header)."\n";
            echo implode("\n", $xlsrow);
            exit();
        }
        $schedule->setPage($_GET['page']);
        $data['schedule'] = $schedule->read();
        /*
        * Chart
        */
        // if (!in_array(session::get('user_group'), Config::$application['super_group'])) {
        //     $schedule->setCondition('samplingschedule_koperasi = \''.session::get('user_koperasi').'\'');
        // } else {
        //     $schedule->setCondition('');
        // }
        $schedule->setCondition(substr($addSQL, 4));

        $schedule->setChartType('column');
        $schedule->setChartYear(date('Y'));
        $schedule->setChartDate(date('Y-m-d', fn::dateToUnix($_GET['date'])));
        $schedule->setChartFields(array(
            'samplingschedule_status1' => array(
                'field' => 'samplingschedule_status',
                'alias' => 'status1',
                'operation' => 'count',
                'title' => 'Pending',
                'condition' => 'samplingschedule_status = \'p\'',
                ),
            'samplingschedule_status2' => array(
                'field' => 'samplingschedule_status',
                'alias' => 'status2',
                'operation' => 'count',
                'title' => 'Taken',
                'condition' => 'samplingschedule_status = \'y\'',
                ),
            ));
        $schedule->setChartTitle('Sampling Report');
        $schedule->setChartPeriodeField('samplingschedule_date');
        $schedule->setChartSubtitle('Date: '.$_GET['date']);
        $schedule->setChartHeight('400px');
        $schedule->setChartPeriode('date');
        $schedule->setChartContainer('chart-widget');
        $data['chart'] = $schedule->generateChart();
        View::display('samplingschedule/samplingschedule.index', $data);
    }

    /**
     * Display Sampling Scheduler.
     *
     * @return [type]
     */
    public function data()
    {
        session::set('title', 'Sampling Schedule');
        $_GET['date'] = !isset($_GET['date']) ? date('d/m/Y') : $_GET['date'];
        $_GET['page'] = !isset($_GET['page']) ? 1 : $_GET['page'];
        $_GET['sesi'] = !isset($_GET['sesi']) ? '' : $_GET['sesi'];

        $schedule = load::model('samplingschedule');
        $koperasi = load::model('koperasi');
        $tpk = load::model('tpk');

        $tanggalSQL = date('Y-m-d', fn::dateToUnix($_GET['date']));
        $addSQL = '';

        if (!in_array(session::get('user_group'), Config::$application['super_group'])) {
            $koperasi->setCondition('koperasi_status = \'y\' AND koperasi_id = \''.session::get('user_koperasi').'\'');
            $_GET['koperasi'] = session::get('user_koperasi');
            $tpkcondition = ' AND tpk_koperasi = \''.session::get('user_koperasi').'\'';
        } else {
            $koperasi->setCondition('koperasi_status = \'y\'');
            $_GET['koperasi'] = !isset($_GET['koperasi']) ? '' : $_GET['koperasi'];
            $tpkcondition = '';
        }

        $koperasi->setOrderBy('koperasi_nama');
        $koperasi->setLimit(0);
        $dtCoop = $koperasi->read();
        $data['koperasi'] = $dtCoop['records'];

        $tpk->setCondition('tpk_status = \'y\' '.$tpkcondition);
        $tpk->setLimit(0);
        $dtMC = $tpk->read();
        $data['tpk'] = $dtMC['records'];

        if (isset($_GET['koperasi']) && !empty($_GET['koperasi'])) {
            $addSQL .= ' AND samplingschedule_koperasi = \''.$_GET['koperasi'].'\'';
        }

        if (isset($_GET['tpk']) && !empty($_GET['tpk'])) {
            $addSQL .= ' AND samplingschedule_tpk = \''.$_GET['tpk'].'\'';
        }

        if (isset($_GET['sesi']) && $_GET['sesi'] !== '') {
            $addSQL .= ' AND samplingschedule_sesi = \''.$_GET['sesi'].'\'';
        }

        $schedule->setCondition('samplingschedule_date = \''.$tanggalSQL.'\''.$addSQL);
        $schedule->setPage($_GET['page']);
        if (isset($_GET['keyword'])) {
            $schedule->setKeyword($_GET['keyword']);
            $schedule->setSearchIn(array('t_anggota.anggota_nama', 't_anggota.anggota_nomor'));
        }
        $schedule->setOrderBy('samplingschedule_sesi');
        $data['schedule'] = $schedule->read();
        View::display('samplingschedule/samplingschedule.data', $data);
    }

    public function generateSchedule($koperasi_id = '')
    {
        if (!empty($koperasi_id)) {
            $tpk_id = !isset($_GET['tpk_id']) ? '' : $_GET['tpk_id'];
            $schedule = load::model('samplingschedule');

            if (isset($_GET['date'])) {
                $date = date('Y-m-d', fn::dateToUnix($_GET['date']));
            } else {
                $date = date('Y-m-d');
            }
            //die($date);
            $dataPeriode = $schedule->getDataPeriode($koperasi_id, $date);
            //print_r($dataPeriode);exit();
            if (isset($_GET['sesi'])) {
                $sesi = $_GET['sesi'];
            } else {
                $jam = date('G');
                if ($jam <= Config::$application['batas_pagi']) {
                    $sesi = 'p';
                } else {
                    $sesi = 's';
                }
            }

            if ($tpk_id == '') {
                $schedule->setCondition('samplingschedule_periode = '.$dataPeriode['current_periode'].' AND samplingschedule_date = \''.$date.'\' AND samplingschedule_sesi = \''.$sesi.'\' AND samplingschedule_koperasi = \''.$koperasi_id.'\'');
            } else {
                $schedule->setCondition('samplingschedule_periode = '.$dataPeriode['current_periode'].' AND samplingschedule_date = \''.$date.'\' AND samplingschedule_sesi = \''.$sesi.'\' AND samplingschedule_koperasi = \''.$koperasi_id.'\' AND samplingschedule_tpk = \''.$tpk_id.'\'');
            }

            $schedule->setLimit(0);
            $dataSchedule = $schedule->read();
            //print_r($dataSchedule);exit();
            if ($dataSchedule['total_data'] === 0) {
                if ($tpk_id == '') {
                    $schedule->setCondition('samplingschedule_periode = '.$dataPeriode['current_periode'].' AND samplingschedule_sesi = \''.$sesi.'\' AND samplingschedule_koperasi = \''.$koperasi_id.'\'');
                } else {
                    $schedule->setCondition('samplingschedule_periode = '.$dataPeriode['current_periode'].' AND samplingschedule_sesi = \''.$sesi.'\' AND samplingschedule_koperasi = \''.$koperasi_id.'\' AND samplingschedule_tpk = \''.$tpk_id.'\'');
                }

                $getlastschedule = $schedule->read();
                //print_r($getlastschedule);exit();
                $userlist = array();
                foreach ($getlastschedule['records'] as $k => $v) {
                    $userlist[] = '\''.$v['samplingschedule_anggota'].'\'';
                }
                //print_r($userlist);exit();

                if (count($userlist) > 0) {
                    $addSQL = ' AND anggota_id NOT IN ('.implode(',', $userlist).') ';
                } else {
                    $addSQL = '';
                }

                /*
                 * Generate Schedule
                 */
                $anggota = load::model('anggota');
                if ($tpk_id == '') {
                    $anggota->setCondition('anggota_status = \'y\' AND anggota_koperasi = \''.$koperasi_id.'\''.$addSQL);
                } else {
                    $anggota->setCondition('anggota_status = \'y\' AND anggota_koperasi = \''.$koperasi_id.'\' AND anggota_tpk = \''.$tpk_id.'\''.$addSQL);
                }
                //var_dump($anggota);exit();
                $anggota->setLimit(0);
                $dataAnggota = $anggota->read();
                $arrayAnggota = $dataAnggota['records'];
                //print_r($arrayAnggota); exit();
                $keyAnggota = array_keys($arrayAnggota);
                $limitArray = ceil($dataAnggota['total_data'] / $dataPeriode['total_periode']);
                shuffle($keyAnggota);
                //print_r($keyAnggota); exit();
                $index = 1;
                $error = 0;

                foreach ($keyAnggota as $v) {
                    if ($index <= $limitArray) {
                        $schedule->samplingschedule_id = fn::generate_id(true);
                        $schedule->samplingschedule_periode = $dataPeriode['current_periode'];
                        $schedule->samplingschedule_koperasi = $koperasi_id;
                        $schedule->samplingschedule_tpk = $arrayAnggota[$v]['anggota_tpk'];
                        $schedule->samplingschedule_anggota = $arrayAnggota[$v]['anggota_id'];
                        $schedule->samplingschedule_sesi = 'p';
                        $schedule->samplingschedule_status = 'p';
                        $schedule->samplingschedule_date = date('Y-m-d', strtotime($date));
                        if (!$schedule->insert()) {
                            ++$error;
                        }
                        $schedule->samplingschedule_id = fn::generate_id(true);
                        $schedule->samplingschedule_periode = $dataPeriode['current_periode'];
                        $schedule->samplingschedule_koperasi = $koperasi_id;
                        $schedule->samplingschedule_tpk = $arrayAnggota[$v]['anggota_tpk'];
                        $schedule->samplingschedule_anggota = $arrayAnggota[$v]['anggota_id'];
                        $schedule->samplingschedule_sesi = 's';
                        $schedule->samplingschedule_status = 'p';
                        $schedule->samplingschedule_date = date('Y-m-d', strtotime($date));
                        if (!$schedule->insert()) {
                            ++$error;
                        }
                    }
                    ++$index;
                }
                if ($error > 0) {
                    if ($tpk_id == '') {
                        $schedule->setCondition('samplingschedule_periode = '.$dataPeriode['current_periode'].' AND samplingschedule_date = \''.date('Y-m-d').'\' AND samplingschedule_sesi = \''.$sesi.'\' AND samplingschedule_koperasi = \''.$koperasi_id.'\'');
                    } else {
                        $schedule->setCondition('samplingschedule_periode = '.$dataPeriode['current_periode'].' AND samplingschedule_date = \''.date('Y-m-d').'\' AND samplingschedule_sesi = \''.$sesi.'\' AND samplingschedule_koperasi = \''.$koperasi_id.'\' AND samplingschedule_tpk = \''.$tpk_id.'\'');
                    }
                    $schedule->delete();
                    session::set('errorMsg', 'Failed while saving data into database');

                    return false;
                } else {
                    session::set('errorMsg', 'Sampling Schedule generated');

                    return true;
                }
            } else {
                session::set('errorMsg', 'Sampling Schedule Already created');

                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Generate Sampling Schedule Manualy.
     *
     * @return [type] [description]
     */
    public function generate()
    {
        if (isset($_POST['samplingschedule_koperasi'])) {
            $schedule = load::model('samplingschedule');

            if (isset($_POST['date'])) {
                $date = date('Y-m-d', fn::dateToUnix($_POST['date']));
            } else {
                $date = date('Y-m-d');
            }
            //die($date);
            $dataPeriode = $schedule->getDataPeriode($_POST['samplingschedule_koperasi'], $date);

            $schedule->setCondition('samplingschedule_periode = '.$dataPeriode['current_periode'].' AND samplingschedule_date = \''.$date.'\' AND samplingschedule_koperasi = \''.$_POST['samplingschedule_koperasi'].'\' AND samplingschedule_tpk = \''.$_POST['samplingschedule_tpk'].'\'');
            $schedule->setLimit(0);
            $dataSchedule = $schedule->read();
            //print_r($dataSchedule);exit();
            if ($dataSchedule['total_data'] === 0) {
                $schedule->setCondition('samplingschedule_periode = '.$dataPeriode['current_periode'].' AND samplingschedule_status = \'y\' AND samplingschedule_koperasi = \''.$_POST['samplingschedule_koperasi'].'\' AND samplingschedule_tpk = \''.$_POST['samplingschedule_tpk'].'\' AND date_part(\'year\', samplingschedule_date) = \''.date('Y', strtotime($date)).'\' AND date_part(\'month\', samplingschedule_date) = \''.date('n', strtotime($date)).'\'');
                $getlastschedule = $schedule->read();
                //print_r($getlastschedule);exit();
                $userlist = array();
                foreach ($getlastschedule['records'] as $k => $v) {
                    $userlist[] = '\''.$v['samplingschedule_anggota'].'\'';
                }
                // print_r($userlist);exit();
                if (count($userlist) > 0) {
                    $addSQL = ' AND anggota_id NOT IN ('.implode(',', $userlist).') ';
                } else {
                    $addSQL = '';
                }

                /*
                 * Generate Schedule
                 */
                $anggota = load::model('anggota');
                if (!isset($_POST['samplingschedule_tpk'])) {
                    $anggota->setCondition('anggota_status = \'y\' AND anggota_koperasi = \''.$_POST['samplingschedule_koperasi'].'\''.$addSQL);
                } else {
                    $anggota->setCondition('anggota_status = \'y\' AND anggota_koperasi = \''.$_POST['samplingschedule_koperasi'].'\' AND anggota_tpk = \''.$_POST['samplingschedule_tpk'].'\''.$addSQL);
                }
                //var_dump($anggota);exit();
                $anggota->setLimit(0);
                $dataAnggota = $anggota->read();
                $arrayAnggota = $dataAnggota['records'];
                $keyAnggota = array_keys($arrayAnggota);
                $sisaHariPeriode = (int) $dataPeriode['periode_enddate'] - (int) date('d', strtotime($date));
                $limitArray = ceil($dataAnggota['total_data'] / $sisaHariPeriode);
                shuffle($keyAnggota);

                $index = 1;
                $error = 0;

                foreach ($keyAnggota as $v) {
                    if ($index <= $limitArray) {
                        $schedule->samplingschedule_id = fn::generate_id(true);
                        $schedule->samplingschedule_periode = $dataPeriode['current_periode'];
                        $schedule->samplingschedule_koperasi = $_POST['samplingschedule_koperasi'];
                        $schedule->samplingschedule_tpk = $arrayAnggota[$v]['anggota_tpk'];
                        $schedule->samplingschedule_anggota = $arrayAnggota[$v]['anggota_id'];
                        $schedule->samplingschedule_sesi = 'p';
                        $schedule->samplingschedule_status = 'p';
                        $schedule->samplingschedule_date = date('Y-m-d', strtotime($date));
                        if (!$schedule->insert()) {
                            ++$error;
                        }
                        $schedule->samplingschedule_id = fn::generate_id(true);
                        $schedule->samplingschedule_periode = $dataPeriode['current_periode'];
                        $schedule->samplingschedule_koperasi = $_POST['samplingschedule_koperasi'];
                        $schedule->samplingschedule_tpk = $arrayAnggota[$v]['anggota_tpk'];
                        $schedule->samplingschedule_anggota = $arrayAnggota[$v]['anggota_id'];
                        $schedule->samplingschedule_sesi = 's';
                        $schedule->samplingschedule_status = 'p';
                        $schedule->samplingschedule_date = date('Y-m-d', strtotime($date));
                        if (!$schedule->insert()) {
                            ++$error;
                        }
                    }
                    ++$index;
                }
                if ($error > 0) {
                    $schedule->setCondition('samplingschedule_periode = '.$dataPeriode['current_periode'].' AND samplingschedule_date = \''.$date.'\' AND samplingschedule_koperasi = \''.$_POST['samplingschedule_koperasi'].'\' AND samplingschedule_tpk = \''.$_POST['samplingschedule_tpk'].'\'');
                    $schedule->delete();
                    session::set('errorMsg', 'Failed while saving data into database');
                    //return false;
                } else {
                    session::set('errorMsg', 'Sampling Schedule generated');
                    //return true;
                }
            } else {
                session::set('errorMsg', 'Sampling Schedule Already created');
                //return false;
            }
        } else {
            session::set('errorMsg', 'Invalid Request');
        }
        header('location:'.BASE_URL.'index.php?/samplingschedule/data');
    }
    public function cronjob()
    {
    }
}
