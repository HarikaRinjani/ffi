<?php

class Sync_Controller extends Controller{
    public $version = 1.3;
    public $description = "v1.3\n - Fix minor bugs";
	
	function index(){
		echo 'tes';
	}
	function updatedb(){
		$license = file_get_contents(path('base').DS.'license.ini');
        //$target_url = Config::$application['app_mirror'].'index.php?/updater/install/'.$license.'/'.$package_id.'';
		$uuid = fn::generate_id(true);
        $userAgent = 'Googlebot/2.1 (http://www.googlebot.com/bot.html)';
        $file_zip = Config::$application['file_path'].DS.'tmp'.DS.$uuid.'.zip';

        $ch = curl_init();
        $fp = fopen("$file_zip", "w");
        curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);
        curl_setopt($ch, CURLOPT_URL,Config::$application['mcp_server_url'].'index.php?/synchandler/updatedb/');
        curl_setopt($ch, CURLOPT_POST, 1);
        
        // in real life you should use something like:
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            http_build_query(
                array('mcp_tpk'   => security::encrypt(Config::$application['mcp_tpk']),
                      'mcp_key'=> security::encrypt(Config::$application['mcp_key']),
                      'mcp_koperasi' => security::encrypt(Config::$application['mcp_koperasi']),
                      'uuid'=>security::encrypt($uuid)
                )
            )
        );

        curl_setopt($ch, CURLOPT_FAILONERROR, true);
        curl_setopt($ch, CURLOPT_HEADER,0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER,true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3600);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_FILE, $fp);

        $page = curl_exec($ch);
        
        if (!$page) {
            // echo "<br />cURL error number:" .curl_errno($ch);
            // echo "<br />cURL error:" . curl_error($ch);
            // exit();
            $error = _('Failed to get update. Make sure you have an internet connection');

        }else{
	        $zip = new ZipArchive;
	        if (! $zip) {
	            $error = "Could not make ZipArchive object.";

	        }else{
	        	if($zip->open("$file_zip") != "true") {
		            $error = _("Failed performing update. Update file may be damaged or corrupted");
		        }else{
		        	$zip->extractTo(Config::$application['file_path'].DS.'tmp');
			        $zip->close();
			        @unlink($file_zip);
			        //@touch($file_zip);
			        $sql_update = Config::$application['file_path'].DS.'tmp'.DS.$uuid.'.txt';
			        if(file_exists($sql_update)){
			            $jsonfile = file_get_contents($sql_update);
			            $decrypt_jsonfile = security::decrypt($jsonfile);
			            $array_data = json_decode($decrypt_jsonfile,true);
			            //print_r($array_data);
			            foreach($array_data as $k=>$v){
			            	$modelname = substr($k,2);
			            	$__model = load::model($modelname);
			            	$primary_key = $__model->primary_key;
			            	foreach($v as $key=>$val){
			            		$__model->setId($val[$primary_key]);
			            		$totaldata = $__model->getTotalData();
			            		if($modelname === 'user'){
			            			unset($val['user_token']);
			            		}
			            		$__model->setDataArray($val);
			            		if($totaldata > 0){
			            			$__model->setId($val[$primary_key]);
			            			$__model->update();
			            		}else{
			            			$__model->insert();
			            		}
			            	}
			            }
			            @unlink($sql_update);
			            //db::query($query);
			        }
			        $error = 'Database Update successfull';
		        }
	        }   
        }
        curl_close($ch);
        fn::log('#UPDATE STATUS: '.$error);
        echo $error;
	}
	function transaksi(){
        $logger = load::model('datalog');
        $transaksi = load::model('transaksi');
        $transaksi->setCondition('transaksi_status = \'y\' AND transaksi_sync != \'y\'');
        $transaksi->setOrderBy('transaksi_waktu');
        $tr = $transaksi->read();
        load::library('tcurl');
        if($tr['total_data'] > 0){
            $fields = $transaksi->getFields();
            $data = array();
            foreach($tr['records'] as $k=>$v){
                foreach($fields as $fk=>$fv){
                    $data[$k][$fv] = $v[$fv];
                }
            }
            //$data['tes'] = 'tes';
            $json_data = json_encode($data);

            $postdata['data'] = security::encrypt($json_data);
            $postdata['user_id'] = security::encrypt(session::get('user_id'));
            $postdata['public_key'] = security::encrypt(Config::$application['mcp_key']);
            $postdata['table'] = security::encrypt('t_transaksi');
            $request = tcurl::request(Config::$application['mcp_server_url'].'index.php?/synchandler/process',$postdata);
            //print_r($request);exit();
            if($request['status'] == 'success'){
                $arrayID = array();
                foreach($data as $dk=>$dv){
                    $transaksi->setCondition('');
                    $transaksi->setId($dv['transaksi_id']);
                    $transaksi->setData('transaksi_sync','y');
                    $transaksi->update();
//                    $updateData['transaksi_sync'] = 'y';
//                    db::update('t_transaksi',$updateData,'WHERE transaksi_id = \''.$dv['transaksi_id'].'\'');
                    $arrayID[] = $dv['transaksi_id'];
                }
                $detail = load::model('transaksidetail');
                $detail->setCondition('transaksidetail_sync != \'y\' AND transaksidetail_transaksi IN (\''.implode('\',',$arrayID).'\')');
                $detail->setLimit(0);
                $dataDetail = $detail->read();
                $detailFields = $detail->getFields();
                $dataStoreDetail = array();
                foreach($dataDetail['records'] as $lk=>$lv){
                    foreach($detailFields as $dfk=>$dfv){
                        $dataStoreDetail[$lk][$dfv] = $lv[$dfv];
                    }
                }
                $json_dataDetail = json_encode($dataStoreDetail);
                
                $postdataDetail['data'] = security::encrypt($json_dataDetail);
                $postdataDetail['user_id'] = security::encrypt(session::get('user_id'));
                $postdataDetail['mcp_key'] = security::encrypt(Config::$application['mcp_key']);
                $postdataDetail['table'] = security::encrypt('t_transaksidetail');
                $requestDetail = tcurl::request(Config::$application['mcp_server_url'].'index.php?/synchandler/process',$postdataDetail);
                //print_r($requestDetail);exit();
                if($requestDetail['status'] == 'success'){
                    foreach($dataStoreDetail as $dtk=>$dtv){
                        $detail->setCondition('');
                        $detail->setId($dv['transaksidetail_id']);
                        $detail->setData('transaksidetail_sync','y');
                        $detail->update();
//                        $updateDetailData['transaksidetail_sync'] = 'y';
//                        db::update('t_transaksidetail',$updateDetailData,'WHERE transaksidetail_id = \''.$dtv['transaksidetail_id'].'\'');
                    }
                    //session::set('errorMsg','Sync success');
                    $datalog['datalog_id'] = fn::generate_id(true);
                    $datalog['datalog_time'] = date('Y-m-d H:i:s');
                    $datalog['datalog_ip'] = $_SERVER['REMOTE_ADDR'];
                    $datalog['datalog_key'] = 'sync';
                    $datalog['datalog_client'] = Config::$application['mcp_tpk'];
                    $datalog['datalog_data'] = 'Milk Collection Data synchronized by: '. session::get('user_fullname').' ('.session::get('user_id').') #ID: '.implode('\',',$arrayID);
                    $logger->setDataArray($datalog);
                    $logger->insert();
                }else{
                    //session::set('errorMsg',$requestDetail['msg']);
                    echo $requestDetail['msg'];
                }
                //session::set('errorMsg','Sync success');
            }else{
                //session::set('errorMsg',$request['msg']);
                echo $request['msg'];
            }

        }else{
            //session::set('errorMsg','Sync success');
        }
        // SYNC SETTINGS

        $postSettingsUser['data'] = security::encrypt(json_encode(array()));
        $postSettingsUser['user_id'] = security::encrypt(session::get('user_id'));
        $postSettingsUser['mcp_key'] = security::encrypt(Config::$application['mcp_key']);
        $postSettingsUser['table'] = security::encrypt('t_user');
        $requestUser = tcurl::request(Config::$application['mcp_server_url'].'index.php?/synchandler/process',$postSettingsUser);
        //print_r($requestUser);exit();
        $usermod = load::model('user');
        foreach($requestUser['msg'] as $k=>$v){
            $usermod->setCondition('user_id = \''.$v['user_id'].'\'');
            $tes = $usermod->read();
            if($tes['total_data'] === 0){
                $usermod->setDataArray($v);
                $usermod->insert();
            }else{
                $usermod->setId($v['user_id']);
                $usermod->setDataArray($v);
                $usermod->update();
            }
        }
        $datalog['datalog_id'] = fn::generate_id(true);
        $datalog['datalog_time'] = date('Y-m-d H:i:s');
        $datalog['datalog_ip'] = $_SERVER['REMOTE_ADDR'];
        $datalog['datalog_key'] = 'sync';
        $datalog['datalog_client'] = Config::$application['mcp_tpk'];
        $datalog['datalog_data'] = 'User Data synchronized by: '. session::get('user_fullname').' ('.session::get('user_id').') ';
        $logger->setDataArray($datalog);
        $logger->insert();
        echo 'success';
    }

    function softwareupdate(){
        $package_dir = path('app').DS.'controllers';
        $package = $this->openDir($package_dir);
        $data = array();

        $i=0;
        foreach($package as $v){
            if($v != 'login'){
                require_once $package_dir.DS.$v.'.php';
                $controller = ucfirst($v) . '_Controller';
                $name = new $controller;
                $data[$i]['package'] = $v;
                $data[$i]['version'] = $name->version;
                $data[$i]['publisher'] = $name->publisher;
                $data[$i]['description'] = $name->description;
                $data[$i]['dependencies'] = $name->dependencies;
                $data[$i]['type'] = 'package';
                $i++;
            }
        }
        
    }
    protected function openDir($dir){
        $files = scandir($dir);
        $package = array();
        foreach($files as $k=>$v){
            if (preg_match('/^[^_].*\.(php)$/i', $v)){
                $package[] = str_replace('.php','',$v);
            }
        }
        return $package;
    }
    function all(){
        $this->transaksi();
        $this->updatedb();
    }
    function randomNumber(){
    	echo $_SESSION['kg'];
    }
}