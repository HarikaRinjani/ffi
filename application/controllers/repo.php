<?php 

class Repo_Controller extends Controller
{
	public function __construct(){
        parent::__construct();
        security::checkSession();
    }


    public function index(){
        session::set('title','Repository');
        $repo = load::model('repo');
        $koperasi = $repo->setCondition('repo_type = \'koperasi\'')
			        	 ->setOrderBy('updated_at')
			        	 ->setOrderType('DESC')
			        	 ->setLimit(1)
			        	 ->read();
		$user = $repo->setCondition('repo_type = \'user\'')
					 ->setOrderBy('updated_at')
		        	 ->setOrderType('DESC')
		        	 ->setLimit(1)
		        	 ->read();
		$peternak = $repo->setCondition('repo_type = \'anggota\'')
						 ->setOrderBy('updated_at')
			        	 ->setOrderType('DESC')
			        	 ->setLimit(1)
			        	 ->read();
        View::display('repo/repo.index', array('koperasi' => $koperasi, 'user' => $user, 'peternak' => $peternak));
    }
    public function update()
    {
    	$repo = load::model('repo');
    	$repo->updateRepo('koperasi');
    	$repo->updateRepo('user');
    	$repo->updateRepo('periode');
    	$repo->updateRepo('anggota');
    	header('location:'.BASE_URL.'index.php?/repo/index');
    	exit();
    }
}